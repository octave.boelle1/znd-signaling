package zoranodensha.trackpack.client;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class ObjTrackSpeedy extends ObjModelSpeedy
{
	public ObjectList fastening;
	public ObjectList guide;
	public ObjectList plate;
	public ObjectList rail;
	public ObjectList rail_broken;
	public ObjectList trackbed;
	public ObjectList trackbed_broken;



	public ObjTrackSpeedy(String modelName)
	{
		super(ModData.ID, modelName);

		fastening = makeGroupSafe("fastening");
		guide = makeGroupSafe("guide");
		plate = makeGroupSafe("plate");
		rail = makeGroupSafe("rail");
		rail_broken = makeGroupSafe("railBroken");
		trackbed = makeGroupSafe("trackbed");
		trackbed_broken = makeGroupSafe("trackbedBroken");
	}

	public ObjectList makeGroupSafe(String s)
	{
		try
		{
			return makeGroup(s);
		}
		catch (Exception e)
		{
			return null;
		}
	}
}
