package zoranodensha.trackpack.client;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.util.ResourceLocation;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.api.structures.tracks.ARailwaySectionRenderer;
import zoranodensha.api.structures.tracks.IRailwaySectionRenderer;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.Mat3x3;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class RailwaySectionRenderer_Switch_1131_0000 extends ARailwaySectionRenderer
{
	private static final ResourceLocation[] textures = new ResourceLocation[] {
			new ResourceLocation(ModData.ID, "textures/tracks/Track_switch_wood.png"),
			new ResourceLocation(ModData.ID, "textures/tracks/Track_switch_concrete.png")
			};
	private static final SwitchModel[] models = new SwitchModel[] {
			new SwitchModel(ModCenter.DIR_RESOURCES_TRACKS + "ModelTrack_switchL_3x10_1131_0000"),
			new SwitchModel(ModCenter.DIR_RESOURCES_TRACKS + "ModelTrack_switchR_3x10_1131_0000")
			};
	private static final Mat3x3 rotationMatrix = new Mat3x3();
	

	@Override
	public void renderTrack(int detail, int gagTrack, IRailwaySection section, ITrackBase trackBase, float x, float y, float z, float f)
	{
		super.renderTrack(detail, gagTrack, section, trackBase, x, y, z, f);

		boolean isRightHanded = trackBase.getDirectionOfSection() == EDirection.RIGHT;
		SwitchModel model = models[isRightHanded ? 1 : 0];
		int orientation = trackBase.getOrientation();

		if (gagTrack > 0)
		{
			Minecraft.getMinecraft().renderEngine.bindTexture(textures[1]);
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
			GL11.glTranslatef(x + 0.5F, y, z + 0.5F);
			
			if (gagTrack == 1)
			{
				GL11.glColor4f(0.5F, 0.5F, 1.0F, 0.5F);
			}
			else
			{
				GL11.glColor4f(1.0F, 0.5F, 0.5F, 0.5F);
			}
			
			if (orientation % 2 != 0)
			{
				GL11.glRotatef(orientation * 90.0F - 180.0F, 0.0F, 1.0F, 0.0F);
			}
			else
			{
				GL11.glRotatef(orientation * 90.0F, 0.0F, 1.0F, 0.0F);
			}
			
			model.cablebox.render();
			model.trackbed.render();
			model.fastening.render();
			model.rail.render();
			renderTongues(model, isRightHanded, 0.0F);
			renderSignal(model, isRightHanded, 0.0F);
			GL11.glDisable(GL11.GL_BLEND);
		}
		else
		{
			int type = trackBase.getField(0, 0, Integer.class);
			boolean hideLever = trackBase.getField(4, false, Boolean.class);
			Minecraft.getMinecraft().renderEngine.bindTexture(textures[type % 2]);
			
			GL11.glPushMatrix();
			GL11.glTranslatef(x + 0.5F, y, z + 0.5F);
			
			if (orientation % 2 != 0)
			{
				GL11.glRotatef(orientation * 90.0F - 180.0F, 0.0F, 1.0F, 0.0F);
			}
			else
			{
				GL11.glRotatef(orientation * 90.0F, 0.0F, 1.0F, 0.0F);
			}

			float f0 = trackBase.getField(2, 0.0F, Float.class);
			float f1 = (f0 / 2.1F) * 2;
			boolean isAnimated = (f0 > 0.0F) && (f0 < 2.1F);
			
			Tessellator tessellator = Tessellator.instance;
			tessellator.startDrawing(GL11.GL_TRIANGLES);
			{
				TesselatorVertexState vertexState = trackBase.getField(10, null, TesselatorVertexState.class);
				if (vertexState == null)
				{	
					tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
					switch (trackBase.getStateOfShape())
					{
						case PLATED:
						case FINISHED:
							model.rail.render(tessellator);
							if (!isAnimated)
							{
								tessellator.setTranslation((isRightHanded ? -3.471D : 3.471D), 0.0D, -1.002D);
								model.ton0.render(tessellator, rotationMatrix.reset().rotateY(isRightHanded ? f0 : -f0));
								tessellator.setTranslation((isRightHanded ? -4.465D : 4.465D), 0.0D, -0.687D);
								model.ton1.render(tessellator, rotationMatrix.reset().rotateY(isRightHanded ? f0 : -f0));
								tessellator.setTranslation(0, 0, 0);
							}
							
						case FASTENED:
							model.fastening.render(tessellator);
							if (!hideLever && !isAnimated)
							{
								float f3 = (isRightHanded ? -f1 : f1);
								tessellator.setTranslation((isRightHanded ? -2.3D : 2.3D), 0.0D, 0.715D);
								model.lever.render(tessellator, rotationMatrix.reset().rotateY(f3 * -5.65F).rotateX(f1 * -45.0F).rotateY(f3 * 5.65F));
								tessellator.setTranslation(0, 0, 0);
							}
							
						case TRACKBED:
							model.trackbed.render(tessellator);
							model.cablebox.render(tessellator);
							if (!isAnimated)
							{
								tessellator.setTranslation((isRightHanded ? -2.2D : 2.2D), 0.0F, 0.735D);
								model.signal.render(tessellator, rotationMatrix.reset().rotateY((isRightHanded ? -f1 : f1) * 45.0F));
								tessellator.setTranslation(0, 0, 0);
							}
							break;
							
						case BUILDINGGUIDE:
							model.guide.render(tessellator);
							break;
					}
					trackBase.setField(10, VertexStateCreator_Tris.getVertexState());
				}
				else
				{
					tessellator.setVertexState(vertexState);
				}
			}
			tessellator.draw();
			
			if (isAnimated)
			{
				switch (trackBase.getStateOfShape())
				{
					case PLATED:
					case FINISHED:
						renderTongues(model, isRightHanded, f0);
						
					case FASTENED:
						if (!hideLever)
						{
							renderLever(model, isRightHanded, f1);
						}
						
					case TRACKBED:
						renderSignal(model, isRightHanded, f1);
						break;
				}
			}
			GL11.glPopMatrix();
			
			TrackRenderHelper.renderConcreteBed(trackBase, x, y, z);
		}
	}

	private static void renderLever(SwitchModel model, boolean isRightHanded, float f)
	{
		float f1 = (isRightHanded ? -f : f);

		GL11.glPushMatrix();
		GL11.glTranslatef((isRightHanded ? -2.3F : 2.3F), 0.0F, 0.715F);
		GL11.glRotatef(f1 * 5.65F, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(f * 45.0F, 1.0F, 0.0F, 0.0F);
		GL11.glRotatef(f1 * -5.65F, 0.0F, 1.0F, 0.0F);
		model.lever.render();
		GL11.glPopMatrix();
	}

	private static void renderSignal(SwitchModel model, boolean isRightHanded, float rota)
	{
		GL11.glPushMatrix();
		GL11.glTranslatef((isRightHanded ? -2.2F : 2.2F), 0.0F, 0.735F);
		GL11.glRotatef((isRightHanded ? -rota : rota) * 45.0F, 0.0F, 1.0F, 0.0F);
		model.signal.render();
		GL11.glPopMatrix();
	}

	private static void renderTongues(SwitchModel model, boolean isRightHanded, float rota)
	{
		GL11.glPushMatrix();
		GL11.glTranslatef((isRightHanded ? -3.471F : 3.471F), 0.0F, -1.002F);
		GL11.glRotatef((isRightHanded ? rota : -rota), 0.0F, 1.0F, 0.0F);
		model.ton0.render();
		GL11.glPopMatrix();
		GL11.glPushMatrix();
		GL11.glTranslatef((isRightHanded ? -4.465F : 4.465F), 0.0F, -0.687F);
		GL11.glRotatef((isRightHanded ? rota : -rota), 0.0F, 1.0F, 0.0F);
		model.ton1.render();
		GL11.glPopMatrix();
	}
	
	
	@SideOnly(Side.CLIENT)
	private static class SwitchModel extends ObjTrackSpeedy
	{
		public ObjectList cablebox;
		public ObjectList lever;
		public ObjectList signal;
		public ObjectList ton0;
		public ObjectList ton1;

		public SwitchModel(String modelName)
		{
			super(modelName);
			
			cablebox = makeGroupSafe("cablebox");
			lever = makeGroupSafe("lever");
			signal = makeGroupSafe("signal");
			ton0 = makeGroupSafe("ton0");
			ton1 = makeGroupSafe("ton1");
		}
	}
}
