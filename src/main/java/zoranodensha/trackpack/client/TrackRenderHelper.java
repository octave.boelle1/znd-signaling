package zoranodensha.trackpack.client;

import java.util.List;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.tileentity.TileEntity;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.structures.tracks.RailwaySectionBase;
import zoranodensha.common.core.ModCenter;



/**
 * Class containing helper methods for track rendering.
 */
@SideOnly(Side.CLIENT)
public final class TrackRenderHelper
{
	/**
	 * Render concrete ballast extensions where applicable.<br>
	 * <br>
	 * Uses index 11 in the track's field list.
	 */
	@SideOnly(Side.CLIENT)
	public static final void renderConcreteBed(ITrackBase trackBase, float x, float y, float z)
	{
		if (trackBase instanceof TileEntity && trackBase.getStateOfShape().isFinished() && trackBase.getField(0, 0, Integer.class) % 2 != 0)
		{
			GL11.glTranslatef(x + 0.5F, y, z + 0.5F);
			Minecraft.getMinecraft().renderEngine.bindTexture(TextureMap.locationBlocksTexture);

			TileEntity tile = (TileEntity)trackBase;
			boolean hasRendered = false;

			Tessellator tessellator = Tessellator.instance;
			tessellator.startDrawingQuads();
			{
				TesselatorVertexState vertexState = trackBase.getField(11, null, TesselatorVertexState.class);
				if (vertexState == null || ModCenter.cfg.rendering.detailTracks > 1)
				{
					RenderBlocks renderBlocks = RenderBlocks.getInstance();
					if (renderBlocks.blockAccess != Minecraft.getMinecraft().theWorld)
					{
						renderBlocks.blockAccess = Minecraft.getMinecraft().theWorld;
					}

					tessellator.setTranslation(-(tile.xCoord + 0.5), -tile.yCoord, -(tile.zCoord + 0.5));

					List<int[]> gags = RailwaySectionBase.getGagBlocksWithRotation(0, trackBase.getOrientation(), trackBase.getInstanceOfShape().getGagBlocks(trackBase.getDirectionOfSection()));
					for (int[] gag : gags)
					{
						int gagX = tile.xCoord + gag[0];
						int gagY = tile.yCoord + gag[1];
						int gagZ = tile.zCoord + gag[2];

						if (tile.getWorldObj().getBlock(gagX, gagY - 1, gagZ) == ModCenter.BlockBallastConcrete)
						{
							tessellator.setNormal(0, 1, 0);
							tessellator.setBrightness(tile.getWorldObj().getLightBrightnessForSkyBlocks(gagX, gagY, gagZ, 0));
							renderBlocks.setRenderBounds(0.0, 0.0, 0.0, 1.0, 0.0315, 1.0);
							
							int l = tile.getWorldObj().getLightBrightnessForSkyBlocks(gagX, gagY, gagZ, 0);
							int i1 = l % 65536;
					        int j1 = l / 65536;
					        OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, i1, j1);
					        GL11.glColor3f(1.0F, 1.0F, 1.0F);

							renderBlocks.setRenderAllFaces(true);
							if (renderBlocks.renderStandardBlock(ModCenter.BlockBallastConcrete, gagX, gagY, gagZ))
							{
								hasRendered = true;
							}
							renderBlocks.setRenderAllFaces(false);
						}
					}

					if (hasRendered && ModCenter.cfg.rendering.detailTracks <= 1)
					{
						trackBase.setField(11, tessellator.getVertexState(0, 0, 0));
					}
					tessellator.setTranslation(0, 0, 0);
				}
				else
				{
					tessellator.setVertexState(vertexState);
				}
			}
			tessellator.draw();
		}
	}
}