package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.tileentity.TileEntity;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.ARailwaySectionRenderer;
import zoranodensha.api.structures.tracks.IRailwaySectionRenderer;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.structures.tracks.Path;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.util.PositionStackEntry;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;


public class ZnD_Switch_1131_0000 extends ASwitch {
    public ZnD_Switch_1131_0000() {
        super(7, 11, 11, 0, "ZnD_Switch_1131_0000", 10,
                new Path[]{new Path(new String[]{"(0.2 * x) + 0.5", "0.2514"}, null), new Path(new String[]{"(0.0001 * (x-10)^3) - (0.009 * (x-10)^2) + 1.5", "-(0.0003 * x^2) - (0.02214 * x) + 0.2514"}, null)});
    }

    @SideOnly(Side.CLIENT)
    public ZnD_Switch_1131_0000(IRailwaySectionRenderer render) {
        this();
        this.render = render;
    }

    @Override
    public EShape getEShapeType() {
        return EShape.SWITCH;
    }

    @Override
    public List<int[]> getGagBlocks(EDirection dir) {
        List<int[]> list = new ArrayList<int[]>();

        int modifier = dir == EDirection.LEFT ? 1 : -1;


        for (int i = 0; i < length; ++i) {
            if (i < 5) {
                for (int j = 0; j < 2; ++j) {
                    list.add(new int[]{modifier * i, 0, -j});
                }
            } else if (i == 5) {
                list.add(new int[]{modifier * i, 0, -1});
            } else {
                for (int j = 1; j < 3; ++j) {
                    list.add(new int[]{modifier * i, 0, -j});
                }
            }
        }


        return list;
    }

    @Override
    public double[][] getPositionOnTrack(ITrackBase track, double vehX, double vehY, double vehZ, @Nullable Object vehicle, TileEntity tile, double localX, double localZ) {
        /* Prepare return value, retrieve current path and side orientation. */
        Path path = getPath(track.getPath());
        int rota = track.getOrientation();
        boolean isRight = track.getDirectionOfSection() == EDirection.RIGHT;

        /*
         * Determine whether set path or other path will be selected.
         */
        boolean useSetPath = localX <= 2.0D;

        if (vehicle instanceof PartTypeBogie) {
            PartTypeBogie bogie = (PartTypeBogie) vehicle;
            double prevLocalX = TileEntityTrackBase.getLocalX(isRight ? (rota + 2) : rota, bogie.getPrevPosX(), bogie.getPrevPosZ(), tile.xCoord, tile.zCoord, track.getLengthOfSection());

            /* If we're heading for the split-track end, use the set path. */
            if (localX > prevLocalX) {
                useSetPath = true;
            }
        } else if (vehicle instanceof EntityMinecart) {
            EntityMinecart cart = (EntityMinecart) vehicle;
            double prevLocalX = TileEntityTrackBase.getLocalX(isRight ? (rota + 2) : rota, cart.prevPosX, cart.prevPosZ, tile.xCoord, tile.zCoord, track.getLengthOfSection());

            /* If we're heading for the split-track end, use the set path. */
            if (localX > prevLocalX) {
                useSetPath = true;
            }
        } else if (vehicle instanceof PositionStackEntry) {
            PositionStackEntry pse = (PositionStackEntry) vehicle;
            double prevLocalX = TileEntityTrackBase.getLocalX(isRight ? (rota + 2) : rota, pse.x, pse.z, tile.xCoord, tile.zCoord, track.getLengthOfSection());

            /* If we're heading for the split-track end, use the set path. */
            if (localX > prevLocalX) {
                useSetPath = true;
            }
        }

        if (!useSetPath) {
            Path pathSet = path;
            Path pathOther = getPath(track.getPath() > 0 ? 0 : 1);
            double[] posSet = pathSet.calculatePosition(localX);
            double[] posOther = pathOther.calculatePosition(localX);

            path = (Math.abs(posOther[0] - localZ) < Math.abs(posSet[0] - localZ)) ? pathOther : pathSet;
        }

        /*
         * Apply new position and rotation data.
         */
        double[][] ret = new double[][]{path.calculatePosition(localX), path.calculateRotation(localX)};

        for (int i = 0; i < 3; ++i) {
            switch (i) {
                case 0:
                    ret[1][i] *= (isRight ? -45.0D : 45.0D);
                    ret[1][i] += (rota % 4) * 90.0D;
                    break;
                default:
                    ret[1][i] *= 45.0D;
                    break;
            }
            ret[1][i] %= 360.0D;
        }

        double[] pos = new double[]{0.0D, tile.yCoord, 0.0D};

        switch (rota % 4) {
            case 0:
                pos[0] = vehX + ret[0][2];
                pos[2] = tile.zCoord - ret[0][0] + 1.0D;
                break;
            case 1:
                pos[0] = tile.xCoord + ret[0][0];
                pos[2] = vehZ + ret[0][2];
                break;
            case 2:
                pos[0] = vehX + ret[0][2];
                pos[2] = tile.zCoord + ret[0][0];
                break;
            case 3:
                pos[0] = tile.xCoord - ret[0][0] + 1.0D;
                pos[2] = vehZ + ret[0][2];
                break;
        }

        ret[0] = pos;
        return ret;
    }

    @Override
    public List<Integer> getValidPaths() {
        List<Integer> paths = new ArrayList<Integer>();
        paths.add(0);
        paths.add(1);
        return paths;
    }
}
