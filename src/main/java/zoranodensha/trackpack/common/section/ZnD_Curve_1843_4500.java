package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.ARailwaySectionRenderer;
import zoranodensha.api.structures.tracks.IRailwaySectionRenderer;
import zoranodensha.api.structures.tracks.Path;



public class ZnD_Curve_1843_4500 extends RailwaySectionTrackPack
{
	public ZnD_Curve_1843_4500()
	{ 
		super(6, 6, 6, 0, "ZnD_Curve_1843_4500", 6, new Path[] {
				new Path(new String[] { "(0.0515 * x^2) + (0.275 * x) + 0.5", "(0.0984 * x) + 0.4096" }, null)
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_Curve_1843_4500(IRailwaySectionRenderer render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.CURVE;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.LEFT)
		{
			for (int i = 0; i < length; ++i)
			{
				switch (i)
				{
					case 0:
					case 1:
						for (int j = 0; j < 2; ++j)
						{
							list.add(new int[] { i, 0, -j });
						}
						break;
					case 2:
						for (int j = 0; j < 3; ++j)
						{
							list.add(new int[] { i, 0, -j });
						}
						break;
					case 3:
						for (int j = 1; j < 3; ++j)
						{
							list.add(new int[] { i, 0, -j });
						}
						break;
					case 4:
						for (int j = 1; j < 4; ++j)
						{
							list.add(new int[] { i, 0, -j });
						}
						break;
					case 5:
						for (int j = 2; j < 5; ++j)
						{
							list.add(new int[] { i, 0, -j });
						}
						break;
				}
			}
		}
		else if (dir == EDirection.RIGHT)
		{
			for (int i = 0; i < length + 1; ++i)
			{
				switch (i)
				{
					case 0:
					case 1:
						for (int j = 0; j < 2; ++j)
						{
							list.add(new int[] { -i, 0, -j });
						}
						break;
					case 2:
						for (int j = 0; j < 3; ++j)
						{
							list.add(new int[] { -i, 0, -j });
						}
						break;
					case 3:
						for (int j = 1; j < 3; ++j)
						{
							list.add(new int[] { -i, 0, -j });
						}
						break;
					case 4:
						for (int j = 1; j < 4; ++j)
						{
							list.add(new int[] { -i, 0, -j });
						}
						break;
					case 5:
						for (int j = 2; j < 4; ++j)
						{
							list.add(new int[] { -i, 0, -j });
						}
						break;
					case 6:
						list.add(new int[] { -i, 0, -3 });
						break;
				}
			}
		}
		return list;
	}
}
