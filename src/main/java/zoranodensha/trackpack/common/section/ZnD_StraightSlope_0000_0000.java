package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.structures.tracks.*;


public class ZnD_StraightSlope_0000_0000 extends RailwaySectionTrackPack
{
	public ZnD_StraightSlope_0000_0000()
	{
		// Note: Keep "old" name (ZnD_Slope_0000_0000) to maintain backwards-compatibility.
		super(10, 10, 10, 0, "ZnD_Slope_0000_0000", 10, new Path[] {
				new Path(new String[] { "0.5",     "0" },
				         new String[] { "0.1 * x", "5.71" })
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_StraightSlope_0000_0000(IRailwaySectionRenderer render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.STRAIGHT;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.NONE)
		{
			for (int i = 0; i < 10; ++i)
			{
				list.add(new int[] { i, 0, 0, i });
			}
		}
		return list;
	}
}
