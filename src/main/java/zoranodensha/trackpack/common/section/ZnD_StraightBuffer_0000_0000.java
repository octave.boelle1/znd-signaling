package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.ARailwaySectionRenderer;
import zoranodensha.api.structures.tracks.IRailwaySectionRenderer;
import zoranodensha.api.structures.tracks.Path;


public class ZnD_StraightBuffer_0000_0000 extends ABuffer
{
	public ZnD_StraightBuffer_0000_0000()
	{
		super(2, 2, 2, 1, "ZnD_StraightBuffer_0000_0000", 2, new Path[] {
				new Path(new String[] { "0.5", "0" }, null)
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_StraightBuffer_0000_0000(IRailwaySectionRenderer render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.STRAIGHT;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.NONE)
		{
			list.add(new int[] { 0, 0, 0 });
			list.add(new int[] { 1, 0, 0 });
		}
		return list;
	}

}
