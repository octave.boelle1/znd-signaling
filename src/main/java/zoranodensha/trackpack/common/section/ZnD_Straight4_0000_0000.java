package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import zoranodensha.api.structures.signals.APulse;
import zoranodensha.api.structures.signals.IPulseEmitter;
import zoranodensha.api.structures.signals.TrainPulse;
import zoranodensha.api.structures.tracks.*;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;


public class ZnD_Straight4_0000_0000 extends RailwaySectionTrackPack
{

	public ZnD_Straight4_0000_0000()
	{
		super(4, 4, 4, 4, "ZnD_Straight4_0000_0000", 4, new Path[] {
				new Path(new String[] { "0.5", "0" }, null)
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_Straight4_0000_0000(IRailwaySectionRenderer render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.STRAIGHT;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.NONE)
		{
			for (int i = 0; i < 4; ++i)
			{
				list.add(new int[] { i, 0, 0 });
			}
		}
		return list;
	}

}
