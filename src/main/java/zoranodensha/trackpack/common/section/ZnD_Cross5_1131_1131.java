package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.tileentity.TileEntity;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.ARailwaySectionRenderer;
import zoranodensha.api.structures.tracks.IRailwaySectionRenderer;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.structures.tracks.Path;
import zoranodensha.api.vehicles.handlers.MovementHandler;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.util.PositionStackEntry;



public class ZnD_Cross5_1131_1131 extends RailwaySectionTrackPack
{
	public ZnD_Cross5_1131_1131()
	{
		super(6, 6, 6, 0, "ZnD_Cross5_1131_1131", 5, new Path[] {
				new Path(new String[] {  "(0.2 * x) + 0.5",  "0.2514" }, null),
				new Path(new String[] { "-(0.2 * x) + 1.5", "-0.2514" }, null)
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_Cross5_1131_1131(IRailwaySectionRenderer render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.CROSS;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		
		for (int i = 0; i < length; ++i)
		{
			for (int j = 0; j < 2; ++j)
			{
				list.add(new int[] { i, 0, -j });
			}
		}
		
		return list;
	}

	@Override
	public double[][] getPositionOnTrack(ITrackBase track, double vehX, double vehY, double vehZ, @Nullable Object vehicle, TileEntity tile, double localX, double localZ)
	{
		/* Prepare return value, retrieve current path and side orientation. */
		Path path = getPath(0);
		int rota = track.getOrientation();

		/*
		 * Determine which path will be selected.
		 * 
		 * If vehicle is null, return default path.
		 * Otherwise use vehicle to determine correct path by comparing whether previous
		 * and current localX of the given vehicle lie on a line along either X or Z.
		 */
		float trackYaw = -11.31F;
		if (rota % 2 != 0)
		{
			trackYaw += 90.0F;
		}

		if (vehicle instanceof PartTypeBogie)
		{
			PartTypeBogie bogie = (PartTypeBogie)vehicle;
			if (MovementHandler.INSTANCE.getIsRotationMatching(bogie.getRotationYaw(), trackYaw, 1.0F))
			{
				path = getPath(1);
			}
		}
		else if (vehicle instanceof EntityMinecart)
		{
			EntityMinecart cart = (EntityMinecart)vehicle;
			if (MovementHandler.INSTANCE.getIsRotationMatching(cart.rotationYaw, trackYaw, 1.0F))
			{
				path = getPath(1);
			}
		}
		else if (vehicle instanceof PositionStackEntry)
		{
			if (MovementHandler.INSTANCE.getIsRotationMatching(((PositionStackEntry)vehicle).yaw, trackYaw, 1.0F))
			{
				path = getPath(1);
			}
		}

		/*
		 * Apply new position and rotation data.
		 */
		double[][] ret = new double[][] { path.calculatePosition(localX), path.calculateRotation(localX) };

		for (int i = 0; i < 3; ++i)
		{
			switch (i)
			{
				case 0:
					ret[1][i] *= 45.0D;
					ret[1][i] += (rota % 4) * 90.0D;
					break;
				default:
					ret[1][i] *= 45.0D;
					break;
			}
			ret[1][i] %= 360.0D;
		}

		double[] pos = new double[] { 0.0D, tile.yCoord, 0.0D };

		switch (rota % 4)
		{
			case 0:
				pos[0] = vehX + ret[0][2];
				pos[2] = tile.zCoord - ret[0][0] + 1.0D;
				break;
			case 1:
				pos[0] = tile.xCoord + ret[0][0];
				pos[2] = vehZ + ret[0][2];
				break;
			case 2:
				pos[0] = vehX + ret[0][2];
				pos[2] = tile.zCoord + ret[0][0];
				break;
			case 3:
				pos[0] = tile.xCoord - ret[0][0] + 1.0D;
				pos[2] = vehZ + ret[0][2];
				break;
		}

		ret[0] = pos;
		return ret;
	}

	@Override
	public List<Integer> getValidPaths()
	{
		List<Integer> paths = new ArrayList<Integer>();
		paths.add(0);
		paths.add(1);
		return paths;
	}
}
