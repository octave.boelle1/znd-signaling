package zoranodensha.trackpack.common.section;

import mods.railcraft.api.core.items.IToolCrowbar;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.structures.tracks.Path;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.items.ItemTrackPart;

import java.util.List;

public abstract class ABuffer extends RailwaySectionTrackPack {
    public ABuffer(int trackbeds, int fastenings, int rails, int plates, String name, int length, Path[] paths) {
        super(trackbeds, fastenings, rails, plates, name, length, paths);
    }

    @Override
    public List<ItemStack> onTrackDestroyed(TileEntity tileEntity, EntityPlayer player)
    {
        if ((player == null || !player.capabilities.isCreativeMode) && ((ITrackBase)tileEntity).getField(2, false, Boolean.class))
        {
            if (player == null || (player.getHeldItem() != null && player.getHeldItem().getItem() instanceof IToolCrowbar))
            {
                ((ITrackBase)tileEntity).setField(2, false);
                ModCenter.BlockTrackBase.dropBlockAsItem(tileEntity.getWorldObj(), tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord, new ItemStack(ModCenter.ItemTrackPart, 1, 6));
            }
            return null;
        }

        return super.onTrackDestroyed(tileEntity, player);
    }

    @Override
    public boolean onTrackWorkedOn(ITrackBase trackBase, EntityPlayer player, boolean isRemote)
    {
        if (super.onTrackWorkedOn(trackBase, player, isRemote))
        {
            return true;
        }
        ItemStack currentItem = player.getHeldItem();
        if (currentItem == null)
        {
            return false;
        }
        if (currentItem.getItem() instanceof ItemTrackPart && currentItem.getItemDamage() == 6)
        {
            if (!trackBase.getField(2, false, Boolean.class))
            {
                if (player.capabilities.isCreativeMode || getInventorySufficientStack(player.inventory, new ItemStack(currentItem.getItem(), 1, 6), 1))
                {
                    trackBase.setField(2, true);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void onReadFromNBT(NBTTagCompound nbt, ITrackBase trackBase)
    {
        for (int i = 0; i < 3; ++i)
        {
            String s = nbt.getString("FieldDec_" + i);
            if ("int".equals(s))
            {
                trackBase.setField(i, nbt.getInteger("Field_" + i));
            }
            else if ("boolean".equals(s))
            {
                trackBase.setField(i, nbt.getBoolean("Field_" + i));
            }
        }
    }

    @Override
    public void onWriteToNBT(NBTTagCompound nbt, ITrackBase trackBase)
    {
        super.onWriteToNBT(nbt, trackBase);
        nbt.setString("FieldDec_2", "boolean");
        nbt.setBoolean("Field_2", trackBase.getField(2, false, Boolean.class));
    }

    @Override
    public TileEntity initializeTrack(ITrackBase trackBase)
    {
        super.initializeTrack(trackBase);
        trackBase.setField(2, false);
        return (TileEntity)trackBase;
    }
}
