package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import mods.railcraft.api.core.items.IToolCrowbar;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import zoranodensha.api.items.IItemTrackPart;
import zoranodensha.api.structures.tracks.*;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.items.ItemTrackPart;



public class ZnD_StraightMaint_0000_0000 extends RailwaySectionTrackPack
{
	public ZnD_StraightMaint_0000_0000()
	{
		super(0, 1, 1, 0, "ZnD_StraightMaint_0000_0000", 1, new Path[] {
				new Path(new String[] { "0.5", "0" },
				         new String[] { "1.0", "0" })
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_StraightMaint_0000_0000(IRailwaySectionRenderer render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.STRAIGHT;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.NONE)
		{
			list.add(new int[] { 0, 0, 0 });
		}
		return list;
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox(double x, double y, double z)
	{
		return AxisAlignedBB.getBoundingBox(x - length, y, z - length, x + length + 1.0D, y + 2.0D, z + length + 1.0D);
	}

	@Override
	public boolean isMaintenanceTrack()
	{
		return true;
	}

	@Override
	public List<ItemStack> onTrackDestroyed(TileEntity tileEntity, EntityPlayer player)
	{
		if (!player.capabilities.isCreativeMode)
		{
			ITrackBase trackBase = (ITrackBase)tileEntity;
			List<ItemStack> drops = new ArrayList<ItemStack>();
			boolean flag = player.getHeldItem() != null && player.getHeldItem().getItem() instanceof IToolCrowbar;
			switch (trackBase.getStateOfShape())
			{
				case BUILDINGGUIDE: {
					drops.add(null);
					return drops;
				}
				case TRACKBED: {
					if (flag)
					{
						drops.add(new ItemStack(ModCenter.ItemTrackPart, 1, 5));
					}
					drops.add(null);
					return drops;
				}
				case FASTENED: {
					if (flag)
					{
						drops.add(new ItemStack(ModCenter.ItemTrackPart, fastenings, 4));
					}
					trackBase.setStateOfShape(ERailwayState.TRACKBED);
					return drops;
				}
				case FINISHED: {
					int damage = trackBase.getField(1, 0, Integer.class);
					if (damage < 2)
					{
						if (flag)
						{
							drops.add(new ItemStack(ModCenter.ItemTrackPart, rails, 2));
							trackBase.setStateOfShape(ERailwayState.FASTENED);
							return drops;
						}
						trackBase.setField(1, damage + 2);
						return null;
					}
					trackBase.setStateOfShape(ERailwayState.FASTENED);
					trackBase.setField(1, damage - 2);
					break;
				}
			}
		}
		return null;
	}

	@Override
	public boolean onTrackWorkedOn(ITrackBase trackBase, EntityPlayer player, boolean isRemote)
	{
		ItemStack currentItem = player.getHeldItem();
		if (currentItem == null)
		{
			return false;
		}
		boolean flag0 = player.capabilities.isCreativeMode;
		if (currentItem.getItem() instanceof IItemTrackPart)
		{
			if (((IItemTrackPart)currentItem.getItem()).type(currentItem) != null)
			{
				switch (((IItemTrackPart)currentItem.getItem()).type(currentItem))
				{
					case RAIL:
						if (trackBase.getStateOfShape().equals(ERailwayState.FASTENED) && rails > 0)
						{
							if (flag0 || getInventorySufficientStack(player.inventory, new ItemStack(currentItem.getItem(), rails, 2), 1))
							{
								if (!isRemote)
								{
									trackBase.setStateOfShape(ERailwayState.FINISHED);
								}
								return true;
							}
						}
						break;
					case FASTENING:
						if (trackBase.getStateOfShape().equals(ERailwayState.TRACKBED) && fastenings > 0)
						{
							if (flag0 || getInventorySufficientStack(player.inventory, new ItemStack(currentItem.getItem(), fastenings, 4), 1))
							{
								if (!isRemote)
								{
									trackBase.setStateOfShape(ERailwayState.FASTENED);
								}
								return true;
							}
						}
						break;
				}
			}
			else if (currentItem.getItem() instanceof ItemTrackPart)
			{
				if (trackBase.getStateOfShape().equals(ERailwayState.BUILDINGGUIDE))
				{
					if (flag0 || getInventorySufficientStack(player.inventory, new ItemStack(currentItem.getItem(), 1, 5), 1))
					{
						if (!isRemote)
						{
							trackBase.setStateOfShape(ERailwayState.TRACKBED);
						}
						return true;
					}
				}
			}
		}
		else if (currentItem.getItem() instanceof IToolCrowbar)
		{
			int type = trackBase.getField(0, 0, Integer.class);
			if (!isRemote)
			{
				trackBase.setField(0, (type != 0 ? 0 : 2));
			}
			return true;
		}
		return false;
	}
}
