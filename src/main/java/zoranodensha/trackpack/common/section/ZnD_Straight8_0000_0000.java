package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.ARailwaySectionRenderer;
import zoranodensha.api.structures.tracks.IRailwaySectionRenderer;
import zoranodensha.api.structures.tracks.Path;



public class ZnD_Straight8_0000_0000 extends RailwaySectionTrackPack
{
	public ZnD_Straight8_0000_0000()
	{
		super(8, 8, 8, 8, "ZnD_Straight8_0000_0000", 8, new Path[] {
				new Path(new String[] { "0.5", "0" }, null)
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_Straight8_0000_0000(IRailwaySectionRenderer render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.STRAIGHT;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.NONE)
		{
			for (int i = 0; i < 8; ++i)
			{
				list.add(new int[] { i, 0, 0 });
			}
		}
		return list;
	}
}
