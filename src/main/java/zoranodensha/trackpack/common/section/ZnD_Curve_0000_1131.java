package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.ARailwaySectionRenderer;
import zoranodensha.api.structures.tracks.IRailwaySectionRenderer;
import zoranodensha.api.structures.tracks.Path;



public class ZnD_Curve_0000_1131 extends RailwaySectionTrackPack
{
	public ZnD_Curve_0000_1131()
	{
		super(10, 10, 10, 0, "ZnD_Curve_0000_1131", 10, new Path[] {
				new Path(new String[] { "(0.0001 * x^3) + (0.009 * x^2) + 0.5", "(0.0003 * x^2) + (0.02214 * x)" }, null)
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_Curve_0000_1131(IRailwaySectionRenderer render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.CURVE;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.LEFT)
		{
			for (int i = 0; i < length; ++i)
			{
				if (i > 3)
				{
					for (int j = 0; j < 2; ++j)
					{
						list.add(new int[] { i, 0, -j });
					}
				}
				else
				{
					list.add(new int[] { i, 0, 0 });
				}
			}
		}
		else if (dir == EDirection.RIGHT)
		{
			for (int i = length - 1; i >= 0; --i)
			{
				if (i > 3)
				{
					for (int j = 0; j < 2; ++j)
					{
						list.add(new int[] { -i, 0, -j });
					}
				}
				else
				{
					list.add(new int[] { -i, 0, 0 });
				}
			}
		}
		return list;
	}
}
