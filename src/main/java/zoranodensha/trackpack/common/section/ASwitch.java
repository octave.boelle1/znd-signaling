package zoranodensha.trackpack.common.section;

import mods.railcraft.api.core.items.IToolCrowbar;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import zoranodensha.api.structures.signals.APulse;
import zoranodensha.api.structures.signals.IPulseEmitter;
import zoranodensha.api.structures.signals.SwitchPulse;
import zoranodensha.api.structures.tracks.*;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;

import java.util.List;
import java.util.Map;

public abstract class ASwitch extends RailwaySectionTrackPack {


    public ASwitch(int trackbeds, int fastenings, int rails, int plates, String name, int length, Path[] paths) {
        super(trackbeds, fastenings, rails, plates, name, length, paths);
    }

    @Override
    public EShape getEShapeType()
    {
        return EShape.SWITCH;
    }

    @Override
    public TileEntity initializeTrack(ITrackBase trackBase)
    {
        trackBase.setField(0, 2); // Texture name
        trackBase.setField(1, 0); // Damage
        trackBase.setField(2, 0.0F); // Rotation of switch blades
        trackBase.setField(3, false); // Manual path override
        trackBase.setField(4, false); // Automatic switch (hide lever)
        trackBase.setField(5, -1); // Signal-controlled Path Override
        return (TileEntity)trackBase;

    }

    @Override
    public void onReadFromNBT(NBTTagCompound nbt, ITrackBase trackBase)
    {
        for (int i = 0; i < 6; ++i)
        {
            String s = nbt.getString("FieldDec_" + i);
            if ("int".equals(s))
            {
                trackBase.setField(i, nbt.getInteger("Field_" + i));
            }
            else if ("float".equals(s))
            {
                trackBase.setField(i, nbt.getFloat("Field_" + i));
            }
            else if ("boolean".contentEquals(s))
            {
                trackBase.setField(i, nbt.getBoolean("Field_" + i));
            }
            else
            {
                switch (i)
                {
                    case 0:
                        trackBase.setField(i, 2);
                        break;
                    case 1:
                        trackBase.setField(i, 0);
                        break;
                    case 2:
                        trackBase.setField(i, 0.0F);
                        break;
                    case 3:
                    case 4:
                        trackBase.setField(i, false);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    @Override
    public boolean onTrackWorkedOn(ITrackBase trackBase, EntityPlayer player, boolean isRemote)
    {
        if (super.onTrackWorkedOn(trackBase, player, isRemote))
        {
            return true;
        }

        /* Toggle switch lever visibility. */
        ItemStack currentItem = player.getHeldItem();
        if (currentItem != null)
        {
            if (currentItem.getItem() instanceof IToolCrowbar)
            {
                if (!isRemote)
                {
                    trackBase.setField(4, !trackBase.getField(4, false, Boolean.class));
                }
                return true;
            }
        }

        /* Manual switch lever change (only if lever not hidden). */
        if (!trackBase.getField(4, false, Boolean.class))
        {
            if (!isRemote)
            {
                /* Switch path inversion flag upon manual interaction. */
                boolean isPathInverse = trackBase.getField(3, false, Boolean.class);
                trackBase.setField(3, !isPathInverse);
            }



            return true;
        }

        return false;
    }

    @Override
    public Map<Integer, Object> onUpdate(World world, int x, int y, int z, ITrackBase trackBase, Map<Integer, Object> params)
    {
        /* Update active path. */
        boolean isRedstonePowered = trackBase.getField(4, false, Boolean.class);
        boolean isDivergingPath = isRedstonePowered && world.isBlockIndirectlyGettingPowered(x, y, z);

        if (trackBase.getField(3, false, Boolean.class))
        {
            // Override diverging path after manual path change.
            isDivergingPath = !isDivergingPath;
        }

        int targetPath = (isDivergingPath) ? 1 : 0;
        int signalOverridePath = trackBase.getField(5, -1, Integer.class);
        if (signalOverridePath >= 0)
        {
            targetPath = signalOverridePath;
        }
        if (trackBase.getPath() != targetPath)
        {
            trackBase.setPath(targetPath);
            ((TileEntityTrackBase) trackBase).clear();
        }

        /* Update switch blade rotation. */
        float bladeRotation = (Float)params.get(2);
        if (trackBase.getPath() != 0)
        {
            if (bladeRotation < 2.1F)
            {
                bladeRotation = MathHelper.clamp_float(bladeRotation + 0.05F, 0.0F, 2.1F);
            }
        }
        else
        {
            if (bladeRotation > 0.0F)
            {
                bladeRotation = MathHelper.clamp_float(bladeRotation - 0.05F, 0.0F, 2.1F);
            }
        }

        if ((Float)params.get(2) != bladeRotation)
        {
            // Clear model cache on update.
            params.put(2, bladeRotation);
            params.put(10, null);
        }

        return super.onUpdate(world, x, y, z, trackBase, params);
    }

    @Override
    public void onWriteToNBT(NBTTagCompound nbt, ITrackBase trackBase)
    {

        for (int i = 0; i < 6; ++i)
        {
            Object obj = trackBase.getFieldUnchecked(i);
            if (obj instanceof Integer)
            {
                nbt.setString("FieldDec_" + i, "int");
                nbt.setInteger("Field_" + i, (Integer)obj);
            }
            else if (obj instanceof Float)
            {
                nbt.setString("FieldDec_" + i, "float");
                nbt.setFloat("Field_" + i, (Float)obj);
            }
            else if (obj instanceof Boolean)
            {
                nbt.setString("FieldDec_" + i, "boolean");
                nbt.setBoolean("Field_" + i, (Boolean)obj);
            }
        }
    }
}
