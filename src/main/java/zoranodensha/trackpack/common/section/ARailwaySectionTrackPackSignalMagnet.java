package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import zoranodensha.api.structures.signals.*;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.structures.tracks.Path;
import zoranodensha.api.vehicles.Train;
import zoranodensha.common.blocks.tileEntity.TileEntitySignal;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;



/**
 * <p>
 * Any tracks that have the capability of having a signal magnet attached to them should extend from this class.
 * </p>
 * 
 * @author Jaffa
 */
public abstract class ARailwaySectionTrackPackSignalMagnet extends RailwaySectionTrackPack implements ISignalMagnet
{


	private boolean isNotified;
	private Vec3 notifyDirection;

	/*
	 * Track Base Field Indexes
	 */
	/**
	 * This field should be {@code true} when there is any part of a train directly
	 * above this track section.
	 */
	public static final int INDEX_DETECTTRAIN = 6;
	/**
	 * This field is {@code true} when there is a magnet track part installed.
	 */
	public static final int INDEX_HASMAGNET = 2;
	public static final int INDEX_SIGNALX = 3;
	public static final int INDEX_SIGNALY = 4;
	public static final int INDEX_SIGNALZ = 5;
	/**
	 * This field contains the assigned track speed in km/h. {@code 0} if there is no track speed.
	 */
	public static final int INDEX_TRACKSPEED = 7;
	/**
	 * This field should be {@code true} if automatic trains should stop at this magnet.
	 */
	public static final int INDEX_IS_STATION = 8;
	/**
	 * This field should be {@code true} if automatic trains should stop and change direction at this magnet.
	 */
	public static final int INDEX_IS_TERMINATION = 9;



	/**
	 * <p>
	 * Initialises a enw instance of the {@link ARailwaySectionTrackPackSignalMagnet} class.
	 * </p>
	 * 
	 * @see {@link RailwaySectionTrackPack#RailwaySectionTrackPack(int, int, int, int, String, int, Path[])}
	 */
	public ARailwaySectionTrackPackSignalMagnet(int trackbeds, int fastenings, int rails, int plates, String name, int length, Path[] paths)
	{
		super(trackbeds, fastenings, rails, plates, name, length, paths);
	}


	@Override
	public boolean getIsStation(ITrackBase trackBase)
	{
		/*
		 * Ensure the supplied track base exists.
		 */
		if (!(trackBase instanceof TileEntityTrackBase))
		{
			return false;
		}

		return trackBase.getField(INDEX_IS_STATION, false, Boolean.class);
	}


	@Override
	public boolean getIsTermination(ITrackBase trackBase)
	{
		/*
		 * Ensure the supplied track base exists.
		 */
		if (!(trackBase instanceof TileEntityTrackBase))
		{
			return false;
		}

		return trackBase.getField(INDEX_IS_TERMINATION, false, Boolean.class);
	}


	@Override
	public ISignal getLinkedSignal(ITrackBase trackBase)
	{
		if (!(trackBase instanceof TileEntityTrackBase))
		{
			return null;
		}

		TileEntityTrackBase tile = (TileEntityTrackBase)trackBase;
		if (tile.getWorldObj().isRemote)
		{
			return null;
		}

		int sigX = trackBase.getField(INDEX_SIGNALX, -1, Integer.class);
		int sigY = trackBase.getField(INDEX_SIGNALY, -1, Integer.class);
		int sigZ = trackBase.getField(INDEX_SIGNALZ, -1, Integer.class);

		if (sigY >= 0)
		{
			TileEntity tileEntity = tile.getWorldObj().getTileEntity(sigX, sigY, sigZ);
			if (tileEntity instanceof TileEntitySignal)
			{
				return ((TileEntitySignal)tileEntity).getSignal();
			}
		}

		return null;
	}


	@Override
	public int getTrackSpeed(ITrackBase trackBase)
	{
		return trackBase.getField(INDEX_TRACKSPEED, 0, Integer.class);
	}


	@Override
	public TileEntity initializeTrack(ITrackBase trackBase)
	{
		super.initializeTrack(trackBase);
		trackBase.setField(INDEX_HASMAGNET, false);
		trackBase.setField(INDEX_SIGNALX, 0);
		trackBase.setField(INDEX_SIGNALY, 0);
		trackBase.setField(INDEX_SIGNALZ, 0);
		trackBase.setField(INDEX_DETECTTRAIN, false);
		trackBase.setField(INDEX_TRACKSPEED, 0);
		trackBase.setField(INDEX_IS_STATION, false);
		trackBase.setField(INDEX_IS_TERMINATION, false);
		return (TileEntity)trackBase;
	}


	@Override
	public void onReadFromNBT(NBTTagCompound nbt, ITrackBase trackBase)
	{
		super.onReadFromNBT(nbt, trackBase);

		for (int i = 0; i < 10; ++i)
		{
			String s = nbt.getString("FieldDec_" + i);
			if ("int".equals(s))
			{
				trackBase.setField(i, nbt.getInteger("Field_" + i));
			}
			else if ("boolean".equals(s))
			{
				trackBase.setField(i, nbt.getBoolean("Field_" + i));
			}
		}
	}


	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, Object> onUpdate(World world, int x, int y, int z, ITrackBase trackBase, Map<Integer, Object> params)
	{
		super.onUpdate(world, x, y, z, trackBase, params);

		if (world.isRemote)
		{
			return params;
		}

		Object oldDetectValue = params.get(INDEX_DETECTTRAIN);

		/*
		 * Reset the train detection flag.
		 */
		params.put(INDEX_DETECTTRAIN, false);
		params.put(10, null);

		/*
		 * Determine if the track has a magnet.
		 */
		Object hasMagnetObj = params.get(INDEX_HASMAGNET);
		boolean hasMagnet = false;

		if (hasMagnetObj instanceof Boolean)
		{
			hasMagnet = (Boolean)hasMagnetObj;
		}

		if (hasMagnet)
		{
			/*
			 * There is a magnet on this track section. Go ahead and check for a train.
			 */
			List<Object> entities = world.getEntitiesWithinAABB(Train.class, AxisAlignedBB.getBoundingBox(x, y, z, x + 1.0D, y + 1.0D, z + 1.5D));
			if (!entities.isEmpty())
			{
				params.put(INDEX_DETECTTRAIN, true);
			}
		}

		if (oldDetectValue != params.get(INDEX_DETECTTRAIN))
		{
			/*
			 * Block should be updated.
			 */
			Block block = world.getBlock(x, y, z);
			world.notifyBlocksOfNeighborChange(x, y, z, block);
            world.notifyBlocksOfNeighborChange(x - 1, y, z, block);
            world.notifyBlocksOfNeighborChange(x + 1, y, z, block);
            world.notifyBlocksOfNeighborChange(x, y, z - 1, block);
            world.notifyBlocksOfNeighborChange(x, y, z + 1, block);
            world.notifyBlocksOfNeighborChange(x, y - 1, z, block);
            world.notifyBlocksOfNeighborChange(x, y + 1, z, block);
		}

		return params;
	}


	@Override
	public void onWriteToNBT(NBTTagCompound nbt, ITrackBase trackBase)
	{
		super.onWriteToNBT(nbt, trackBase);

		nbt.setString("FieldDec_" + INDEX_HASMAGNET, "boolean");
		nbt.setString("FieldDec_" + INDEX_SIGNALX, "int");
		nbt.setString("FieldDec_" + INDEX_SIGNALY, "int");
		nbt.setString("FieldDec_" + INDEX_SIGNALZ, "int");
		nbt.setString("FieldDec_" + INDEX_DETECTTRAIN, "boolean");
		nbt.setString("FieldDec_" + INDEX_TRACKSPEED, "int");
		nbt.setString("FieldDec_" + INDEX_IS_STATION, "boolean");
		nbt.setString("FieldDec_" + INDEX_IS_TERMINATION, "boolean");

		nbt.setBoolean("Field_" + INDEX_HASMAGNET, trackBase.getField(INDEX_HASMAGNET, false, Boolean.class));
		nbt.setInteger("Field_" + INDEX_SIGNALX, trackBase.getField(INDEX_SIGNALX, 0, Integer.class));
		nbt.setInteger("Field_" + INDEX_SIGNALY, trackBase.getField(INDEX_SIGNALY, 0, Integer.class));
		nbt.setInteger("Field_" + INDEX_SIGNALZ, trackBase.getField(INDEX_SIGNALZ, 0, Integer.class));
		nbt.setBoolean("Field_" + INDEX_DETECTTRAIN, trackBase.getField(INDEX_DETECTTRAIN, false, Boolean.class));
		nbt.setInteger("Field_" + INDEX_TRACKSPEED, trackBase.getField(INDEX_TRACKSPEED, 0, Integer.class));
		nbt.setBoolean("Field_" + INDEX_IS_STATION, trackBase.getField(INDEX_IS_STATION, false, Boolean.class));
		nbt.setBoolean("Field_" + INDEX_IS_TERMINATION, trackBase.getField(INDEX_IS_TERMINATION, false, Boolean.class));
	}


	@Override
	public void setIsStation(ITrackBase trackBase, boolean isStation)
	{
		trackBase.setField(INDEX_IS_STATION, isStation);
	}


	@Override
	public void setIsTermination(ITrackBase trackBase, boolean isTermination)
	{
		trackBase.setField(INDEX_IS_TERMINATION, isTermination);
	}


	@Override
	public void setLinkedSignal(ISignal signal, ITrackBase trackBase)
	{
		if (signal != null)
		{
			if (signal.getTileEntity() instanceof TileEntitySignal)
			{
				TileEntitySignal tileEntity = (TileEntitySignal)signal.getTileEntity();

				trackBase.setField(INDEX_SIGNALX, tileEntity.xCoord);
				trackBase.setField(INDEX_SIGNALY, tileEntity.yCoord);
				trackBase.setField(INDEX_SIGNALZ, tileEntity.zCoord);

				return;
			}
		}
	}

	@Override
	public void clearSignal(ITrackBase trackBase) {
		trackBase.setField(INDEX_SIGNALX, -1);
		trackBase.setField(INDEX_SIGNALY, -1);
		trackBase.setField(INDEX_SIGNALZ, -1);
	}


	@Override
	public void setTrackSpeed(int newSpeed, ITrackBase trackBase)
	{
		if (newSpeed >= 0)
		{
			trackBase.setField(INDEX_TRACKSPEED, newSpeed);
		}
		else
		{
			trackBase.setField(INDEX_TRACKSPEED, 0);
		}
	}
}
