package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.ARailwaySectionRenderer;
import zoranodensha.api.structures.tracks.IRailwaySectionRenderer;
import zoranodensha.api.structures.tracks.Path;


public class ZnD_Straight_0000_0000 extends ARailwaySectionTrackPackSignalMagnet
{

	/**
	 * Initialises a new instance of the {@link zoranodensha.trackpack.common.section.ZnD_Straight_0000_0000} class.
	 */
	public ZnD_Straight_0000_0000()
	{
		super(1, 1, 1, 1, "ZnD_Straight_0000_0000", 1, new Path[] {
				new Path(new String[] { "0.5", "0" }, null)
				});
	}


	@SideOnly(Side.CLIENT)
	public ZnD_Straight_0000_0000(IRailwaySectionRenderer render)
	{
		this();
		this.render = render;
	}


	@Override
	public EShape getEShapeType()
	{
		return EShape.STRAIGHT;
	}


	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.NONE)
		{
			list.add(new int[] { 0, 0, 0 });
		}
		return list;
	}


}
