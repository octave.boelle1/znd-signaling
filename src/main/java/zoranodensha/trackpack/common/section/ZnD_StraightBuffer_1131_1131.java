package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.ARailwaySectionRenderer;
import zoranodensha.api.structures.tracks.IRailwaySectionRenderer;
import zoranodensha.api.structures.tracks.Path;


public class ZnD_StraightBuffer_1131_1131 extends ABuffer
{
	public ZnD_StraightBuffer_1131_1131()
	{
		super(5, 5, 5, 4, "ZnD_StraightBuffer_1131_1131", 5, new Path[] {
				new Path(new String[] { "(0.2 * x) + 0.5", "0.2514" }, null)
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_StraightBuffer_1131_1131(IRailwaySectionRenderer render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.STRAIGHT;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.LEFT)
		{
			for (int i = 0; i < length; ++i)
			{
				for (int j = 0; j < 2; ++j)
				{
					if (i + 1 == length && j == 1)
					{
						list.add(new int[] { i, 0, -j, 1 });
					}
					else
					{
						list.add(new int[] { i, 0, -j, 0 });
					}
				}
			}
		}
		else if (dir == EDirection.RIGHT)
		{
			for (int i = 0; i < length; ++i)
			{
				for (int j = 0; j < 2; ++j)
				{
					if (i + 1 == length && j == 1)
					{
						list.add(new int[] { -i, 0, -j, 1 });
					}
					else
					{
						list.add(new int[] { -i, 0, -j, 0 });
					}
				}
			}
		}
		return list;
	}

}
