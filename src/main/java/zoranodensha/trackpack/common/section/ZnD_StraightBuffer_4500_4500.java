package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.ARailwaySectionRenderer;
import zoranodensha.api.structures.tracks.IRailwaySectionRenderer;
import zoranodensha.api.structures.tracks.Path;


public class ZnD_StraightBuffer_4500_4500 extends ABuffer
{
	public ZnD_StraightBuffer_4500_4500()
	{
		super(2, 2, 2, 1, "ZnD_StraightBuffer_4500_4500", 2, new Path[] {
				new Path(new String[] { "x", "1" }, null)
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_StraightBuffer_4500_4500(IRailwaySectionRenderer render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.STRAIGHT;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.NONE)
		{
			for (int i = 0; i < length; ++i)
			{	
				for (int j = (i - 1); j < (2 + i); ++j)
				{
					if (i + 1 == length && j == i)
					{
						list.add(new int[] { i, 0, -j, 1 });
					}
					else
					{
						list.add(new int[] { i, 0, -j, 0 });
					}
				}
			}
		}
		return list;
	}

}
