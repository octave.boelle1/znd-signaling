package zoranodensha.trackpack.common.section;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.ARailwaySectionRenderer;
import zoranodensha.api.structures.tracks.IRailwaySectionRenderer;
import zoranodensha.api.structures.tracks.Path;



public class ZnD_Curve_1131_1843 extends RailwaySectionTrackPack
{
	public ZnD_Curve_1131_1843()
	{
		super(4, 4, 4, 0, "ZnD_Curve_1131_1843", 4, new Path[] {
				new Path(new String[] { "(0.0125 * x^2) + (0.2 * x) + 0.5", "(0.03955 * x) + 0.2514" }, null)
				});
	}

	@SideOnly(Side.CLIENT)
	public ZnD_Curve_1131_1843(IRailwaySectionRenderer render)
	{
		this();
		this.render = render;
	}

	@Override
	public EShape getEShapeType()
	{
		return EShape.CURVE;
	}

	@Override
	public List<int[]> getGagBlocks(EDirection dir)
	{
		List<int[]> list = new ArrayList<int[]>();
		if (dir == EDirection.LEFT)
		{
			for (int i = 0; i < length; ++i)
			{
				for (int j = 0; j < 2; ++j)
				{
					list.add(new int[] { i, 0, -j });
				}
			}
		}
		else if (dir == EDirection.RIGHT)
		{
			for (int i = 0; i < length; ++i)
			{
				for (int j = 0; j < 2; ++j)
				{
					list.add(new int[] { -i, 0, -j });
				}
			}
		}
		return list;
	}
}
