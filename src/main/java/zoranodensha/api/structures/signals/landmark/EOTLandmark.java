package zoranodensha.api.structures.signals.landmark;

import net.minecraft.util.Vec3;
import zoranodensha.api.structures.signals.APulse;
import zoranodensha.api.structures.signals.ETrafficResult;
import zoranodensha.api.structures.signals.MovementVector;

public class EOTLandmark extends ALandmark {

    public EOTLandmark(APulse pulse, MovementVector movement, boolean isBumper) {
        super(pulse, movement, isBumper ? ELandmarkType.BUMPER : ELandmarkType.BROKEN_RAIL);
    }

    @Override
    public void appear() {}

    @Override
    public void dismiss() {}

    @Override
    public ETrafficResult getResult() {
        return ETrafficResult.STOP;
    }


}
