package zoranodensha.api.structures.signals.landmark;

import net.minecraft.util.Vec3;
import zoranodensha.api.structures.signals.APulse;
import zoranodensha.api.structures.signals.ETrafficResult;
import zoranodensha.api.structures.signals.MovementVector;

public class UndefinedLandmark extends ALandmark {

    public UndefinedLandmark(APulse pulse, MovementVector movement) {
        super(pulse, movement);
    }

    @Override
    public void appear() {

    }

    @Override
    public void dismiss() {

    }

    @Override
    public ETrafficResult getResult() {
        return ETrafficResult.CLEAR;
    }
}
