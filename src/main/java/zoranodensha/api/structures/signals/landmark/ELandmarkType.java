package zoranodensha.api.structures.signals.landmark;

public enum ELandmarkType {

    UNKNOWN,
    SPEED_LIMIT,
    TRAIN,
    SWITCH,
    BROKEN_RAIL,
    BUMPER,
    MAGNET,
}
