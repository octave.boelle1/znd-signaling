package zoranodensha.api.structures.signals.landmark;

import net.minecraft.util.Vec3;
import zoranodensha.api.structures.signals.APulse;
import zoranodensha.api.structures.signals.ETrafficResult;
import zoranodensha.api.structures.signals.MovementVector;

import javax.annotation.Nullable;

public abstract class ALandmark {

    private static final float BUFFER = 10.0F;

    protected APulse parentPulse;

    protected MovementVector movement;
    ELandmarkType type;

    private float distance;

    private float speedLimit = -1.0F;


    public ALandmark(APulse pulse, MovementVector movement) {
        this(pulse, movement, ELandmarkType.UNKNOWN);
    }

    public ALandmark(APulse pulse, MovementVector movement, ELandmarkType type) {
        this.parentPulse = pulse;
        this.movement = movement;
        this.type = type;
    }

    /**
     * <p>
     * Called when a Landmark is getting in sight .
     * </p>
     *
     * @return - A {@link Vec3} representing the position of this landmark.
     */
    public abstract void appear();

    /**
     * <p>
     * Called when a Landmark is getting out of sight .
     * </p>
     *
     * @return - A {@link Vec3} representing the position of this landmark.
     */
    public abstract void dismiss();

    /**
     * <p>
     * Returns the type of this landmark.
     * </p>
     *
     * @return - A {@link ELandmarkType} representing the type of this landmark.
     */
    public ELandmarkType getType() {
        return type;
    }

    public MovementVector getMovement() {
        return movement;
    }

    @Override
    public String toString() {
        return "Type: " + type + " at " + movement + " : result is " + getResult() + " speed limit is " + speedLimit;
    }

    public void notifyPulse(int blocks, float distance) {
        this.distance = distance;
    }

    public abstract ETrafficResult getResult();

    public float getDistance() {
        return distance;
    }

    public @Nullable APulse getPulse() {
        return null;
    }

    public String getID() {
        return parentPulse.getID().toString();
    }

    public @Nullable ALandmark next() {
        return null;
    }

    public float getSpeedLimit() {
        return speedLimit;
    }

    protected void setSpeedLimit(float speedLimit) {
        this.speedLimit = speedLimit;
    }

    /*public float[] getTargetSpeed(float deceleration, float offset)
    {
        /*
         * We need to go through every landmark in this landscape, figure out what the speed limit for that landmark currently is, and then determine the number of seconds until that speed limit will take effect.
         * The closest landmark where the speed limit will occur in less than 20 seconds..? should then be returned by this method. Otherwise, -1.
         */


        //ETrafficResult result = getResult();


        /* Ignore if the landmark has no speed limit. */
        /*if (result.getSpeed() < 0.0F)
        {
            return -1;
        }*

        float distanceToLandmark = (distance + offset) / ModCenter.cfg.trains.velocityScale;
        float landmarkBrakingCurve = (float)(Math.sqrt(2.0F * deceleration * Math.max(distanceToLandmark - BUFFER, 0.0F) + Math.pow(result.getSpeed() / 3.6F, 2.0D)));

        float deltaVelocity = landmarkBrakingCurve - (result.getSpeed() / 3.6F);
        float deltaVelocityTime = (deltaVelocity / deceleration);

        ALandmark nextLandmark = next();
        if (nextLandmark != null) {
            float[] nextLandmarkSpeed = nextLandmark.getTargetSpeed(deceleration, distance);
            if (nextLandmarkSpeed[0] > 0) {
                return nextLandmarkSpeed;
            }
        }
        /*if(deltaVelocityTime < 20.0F)
        {
            if(slowestLandmark == null || (landmark.getSpeedLimit() < slowestLandmarkSpeed))
            {
                slowestLandmark = landmark;
                slowestLandmarkSpeed = landmark.getSpeedLimit();
            }

            if(earliestLandmark == null || (deltaVelocityTime < earliestLandmarkTime))
            {
                earliestLandmark = landmark;
                earliestLandmarkTime = deltaVelocityTime;
            }
        }*

        return deltaVelocityTime;
    }*/
}
