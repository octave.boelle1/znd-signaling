package zoranodensha.api.structures.signals.landmark;

import net.minecraft.entity.Entity;
import net.minecraft.util.Vec3;
import zoranodensha.api.structures.signals.APulse;
import zoranodensha.api.structures.signals.ETrafficResult;
import zoranodensha.api.structures.signals.MovementVector;
import zoranodensha.api.vehicles.Train;

public class TrainLandmark extends ALandmark {
    public TrainLandmark(APulse pulse, MovementVector movement) {
        super(pulse, movement, ELandmarkType.TRAIN);
    }

    @Override
    public void appear() {

    }

    @Override
    public void dismiss() {

    }

    @Override
    public ETrafficResult getResult() {
        return ETrafficResult.STOP;
    }


}
