package zoranodensha.api.structures.signals.landmark;

import org.lwjgl.openal.AL;
import zoranodensha.api.structures.signals.*;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;
import zoranodensha.signals.common.ASignal;

import javax.annotation.Nullable;

public class MagnetLandmark extends ALandmark {

    public final ISignalMagnet magnet;
    public final TileEntityTrackBase track;

    public MagnetLandmark(APulse pulse, ISignalMagnet magnet, TileEntityTrackBase track, MovementVector movement) {
        super(pulse, movement, ELandmarkType.MAGNET);
        this.magnet = magnet;
        this.track = track;
    }

    @Override
    public void appear() {
        track.addPulse(new TrackPulse(track, parentPulse.getID()));
    }

    @Override
    public void dismiss() {
        track.getPulse(parentPulse.getID()).clear();
        track.removePulse(parentPulse.getID());

        if (magnet.getLinkedSignal(track) != null) {
            ISignal signal = magnet.getLinkedSignal(track);
            signal.setLandmark(null);
        }
    }

    @Override
    public void notifyPulse(int blocks, float distance) {
        if (magnet.getLinkedSignal(track) != null) {
            ISignal signal = magnet.getLinkedSignal(track);
            signal.setLandmark(this);
        }


        if (blocks > 0)
            track.notifyPulse(new PulseNotification(parentPulse.getID(), movement, blocks - 1));

        super.notifyPulse(blocks, distance);
    }

    @Override
    public ETrafficResult getResult() {
        ALandmark nextLandmark = next();

        if (nextLandmark == null) {
            return ETrafficResult.STOP;
        }

        ETrafficResult result = nextLandmark.getResult();
        switch (nextLandmark.getResult()) {
            case HIGH_SPEED:
            case CLEAR:
                result = ETrafficResult.HIGH_SPEED;
                break;
            case CAUTION:
            case EXPECT_SLOW:
                result = ETrafficResult.CLEAR;
                break;
            case STOP:
                result = ETrafficResult.CAUTION;
                break;
            /*case SPEED_LIMIT:
                result = ETrafficResult.EXPECT_SLOW;*/
            default:
                result = ETrafficResult.STOP;
                break;
        }

        if (magnet.getTrackSpeed(track) != 0) {
            this.setSpeedLimit(magnet.getTrackSpeed(track));
        }

        return result;
    }

    @Override
    public @Nullable APulse getPulse() {
        return track.getPulse(parentPulse.getID());
    }

    @Override
    public @Nullable ALandmark next() {
        APulse pulse = getPulse();
        if (pulse != null) {
            return pulse.landmark;
        }
        return null;
    }
}
