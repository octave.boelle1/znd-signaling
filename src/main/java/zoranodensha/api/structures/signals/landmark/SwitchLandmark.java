package zoranodensha.api.structures.signals.landmark;

import zoranodensha.api.structures.signals.*;
import zoranodensha.trackpack.common.section.ASwitch;

import javax.annotation.Nullable;

public class SwitchLandmark extends ALandmark {

    private final IPulseEmitter track;
    //private final ASwitch switchTrack;

    public SwitchLandmark(APulse pulse , IPulseEmitter track, MovementVector movement) {
        super(pulse, movement, ELandmarkType.SWITCH);
        this.track = track;
        //this.switchTrack = switchTrack;

        this.setSpeedLimit(40.0F);
    }

    @Override
    public void appear() {
        track.addPulse(new TrackPulse(track, parentPulse.getID()));
    }

    @Override
    public void dismiss() {
        track.getPulse(parentPulse.getID()).clear();
        track.removePulse(parentPulse.getID());
    }

    @Override
    public void notifyPulse(int blocks, float distance) {
        if (blocks > 0)
            track.notifyPulse(new PulseNotification(parentPulse.getID(), movement, blocks - 1));

        super.notifyPulse(blocks, distance);

    }

    @Override
    public ETrafficResult getResult() {
        ALandmark nextLandmark = next();

        if (nextLandmark == null) {
            return ETrafficResult.STOP;
        }

        switch (track.getPulse(parentPulse.getID()).landmark.getResult()) {
            case HIGH_SPEED:
            case CLEAR:
                return ETrafficResult.HIGH_SPEED;
            case CAUTION:
            case EXPECT_SLOW:
                return ETrafficResult.CLEAR;
            case STOP:
                return ETrafficResult.CAUTION;
            /*case SPEED_LIMIT:
                return ETrafficResult.EXPECT_SLOW;*/
            default:
                return ETrafficResult.STOP;
        }
    }


    @Override
    public @Nullable APulse getPulse() {
        return track.getPulse(parentPulse.getID());
    }

    @Override
    public @Nullable ALandmark next() {
        APulse pulse = getPulse();
        if (pulse != null) {
            return pulse.landmark;
        }
        return null;
    }
}
