package zoranodensha.api.structures.signals;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.client.IItemRenderer;



/**
 * <h1>ISpeedSign</h1>
 * <p>
 * This is an interface implemented by Zora no Densha speed signs.
 * </p>
 * 
 * @author Jaffa
 */
public interface ISpeedSign extends ISignalEquipment
{
	/**
	 * @return - The meta data of the parent tile entity.
	 */
	int getMetaData();

	/**
	 * @return - The unique, unlocalised name of this speed sign instance.<br>
	 *         For example, '{@code zoranodensha.speedSign.de.lf7}'.
	 */
	String getName();

	/**
	 * <p>
	 * Gets the speed limit this sign enforces.
	 * </p>
	 * 
	 * @return - A speed limit, rounded to the nearest {@code int}, in {@code km/h}.
	 */
	int getSpeedLimit();

	/**
	 * <p>
	 * Gets the parent tile entity of this instance.
	 * </p>
	 * 
	 * @return - A {@link net.minecraft.tileentity.TileEntity} instance.
	 */
	TileEntity getTileEntity();

	/**
	 * @return - The X position of this speed sign's block in the world.
	 */
	int getX();

	/**
	 * @return - The Y position of this speed sign's block in the world.
	 */
	int getY();

	/**
	 * @return - The Z position of this speed sign's block in the world.
	 */
	int getZ();

	/**
	 * <p>
	 * Flags this instance for a block update. This should be called after important attributes have changed, such as the sign's displayed speed limit.
	 * </p>
	 */
	void markForUpdate();

	/**
	 * <p>
	 * Called when this speed sign instance receives a new parent tile entity.<br>
	 * In this method, the speed sign should store the provided tile entity as a reference so it may be referred to later.
	 * </p>
	 * 
	 * @param tile - The new parent tile entity.
	 */
	void onTileEntityChange(TileEntity tile);

	/**
	 * <p>
	 * Called on client & server update ticks.
	 * </p>
	 */
	void onUpdate();

	/**
	 * <p>
	 * Called to read the contents of the specified {@link NBTTagCompound} and reload existing saved data.
	 * </p>
	 * 
	 * @param nbt - The {@link NBTTagCompound} to read from.
	 */
	void readFromNBT(NBTTagCompound nbt);

	/**
	 * <p>
	 * Called to register any crafting recipes this speed sign should need.
	 * </p>
	 */
	void registerRecipes();

	/**
	 * <p>
	 * Called on client render ticks.
	 * </p>
	 * 
	 * @param partialTick - The current {@code partialTick} value, used for smoothing out animations.
	 */
	@SideOnly(Side.CLIENT)
	void render(float partialTick);

	/**
	 * <p>
	 * Called to render this isntance as an item, according to the specified {@code type} parameter.
	 * </p>
	 * 
	 * @param type - The situation the item should be rendered in, such as in an inventory or dropped in the game world.
	 */
	@SideOnly(Side.CLIENT)
	void renderItem(IItemRenderer.ItemRenderType type);

	/**
	 * <p>
	 * Called to assign a new fixed speed limit to this sign.
	 * </p>
	 * 
	 * @param newSpeedLimit - The new speed limit, rounded to the nearest {@code int}, in {@code km/h}.
	 */
	void setSpeedLimit(int newSpeedLimit);

	/**
	 * <p>
	 * Called to save the state of this speed sign instance to the specified NBT Tag Compound.
	 * </p>
	 * 
	 * @param nbt - The {@link NBTTagCompound} instance to save to.
	 */
	void writeToNBT(NBTTagCompound nbt);
}
