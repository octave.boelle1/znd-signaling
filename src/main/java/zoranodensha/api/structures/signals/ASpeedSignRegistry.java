package zoranodensha.api.structures.signals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import zoranodensha.api.util.APILogger;



/**
 * This is a registry which holds all registered types of speed signs.
 * You must register your speed sign class with this registry for it to work.
 * 
 * @author Jaffa
 */
public class ASpeedSignRegistry
{
	/**
	 * The static map instance containing all registered speed signs.
	 */
	protected static Map<String, ISpeedSign> speedSignMap = new HashMap<String, ISpeedSign>();



	/**
	 * @return - A list of the names of all registered speed signs.
	 */
	public static final ArrayList<String> getRegisteredSpeedSignNames()
	{
		ArrayList<String> speedSignNames = new ArrayList<String>();

		for (ISpeedSign speedSign : speedSignMap.values())
		{
			speedSignNames.add(speedSign.getName());
		}

		return speedSignNames;
	}


	/**
	 * <p>
	 * Returns a newly-created instance of
	 * {@link zoranodensha.api.structures.signals.ISpeedSign} based on the name
	 * provided.
	 * </p>
	 * 
	 * @param name - The name of an already-registered speed sign to create a new
	 *            instance of.
	 * @return - The newly-created instance of the speed sign, or if no such speed
	 *         sign type exists under the specified name, {@code null}.
	 */
	public static final ISpeedSign getSpeedSignFromName(String name)
	{
		try
		{
			return speedSignMap.get(name).getClass().newInstance();
		}
		catch (NullPointerException npe)
		{
			APILogger.catching(npe);
		}
		catch (InstantiationException ex)
		{
			/* Silent catch. */
		}
		catch (IllegalAccessException ex)
		{
			/* Silent catch. */
		}

		return null;
	}


	/**
	 * Called to register the crafting recipes of all the registered speed signs.
	 */
	public static void registerRecipes()
	{
		for (Object speedSign : speedSignMap.values().toArray())
		{
			if (speedSign instanceof ISpeedSign)
			{
				((ISpeedSign)speedSign).registerRecipes();
			}
		}
	}


	/**
	 * <p>
	 * Registers an {@link zoranodensha.api.structures.signals.ISpeedSign} instance.
	 * </p>
	 * 
	 * @param speedSign - The speed sign to register.
	 * @return - A flag indicating whether the operation succeeded.
	 * @throws - {@link IllegalArgumentException}
	 */
	public static boolean registerSpeedSign(ISpeedSign speedSign)
	{
		if (speedSign != null)
		{
			String name = speedSign.getName();

			if (name == null || name.isEmpty())
			{
				throw new IllegalArgumentException("[SpeedSignRegistry] A mod tried to register an ISpeedSign with null or empty name.");
			}

			speedSignMap.put(name, speedSign);

			return true;
		}

		return false;
	}
}
