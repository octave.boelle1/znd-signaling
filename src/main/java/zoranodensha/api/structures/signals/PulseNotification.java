package zoranodensha.api.structures.signals;

import net.minecraft.util.Vec3;

import java.util.UUID;

public class PulseNotification {

    private final UUID pulseID;
    private final MovementVector movement;
    private final int blocks;

    public PulseNotification(UUID pulseID, MovementVector movement, int blocks) {
        this.pulseID = pulseID;

        this.movement = movement;
        this.blocks = blocks;
    }

    public int getBlocks() {
        return blocks;
    }

    public UUID getPulseID() {
        return pulseID;
    }

    public MovementVector getMovement() {
        return movement;
    }

    @Override
    public String toString() {
        return "PulseNotification{" +
                "pulseID=" + pulseID +
                ", movement=" + movement +
                ", blocks=" + blocks +
                '}';
    }
}
