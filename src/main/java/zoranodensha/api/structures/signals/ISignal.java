package zoranodensha.api.structures.signals;

import java.util.ArrayList;
import java.util.HashMap;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.structures.signals.landmark.ALandmark;
import zoranodensha.api.util.Vec2;
import zoranodensha.api.util.Vec4i;
import zoranodensha.common.blocks.tileEntity.TileEntitySignal;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;


/**
 * <h1>ISignal</h1>
 * <p>
 * This is an interface implemented by Zora no Densha signals.
 * </p>
 * 
 * @author Jaffa
 */
public interface ISignal extends ISignalEquipment
{
	/**
	 * <p>
	 * Forces this signal to display its most restrictive possible indication.
	 * </p>
	 */
	void danger();


	/**
	 * <p>
	 * Gets a hash table representing any additional properties this signal may posess (for example, whether extra speed displays should be rendered).
	 * </p>
	 * 
	 * @return - A {@link java.util.HashMap} instance, with {@code String} keys and
	 *         {@link zoranodensha.api.structures.signals.SignalProperty} values.
	 */
	HashMap<String, SignalProperty> getAdditionalProperties();


	/**
	 * <p>
	 * Gets the length of this signal's signal block (the distance between this signal and the next signal), in metres.
	 * </p>
	 * 
	 * @return - A {@code float} indicating the block length, in metres (m).
	 */
	float getBlockLength();


	/**
	 * <p>
	 * Gets a list of floats which make up a list of coordinates used for debugging the path that this signal checks each tick.
	 * </p>
	 * 
	 * @return - A list of floats which make up Vector3 coordinates, e.g. X, Y, Z, X, Y, Z, etc
	 */
	ArrayList<Double> getDebugPath();


	/**
	 * <p>
	 * This method must return a {@link Vec2} containing the width and height of this signal's hitbox in the game world.
	 * </p>
	 * 
	 * @return - A {@code Vec2}, with the X value containing the width of this signal's hitbox and the Y value being the height of this signal's hitbox.
	 */
	Vec2 getDimensions();


	/**
	 * <p>
	 * Gets the ID of this signal, which is a unique customisable identifier which can be referred to by signal boxes, etc.
	 * </p>
	 * 
	 * @return - A {@code String}.
	 */
	String getID();


	/**
	 * <p>
	 * Gets the current indication displayed by the signal.
	 * </p>
	 * 
	 * @return - A {@link SignalIndication} instance.
	 */
	SignalIndication getIndication();

	/**
	 * <p>
	 * Gets the current list of track sections that are interlocked by this signal. If there are no currently interlocked track sections under this signal's control, this method will return an empty
	 * list.
	 * </p>
	 */
	ArrayList<Vec4i> getInterlockedTracks();


	/**
	 * @return - The unique, unlocalised name of this signal instance.<br>
	 *         For example, '{@code zoranodensha.signal.de.ks}'.
	 */
	String getName();


	/**
	 * <p>
	 * Gets the tile entity currently bound to this instance.
	 * </p>
	 * 
	 * @return - A {@link net.minecraft.tileentity.TileEntity} instance.
	 */
	TileEntity getTileEntity();


	/**
	 * @return - The X position of this signal's block in the world.
	 */
	int getX();


	/**
	 * @return - The Y position of this signal's block in the world.
	 */
	int getY();


	/**
	 * @return - The Z position of this signal's block in the world.
	 */
	int getZ();


	/**
	 * @return - {@code true} if other signals should NOT show a proceed indication if their path would cross this signal's location in the opposite direction.
	 */
	boolean isOneWay();

	/**
	 * @return - {@code true} if this signal is a repeater and does not mark the division of a block section.
	 */
	boolean isRepeater();


	/**
	 * <p>
	 * Flags this instance for a block update. This should be called after important attributes have changed, such as rotation.
	 * </p>
	 */
	void markForUpdate();


	/**
	 * <p>
	 * Called when this signal instance receives a new parent tile entity.
	 * </p>
	 * <p>
	 * In this method, the signal should store the provided tile entity argument as a reference so it may refer to it later.
	 * </p>
	 * 
	 * @param tileEntitySignal - The newly-assigned tile entity instance that is the
	 *            parent of this signal.
	 */
	void onTileEntityChange(TileEntitySignal tileEntitySignal);


	/**
	 * <p>
	 * Called to perform an update tick on this instance.
	 * </p>
	 * <p>
	 * This method is called on every tick, both on clients and servers.
	 * </p>
	 */
	void onUpdate();


	/**
	 * <p>
	 * This method is called on a timed interval (usually every few seconds) to update the signal's indication.
	 * </p>
	 * <p>
	 * In this method, various things should happen, such as:
	 * <ul>
	 * <li>Check the path ahead for trains/obstructions,</li>
	 * <li>Update the signal indication/aspect, and</li>
	 * <li>Send messages to approaching trains, if the signal is designed to support it.</li>
	 * </ul>
	 * </p>
	 */
	void pathTick();


	/**
	 * <p>
	 * Called to read the contents of the specified {@link NBTTagCompound} and
	 * reload existing saved data.
	 * </p>
	 * 
	 * @param nbt - The {@link NBTTagCompound} to read from.
	 */
	void readFromNBT(NBTTagCompound nbt);


	/**
	 * <p>
	 * Called to register any crafting recipes regarding this signal type.
	 * </p>
	 * 
	 * @see GameRegistry#addRecipe(net.minecraft.item.crafting.IRecipe)
	 */
	void registerRecipes();


	/**
	 * <p>
	 * Called to render the signal in the game world.
	 * </p>
	 * 
	 * @param partialTick - The current {@code partialTick} value, used for
	 *            animations.
	 */
	@SideOnly(Side.CLIENT)
	void render(float partialTick, boolean isInventory);


	/**
	 * <p>
	 * Called to render this instance as an inventory item.
	 * </p>
	 * 
	 * @param type - The render type enum, specifying the circumstances where this
	 *            instance will be rendered (for example, in an inventory slot,
	 *            dropped in the game world, etc.).
	 */
	@SideOnly(Side.CLIENT)
	void renderItem(IItemRenderer.ItemRenderType type);


	/**
	 * <p>
	 * Call this method to update the target magnet location. These are coordinates in the game world where this signal will look for a magnet and communicate with.
	 * </p>
	 * 
	 * @param x - The X coordinate of the magnet.
	 * @param y - The Y coordinate of the magnet.
	 * @param z - The Z coordinate of the magnet.
	 */
	void setTargetMagnet(int x, int y, int z);


	/**
	 * <p>
	 * Called to save the state of this signal instance to the specified NBT Tag Compound.
	 * </p>
	 * 
	 * @param nbt - The {@link net.minecraft.nbt.NBTTagCompound} instance to save to.
	 */
	void writeToNBT(NBTTagCompound nbt);

	ALandmark getLandmark();

	void setLandmark(ALandmark landmark);

	TileEntityTrackBase getMagnetTrack();
}
