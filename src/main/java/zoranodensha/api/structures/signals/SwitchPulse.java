package zoranodensha.api.structures.signals;

import net.minecraft.util.Vec3;

import java.util.UUID;

public class SwitchPulse extends APulse {

    public SwitchPulse(IPulseEmitter emitter, UUID pulseID) {
        super(emitter, pulseID);
    }

}
