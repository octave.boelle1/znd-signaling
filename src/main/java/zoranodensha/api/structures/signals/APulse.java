package zoranodensha.api.structures.signals;

import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import zoranodensha.api.structures.signals.landmark.*;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.handlers.CollisionHandler;
import zoranodensha.api.vehicles.util.PositionStackEntry;
import zoranodensha.api.vehicles.util.TrackObj;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBaseGag;
import zoranodensha.trackpack.common.section.ABuffer;
import zoranodensha.trackpack.common.section.ARailwaySectionTrackPackSignalMagnet;
import zoranodensha.trackpack.common.section.ASwitch;
import zoranodensha.trackpack.common.section.ZnD_Straight_0000_0000;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import static zoranodensha.api.vehicles.util.VehicleHelper.DEG_FACTOR;

public abstract class APulse {


    protected final int SIGHT_DISTANCE = 8;
    protected final int MAX_DISTANCE = SIGHT_DISTANCE * 32;
    protected final float STEP = 0.5F;

    protected final IPulseEmitter emitter;

    protected ArrayDeque<MovementVector> movementDeque = new ArrayDeque<>();
    protected ArrayDeque<MovementVector> oldMovementDeque = new ArrayDeque<>();

    final ArrayDeque<MovementVector> toRemoveQueue = new ArrayDeque<>();
    private final UUID pulseID;

    MovementVector lastVisitedMovement;

    ArrayDeque<TileEntityTrackBase> visitedTracks = new ArrayDeque<>();
    public ALandmark landmark;

    private MovementVector emitterMovement;

    protected CollisionHandler.EntitySelectorTrains selectorTrains = new CollisionHandler.EntitySelectorTrains(null);

    ITrackBase currentTrack;

    public APulse(IPulseEmitter emitter, UUID pulseID) {

        this.emitter = emitter;

        this.pulseID = pulseID;
    }


    public void tickPulse(MovementVector emitterMovement, int blocks) {
        this.emitterMovement = emitterMovement;

        if (lastVisitedMovement == null) {
            lastVisitedMovement = emitterMovement;
        }

        if (landmark == null || landmark.getMovement() == null) {
            landmark = new UndefinedLandmark(this, emitterMovement);
        }

        currentTrack = getTrackTileEntityBase(emitter.getWorld(), emitterMovement.getPosition());

        //long time = System.nanoTime();

        pathTrace(emitterMovement);

        //long pathTrace = System.nanoTime() - time;

        detectNextLandmark();

        //long detectNextLandmark = System.nanoTime() - time - pathTrace;

        clearOutOfSight();

       /* long end = System.nanoTime() - time - pathTrace - detectNextLandmark;

        if (detectNextLandmark > 1000000)
            System.out.println("Path Trace: " + pathTrace + " Detect Next Landmark: " + detectNextLandmark + " Clear Out Of Sight: " + end);*/

        float distance = 0.0F;
        for (Iterator<MovementVector> it = movementDeque.iterator(); it.hasNext() && landmark.getMovement() != it; it.next()) {
            distance++;
        }

        landmark.notifyPulse(blocks, distance * STEP);

    }


    /**
     * This method is used to trace the path of the pulse.
     * It uses the current position and direction of the pulse to calculate the next positions and directions of the pulse.
     *
     * @param position  is the current position of the pulse emitter.
     * @param direction is the current direction of the pulse emitter.
     *                  SIGHT_DISTANCE is the maximum distance the pulse can see per call.
     *                  MAX_DISTANCE is the maximum distance the pulse can travel.
     *                  STEP is the distance the pulse travels each loop.
     *                  <p>
     *                  The pulse will travel STEP distance in the direction it is facing, and then check if it is on a track.
     *                  If it is on a track, it will snap to the track
     *                  <p>
     *                  The pulse caches the positions it has already visited, so it does not have to recalculate them during landmark detection.
     *                  positionDeque is the queue of positions in front of the emitter.
     *                  oldPositionDeque is the queue of positions passed by the emitter.
     *                  toRemoveQueue is the queue of positions to getting out of sight.
     */
    protected void pathTrace(MovementVector emitterMovement) {
        if (movementDeque.isEmpty()) {
            movementDeque.addLast(emitterMovement);
        }

        for (float i = 0.0F; i < Math.min(MAX_DISTANCE - movementDeque.size() * STEP, SIGHT_DISTANCE) && landmark.getType() == ELandmarkType.UNKNOWN; i += STEP) {
            MovementVector newMovement = movementTick(movementDeque.getLast());

            movementDeque.addLast(newMovement);
        }

        /*
          This removes the old positions from the position deque.
          It does this by comparing the distance between the position of the IPulseEmitter and first postion in {@link positionDeque}
          If the distance between the current position and the first position in the old position deque is smaller, then the first position in the old position deque is added to the toRemoveQueue.
         */

        while (!movementDeque.isEmpty() && emitterMovement.step(STEP).getDistanceTo(movementDeque.getFirst()) > emitterMovement.getDistanceTo(movementDeque.getFirst()) && emitterMovement.step(-STEP).getDistanceTo(movementDeque.getFirst()) < emitterMovement.getDistanceTo(movementDeque.getFirst())) {
            oldMovementDeque.add(movementDeque.removeFirst());
        }


        /*while (oldMovementDeque.size() * STEP >= SIGHT_DISTANCE) {
            toRemoveQueue.add(oldMovementDeque.removeFirst());
        }*/

        if (landmark.getType() != ELandmarkType.UNKNOWN) {
            while (!movementDeque.isEmpty()) {
                if (movementDeque.peekLast() == landmark.getMovement()) {
                    break;
                } else {
                    movementDeque.pollLast();
                }
            }
        }
    }

    protected void clearOutOfSight() {


        if (oldMovementDeque.isEmpty()) {
            return;
        }


        List<TileEntityTrackBase> toRemoveTracks = new ArrayList<>();

        while (!oldMovementDeque.isEmpty()) {
            MovementVector movement = oldMovementDeque.removeFirst();

            if (movement.getDistanceTo(landmark.getMovement()) < STEP) {

                APulse nextPulse = landmark.getPulse();
                if (nextPulse != null) {
                    movementDeque = nextPulse.movementDeque.clone();


                    landmark = nextPulse.landmark;
                    /*if (landmark == null) {
                        landmark = new UndefinedLandmark(this, oldMovementDeque.getLast());
                    }*/
                    nextPulse.landmark = null;
                    //nextPulse.clear();
                }
            }


            TileEntityTrackBase oldTrack = getTrackTileEntityBase(emitter.getWorld(), movement.getPosition());

            if (oldTrack != null) {
                if (currentTrack != oldTrack) {
                    if (visitedTracks.contains(oldTrack) && !toRemoveTracks.contains(oldTrack)) {
                        toRemoveTracks.add(oldTrack);
                        visitedTracks.remove(oldTrack);
                    }
                } else {
                    oldMovementDeque.addLast(movement);
                    break;
                }
            }
        }

        for (TileEntityTrackBase track : toRemoveTracks) {
            APulse pulse = track.getPulse(pulseID);
            if (pulse != null) {
                pulse.clear();
            }


        }

        toRemoveTracks.clear();

        toRemoveQueue.clear();
    }

    protected void detectNextLandmark() {
        if (movementDeque.isEmpty()) {
            return;
        }


        Stack<MovementVector> visitedPositions = new Stack<>();
        // Check not performed on the first position in the queue, as it is the position of the emitter.
        MovementVector movement = movementDeque.pollFirst();
        visitedPositions.push(movement);

        for (int i = 0; i < Math.min(SIGHT_DISTANCE * 2, movementDeque.size() + visitedPositions.size()); i++) {
            // If we are over the sight distance, we skip all the position that have been already visited.
            // If we hit the last position on the position deque, we start again.
            if (i >= SIGHT_DISTANCE) {
                if (lastVisitedMovement == movementDeque.peekLast()) {
                    lastVisitedMovement = visitedPositions.peek();
                }
                while (movementDeque.contains(lastVisitedMovement)) {
                    visitedPositions.push(movementDeque.pollFirst());
                }
            }

            // If the position deque is empty, then there are no more positions to check.
            if (movementDeque.isEmpty()) {
                break;
            }

            // If we visited the position of the next landmark, we stop the detection.
            if (landmark.getType() != ELandmarkType.UNKNOWN && visitedPositions.contains(landmark.getMovement())) {
                break;
            }

            // Start detecting the next landmark.
            movement = movementDeque.removeFirst();

            long time = System.currentTimeMillis();

            Vec3 lastDirection = visitedPositions.peek().getDirection();

            //System.out.println(System.currentTimeMillis() - time);

            visitedPositions.push(movement);


            ALandmark nextLandmark = getLandmark(movement, visitedPositions.peek().getDirection(), i);

            if ((visitedPositions.contains(landmark.getMovement()) || nextLandmark.getType() != ELandmarkType.UNKNOWN) && (nextLandmark.getType() != landmark.getType() || nextLandmark.getMovement() != landmark.getMovement()) && nextLandmark.getMovement().getDistanceTo(visitedPositions.get(0)) > STEP) {
                landmark.dismiss();
                nextLandmark.appear();
                landmark = nextLandmark;

                if (landmark.getType() != ELandmarkType.UNKNOWN) {
                    break;
                }
            } else {

            }

        }

        lastVisitedMovement = movement;

        /*
            Added the visited positions to back into the position deque, so they are not recalculated on the next tickPulse path tracing.
         */
        while (!visitedPositions.isEmpty()) {
            movementDeque.addFirst(visitedPositions.pop());
        }
    }

    public ALandmark getLandmark(MovementVector movement, Vec3 lastDirection, int step) {

        if (Math.acos(BigDecimal.valueOf(movement.getDirection().dotProduct(lastDirection)).setScale(4, RoundingMode.HALF_UP).doubleValue()) * DEG_FACTOR > 60.0) {
            return new EOTLandmark(this, movement, false);
        }

        TrackObj trackUnder = getTrackBelowThis(emitter.getWorld(), movement.getPosition());
        if (trackUnder == null) {
            return new EOTLandmark(this, movement, false);
        } else {

            TileEntityTrackBase track = getTrackTileEntityBase(emitter.getWorld(), movement.getPosition());
            if (track != null) {

                if (step > 4) {
                    CopyOnWriteArrayList<Entity> colliding = getCollidingEntities(movement.getPosition());

                    Optional<Entity> first = colliding.stream().findFirst();
                    if (first.isPresent()) {
                        if (first.get() instanceof Train) {
                            return new TrainLandmark(this, movement);
                        }
                    }
                }
                // The bounding box used to check for trains.


                if (visitedTracks.isEmpty() || !visitedTracks.contains(track)) {
                    visitedTracks.addLast(track);
                }

                if (track != emitter && currentTrack != track) {

                    if (track.getInstanceOfShape() instanceof ISignalMagnet) {
                        ISignalMagnet magnet = (ISignalMagnet) track.getInstanceOfShape();
                        if (track.getField(ARailwaySectionTrackPackSignalMagnet.INDEX_HASMAGNET, false, Boolean.class)) {
                            if (!magnet.getIsTermination(track) && !magnet.getIsStation(track)) {
                                return new MagnetLandmark(this, magnet, track, movement);
                            } else {
                                return new EOTLandmark(this, movement, false);
                            }
                        }
                    }

                    if (track.getInstanceOfShape() instanceof ASwitch) {
                        return new SwitchLandmark(this, track, movement);
                    }

                    if (track.getInstanceOfShape() instanceof ABuffer) {
                        return new EOTLandmark(this, movement, true);
                    }

                }
            }
        }
        return new UndefinedLandmark(this, movement);
    }

    protected CopyOnWriteArrayList<Entity> getCollidingEntities(Vec3 pos) {
        AxisAlignedBB mask = AxisAlignedBB.getBoundingBox(pos.xCoord - 0.5D, pos.yCoord - 1.0D, pos.zCoord - 0.5D, pos.xCoord + 0.5D, pos.yCoord + 2.0D, pos.zCoord + 0.5D);

        return CollisionHandler.INSTANCE.getCollidingEntities(emitter.getWorld(), mask, selectorTrains);
    }


    /**
     * Called to perform a 'movement tick' to this balise pulse. When this is called, the balise pulse will move forward by the distance specified in the arguments of this method (distance) and automatically snap itself onto any tracks
     * below it as it goes along.
     *
     * @param position  - The current position of this balise pulse.
     * @param direction - The current direction of this balise pulse.
     */
    protected MovementVector movementTick(MovementVector movementVector) {


        Vec3 position = movementVector.getPosition();
        Vec3 direction = movementVector.getDirection();

        PositionStackEntry previousPos = new PositionStackEntry(position.xCoord, position.yCoord, position.zCoord);
        previousPos.yaw = (float) (-Math.atan2(direction.zCoord, direction.xCoord) * DEG_FACTOR);

        PositionStackEntry currentPos = previousPos.copy();

        // Actually moving the pulse.

        currentPos.offset(direction.xCoord * STEP, direction.yCoord * STEP, direction.zCoord * STEP);

        // Detecting the track below the pulse.
        TrackObj trackUnder = getTrackBelowThis(emitter.getWorld(), position);

        // Positioning on track
        boolean isOnTrack = false;

        if (trackUnder != null) {
            PositionStackEntry newPos = currentPos.copy();

            isOnTrack = trackUnder.clampToTrack(null, newPos);

            if (isOnTrack) {
                currentPos.setPosition(newPos.x, newPos.y, newPos.z);
            }
        }

        // Returns new position and direction
        return new MovementVector(Vec3.createVectorHelper(currentPos.x, currentPos.y, currentPos.z), Vec3.createVectorHelper(currentPos.x - previousPos.x, currentPos.y - previousPos.y, currentPos.z - previousPos.z).normalize());
    }

    /**
     * <p>
     * Helper method which gets a {@link zoranodensha.api.vehicles.util.TrackObj} instance of the track currently underneath this Balise Pulse's location.
     * </p>
     * <p>
     * This method will return {@code null} if there is no track found.
     * </p>
     *
     * @param world - The current world object which is used to access the blocks underneath this balise pulse.
     * @return - A {@link zoranodensha.api.vehicles.util.TrackObj} instance if a track is found underneath this balise pulse, but if no tracks are found, {@code null}.
     */
    protected TrackObj getTrackBelowThis(World world, Vec3 position) {
        TrackObj trackUnder = null;

        int x = MathHelper.floor_double(position.xCoord);
        int y = MathHelper.floor_double(position.yCoord + 0.125D);
        int z = MathHelper.floor_double(position.zCoord);

        for (int i = 0; i <= 1; i++) {
            trackUnder = TrackObj.isTrack(world, x, y - i, z, null);

            if (trackUnder != null)
                break;
        }

        return trackUnder;
    }

    public ArrayDeque<MovementVector> getDebugPath() {
        return movementDeque.clone();
    }

    public void clear() {
        if (landmark != null) {
            landmark.dismiss();
        }

        landmark = new UndefinedLandmark(this, emitterMovement);

        movementDeque.clear();
    }

    public UUID getID() {
        return pulseID;
    }

    /*private boolean containsPos(Stack<MovementVector> deque, MovementVector movementVector) {
        for (MovementVector vec : deque) {
            if (vec.getPosition().distanceTo(movementVector.getPosition()) == 0.0) {
                return true;
            }
        }
        return false;

    }*/

    public @Nullable TileEntityTrackBase getTrackTileEntityBase(World world, Vec3 position) {
        TrackObj trackUnder = getTrackBelowThis(world, position);

        if (trackUnder != null) {
            TileEntity tile = world.getTileEntity(trackUnder.x, trackUnder.y, trackUnder.z);

            if (tile instanceof ITrackBase) {
                ITrackBase track = (ITrackBase) tile;

                if (track instanceof TileEntityTrackBaseGag) {
                    track = ((TileEntityTrackBaseGag) track).getSourceTile();
                }

                if (track instanceof TileEntityTrackBase) {
                    return (TileEntityTrackBase) track;
                }
            }
        }

        return null;
    }


}
