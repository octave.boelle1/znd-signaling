package zoranodensha.api.structures.signals;

import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import org.lwjgl.Sys;
import zoranodensha.api.structures.signals.landmark.UndefinedLandmark;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.handlers.CollisionHandler;
import zoranodensha.api.vehicles.part.type.PartTypeCab;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class TrainPulse extends APulse {

    public static final int BLOCKS = 8;

    public TrainPulse(PartTypeCab emitter) {
        super(emitter, UUID.randomUUID());
        selectorTrains = new CollisionHandler.EntitySelectorTrains(emitter.getEmitterEntity());
    }

    public void tickPulse(MovementVector emitterMovement, int blocks) {


        super.tickPulse(emitterMovement, BLOCKS);

        /*System.out.println(movementDeque.size());*/
        System.out.println(landmark);
    }

    @Override
    protected CopyOnWriteArrayList<Entity> getCollidingEntities(Vec3 pos) {
        AxisAlignedBB mask = AxisAlignedBB.getBoundingBox(pos.xCoord - 0.5D, pos.yCoord - 1.0D, pos.zCoord - 0.5D, pos.xCoord + 0.5D, pos.yCoord + 2.0D, pos.zCoord + 0.5D);

        CopyOnWriteArrayList<Entity> collisionCandidates = CollisionHandler.INSTANCE.getCollidingEntities(emitter.getWorld(), mask, selectorTrains);

        if (collisionCandidates.contains(emitter.getEmitterEntity())) {
            collisionCandidates.remove(emitter.getEmitterEntity());
        }

        return collisionCandidates;
    }
}
