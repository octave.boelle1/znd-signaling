package zoranodensha.api.structures.signals;

import net.minecraft.util.Vec3;

public class MovementVector {

    Vec3 position;
    Vec3 direction;

    public MovementVector(Vec3 position, Vec3 direction) {
        this.position = Vec3.createVectorHelper(position.xCoord, position.yCoord, position.zCoord);
        this.direction = Vec3.createVectorHelper(direction.xCoord, direction.yCoord, direction.zCoord);
    }


    public Vec3 getPosition() {
        return position;
    }

    public Vec3 getDirection() {
        return direction;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof MovementVector && ((MovementVector) obj).getPosition().equals(position);
    }

    @Override
    public String toString() {
        return "MovementVector: " + position.toString() + " " + direction.toString();
    }


    public double getDistanceTo(MovementVector other) {
        return position.distanceTo(other.getPosition());
    }

    public MovementVector step(float step) {
        return new MovementVector(position.addVector(this.direction.xCoord * step, this.direction.yCoord * step, this.direction.zCoord * step), direction);
    }
}

