package zoranodensha.api.structures.signals;

import zoranodensha.api.structures.tracks.ITrackBase;



public interface ISignalMagnet extends ISignalEquipment
{
	/**
	 * <p>
	 * Automatic trains can be instructed to stop at this magnet (representing a station stopping point) if this method returns {@code true}.
	 * </p>
	 * 
	 * @return - A {@code boolean} which is {@code true} if automatic trains should stop at this magnet.
	 */
	boolean getIsStation(ITrackBase trackBase);

	/**
	 * <p>
	 * Autoamtic trains can be instructed to stop and change direction at this magnet (representing a turnback/terminating point) if this method returns {@code true}.
	 * </p>
	 * 
	 * @return - A {@code boolean} which is {@code true} if automatic trains should stop and terminate/change direction at this magnet.
	 */
	boolean getIsTermination(ITrackBase trackBase);

	/**
	 * <p>
	 * This method is used to retrieve the linked signal of this signal magnet. If there is no signal linked, this method will return {@code null}.
	 * </p>
	 * 
	 * @return - The linked {@link ISignal} instance, or if none exists,
	 *         {@code null}.
	 */
	ISignal getLinkedSignal(ITrackBase trackBase);

	/**
	 * <p>
	 * The track speed beyond this magnet can be assigned by having this method return an integer other than 0.
	 * </p>
	 * 
	 * @return - An integer representing the track speed in km/h beyond this magnet. {@code 0} means unlimited speed.
	 */
	int getTrackSpeed(ITrackBase trackBase);

	/**
	 * <p>
	 * Called to assign whether this signal magnet represents a station stopping point.
	 * </p>
	 * 
	 * @param isStation - {@code true} if this magnet should represent a station stopping point.
	 */
	void setIsStation(ITrackBase trackBase, boolean isStation);

	/**
	 * <p>
	 * Called to assign whether this signal magnet represents a train turnback/termination point.
	 * </p>
	 * 
	 * @param isTermination {@code true} if this magnet should represent a train turnback/termination point where automatic trains will stop and change direction.
	 */
	void setIsTermination(ITrackBase trackBase, boolean isTermination);

	/**
	 * <p>
	 * Called to assign a new signal reference that this magnet will link to.
	 * </p>
	 * 
	 * @param signal - A {@link ISignal} instance that this magnet should link to.
	 */
	void setLinkedSignal(ISignal signal, ITrackBase trackBase);


	void clearSignal(ITrackBase trackBase);

	/**
	 * <p>
	 * Called to assign a new track speed to this magnet. If you supply a {@code 0} it will essentially remove the speed limit from this magnet, otherwise any other integer will act as the track speed
	 * for the track beyond this magnet.
	 * </p>
	 * 
	 * @param newSpeed - The new speed limit to use in km/h, as an integer.
	 */
	void setTrackSpeed(int newSpeed, ITrackBase trackBase);

}
