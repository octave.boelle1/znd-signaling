package zoranodensha.api.structures.signals;

import com.mojang.realmsclient.util.Pair;
import net.minecraft.util.Vec3;

import java.util.UUID;

public class TrackPulse extends APulse {
    public TrackPulse(IPulseEmitter emitter, UUID uuid) {
        super(emitter, uuid);
    }
}
