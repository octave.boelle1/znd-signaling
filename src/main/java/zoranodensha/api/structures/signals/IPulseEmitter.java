package zoranodensha.api.structures.signals;

import net.minecraft.entity.Entity;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public interface IPulseEmitter {

    /*HashMap<UUID, APulse> pulses;
    CopyOnWriteArrayList<PulseNotification> notifications = new CopyOnWriteArrayList<>();*/

    Vec3 getEmitterPosition();

    World getWorld();

    Object getEmitterEntity();

    void notifyPulse(PulseNotification notification);

    APulse getPulse(UUID pulseID);

    void addPulse(APulse pulse);

    HashMap<UUID, APulse> getPulses();

    void clear();

    void removePulse(UUID id);
}
