package zoranodensha.api.structures.signals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import zoranodensha.api.util.APILogger;



/**
 * This is a registry which holds all registered types of signals.
 * You must register your signal class with this registry for it to work.
 * 
 * @author Jaffa
 */
public abstract class ASignalRegistry
{
	/**
	 * The static map instance containing all registered signals.
	 */
	protected static Map<String, ISignal> signalMap = new HashMap<String, ISignal>();



	/**
	 * @return - A list of the names of all registered signals.
	 */
	public static final ArrayList<String> getRegisteredSignalNames()
	{
		ArrayList<String> signalNames = new ArrayList<String>();

		for (ISignal signal : signalMap.values())
		{
			signalNames.add(signal.getName());
		}

		return signalNames;
	}


	/**
	 * <p>
	 * Returns a newly-created instance of
	 * {@link zoranodensha.api.structures.signals.ISignal} based on the name
	 * provided.
	 * </p>
	 * 
	 * @param name - The name of an already-registered signal to create a new
	 *            instance of.
	 * @return - The newly-created instance of the signal, or if no such signal type
	 *         exists under the specified name, {@code null}.
	 */
	public static final ISignal getSignalFromName(String name)
	{
		try
		{
			return signalMap.get(name).getClass().newInstance();
		}
		catch (NullPointerException npe)
		{
			APILogger.catching(npe);
		}
		catch (InstantiationException ex)
		{
			/* Silent catch. */
		}
		catch (IllegalAccessException ex)
		{
			/* Silent catch. */
		}

		return null;
	}


	/**
	 * Called to register the crafting recipes of all the registered signals.
	 */
	public static void registerRecipes()
	{
		for (Object signal : signalMap.values().toArray())
		{
			if (signal instanceof ISignal)
			{
				((ISignal)signal).registerRecipes();
			}
		}
	}


	/**
	 * <p>
	 * Registers an {@link zoranodensha.api.structures.signals.ISignal} instance.
	 * </p>
	 * 
	 * @param signal - The signal to register.
	 * @return - A flag indicating whether the operation succeeded.
	 * @throws {@link IllegalArgumentException}
	 */
	public static boolean registerSignal(ISignal signal)
	{
		if (signal != null)
		{
			String name = signal.getName();

			if (name == null || name.isEmpty())
			{
				throw new IllegalArgumentException("[SignalRegistry] A mod tried to register an ISignal with null or empty name.");
			}

			signalMap.put(name, signal);

			return true;
		}

		return false;
	}
}
