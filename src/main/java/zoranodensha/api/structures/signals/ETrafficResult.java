package zoranodensha.api.structures.signals;

public enum ETrafficResult {


    HIGH_SPEED(-1),
    CLEAR(-1),
    CAUTION(-1),
    STOP(0),
    CLEAR_SLOW(40),
    EXPECT_SLOW(40),
    SUSTAINED_SLOW(40);

    private final float speed;

    ETrafficResult(float speed) {
        this.speed = speed;
    }

    public float getSpeed() {
        return speed;
    }

    @Override
    public String toString() {
        return super.toString() + " " + speed;
    }

}
