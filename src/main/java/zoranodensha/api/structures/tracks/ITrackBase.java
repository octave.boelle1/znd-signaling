package zoranodensha.api.structures.tracks;

import net.minecraft.world.World;

import javax.annotation.Nullable;



/**
 * The interface that is implemented by all tracks of Zora no Densha.
 */
public interface ITrackBase
{
	/**
	 * Return the direction instance this shape faces.
	 */
	EDirection getDirectionOfSection();

	/**
	 * Return the field of internally handled Objects at the given index.
	 */
	<T> T getField(int index, T defaultVal, Class<T> type);

	/**
	 * Return the field of internally handled Object at the given index without type checks or default return value.
	 */
	Object getFieldUnchecked(int index);

	/**
	 * Returns the corresponding instance name of the overall track.
	 */
	IRailwaySection getInstanceOfShape();

	/**
	 * Return the length of this overall track section.
	 */
	int getLengthOfSection();

	/**
	 * Returns the orientation this ITrackBase faces. Used as replacement for Minecraft's Metadata flag.
	 * <p>
	 * 0 = East (+X)<br>
	 * 1 = South (+Z)<br>
	 * 2 = West (-X)<br>
	 * 3 = North (-Z)
	 */
	int getOrientation();

	/**
	 * Return the current path number.
	 */
	int getPath();

	/**
	 * Calculates and returns the next position of the given Entity on this track.<br>
	 * Also returns the rotation (yaw [about Y-axis], pitch [about Z-axis], and roll [about X-axis]) at the new position.
	 *
	 * @param vehicle - A moving object to reference. Used by switch tracks to further determine correct positioning depending on the vehicle's motion.
	 * @param x - The raw position on X-axis.
	 * @param y - The raw position on Y-axis.
	 * @param z - The raw position on Z-axis.
	 * @param defYaw - The default yaw value to be returned if the track is not finished.
	 * @param defPitch - The default pitch value to be returned if the track is not finished.
	 * @return The position and rotation of the vehicle, respectively.<br>
	 *         Index 0 = Position to apply (x,y,z);<br>
	 *         Index 1 = Rotation (yaw, pitch, roll).
	 */
	double[][] getPositionOnTrack(@Nullable Object vehicle, double x, double y, double z, float defYaw, float defPitch);

	/**
	 * Returns the state of this track segment, i.e. whether it is a track bed, has fastening, etc. See {@link ERailwayState} for all states.
	 */
	ERailwayState getStateOfShape();

	/** Gets the X coordinate of this instance. */
	int getX();

	/** Gets the Y coordinate of this instance. */
	int getY();

	/** Gets the Z coordinate of this instance. */
	int getZ();

	/** Gets the Z coordinate of this instance. */
	World getWorld();

	/**
	 * Tries to set the given Object into the given index of the track's internal field Map. Setting an index will override the field at the given index. Returns true if the previous value was null.
	 */
	boolean setField(int index, Object param);

	/**
	 * Sets the path variable of this track.
	 */
	void setPath(int path);

	/**
	 * Sets the state of the overall track.
	 */
	void setStateOfShape(ERailwayState state);
}
