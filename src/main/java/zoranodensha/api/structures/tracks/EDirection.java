package zoranodensha.api.structures.tracks;

/**
 * The direction of a track.
 * <p>
 * If a track (such as switches or certain cross tracks) has a mirrored counterpart, it is either {@code RIGHT} or {@code LEFT}, respectively.
 */
public enum EDirection
{
	LEFT(-1),
	NONE(0),
	RIGHT(1);
	private final int id;



	EDirection(int id)
	{
		this.id = id;
	}


	public int toInt()
	{
		return id;
	}

	@Override
	public String toString()
	{
		switch (this)
		{
			default:
				return "";
			case LEFT:
				return "Left";
			case RIGHT:
				return "Right";
		}
	}

	public static EDirection fromString(String s)
	{
		return ("left".equalsIgnoreCase(s) ? LEFT : "right".equalsIgnoreCase(s) ? RIGHT : NONE);
	}
}
