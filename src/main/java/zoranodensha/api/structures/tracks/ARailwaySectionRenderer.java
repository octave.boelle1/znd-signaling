package zoranodensha.api.structures.tracks;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.util.Vec3;
import org.lwjgl.opengl.GL11;
import zoranodensha.api.structures.signals.APulse;
import zoranodensha.api.structures.signals.IPulseEmitter;
import zoranodensha.api.structures.signals.MovementVector;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;
import zoranodensha.vehicleParts.client.render.RenderUtil;

import java.util.ArrayDeque;
import java.util.ArrayList;

public abstract class ARailwaySectionRenderer implements IRailwaySectionRenderer {
    @Override
    public void renderTrack(int detail, int gagTrack, IRailwaySection section, ITrackBase trackBase, float x, float y, float z, float f) {
        if (trackBase instanceof IPulseEmitter) {
            GL11.glPushMatrix();
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);


            if (Minecraft.getMinecraft().gameSettings.showDebugInfo) {
                renderPulse(trackBase, f);
            }


            GL11.glDisable(GL11.GL_BLEND);
            GL11.glPopMatrix();
        }
    }

    public void renderPulse(ITrackBase trackBase, float partialTick) {

        if (!(trackBase instanceof TileEntityTrackBase) && trackBase.getInstanceOfShape() == null) {
            return;
        }

        IPulseEmitter track = (IPulseEmitter) trackBase;

        for (APulse pulse : new ArrayList<>(track.getPulses().values())) {
            ArrayDeque<MovementVector> debugPath = pulse.getDebugPath();

            if (debugPath != null && !debugPath.isEmpty()) {
                GL11.glPushMatrix();
                RenderUtil.lightmapPush();
                GL11.glPushAttrib(GL11.GL_ENABLE_BIT);
                {
                    GL11.glDisable(GL11.GL_TEXTURE_2D);
                    GL11.glDisable(GL11.GL_LIGHTING);
                    GL11.glEnable(GL11.GL_LINE_SMOOTH);
                    RenderUtil.lightmapBright();
                    {
                        if (pulse.landmark != null)
                            switch (pulse.landmark.getResult()) {
                                case HIGH_SPEED:
                                    GL11.glColor3f(0.0F, 0.0F, 1.0F);
                                    break;
                                case CLEAR:
                                    GL11.glColor3f(0.0F, 1.0F, 0.0F);
                                    break;
                                case CAUTION:
                                //case SPEED_LIMIT:
                                case CLEAR_SLOW:
                                case EXPECT_SLOW:
                                case SUSTAINED_SLOW:
                                    GL11.glColor3f(1.0F, 1.0F, 0.0F);
                                    break;
                                case STOP:
                                    GL11.glColor3f(1.0F, 0.0F, 0.0F);
                                    break;
                                default:
                                    GL11.glColor3f(1.0F, 1.0F, 1.0F);
                                    break;
                            }
                        else {
                            GL11.glColor3f(0.0F, 0.0F, 0.0F);
                        }
                        GL11.glLineWidth(3.0F);

                        Entity player = Minecraft.getMinecraft().thePlayer;
                        double offsetX, offsetY, offsetZ;
                        {
                            offsetX = player.prevPosX + ((player.posX - player.prevPosX) * partialTick);
                            offsetY = player.prevPosY + ((player.posY - player.prevPosY) * partialTick);
                            offsetZ = player.prevPosZ + ((player.posZ - player.prevPosZ) * partialTick);
                        }
                        GL11.glTranslated(-offsetX, -offsetY, -offsetZ);
                        GL11.glBegin(GL11.GL_LINE_STRIP);
                        {
                            while (!debugPath.isEmpty()) {
                                Vec3 pos = debugPath.poll().getPosition();
                                //GL11.glPushMatrix();
                           /* Vec3 direction = lastPos.subtract(pos).normalize();
                            pos.rotateAroundY((float) Math.atan2(direction.zCoord, direction.xCoord));*/
                                //GL11.glRotated(90.0, pos.xCoord, pos.yCoord + 0.5F, pos.zCoord);

                                GL11.glVertex3d(pos.xCoord, pos.yCoord + 0.5F, pos.zCoord);

                                /*if (pulse.nextLandmark != null && pos == pulse.nextLandmark.getPosition()) {
                                    break;
                                }*/


                                //GL11.glVertex3d(pos.xCoord * direction.xCoord, pos.yCoord + 0.5F, pos.zCoord * direction.zCoord);
                                //GL11.glPopMatrix();

                                //lastPos = pos;
                                // GL11.glVertex3d(pos.xCoord, pos.yCoord + 0.5F, -pos.zCoord);

                            }
                        }
                        GL11.glEnd();

                    }
                }
                RenderUtil.lightmapPop();
                GL11.glPopAttrib();
                GL11.glPopMatrix();

            }
        }
    }
}
