package zoranodensha.api.structures.tracks;

import net.minecraft.nbt.NBTTagCompound;



/**
 * This class contains methods to handle paths as used in Zora no Densha. A Path is a graph (defined by an {@link Expression}) that operates solely on local (i.e. relative) coordinates.
 */
public final class Path
{
	final Expression[] paths = new Expression[4];



	/**
	 * Construct a new Path from the given argument arrays. May throw an exception if fed incorrect parameters.
	 *
	 * @param pathFlat - A mathematical expression of this Path on flat (x/z) level in a String; This argument must not be null
	 * @param pathGrade - A mathematical expression of this Path's grade (x/y) in a String
	 * @throws IllegalArgumentException If the flat path is invalid or null.
	 */
	public Path(String[] pathFlat, String[] pathGrade)
	{
		if (pathFlat == null || pathFlat.length != 2)
		{
			throw new IllegalArgumentException("[ModTrackRegistry] A mod tried to register a Path of invalid expression.");
		}
		paths[0] = new Expression(pathFlat[0]);
		paths[1] = new Expression(pathFlat[1]);
		paths[2] = (pathGrade != null ? new Expression(pathGrade[0]) : null);
		paths[3] = (pathGrade != null ? new Expression(pathGrade[1]) : null);
	}


	/**
	 * Calculates and returns the local coordinates for the given local coordinate.
	 *
	 * @param localX - The local X-coordinate
	 * @return The local (X-, Y- and Z-) coordinates
	 */
	public double[] calculatePosition(double localX)
	{
		return new double[] { get(0, localX), get(2, localX), 0.0D };
	}

	/**
	 * Calculates and returns the rotations for the given local coordinate.<br>
	 * <b>Note the different order of rotation axes in the returned array.</b>
	 *
	 * @param localX - The local X-coordinate
	 * @return The array of rotations [Rotation about Y-, Z- and X-axis, respectively]
	 */
	public double[] calculateRotation(double localX)
	{
		return new double[] { get(1, localX), get(3, localX), 0.0D };
	}

	/**
	 * Helper to access the value cache of the respective path.
	 */
	private double get(int index, double localX)
	{
		return (paths[index] != null) ? paths[index].value(localX) : 0.0D;
	}

	/**
	 * Writes this Path to NBT and returns the passed NBTTagCompound again.
	 */
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		for (int i = 0; i < paths.length; i += 2)
		{
			if (paths[i] != null && paths[i + 1] != null)
			{
				nbt.setString("Path_" + i, paths[i].toString());
				nbt.setString("Path_" + (i + 1), paths[i + 1].toString());
			}
		}
		return nbt;
	}

	/**
	 * Reads a Path from NBT and returns a new Path instance.
	 */
	public static Path readFromNBT(NBTTagCompound nbt)
	{
		String[] path0 = new String[] { nbt.getString("Path_0"), nbt.getString("Path_1") };
		String[] path1 = (nbt.hasKey("Path_2") ? new String[] { nbt.getString("Path_2"), nbt.getString("Path_3") } : null);
		return new Path(path0, path1);
	}
}
