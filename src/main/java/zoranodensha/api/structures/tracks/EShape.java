package zoranodensha.api.structures.tracks;

/**
 * The generic shape types of a track.
 */
public enum EShape
{
	/** Diagonal tracks, straight tracks, (straight) slope tracks, ... */
	STRAIGHT(0, "S"),
	/** (Tilted) Curves, ... */
	CURVE(1, "C"),
	/** Cross tracks, ... */
	CROSS(2, "X"),
	/** Cross switches, (Curve) Switches, ... */
	SWITCH(3, "Y");

	private final int id;
	private final String label;



	EShape(int id, String label)
	{
		this.id = id;
		this.label = label;
	}

	public int toInt()
	{
		return id;
	}

	public String toLabel()
	{
		return label;
	}

	@Override
	public String toString()
	{
		switch (this)
		{
			default:
				return "<Error>";
			case STRAIGHT:
				return "Straight";
			case CURVE:
				return "Curve";
			case CROSS:
				return "Cross";
			case SWITCH:
				return "Switch";
		}
	}
}
