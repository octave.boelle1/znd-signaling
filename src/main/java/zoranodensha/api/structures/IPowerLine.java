package zoranodensha.api.structures;

/**
 * All blocks implementing this interface are regarded as overhead line/ 3rd rail to draw electric power from.
 */
public interface IPowerLine
{
}
