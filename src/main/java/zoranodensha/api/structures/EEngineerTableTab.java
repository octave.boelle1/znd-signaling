package zoranodensha.api.structures;

/**
 * An enum containing all tabs of the Engineer's Table.
 */
public enum EEngineerTableTab
{
	TRACKS(0),
	TRAINS(1),
	SIGNALS(2);
	private final int index;



	EEngineerTableTab(int i)
	{
		index = i;
	}


	public boolean isTrainTab()
	{
		return (this == TRAINS);
	}

	public int toInt()
	{
		return index;
	}

	public static EEngineerTableTab getFromInt(int index)
	{
		switch (index)
		{
			default:
				return TRACKS;
			case 1:
				return TRAINS;
			case 2:
				return SIGNALS;
		}
	}
}
