package zoranodensha.api.util;

import javax.annotation.Nullable;

import org.apache.logging.log4j.Logger;



/**
 * Logger class to log errors and warnings that involve API files. Its {@link #logger} field will be populated by Zora no Densha during post initialisation.
 * 
 * @author Leshuwa Kaiheiwa
 */
public abstract class APILogger
{
	/** The Logger field. May be {@code null}. */
	private static Logger logger;



	/**
	 * Bridge method to {@link Logger#catching(Throwable)}.
	 */
	public static final void catching(Throwable t)
	{
		if (logger != null)
		{
			logger.catching(t);
		}
		else
		{
			t.printStackTrace(System.err);
		}
	}

	/**
	 * Appends, if specified, the given caller object's class name before the given message.
	 */
	public static final void debug(@Nullable Object caller, String msg)
	{
		if (caller != null)
		{
			if (caller instanceof Class)
			{
				msg = String.format("[%s] %s", ((Class<?>)caller).getSimpleName(), msg);
			}
			else
			{
				msg = String.format("[%s] %s", caller.getClass().getSimpleName(), msg);
			}
		}
		
		if (logger != null)
		{
			logger.debug(msg);
		}
	}

	/**
	 * Appends, if specified, the given caller object's class name before the given message.
	 */
	public static final void info(@Nullable Object caller, String msg)
	{
		if (caller != null)
		{
			if (caller instanceof Class)
			{
				msg = String.format("[%s] %s", ((Class<?>)caller).getSimpleName(), msg);
			}
			else
			{
				msg = String.format("[%s] %s", caller.getClass().getSimpleName(), msg);
			}
		}
		
		if (logger != null)
		{
			logger.info(msg);
		}
		else
		{
			String s = (caller != null) ? String.format("[%s][%s] %s", "INFO", caller.getClass().getSimpleName(), msg) : String.format("[%s] %s", "INFO", msg);
			System.out.println(s);
		}
	}

	/**
	 * Overrides the {@link APILogger#logger logger} field with the given logger.
	 * <p>
	 * <b>NOTE</b><br>
	 * You do not have to set this field, it will be initialised by Zora no Densha by default.
	 */
	public static final void overrideLogger(Logger logger)
	{
		if (logger != null)
		{
			APILogger.logger = logger;
		}
	}

	/**
	 * Appends, if specified, the given caller object's class name before the given message.
	 */
	public static final void warn(@Nullable Object caller, String msg)
	{
		if (caller != null)
		{
			if (caller instanceof Class)
			{
				msg = String.format("[%s] %s", ((Class<?>)caller).getSimpleName(), msg);
			}
			else
			{
				msg = String.format("[%s] %s", caller.getClass().getSimpleName(), msg);
			}
		}
		
		if (logger != null)
		{
			logger.warn(msg);
		}
		else
		{
			String s = (caller != null) ? String.format("[%s][%s] %s", "WARN", caller.getClass().getSimpleName(), msg) : String.format("[%s] %s", "WARN", msg);
			System.out.println(s);
		}
	}
}
