package zoranodensha.api.util;

import java.io.IOException;

import com.google.common.base.Throwables;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTSizeTracker;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.PacketBuffer;



/**
 * Utility class containing helper methods for high-load NBT/buffer operations.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class NBTUtil
{
	/* Maximum NBT size, in megabytes. Just short of two MB. */
	public static final int MAX_NBT_SIZE = 2097050;



	/**
	 * Determines whether the total size of both NBT tags does not exceed the {@link #MAX_NBT_SIZE maximum NBT size}.
	 * 
	 * @return {@code true} if the combined size of both NBT tags is valid.
	 */
	public static boolean getIsTotalSizeValid(NBTTagCompound nbtA, NBTTagCompound nbtB)
	{
		return getTotalNBTSize(nbtA, nbtB) <= MAX_NBT_SIZE;
	}

	/**
	 * Calculates the total size of both given NBT tags.
	 * 
	 * @return The size of both NBT tags.
	 */
	public static int getTotalNBTSize(NBTTagCompound nbtA, NBTTagCompound nbtB)
	{
		return writeNBTToBuffer(nbtA, Unpooled.buffer()) + writeNBTToBuffer(nbtB, Unpooled.buffer());
	}

	/**
	 * Reads an NBT tag from the given byte buffer.
	 * 
	 * @param byteBuf - {@link io.netty.buffer.ByteBuf Byte buffer} to read the tag from.
	 * @return The {@link net.minecraft.nbt.NBTTagCompound NBT tag} that was read from the buffer.
	 */
	public static NBTTagCompound readNBTFromBuffer(ByteBuf byteBuf)
	{
		try
		{
			/* Get number of bytes and throw exception in case of overflow. */
			PacketBuffer pb = new PacketBuffer(byteBuf);
			int nbtBytes = pb.readInt();
			if (nbtBytes < 0)
			{
				throw new RuntimeException(String.format("Packet length overflow while reading train spawn data! [%d B]", nbtBytes));
			}

			/* Read bytes and create NBT tag. */
			byte[] abyte = new byte[nbtBytes];
			pb.readBytes(abyte);
			return CompressedStreamTools.func_152457_a(abyte, new NBTSizeTracker(MAX_NBT_SIZE));
		}
		catch (IOException e)
		{
			throw Throwables.propagate(e);
		}
	}

	/**
	 * Writes the given NBT tag compound to the given buffer and returns the number of bytes that were written to the buffer.
	 * 
	 * @param nbt - {@link net.minecraft.nbt.NBTTagCompound NBT tag} to write to the buffer.
	 * @param byteBuf - {@link io.netty.buffer.ByteBuf Byte buffer} to write to.
	 * @return Number of bytes written to the buffer.
	 */
	public static int writeNBTToBuffer(NBTTagCompound nbt, ByteBuf byteBuf)
	{
		try
		{
			PacketBuffer packetBuffer = new PacketBuffer(byteBuf);
			byte[] nbtBytes = CompressedStreamTools.compress(nbt);
			packetBuffer.writeInt(nbtBytes.length);
			packetBuffer.writeBytes(nbtBytes);
			return byteBuf.readableBytes();
		}
		catch (IOException e)
		{
			throw Throwables.propagate(e);
		}
	}
}