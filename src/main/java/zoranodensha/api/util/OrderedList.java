package zoranodensha.api.util;

import java.lang.reflect.Array;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Comparator;



/**
 * <p>
 * Array-backed {@link java.util.List List} implementation, ordering its elements using a supplied comparator.
 * </p>
 * <p>
 * This list implementation does not accept {@code null} values.<br>
 * An attempt to insert {@code null} will result in a {@link NullPointerException} being thrown.
 * </p>
 * <p>
 * Types stored in this list don't need to implement the {@link Comparable} interface,
 * as a {@link Comparator} is supplied upon construction of this list.
 * </p>
 * 
 * @author Leshuwa Kaiheiwa
 */
public class OrderedList<T> extends AbstractList<T> implements Cloneable
{
	/** {@link Comparator} used to order this list. */
	private final Comparator<T> comparator;
	/** Element buffer of this list. The length of the array is this list's capacity. */
	private transient T[] array;



	@SuppressWarnings("unchecked")
	public OrderedList(Class<T> type, Comparator<T> comp)
	{
		comparator = comp;
		array = (T[])Array.newInstance(type, 0);
	}

	/**
	 * Adds the given element to this list, moving all subsequent elements.
	 * 
	 * @param e - Element to be added. Must not be {@code null}.
	 * @return {@code true} as specified by {@link java.util.Collection#add(Object)}.
	 * 
	 * @throws NullPointerException If the given parameter is {@code null}.
	 */
	@Override
	public boolean add(T e)
	{
		if (e == null)
		{
			throw new NullPointerException("Element was null!");
		}

		ensureCapacity(array.length + 1);

		T elem = e;
		for (int i = 0; i < array.length && elem != null; ++i)
		{
			if (array[i] == null || comparator.compare(array[i], elem) < 0)
			{
				T tmp = array[i];
				array[i] = elem;
				elem = tmp;
			}
		}

		onListChanged();
		return true;
	}

	/**
	 * Clears the list contents and resets the size.
	 */
	@Override
	public void clear()
	{
		ensureCapacity(0);
		onListChanged();
	}

	@Override
	public OrderedList<T> clone()
	{
		try
		{
			@SuppressWarnings("unchecked") OrderedList<T> ret = (OrderedList<T>)super.clone();
			ret.array = Arrays.copyOf(array, array.length);
			ret.modCount = 0;
			return ret;
		}
		catch (CloneNotSupportedException e)
		{
			throw new InternalError();
		}
	}

	/**
	 * Returns whether the given element is contained by this list.
	 * 
	 * @return {@code true} if the given element is contained by this list.
	 */
	@Override
	public boolean contains(Object o)
	{
		return (indexOf(o) >= 0);
	}

	/**
	 * Ensures the backed array is of given size.
	 * 
	 * @throws IllegalArgumentException If the specified size is smaller than zero.
	 */
	private void ensureCapacity(int size)
	{
		if (size < 0)
		{
			throw new IllegalArgumentException("Illegal array size specified:" + size);
		}
		modCount++;

		int oldSize = array.length;
		if (size != oldSize)
		{
			array = Arrays.copyOf(array, size);
		}
	}

	/**
	 * Returns the element in this list at the given index.
	 * 
	 * @return List element at given index.
	 * @throws NullPointerException If the index is out of range.
	 */
	@Override
	public T get(int index)
	{
		rangeCheck(index);
		return array[index];
	}

	/**
	 * Returns the index of the given object.
	 * 
	 * @return The given object's index, or {@code -1} if the object isn't contained in this list.
	 */
	@Override
	public int indexOf(Object o)
	{
		if (o != null)
		{
			for (int i = 0; i < array.length; ++i)
			{
				if (o.equals(array[i]))
				{
					return i;
				}
			}
		}
		return -1;
	}

	/**
	 * Called when this list changed, i.e. after successful {@link #add(Object)} and {@link #remove(Object)} operations.
	 */
	protected void onListChanged()
	{
		/* Empty by default. */
	}

	/**
	 * Does a range check to ensure the given index is within the backed
	 * array's bounds. Throws an exception if not.
	 * 
	 * @throws IndexOutOfBoundsException - If {@code Index < 0 || index >= size}
	 */
	private void rangeCheck(int index)
	{
		if (index < 0 || index >= array.length)
		{
			throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + array.length);
		}
	}

	/**
	 * Removes the element at the given index.
	 * 
	 * @return The element that was removed from the list.
	 * @throws IndexOutOfBoundsException If the specified index is not within range.
	 */
	@Override
	public T remove(int index)
	{
		rangeCheck(index);
		modCount++;
		T rem = array[index];

		int numMoved = array.length - index - 1;
		if (numMoved > 0)
		{
			System.arraycopy(array, index + 1, array, index, numMoved);
		}

		int newSize = array.length - 1;
		array[newSize] = null;
		ensureCapacity(newSize);

		onListChanged();

		return rem;
	}

	/**
	 * Removes the specified element from this list.
	 * 
	 * @return {@code true} if the element was successfully removed from the list.
	 */
	@Override
	public boolean remove(Object o)
	{
		int i = indexOf(o);
		if (i >= 0)
		{
			return (remove(i) != null);
		}

		return false;
	}

	/**
	 * Returns the size of this list.
	 */
	@Override
	public int size()
	{
		return array.length;
	}
}