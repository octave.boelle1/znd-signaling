package zoranodensha.api.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;



/**
 * Describes the associated field's synchronisation behaviour between client and server side.<br>
 * Meant for use in source code only, to distinguish which fields will be actively synchronised and which won't.
 *
 * @see zoranodensha.api.util.ESyncDir ESyncDir
 * @see cpw.mods.fml.relauncher.Side Side
 * 
 * @author Leshuwa Kaiheiwa
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.FIELD)
public @interface SyncDir
{
	ESyncDir value();
}
