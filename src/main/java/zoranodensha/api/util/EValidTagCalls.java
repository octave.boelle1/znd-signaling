package zoranodensha.api.util;

/**
 * Flags declaring which NBT tag calls are considered when interacting with NBT.
 * 
 * @author Leshuwa Kaiheiwa
 */
public enum EValidTagCalls
{
	/** All tag calls are valid. */
	ANY,
	/** {@code ITEM} and {@code PACKET} calls are valid. */
	ITEM_PACKET,
	/** {@code ITEM} and {@code SAVE} calls are valid. */
	ITEM_SAVE,
	/** {@code PACKET} and {@code SAVE} calls are valid. */
	PACKET_SAVE,
	/** Only {@code ITEM} calls are valid. */
	ITEM,
	/** Only {@code PACKET} calls are valid. */
	PACKET,
	/** Only {@code SAVE} calls are valid. */
	SAVE;

	
	/**
	 * Determines whether the given call type is accepted by this validity mode.
	 * 
	 * @param callType - {@link zoranodensha.api.util.ETagCall Tag call} to consider.
	 * @return {@code true} if the given tag call is accepted.
	 */
	public boolean isValid(ETagCall callType)
	{
		if (this == ANY)
		{
			return true;
		}

		switch (callType)
		{
			case ITEM:
				return (this == ITEM || this == ITEM_PACKET || this == ITEM_SAVE);

			case PACKET:
				return (this == PACKET || this == ITEM_PACKET || this == PACKET_SAVE);

			case SAVE:
				return (this == SAVE || this == ITEM_SAVE || this == PACKET_SAVE);

			default:
				return false;
		}
	}
}