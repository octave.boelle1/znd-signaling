package zoranodensha.api.util;

/**
 * A 2D vector using {@code float} vector components.
 * 
 * @author Jaffa
 */
public class Vec2
{
	/**
	 * The X component of this 2D vector.
	 */
	public float x = 0.0F;

	/**
	 * The Y component of this 2D vector.
	 */
	public float y = 0.0F;



	/**
	 * Initialises a new instance of the {@link zoranodensha.api.util.Vec2} class.
	 */
	public Vec2()
	{

	}

	/**
	 * Initialises a new instance of the {@link zoranodensha.api.util.Vec2} class.
	 * 
	 * @param x - The starting X component of this 2D vector.
	 * @param y - The starting Y component of this 2D vector.
	 */
	public Vec2(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString()
	{
		return String.format("{%f, %f}", x, y);
	}

	/**
	 * Returns this 2D vector in {@code String} format.
	 * 
	 * @param rounding - How many digits to round the components to. For example, if this is set to {@code 2}, the returned result would look like {@code {0.45, -6.43}}.
	 * @return - A string representing this 2D vector.
	 */
	public String toString(int rounding)
	{
		if (rounding >= 0)
		{
			return String.format("{%f." + rounding + ", %f." + rounding + "}", x, y);
		}

		return this.toString();
	}
}
