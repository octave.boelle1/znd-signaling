package zoranodensha.api.items;

import net.minecraft.item.ItemStack;


/**
 * Items that behave like default track parts such as rails or track beds should
 * implement this interface.
 */
public interface IItemTrackPart
{
	/**
	 * Return the {@link ETrackPart} name of this Item for default track parts. If
	 * this Item is not a default track part and/ or should not be regarded as equal
	 * to others, return null.
	 *
	 * @param itemStack - The current ItemStack this Item is in.
	 * @return The corresponding ETrackPart or null if this item can not be regarded
	 *         as equal to others.
	 */
	public ETrackPart type(ItemStack itemStack);


	/**
	 * Numerous types of track parts used to identify different track parts as the
	 * same, e.g. to label Railcraft's ties and Zora no Densha's ties as equal.
	 */
	public enum ETrackPart
	{
		/** This Item represents switch blades. */
		BLADES,
		/** This Item represents buffers. */
		BUFFER,
		/** This Item represents fastenings. */
		FASTENING,
		/** This Item represents a trackside magnet. */
		MAGNET,
		/** This Item represents a switch motor. */
		MOTOR,
		/** This Item represents a crossing plate. */
		PLATE,
		/** This Item represents rails. */
		RAIL,
		/** This Item represents a track bed. */
		TRACKBED;
	}
}
