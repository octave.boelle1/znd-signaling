package zoranodensha.api.vehicles.handlers;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import zoranodensha.api.util.APILogger;
import zoranodensha.api.util.NBTUtil;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.handlers.CollisionHandler.EntitySelectorTrains;
import zoranodensha.api.vehicles.part.type.APartTypeCoupling;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.type.PartTypeCab;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.api.vehicles.part.util.PartHelper;



/**
 * Default coupling handler class, called to (dis-)connect {@link zoranodensha.api.vehicles.Train trains}.<br>
 * <br>
 * This class handles:<br>
 * > Monitoring front/back directions<br>
 * > Establishing and dissolving coupler connections
 * 
 * @author Leshuwa Kaiheiwa
 */
public class CouplingHandler
{
	/** Minimum distance that a coupling's bounding box expands in order to check for coupler connections. */
	public static final float MINIMUM_EXPANSION_FACTOR = 1.000F;

	/** A reference to the currently set coupling handler instance. */
	public static CouplingHandler INSTANCE = new CouplingHandler();



	/**
	 * Checks whether any of the given train's coupler connections have gone invalid.<br>
	 * Splits the train at the first coupler pair with invalid connection.
	 * 
	 * @param train - {@link zoranodensha.api.vehicles.Train Train} whose connections shall be checked.
	 */
	public void ensureCouplersConnect(Train train)
	{
		VehicleData prevVehicle = null;

		/* For each vehicle in the train.. */
		for (VehicleData currVehicle : train)
		{
			/* ..check whether its connection to the front vehicle is still valid. */
			if (prevVehicle != null && getHasBroken(currVehicle.getCouplingFront(), prevVehicle.getCouplingBack()))
			{
				/* If the connection was successfully dissolved, break the loop to avoid concurrent list modification. */
				if (tryDecouple(currVehicle.getCouplingFront()))
				{
					break;
				}
			}

			prevVehicle = currVehicle;
		}
	}

	/**
	 * Returns whether the given coupler pair is considered broken.<br>
	 * A coupler pair breaks when the in-world bounding boxes, expanded by each respective coupler's length, don't intersect.
	 * 
	 * @return {@code true} if the coupler pair has broken.
	 */
	public boolean getHasBroken(APartTypeCoupling a, APartTypeCoupling b)
	{
		float length = Math.max(a.getExpansion(), MINIMUM_EXPANSION_FACTOR);
		AxisAlignedBB aabb_a = a.getParent().getBoundingBox().expand(length, length, length);

		length = Math.max(b.getExpansion(), MINIMUM_EXPANSION_FACTOR);
		AxisAlignedBB aabb_b = b.getParent().getBoundingBox().expand(length, length, length);

		return !aabb_a.intersectsWith(aabb_b);
	}
	
	/**
	 * Returns whether the coupler pair is close enough to physically couple.
	 *
	 * @return {@code true} if the coupler pair is almost touching and ready to couple.
	 */
	public boolean getCloseEnoughToCouple(APartTypeCoupling a, APartTypeCoupling b)
	{
		float length = MINIMUM_EXPANSION_FACTOR;
		AxisAlignedBB aabb_a = a.getParent().getBoundingBox().expand(length, length, length);
		AxisAlignedBB aabb_b = b.getParent().getBoundingBox().expand(length, length, length);
		
		return aabb_a.intersectsWith(aabb_b);
	}

	/**
	 * <p>
	 * Searches for either coupling at the given train's end which is able to connect to the given coupling.<br>
	 * The ability to connect is determined by a symmetrical call to {@link zoranodensha.api.vehicles.part.type.APartTypeCoupling#getMayConnect(APartTypeCoupling) getMayConnect()}.
	 * </p>
	 *
	 * <p>
	 * This method assumes that:
	 * <ul>
	 * <li>the given coupling has a {@link zoranodensha.api.vehicles.part.type.APartType#getParent() parent} reference</li>
	 * <li>the train contains at least one vehicle</li>
	 * </ul>
	 * </p>
	 * 
	 * @param coupling - {@link zoranodensha.api.vehicles.part.type.APartTypeCoupling Coupling} to test for.
	 * @param train - {@link zoranodensha.api.vehicles.Train Train} whose front and back couplings are checked.
	 * @return A coupling at either end of the given train, able to connect to the given coupling, or {@code null} if none such was found.
	 */
	public APartTypeCoupling getMatchingCoupling(APartTypeCoupling coupling, Train train)
	{
		APartTypeCoupling couplingTrainF = train.getVehicleAt(0).getCouplingFront();
		APartTypeCoupling couplingTrainB = train.getVehicleAt(train.getVehicleCount() - 1).getCouplingBack();

		if (coupling.getMayConnect(couplingTrainF) && couplingTrainF.getMayConnect(coupling))
		{
			return couplingTrainF;
		}
		else if (coupling.getMayConnect(couplingTrainB) && couplingTrainB.getMayConnect(coupling))
		{
			return couplingTrainB;
		}

		return null;
	}

	/**
	 * Method to run permission checks and other pre-requisites before actually trying to couple
	 * {@link zoranodensha.api.vehicles.part.type.APartTypeCoupling couplers}.
	 * 
	 * @return {@code true} if both couplers may connect to each other.
	 */
	public boolean getMayCouple(APartTypeCoupling caller, APartTypeCoupling other)
	{
		/*
		 * If both couplers are part of the same train, there's nothing to do.
		 */
		Train callerTrain = caller.getTrain();
		Train otherTrain = other.getTrain();
		if (caller == other)
		{
			return false;
		}

		/*
		 * Do not couple if there are two active cabs that can't be overridden.
		 */
		PartTypeCab callerActiveCab = callerTrain.getActiveCab();
		PartTypeCab otherActiveCab = otherTrain.getActiveCab();
		if (callerActiveCab != null && otherActiveCab != null)
		{
			if (!callerActiveCab.getMayOverrideActive() && !otherActiveCab.getMayOverrideActive())
			{
				return false;
			}
		}

		/*
		 * Finally let the couplers decide whether they will connect to each other.
		 */
		return caller.getMayConnect(other) && other.getMayConnect(caller);
	}

	/**
	 * Splits the given train at the given coupling's connection.
	 * 
	 * @param coupling - {@link zoranodensha.api.vehicles.part.type.APartTypeCoupling Coupling} to split the train at.
	 * @return A newly created split train, or {@code null} if unsuccessful.
	 */
	public Train getSplitTrain(APartTypeCoupling coupling)
	{
		/* Retrieve the coupling's parent train and verify it exists. */
		Train couplingTrain = coupling.getTrain();
		if (couplingTrain == null)
		{
			throw new IllegalArgumentException("Received coupling without a train reference!");
		}

		/* We can't split the train if it consists of less than two vehicles. */
		int vehicleCount = couplingTrain.getVehicleCount();
		if (vehicleCount < 2)
		{
			return null;
		}

		/*
		 * Determine length of each train half.
		 */
		VehicleData couplingVehicle = coupling.getVehicle();
		int couplingVehicleIndex = couplingTrain.getVehicleIndex(couplingVehicle);
		int frontTrainLength;
		int backTrainLength;

		if (coupling == couplingVehicle.getCouplingFront())
		{
			backTrainLength = vehicleCount - couplingVehicleIndex;
			frontTrainLength = vehicleCount - backTrainLength;
		}
		else if (coupling == couplingVehicle.getCouplingBack())
		{
			backTrainLength = vehicleCount - couplingVehicleIndex - 1;
			frontTrainLength = vehicleCount - backTrainLength;
		}
		else
		{
			throw new IllegalArgumentException("Given coupling was neither coupling of its vehicle!");
		}

		/* Abort if either train would be empty. */
		if (frontTrainLength == 0 || backTrainLength == 0)
		{
			return null;
		}

		/*
		 * Split the shorter train off the parent train and return.
		 */
		Train newTrain = new Train(couplingTrain.worldObj);
		{
			if (frontTrainLength < backTrainLength)
			{
				for (VehicleData vehicle; frontTrainLength > 0; --frontTrainLength)
				{
					vehicle = couplingTrain.getVehicleAt(0);
					if (!couplingTrain.removeVehicle(vehicle))
					{
						APILogger.warn(this, String.format("Couldn't remove vehicle from train! V=%s T=%s", vehicle, couplingTrain));
						return null;
					}

					newTrain.addVehicle(vehicle);
				}
			}
			else
			{
				for (VehicleData vehicle; backTrainLength > 0; --backTrainLength)
				{
					vehicle = couplingTrain.getVehicleAt(couplingTrain.getVehicleCount() - 1);
					if (!couplingTrain.removeVehicle(vehicle))
					{
						APILogger.warn(this, String.format("Couldn't remove vehicle from train! V=%s T=%s", vehicle, couplingTrain));
						return null;
					}

					newTrain.addVehicle(vehicle);
				}
			}
		}
		return newTrain;
	}

	/**
	 * Merges the given trains into one.<br>
	 * The other train is guaranteed to have been merged into the caller train upon successful completion.
	 * 
	 * @param callerTrain - {@link zoranodensha.api.vehicles.Train Train} that will be merged into.
	 * @param otherTrain - Train to merge.
	 * @return {@code true} if the merge was successful.
	 */
	public boolean mergeTrains(Train callerTrain, Train otherTrain)
	{
		/*
		 * Prepare backup NBT tags so both trains can be reverted in case of failure.
		 */
		NBTTagCompound callerTrainNBT = mergeTrains_createBackup(callerTrain);
		NBTTagCompound otherTrainNBT = mergeTrains_createBackup(otherTrain);
		if (!NBTUtil.getIsTotalSizeValid(callerTrainNBT, otherTrainNBT))
		{
			APILogger.info(this, String.format("Refusing to merge two trains whose total NBT size would be too large.. [C=%s O=%s]", callerTrain, otherTrain));
			return false;
		}

		/*
		 * Try to merge the given trains.
		 */
		boolean isSuccessful = true;
		{
			/* First, determine which side of the other train we start with. */
			APartTypeCoupling couplingOtherF = otherTrain.getVehicleAt(0).getCouplingFront();
			APartTypeCoupling couplingOtherB = otherTrain.getVehicleAt(otherTrain.getVehicleCount() - 1).getCouplingBack();

			// FIXME @Leshuwa - Matching coupling differs from the coupling selected in Train#addVehicle() ?!
			// FIXME @Leshuwa - Compare values used to compute the matching coupling BEFORE and AFTER adding the vehicle to the train. <<<<<
			if (getMatchingCoupling(couplingOtherF, callerTrain) != null)
			{
				System.out.println(getMatchingCoupling(couplingOtherF, callerTrain)); // XXX REMOVE DEBUG
				System.out.println(couplingOtherF); // XXX REMOVE DEBUG
				System.out.println(); // XXX REMOVE DEBUG

				/* Iterate from front to back through the other train and add its vehicles to the caller train. */
				while (otherTrain.getHasVehicles() && isSuccessful)
				{
					VehicleData otherVehicle = otherTrain.getVehicleAt(0);
					isSuccessful = otherTrain.removeVehicle(otherVehicle) && callerTrain.addVehicle(otherVehicle);
				}
			}
			else if (getMatchingCoupling(couplingOtherB, callerTrain) != null)
			{
				System.out.println(getMatchingCoupling(couplingOtherB, callerTrain)); // XXX REMOVE DEBUG
				System.out.println(couplingOtherB); // XXX REMOVE DEBUG
				System.out.println(); // XXX REMOVE DEBUG

				/* Iterate from back to front through the other train and add its vehicles to the caller train. */
				while (otherTrain.getHasVehicles() && isSuccessful)
				{
					VehicleData otherVehicle = otherTrain.getVehicleAt(otherTrain.getVehicleCount() - 1);
					isSuccessful = otherTrain.removeVehicle(otherVehicle) && callerTrain.addVehicle(otherVehicle);
				}
			}
			else
			{
				/* If neither coupling could've connected to either end of the caller train, they shouldn't merge anyways. Abort. */
				return false;
			}
		}

		/*
		 * If the merge operation failed, do a rollback of both trains.
		 */
		if (!isSuccessful)
		{
			APILogger.warn(this, "Method mergeTrains() failed. Please notify the author about this. Reverting trains..");
			if (mergeTrains_rollback(callerTrainNBT, callerTrain) && mergeTrains_rollback(otherTrainNBT, otherTrain))
			{
				APILogger.warn(this, "..successfully reverted both trains.");
			}
			else
			{
				APILogger.warn(this, "..failed to revert one or more trains!");
			}
			return false;
		}

		return true;
	}

	/**
	 * Helper method to create a backup NBT tag for the given train.
	 *
	 * @param train - {@link zoranodensha.api.vehicles.Train Train} to create a backup NBT tag of.
	 * @return The {@link net.minecraft.nbt.NBTTagCompound NBT tag} containing the given train's save data.
	 */
	private NBTTagCompound mergeTrains_createBackup(Train train)
	{
		NBTTagCompound nbt = new NBTTagCompound();
		train.writeToNBT(nbt);
		return nbt;
	}

	/**
	 * Helper method that spawns a rolled-back train parsed from the given NBT tag.
	 *
	 * @param backupNBT - {@link net.minecraft.nbt.NBTTagCompound NBT tag} containing the backed-up train data.
	 * @param train - {@link zoranodensha.api.vehicles.Train Train} to rollback.
	 * @return {@code true} if the train's rolled-back version was successfully spawned.
	 */
	private boolean mergeTrains_rollback(NBTTagCompound backupNBT, Train train)
	{
		/* Kill train and override reference to a new instance with same world object. */
		train.setDead();
		train = new Train(train.worldObj);
		train.readFromNBT(backupNBT);

		/* Force train spawn in the world. */
		train.forceSpawn = true;
		boolean isSpawnSuccessful = train.worldObj.spawnEntityInWorld(train);
		train.forceSpawn = false;
		return isSpawnSuccessful;
	}

	/**
	 * Called to search for nearby couplings and to initiate interaction if possible.
	 *
	 * @param callerCoupling - The {@link zoranodensha.api.vehicles.part.type.APartTypeCoupling coupler} that is searching the area.
	 * @return {@code true} if a connection could be established successfully.
	 */
	public boolean tryCouple(APartTypeCoupling callerCoupling)
	{
		/*
		 * Ensure there is no pre-existing connection.
		 */
		if (callerCoupling.getIsCoupled())
		{
			return false;
		}

		Train callerTrain = callerCoupling.getParent().getTrain();

		/*
		 * Check for each nearby train whether there's a coupling that connects to our coupling. If so, try to merge trains.
		 */
		CopyOnWriteArrayList<Entity> list = CollisionHandler.INSTANCE.getCollidingEntities(callerTrain.worldObj, callerCoupling.getParent().getBoundingBox().expand(1, 1, 1), new EntitySelectorTrains(callerTrain));
		if (!list.isEmpty())
		{
			/* For each train in the list.. */
			for (Entity entity : list)
			{
				Train otherTrain = (Train)entity;

				/* ..retrieve a coupling that matches our coupling.. */
				APartTypeCoupling otherCoupling = getMatchingCoupling(callerCoupling, otherTrain);
				if (otherCoupling == null || !getMayCouple(callerCoupling, otherCoupling))
				{
					continue;
				}

				/* ..check whether any train ever moved and, if so, whether the trains moved towards each other.. */
				if (callerTrain.getLastSpeedNonZero() != 0.0)
				{
					/* Get current and next position of caller train's coupling. */
					Vec3 posCurr, posNext;
					{
						PartTypeBogie bogie = callerCoupling.getVehicle().getTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY).getClosestPartType(callerCoupling.getParent().getOffset()[0]);
						if (bogie == null)
						{
							continue;
						}

						double movementDistance = Math.copySign(0.1, callerTrain.getLastSpeedNonZero());
						Vec3 bogieDirection = bogie.getDirection().normalize();

						posCurr = PartHelper.getPosition(callerCoupling.getParent());
						posNext = posCurr.addVector(bogieDirection.xCoord * movementDistance, bogieDirection.yCoord * movementDistance, bogieDirection.zCoord * movementDistance);
					}

					/* Merge if the caller train gets closer to the other. */
					Vec3 posRef = PartHelper.getPosition(otherCoupling.getParent());
					if (posNext.squareDistanceTo(posRef) <= posCurr.squareDistanceTo(posRef))
					{
						return mergeTrains(callerTrain, otherTrain);
					}
				}
				else if (otherTrain.getLastSpeedNonZero() != 0.0)
				{
					/* Get current and next position of other train's coupling. */
					Vec3 posCurr, posNext;
					{
						PartTypeBogie bogie = otherCoupling.getVehicle().getTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY).getClosestPartType(otherCoupling.getParent().getOffset()[0]);
						if (bogie == null)
						{
							continue;
						}

						double movementDistance = Math.copySign(0.1, otherTrain.getLastSpeedNonZero());
						Vec3 bogieDirection = bogie.getDirection().normalize();

						posCurr = PartHelper.getPosition(otherCoupling.getParent());
						posNext = posCurr.addVector(bogieDirection.xCoord * movementDistance, bogieDirection.yCoord * movementDistance, bogieDirection.zCoord * movementDistance);
					}

					/* Merge if the other train gets closer to the caller train. */
					Vec3 posRef = PartHelper.getPosition(callerCoupling.getParent());
					if (posNext.squareDistanceTo(posRef) <= posCurr.squareDistanceTo(posRef))
					{
						return mergeTrains(callerTrain, otherTrain);
					}
				}
				else
				{
					/* ..or if neither train moved, just merge them. */
					return mergeTrains(callerTrain, otherTrain);
				}
			}
		}

		return false;
	}

	/**
	 * Dissolves the coupler connection of the given coupler, splitting the train.
	 * 
	 * @param coupling - {@link zoranodensha.api.vehicles.part.type.APartTypeCoupling Coupling} in question.
	 * @return {@code true} if this coupler's connection was cleared.
	 */
	public boolean tryDecouple(APartTypeCoupling coupling)
	{
		/* Ensure there is a connection to dissolve. */
		if (!coupling.getIsCoupled())
		{
			return false;
		}

		/* Create backup data of the coupler's parent train in case of failure. */
		NBTTagCompound trainNBT = new NBTTagCompound();
		Train train = coupling.getTrain();
		train.writeToNBT(trainNBT);

		/*
		 * Split the train at the given coupler's position.
		 */
		Train ret = getSplitTrain(coupling);
		if (ret == null || ret == train)
		{
			return false;
		}

		/* Force-spawn the train, ensuring it actually spawns. */
		boolean isSuccessful;
		{
			ret.forceSpawn = true;
			ret.updatePosition();
			isSuccessful = train.worldObj.spawnEntityInWorld(ret);
			ret.forceSpawn = false;
		}

		/* If the spawn failed, revert the old train and destroy the new train. */
		if (!isSuccessful)
		{
			APILogger.warn(this, "Method tryDecouple() failed. Please notify the author about this. Reverting train..");
			ret.setDead();
			train.readFromNBT(trainNBT);
		}

		return isSuccessful;
	}

	/**
	 * Call to globally override the coupling handler.
	 *
	 * @param handler - The new CouplingHandler to set.
	 */
	public static final void overrideHandler(CouplingHandler handler)
	{
		INSTANCE = handler;
	}
}