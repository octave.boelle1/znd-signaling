package zoranodensha.api.vehicles.packet;

import io.netty.buffer.ByteBuf;



public class PropertyMessage_Generic extends APropertyMessage<Object>
{
	public PropertyMessage_Generic()
	{
	}


	public PropertyMessage_Generic(int entityID, int vehicleID, int partID, byte key)
	{
		super(entityID, vehicleID, partID, key, null);
	}

	@Override
	public Object fromByteBuf(ByteBuf bbuf)
	{
		return null;
	}

	@Override
	public void toByteBuf(ByteBuf bbuf)
	{
	}



	public static class Handler extends APropertyMessage.Handler<PropertyMessage_Generic>
	{
	}
}
