package zoranodensha.api.vehicles.packet;

import io.netty.buffer.ByteBuf;



public class PropertyMessage_Short extends APropertyMessage<Short>
{
	public PropertyMessage_Short()
	{
	}


	public PropertyMessage_Short(int entityID, int vehicleID, int partID, byte key, Short newVal)
	{
		super(entityID, vehicleID, partID, key, newVal);
	}

	@Override
	public Short fromByteBuf(ByteBuf bbuf)
	{
		return bbuf.readShort();
	}

	@Override
	public void toByteBuf(ByteBuf bbuf)
	{
		bbuf.writeShort(newVal);
	}



	public static class Handler extends APropertyMessage.Handler<PropertyMessage_Short>
	{
	}
}
