package zoranodensha.api.vehicles.packet;

import io.netty.buffer.ByteBuf;



public class PropertyMessage_Double extends APropertyMessage<Double>
{
	public PropertyMessage_Double()
	{
	}


	public PropertyMessage_Double(int entityID, int vehicleID, int partID, byte key, Double newVal)
	{
		super(entityID, vehicleID, partID, key, newVal);
	}

	@Override
	public Double fromByteBuf(ByteBuf bbuf)
	{
		return bbuf.readDouble();
	}

	@Override
	public void toByteBuf(ByteBuf bbuf)
	{
		bbuf.writeDouble(newVal);
	}



	public static class Handler extends APropertyMessage.Handler<PropertyMessage_Double>
	{
	}
}
