package zoranodensha.api.vehicles.packet;

import io.netty.buffer.ByteBuf;



public class PropertyMessage_Char extends APropertyMessage<Character>
{
	public PropertyMessage_Char()
	{
	}


	public PropertyMessage_Char(int entityID, int vehicleID, int partID, byte key, Character newVal)
	{
		super(entityID, vehicleID, partID, key, newVal);
	}

	@Override
	public Character fromByteBuf(ByteBuf bbuf)
	{
		return bbuf.readChar();
	}

	@Override
	public void toByteBuf(ByteBuf bbuf)
	{
		bbuf.writeChar(newVal);
	}



	public static class Handler extends APropertyMessage.Handler<PropertyMessage_Char>
	{
	}
}
