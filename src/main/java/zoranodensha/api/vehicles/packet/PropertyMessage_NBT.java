package zoranodensha.api.vehicles.packet;

import cpw.mods.fml.common.network.ByteBufUtils;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;



public class PropertyMessage_NBT extends APropertyMessage<NBTTagCompound>
{
	public PropertyMessage_NBT()
	{
	}


	public PropertyMessage_NBT(int entityID, int vehicleID, int partID, byte key, NBTTagCompound newVal)
	{
		super(entityID, vehicleID, partID, key, newVal);
	}

	@Override
	public NBTTagCompound fromByteBuf(ByteBuf bbuf)
	{
		return ByteBufUtils.readTag(bbuf);
	}

	@Override
	public void toByteBuf(ByteBuf bbuf)
	{
		ByteBufUtils.writeTag(bbuf, newVal);
	}



	public static class Handler extends APropertyMessage.Handler<PropertyMessage_NBT>
	{
	}
}
