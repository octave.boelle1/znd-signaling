package zoranodensha.api.vehicles.packet;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityTracker;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.WorldServer;
import zoranodensha.api.util.APILogger;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.util.VehicleHelper;



/**
 * Generic base class for property messages.<br>
 * All default messages in this package are registered by Zora no Densha bi-directionally, i.e. to server <b>and</b> client side.
 *
 * @param <TYPE> The type of this message. Means to write to/ read from a {@link ByteBuf} byte buffer need to be implemented.
 * 
 * @author Leshuwa Kaiheiwa
 */
public abstract class APropertyMessage<TYPE> implements IPropertyMessage<TYPE>
{
	/** A reference to the {@link cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper packet channel} that can be used to send property packets on. May be {@code null}. */
	public static SimpleNetworkWrapper NETWORK_ACCESS;
	/** Separate thread used to synchronise data from server to client. May be {@code null}. */
	private static SyncThread_ToClient syncThread_toClient;

	/** The numerical entity ID as retrieved by {@link net.minecraft.entity.Entity#getEntityId() getEntityId()}. */
	int entityID;
	/** The part ID, as retrieved by {@link zoranodensha.api.vehicles.part.VehParBase#getID() getID()}. May be {@code 0}. */
	int partID;
	/** The part ID, as retrieved by {@link zoranodensha.api.vehicles.VehicleData#getID() getID()}. May be {@code 0}. */
	int vehicleID;
	/** The packet ID; should be unique to your vehicle part type hierarchy. */
	byte key;
	/** The new value. */
	TYPE newVal;



	public APropertyMessage()
	{
	}


	/**
	 * Construct a new message.
	 */
	public APropertyMessage(int entityID, int vehicleID, int partID, byte key, TYPE newVal)
	{
		this.entityID = entityID;
		this.vehicleID = vehicleID;
		this.partID = partID;
		this.key = key;
		this.newVal = newVal;
	}

	/**
	 * Called to read this object's {@code newVal} field from the given {@link ByteBuf} instance.
	 */
	public abstract TYPE fromByteBuf(ByteBuf bbuf);

	@Override
	public void fromBytes(ByteBuf bbuf)
	{
		entityID = bbuf.readInt();
		partID = bbuf.readInt();
		key = bbuf.readByte();
		newVal = fromByteBuf(bbuf);
	}

	@Override
	public int getEntityID()
	{
		return entityID;
	}

	@Override
	public byte getKey()
	{
		return key;
	}

	@Override
	public TYPE getNewVal()
	{
		return newVal;
	}

	@Override
	public int getPartID()
	{
		return partID;
	}
	
	@Override
	public int getVehicleID()
	{
		return vehicleID;
	}

	/**
	 * Called to write this object's {@code newVal} field to the given {@link ByteBuf} instance.
	 */
	public abstract void toByteBuf(ByteBuf bbuf);

	@Override
	public void toBytes(ByteBuf bbuf)
	{
		bbuf.writeInt(entityID);
		bbuf.writeInt(partID);
		bbuf.writeByte(key);
		toByteBuf(bbuf);
	}

	/**
	 * Called by Zora no Densha to initialise the {@link #NETWORK_ACCESS} field and to register all property packets.O
	 * 
	 * @return The last used packet ID.
	 * @throws IllegalArgumentException If the {@link #NETWORK_ACCESS} has already been initialised.
	 */
	public static int init(SimpleNetworkWrapper snw) throws IllegalArgumentException
	{
		/* Ensure the Network Wrapper isn't already initialised by foreign sources. */
		if (NETWORK_ACCESS != null)
		{
			throw new IllegalArgumentException("Network Wrapper already initialised! This is a mod issue.");
		}

		/* Assign Network Wrapper field and register all messages on both sides. */
		NETWORK_ACCESS = snw;
		int packetID = 0;

		for (Side side : Side.values())
		{
			snw.registerMessage(PropertyMessage_Boolean.Handler.class, PropertyMessage_Boolean.class, ++packetID, side);
			snw.registerMessage(PropertyMessage_Byte.Handler.class, PropertyMessage_Byte.class, ++packetID, side);
			snw.registerMessage(PropertyMessage_Char.Handler.class, PropertyMessage_Char.class, ++packetID, side);
			snw.registerMessage(PropertyMessage_Double.Handler.class, PropertyMessage_Double.class, ++packetID, side);
			snw.registerMessage(PropertyMessage_Float.Handler.class, PropertyMessage_Float.class, ++packetID, side);
			snw.registerMessage(PropertyMessage_Generic.Handler.class, PropertyMessage_Generic.class, ++packetID, side);
			snw.registerMessage(PropertyMessage_Int.Handler.class, PropertyMessage_Int.class, ++packetID, side);
			snw.registerMessage(PropertyMessage_Long.Handler.class, PropertyMessage_Long.class, ++packetID, side);
			snw.registerMessage(PropertyMessage_NBT.Handler.class, PropertyMessage_NBT.class, ++packetID, side);
			snw.registerMessage(PropertyMessage_Short.Handler.class, PropertyMessage_Short.class, ++packetID, side);
		}

		return packetID;
	}

	/**
	 * Helper method to send a {@link cpw.mods.fml.common.network.simpleimpl.IMessage message} to the server.<br>
	 * Does <b>not</b> do a remote check.
	 */
	public static final void sendToServer(IMessage message)
	{
		APropertyMessage.NETWORK_ACCESS.sendToServer(message);
	}

	/**
	 * Helper method to send a {@link cpw.mods.fml.common.network.simpleimpl.IMessage message}
	 * as Packet to all clients tracking the given entity.<br>
	 * Does a remote check first to ensure server-sided operation.
	 */
	public static final void sendToTracking(Entity entity, IMessage message)
	{
		if (!entity.worldObj.isRemote)
		{
			/* Initialise thread if required. */
			if (syncThread_toClient == null || !syncThread_toClient.isAlive())
			{
				syncThread_toClient = new SyncThread_ToClient();
				syncThread_toClient.start();
			}

			/* Place message in queue. */
			syncThread_toClient.sendToTracking(entity, message);
		}
	}



	/**
	 * Handler class for this message.
	 */
	public static class Handler<CLSS extends APropertyMessage<?>> implements IMessageHandler<CLSS, IMessage>
	{
		@Override
		public IMessage onMessage(CLSS message, MessageContext context)
		{
			try
			{
				Entity entity = VehicleHelper.getPlayerFromContext(context).worldObj.getEntityByID(message.getEntityID());
				if (entity instanceof Train)
				{
					((Train)entity).handlePropertyMessage(message);
				}
				else if (entity != null)
				{
					// @formatter:off
					APILogger.warn(message, String.format("Dropping mismatched packet. (Entity ID: %d;  Entity: %s;  Part ID: %d;  Key: %d;  Value: %s)",
					                                      message.entityID,
					                                      entity,
					                                      message.partID,
					                                      message.key,
					                                      (message.newVal != null ? message.newVal.toString() : "~NULL~")));
					// @formatter:on
				}
			}
			catch (Exception ex)
			{
				APILogger.catching(ex);
			}
			return null;
		}
	}


	/**
	 * Thread used to synchronise packets to clients without slowing server-sided calculation.
	 */
	public static class SyncThread_ToClient extends Thread
	{
		public static final ArrayList<Entry<Entity, IMessage>> placedBuffer = new ArrayList<Entry<Entity, IMessage>>();
		private static final ArrayList<Entry<Entity, IMessage>> readBuffer = new ArrayList<Entry<Entity, IMessage>>();



		public SyncThread_ToClient()
		{
			super("Zora no Densha ClientSyncThread");
		}

		/**
		 * Distributes all messages from the {@link #readBuffer} to all clients.
		 */
		private void flush()
		{
			while (!readBuffer.isEmpty())
			{
				Entry<Entity, IMessage> e = readBuffer.remove(0);
				Entity entity = e.getKey();
				IMessage message = e.getValue();
				if (entity == null || message == null)
				{
					continue;
				}

				EntityTracker entityTracker = ((WorldServer)e.getKey().worldObj).getEntityTracker();
				if (entityTracker != null)
				{
					Set<EntityPlayer> trackingPlayers = entityTracker.getTrackingPlayers(entity);
					for (EntityPlayer player : trackingPlayers)
					{
						APropertyMessage.NETWORK_ACCESS.sendTo(message, (EntityPlayerMP)player);
					}
				}
			}
		}

		@Override
		public void run()
		{
			while (true)
			{
				try
				{
					/* Wait for messages to be placed in the buffer. Move them over once notified. */
					synchronized (placedBuffer)
					{
						placedBuffer.wait();
						readBuffer.addAll(placedBuffer);
						placedBuffer.clear();
					}

					/* Flush messages and go back to waiting state. */
					flush();
				}
				catch (InterruptedException e)
				{
					;
				}
			}
		}

		/**
		 * Send the given message to all clients tracking the given entity.<br>
		 * Actually creates a new entry and enqueues it in the {@link #placedBuffer}.
		 * 
		 * @param entity - The tracked {@link net.minecraft.entity.Entity entity}.
		 * @param message - {@link cpw.mods.fml.common.network.simpleimpl.IMessage Message} to send.
		 */
		public void sendToTracking(Entity entity, IMessage message)
		{
			/* Apply lock to #placedBuffer to avoid concurrent modification. */
			synchronized (placedBuffer)
			{
				placedBuffer.add(new AbstractMap.SimpleEntry<Entity, IMessage>(entity, message));
				placedBuffer.notify();
			}
		}
	}
}
