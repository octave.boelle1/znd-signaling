package zoranodensha.api.vehicles.packet;

import io.netty.buffer.ByteBuf;



public class PropertyMessage_Long extends APropertyMessage<Long>
{
	public PropertyMessage_Long()
	{
	}


	public PropertyMessage_Long(int entityID, int vehicleID, int partID, byte key, Long newVal)
	{
		super(entityID, vehicleID, partID, key, newVal);
	}

	@Override
	public Long fromByteBuf(ByteBuf bbuf)
	{
		return bbuf.readLong();
	}

	@Override
	public void toByteBuf(ByteBuf bbuf)
	{
		bbuf.writeLong(newVal);
	}



	public static class Handler extends APropertyMessage.Handler<PropertyMessage_Long>
	{
	}
}
