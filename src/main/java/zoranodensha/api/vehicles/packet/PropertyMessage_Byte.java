package zoranodensha.api.vehicles.packet;

import io.netty.buffer.ByteBuf;



public class PropertyMessage_Byte extends APropertyMessage<Byte>
{
	public PropertyMessage_Byte()
	{
	}


	public PropertyMessage_Byte(int entityID, int vehicleID, int partID, byte key, Byte newVal)
	{
		super(entityID, vehicleID, partID, key, newVal);
	}

	@Override
	public Byte fromByteBuf(ByteBuf bbuf)
	{
		return bbuf.readByte();
	}

	@Override
	public void toByteBuf(ByteBuf bbuf)
	{
		bbuf.writeByte(newVal);
	}



	public static class Handler extends APropertyMessage.Handler<PropertyMessage_Byte>
	{
	}
}
