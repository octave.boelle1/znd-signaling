package zoranodensha.api.vehicles.event.vehicle;

import java.util.ArrayList;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;



/**
 * Fired when a {@link VehicleData vehicle} is about to be dropped due to excess damage.<br>
 * Set to be fired in {@link Train#attackEntityFrom(net.minecraft.util.DamageSource, float) attackEntityFrom()}.<br>
 * <br>
 * This event is {@link Cancelable}.<br>
 * <br>
 * If this event is canceled, the RailVehicle won't drop.<br>
 * <br>
 * This event has a result and will pass only if it's set to @ {@link Result#ALLOW}. {@link HasResult}<br>
 * <br>
 * This event is fired on the {@link MinecraftForge#EVENT_BUS}.
 */
@Cancelable
@Event.HasResult
public class VehicleDropEvent extends AVehicleEvent
{
	/** The {@link ItemStack}s that should be dropped. */
	public final ArrayList<ItemStack> results = new ArrayList<ItemStack>();



	public VehicleDropEvent(Train train, VehicleData vehicle)
	{
		super(train, vehicle);
	}
}
