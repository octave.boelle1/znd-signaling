package zoranodensha.api.vehicles.event.vehicle;

import cpw.mods.fml.common.eventhandler.Cancelable;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.event.train.TrainUpdateEvent;

/**
 * Fired after a {@link Train}'s {@link VehicleData vehicles} list has changed.<br>
 * More formally, fires if after running a train's update the train's vehicle list had been marked as changed.<br>
 * <br>
 * This event is not {@link Cancelable}.<br>
 * <br>
 * This event does not have a result. {@link HasResult}<br>
 * <br>
 * This event is fired on the {@link MinecraftForge#EVENT_BUS}.
 */
public class VehicleListChangedEvent extends TrainUpdateEvent
{
	public VehicleListChangedEvent(Train train)
	{
		super(train);
	}
}