package zoranodensha.api.vehicles.event.vehicle;

import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.event.train.ATrainEvent;



/**
 * Fired when something involving a {@link VehicleData vehicle} is about to happen.<br>
 * <br>
 * Events of this type are fired on the {@link MinecraftForge#EVENT_BUS}.
 * 
 * @author Leshuwa Kaiheiwa
 */
public abstract class AVehicleEvent extends ATrainEvent
{
	/** The vehicle in question. */
	public final VehicleData vehicle;



	public AVehicleEvent(Train train, VehicleData vehicle)
	{
		super(train);
		this.vehicle = vehicle;
	}
}