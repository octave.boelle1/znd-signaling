package zoranodensha.api.vehicles.event.living;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.DamageSource;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingEvent;
import zoranodensha.api.vehicles.Train;



/**
 * Fired when a {@link Train} is about to hit a {@link EntityLivingBase living entity}.<br>
 * Set to be fired in {@link zoranodensha.api.vehicles.handlers.CollisionHandler#onEntityCollision(Train, net.minecraft.entity.Entity) onEntityCollision()}.<br>
 * <br>
 * This event is {@link Cancelable}.<br>
 * <br>
 * If this event is canceled, the Entity is not hurt.<br>
 * <br>
 * This event has a result which needs to be set in order for this event to pass. {@link HasResult}<br>
 * <br>
 * This event is fired on the {@link MinecraftForge#EVENT_BUS}.
 */
@Cancelable
@Event.HasResult
public class LivingHitByTrainEvent extends LivingEvent
{
	/** {@link Train} inflicting the damage. */
	public final Train vehicle;
	/** The amount of damage inflicted. */
	public final float damage;
	/** The resulting {@link DamageSource} of this event. Has to be assigned non-{@code null} in order for the event to pass. */
	public DamageSource damageSource;



	/**
	 * Construct a new event used to determine the type of DamageSource to apply to the injured entity.
	 *
	 * @param vehicle - The {@link Train} that is about to hit the given entity.
	 * @param victim - The {@link net.minecraft.entity.Entity} that's about to be hit.
	 * @param damage - The amount of damage inflicted.
	 */
	public LivingHitByTrainEvent(Train vehicle, EntityLivingBase victim, float damage)
	{
		super(victim);

		this.vehicle = vehicle;
		this.damage = damage;
	}
}
