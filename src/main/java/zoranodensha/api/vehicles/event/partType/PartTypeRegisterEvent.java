package zoranodensha.api.vehicles.event.partType;

import java.util.ArrayList;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.vehicles.part.type.APartType;



/**
 * Fired to call for registration of {@link zoranodensha.api.vehicles.part.type.APartType part types}.<br>
 * Use this event to register your custom part types so they can be parsed by the
 * {@link zoranodensha.api.vehicles.part.xml.XMLPartFactory XML part factory}.<br>
 * <br>
 * This event is not {@link Cancelable}.<br>
 * <br>
 * This event does not have a result. {@link HasResult}<br>
 * <br>
 * This event is fired on the {@link MinecraftForge#EVENT_BUS}.
 */
public class PartTypeRegisterEvent extends Event
{
	/** List of all registered part types. */
	public final ArrayList<Class<? extends APartType>> partTypes;



	public PartTypeRegisterEvent(ArrayList<Class<? extends APartType>> partTypes)
	{
		this.partTypes = partTypes;
	}
}