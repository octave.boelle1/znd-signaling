package zoranodensha.api.vehicles;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import cpw.mods.fml.common.eventhandler.Event.Result;
import cpw.mods.fml.common.registry.IEntityAdditionalSpawnData;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import io.netty.buffer.ByteBuf;

import net.minecraft.client.Minecraft;
import net.minecraft.crash.CrashReport;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.ReportedException;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;

import zoranodensha.api.structures.signals.TrainPulse;
import zoranodensha.api.util.APILogger;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.util.NBTUtil;
import zoranodensha.api.util.SyncDir;
import zoranodensha.api.vehicles.boundingBox.IBoundingBoxProvider;
import zoranodensha.api.vehicles.boundingBox.VTBoundingBox;
import zoranodensha.api.vehicles.event.PlayerInteractTrainEvent;
import zoranodensha.api.vehicles.event.living.LivingKilledByTrainEvent;
import zoranodensha.api.vehicles.event.train.TrainDestroyedEvent;
import zoranodensha.api.vehicles.event.train.TrainFallEvent;
import zoranodensha.api.vehicles.event.train.TrainMiddleClickEvent;
import zoranodensha.api.vehicles.event.train.TrainUpdateEvent;
import zoranodensha.api.vehicles.event.train.TrainUpdateEvent.TrainUpdateSpeedEvent;
import zoranodensha.api.vehicles.event.vehicle.VehicleDamagedEvent;
import zoranodensha.api.vehicles.event.vehicle.VehicleDropEvent;
import zoranodensha.api.vehicles.event.vehicle.VehicleListChangedEvent;
import zoranodensha.api.vehicles.handlers.CollisionHandler;
import zoranodensha.api.vehicles.handlers.CouplingHandler;
import zoranodensha.api.vehicles.handlers.MovementHandler;
import zoranodensha.api.vehicles.handlers.PneumaticsHandler;
import zoranodensha.api.vehicles.packet.APropertyMessage;
import zoranodensha.api.vehicles.packet.IPropertyMessage;
import zoranodensha.api.vehicles.packet.PropertyMessage_NBT;
import zoranodensha.api.vehicles.packet.TrainMessage_UpdateVehicles;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.ITickableProperty;
import zoranodensha.api.vehicles.part.type.*;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Tickable;
import zoranodensha.api.vehicles.part.util.AVehiclePartRegistry;
import zoranodensha.api.vehicles.part.util.ConcatenatedPartsList;
import zoranodensha.api.vehicles.part.util.ConcatenatedTypesList;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.api.vehicles.part.util.OrderedPartsList.IPartsSelector;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.api.vehicles.part.util.OrderedTypesList.ITypesSelector;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.api.vehicles.util.VehicleHelper;


/**
 * Core functionality for rolling stock, responsible to initiate update actions.
 * <hr>
 *
 * <p>
 * A train consists of at least one {@link zoranodensha.api.vehicles.VehicleData vehicle}
 * which in turn contains at least one {@link zoranodensha.api.vehicles.VehicleSection section}.<br>
 * This means that any vehicle is also, technically, a train.<br>
 * </p>
 *
 * <p>
 * Trains can be coupled to each other, merging one train's data into the other train.
 * </p>
 *
 * <p>
 * Implements {@link java.lang.Iterable} for iteration over the train's vehicles.
 * Iteration order corresponds to the order of vehicles in the train.
 * </p>
 *
 * @author Leshuwa Kaiheiwa
 */
public class Train extends Entity implements IBoundingBoxProvider, IEntityAdditionalSpawnData, Iterable<VehicleData>
{
	/**
	 * Speed scaling factor. Used to scale the actual speed for display.
	 */
	private static float SPEED_SCALE_FACTOR = 1.0F;
	
	
	/*
	 * Final values
	 */
	/**
	 * All {@link zoranodensha.api.vehicles.VehicleData vehicles} in this train.
	 */
	private final VehicleList vehicles;
	
	
	/*
	 * Caches and cached values
	 */
	/**
	 * Total train mass, in metric tons (1t = 1000kg).
	 */
	private float mass;
	
	/**
	 * Overall train length, in metres.
	 */
	private float length;
	
	/**
	 * Train maximum speed, in km/h.
	 */
	private int maxSpeed;
	
	/**
	 * Current active cab, as defined per {@link #updateActiveCab()}.
	 */
	@SyncDir(ESyncDir.CLIENT)
	private PartTypeCab activeCab;
	
	/**
	 * The last speed of this train, in meters per tick [m/tick].
	 */
	@SyncDir(ESyncDir.CLIENT)
	private double lastSpeed;
	
	/**
	 * The last non-zero movement speed of this train, in meters per tick [m/tick]. Only zero ({@code 0.0}) if this train hasn't moved yet.
	 */
	@SyncDir(ESyncDir.NOSYNC)
	private double lastSpeedNonZero;
	
	
	/**
	 * Default constructor.
	 *
	 * @param world - This train's {@link net.minecraft.world.World world} object.
	 */
	public Train(World world)
	{
		super(world);
		
		setSize(1.0F, 1.0F);
		vehicles = new VehicleList(this);
	}
	
	/**
	 * Instantiates a new train from the given vehicle by creating a deep copy of the given vehicle.
	 *
	 * @param world   - This train's {@link net.minecraft.world.World world} object.
	 * @param vehicle - {@link zoranodensha.api.vehicles.VehicleData Vehicle} in this train.
	 * @throws IllegalArgumentException If {@code vehicle} is {@code null}.
	 */
	public Train(World world, @Nonnull VehicleData vehicle)
	{
		this(world);
		
		/* Create a copy of the given vehicle. */
		NBTTagCompound nbt = new NBTTagCompound();
		vehicle.writeToNBT(nbt, ETagCall.SAVE);
		VehicleData vehicleCopy = new VehicleData(nbt, ETagCall.SAVE);
		
		/* Reset vehicle offset and inversion, then add to this train. */
		vehicleCopy.popChanges();
		addVehicle(vehicleCopy);
	}
	
	
	/**
	 * <p>
	 * Adds the given vehicle to this train.
	 * </p>
	 *
	 * <p>
	 * Actually just hands this down to the private method of same name, as "external" calls to this method
	 * are assumed to always require updates of the vehicle's offset and inversion.
	 * </p>
	 *
	 * <p>
	 * <b>Note:</b>
	 * The given vehicle's {@link zoranodensha.api.vehicles.VehicleData#getOffset() offset} and {@link zoranodensha.api.vehicles.VehicleData#getIsInverse() inversion}
	 * may have been changed, even if this method returned {@code false}.
	 * </p>
	 *
	 * @see #addVehicle(VehicleData, boolean)
	 */
	public boolean addVehicle(VehicleData vehicle)
	{
		return addVehicle(vehicle, true);
	}
	
	/**
	 * <p>
	 * Adds the given vehicle to this train.
	 * </p>
	 *
	 * <p>
	 * <b>Note:</b>
	 * The given vehicle's {@link zoranodensha.api.vehicles.VehicleData#getOffset() offset} and {@link zoranodensha.api.vehicles.VehicleData#getIsInverse() inversion}
	 * may have been changed, even if this method returned {@code false}.
	 * </p>
	 *
	 * @param vehicle       - {@link zoranodensha.api.vehicles.VehicleData Vehicle} to add.
	 * @param updateOffsets - {@code true} to recalculate vehicle data offset and inversion before appending the vehicle to this train.
	 * @return {@code true} if successful.
	 */
	private boolean addVehicle(VehicleData vehicle, boolean updateOffsets)
	{
		/* Skip if the vehicle has no section. */
		if (vehicle.getSection() == null)
		{
			APILogger.warn(this, String.format("Tried to add vehicle without section to this train! V=%s T=%s", vehicle.toString(), toString()));
			return false;
		}
		
		/* Also abort if the vehicle still belongs to another train. */
		if (vehicle.getTrain() != null)
		{
			System.out.println("WARNING! Tried to add vehicle " + vehicle + " to train " + this + " which had parent " + vehicle.getTrain() + " !"); // XXX DEBUG
			return false;
		}
		
		/*
		 * Recalculate offset and inversion if desired.
		 */
		if (updateOffsets)
		{
			/* Pop vehicle offset and inversion. */
			vehicle.popChanges();
			
			/* If there are other vehicles already present in this train.. */
			if (getHasVehicles())
			{
				CouplingHandler couplingHandler = CouplingHandler.INSTANCE;
				
				/* Retrieve front and back coupling of other vehicle. */
				APartTypeCoupling couplingOtherF = vehicle.getCouplingFront();
				APartTypeCoupling couplingOtherB = vehicle.getCouplingBack();
				
				/* Retrieve front and back coupling of this train. */
				APartTypeCoupling couplingTrainF = getVehicleAt(0).getCouplingFront();
				APartTypeCoupling couplingTrainB = getVehicleAt(getVehicleCount() - 1).getCouplingBack();
				
				/*
				 * Determine which coupling of one vehicle connects to which coupling of the other vehicle.
				 */
				APartTypeCoupling couplingTrain = null;
				APartTypeCoupling couplingOther = null;
				{
					if (couplingOtherF != null)
					{
						couplingTrain = couplingHandler.getMatchingCoupling(couplingOtherF, this);
						if (couplingTrain != null)
						{
							couplingOther = couplingOtherF;
						}
					}
					
					if (couplingTrain == null && couplingOtherB != null)
					{
						couplingTrain = couplingHandler.getMatchingCoupling(couplingOtherB, this);
						if (couplingTrain != null)
						{
							couplingOther = couplingOtherB;
						}
					}
				}
				
				/* Abort if no connecting pair was found. */
				if (couplingTrain == null)
				{
					// XXX DEBUG
					{
						System.out.println();
						System.out.println("===============================================");
						System.out.println("WARNING! Found no connecting couplings while adding vehicle " + vehicle + " to train " + this + " :");
						System.out.println("cOF=" + couplingOtherF);
						System.out.println("cOB=" + couplingOtherB);
						System.out.println("cTF=" + couplingTrainF);
						System.out.println("cTB=" + couplingTrainB);
						System.out.println("===============================================");
						System.out.println();
					}
					// XXX DEBUG
					return false;
				}
				
				/*
				 * If successful, calculate offset and inversion for the other vehicle.
				 */
				{
					/* Check for inversion; if both couplers are either minimum or maximum positions of their vehicles, invert. */
					boolean invert = (couplingTrain == couplingTrainF) ? (couplingOther == couplingOtherF) : (couplingOther == couplingOtherB);

					/* Compute offset. */
					double xBoundOther, xBoundTrain;
					{
						AxisAlignedBB aabb = couplingOther.getParent().getLocalBoundingBox();
						if (couplingOther == couplingOtherB)
						{
							xBoundOther = aabb.minX;

							if(couplingOther instanceof PartTypeCouplingChain)
							{
								xBoundOther -= ((PartTypeCouplingChain)couplingOther).extraDimension.get();
							}
						}
						else
						{
							xBoundOther = aabb.maxX;

							if(couplingOther instanceof PartTypeCouplingChain)
							{
								xBoundOther += ((PartTypeCouplingChain)couplingOther).extraDimension.get();
							}
						}
						
						if (invert)
						{
							xBoundOther = -xBoundOther;
						}
						
						aabb = couplingTrain.getParent().getLocalBoundingBox();
						if (couplingTrain == couplingTrainB)
						{
							xBoundTrain = aabb.minX;

							if(couplingTrain instanceof PartTypeCouplingChain)
							{
								xBoundTrain -= ((PartTypeCouplingChain)couplingTrain).extraDimension.get();
							}
						}
						else
						{
							xBoundTrain = aabb.maxX;

							if(couplingTrain instanceof PartTypeCouplingChain)
							{
								xBoundTrain += ((PartTypeCouplingChain)couplingTrain).extraDimension.get();
							}
						}
					}
					
					vehicle.pushChanges(invert, (float) (xBoundTrain - xBoundOther));
					
					// XXX DEBUG
					{
						// Check for overlap
						for (VehicleData v : this)
						{
							if (Math.abs(v.getOffset() - vehicle.getOffset()) < 0.001D)
							{
								System.out.println();
								System.out.println("===============================================");
								System.out.println("WARNING! The newly coupled vehicle would overlap another vehicle's offset!");
								System.out.printf("oV=%s\n%n", vehicle);
								System.out.printf("tV=%s\n%n", v);
								System.out.println("===============================================");
								System.out.println();
							}
						}
						
						// Check for central insertion
						if (vehicles.size() > 1 && vehicles.get(vehicles.size() - 1).getOffset() < vehicle.getOffset() && vehicle.getOffset() < vehicles.get(0).getOffset())
						{
							System.out.printf("Adding vehicle with offset << %f >> into center of train << %s >>\n", vehicle.getOffset(), this);
						}
					}
					// XXX DEBUG
				}
			}
		}
		
		/*
		 * Add vehicle to this train. Update train reference and notify this train of vehicle list change.
		 */
		vehicles.add(vehicle);
		vehicle.setTrain(this);
		onVehicleListChanged();
		
		// XXX DEBUG
		{
			VehicleData frontVehicle = getVehicleAt(0);
			
			// Check order along X
			{
				// XXX This part has been bodged to include a null-check
				boolean isFrontGreater = false;
				if (frontVehicle.getCouplingFront() != null && frontVehicle.getCouplingBack() != null)
				{
					isFrontGreater = PartHelper.getPosition(frontVehicle.getCouplingFront().getParent()).xCoord > PartHelper.getPosition(frontVehicle.getCouplingBack().getParent()).xCoord;
				}
				
				double lastCoord = Double.NaN;
				for (APartTypeCoupling c : getVehiclePartTypes(OrderedTypesList.ASELECTOR_COUPLING))
				{
					double currCoord = PartHelper.getPosition(c.getParent()).xCoord;
					if (!Double.isNaN(lastCoord))
					{
						boolean printWarning;
						if (isFrontGreater)
						{
							printWarning = (currCoord > lastCoord);
						}
						else
						{
							printWarning = (currCoord < lastCoord);
						}
						
						if (printWarning)
						{
							System.out.println("=============================================== WARNING! ===============================================");
							System.out.println("Train contains couplings in incorrect order (X):");
							for (APartTypeCoupling c0 : getVehiclePartTypes(OrderedTypesList.ASELECTOR_COUPLING))
								System.out.println(c0);
							for (VehicleData v0 : this)
								System.out.println(v0);
							System.out.println("========================================================================================================");
							System.out.println();
							break;
						}
					}
					
					lastCoord = currCoord;
				}
			}
			
			// Check order along Z
			if (false)
			{
				boolean isFrontGreater = PartHelper.getPosition(frontVehicle.getCouplingFront().getParent()).zCoord > PartHelper.getPosition(frontVehicle.getCouplingBack().getParent()).zCoord;
				double lastCoord = Double.NaN;
				for (APartTypeCoupling c : getVehiclePartTypes(OrderedTypesList.ASELECTOR_COUPLING))
				{
					double currCoord = PartHelper.getPosition(c.getParent()).zCoord;
					if (!Double.isNaN(lastCoord))
					{
						boolean printWarning;
						if (isFrontGreater)
						{
							printWarning = (currCoord > lastCoord);
						}
						else
						{
							printWarning = (currCoord < lastCoord);
						}
						
						if (printWarning)
						{
							System.out.println("=============================================== WARNING! ===============================================");
							System.out.println("Train contains couplings in incorrect order (Z):");
							for (APartTypeCoupling c0 : getVehiclePartTypes(OrderedTypesList.ASELECTOR_COUPLING))
								System.out.println(c0);
							for (VehicleData v0 : this)
								System.out.println(v0);
							System.out.println("========================================================================================================");
							System.out.println();
							break;
						}
					}
					
					lastCoord = currCoord;
				}
			}
		}
		// XXX DEBUG
		
		return true;
	}
	
	/**
	 * Client-only method to unsafely add a vehicle to this train's {@link #vehicles vehicle list}.<br>
	 * Does not update the vehicle's offset or inversion flags.<br>
	 * <br>
	 * <i>Client-side only.</i>
	 *
	 * @param vehicle - {@link zoranodensha.api.vehicles.VehicleData Vehicle} to add.
	 * @return {@code true} if successful.
	 */
	@SideOnly(Side.CLIENT)
	public final boolean addVehicle_Client(VehicleData vehicle)
	{
		return worldObj.isRemote && addVehicle(vehicle, false);
	}
	
	@Override
	public void applyEntityCollision(Entity entity)
	{
		if (entity instanceof Train || worldObj.isRemote || isPassenger(entity))
		{
			return;
		}
		CollisionHandler.INSTANCE.onEntityCollision(this, entity);
	}
	
	@Override
	public boolean attackEntityFrom(DamageSource source, float damage)
	{
		/* Handled on server side only. */
		if (worldObj.isRemote)
		{
			return false;
		}
		
		/*
		 * Ensure this entity can be hit and the damage is sufficient. Also make sure the source of the damage is a living entity.
		 */
		Entity entitySource = source.getSourceOfDamage();
		if (isEntityInvulnerable() || damage < 1.0F || !(entitySource instanceof EntityLivingBase))
		{
			return false;
		}
		
		/* Also ignore the attack if there are passengers in the train. */
		if (!getPassengerList().isEmpty())
		{
			return false;
		}
		
		/*
		 * Ray trace the train to determine which vehicle was hit.
		 */
		VehicleData hitVehicle = null;
		{
			Vec3 vec3_Pos = Vec3.createVectorHelper(entitySource.posX, entitySource.posY, entitySource.posZ);
			
			/* Work-around to align client and server sided player camera. */
			if (!worldObj.isRemote && entitySource instanceof EntityPlayer)
			{
				vec3_Pos.yCoord += 1.62D;
			}
			
			Vec3 vec3_Look = ((EntityLivingBase) entitySource).getLook(1.0F);
			Vec3 vec3_Ray = vec3_Pos.addVector(vec3_Look.xCoord * 2, vec3_Look.yCoord * 2, vec3_Look.zCoord * 2);
			
			VehParBase selectedPart = PartHelper.rayTraceParts(null, vec3_Pos, vec3_Ray, null, getVehicleParts(OrderedPartsList.SELECTOR_ANY));
			if (selectedPart != null)
			{
				hitVehicle = selectedPart.getVehicle();
			}
		}
		
		/*
		 * Don't deal damage if the event is cancelled.
		 */
		if (hitVehicle == null || MinecraftForge.EVENT_BUS.post(new VehicleDamagedEvent(this, hitVehicle, source, damage)))
		{
			return false;
		}
		
		/*
		 * Don't destroy or drop vehicle if drop event is cancelled.
		 */
		VehicleDropEvent dropEvent = new VehicleDropEvent(this, hitVehicle);
		if (MinecraftForge.EVENT_BUS.post(dropEvent) || dropEvent.getResult() != Result.ALLOW)
		{
			return false;
		}
		
		/*
		 * Drop vehicle, if allowed.
		 *
		 * Drop item(s) at the source entity's position, if the source entity is a player.
		 */
		if (entitySource instanceof EntityPlayer)
		{
			for (ItemStack itemStack : dropEvent.results)
			{
				entitySource.entityDropItem(itemStack, 0.3F);
			}
		}
		else
		{
			for (ItemStack itemStack : dropEvent.results)
			{
				entityDropItem(itemStack, 0.3F);
			}
		}
		
		return true;
	}
	
	@Override
	public boolean canBeCollidedWith()
	{
		return true;
	}
	
	@Override
	public boolean canBePushed()
	{
		return true;
	}
	
	@Override
	public boolean canTriggerWalking()
	{
		return false;
	}
	
	@Override
	public void copyLocationAndAnglesFrom(Entity entity)
	{
		/* Supposedly empty; We don't want to crash vehicles, so it wouldn't make sense to put one at the position of another. */
	}
	
	@Override
	public void dealFireDamage(int damage)
	{
		;
	}
	
	/**
	 * Called by the default constructor of {@link net.minecraft.entity.Entity#Entity(World) Entity(World)} after base initialization.
	 */
	@Override
	protected void entityInit()
	{
		;
	}
	
	@Override
	protected void fall(float distance)
	{
		if (!MinecraftForge.EVENT_BUS.post(new TrainFallEvent(this, distance)))
		{
			for (PartTypeSeat seat : getVehiclePartTypes(OrderedTypesList.SELECTOR_SEAT))
			{
				seat.fall(distance);
			}
		}
	}
	
	/**
	 * Returns the cab that currently controls this train.
	 *
	 * @return The currently active cab. May be {@code null}.
	 */
	public PartTypeCab getActiveCab()
	{
		return activeCab;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public VTBoundingBox<VehicleData, Train> getBoundingBox()
	{
		if (!(boundingBox instanceof VTBoundingBox))
		{
			try
			{
				VTBoundingBox.setTrainVTBB(this, new VTBoundingBox<VehicleData, Train>(this));
			}
			catch (Exception e)
			{
				throw new ReportedException(new CrashReport("Overriding bounding box", e));
			}
		}
		return (VTBoundingBox<VehicleData, Train>) boundingBox;
	}
	
	/**
	 * Gets the cab in this train that matches the specified UUID. If no cab is found, {@code null} is returned.
	 *
	 * @param uuid - The UUID to use when searching for a matching cab.
	 * @return - If a matching cab is found, a {@link PartTypeCab} instance, otherwise {@code null}.
	 */
	public PartTypeCab getCabFromUUID(String uuid)
	{
		for (PartTypeCab cab : getVehiclePartTypes(OrderedTypesList.SELECTOR_CAB))
		{
			if (cab.uuid.get().equals(uuid))
			{
				return cab;
			}
		}
		
		return null;
	}
	
	@Override
	public AxisAlignedBB getCollisionBox(Entity entity)
	{
		return getBoundingBox();
	}
	
	@Override
	public String getCommandSenderName()
	{
		String clazz = getClass().getSimpleName();
		String vehicles;
		
		switch (getVehicleCount())
		{
			case 0:
				vehicles = "";
				break;
			
			case 1:
				vehicles = String.format("Vehicle=%s", getVehicleAt(0).getUVID().toString());
				break;
			
			default:
				vehicles = String.format("Vehicles=( F:%s B:%s )", getVehicleAt(0).getUVID().toString(), getVehicleAt(getVehicleCount() - 1).getUVID().toString());
				break;
		}
		
		return String.format("%s[%s (%d total)]", clazz, vehicles, getVehicleCount());
	}
	
	/**
	 * Returns whether this train contains vehicles.
	 *
	 * @return {@code true} if this train contains any vehicles.
	 */
	public boolean getHasVehicles()
	{
		return !getVehicleList().isEmpty();
	}
	
	/**
	 * Returns the last speed of this train.
	 *
	 * @return This train's last speed, in meters per tick [m/tick]. May be negative to indicate backward movement.
	 */
	public double getLastSpeed()
	{
		return lastSpeed;
	}
	
	/**
	 * Returns the last non-zero movement speed of this train.
	 *
	 * @return This train's last non-zero movement speed. Negative to indicate backward movement. {@code 0.0} if this train hasn't moved yet.
	 */
	public double getLastSpeedNonZero()
	{
		return lastSpeedNonZero;
	}
	
	/**
	 * Returns the overall length of this train.
	 *
	 * @return - The total length of the train in metres.
	 */
	public float getLength()
	{
		return length;
	}
	
	/**
	 * Returns total the mass of this train, in metric tonnes (1t = 1000kg).
	 *
	 * @see #mass
	 */
	public float getMass()
	{
		return mass;
	}
	
	/**
	 * Returns the current maximum speed [in km/h] of this train.
	 *
	 * @see #maxSpeed
	 */
	public int getMaxSpeed()
	{
		return maxSpeed;
	}
	
	@Override
	public double getMountedYOffset()
	{
		return 0;
	}
	
	/**
	 * Tries to find a {@link zoranodensha.api.vehicles.part.VehParBase vehicle part} with the given id in this train.
	 *
	 * @param vehicleID - The identifier of the vehicle the part should be in.
	 * @param partID    - The part's ID.
	 * @return The matching part if successful, {@code null} otherwise.
	 */
	public VehParBase getPartFromID(int vehicleID, int partID)
	{
		return getPartFromID(getVehicleByID(vehicleID), partID);
	}
	
	/**
	 * Tries to find a {@link zoranodensha.api.vehicles.part.VehParBase vehicle part} with the given id in this train.
	 *
	 * @param vehicle - The vehicle the part should be in.
	 * @param partID  - The part's ID.
	 * @return The matching part if successful, {@code null} otherwise.
	 */
	public VehParBase getPartFromID(VehicleData vehicle, int partID)
	{
		return (vehicle != null) ? vehicle.getPartByID(partID) : null;
	}
	
	/**
	 * Returns all passengers of this train in a List.
	 */
	public ArrayList<Entity> getPassengerList()
	{
		ArrayList<Entity> list = new ArrayList<Entity>();
		Entity passenger;
		
		for (PartTypeSeat seat : getVehiclePartTypes(OrderedTypesList.SELECTOR_SEAT))
		{
			if ((passenger = seat.getPassenger()) != null)
			{
				list.add(passenger);
			}
		}
		return list;
	}
	
	@Override
	public ItemStack getPickedResult(MovingObjectPosition movObjPos)
	{
		TrainMiddleClickEvent clickEvent = new TrainMiddleClickEvent(this, movObjPos);
		if (MinecraftForge.EVENT_BUS.post(clickEvent) || clickEvent.getResult() == Result.DENY)
		{
			return null;
		}
		return clickEvent.result;
	}
	
	/**
	 * Returns the last known vehicle speed, scaled by the user-defined speed scale.<br>
	 * <br>
	 * Use for speed displaying <b>only</b>.
	 */
	public double getScaledSpeed()
	{
		return getLastSpeed() * getSpeedScale();
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public float getShadowSize()
	{
		/* We don't want to cast a shadow, therefore disabled. */
		return 0.0F;
	}
	
	/**
	 * Creates a new NBT tag containing all new property values that require synchronisation in the given direction.
	 *
	 * @return An {@link net.minecraft.nbt.NBTTagCompound NBT tag} holding all values to synchronise, or {@code null} if there's nothing to do.
	 */
	private NBTTagCompound getSyncTag()
	{
		final ESyncDir syncDir = (worldObj.isRemote) ? ESyncDir.SERVER : ESyncDir.CLIENT;
		final NBTTagCompound syncTag = new NBTTagCompound();
		
		/*
		 * Synchronisation packets for clients contain last speed value.
		 */
		if (syncDir == ESyncDir.CLIENT)
		{
			syncTag.setDouble(EDataKey.LAST_SPEED.key, getLastSpeed());
			
			if (getActiveCab() != null)
			{
				syncTag.setString(EDataKey.ACTIVE_CAB.key, getActiveCab().uuid.get());
//				VehParBase activeCabPart = getActiveCab().getParent();
//				long activeCabValue = ((long)activeCabPart.getVehicle().getID() << 32) | activeCabPart.getID();
//				syncTag.setLong(EDataKey.ACTIVE_CAB.key, activeCabValue);
			}
		}
		
		/*
		 * Iterate through all parts' properties which synchronise their values.
		 */
		NBTTagList tagListTrain = new NBTTagList();
		
		for (VehicleData vehicle : this)
		{
			NBTTagList tagListVehicle = new NBTTagList();
			
			/* For each synchronising part in the vehicle.. */
			for (VehParBase part : vehicle.getParts(OrderedPartsList.SELECTOR_PROPS_SYNCHRONISED))
			{
				NBTTagCompound nbt = null;
				char index = 0;
				
				/* ..write every property which needs synchronisation to NBT.. */
				for (IPartProperty<?> prop : part.getProperties())
				{
					if (syncDir.getMatchesDirection(prop.getSyncDir()) && prop.getHasChanged())
					{
						if (nbt == null)
						{
							nbt = new NBTTagCompound();
						}
						nbt.setInteger(String.valueOf(++index), prop.getKey());
						prop.writeToNBT(nbt, ETagCall.PACKET);
						prop.setHasChanged(false);
					}
				}
				
				/* ..and if any property was written to NBT, append tag to list with part ID. */
				if (nbt != null)
				{
					nbt.setInteger("id", part.getID());
					tagListVehicle.appendTag(nbt);
				}
			}
			
			/* Then, if any part in the vehicle was written to NBT, append it to the tag list. */
			if (tagListVehicle.tagCount() > 0)
			{
				NBTTagCompound tagListVehicleNBT = new NBTTagCompound();
				tagListVehicleNBT.setInteger("id", vehicle.getID());
				tagListVehicleNBT.setTag("props", tagListVehicle);
				tagListTrain.appendTag(tagListVehicleNBT);
			}
		}
		
		/*
		 * If the tag list isn't empty, append to the synchronisation tag.
		 */
		if (tagListTrain.tagCount() > 0)
		{
			syncTag.setTag("props", tagListTrain);
		}
		
		return (syncTag.hasNoTags() ? null : syncTag);
	}
	
	/**
	 * Returns the vehicle at the given index of this train's {@link #vehicles vehicle list}.
	 *
	 * @param index - The index of the vehicle.
	 * @return The requested {@link zoranodensha.api.vehicles.VehicleData vehicle}, or {@code null} if the index was invalid.
	 */
	public VehicleData getVehicleAt(int index)
	{
		return (index >= 0 && index < getVehicleCount()) ? getVehicleList().get(index) : null;
	}
	
	/**
	 * Returns the vehicle with the given ID, if it's part of this train.
	 *
	 * @param vehicleID - Identifier of the vehicle to return.
	 * @return The requested {@link zoranodensha.api.vehicles.VehicleData vehicle}, or {@code null} if the vehicle ID did not match any vehicle in this train.
	 */
	public VehicleData getVehicleByID(int vehicleID)
	{
		for (VehicleData vehicle : this)
		{
			if (vehicle.getID() == vehicleID)
			{
				return vehicle;
			}
		}
		return null;
	}
	
	/**
	 * Returns the number of vehicles in this train.
	 */
	public int getVehicleCount()
	{
		return getVehicleList().size();
	}
	
	/**
	 * Returns the index of the given vehicle in this train's vehicle list.
	 *
	 * @param vehicle - {@link zoranodensha.api.vehicles.VehicleData Vehicle} whose index is to be determined.
	 * @return The given vehicle's index in this train's vehicle list, or {@code -1} if it wasn't in the list.
	 */
	public int getVehicleIndex(VehicleData vehicle)
	{
		return getVehicleList().indexOf(vehicle);
	}
	
	/**
	 * Returns a list of all {@link #vehicles} in this train.<br>
	 * <i>This is a getter only for cases where the returned list is not being modified.</i>
	 */
	protected VehicleList getVehicleList()
	{
		return vehicles;
	}
	
	/**
	 * Returns a list of all vehicle parts in this train matching the given selector.<br>
	 * Bridge method to {@link zoranodensha.api.vehicles.VehicleList#getParts(zoranodensha.api.vehicles.part.util.OrderedPartsList.IPartsSelector) getParts()}.
	 */
	public ConcatenatedPartsList getVehicleParts(IPartsSelector selector)
	{
		return getVehicleList().getParts(selector);
	}
	
	/**
	 * Returns a list of all vehicle parts in this train matching the given selector.<br>
	 * Bridge method to {@link zoranodensha.api.vehicles.VehicleList#getTypes(zoranodensha.api.vehicles.part.util.OrderedTypesList.ITypesSelector) getTypes()}.
	 */
	public <T extends APartType> ConcatenatedTypesList<T> getVehiclePartTypes(ITypesSelector<T> selector)
	{
		return getVehicleList().getTypes(selector);
	}
	
	/**
	 * Returns an array list containing this train's vehicles in order. The returned list may be modified freely without impacting this train's contents.
	 *
	 * @return {@link java.util.ArrayList Array list} containing this train's {@link zoranodensha.api.vehicles.VehicleData vehicles}.
	 */
	public ArrayList<VehicleData> getVehicles()
	{
		return new ArrayList<VehicleData>(getVehicleList());
	}
	
	@Override
	public boolean handleLavaMovement()
	{
		return false;
	}
	
	/**
	 * Handles the given property message.
	 *
	 * @param msg - {@link zoranodensha.api.vehicles.packet.IPropertyMessage Property message} to handle.
	 */
	public void handlePropertyMessage(IPropertyMessage<?> msg)
	{
		final int partID = msg.getPartID();
		if (partID == 0)
		{
			/* In case there is no part associated with the message, try to handle it as synchronisation tag. */
			if (EDataKey.fromInt(msg.getKey()) == EDataKey.SYNC_PACKET)
			{
				Object newVal = msg.getNewVal();
				if (newVal instanceof NBTTagCompound)
				{
					handleSyncTag((NBTTagCompound) newVal);
				}
			}
		}
		else
		{
			/* Otherwise try to retrieve the part and notify about the new data. */
			VehParBase part = getPartFromID(msg.getVehicleID(), partID);
			if (part != null)
			{
				part.setProperty(msg.getKey(), msg.getNewVal());
			}
		}
	}
	
	/**
	 * Called to handle NBT tags holding synchronisation data.
	 */
	public void handleSyncTag(NBTTagCompound syncTag)
	{
		/*
		 * Client-only values.
		 */
		if (worldObj.isRemote)
		{
			/* Update last known train speed. */
			setLastSpeed(syncTag.getDouble(EDataKey.LAST_SPEED.key));
			
			/* Update active cab reference. */
//			long activeCabValue = syncTag.getLong(EDataKey.ACTIVE_CAB.key);
//			VehParBase part = getPartFromID((int) (activeCabValue >> 32), (int) activeCabValue);
			String activeCabUUID = syncTag.getString(EDataKey.ACTIVE_CAB.key);
			PartTypeCab cab = getCabFromUUID(activeCabUUID);
			if (cab != null)
			{
				activeCab = cab;
			}
		}
		
		/* Next, update all respective properties. */
		NBTTagList tagListTrain = syncTag.getTagList("props", 10);
		NBTTagCompound nbt;
		VehParBase part;
		
		for (int i = tagListTrain.tagCount() - 1; i >= 0; --i)
		{
			NBTTagCompound nbtVehicle = tagListTrain.getCompoundTagAt(i);
			VehicleData vehicle = getVehicleByID(nbtVehicle.getInteger("id"));
			if (vehicle != null)
			{
				NBTTagList tagListVehicle = nbtVehicle.getTagList("props", 10);
				for (int j = tagListVehicle.tagCount() - 1; j >= 0; --j)
				{
					nbt = tagListVehicle.getCompoundTagAt(j);
					part = getPartFromID(vehicle, nbt.getInteger("id"));
					
					if (part != null)
					{
						char index = 0;
						int key = nbt.getInteger(String.valueOf(++index));
						
						/*
						 * We assume that property order (and therefore key order) are the same on both sides.
						 * Thus, we can iterate over all properties until our matching key was found.
						 */
						for (IPartProperty<?> prop : part.getProperties())
						{
							if (prop.getKey() == key)
							{
								prop.readFromNBT(nbt, ETagCall.PACKET);
								key = nbt.getInteger(String.valueOf(++index));
							}
						}
					}
				}
			}
		}
	}
	
	@Override
	public boolean handleWaterMovement()
	{
		return false;
	}
	
	/**
	 * First layer of player interaction. Overridden so clients are responsible to send ray trace data to the server.
	 */
	@Override
	public boolean interactFirst(EntityPlayer player)
	{
		/*
		 * On server, try to find and apply a part if this train isn't finished.
		 */
		if (!worldObj.isRemote && !isFinished())
		{
			ItemStack heldStack = player.getHeldItem();
			VehParBase heldPart = AVehiclePartRegistry.getPartFromItemStack(heldStack);
			if (heldPart != null)
			{
				/* For each unfinished part: */
				for (VehParBase part : getVehicleParts(OrderedPartsList.SELECTOR_UNFINISHED))
				{
					/* Skip if the part's instance name doesn't match. */
					if (!heldPart.getName().equals(part.getName()))
					{
						continue;
					}
					
					/* Otherwise reduce item stack size if necessary and apply item. */
					if (!player.capabilities.isCreativeMode)
					{
						--heldStack.stackSize;
					}
					
					part.setIsFinished(true);
					return true;
				}
			}
		}
		
		/*
		 * If nothing has worked up to this point, post the interaction event.
		 */
		return !MinecraftForge.EVENT_BUS.post(new PlayerInteractTrainEvent(this, player));
	}
	
	/**
	 * Returns whether this train is finished, i.e. whether all vehicle parts have been applied.
	 *
	 * @return {@code true} if all vehicle parts are finished.
	 */
	public boolean isFinished()
	{
		return getVehicleParts(OrderedPartsList.SELECTOR_UNFINISHED).isEmpty();
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean isInRangeToRender3d(double x, double y, double z)
	{
		/* Retrieve this train's bounding box mask. */
		AxisAlignedBB mask = AxisAlignedBB.getBoundingBox(0, 0, 0, 0, 0, 0);
		mask.setBB(getBoundingBox());
		
		/* Return true if the given position lies inside of the box. */
		Vec3 vecPos = Vec3.createVectorHelper(x, y, z);
		if (mask.isVecInside(vecPos))
		{
			return true;
		}
		
		/* Otherwise get the closest bounding box corner and determine whether it is in render distance range. */
		double cornerX = (Math.abs(x - mask.maxX) < Math.abs(x - mask.minX)) ? mask.maxX : mask.minX;
		double cornerY = (Math.abs(y - mask.maxY) < Math.abs(y - mask.minY)) ? mask.maxY : mask.minY;
		double cornerZ = (Math.abs(z - mask.maxZ) < Math.abs(z - mask.minZ)) ? mask.maxZ : mask.minZ;
		
		x = cornerX - x;
		y = cornerY - y;
		z = cornerZ - z;
		
		return isInRangeToRenderDist(x * x + y * y + z * z);
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public boolean isInRangeToRenderDist(double dist)
	{
		// FIXME @Leshuwa - Ensure general render distance influences train render distance, too.
		double avgLength = boundingBox.getAverageEdgeLength() * 64.0 * renderDistanceWeight;
		return dist < (avgLength * avgLength);
	}
	
	/**
	 * Checks whether the given entity is a passenger of this train,
	 * i.e. whether there is any {@link zoranodensha.api.vehicles.part.type.PartTypeSeat seat} occupied by the given entity.
	 *
	 * @param passenger - {@link net.minecraft.entity.Entity Entity} in question. Will return {@code false} if {@code null}.
	 * @return {@code true} if the given entity is a passenger of this train.
	 */
	public boolean isPassenger(@Nullable Entity passenger)
	{
		if (passenger != null)
		{
			for (PartTypeSeat seat : getVehiclePartTypes(OrderedTypesList.SELECTOR_SEAT))
			{
				if (passenger.isEntityEqual(seat.getPassenger()))
				{
					return true;
				}
			}
		}
		return false;
	}
	
	@Nonnull
	@Override
	public Iterator<VehicleData> iterator()
	{
		return getVehicleList().iterator();
	}
	
	/**
	 * Returns a list iterator over this train's {@link #vehicles vehicle list}.
	 *
	 * @return {@link java.util.ListIterator List iterator} over vehicles in this train.
	 */
	public ListIterator<VehicleData> listIterator()
	{
		return getVehicleList().listIterator();
	}
	
	@Override
	public void mountEntity(Entity entity)
	{
		/* Cleared out. A train shouldn't mount an entity. */
	}
	
	@Override
	public void onKillEntity(EntityLivingBase entity)
	{
		MinecraftForge.EVENT_BUS.post(new LivingKilledByTrainEvent(this, entity));
	}
	
	@Override
	public void onStruckByLightning(EntityLightningBolt lightningBolt)
	{
		;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onUpdate()
	{
		/*
		 * Kill train and abort if there is no vehicle in this train.
		 */
		if (!getHasVehicles())
		{
			if (!worldObj.isRemote)
			{
				setDead();
			}
			return;
		}
		
		/* Reference to the handler for movement; used on client and server. */
		MovementHandler movementHandler = MovementHandler.INSTANCE;
		
		/*
		 * Handle position updates as well as collision and other physics on the server.
		 */
		if (!worldObj.isRemote)
		{
			/*
			 * Prepare update.
			 */
			{
				/* Split train if coupler connections have broken. */
				CouplingHandler.INSTANCE.ensureCouplersConnect(this);
				
				/* Update active cab and its train data. */
				updateActiveCab();
				if (getActiveCab() != null)
				{
					getActiveCab().onUpdateTrain();
				}
			}
			
			/*
			 * Update direction vectors. Calculate and clamp movement distance.
			 */
			double movementDistance;
			{
				/* Calculate movement distance and direction vectors. */
				movementDistance = movementHandler.calculateSpeed(this);
				movementHandler.refreshDirectionVectors(this);
				
				/* Let subscribed event handlers check the speed. */
				TrainUpdateSpeedEvent speedEvent = new TrainUpdateSpeedEvent(this, movementDistance);
				{
					MinecraftForge.EVENT_BUS.post(speedEvent);
					movementDistance = speedEvent.result;
				}
				
				/* If there is any part unfinished, don't move the train. */
				if (!isFinished())
				{
					movementDistance = 0.0D;
				}
			}
			
			/*
			 * Move train, update position and parts.
			 */
			CollisionHandler collisionHandler = CollisionHandler.INSTANCE;
			{
				/* Check train for collision, change speed accordingly. */
				double effectiveMovementDistance;
				{
					effectiveMovementDistance = collisionHandler.collideWithSolids(this, movementDistance);
					setLastSpeed(effectiveMovementDistance);
					isCollided = (effectiveMovementDistance < movementDistance);
				}
				
				/* Move bogies, and update the train's position and parts. */
				movementHandler.moveBogies(this, effectiveMovementDistance);
				movementHandler.updateTrainPosition(this);
				updateParts();
				
				/* Run entity collision checks and notify blocks about bogie collision. */
				collisionHandler.collideWithEntities(this);
				collisionHandler.collideBogiesWithBlocks(this);
			}
			
			/*
			 * Update Pneumatics
			 */
			PneumaticsHandler pneumaticsHandler = PneumaticsHandler.INSTANCE;
			{
				pneumaticsHandler.updateTrainPneumatics(this);
			}
		}
		
		/*
		 * On client, only update train positions according to bogie positions.
		 */
		else
		{
			/*
			 * Update train and part position, update bounding box.
			 */
			{
				movementHandler.updateTrainPosition(this);
				updateParts();
			}
			
			/*
			 * Bug fix to avoid "jumpy" movement of trains due to too early updates, relative to the local client's player.
			 *
			 * Appending the train to the back of the world's loaded entity list should fix the issue.
			 */
			if (worldObj.loadedEntityList.indexOf(this) < worldObj.loadedEntityList.indexOf(Minecraft.getMinecraft().thePlayer))
			{
				if (worldObj.loadedEntityList.remove(this))
				{
					worldObj.loadedEntityList.add(this);
				}
			}
		}
		
		/*
		 * Check for vehicle changes, post event if some were detected.
		 */
		if (getVehicleList().getHasChanged())
		{
			/* Post event for vehicle changes. */
			MinecraftForge.EVENT_BUS.post(new VehicleListChangedEvent(this));
			
			/* On server side, kill train if empty. Otherwise, synchronise vehicles. */
			if (!worldObj.isRemote)
			{
				if (!getHasVehicles())
				{
					setDead();
				}
				else
				{
					APropertyMessage.sendToTracking(this, new TrainMessage_UpdateVehicles(this));
				}
			}
		}
		
		/*
		 * If the train isn't marked for removal, check for changed vehicle part properties and synchronise if required.
		 */
		if (!isDead)
		{
			NBTTagCompound syncTag = getSyncTag();
			if (syncTag != null)
			{
				PropertyMessage_NBT message = new PropertyMessage_NBT(getEntityId(), 0, 0, (byte) EDataKey.SYNC_PACKET.ordinal(), syncTag);
				
				if (worldObj.isRemote)
				{
					APropertyMessage.sendToServer(message);
				}
				else
				{
					APropertyMessage.sendToTracking(this, message);
				}
			}
		}
		
		/*
		 * Finished updating. Notify event listeners.
		 */
		MinecraftForge.EVENT_BUS.post(new TrainUpdateEvent.TrainUpdateFinishedEvent(this));
	}
	
	/**
	 * Called after the {@link #vehicles} list changed to clear vehicle list caches,
	 * update the train position, and refresh train properties.
	 */
	protected void onVehicleListChanged()
	{
		getVehicleList().clearCaches();
		MovementHandler.INSTANCE.updateTrainPosition(this);
		refreshProperties();
	}
	
	@Override
	protected void readEntityFromNBT(NBTTagCompound nbt)
	{
		readEntityFromNBT(nbt, ETagCall.SAVE);
	}
	
	/**
	 * Called to read entity data from the given NBT tag.
	 */
	public void readEntityFromNBT(NBTTagCompound nbt, ETagCall callType)
	{
		/*
		 * Read vehicle data.
		 */
		if (nbt.hasKey(EDataKey.VEHICLE_DATA.key))
		{
			/* Clear vehicle data list first. */
			vehicles.clear();
			
			/* Read each vehicle from NBT. */
			NBTTagList vehicleDataList = nbt.getTagList(EDataKey.VEHICLE_DATA.key, 10);
			VehicleData vehicle;
			
			for (int i = vehicleDataList.tagCount() - 1; i >= 0; --i)
			{
				vehicle = new VehicleData(vehicleDataList.getCompoundTagAt(i), callType);
				if (vehicle.getHasParts())
				{
					addVehicle(vehicle, false);
				}
			}
		}
		
		/*
		 * Read call-type dependent data.
		 */
		if (ETagCall.ITEM != callType)
		{
			/* Update last known train speed. */
			if (nbt.hasKey(EDataKey.LAST_SPEED.key))
			{
				setLastSpeed(nbt.getDouble(EDataKey.LAST_SPEED.key));
			}
			
			/* Update active cab reference. */
			if (nbt.hasKey(EDataKey.ACTIVE_CAB.key))
			{
				String activeCabUUID = nbt.getString(EDataKey.ACTIVE_CAB.key);
				PartTypeCab cab = getCabFromUUID(activeCabUUID);
				
				if (cab != null)
				{
					activeCab = cab;
				}
			}
//			if (nbt.hasKey(EDataKey.ACTIVE_CAB.key))
//			{
//				long activeCabValue = nbt.getLong(EDataKey.ACTIVE_CAB.key);
//				VehParBase part = getPartFromID((int) (activeCabValue >> 32), (int) activeCabValue);
//
//				if (part != null)
//				{
//					activeCab = part.getPartType(PartTypeCabBasic.class);
//				}
//			}
		}
	}
	
	@Override
	public void readSpawnData(ByteBuf byteBuf)
	{
		/* Read the NBT tag from the given byte buffer. */
		NBTTagCompound nbt = NBTUtil.readNBTFromBuffer(byteBuf);
		
		/* Update speed scale, read vehicle from NBT. */
		setSpeedScale(nbt.getFloat(EDataKey.SPEED_SCALE.key));
		readEntityFromNBT(nbt, ETagCall.PACKET);
	}
	
	/**
	 * Called to refresh this train's properties, such as weight and maximum speed.
	 */
	public void refreshProperties()
	{
		/* Reset mass, prepare iteration. */
		mass = 0;
		length = 0;
		maxSpeed = 0;
		
		/*
		 * Calculate overall mass.
		 */
		for (VehParBase part : getVehicleParts(OrderedPartsList.SELECTOR_ANY))
		{
			/* Add mass of each vehicle part. */
			mass += part.getMass();
		}
		
		/*
		 * Determine overall train length.
		 */
		ConcatenatedPartsList parts = getVehicleParts(OrderedPartsList.SELECTOR_ANY);
		if (!parts.isEmpty())
		{
			AxisAlignedBB partFirstBox = parts.get(0).getLocalBoundingBox();
			AxisAlignedBB partLastBox = parts.get(parts.size() - 1).getLocalBoundingBox();
			length = (float) Math.abs(partLastBox.maxX - partFirstBox.minX);
		}
		
		/*
		 * Calculate part-wise speed limit.
		 */
		for (PartTypeBogie bogie : getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY))
		{
			if (bogie.getMaxSpeed() > 0 && (maxSpeed <= 0 || bogie.getMaxSpeed() < maxSpeed))
			{
				maxSpeed = bogie.getMaxSpeed();
			}
		}
		
		/*
		 * Refresh last active cab's part reference.
		 */
		if (activeCab != null)
		{
//			VehParBase part = getPartFromID(activeCab.getVehicle().getID(), activeCab.getParent().getID());
//			activeCab = (part != null) ? part.getPartType(PartTypeCabBasic.class) : null;
			activeCab = getCabFromUUID(activeCab.uuid.get());
		}
		
		/*
		 * Update entity size depending on bounding box size.
		 */
		VTBoundingBox<VehicleData, Train> vtbb = getBoundingBox();
		setSize((float) Math.min((vtbb.maxX - vtbb.minX), (vtbb.maxZ - vtbb.minZ)), (float) (vtbb.maxY - vtbb.minY));
	}
	
	/**
	 * Removes the given vehicle from either end of this train.<br>
	 * If the specified vehicle is somewhere in between this train, this method intentionally fails.
	 *
	 * @param vehicle - The {@link zoranodensha.api.vehicles.VehicleData vehicle} to remove.
	 * @return {@code true} if successful.
	 */
	public boolean removeVehicle(VehicleData vehicle)
	{
		int indexOf = getVehicleIndex(vehicle);
		if (indexOf == 0 || indexOf == (getVehicleCount() - 1))
		{
			VehicleData removedVehicle = vehicles.remove(indexOf);
			if (removedVehicle == vehicle)
			{
				vehicle.setTrain(null);
				vehicle.popChanges();
				onVehicleListChanged();
				return true;
			}
			
			APILogger.warn(this, String.format("Removed a vehicle from this train which wasn't the specified vehicle! R=%s S=%s", removedVehicle, vehicle));
		}
		return false;
	}
	
	/**
	 * Client-only method to unsafely remove a vehicle from this train's {@link #vehicles vehicle list}.<br>
	 * <br>
	 * <i>Client-side only.</i>
	 *
	 * @param vehicle - {@link zoranodensha.api.vehicles.VehicleData Vehicle} to remove.
	 */
	@SideOnly(Side.CLIENT)
	public final void removeVehicle_Client(VehicleData vehicle)
	{
		if (worldObj.isRemote && vehicles.remove(vehicle))
		{
			vehicle.setTrain(null);
			vehicle.popChanges();
			onVehicleListChanged();
		}
	}
	
	@Override
	public void setDead()
	{
		super.setDead();
		MinecraftForge.EVENT_BUS.post(new TrainDestroyedEvent(this));
		
		/*
		 * Go through all cabs and make sure all loaded chunks are released.
		 */
		{
			// Create a list of cabs.
			ConcatenatedTypesList<PartTypeCab> cabs = this.getVehiclePartTypes(OrderedTypesList.SELECTOR_CAB);
			
			for (PartTypeCab rawCab : cabs)
			{
				/*
				 * Check if any of those cabs are of the 'PartTypeCab' extension, these instances have MTMS units and therefore can keep chunks loaded.
				 */
				if (rawCab instanceof PartTypeCab)
				{
					PartTypeCab cab = (PartTypeCab) rawCab;
					
					/*
					 * Check if the MTMS system is not null, then call the appropriate method to remind MTMS to cancel all its loaded chunks.
					 */
					if (cab.mtms != null)
					{
						cab.mtms.onTrainDead();
					}

					cab.clear();
				}
			}
		}
	}
	
	/**
	 * Set the last known speed of this train.
	 *
	 * @param lastSpeed - The last known speed of this train, in meters per tick [m/tick]. May be negative to indicate backward movement.
	 */
	public void setLastSpeed(double lastSpeed)
	{
		this.lastSpeed = lastSpeed;
		if (this.lastSpeed != 0.0)
		{
			lastSpeedNonZero = this.lastSpeed;
		}
	}
	
	@Override
	public void setLocationAndAngles(double x, double y, double z, float yaw, float pitch)
	{
		/* Disabled. */
	}
	
	@Override
	public void setPosition(double x, double y, double z)
	{
		posX = x;
		posY = y;
		posZ = z;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void setPositionAndRotation2(double x, double y, double z, float yaw, float pitch, int i)
	{
		/*
		 * Emptied, because this is used by Minecraft's position update packets to update client-sided entity positions.
		 * We use our own position update logic, however. Thus, no need for this.
		 */
	}
	
	@Override
	public boolean shouldRenderInPass(int pass)
	{
		return (pass == 0 || pass == 1);
	}
	
	@Override
	public String toString()
	{
		String clazz = getClass().getSimpleName();
		String world = (worldObj != null) ? worldObj.getWorldInfo().getWorldName() : "~NULL~";
		String vehicles = "";
		
		for (VehicleData vehicle : getVehicleList())
		{
			vehicles += String.format(" %s ", vehicle.toString());
		}
		
		return String.format("%s[ID=%d | World=%s | %.2fx %.2fy %.2fz | Vehicles={ %s }]", clazz, getEntityId(), world, posX, posY, posZ, vehicles);
	}
	
	/**
	 * <p>
	 * Updates the currently active cab controlling this train.
	 * </p>
	 *
	 * <p>
	 * Generally, a cab may only become the new active cab if the train stands still <b>and</b> the current active cab is either {@code null}
	 * or allows being {@link PartTypeCab#getMayOverrideActive() overridden}.<br>
	 * </p>
	 *
	 * <p>
	 * <i>Server-side only.</i>
	 * </p>
	 */
	public void updateActiveCab()
	{
		/*
		 * The client may not update the cab by itself.
		 */
		if (worldObj.isRemote)
		{
			return;
		}
		
		/* If the current active cab isn't in this train anymore, remove it. */
		if (activeCab != null && activeCab.getParent().getTrain() != this)
		{
			activeCab = null;
		}
		
		/*
		 * If there is either no active cab or the current active cab allows overrides,
		 * check for a new cab candidate within this train.
		 */
		if (getMayOverrideCab(activeCab))
		{
			ConcatenatedTypesList<PartTypeCab> cabs = getVehiclePartTypes(OrderedTypesList.SELECTOR_CAB);
			if (!cabs.isEmpty())
			{
				for (PartTypeCab cab : cabs)
				{
					if (cab.trySetActive())
					{
						activeCab = cab;
						return;
					}
				}
			}
		}
	}
	
	/**
	 * Called to update all tickable vehicle {@link zoranodensha.api.vehicles.part.type.APartType.IPartType_Tickable part types}
	 * and {@link zoranodensha.api.vehicles.part.property.ITickableProperty properties}.
	 */
	public void updateParts()
	{
		/* First, update part types. */
		for (APartType type : getVehiclePartTypes(OrderedTypesList.SELECTOR_TICKABLE))
		{
			((IPartType_Tickable) type).onTick();
		}
		
		/* Then update properties. */
		for (VehParBase part : getVehicleParts(OrderedPartsList.SELECTOR_PROPS_TICKABLE))
		{
			for (IPartProperty<?> prop : part.getProperties())
			{
				if (prop instanceof ITickableProperty)
				{
					((ITickableProperty<?>) prop).onUpdate();
				}
			}
		}
		
		/* And also update expressions. */
		for (VehParBase part : getVehicleParts(OrderedPartsList.SELECTOR_EXPRESSION))
		{
			part.onUpdateExpressions();
		}
	}
	
	/**
	 * Updates this train's position.<br>
	 * More precisely, moves the train's {@link net.minecraft.entity.Entity#posX positions}
	 * to the center of its {@link #getBoundingBox() bounding box}.
	 */
	public void updatePosition()
	{
		/* Update previous position values. */
		prevPosX = posX;
		prevPosY = posY;
		prevPosZ = posZ;
		
		/* Move current entity position. */
		Vec3 vecPosBB = VehicleHelper.getPosition(getBoundingBox());
		setPosition(vecPosBB.xCoord, getBoundingBox().minY, vecPosBB.zCoord);
		
		/* If this train has fallen out of the world, set it dead. */
		if (posY < -64.0D)
		{
			setDead();
		}
	}
	
	@Override
	protected void writeEntityToNBT(NBTTagCompound nbt)
	{
		writeEntityToNBT(nbt, ETagCall.SAVE);
	}
	
	/**
	 * Called to write entity data to the given NBT tag.
	 */
	public void writeEntityToNBT(NBTTagCompound nbt, ETagCall callType)
	{
		/* Refresh properties before writing to avoid incorrect save data. */
		refreshProperties();
		
		/*
		 * Write call-type dependent data.
		 */
		switch (callType)
		{
			case ITEM:
				/* Mass and maximum speed */
				nbt.setInteger(EDataKey.MAX_SPEED.key, getMaxSpeed());
				nbt.setInteger(EDataKey.MASS.key, MathHelper.floor_float(getMass()));
				break;
			
			case PACKET:
			case SAVE:
				/* Last speed */
				nbt.setDouble(EDataKey.LAST_SPEED.key, getLastSpeed());
				
				/* Active cab */
				if (getActiveCab() != null)
				{
					String activeCabUUID = getActiveCab().uuid.get();
					nbt.setString(EDataKey.ACTIVE_CAB.key, activeCabUUID);
//					long activeCabValue = ((long) getActiveCab().getParent().getVehicle().getID() << 32) | getActiveCab().getParent().getID();
//					nbt.setLong(EDataKey.ACTIVE_CAB.key, activeCabValue);
				}
				break;
		}
		
		/*
		 * Write vehicle data.
		 */
		NBTTagList tagList = new NBTTagList();
		NBTTagCompound nbt0;
		
		for (VehicleData vehicleData : this)
		{
			vehicleData.writeToNBT(nbt0 = new NBTTagCompound(), callType);
			tagList.appendTag(nbt0);
		}
		
		nbt.setTag(EDataKey.VEHICLE_DATA.key, tagList);
	}
	
	@Override
	public void writeSpawnData(ByteBuf byteBuf)
	{
		/* Write speed scale and train data to new NBT tag. */
		NBTTagCompound nbt = new NBTTagCompound();
		nbt.setFloat(EDataKey.SPEED_SCALE.key, SPEED_SCALE_FACTOR);
		writeEntityToNBT(nbt, ETagCall.PACKET);
		
		/* Write NBT tag to byte buffer. */
		NBTUtil.writeNBTToBuffer(nbt, byteBuf);
	}
	
	/**
	 * Checks whether the given cab may be overridden by another cab, according to convention of the {@link #activeCab}.
	 *
	 * @param cab - The {@link PartTypeCab cab} that is to be checked.
	 * @return {@code true} if the given cab may be overridden.
	 */
	public static boolean getMayOverrideCab(@Nullable PartTypeCab cab)
	{
		return (cab == null) || (cab.getMayOverrideActive() && cab.getParent().getTrain().getLastSpeed() == 0.0D);
	}
	
	
	/**
	 * Returns the speed scale value, used to scale speed for display purposes.
	 */
	public static double getSpeedScale()
	{
		return SPEED_SCALE_FACTOR;
	}
	
	/**
	 * Update the {@link #SPEED_SCALE_FACTOR speed scale value}. Does a range check before applying.<br>
	 * <br>
	 * <b>IMPORTANT:</b><br>
	 * Does not synchronise the new value.
	 */
	public static void setSpeedScale(float newVal)
	{
		if (newVal <= 0.001F)
		{
			newVal = 1.0F;
		}
		SPEED_SCALE_FACTOR = newVal;
	}
	
	
	/**
	 * Enum containing NBT keys used when writing/reading data to/from NBT.
	 */
	public enum EDataKey
	{
		/**
		 * {@code String} UUID of current active cab
		 */
		ACTIVE_CAB("activeCab"),
		
		/**
		 * {@code double} Last known train speed in km/h
		 */
		LAST_SPEED("lastSpeed"),
		
		/**
		 * {@code boolean} Whether this vehicle is saved as blueprint, used for item rendering
		 */
		IS_BLUEPRINT("isBlueprint"),
		
		/**
		 * {@code boolean} Whether this vehicle is an epic blueprint
		 */
		IS_EPIC("isEpic"),
		
		/**
		 * {@code int} Vehicle mass
		 */
		MASS("mass"),
		
		/**
		 * {@code int} Maximum speed in km/h
		 */
		MAX_SPEED("maxSpeed"),
		
		/**
		 * {@code float} Speed scale value
		 */
		SPEED_SCALE("speedScale"),
		
		/**
		 * {@code NBTTagCompound} Synchronisation packet for bogie positions
		 */
		SYNC_PACKET("syncPacket"),
		
		/**
		 * {@code NBTTagList} Vehicle data list
		 */
		VEHICLE_DATA("vehicleData");
		
		
		public final String key;
		
		
		EDataKey(String key)
		{
			this.key = key;
		}
		
		@Override
		public String toString()
		{
			return key;
		}
		
		/**
		 * Tries to assign a key to the given number.
		 *
		 * @return The matching {@link zoranodensha.api.vehicles.Train.EDataKey key}, or {@code null}.
		 */
		public static EDataKey fromInt(int i)
		{
			for (EDataKey key : values())
			{
				if (key.ordinal() == i)
				{
					return key;
				}
			}
			
			return null;
		}
	}
}