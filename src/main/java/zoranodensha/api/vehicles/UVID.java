package zoranodensha.api.vehicles;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.annotation.Nullable;

import net.minecraft.nbt.NBTTagCompound;



/**
 * The Unique Vehicle ID [short <b>UVID</b>] is a globally consistent system to label and identify rolling stock with minimum ambiguity.<br>
 * <br>
 * All UVIDs are Strings of length 23 and contain the following information:<br>
 * <br>
 * D: {@link #date Date of creation}<br>
 * L: {@link #label Vehicle name}, containing 3-5 letters<br>
 * R: {@link #ranNum Random numbers}, filling with 2-4 random numbers<br>
 * N: {@link #number Vehicle ID}, containing 4 numbers<br>
 * <br>
 * Since {@code vehicle ID} and {@code vehicle name} are user-defined values, they may vary in length.
 * This results in following possible UVID formats:<br>
 * {@code DD-DD-DD LLL RRRR-NNN-N}.<br>
 * {@code DD-DD-DD LLLL RRR-NNN-N}.<br>
 * {@code DD-DD-DD LLLLL RR-NNN-N}.
 * 
 * @author Leshuwa Kaiheiwa
 */
public final class UVID implements Cloneable
{
	/** A statically accessible random instance, used for {@link #ranNum}. */
	private static final Random ran = new Random();

	/** Random number filler, holding 2-4 numbers. May be {@code null}. */
	@Nullable
	private String ranNum;
	/** Date of blueprint creation, in (YY-MM-DD) format. */
	private String date;
	/** Vehicle name, consisting of 3-5 letters. */
	private String label;
	/** Vehicle ID, containing 4 numbers in (000-0) format. */
	private String number;
	/** String returned by {@link #toString()}. Reset to {@code null} whenever a property changes. */
	private String string;



	/**
	 * Empty constructor to create an empty UVID.
	 */
	public UVID()
	{
		this(EIdentifier.DATE.defValue, EIdentifier.LABEL.defValue, EIdentifier.NUMBER.defValue);
	}

	/**
	 * Construct a UVID from the given data.<br>
	 * {@link #date Creation date} will be initialised automatically.
	 */
	public UVID(String label, String number)
	{
		this(getFormattedDate(), label, number);
	}

	/**
	 * Fully initialise this UVID with given data.
	 * 
	 * @param date - Creation date, see {@link #date}.
	 * @param label - Vehicle label, see {@link #label}.
	 * @param number - Vehicle ID, see {@link #number}.
	 */
	private UVID(String date, String label, String number)
	{
		this.date = date;
		this.label = label;
		this.ranNum = null;
		this.number = number;

		if (label.length() < 3 || label.length() > 5)
		{
			throw new IllegalArgumentException("Invalid label length: " + label.length());
		}
		else if (number.length() != EIdentifier.NUMBER.defValue.length())
		{
			throw new IllegalArgumentException("Invalid number length: " + number.length());
		}
	}


	@Override
	public boolean equals(Object obj)
	{
		return (obj instanceof UVID && obj.hashCode() == hashCode());
	}

	@Override
	public int hashCode()
	{
		return toString().hashCode();
	}

	/**
	 * Read a UVID from given NBT tag.
	 * 
	 * @param nbt - {@link net.minecraft.nbt.NBTTagCompound NBT tag} to read from.
	 */
	public void readFromNBT(NBTTagCompound nbt)
	{
		if (nbt.hasKey(EIdentifier.DATE.nbt_key))
		{
			date = EIdentifier.DATE.readFromNBT(nbt);
		}

		if (nbt.hasKey(EIdentifier.LABEL.nbt_key))
		{
			label = EIdentifier.LABEL.readFromNBT(nbt);
		}

		if (nbt.hasKey(EIdentifier.RAN_NUM.nbt_key))
		{
			ranNum = EIdentifier.RAN_NUM.readFromNBT(nbt);
		}

		if (nbt.hasKey(EIdentifier.NUMBER.nbt_key))
		{
			number = EIdentifier.NUMBER.readFromNBT(nbt);
		}
		
		resetString();
	}
	
	/**
	 * Called to reset the {@link #string} cache.
	 */
	protected void resetString()
	{
		string = null;
	}

	/**
	 * Computes a new {@link #ranNum random number}.
	 */
	public void setRandomNumber()
	{
		int length = 7 - label.length();
		if (length >= 2 && length <= 4)
		{
			String s = EIdentifier.RAN_NUM.defValue + ran.nextInt(10000);
			ranNum = new StringBuffer(s).reverse().toString();
			ranNum = ranNum.substring(0, length);
			resetString();
		}
	}

	@Override
	public String toString()
	{
		if (string == null)
		{
			string = String.format("%s %s %s-%s", date, label, (ranNum != null ? ranNum : EIdentifier.RAN_NUM.defValue), number);
		}
		return string;
	}

	/**
	 * Write this UVID to NBT.
	 */
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setString(EIdentifier.DATE.nbt_key, date);
		nbt.setString(EIdentifier.LABEL.nbt_key, label);
		nbt.setString(EIdentifier.NUMBER.nbt_key, number);

		if (ranNum != null)
		{
			nbt.setString(EIdentifier.RAN_NUM.nbt_key, ranNum);
		}
	}

	/**
	 * Returns the current date as formatted String.
	 */
	private static String getFormattedDate()
	{
		return (new SimpleDateFormat("yyyy-MM-dd")).format(new Date()).substring(2);
	}



	/**
	 * All identifiers of UVID fields, along with their respective NBT keys.
	 */
	public enum EIdentifier
	{
		/** Date, in format {@code YY-MM-DD}. */
		DATE("00-00-00"),
		/** 3-5 custom letters. */
		LABEL("XXX"),
		/** 2-4 random numbers. */
		RAN_NUM("0000"),
		/** 4 custom numbers. */
		NUMBER("000-0");

		public final String nbt_key;
		public final String defValue;



		EIdentifier(String defValue)
		{
			this.defValue = defValue;
			this.nbt_key = "uvid_" + toString().toLowerCase();
		}

		/**
		 * Reads this identifier from the given NBT tag and returns the read value from NBT.
		 * 
		 * @return Value read from the given {@link net.minecraft.nbt.NBTTagCompound NBT tag}.
		 */
		public String readFromNBT(NBTTagCompound nbt)
		{
			return nbt.getString(nbt_key);
		}
	}
}
