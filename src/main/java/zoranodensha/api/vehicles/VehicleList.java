package zoranodensha.api.vehicles;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import javax.annotation.Nonnull;

import zoranodensha.api.vehicles.part.type.APartType;
import zoranodensha.api.vehicles.part.util.ConcatenatedPartsList;
import zoranodensha.api.vehicles.part.util.ConcatenatedTypesList;
import zoranodensha.api.vehicles.part.util.OrderedPartsList.IPartsSelector;
import zoranodensha.api.vehicles.part.util.OrderedTypesList.ITypesSelector;



/**
 * <h1>Vehicle List</h1>
 * <hr>
 * 
 * <p>
 * A list specifically made for {@link zoranodensha.api.vehicles.VehicleData vehicles}, sorted by their local
 * {@link zoranodensha.api.vehicles.VehicleData#getOffset() X-offset}, in descending order (i.e. from front [+X] to back [-X]).<br>
 * </p>
 * 
 * <p>
 * Maintains a {@link #partsCache} and a {@link #typesCache} for quicker access to vehicle parts and part types.
 * </p>
 * 
 * @author Leshuwa Kaiheiwa
 */
public class VehicleList extends AbstractList<VehicleData> implements Cloneable
{
	/** Cache holding lists of vehicle {@link zoranodensha.api.vehicles.part.VehParBase parts}. */
	private final HashMap<IPartsSelector, ConcatenatedPartsList> partsCache = new HashMap<IPartsSelector, ConcatenatedPartsList>();
	/** Cache holding lists of vehicle part {@link zoranodensha.api.vehicles.part.type.APartType types}. */
	private final HashMap<ITypesSelector<?>, ConcatenatedTypesList<? extends APartType>> typesCache = new HashMap<ITypesSelector<?>, ConcatenatedTypesList<? extends APartType>>();
	/** This list's parent {@link zoranodensha.api.vehicles.Train train}. */
	private final Train train;

	/** Element buffer of this vehicle list. The length of the array is this list's capacity. */
	private transient VehicleData[] array;

	/** {@code true} if the list was changed since the last call to {@link #getHasChanged()}. */
	private boolean hasChanged;



	/**
	 * Create a new, empty vehicle list with the given parent.
	 * 
	 * @param train - Parent {@link zoranodensha.api.vehicles.Train train} this list belongs to.
	 */
	public VehicleList(Train train)
	{
		this.train = train;
		this.array = new VehicleData[0];
	}


	@Override
	public boolean add(VehicleData e)
	{
		if (contains(e))
		{
			return false;
		}
		addFast(e);
		return true;
	}

	@Override
	public boolean addAll(@Nonnull Collection<? extends VehicleData> c)
	{
		boolean listChanged = false;

		for (VehicleData e : c)
		{
			if (!contains(e))
			{
				addFast(e);
				listChanged = true;
			}
		}

		return listChanged;
	}

	/**
	 * Helper method to quickly add a single vehicle to the backed array.
	 * 
	 * @throws NullPointerException If the given element was {@code null}.
	 */
	private void addFast(VehicleData e)
	{
		if (e == null)
		{
			throw new NullPointerException("Element was null!");
		}

		ensureCapacity(array.length + 1);
		hasChanged = true;

		VehicleData veh = e;

		for (int i = 0; i < array.length && veh != null; ++i)
		{
			if (array[i] == null || array[i].getOffset() < veh.getOffset())
			{
				VehicleData tmp = array[i];
				array[i] = veh;
				veh = tmp;
			}
		}
	}

	@Override
	public void clear()
	{
		while (!isEmpty())
		{
			remove(0);
		}

		ensureCapacity(0);
		hasChanged = true;
	}

	/**
	 * Helper method to clear the {@link #partsCache} and {@link #typesCache}.
	 */
	public void clearCaches()
	{
		partsCache.clear();
		typesCache.clear();
	}

	@Override
	public VehicleList clone()
	{
		try
		{
			VehicleList ret = (VehicleList)super.clone();
			ret.array = Arrays.copyOf(array, array.length);
			ret.modCount = 0;
			return ret;
		}
		catch (CloneNotSupportedException e)
		{
			throw new InternalError();
		}
	}

	/**
	 * Returns whether the given element is part of this list.
	 * 
	 * @return {@code true} if the given element is contained by this list.
	 */
	@Override
	public boolean contains(Object o)
	{
		return (indexOf(o) >= 0);
	}

	/**
	 * Ensures that the backed array is of given size.
	 * 
	 * @throws IllegalArgumentException If the specified size is smaller than zero.
	 */
	private void ensureCapacity(int size)
	{
		if (size < 0)
		{
			throw new IllegalArgumentException("Illegal array size specified:" + size);
		}
		modCount++;

		int oldSize = array.length;
		if (size != oldSize)
		{
			array = Arrays.copyOf(array, size);
		}
	}

	/**
	 * Returns the element in this list at the given index.
	 * 
	 * @return The part at the given index.
	 * @throws NullPointerException If the index is out of range.
	 */
	@Override
	public VehicleData get(int index)
	{
		rangeCheck(index);
		return array[index];
	}

	/**
	 * Returns whether the list has been changed since the last call to this method.<br>
	 * Used by the parent vehicle's {@link zoranodensha.api.vehicles.Train train} to determine when to synchronise its vehicle list.
	 */
	public boolean getHasChanged()
	{
		final boolean flag = hasChanged;
		hasChanged = false;
		return flag;
	}

	/**
	 * Returns a list holding all vehicle parts matching the given selector's criteria.
	 * 
	 * @param selector - {@link zoranodensha.api.vehicles.part.util.OrderedPartsList.IPartsSelector Selector} to filter which parts should be included in the returned list.
	 * @return A new {@link zoranodensha.api.vehicles.part.util.OrderedPartsList parts list} holding all parts that matched the given selector.
	 */
	public ConcatenatedPartsList getParts(IPartsSelector selector)
	{
		/* If there is no cached list, calculate and store in cache. */
		if (!partsCache.containsKey(selector))
		{
			partsCache.put(selector, new ConcatenatedPartsList(train, selector));
		}

		/* Retrieve the list matching this selector and return it. It cannot be modified anyways. */
		return partsCache.get(selector);
	}

	/**
	 * Returns a list holding all vehicle part types matching the given selector's criteria.
	 * 
	 * @param selector - {@link zoranodensha.api.vehicles.part.util.OrderedTypesList.ITypesSelector Selector} to filter which part types should be included in the returned list.
	 * @return A new {@link zoranodensha.api.vehicles.part.util.OrderedTypesList types list} holding all part types that matched the given selector.
	 */
	@SuppressWarnings("unchecked")
	public <T extends APartType> ConcatenatedTypesList<T> getTypes(ITypesSelector<T> selector)
	{
		/* If there is no cached list, calculate and store in cache. */
		if (!typesCache.containsKey(selector))
		{
			typesCache.put(selector, new ConcatenatedTypesList<T>(train, selector));
		}

		/* Retrieve the list matching this selector and return it. It cannot be modified anyways. */
		return (ConcatenatedTypesList<T>)typesCache.get(selector);
	}

	/**
	 * Returns the index of the given object.
	 * 
	 * @return The given object's index, or {@code -1} if the object isn't contained in this list.
	 */
	@Override
	public int indexOf(Object o)
	{
		if (o != null)
		{
			for (int i = 0; i < array.length; ++i)
			{
				if (o.equals(array[i]))
				{
					return i;
				}
			}
		}

		return -1;
	}

	/**
	 * Returns a modified version of Java's iterator which allows read-only list access.<br>
	 * Calls to its {@link java.util.Iterator#remove() remove()} method will raise an {@link UnsupportedOperationException}.
	 */
	@Nonnull
	@Override
	public Iterator<VehicleData> iterator()
	{
		return new Itera();
	}

	/**
	 * Returns a modified version of Java's list iterator which allows read-only list access.<br>
	 * Calls to its {@link java.util.ListIterator#add(Object) add()}, {@link java.util.Iterator#remove() remove()} and
	 * {@link java.util.ListIterator#set(Object) set()} methods will raise an {@link UnsupportedOperationException}.
	 */
	@Nonnull
	@Override
	public ListIterator<VehicleData> listIterator(final int index)
	{
		return new ListItera(index);
	}

	/**
	 * Does a range check to ensure the given index is within the backed
	 * array's bounds. Throws an exception if not.
	 * 
	 * @throws IndexOutOfBoundsException - If {@code Index < 0 || index >= size}
	 */
	private void rangeCheck(int index)
	{
		if (index < 0 || index >= array.length)
		{
			throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + array.length);
		}
	}

	/**
	 * Removes the element at the given index.
	 * 
	 * @return The element that was removed from the list.
	 * @throws IndexOutOfBoundsException If the specified index is not within range.
	 */
	@Override
	public VehicleData remove(int index)
	{
		rangeCheck(index);
		modCount++;
		VehicleData rem = array[index];

		int numMoved = array.length - index - 1;
		if (numMoved > 0)
		{
			System.arraycopy(array, index + 1, array, index, numMoved);
		}

		int newSize = array.length - 1;
		array[newSize] = null;
		ensureCapacity(newSize);
		hasChanged = true;

		return rem;
	}

	/**
	 * Removes the specified element from this list.
	 * 
	 * @return {@code true} if the element was successfully removed from the list.
	 */
	@Override
	public boolean remove(Object o)
	{
		int i = indexOf(o);
		if (i >= 0)
		{
			return (remove(i) != null);
		}
		return false;
	}

	/**
	 * Returns the size of this list.
	 */
	@Override
	public int size()
	{
		return array.length;
	}



	/**
	 * Modified version of the iterator class used by {@link java.util.AbstractList AbstractList}.<br>
	 * <br>
	 * No copyright claims are made on this class.
	 */
	private class Itera implements Iterator<VehicleData>
	{
		int cursor = 0;
		int expectedModCount = modCount;



		protected final void checkForComodification()
		{
			if (modCount != expectedModCount)
			{
				throw new ConcurrentModificationException();
			}
		}

		@Override
		public boolean hasNext()
		{
			return (cursor != size());
		}

		@Override
		public VehicleData next()
		{
			checkForComodification();
			try
			{
				VehicleData next = get(cursor);
				++cursor;
				return next;
			}
			catch (IndexOutOfBoundsException e)
			{
				checkForComodification();
				throw new NoSuchElementException();
			}
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * Modified version of the list iterator class used by {@link java.util.AbstractList AbstractList}.<br>
	 * <br>
	 * No copyright claims are made on this class.
	 */
	private class ListItera extends Itera implements ListIterator<VehicleData>
	{
		ListItera(int index)
		{
			cursor = index;
		}

		@Override
		public void add(VehicleData e)
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean hasPrevious()
		{
			return (cursor != 0);
		}

		@Override
		public int nextIndex()
		{
			return cursor;
		}

		@Override
		public VehicleData previous()
		{
			checkForComodification();
			try
			{
				VehicleData previous = get(cursor);
				--cursor;
				return previous;
			}
			catch (IndexOutOfBoundsException e)
			{
				checkForComodification();
				throw new NoSuchElementException();
			}
		}

		@Override
		public int previousIndex()
		{
			return (cursor - 1);
		}

		@Override
		public void set(VehicleData e)
		{
			throw new UnsupportedOperationException();
		}
	}
}