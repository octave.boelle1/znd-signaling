package zoranodensha.api.vehicles.util;

import net.minecraft.util.Vec3;

/**
 * Wrapper object to hold position and rotation values.
 */
public class PositionStackEntry implements Cloneable
{
	/** Position */
	public double x, y, z;
	/** Rotation */
	public float roll, yaw, pitch;



	/**
	 * Initialise a new entry with the given position, leaving rotation fields blank.
	 */
	public PositionStackEntry(double x, double y, double z)
	{
		setPosition(x, y, z);
	}

	/**
	 * Initialise a new entry with the given values.
	 */
	public PositionStackEntry(double x, double y, double z, float roll, float yaw, float pitch)
	{
		setPosition(x, y, z);
		setRotation(roll, yaw, pitch);
	}

	/**
	 * Returns a new instance with this entry's data.
	 */
	public PositionStackEntry copy()
	{
		return new PositionStackEntry(x, y, z, roll, yaw, pitch);
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		long tmp;
		
		int result = 1;
		{
			result = prime * result + Float.floatToIntBits(yaw);
			result = prime * result + Float.floatToIntBits(pitch);
			result = prime * result + Float.floatToIntBits(roll);
			
			tmp = Double.doubleToLongBits(x);
			result = prime * result + (int)(tmp ^ (tmp >>> 32));
			tmp = Double.doubleToLongBits(y);
			result = prime * result + (int)(tmp ^ (tmp >>> 32));
			tmp = Double.doubleToLongBits(z);
			result = prime * result + (int)(tmp ^ (tmp >>> 32));
		}
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		
		if (obj instanceof PositionStackEntry)
		{
			PositionStackEntry other = (PositionStackEntry)obj;
			
			if (Float.floatToIntBits(yaw) != Float.floatToIntBits(other.yaw))
				return false;
			if (Float.floatToIntBits(pitch) != Float.floatToIntBits(other.pitch))
				return false;
			if (Float.floatToIntBits(roll) != Float.floatToIntBits(other.roll))
				return false;
			
			if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
				return false;
			if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
				return false;
			if (Double.doubleToLongBits(z) != Double.doubleToLongBits(other.z))
				return false;
			
			return true;
		}
		
		return false;
	}

	/**
	 * Offsets this position by the given parameters.
	 */
	public void offset(double x, double y, double z)
	{
		this.x += x;
		this.y += y;
		this.z += z;
	}

	/**
	 * Set position to given values.
	 */
	public void setPosition(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * Set rotation to given values.
	 */
	public void setRotation(float roll, float yaw, float pitch)
	{
		this.roll = roll;
		this.yaw = yaw;
		this.pitch = pitch;
	}

	public Vec3 toVec3() {
		return Vec3.createVectorHelper(x, y, z);
	}

	@Override
	public String toString()
	{
		return String.format("PositionPair[x=%.2f, y=%.2f, z=%.2f, roll=%.2f, yaw=%.2f, pitch=%.2f]", x, y, z, roll, yaw, pitch);
	}
}