package zoranodensha.api.vehicles.part.property;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * <h1>Vehicle Part Property</h1>
 * <hr>
 * 
 * <p>
 * Vehicle part properties are offered through an interface rich in functionality.<br>
 * Properties using this interface decide whether or not they interact with {@link net.minecraft.nbt.NBTBase NBT},
 * if and when their data is synchronised, and whether it may be changed in the Model Editor.
 * </p>
 * 
 * <p>
 * Furthermore, properties of this interface can make use of automatic synchronisation between client and server.<br>
 * To <b>enable property synchronisation</b>:
 * <ul>
 * <li>{@link #setSyncDir(ESyncDir) Set} the direction of synchronisation and</li>
 * <li>Ensure valid {@link #setValidTagCalls(EValidTagCalls) tag calls} include packet calls.</li>
 * </ul>
 * </p>
 * 
 * <p>
 * In most cases, property values should persist between world saves and such.<br>
 * To <b>enable property NBT writing/ reading</b>:
 * <ul>
 * <li>{@link #setValidTagCalls(EValidTagCalls) Define} when the property may be written to/ read from NBT.</li>
 * </ul>
 * </p>
 * 
 * @param <TYPE> - Object type of the property's wrapped value.
 * 
 * @author Leshuwa Kaiheiwa
 */
public interface IPartProperty<TYPE>
{
	/**
	 * Return the value held by this property.
	 * 
	 * @return The property value. May be {@code null}.
	 */
	TYPE get();

	/**
	 * Return whether the value of this property has changed and needs to be synchronised.
	 * 
	 * @return {@code true} if this property has changed.
	 */
	boolean getHasChanged();

	/**
	 * Return whether this property may be configured in the Model Editor.
	 * 
	 * @return {@code true} if this property can be configured.
	 */
	boolean getIsConfigurable();

	/**
	 * Return whether the given NBT tag call is valid for this property.
	 * 
	 * @return {@code true} if the {@link zoranodensha.api.util.ETagCall tag call} is valid.
	 */
	boolean getIsValidTagCall(ETagCall tagCall);

	/**
	 * Return the key of this property, unique within its {@link #getParent() parent} part.
	 * 
	 * @return This property's unique key.
	 */
	int getKey();

	/**
	 * Return the basic and unlocalised name of this property, without any prefixes or suffixes.
	 * 
	 * @return The property's basic name.
	 */
	String getName();

	/**
	 * Return the parent of this property, i.e. the vehicle part this property belongs to.
	 * 
	 * @return This property's parent {@link zoranodensha.api.vehicles.part.VehParBase part}.
	 */
	VehParBase getParent();

	/**
	 * Return the direction of synchronisation this property may be synchronised in.
	 * 
	 * @return {@link zoranodensha.api.util.ESyncDir Direction} in which this property shall be synchronised.
	 */
	ESyncDir getSyncDir();

	/**
	 * Return the class of the wrapped value type.
	 * 
	 * @return {@link java.lang.Class Class} of the property value.
	 */
	Class<TYPE> getTypeClass();

	/**
	 * Return the property name as a localisation key.
	 * 
	 * @return Unlocalised property name.
	 */
	String getUnlocalisedName();

	/**
	 * Return the tag calls accepted by this property.
	 * 
	 * @return The {@link zoranodensha.api.util.EValidTagCalls tag calls} accepted by this property.
	 */
	EValidTagCalls getValidTagCalls();

	/**
	 * Read this property from the given NBT tag.
	 * 
	 * @param nbt - {@link net.minecraft.nbt.NBTTagCompound NBT tag} to read from.
	 * @param callType - NBT interaction {@link zoranodensha.api.util.ETagCall call type}.
	 */
	void readFromNBT(NBTTagCompound nbt, ETagCall callType);

	/**
	 * Override the previous property with the given value.<br>
	 * If the property value changed, trigger synchronisation by calling {@link #setHasChanged(boolean)}.
	 * 
	 * @param val - New property value.
	 * @return {@code true} if this property's value was overwritten.
	 */
	boolean set(Object val);

	/**
	 * Set this property as configurable.<br>
	 * This will make it appear in the Model Editor.
	 * 
	 * @return This property.
	 */
	IPartProperty<TYPE> setConfigurable();

	/**
	 * Set whether this property value has changed and now waits for synchronisation.
	 * 
	 * @param hasChanged - {@code true} if the value of this property has changed.
	 */
	void setHasChanged(boolean hasChanged);

	/**
	 * Set this property's synchronisation direction.
	 * 
	 * @param syncDir - This property's {@link zoranodensha.api.util.ESyncDir direction} of synchronisation.
	 * @return This property.
	 */
	IPartProperty<TYPE> setSyncDir(ESyncDir syncDir);

	/**
	 * Set which tag calls are valid for this property.<br>
	 * This decides during which NBT actions this property will be written/ read.
	 * 
	 * @param validTagCalls - The type of {@link EValidTagCalls valid tag calls}.
	 */
	IPartProperty<TYPE> setValidTagCalls(EValidTagCalls validTagCalls);

	/**
	 * Writes this property to the given NBT tag.
	 * 
	 * @param nbt - {@link net.minecraft.nbt.NBTTagCompound NBT tag} to write to.
	 * @param callType - NBT interaction {@link zoranodensha.api.util.ETagCall call type}.
	 */
	void writeToNBT(NBTTagCompound nbt, ETagCall callType);
}