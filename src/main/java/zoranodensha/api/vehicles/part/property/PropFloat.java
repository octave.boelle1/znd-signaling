package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding float values.
 */
public class PropFloat extends APartProperty<Float> implements IXMLProperty<Float>
{
	/** Threshold value below which all values in {@link #set(Object)} are regarded as {@code 0.0F}. */
	public static final float ZERO_THRESHOLD = 0.0001F;



	public PropFloat(VehParBase parent, String name)
	{
		this(parent, 0.0F, name);
	}

	public PropFloat(VehParBase parent, float val, String name)
	{
		super(parent, Float.class, name);
		set(val);
	}


	@Override
	public PropFloat copy(VehParBase parent)
	{
		return new PropFloat(parent, get(), getName());
	}

	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		try
		{
			return super.set(Float.parseFloat(s));
		}
		catch (NumberFormatException e)
		{
			return false;
		}
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (nbt.hasKey(nbtKey))
		{
			set(nbt.getFloat(nbtKey));
			return true;
		}
		return false;
	}

	@Override
	public boolean set(Object val)
	{
		if (val instanceof Float)
		{
			/* If the value is below threshold, set as zero. */
			if (Math.abs((Float)val) < ZERO_THRESHOLD)
			{
				val = 0.0F;
			}
		}
		return super.set(val);
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		nbt.setFloat(nbtKey, get());
	}



	/**
	 * Properties holding bounded float values.
	 */
	public static class PropFloatBounded extends PropFloat
	{
		/** Lower bound, inclusive. */
		protected final float min;
		/** Upper bound, inclusive. */
		protected final float max;



		public PropFloatBounded(VehParBase parent, float min, float max, String name)
		{
			this(parent, 0.0F, min, max, name);
		}

		public PropFloatBounded(VehParBase parent, float val, float min, float max, String name)
		{
			super(parent, val, name);

			this.min = min;
			this.max = max;

			/*
			 * Initial value of 'val' might not have been assigned since it was initialised before its bounds.
			 * Ensure the value will be assigned after bounds are defined.
			 */
			set(val);
		}


		@Override
		public PropFloatBounded copy(VehParBase parent)
		{
			return new PropFloatBounded(parent, get(), min, max, getName());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Float)
			{
				Float newVal = (Float)property;
				if (newVal < min)
				{
					newVal = min;
				}
				else if (newVal > max)
				{
					newVal = max;
				}
				return super.set(newVal);
			}
			return false;
		}
	}
}