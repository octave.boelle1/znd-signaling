package zoranodensha.api.vehicles.part.property;

import zoranodensha.api.vehicles.part.VehParBase;



/**
 * <h1>Breaker Switch Property</h1>
 * <p>
 * Property to represent a breaker switch which could be used to control different components of a train.
 * </p>
 * <p>
 * A breaker has two states - {@code true} (set) in which the breaker is 'closed' and power can flow through it, and {@code false} (tripped) in which the breaker is 'open' and power cannot flow
 * through it.
 * </p>
 * 
 * @author Jaffa
 */
public class PropBreaker extends PropBoolean
{
	/**
	 * Called to initialise a new instance of the {@link zoranodensha.api.vehicles.part.property.PropBreaker} class. This breaker will start in the 'tripped' position.
	 * 
	 * @param parent - The parent vehicle part which owns this property.
	 * @param name - The NBT name to give to this property.
	 */
	public PropBreaker(VehParBase parent, String name)
	{
		super(parent, false, name);
	}

	/**
	 * Called to initialise a new instance of the {@link zoranodensha.api.vehicles.part.property.PropBreaker} class.
	 * 
	 * @param parent - The parent vehicle part which owns this property.
	 * @param state - The starting state of this breaker.
	 * @param name - The NBT name to give to this property.
	 */
	public PropBreaker(VehParBase parent, boolean state, String name)
	{
		super(parent, state, name);
	}

	/**
	 * Called to 'set' this breaker switch.
	 */
	public void set()
	{
		this.set(true);
	}

	/**
	 * Called to 'trip' this breaker switch.
	 */
	public void trip()
	{
		this.set(false);
	}

}
