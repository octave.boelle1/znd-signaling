package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding double values.
 */
public class PropDouble extends APartProperty<Double> implements IXMLProperty<Double>
{
	/** Threshold value below which all values in {@link #set(Object)} are regarded as {@code 0.0}. */
	public static final double ZERO_THRESHOLD = 0.000001;



	public PropDouble(VehParBase parent, String name)
	{
		this(parent, 0.0, name);
	}

	public PropDouble(VehParBase parent, double val, String name)
	{
		super(parent, Double.class, name);
		set(val);
	}


	@Override
	public PropDouble copy(VehParBase parent)
	{
		return new PropDouble(parent, get(), getName());
	}

	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		try
		{
			return super.set(Double.parseDouble(s));
		}
		catch (NumberFormatException e)
		{
			return false;
		}
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (nbt.hasKey(nbtKey))
		{
			set(nbt.getDouble(nbtKey));
			return true;
		}
		return false;
	}

	@Override
	public boolean set(Object val)
	{
		if (val instanceof Double)
		{
			/* If the value is below threshold, set as zero. */
			if (Math.abs((Double)val) < ZERO_THRESHOLD)
			{
				val = 0.0;
			}
		}
		return super.set(val);
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		nbt.setDouble(nbtKey, get());
	}



	/**
	 * Properties holding bounded double values.
	 */
	public static class PropDoubleBounded extends PropDouble
	{
		/** Lower bound, inclusive. */
		private final double min;
		/** Upper bound, inclusive. */
		private final double max;



		public PropDoubleBounded(VehParBase parent, double min, double max, String name)
		{
			this(parent, 0.0, min, max, name);
		}

		public PropDoubleBounded(VehParBase parent, double val, double min, double max, String name)
		{
			super(parent, val, name);

			this.min = min;
			this.max = max;

			/*
			 * Initial value of 'val' might not have been assigned since it was initialised before its bounds.
			 * Ensure the value will be assigned after bounds are defined.
			 */
			set(val);
		}


		@Override
		public PropDoubleBounded copy(VehParBase parent)
		{
			return new PropDoubleBounded(parent, get(), min, max, getName());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Double)
			{
				Double newVal = (Double)property;
				if (newVal < min)
				{
					newVal = min;
				}
				else if (newVal > max)
				{
					newVal = max;
				}
				return super.set(newVal);
			}
			return false;
		}
	}
}