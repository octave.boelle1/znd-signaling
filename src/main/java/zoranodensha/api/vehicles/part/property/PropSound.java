package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import javax.annotation.Nullable;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.MovingSound;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.api.vehicles.part.xml.XMLResourcePack;



/**
 * Property holding {@link BasicSound sounds}.
 */
public class PropSound extends PropIntegerBounded implements ITickableProperty<Integer>
{
	/** Resource location of the played sound. May be {@code null}. */
	protected ResourceLocation resLoc;

	/** The sound played by this property. */
	@SideOnly(Side.CLIENT) public BasicSound sound;



	/**
	 * Create a sound property without a specified sound. Used especially in combination with XML parts.
	 * 
	 * @param parent - Parent vehicle {@link zoranodensha.api.vehicles.part.VehParBase part}.
	 * @param name - The name of this property.
	 */
	public PropSound(VehParBase parent, String name)
	{
		super(parent, 0, -1, Integer.MAX_VALUE, name);

		/* Sounds are synchronised to client by default. */
		setSyncDir(ESyncDir.CLIENT);
		setValidTagCalls(EValidTagCalls.PACKET);
	}

	/**
	 * Create a sound property with given parent and name at given resource location.
	 * 
	 * @param parent - Parent vehicle {@link zoranodensha.api.vehicles.part.VehParBase part}.
	 * @param resLoc - String describing the sound's {@link net.minecraft.util.ResourceLocation resource location}.
	 * @param name - The name of this property.
	 */
	public PropSound(VehParBase parent, String resLoc, String name)
	{
		this(parent, name);

		/* Assign resource location. */
		String[] s = resLoc.split(":", 2);
		setResourceLocation(new ResourceLocation(s[0], s[1]));
	}


	@Override
	public PropSound copy(VehParBase parent)
	{
		PropSound sound = new PropSound(parent, getName());
		sound.setResourceLocation(resLoc);
		return sound;
	}

	/**
	 * Creates the actual sound object.<br>
	 * Override this method in order to return custom sound types.<br>
	 * <br>
	 * <b>Client-side only.</b>
	 */
	@SideOnly(Side.CLIENT)
	protected BasicSound getNewSound(ResourceLocation resLoc)
	{
		return new BasicSound(resLoc, this);
	}

	@Override
	public void onUpdate()
	{
		/* Updates are client-sided only. */
		if (getParent().getTrain().worldObj.isRemote)
		{
			updateSound();

			int playTicks = get();
			if (playTicks > 0)
			{
				set(--playTicks);
			}
		}
	}

	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		ResourceLocation resLoc = XMLResourcePack.INSTANCE.loadSound(zipFile, s);
		if (XMLResourcePack.INSTANCE.resourceExists(resLoc))
		{
			setResourceLocation(resLoc);
			return true;
		}
		return false;
	}

	/**
	 * Play this sound over indefinite time.<br>
	 * Triggers synchronisation to all tracking clients when called on server.
	 */
	public void play()
	{
		play(-1);
	}

	/**
	 * Play this sound for the given ticks' time.<br>
	 * Triggers a synchronsation to all tracking clients when called on server.
	 * 
	 * @param time - Any value greater than {@code 0} to specify an exact amount of ticks, or {@code -1} to play indefinitely.
	 */
	public void play(int time)
	{
		set(time);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt, ETagCall callType)
	{
		super.readFromNBT(nbt, callType);

		/* Extended NBT data containing default resource location. Used by XML part type overrides. */
		if (getParent().getIsXMLPart() && callType == ETagCall.SAVE && resLoc == null)
		{
			if (nbt.hasKey(nbtKey + "_defResLoc"))
			{
				String[] s = nbt.getString(nbtKey + "_defResLoc").split(":", 2);
				if (s.length == 2)
				{
					setResourceLocation(new ResourceLocation(s[0], s[1]));
				}
			}
		}
	}

	@Override
	public boolean set(Object property)
	{
		if (property instanceof Integer)
		{
			int val = MathHelper.clamp_int((Integer)property, min, max);
			if (this.property == null || val != this.property)
			{
				this.property = val;

				/* Only mark this property as changed if the parent train exists on server side. */
				if (getParent().getTrain() != null && !getParent().getTrain().worldObj.isRemote)
				{
					setHasChanged(true);
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * Override this sound's resource location with the given String.<br>
	 * <br>
	 * Behaviour in case of sound change while playing the sound is unknown.<br>
	 * Also note that changes to the resource location are not synchronised to clients.
	 * 
	 * @param resLoc - The {@link net.minecraft.util.ResourceLocation resource location} of this sound. May be {@code null}.
	 */
	public void setResourceLocation(@Nullable ResourceLocation resLoc)
	{
		this.resLoc = resLoc;
	}

	@Override
	public void setHasChanged(boolean hasChanged)
	{
		/* On server worlds, reset value to 0 after synchronisation. */
		if (!hasChanged && getParent().getTrain() != null && !getParent().getTrain().worldObj.isRemote)
		{
			property = 0;
		}
		super.setHasChanged(hasChanged);
	}

	/**
	 * Updates the sound object.<br>
	 * <br>
	 * <b>Client-side only.</b>
	 */
	@SideOnly(Side.CLIENT)
	protected void updateSound()
	{
		/* Discard the sound after timer has run out, or if the sound may not repeat and doesn't play anymore. */
		if (sound != null)
		{
			int playTicks = get();
			if ((playTicks == 0) || (playTicks < 0 && !sound.canRepeat() && !Minecraft.getMinecraft().getSoundHandler().isSoundPlaying(sound)))
			{
				sound.setDonePlaying();
				sound = null;
				set(0);
			}
		}

		/* If there is no sound existent, play a new one. */
		if (sound == null && get() != 0 && resLoc != null)
		{
			Minecraft.getMinecraft().getSoundHandler().playSound(sound = getNewSound(resLoc));
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt, ETagCall callType)
	{
		super.writeToNBT(nbt, callType);

		/* During XML part save calls, write default resource location if applicable. */
		if (getParent().getIsXMLPart() && callType == ETagCall.SAVE && resLoc != null)
		{
			nbt.setString(nbtKey + "_defResLoc", resLoc.toString());
		}
	}



	/**
	 * Base sound class.<br>
	 * Can be configured to handle most sound behavior, or extended for custom functionality.<br>
	 * <br>
	 * <b>Client-side only.</b>
	 */
	@SideOnly(Side.CLIENT)
	public static class BasicSound extends MovingSound
	{
		/** Parent {@link PropSound sound}. The part owning the parent will be followed by this sound. */
		protected final PropSound prop;



		public BasicSound(ResourceLocation resLoc, PropSound prop)
		{
			super(resLoc);
			this.prop = prop;
		}

		@Override
		public float getVolume()
		{
			return (prop.get() != 0) ? super.getVolume() : 0.0F;
		}

		/**
		 * Enable repetition of this sound.
		 */
		public BasicSound setCanRepeat()
		{
			repeat = true;
			return this;
		}

		/**
		 * Kills this sound.
		 */
		public void setDonePlaying()
		{
			donePlaying = true;
		}

		/**
		 * Override the pitch with the given value.
		 */
		public BasicSound setPitch(float pitch)
		{
			field_147663_c = (pitch < 0.0F) ? 0.0F : pitch;
			return this;
		}

		/**
		 * Override the volume with the given value.
		 */
		public BasicSound setVolume(float volume)
		{
			this.volume = (volume < 0.0F) ? 0.0F : volume;
			return this;
		}

		@Override
		public void update()
		{
			/* Stop this sound if the parent part or its parent train are null or dead. */
			VehParBase part = prop.getParent();
			if (part == null || part.getTrain() == null || part.getTrain().isDead)
			{
				setDonePlaying();
				return;
			}

			/*
			 * Update sound position from part.
			 */
			Vec3 pos = PartHelper.getPosition(part);
			xPosF = (float)pos.xCoord;
			yPosF = (float)pos.yCoord;
			zPosF = (float)pos.zCoord;
		}
	}


	/**
	 * Sounds that play for indefinite time, or sounds that are looped.
	 */
	public static class PropSoundRepeated extends PropSound
	{
		public PropSoundRepeated(VehParBase parent, String resLoc, String name)
		{
			super(parent, resLoc, name);
		}

		@Override
		protected BasicSound getNewSound(ResourceLocation resLoc)
		{
			return super.getNewSound(resLoc).setCanRepeat();
		}
	}
}