package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding float values.
 */
public class PropString extends APartProperty<String> implements IXMLProperty<String>
{
	public PropString(VehParBase parent, String name)
	{
		this(parent, "", name);
	}

	public PropString(VehParBase parent, String val, String name)
	{
		super(parent, String.class, name);
		set(val);
	}


	@Override
	public PropString copy(VehParBase copyParent)
	{
		return new PropString(copyParent, (get() == null ? null : new String(get())), getName());
	}

	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		return set(s);
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (nbt.hasKey(nbtKey))
		{
			set(nbt.getString(nbtKey));
			return true;
		}
		return false;
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (get() != null && !get().isEmpty())
		{
			nbt.setString(nbtKey, get());
		}
	}
}