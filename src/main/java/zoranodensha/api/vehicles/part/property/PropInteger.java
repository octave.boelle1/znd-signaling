package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding integer values.
 */
public class PropInteger extends APartProperty<Integer> implements IXMLProperty<Integer>
{
	public PropInteger(VehParBase parent, String name)
	{
		this(parent, 0, name);
	}

	public PropInteger(VehParBase parent, int val, String name)
	{
		super(parent, Integer.class, name);
		set(val);
	}


	@Override
	public PropInteger copy(VehParBase parent)
	{
		return new PropInteger(parent, get(), getName());
	}

	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		try
		{
			return super.set(Integer.parseInt(s));
		}
		catch (NumberFormatException e)
		{
			return false;
		}
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (nbt.hasKey(nbtKey))
		{
			set(nbt.getInteger(nbtKey));
			return true;
		}
		return false;
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		nbt.setInteger(nbtKey, get());
	}



	/**
	 * Properties holding bounded integer values.
	 */
	public static class PropIntegerBounded extends PropInteger
	{
		/** Lower bound, inclusive. */
		protected final int min;
		/** Upper bound, inclusive. */
		protected final int max;



		public PropIntegerBounded(VehParBase parent, int min, int max, String name)
		{
			this(parent, 0, min, max, name);
		}

		public PropIntegerBounded(VehParBase parent, int val, int min, int max, String name)
		{
			super(parent, val, name);

			this.min = min;
			this.max = max;

			/*
			 * Initial value of 'val' might not have been assigned since it was initialised before its bounds.
			 * Ensure the value will be assigned after bounds are defined.
			 */
			set(val);
		}


		@Override
		public PropIntegerBounded copy(VehParBase parent)
		{
			return new PropIntegerBounded(parent, get(), min, max, getName());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Integer)
			{
				Integer newVal = (Integer)property;
				if (newVal < min)
				{
					newVal = min;
				}
				else if (newVal > max)
				{
					newVal = max;
				}
				return super.set(newVal);
			}
			return false;
		}
	}
}