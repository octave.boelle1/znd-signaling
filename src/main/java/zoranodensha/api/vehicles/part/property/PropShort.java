package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding short values.
 */
public class PropShort extends APartProperty<Short> implements IXMLProperty<Short>
{
	public PropShort(VehParBase parent, String name)
	{
		this(parent, (short)0, name);
	}

	public PropShort(VehParBase parent, short val, String name)
	{
		super(parent, Short.class, name);
		set(val);
	}


	@Override
	public PropShort copy(VehParBase parent)
	{
		return new PropShort(parent, get(), getName());
	}

	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		try
		{
			return super.set(Short.parseShort(s));
		}
		catch (NumberFormatException e)
		{
			return false;
		}
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (nbt.hasKey(nbtKey))
		{
			set(nbt.getShort(nbtKey));
			return true;
		}
		return false;
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		nbt.setShort(nbtKey, get());
	}



	/**
	 * Properties holding bounded integer values.
	 */
	public static class PropShortBounded extends PropShort
	{
		/** Lower bound, inclusive. */
		protected final short min;
		/** Upper bound, inclusive. */
		protected final short max;



		public PropShortBounded(VehParBase parent, short min, short max, String name)
		{
			this(parent, (short)0, min, max, name);
		}

		public PropShortBounded(VehParBase parent, short val, short min, short max, String name)
		{
			super(parent, val, name);

			this.min = min;
			this.max = max;

			/*
			 * Initial value of 'val' might not have been assigned since it was initialised before its bounds.
			 * Ensure the value will be assigned after bounds are defined.
			 */
			set(val);
		}


		@Override
		public PropShortBounded copy(VehParBase parent)
		{
			return new PropShortBounded(parent, get(), min, max, getName());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Short)
			{
				Short newVal = (Short)property;
				if (newVal < min)
				{
					newVal = min;
				}
				else if (newVal > max)
				{
					newVal = max;
				}
				return super.set(newVal);
			}
			return false;
		}
	}
}