package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.util.APILogger;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding {@link java.lang.Enum enum} values.
 * 
 * @param <E> Enum type parameter.
 */
public class PropEnum<E extends Enum<E>> extends APartProperty<E> implements IXMLProperty<E>
{
	public PropEnum(VehParBase parent, E val, String name)
	{
		super(parent, val.getDeclaringClass(), name);
		set(val);
	}


	@Override
	public PropEnum<E> copy(VehParBase parent)
	{
		return new PropEnum<E>(parent, get(), getName());
	}

	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		try
		{
			return set(Enum.valueOf(get().getDeclaringClass(), s));
		}
		catch (IllegalArgumentException e)
		{
			return false;
		}
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		/* Ensure NBT tag even contains this key. */
		if (!nbt.hasKey(nbtKey))
		{
			return false;
		}

		/* Special case: Enum#valueOf() may throw an exception if the given String doesn't match an enum type. */
		try
		{
			set(Enum.valueOf(get().getDeclaringClass(), nbt.getString(nbtKey)));
		}
		catch (IllegalArgumentException e)
		{
			/* If exception was thrown, log as warning. */
			APILogger.warn(this, String.format("Couldn't read enum value from NBT for key '%s': %s", nbtKey, e));
			return false;
		}
		return true;
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		nbt.setString(nbtKey, get().name());
	}
}