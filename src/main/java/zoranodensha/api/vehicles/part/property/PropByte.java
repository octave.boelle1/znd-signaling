package zoranodensha.api.vehicles.part.property;

import java.util.zip.ZipFile;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.vehicles.part.VehParBase;



/**
 * Properties holding byte values.
 */
public class PropByte extends APartProperty<Byte> implements IXMLProperty<Byte>
{
	public PropByte(VehParBase parent, String name)
	{
		this(parent, (byte)0, name);
	}

	public PropByte(VehParBase parent, byte val, String name)
	{
		super(parent, Byte.class, name);
		set(val);
	}

	
	@Override
	public PropByte copy(VehParBase parent)
	{
		return new PropByte(parent, get(), getName());
	}


	@Override
	public boolean parse(String s, ZipFile zipFile)
	{
		try
		{
			return set(Byte.parseByte(s));
		}
		catch (NumberFormatException e)
		{
			return false;
		}
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (nbt.hasKey(nbtKey))
		{
			set(nbt.getByte(nbtKey));
			return true;
		}
		return false;
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		nbt.setByte(nbtKey, get());
	}



	/**
	 * Properties holding bounded integer values.
	 */
	public static class PropByteBounded extends PropByte
	{
		/** Lower bound, inclusive. */
		protected final byte min;
		/** Upper bound, inclusive. */
		protected final byte max;



		public PropByteBounded(VehParBase parent, byte min, byte max, String name)
		{
			this(parent, (byte)0, min, max, name);
		}

		public PropByteBounded(VehParBase parent, byte val, byte min, byte max, String name)
		{
			super(parent, val, name);

			this.min = min;
			this.max = max;

			/*
			 * Initial value of 'val' might not have been assigned since it was initialised before its bounds.
			 * Ensure the value will be assigned after bounds are defined.
			 */
			set(val);
		}

		
		@Override
		public PropByteBounded copy(VehParBase parent)
		{
			return new PropByteBounded(parent, get(), min, max, getName());
		}

		@Override
		public boolean set(Object property)
		{
			if (property instanceof Byte)
			{
				Byte newVal = (Byte)property;
				if (newVal < min)
				{
					newVal = min;
				}
				else if (newVal > max)
				{
					newVal = max;
				}
				return super.set(newVal);
			}
			return false;
		}
	}
}