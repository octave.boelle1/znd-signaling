package zoranodensha.api.vehicles.part.rendering;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.vehicles.part.VehParBaseRenderer;
import zoranodensha.api.vehicles.part.util.expr.Expression;
import zoranodensha.api.vehicles.part.xml.XMLTreeNode;



/**
 * Basic transformation class.<br>
 * Subclasses provide transformation implementation for offset, rotation, and scale.<br>
 * <i>Client-side only.</i>
 */
@SideOnly(Side.CLIENT)
public abstract class ATransformation extends ARenderTag
{
	protected final Expression x;
	protected final Expression y;
	protected final Expression z;



	public ATransformation(XMLTreeNode node, String defaultExpression, VehParBaseRenderer renderer)
	{
		super(node);

		x = new Expression(parseNode(node, "x", defaultExpression), renderer.part);
		y = new Expression(parseNode(node, "y", defaultExpression), renderer.part);
		z = new Expression(parseNode(node, "z", defaultExpression), renderer.part);
	}


	/**
	 * Return the interpolated transformation value on X-axis.
	 */
	protected double getX(float partialTick)
	{
		if (x.getHasReference())
		{
			return x.getPrevValue() + (x.getValue() - x.getPrevValue()) * partialTick;
		}
		return x.getValue();
	}

	/**
	 * Return the interpolated transformation value on Y-axis.
	 */
	protected double getY(float partialTick)
	{
		if (y.getHasReference())
		{
			return y.getPrevValue() + (y.getValue() - y.getPrevValue()) * partialTick;
		}
		return y.getValue();
	}

	/**
	 * Return the interpolated transformation value on Z-axis.
	 */
	protected double getZ(float partialTick)
	{
		if (z.getHasReference())
		{
			return z.getPrevValue() + (z.getValue() - z.getPrevValue()) * partialTick;
		}
		return z.getValue();
	}

	/**
	 * Helper method to parse the given node for a child node of given name.<br>
	 * If the child node doesn't exist or the value wasn't a String, return the given default value.
	 */
	private String parseNode(XMLTreeNode node, String name, String defaultExpression)
	{
		XMLTreeNode childNode = node.getChildByName(name);
		if (childNode != null && !childNode.getValue().isEmpty())
		{
			return childNode.getValue();
		}
		return defaultExpression;
	}



	/**
	 * Offset tag. Translates following render data by given values along respective axes.
	 * 
	 * @see zoranodensha.api.vehicles.part.xml.ERendererTags#OFFSET OFFSET
	 */
	public static class Offset extends ATransformation
	{
		public Offset(XMLTreeNode node, VehParBaseRenderer renderer)
		{
			super(node, "0.0", renderer);
		}

		@Override
		public void apply(float partialTick, ItemRenderType renderType)
		{
			if (renderType == null || renderTypes == null || renderTypes.contains(renderType))
			{
				GL11.glTranslated(getX(partialTick), getY(partialTick), getZ(partialTick));
			}
			super.apply(partialTick, renderType);
		}
	}

	/**
	 * Rotation render tag. Rotates the following render data by given rotations about respective axes.
	 * 
	 * @see zoranodensha.api.vehicles.part.xml.ERendererTags#ROTATION ROTATION
	 */
	public static class Rotation extends ATransformation
	{
		public Rotation(XMLTreeNode node, VehParBaseRenderer renderer)
		{
			super(node, "0.0", renderer);
		}

		@Override
		public void apply(float partialTick, ItemRenderType renderType)
		{
			if (renderType == null || renderTypes == null || renderTypes.contains(renderType))
			{
				double x = getX(partialTick);
				if (x != 0.0F)
					GL11.glRotated(x, 1, 0, 0);

				double y = getY(partialTick);
				if (y != 0.0F)
					GL11.glRotated(y, 0, 1, 0);

				double z = getZ(partialTick);
				if (z != 0.0F)
					GL11.glRotated(z, 0, 0, 1);
			}
			super.apply(partialTick, renderType);
		}
	}

	/**
	 * Scale render tag. Scales the following render data by given values along respective axes.
	 * 
	 * @see zoranodensha.api.vehicles.part.xml.ERendererTags#SCALE SCALE
	 */
	public static class Scale extends ATransformation
	{
		public Scale(XMLTreeNode node, VehParBaseRenderer renderer)
		{
			super(node, "1.0", renderer);
		}

		@Override
		public void apply(float partialTick, ItemRenderType renderType)
		{
			if (renderType == null || renderTypes == null || renderTypes.contains(renderType))
			{
				GL11.glScaled(getX(partialTick), getY(partialTick), getZ(partialTick));
			}
			super.apply(partialTick, renderType);
		}
	}
}