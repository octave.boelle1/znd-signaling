package zoranodensha.api.vehicles.part.rendering;

import java.util.ArrayList;
import java.util.Iterator;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.model.obj.GroupObject;
import zoranodensha.api.vehicles.part.VehParBaseRenderer;
import zoranodensha.api.vehicles.part.xml.XMLTreeNode;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;



/**
 * Object render tag. Actually renders selected model pieces.<br>
 * <i>Client-side only.</i>
 * 
 * @see zoranodensha.api.vehicles.part.xml.ERendererTags#OBJ OBJ
 */
@SideOnly(Side.CLIENT)
public class Obj extends ARenderTag
{
	private final VehParBaseRenderer renderer;
	private final ObjectList objList;



	public Obj(XMLTreeNode node, VehParBaseRenderer renderer)
	{
		super(node);
		this.renderer = renderer;

		/* Filter invalid list specifications. */
		String pieces = node.getValue();
		if ("!".equals(pieces))
		{
			throw new IllegalArgumentException("Specified list of model objects is invalid!");
		}

		/* Filter model objects if required. */
		String[] groupObjectNames;
		if (pieces.isEmpty() || pieces.startsWith("!"))
		{
			ArrayList<GroupObject> groupObjects = renderer.model.obj.groupObjects;

			if (pieces.startsWith("!"))
			{
				groupObjectNames = pieces.split(",");
				Iterator<GroupObject> itera = groupObjects.iterator();

				while (itera.hasNext())
				{
					String name = itera.next().name;
					for (String objName : groupObjectNames)
					{
						if (objName.equals(name))
						{
							itera.remove();
							break;
						}
					}
				}
			}

			groupObjectNames = new String[groupObjects.size()];
			for (int i = 0; i < groupObjectNames.length; ++i)
			{
				groupObjectNames[i] = groupObjects.get(i).name;
			}
		}
		else
		{
			groupObjectNames = pieces.split(",");
		}

		/* Remove leading and trailing white space from model object names and compile object list. */
		for (int i = 0; i < groupObjectNames.length; ++i)
		{
			groupObjectNames[i] = groupObjectNames[i].trim();
		}

		objList = new ObjectList(renderer.model, groupObjectNames);
	}

	@Override
	public void apply(float partialTick, ItemRenderType renderType)
	{
		if (renderType == null || renderTypes == null || renderTypes.contains(renderType))
		{
			switch (MinecraftForgeClient.getRenderPass())
			{
				default:
					break;

				case -1:
					/* Render everything in item render pass. */
					objList.render();
					break;

				case 0:
					/* In opaque render pass, only render if there either is no active color or if the active color is non-transparent. */
					if (renderer.getActiveColor() == null || renderer.getActiveColor().getA() >= 1.0F)
					{
						objList.render();
					}
					break;

				case 1:
					/* Only render in transparent render pass if there is an active color AND its alpha channel value is less than one. */
					if (renderer.getActiveColor() != null && renderer.getActiveColor().getA() < 1.0F)
					{
						objList.render();
					}
					break;
			}
		}
		super.apply(partialTick, renderType);
	}
}