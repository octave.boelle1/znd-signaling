package zoranodensha.api.vehicles.part.rendering;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.util.APILogger;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.VehParBaseRenderer;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.xml.XMLTreeNode;



/**
 * Color render tag.<br>
 * <i>Client-side only.</i>
 * 
 * @see zoranodensha.api.vehicles.part.xml.ERendererTags#COLOUR COLOUR
 */
@SideOnly(Side.CLIENT)
public class Colour extends ARenderTag
{
	private final VehParBaseRenderer renderer;

	private PropColor property = null;
	private final ColorRGBA color = new ColorRGBA();



	/**
	 * Parses a color tag from the given node, accepting the following color formats:<br>
	 * > HEX values ({@code RRGGBB} or {@code RRGGBBAA} format)<br>
	 * > RGB / RGBA values<br>
	 * > Custom specified {@link zoranodensha.api.vehicles.part.property.PropColor color properties} declared as {@code $propertyName}.
	 */
	public Colour(XMLTreeNode node, VehParBaseRenderer renderer)
	{
		super(node);
		this.renderer = renderer;

		XMLTreeNode valueNode = node.getChildByName("val");
		if (valueNode != null)
		{
			String value = valueNode.getValue();
			if (value.startsWith("$"))
			{
				property = findProperty(value, PropColor.class, renderer.part);
				if (property == null)
				{
					APILogger.warn(this, String.format("Could not find colour property '%s' in part '%s'!", value, renderer.part.getName()));
					throw new NullPointerException("Colour rendering tag is missing its property.");
				}
			}
			else if (value.startsWith("#"))
			{
				color.set(value);
			}
			else
			{
				String[] rgb = value.split(",");
				switch (rgb.length)
				{
					case 3:
						try
						{
							color.setColor(Float.parseFloat(rgb[0]), Float.parseFloat(rgb[1]), Float.parseFloat(rgb[2]));
						}
						catch (NumberFormatException e)
						{
							/* Silent catch. */
						}
						break;

					case 4:
						try
						{
							color.setColor(Float.parseFloat(rgb[0]), Float.parseFloat(rgb[1]), Float.parseFloat(rgb[2]), Float.parseFloat(rgb[3]));
						}
						catch (NumberFormatException e)
						{
							/* Silent catch. */
						}
						break;
				}
			}
		}
	}

	@Override
	public void apply(float partialTick, ItemRenderType renderType)
	{
		if (renderType == null || renderTypes == null || renderTypes.contains(renderType))
		{
			/*
			 * Assign this color as active render color.
			 * Following model render calls execute depending on the render pass (either item, opaque, or transparent) and whether this color has a non-opaque alpha channel.
			 */
			if (property != null)
			{
				renderer.setActiveColor(property.get());
			}
			else
			{
				renderer.setActiveColor(color);
			}
		}
		super.apply(partialTick, renderType);
	}
}