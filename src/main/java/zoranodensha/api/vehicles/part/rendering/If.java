package zoranodensha.api.vehicles.part.rendering;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.vehicles.part.VehParBaseRenderer;
import zoranodensha.api.vehicles.part.util.expr.Expression;
import zoranodensha.api.vehicles.part.xml.XMLTreeNode;



/**
 * Optional render tag.<br>
 * <i>Client-side only.</i>
 * 
 * @see zoranodensha.api.vehicles.part.xml.ERendererTags#IF IF
 */
@SideOnly(Side.CLIENT)
public class If extends ARenderTag
{
	private final Expression expression;



	public If(XMLTreeNode node, VehParBaseRenderer renderer)
	{
		super(node);

		XMLTreeNode childNode = node.getChildByName("val");
		if (childNode == null || childNode.getValue().isEmpty())
		{
			throw new IllegalArgumentException("No property referenced for IF-tag!");
		}

		expression = new Expression(childNode.getValue(), renderer.part);
	}

	@Override
	public void apply(float partialTick, ItemRenderType renderType)
	{
		if (renderType == null || renderTypes == null || renderTypes.contains(renderType))
		{
			if (!expression.getIsTrue())
			{
				return;
			}
		}
		super.apply(partialTick, renderType);
	}
}