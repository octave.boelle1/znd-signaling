package zoranodensha.api.vehicles.part.type;

import net.minecraft.entity.player.EntityPlayer;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropFloat;



/**
 * Chain couplings.<br> Couple and decouple manually.
 */
public class PartTypeCouplingChain extends APartTypeCoupling
{
	/**
	 * A customisable value that indicates the distance between the maximum X of this coupler, and the maximum X of the buffer this coupler is associated with. If there is no buffer, this value can be left at {@code 0.0F}.
	 */
	public final PropFloat extraDimension;

	public PartTypeCouplingChain(VehParBase parent)
	{
		super(parent, PartTypeCouplingChain.class.getSimpleName());

		addProperty(extraDimension = (PropFloat)new PropFloat(parent, 0.0F, "extraDimension").setConfigurable());
	}

	@Override
	public boolean getIsCompatible(APartTypeCoupling coupling)
	{
		return (coupling instanceof PartTypeCouplingChain);
	}

	@Override
	public boolean onActivated(EntityPlayer player)
	{
		Train train = parent.getTrain();
		if (parent.getIsFinished() && train != null)
		{
			if (player.isSneaking() && !train.worldObj.isRemote)
			{
				return getIsCoupled() ? doTryDecouple() : doTryCouple();
			}
		}
		return false;
	}
}