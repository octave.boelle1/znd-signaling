package zoranodensha.api.vehicles.part.type;

import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.property.PropInteger;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Tickable;



/**
 * Engines of any kind.<br> These types take a source of power from within their parent {@link zoranodensha.api.vehicles.VehicleData vehicle} (or, in some cases, {@link zoranodensha.api.vehicles.Train train}) and produce force from the
 * source's power supply.
 *
 * @author Leshuwa Kaiheiwa
 */
public abstract class APartTypeEngine extends APartType implements IPartType_Tickable
{
	/**
	 * Whether this engine is switched on.
	 */
	public final PropBoolean enabled;

	/**
	 * Each tick, this engine's tractive force output (measured in kN) is calculated and stored in this property.
	 */
	protected final PropFloat producedForce;

	/**
	 * This engine's maximum power output, in kiloWatts (kW).
	 */
	public final PropInteger.PropIntegerBounded outputKW;

	public APartTypeEngine(VehParBase parent, String name)
	{
		super(parent, name);

		addProperty(enabled = (PropBoolean)new PropBoolean(parent, true, "engineEnabled").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(outputKW = (PropInteger.PropIntegerBounded)new PropInteger.PropIntegerBounded(parent, 1, 16000, "outputKW").setConfigurable());
		addProperty(producedForce = (PropFloat)new PropFloat(parent, 0.0F, "engineProducedForce").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
	}


	/**
	 * <p>
	 * Return the amount of force (measured in kiloNewton, kN) produced at the moment.
	 * </p>
	 *
	 * @return The produced force in kiloNewton (kN). Negative values indicate backward force (reversing).
	 */
	public abstract float getProducedForce();
}