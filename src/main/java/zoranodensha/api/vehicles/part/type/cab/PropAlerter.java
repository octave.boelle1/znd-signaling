package zoranodensha.api.vehicles.part.type.cab;

import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.type.PartTypeCab;



/**
 * Alerter property for cab types.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class PropAlerter extends PropIntegerBounded
{
	/** The {@link PartTypeCab cab type} this alerter belongs to. */
	private final PartTypeCab cab;



	public PropAlerter(PartTypeCab cab, String name)
	{
		super(cab.getParent(), 0, 600, name);
		this.cab = cab;

		setValidTagCalls(EValidTagCalls.PACKET_SAVE);
		setSyncDir(ESyncDir.CLIENT);
	}

	/**
	 * Decreases this alerter timer by one, if possible.
	 */
	public void decrease()
	{
		set(get() - 1);
	}

	/**
	 * Returns while the alerter level is low enough to emit a warning sound.
	 * 
	 * @return {@code true} if an alerter warning sound should be played.
	 */
	public boolean getDoAlert()
	{
		return get() <= (max / 8) && get() >= 1;
	}

	/**
	 * Returns while the alerter level is low enough to display a warning symbol in cabs with respective capability.
	 * 
	 * @return {@code true} if an alerter symbol may be displayed.
	 */
	public boolean getDoDisplay()
	{
		return get() <= (max / 4);
	}

	/**
	 * Returns whether this alerter has started counting down and thus is running.
	 * 
	 * @return {@code true} if this alerter is counting.
	 */
	public boolean getIsRunning()
	{
		return get() < max;
	}

	/**
	 * Resets this alerter timer to the maximum value.
	 */
	public void reset()
	{
		set(max);
	}

	/**
	 * Called to reduce this alerter's timer to a point where it immediately {@link #getDoDisplay() displays} the warning symbol.
	 */
	public void requestAlertness()
	{
		set(max / 5);
	}

	/**
	 * Update this alerter. If it reaches zero, sets the parent {@link #cab} into an emergency state.
	 */
	public void update()
	{
		decrease();

		if (get() <= 0)
		{
			cab.setEmergency();
		}
	}
}