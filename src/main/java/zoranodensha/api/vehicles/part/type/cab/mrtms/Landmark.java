package zoranodensha.api.vehicles.part.type.cab.mrtms;

public class Landmark
{
	private final float length;
	private final float location;
	private final float speedLimit;
	private final boolean station;
	private final boolean termination;



	public Landmark()
	{
		this(0.0F, false, false, 0.0F, -1.0F);
	}

	public Landmark(float location)
	{
		this(location, false, false, 0.0F, -1.0F);
	}

	public Landmark(float location, boolean isStation, boolean isTermination)
	{
		this(location, isStation, isTermination, 0.0F, -1.0F);
	}

	public Landmark(float location, boolean isStation, boolean isTermination, float length, float speedLimit)
	{
		this.location = location;
		this.station = isStation;
		this.termination = isTermination;
		this.length = length;
		this.speedLimit = speedLimit;
	}

	public boolean getIsStation()
	{
		return station;
	}

	public boolean getIsTermination()
	{
		return termination;
	}

	public float getLength()
	{
		return length;
	}

	public float getLocation()
	{
		return location;
	}

	public float getSpeedLimit()
	{
		return speedLimit;
	}
}
