package zoranodensha.api.vehicles.part.type;

import cpw.mods.fml.common.eventhandler.Event;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.IHopper;
import net.minecraft.tileentity.TileEntityHopper;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.SyncDir;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.event.partType.PartTypeGUIOpenEvent;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.property.PropInventory;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Activatable;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Tickable;
import zoranodensha.api.vehicles.part.type.PartTypeLoadable.ELoadState;
import zoranodensha.api.vehicles.part.util.PartHelper;



/**
 * Item transport type. Contains an inventory which accepts items through a filter.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class PartTypeTransportItem extends APartTypeTransport implements IHopper, IPartType_Activatable, IPartType_Tickable
{
	/** The inventory contained by this part type. */
	public final PropInventory inventory;
	/** Default inventory size. */
	public final PropIntegerBounded sizeInventoryDefault;

	/** Ticks to wait before moving the next slot stack. */
	@SyncDir(ESyncDir.NOSYNC) private int transferTimer;



	public PartTypeTransportItem(VehParBase parent)
	{
		this(parent, 9 * 2, 9, 9 * 4, PartTypeTransportItem.class.getSimpleName());
	}

	protected PartTypeTransportItem(VehParBase parent, int inventorySize, int minSlots, int maxSlots, String name)
	{
		super(parent, name);

		addProperty(inventory = (PropInventory)new PropInventory(parent, inventorySize, "inventory").setSyncDir(ESyncDir.CLIENT));
		addProperty(sizeInventoryDefault = (PropIntegerBounded)new PropIntegerBounded(parent, inventorySize, minSlots, maxSlots, "size")
		{
			@Override
			public boolean set(Object property)
			{
				/* Round the given new inventory size to the closest multiple of nine. */
				if (property instanceof Integer)
				{
					property = Math.round((Integer)property / 9.0F) * 9;
				}

				/* Override inventory if the size changed. */
				if (super.set(property))
				{
					inventory.setSize(get());
					return true;
				}
				return false;
			}
		}.setConfigurable());
	}

	@Override
	public void closeInventory()
	{
		/* Unused by default. */
	}

	@Override
	public ItemStack decrStackSize(int slot, int dec)
	{
		/* Retrieve the specified slot's ItemStack. */
		ItemStack slotStack = getStackInSlot(slot);
		if (slotStack != null)
		{
			/* If the stack contains no more items than requested, clear slot and return contents. */
			if (slotStack.stackSize <= dec)
			{
				setInventorySlotContents(slot, null);
				return slotStack;
			}

			/* Otherwise, split stack and return stack of requested size. */
			ItemStack returnStack = slotStack.splitStack(dec);
			if (slotStack.stackSize == 0)
			{
				slotStack = null;
			}
			setInventorySlotContents(slot, slotStack);

			return returnStack;
		}
		return null;
	}

	/**
	 * Returns the amount of items contained by this item transport part.
	 */
	protected int getFillAmount()
	{
		ItemStack[] inventory = this.inventory.get();
		if (inventory != null)
		{
			int containedItems = 0;
			{
				for (ItemStack slotStack : inventory)
				{
					if (slotStack != null)
					{
						containedItems += slotStack.stackSize;
					}
				}
			}
			return containedItems;
		}
		return 0;
	}

	@Override
	public float getFillPercentage()
	{
		/* Retrieve total number of items and inventory size of this part. */
		int containedItems = getFillAmount();
		int inventorySize = getSizeInventory() * getInventoryStackLimit();

		if (containedItems > 0 && inventorySize > 0)
		{
			/* Safety check to ensure we never exceed 100% fill volume. */
			return (containedItems > inventorySize) ? 1.0F : (float)containedItems / (float)inventorySize;
		}

		return 0.0F;
	}

	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}

	/**
	 * Determine the current load state from the parent part.
	 * 
	 * @return The currently active load state, or {@code null} if there is no loadable part type.
	 */
	protected ELoadState getLoadState()
	{
		PartTypeLoadable partTypeLoadable = parent.getPartType(PartTypeLoadable.class);
		return (partTypeLoadable != null) ? partTypeLoadable.getLoadState() : null;
	}

	@Override
	public int getSizeInventory()
	{
		return inventory.get().length;
	}

	@Override
	public ItemStack getStackInSlot(int slot)
	{
		ItemStack[] inventory = this.inventory.get();
		if (inventory != null && slot >= 0 && slot < inventory.length)
		{
			return inventory[slot];
		}
		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slot)
	{
		return null;
	}

	@Override
	public World getWorldObj()
	{
		return parent.getTrain().worldObj;
	}

	@Override
	public double getXPos()
	{
		AxisAlignedBB aabb = parent.getBoundingBox();
		return (aabb.maxX + aabb.minX) * 0.5D;
	}

	@Override
	public double getYPos()
	{
		AxisAlignedBB aabb = parent.getBoundingBox();
		return (aabb.maxY + aabb.minY) * 0.5D;
	}

	@Override
	public double getZPos()
	{
		AxisAlignedBB aabb = parent.getBoundingBox();
		return (aabb.maxZ + aabb.minZ) * 0.5D;
	}

	@Override
	public boolean hasCustomInventoryName()
	{
		return false;
	}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack itemStack)
	{
		return true;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player)
	{
		Vec3 vecPos = PartHelper.getPosition(parent);
		return (player.getDistanceSq(vecPos.xCoord, vecPos.yCoord, vecPos.zCoord) < 9.0D);
	}

	@Override
	public void markDirty()
	{
		if (inventory.get() != null)
		{
			inventory.setHasChanged(true);
		}
	}

	/**
	 * Tries to move the given slot's contents from the given inventory to the other inventory's given slot.
	 * 
	 * Code taken from {@link TileEntityHopper#func_145892_a(net.minecraft.tileentity.IHopper, net.minecraft.inventory.IInventory, int, int) TileEntityHopper},
	 * thus no copyright claims are made.<br>
	 * Slight changes may have been made.
	 *
	 * @return {@code true} if successful.
	 */
	protected boolean moveItemStack(IInventory inventoryFrom, IInventory inventoryTo, int slotFrom, int slotTo)
	{
		/* Retrieve stack in the origin slot, abort if empty or not extractable. */
		ItemStack itemStack = inventoryTo.getStackInSlot(slotFrom);
		if (itemStack == null)
		{
			return false;
		}
		else if ((inventoryTo instanceof ISidedInventory) && !((ISidedInventory)inventoryTo).canExtractItem(slotFrom, itemStack, slotTo))
		{
			return false;
		}

		/* Try to move a copy of the stack into the other inventory, then decrease respective slots stack size. */
		ItemStack stackCopy = itemStack.copy();
		ItemStack itemStack0 = TileEntityHopper.func_145889_a(inventoryFrom, inventoryTo.decrStackSize(slotFrom, 1), -1);

		if (itemStack0 == null || itemStack0.stackSize == 0)
		{
			inventoryTo.markDirty();
			return true;
		}

		inventoryTo.setInventorySlotContents(slotFrom, stackCopy);
		return false;
	}

	@Override
	public boolean onActivated(EntityPlayer player)
	{
		if (parent.getIsFinished() && !player.isSneaking() && isUseableByPlayer(player))
		{
			/* Create and post event. If cancelled, abort. */
			PartTypeGUIOpenEvent event = new PartTypeGUIOpenEvent(this, player);
			if (MinecraftForge.EVENT_BUS.post(event))
			{
				return false;
			}

			/* Also abort if the event has no (valid) result. */
			if (event.getResult() != Event.Result.ALLOW || event.mod == null)
			{
				return false;
			}

			/* If everything passed, open GUI for the player with given event data. */
			Train train = parent.getTrain();
			player.openGui(event.mod, event.modGuiID, train.worldObj, train.getEntityId(), parent.getVehicle().getID(), parent.getID());
			return true;
		}
		return false;
	}

	/**
	 * Called to scan for nearby {@link net.minecraft.tileentity.IHopper hoppers} who can load this inventory.<br>
	 * Attempts to do so if any such target was found.
	 *
	 * @return {@code true} if items were exchanged.
	 */
	protected boolean onLoad()
	{
		/* If this inventory mustn't load, abort. */
		ELoadState loadState = getLoadState();
		if (loadState != null && !loadState.doLoad())
		{
			return false;
		}

		/* Retrieve a suitable inventory from above. */
		IInventory iInventory = TileEntityHopper.func_145893_b(getWorldObj(), getXPos(), parent.getBoundingBox().maxY + 1.0D, getZPos());
		if (iInventory != null && !getIsEmpty(iInventory, 0))
		{
			if (iInventory instanceof ISidedInventory)
			{
				/* Access a sided inventory's slots and move stacks if possible. */
				ISidedInventory iSidedInv = (ISidedInventory)iInventory;
				int[] arr = iSidedInv.getAccessibleSlotsFromSide(0);
				if (arr != null && arr.length > 0)
				{
					for (int i : arr)
					{
						if (moveItemStack(this, iInventory, i, 0))
						{
							return true;
						}
					}
				}
			}
			else
			{
				/* Move stacks if possible. */
				int size = iInventory.getSizeInventory();
				for (int i = 0; i < size; ++i)
				{
					if (moveItemStack(this, iInventory, i, 0))
					{
						return true;
					}
				}
			}
		}
		else
		{
			/* If no inventory was found, try scanning for nearby item entities which may be absorbed. */
			EntityItem entityitem = TileEntityHopper.func_145897_a(getWorldObj(), getXPos(), parent.getBoundingBox().maxY, getZPos());
			if (entityitem != null)
			{
				return TileEntityHopper.func_145898_a(this, entityitem);
			}
		}
		return false;
	}

	/**
	 * Called to scan for nearby {@link net.minecraft.tileentity.IHopper hoppers} that this inventory may unload into.<br>
	 * Attempts to do so if any such target was found.
	 *
	 * @return {@code true} if items were exchanged.
	 */
	protected boolean onUnload()
	{
		/* If this inventory either mustn't unload or is empty, abort. */
		ELoadState loadState = getLoadState();
		if ((loadState != null && !loadState.doUnload()) || getIsEmpty(this, 0))
		{
			return false;
		}

		/* Retrieve a suitable inventory from below. */
		IInventory inventory = TileEntityHopper.func_145893_b(getWorldObj(), getXPos(), parent.getBoundingBox().minY - 1.0D, getZPos());
		if (inventory != null)
		{
			/* If an inventory was found, try to move items. */
			int size = getSizeInventory();
			for (int i = 0; i < size; ++i)
			{
				if (moveItemStack(inventory, this, i, 0))
				{
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public void onTick()
	{
		/* On server worlds, check whether items can be moved (i.e. loaded or unloaded). */
		Train train = parent.getTrain();
		if (!train.worldObj.isRemote)
		{
			if (transferTimer <= 0)
			{
				/* Bitwise OR to ensure both methods are called. */
				if (onUnload() | onLoad())
				{
					transferTimer = 4;
				}
			}
			else
			{
				--transferTimer;
			}
		}
	}

	@Override
	public void openInventory()
	{
		/* By default, nothing happens here. */
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack itemStack)
	{
		ItemStack[] inventory = this.inventory.get();
		if (inventory != null && slot >= 0 && slot < inventory.length)
		{
			/* Assign item stack to slot if allowed, and mark inventory as dirty. */
			inventory[slot] = itemStack;
			markDirty();

			/* If the slot has changed, refresh the parent train's properties (due to potential changes in vehicle mass). */
			Train train = parent.getTrain();
			if (train != null)
			{
				train.refreshProperties();
			}
		}
	}

	/**
	 * Returns whether the given inventory is empty.<br>
	 * <br>
	 * Code taken from {@link TileEntityHopper#func_152103_b(net.minecraft.inventory.IInventory, int) TileEntityHopper},
	 * thus no copyright claims are made.<br>
	 * Slight changes may have been made.
	 *
	 * @return {@code true} if the given {@link net.minecraft.inventory.IInventory inventory} is empty.
	 */
	protected static boolean getIsEmpty(IInventory inventory, int slot)
	{
		if (inventory instanceof ISidedInventory && slot > -1)
		{
			ISidedInventory iSidedInv = (ISidedInventory)inventory;
			int[] arr = iSidedInv.getAccessibleSlotsFromSide(slot);
			if (arr == null || arr.length <= 0)
			{
				return true;
			}

			for (int i : arr)
			{
				if (iSidedInv.getStackInSlot(arr[i]) != null)
				{
					return false;
				}
			}
		}
		else
		{
			int size = inventory.getSizeInventory();
			for (int i = 0; i < size; ++i)
			{
				if (inventory.getStackInSlot(i) != null)
				{
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public String getInventoryName()
	{
		return "vehiclePart." + getParent().getName() + ".name";
	}

	@Override
	public float getMass(float mass)
	{
		/* Number of items divided by stack limit to get percentage of filled slots. This is the weight in tonnes. */
		float massOfFreight = ((float)getFillAmount() / (float)getInventoryStackLimit());
		return mass + massOfFreight;
	}
}