package zoranodensha.api.vehicles.part.type;

import java.util.List;

import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.Entity;
import net.minecraft.entity.ai.EntityAIPanic;
import net.minecraft.entity.ai.EntityAITasks;
import net.minecraft.entity.ai.EntityAITasks.EntityAITaskEntry;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.util.PartHelper;



/**
 * This part type offers a customisable horn implementation.<br>
 * To fully utilise this part type, its {@link #sound} needs to be initialised.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class PartTypeHorn extends APartType
{
	/**
	 * An entity {@link net.minecraft.command.IEntitySelector selector} that selects entities who can panic.<br>
	 * More precisely, selects {@link net.minecraft.entity.passive.EntityAnimal animals} who have the
	 * {@link net.minecraft.entity.ai.EntityAIPanic EntityAIPanic} task available.
	 */
	public static final IEntitySelector selectorPanickingEntities = new IEntitySelector()
	{
		@Override
		public boolean isEntityApplicable(Entity entity)
		{
			if ((entity instanceof EntityAnimal) && entity.isEntityAlive())
			{
				for (Object taskEntry : ((EntityAnimal)entity).tasks.taskEntries)
				{
					if (((EntityAITaskEntry)taskEntry).action instanceof EntityAIPanic)
					{
						return true;
					}
				}
			}
			return false;
		}
	};

	/** Horn sound, played upon call to {@link #soundHorn()}. */
	public final PropSound sound;



	public PartTypeHorn(VehParBase parent)
	{
		this(parent, new PropSound(parent, "sound"));
	}

	public PartTypeHorn(VehParBase parent, PropSound propSound)
	{
		super(parent, PartTypeHorn.class.getSimpleName());
		addProperty(sound = propSound);
	}

	/**
	 * <p>
	 * Called to sound this horn, if conditions are met.
	 * </p>
	 * 
	 * <p>
	 * <i>Server-side only.</i>
	 * </p>
	 */
	public void soundHorn()
	{
		/* Don't call on client side. */
		Train train = parent.getTrain();
		if (!parent.getIsFinished() || train == null || train.worldObj.isRemote)
		{
			return;
		}

		/*
		 * Play the horn sound.
		 */
		sound.play();

		/*
		 * Try to scare off animals in the train's path.
		 */
		PartTypeCab activeCab = train.getActiveCab();
		if (activeCab == null)
		{
			return;
		}

		/* Create bounding box mask. */
		AxisAlignedBB mask;
		{
			/* The sound has a range between 64m to 128m. */
			double range = 64.0 + (train.worldObj.rand.nextDouble() * 64.0);

			/* Retrieve direction and position vector from active cab. */
			Vec3 vecView = PartHelper.getViewVec(activeCab.getParent(), false);
			Vec3 vecPos = PartHelper.getPosition(getParent());

			/* Get the mask between the target position and this part's position. */
			mask = AxisAlignedBB.getBoundingBox(-1.5, -1.5, -1.5, 1.5, 1.5, 1.5);
			mask.offset(vecPos.xCoord, vecPos.yCoord, vecPos.zCoord);
			mask.offset(vecView.xCoord * range, vecView.yCoord * range, vecView.zCoord * range);
			mask = getParent().getBoundingBox().func_111270_a(mask);
		}

		/* Get list of animals in the bounding box. */
		List<?> list = train.worldObj.selectEntitiesWithinAABB(EntityAnimal.class, mask, selectorPanickingEntities);
		List<?> entityAITaskEntries;
		EntityAnimal animal;
		EntityAITasks entityAITasks;
		EntityAITaskEntry entityAITaskEntry;

		/* Set new AI task for each animal to make them run away. */
		for (int i = list.size() - 1; i >= 0; --i)
		{
			animal = (EntityAnimal)list.get(i);
			entityAITasks = animal.tasks;
			entityAITaskEntries = entityAITasks.taskEntries;

			for (int j = entityAITaskEntries.size() - 1; j >= 0; --j)
			{
				entityAITaskEntry = (EntityAITaskEntry)entityAITaskEntries.get(j);
				if (entityAITaskEntry.action instanceof EntityAIPanic)
				{
					animal.setRevengeTarget(animal);
					entityAITasks.onUpdateTasks();
					break;
				}
			}
		}
	}
}