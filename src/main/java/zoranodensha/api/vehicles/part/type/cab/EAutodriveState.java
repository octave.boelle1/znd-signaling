package zoranodensha.api.vehicles.part.type.cab;

/**
 * <p>
 * List of 'states' that the autodrive system of an MTMS unit must be in to control how the train automatically reacts to certain situations.
 * </p>
 * 
 * @author Jaffa
 */
public enum EAutodriveState
{
	/**
	 * <p>
	 * The autodrive system is not operational.
	 * </p>
	 */
	INACTIVE,
	/**
	 * <p>
	 * The train drives forward at the maximum permissable speed, slowing for and stopping at STOP signals. Passenger doors do not open.
	 * </p>
	 */
	NORMAL,
	/**
	 * <p>
	 * The train remains stationary with the doors unlocked (usually at a station). To depart the station, the autodrive system should use the {@link EAutodriveState#STATION_DEPARTURE} state.
	 * </p>
	 */
	STATION_ARRIVAL,
	/**
	 * <p>
	 * The train drives forward at the maximum permissable speed. The train does not search for stations in this mode. This is to make sure the train doesn't stop at the same station again instantly.
	 * </p>
	 * <p>
	 * After 5-10 seconds in this state, and the train is sufficiently far away from the station it just stopped at, the autodrive system should return to {@link EAutodriveState#NORMAL} to allow the
	 * train to search for and stop at the next station.
	 * </p>
	 */
	STATION_DEPARTURE,
	/**
	 * <p>
	 * The train stops and changes direction, setting a new active cab in the new direction of travel.
	 * </p>
	 */
	TERMINATING
}
