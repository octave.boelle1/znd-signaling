package zoranodensha.api.vehicles.part.type.cab;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;
import zoranodensha.api.vehicles.part.type.PartTypeCab;



/**
 * <h1>Lever Controller Property for cabs</h1>
 * <p>
 * This lever moves smoothly on the client-side, and the server-side value follows by snapping onto each notch that the client side is closest to.
 * </p>
 * <p>
 * <b>Ensure this property is synced in both directions between the client and server, as information coming from the client side is used to determine what the server side value should snap to.</b>
 * </p>
 * 
 * @author Jaffa
 */
public class PropLever extends PropFloatBounded
{
	public static final int DEFAULT_STEP_COUNT = 8;
	public static final float DEFAULT_LEVER_SPEED = 0.05F;


	/** The distance between each notch of the controller. */
	private float step = 1.0F / (float)DEFAULT_STEP_COUNT;
	/** How fast the lever moves. */
	private float leverSpeed = DEFAULT_LEVER_SPEED;

	/**
	 * This is {@code true} when the lever is decreasing at {@link PropLever#leverSpeed} per tick on the client-side.
	 */
	@SideOnly(Side.CLIENT) public boolean decreasing;
	/**
	 * This is {@code true} when the lever is increasing at {@link PropLever#leverSpeed} per tick on the client-side.
	 */
	@SideOnly(Side.CLIENT) public boolean increasing;



	/**
	 * <p>
	 * Initialises a new instance of the {@link PropLever} class.
	 * </p>
	 * 
	 * @param cab - The parent cab that contains this lever.
	 * @param name - The string name that this instance will use when reading and writing from the NBT.
	 */
	public PropLever(PartTypeCab cab, String name)
	{
		super(cab.getParent(), 0.0F, 1.0F, name);

		setValidTagCalls(EValidTagCalls.PACKET_SAVE);
	}


	/**
	 * <p>
	 * Initialises a new instance of the {@link PropLever} class.
	 * </p>
	 * 
	 * @param cab - The parent cab that contains this lever.
	 * @param name - The string name that this instance will use when reading and writing from the NBT.
	 * @param notches - The number of physical 'notches' this lever has that the client side lever will snap to.
	 * @param leverSpeed - The maximum speed that the lever will move at on the client side when either {@link PropLever#increasing} or {@link PropLever#decreasing} are {@code true}.
	 */
	public PropLever(PartTypeCab cab, String name, int notches, float leverSpeed)
	{
		super(cab.getParent(), 0.0F, 1.0F, name);

		this.step = 1.0F / (float)notches;
		this.leverSpeed = leverSpeed;

		setValidTagCalls(EValidTagCalls.PACKET_SAVE);
	}


	public PropLever(PartTypeCab cab, String name, int notches, float leverSpeed, boolean doubleSided)
	{
		super(cab.getParent(), doubleSided ? -1.0F : 0.0F, 1.0F, name);

		this.step = 1.0F / (float)notches;
		this.leverSpeed = leverSpeed;

		setValidTagCalls(EValidTagCalls.PACKET_SAVE);
	}


	/**
	 * <p>
	 * Gets the current notch this lever is closest to, rounded to the nearest integer.
	 * </p>
	 * 
	 * @return - An {@code int} representing the closest notch this lever is in.
	 */
	public int getCurrentNotch()
	{
		return (int)(Math.round(get() / step));
	}


	/**
	 * <p>
	 * Sets this lever to the specified notch.
	 * </p>
	 * <p>
	 * For example, if this lever has a total of 12 notches, and you use this method to set the notch to 6, the lever will move to 50% (or internally as {@code 0.5F}, because this class extends from a
	 * {@link PropFloatBounded}).
	 * </p>
	 * 
	 * @param notch - The notch to set this lever to, supplied as an {@code int}.
	 */
	public void setNotch(int notch)
	{
		set(notch * step);
	}


	/**
	 * <p>
	 * Called to perform an update tick to this lever.
	 * </p>
	 * <p>
	 * On the client side, this lever will smoothly move between notches assuming {@link PropLever#increasing} or {@link PropLever#decreasing} are true. On the server side, the internal {@code float}
	 * value of this lever will merely follow what the client side lever is doing, snapped to the neareset notch.
	 * </p>
	 * 
	 * @param isRemote - Whether this method is being called from the client side.
	 * @return - An {@code int} which is {@code >= 0} if a sound effect should be played. The returned integer represents the index of the sound to play.
	 */
	public Integer tick(boolean isRemote)
	{
		/*
		 * Client-side Logic
		 */
		if (isRemote)
		{
			/*
			 * Remember what notch this lever was at beforehand.
			 */
			float oldPosition = Math.round(get() / step);


			/*
			 * The lever is being moved
			 */
			if (increasing || decreasing)
			{
				if (decreasing)
				{
					set(get() - leverSpeed);
				}

				if (increasing)
				{
					set(get() + leverSpeed);
				}
			}

			/*
			 * The lever is automatically snapping into a notch.
			 */
			else
			{
				/*
				 * Determine which notch this lever should snap into.
				 */
				float notchTarget = Math.round(get() / step) * step;

				/*
				 * Moving up to the notch.
				 */
				if (get() > notchTarget + leverSpeed)
				{
					set(get() - leverSpeed);
				}

				/*
				 * Moving down to the notch.
				 */
				else if (get() < notchTarget - leverSpeed)
				{
					set(get() + leverSpeed);
				}

				/*
				 * Already snapped onto the notch.
				 */
				else
				{
					set(notchTarget);
				}
			}


			/*
			 * Determine if the lever has moved into a new notch.
			 */
			float newPosition = Math.round(get() / step);

			if (oldPosition != newPosition)
			{
				return (int)(newPosition);
			}

			return null;
		}
		else
		{
			return null;
		}
	}

}
