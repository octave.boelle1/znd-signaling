package zoranodensha.api.vehicles.part.type;

import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Tickable;



/**
 * Pantographs of any kind.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class PartTypePantograph extends APartType implements IPartType_Tickable
{
	/** {@code true} if this pantograph should be extended. */
	public final PropBoolean doExtend;
	/** Extension level of this pantograph. Ranges from {@code 0.0F} (fully retracted) to {@code 1.0F} (fully extended). */
	public final PropFloatBounded extension;



	public PartTypePantograph(VehParBase parent)
	{
		super(parent, PartTypePantograph.class.getSimpleName());
		addProperty(doExtend = (PropBoolean)new PropBoolean(parent, "doExtend").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(extension = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, 1.0F, "extension").setValidTagCalls(EValidTagCalls.PACKET_SAVE));
	}

	
	/**
	 * Returns the {@link #extension} of this pantograph.
	 */
	public float getExtension()
	{
		return extension.get();
	}
	
	/**
	 * @return {@code true} if this pantograph is fully retracted.
	 */
	public boolean getIsPantographDown()
	{
		return getExtension() <= 0.0F;
	}

	/**
	 * @return {@code true} if this pantograph is fully extended.
	 */
	public boolean getIsPantographUp()
	{
		return getExtension() >= 1.0F;
	}

	@Override
	public void onTick()
	{		
		/* Update pantograph extension level. */
		if (doExtend.get())
		{
			extension.set(getExtension() + 0.0250F);
		}
		else
		{
			extension.set(getExtension() - 0.0250F);
		}
	}

	/**
	 * Set whether this pantograph should go up or retract.
	 *
	 * @param doExtend - {@code true} if this pantograph should be extended.
	 */
	public void setDoExtend(boolean doExtend)
	{
		this.doExtend.set(doExtend);
	}
}