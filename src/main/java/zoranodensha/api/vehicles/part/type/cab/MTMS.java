package zoranodensha.api.vehicles.part.type.cab;

import java.util.ArrayList;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.Vec3;
import net.minecraft.world.ChunkCoordIntPair;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.ForgeChunkManager.Ticket;
import org.apache.logging.log4j.Level;
import zoranodensha.api.structures.signals.TrainPulse;
import zoranodensha.api.structures.signals.landmark.ALandmark;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.util.SyncDir;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.APartProperty;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropEnum;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;
import zoranodensha.api.vehicles.part.property.PropInteger;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.property.PropString;
import zoranodensha.api.vehicles.part.type.APartTypeEngine;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.type.PartTypeCab;
import zoranodensha.api.vehicles.part.type.PartTypeDoor;
import zoranodensha.api.vehicles.part.type.PartTypeLamp.ELightMode;
import zoranodensha.api.vehicles.part.type.PartTypePantograph;
import zoranodensha.api.vehicles.part.type.cab.PropReverser.EReverser;
import zoranodensha.api.vehicles.part.type.cab.mrtms.Landmark;
import zoranodensha.api.vehicles.part.type.cab.mrtms.Landscape;
import zoranodensha.api.vehicles.part.util.ConcatenatedTypesList;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.TrainDatabase;



/**
 * <h1>Minecraft Train Management System</h1>
 * <hr>
 * <p>
 * This is a centralised system of train management available to cabs. If there are multiple screens in a cab, they should all refer back to a singular instance of this class allowing for consistency with information about the train and
 * operating settings.
 * </p>
 *
 * @author Jaffa
 */
public class MTMS
{

	/**
	 * The maximum speed limit that MTMS will allow. The train may travel faster than this speed however it may cause an overspeed alarm or penalty brake application from MTMS.
	 */
	public static final int MAX_SPEED_LIMIT = 320;
	/**
	 * The minimum speed limit that MTMS will manage. If the train travels below this speed, MTMS will not intervene for any reason (including passing signals at STOP, etc).
	 */
	public static final int MIN_SPEED_LIMIT = 20;

	/**
	 * A counter which is used to make sure train information is collected at a consistent rate, and at a rate that is not too fast to prevent lag.
	 */
	private int informationCounter;
	/**
	 * A counter which keeps track of how long this MTMS instance has been isolated for. This ensures other MTMS instances have time to set up their chunkloading before this instance discards any of its own.
	 */
	private int dormantCounter;
	/**
	 * A counter which is used to make MTCS tick at a slower rate than the game's update rate.
	 */
	private int mtcsTickCounter;
	/**
	 * Any properties added to this list will trigger screen updates/refreshes when they are changed. An example would be values such as speed and braking values, when these values change it is important the screens are redrawn to reflect
	 * any changes in real-time.
	 */
	protected final ArrayList<IPartProperty<?>> monitoredProperties;
	/**
	 * A counter which is incremented when the MTMS systems requests an emergency brake application, and reset to 0 otherwise. This is used to 'delay' the application of an emergency brake to prevent sudden emergency brake applications due
	 * to glitches in the MTMS system.
	 */
	public int penaltyCounter;
	/**
	 * A counter which is used to continually redraw all screens at least at a minimum rate.
	 */
	@SideOnly(Side.CLIENT) protected int repaintCounter;
	/**
	 * An integer which, when incremented by 1, will cause all screens to redraw.
	 */
	@SyncDir(ESyncDir.NOSYNC) @SideOnly(Side.CLIENT) protected int repaintPointer;

	/**
	 * The parent cab part type.
	 */
	public final PartTypeCab parentCab;

	/*
	 * MTCS Information
	 */
	/**
	 * The current operation mode of the MTCS system.
	 */
	public final PropEnum<EMTMSOperationMode> operationMode;
	/**
	 * The most recently {@link Landscape} instance provided by the parent cab.
	 */
	public final PropLandscape lastLandscape;
	public final PropFloat landscapeOffset;
	/**
	 * A boolean which represents whether an automatic overspeed brake should be applied.
	 */
	public final PropBoolean penaltyBrake;

	/*
	 * Temporary Safety System Override Mode
	 */
	/**
	 * This is a counter which counts how many ticks the Override Mode enable button has been pressed for, only if the train is in the correct position to be able to enable the Override Mode. Otherwise this counter will be reset to 0.
	 */
	public final PropInteger overrideModeButtonCounter;
	/**
	 * This boolean is {@code true} when the Override Mode enable button is currently pressed.
	 */
	public final PropBoolean overrideModeButtonPressed;
	/**
	 * This boolean is {@code true} when the Override Mode is ready to be enabled.
	 */
	public final PropBoolean overrideModeCanBegin;
	/**
	 * This boolean is set to {@code true} when the Override Mode has been enabled and indicates that the train should be able to proceed past a stop signal at up to 40 km/h.
	 */
	public final PropBoolean overrideModeEnabled;

	/*
	 * Autodrive System
	 */
	/**
	 * A boolean which can be set to {@code true} to enable the autodrive system. This should be set to {@code true} in all cabs in the train for the autodrive system to function normally.
	 */
	public final PropBoolean autodriveEnable;
	/**
	 * The current state/task of the autodrive system.
	 */
	public final PropEnum<EAutodriveState> autodriveState;
	/**
	 * A counter that increments each tick when the train is stopped at a station stopping point.
	 */
	public final PropIntegerBounded autodriveStationArrivalCounter;
	/**
	 * A counter that increments each tick when the train is moving after departing a station stopping point.
	 */
	public final PropIntegerBounded autodriveStationDepartureCounter;
	/**
	 * A list of all chunk-loading tickets in use by MTMS.
	 */
	private ArrayList<Ticket> chunkTickets;

	/*
	 * Train Operating Information (Properties)
	 */
	/**
	 * The number of available chunks for Zora no Densha that can be claimed by MTMS instances.
	 */
	public final PropInteger autodriveChunksFree;
	/**
	 * The number of chunks that are currently loaded by the autodrive system of this particular MTMS instance.
	 */
	public final PropInteger autodriveChunksLoaded;
	/**
	 * The current headlight mode.
	 */
	public final PropEnum<ELightMode> currentLightMode;
	/**
	 * An integer which represents the number of open doors in the train.
	 */
	public final PropString listDoors;
	/**
	 * A serialised list of brake cylinder pressure values throughout the train.
	 */
	public final PropString listBrakeCylinders;
	/**
	 * A serialised list of brake pipes values throughout the train.
	 */
	public final PropString listBrakePipe;
	/**
	 * A serialised list of main reservoir pipe values throughout the train.
	 */
	public final PropString listMainReservoirPipe;
	/**
	 * A serialised list of pantograph statuses along the train.
	 */
	public final PropString listPantographs;
	/**
	 * A serialised list of park brake statuses.
	 */
	public final PropString listParkBrakes;
	/**
	 * A value that is updated quickly to be used as the realtime display of brake cylinder pressure.
	 */
	public final PropFloatBounded realtimeBrakeCylinderPressure1;
	/**
	 * A value that is updated quickly to be used as the realtime display of brake cylinder pressure.
	 */
	public final PropFloatBounded realtimeBrakeCylinderPressure2;
	/**
	 * A value that is updated quickly to be used as the realtime display of brake pipe pressure.
	 */
	public final PropFloatBounded realtimeBrakePipePressure;
	/**
	 * A value that is updated quickly to be used as the realtime display of main reservoir pressure.
	 */
	public final PropFloatBounded realtimeMainReservoirPressure;
	/**
	 * The controlled speed limit of the train. This number is affected by the current track speed and braking curves to obstructions.
	 */
	public final PropFloatBounded speedLimit;
	/**
	 * The next lower speed limit. This can be {@code 0.0F} if the next obstruction requires that the train stops.
	 */
	public final PropFloatBounded targetSpeed;
	/**
	 * The number of seconds before the current speed limit will begin to drop and the braking curve will take effect.
	 */
	public final PropFloat secondsUntilBrakingCurve;
	/**
	 * The current speed limit of the track the train is currently on.
	 */
	public final PropFloatBounded trackSpeed;
	/**
	 * A percentage (from {@code 0.0F} to {@code 1.0F}) indicating how much available tractive effort is being used by the train.
	 */
	public final PropFloatBounded trainEffortAcceleration;
	/**
	 * A percentage (from {@code 0.0F} to {@code 1.0F}) indicating how much available braking force is being applied throughout the train. A value of {@code 1.0F} indicates that all brakes in the train are applied with as much force as they
	 * can produce.
	 */
	public final PropFloatBounded trainEffortRetardation;
	/**
	 * The total length of the train in metres.
	 */
	public final PropFloat trainLength;
	/**
	 * The total mass of the train in metric tonnes.
	 */
	public final PropFloat trainMass;
	/**
	 * The speed of the train in {@code km/h}.
	 */
	public final PropFloat trainSpeed;
	/**
	 * The number of wagons that make up the train.
	 */
	public final PropInteger trainWagonCount;
	/**
	 * A value between 0.0F and 1.0F which should correlate to the current power controller level of the parent cab. This value should be manually modified by the parent cab to whatever value is appropriate.
	 */
	public final PropFloat cabPowerLevel;
	/**
	 * A value between 0.0F and 1.0F which should correlate to the current brake controller level of the parent cab. If there are multiple brake controllers, i.e. for different braking systems, the highest current value should be assigned
	 * to this variable. This value should be manually modified by the parent cab to whatever value is appropriate.
	 */
	public final PropFloat cabBrakeLevel;

	/*
	 * Client-Side Values used for calculating when to play sound effects.
	 */
	@SideOnly(Side.CLIENT) protected float targetSpeedPrevious;
	@SideOnly(Side.CLIENT) protected float trackSpeedPrevious;
	@SideOnly(Side.CLIENT) protected boolean overrideModeEnabledPrevious;
	@SideOnly(Side.CLIENT) protected boolean brakeInterventionPrevious;

	/*
	 * Unsynchronised Normal Variables
	 */
	private float calculatedDeceleration = 0.0F;
	private int playUpdateSoundTimeout = 0;

	/*
	 * Settings
	 */
	/**
	 * When {@code true}, the brakes will automatically be applied if any passenger door is open.
	 */
	public final PropBoolean automaticDoorBrake;
	/**
	 * When {@code true}, the doors of the train will open and close automatically rather than just unlocking.
	 */
	public final PropBoolean automaticDoorOpen;
	/**
	 * When {@code true}, the AFB/cruise control system will be enabled.
	 */
	public final PropBoolean cruiseControlEnabled;
	/**
	 * A number between {@code 5} and {@code 160} which represents the target cruise control speed. This is controlled by the AFB lever in the cab.
	 */
	public final PropFloatBounded cruiseControlTargetSpeed;
	/**
	 * When {@code true}, a majority of the information displayed on the DDU(s) will be displayed in imperial units.
	 */
	public final PropBoolean imperial;
	/**
	 * When {@code true}, all speed monitoring of MTMS will be suspended. The autodrive system will also cease to function. No speed limit will be enforced and the MTMS system will not actively check the track ahead.
	 */
	public final PropBoolean isolateMTCS;

	public final PropBoolean limitedSupervision;



	/**
	 * Called to initialise a new instance of the {@link zoranodensha.api.vehicles.part.type.cab.MTMS} class.
	 *
	 * @param parentCab - The cab part type which is assigned as the parent of this MTMS instance. Ideally there should only be one MTMS instance per cab.
	 */
	public MTMS(PartTypeCab parentCab)
	{
		/*
		 * Assign the parent cab.
		 */
		this.parentCab = parentCab;

		/*
		 * Set up the monitored properties list.
		 */
		this.monitoredProperties = new ArrayList<IPartProperty<?>>();

		/*
		 * Add Properties
		 */
		addProperty(autodriveChunksFree = (PropInteger)new PropInteger(parentCab.getParent(), 0, getPropertyNameFrom("autodriveChunksFree")).setSyncDir(ESyncDir.CLIENT));
		addProperty(autodriveChunksLoaded = (PropInteger)new PropInteger(parentCab.getParent(), 0, getPropertyNameFrom("autodriveChunksLoaded")).setSyncDir(ESyncDir.CLIENT));
		addProperty(autodriveEnable = (PropBoolean)new PropBoolean(parentCab.getParent(), false, getPropertyNameFrom("autodriveEnable")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(autodriveState = (PropEnum)new PropEnum<EAutodriveState>(parentCab.getParent(), EAutodriveState.INACTIVE, getPropertyNameFrom("autodriveState")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(autodriveStationArrivalCounter = (PropIntegerBounded)new PropIntegerBounded(parentCab.getParent(), 0, 0, 1000, getPropertyNameFrom("autodriveStationArrivalCounter")).setSyncDir(ESyncDir.NOSYNC).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(autodriveStationDepartureCounter = (PropIntegerBounded)new PropIntegerBounded(parentCab.getParent(), 0, 0, 200, getPropertyNameFrom("autodriveStationDepartureCounter")).setSyncDir(ESyncDir.NOSYNC).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(cabBrakeLevel = (PropFloatBounded)new PropFloat.PropFloatBounded(parentCab.getParent(), 0.0F, 0.0F, 1.0F, getPropertyNameFrom("cabBrakeLevel")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(cabPowerLevel = (PropFloatBounded)new PropFloat.PropFloatBounded(parentCab.getParent(), 0.0F, 0.0F, 1.0F, getPropertyNameFrom("cabPowerLevel")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(currentLightMode = (PropEnum)new PropEnum<ELightMode>(parentCab.getParent(), ELightMode.OFF, getPropertyNameFrom("currentLightMode")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(lastLandscape = (PropLandscape)new PropLandscape(parentCab.getParent(), "lastLandscape").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(landscapeOffset = (PropFloat)new PropFloat(parentCab.getParent(), "landscapeOffset").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(listDoors = (PropString)new PropString(parentCab.getParent(), getPropertyNameFrom("listDoors")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(listBrakeCylinders = (PropString)new PropString(parentCab.getParent(), getPropertyNameFrom("listBrakeCylinders")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(listBrakePipe = (PropString)new PropString(parentCab.getParent(), getPropertyNameFrom("listBrakePipe")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(listMainReservoirPipe = (PropString)new PropString(parentCab.getParent(), getPropertyNameFrom("listMainReservoirPipe")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(listPantographs = (PropString)new PropString(parentCab.getParent(), getPropertyNameFrom("listPantographs")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(listParkBrakes = (PropString)new PropString(parentCab.getParent(), getPropertyNameFrom("listParkBrakes")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(operationMode = (PropEnum)new PropEnum<EMTMSOperationMode>(parentCab.getParent(), EMTMSOperationMode.ISOLATION, getPropertyNameFrom("operationMode")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(overrideModeButtonCounter = (PropIntegerBounded)new PropInteger.PropIntegerBounded(parentCab.getParent(), 0, 0, 60, getPropertyNameFrom("overrideModeButtonCounter")).setSyncDir(ESyncDir.NOSYNC).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(overrideModeButtonPressed = (PropBoolean)new PropBoolean(parentCab.getParent(), false, getPropertyNameFrom("overrideModeButtonPressed")).setSyncDir(ESyncDir.NOSYNC).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(overrideModeCanBegin = (PropBoolean)new PropBoolean(parentCab.getParent(), false, getPropertyNameFrom("overrideModeCanBegin")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(overrideModeEnabled = (PropBoolean)new PropBoolean(parentCab.getParent(), false, getPropertyNameFrom("overrideModeEnabled")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(penaltyBrake = (PropBoolean)new PropBoolean(parentCab.getParent(), false, getPropertyNameFrom("penaltyBrake")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(realtimeBrakeCylinderPressure1 = (PropFloatBounded)new PropFloatBounded(parentCab.getParent(), 0.0F, 0.0F, 600.0F, getPropertyNameFrom("realtimeBCP1")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(realtimeBrakeCylinderPressure2 = (PropFloatBounded)new PropFloatBounded(parentCab.getParent(), 0.0F, 0.0F, 600.0F, getPropertyNameFrom("realtimeBCP2")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(realtimeBrakePipePressure = (PropFloatBounded)new PropFloatBounded(parentCab.getParent(), 0.0F, 0.0F, 600.0F, getPropertyNameFrom("realtimeBP")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(realtimeMainReservoirPressure = (PropFloatBounded)new PropFloatBounded(parentCab.getParent(), 0.0F, 0.0F, 1200.0F, getPropertyNameFrom("realtimeMR")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(speedLimit = (PropFloatBounded)new PropFloatBounded(parentCab.getParent(), -1.0F, -1.0F, MAX_SPEED_LIMIT, getPropertyNameFrom("speedLimit")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(targetSpeed = (PropFloatBounded)new PropFloatBounded(parentCab.getParent(), -1.0F, -1.0F, MAX_SPEED_LIMIT, getPropertyNameFrom("targetSpeed")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(secondsUntilBrakingCurve = (PropFloat)new PropFloat(parentCab.getParent(), -1.0F, getPropertyNameFrom("secondsUntilBrakingCurve")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(trackSpeed = (PropFloatBounded)new PropFloatBounded(parentCab.getParent(), 40.0F, -1.0F, MAX_SPEED_LIMIT, getPropertyNameFrom("trackSpeed")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(trainEffortAcceleration = (PropFloatBounded)new PropFloatBounded(parentCab.getParent(), 0.0F, 1.0F, getPropertyNameFrom("trainEffortAcceleration")).setValidTagCalls(EValidTagCalls.PACKET).setSyncDir(ESyncDir.CLIENT));
		addProperty(trainEffortRetardation = (PropFloatBounded)new PropFloatBounded(parentCab.getParent(), 0.0F, 1.0F, getPropertyNameFrom("trainEffortRetardation")).setValidTagCalls(EValidTagCalls.PACKET).setSyncDir(ESyncDir.CLIENT));
		addProperty(trainLength = (PropFloat)new PropFloat(parentCab.getParent(), getPropertyNameFrom("trainLength")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(trainMass = (PropFloat)new PropFloat(parentCab.getParent(), getPropertyNameFrom("trainMass")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(trainSpeed = (PropFloat)new PropFloat(parentCab.getParent(), 0.0F, getPropertyNameFrom("trainSpeed")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));
		addProperty(trainWagonCount = (PropInteger)new PropInteger(parentCab.getParent(), getPropertyNameFrom("trainWagonCount")).setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET));

		/*
		 * Add Settings
		 */
		addProperty(automaticDoorBrake = (PropBoolean)new PropBoolean(parentCab.getParent(), true, getPropertyNameFrom("automaticDoorBrake")).setSyncDir(ESyncDir.CLIENT));
		addProperty(automaticDoorOpen = (PropBoolean)new PropBoolean(parentCab.getParent(), false, getPropertyNameFrom("automaticDoorOpen")).setSyncDir(ESyncDir.CLIENT));
		addProperty(cruiseControlEnabled = (PropBoolean)new PropBoolean(parentCab.getParent(), false, getPropertyNameFrom("cruiseControlEnabled")).setSyncDir(ESyncDir.CLIENT));
		addProperty(cruiseControlTargetSpeed = (PropFloatBounded)new PropFloatBounded(parentCab.getParent(), 0.0F, 160.0F, getPropertyNameFrom("cruiseControlTargetSpeed")).setSyncDir(ESyncDir.CLIENT));
		addProperty(imperial = (PropBoolean)new PropBoolean(parentCab.getParent(), false, getPropertyNameFrom("imperial")).setSyncDir(ESyncDir.CLIENT));
		addProperty(isolateMTCS = (PropBoolean)new PropBoolean(parentCab.getParent(), false, getPropertyNameFrom("isolateMTCS")).setSyncDir(ESyncDir.CLIENT));
		addProperty(limitedSupervision = (PropBoolean)new PropBoolean(parentCab.getParent(), false, getPropertyNameFrom("limitedSupervision")).setSyncDir(ESyncDir.CLIENT));


		/*
		 * Ensure the correct properties are monitored.
		 */
		//		this.monitoredProperties.add(autodriveChunksFree);
		//		this.monitoredProperties.add(autodriveChunksLoaded);
		//		this.monitoredProperties.add(autodriveState);
		//		this.monitoredProperties.add(cruiseControlEnabled);
		//		this.monitoredProperties.add(cruiseControlTargetSpeed);
		//		this.monitoredProperties.add(listDoors);
		//		this.monitoredProperties.add(listPantographs);
		//		this.monitoredProperties.add(listParkBrakes);
		//		this.monitoredProperties.add(operationMode);
		//		this.monitoredProperties.add(realtimeBrakeCylinderPressure1);
		//		this.monitoredProperties.add(realtimeBrakeCylinderPressure2);
		//		this.monitoredProperties.add(realtimeBrakePipePressure);
		//		this.monitoredProperties.add(realtimeMainReservoirPressure);
		//		this.monitoredProperties.add(speedLimit);
		//		this.monitoredProperties.add(targetSpeed);
		//		this.monitoredProperties.add(trackSpeed);
		//		this.monitoredProperties.add(trainSpeed);




		/*
		 * Chunk Loading
		 */
		chunkTickets = new ArrayList<Ticket>();
	}


	/**
	 * A helper method which adds the supplied property to the parent cab's list of properties.
	 */
	private void addProperty(APartProperty property)
	{
		parentCab.getParent().addProperty(property);
	}


	/**
	 * <p>
	 * Helper method to determine if there are any open or unlocked doors in the train.
	 * </p>
	 *
	 * @return - A {@code boolean} which will be {@code true} if at least one door is open or unlocked in the train.
	 */
	protected boolean getDoorsOpen()
	{
		if (parentCab.getTrain() == null)
		{
			return false;
		}

		return PartTypeDoor.getDoorsOpen(parentCab.getTrain()) || PartTypeDoor.getDoorsUnlocked(parentCab.getTrain());
	}


	/**
	 * Returns the maximum throttle that can be applied at this moment.
	 *
	 * @return - A maximum throttle value, range from {@code 0.0F} (off) to {@code 1.0F} (maximum throttle).
	 */
	public float getMaximumThrottle()
	{
		float maximum = 1.0F;

		/*
		 * AFB
		 */
		if (cruiseControlEnabled.get())
		{
			float cruiseControlOverhead = cruiseControlTargetSpeed.get() - trainSpeed.get();

			maximum = Math.min(maximum, cruiseControlOverhead * 0.075F);
		}

		/*
		 * Doors (this stops the train from being able to apply power when any doors are open)
		 */
		if (getDoorsOpen())
		{
			maximum = 0.0F;
		}

		return maximum;
	}


	/**
	 * Returns the minimum brake level that must be applied (or automatically applied) at this moment.
	 *
	 * @return - A minimum brake level, range from {@code 0.0F} (brakes off) to {@code 1.0F} (full brakes/emergency).
	 */
	public float getMinimumBrake()
	{
		float minimum = 0.0F;

		/*
		 * AFB
		 */
		if (cruiseControlEnabled.get())
		{
			float speedOverTarget = trainSpeed.get() - cruiseControlTargetSpeed.get();

			/*
			 * If the cruise control is set to 0, make the train think it's going a bit too fast to ensure it comes to a controlled stop.
			 */
			if (cruiseControlTargetSpeed.get() <= 0.0F)
			{
				speedOverTarget += 0.0F;
			}

			minimum = Math.max(minimum, speedOverTarget * 0.1F);
		}

		/*
		 * Doors (this automatically applies the brakes to at least some degree when any doors are open)
		 */
		if (automaticDoorBrake.get() && getDoorsOpen())
		{
			minimum = Math.max(minimum, 0.60F);
		}

		/*
		 * Automatic Stationary Brake
		 */
		if (Math.abs(trainSpeed.get()) <= 0.01F && parentCab.controlPower.get() == 0.0F)
		{
			minimum = Math.max(minimum, 0.30F);
		}

		/*
		 * Speed Limit
		 */
		if (!operationMode.get().equals(EMTMSOperationMode.ISOLATION) && speedLimit.get() >= 0.0F && parentCab.getIsActive())
		{
			if (penaltyBrake.get())
			{
				minimum = Math.max(minimum, 1.0F);
			}
		}

		return minimum;
	}


	/**
	 * <p>
	 * Gets the {@code km/h} speed of the train over the current speed limit. The speed limit includes braking curves.
	 * </p>
	 * <p>
	 * If the train is not overspeeding, this method will return {@code 0.0F}.
	 * </p>
	 *
	 * @return - A {@code float} representing how many {@code km/h} the train is exceeding the current MTCS speed limit. {@code 0.0F} if the train is not overspeeding.
	 */
	public float getOverspeedAmount()
	{
		if (operationMode.get().equals(EMTMSOperationMode.ISOLATION) || operationMode.get().equals(EMTMSOperationMode.ON_SIGHT))
		{
			return 0.0F;
		}

		float minimumSpeedLimit = Math.max(speedLimit.get(), MIN_SPEED_LIMIT);
		if (trainSpeed.get() > minimumSpeedLimit)
		{
			return trainSpeed.get() - minimumSpeedLimit;
		}
		else
		{
			return 0.0F;
		}
	}


	/**
	 * <p>
	 * Gets the {@code km/h} speed of the train over the upcoming target speed limit.
	 * </p>
	 * <p>
	 * If the train is currently under the next target speed limit, this method will return {@code 0.0F}.
	 * </p>
	 *
	 * @return - A value in {@code km/h}.
	 */
	public float getOvertargetAmount()
	{
		if (operationMode.get().equals(EMTMSOperationMode.ISOLATION) || operationMode.get().equals(EMTMSOperationMode.ON_SIGHT))
		{
			return 0.0F;
		}

		if (trainSpeed.get() > targetSpeed.get())
		{
			return trainSpeed.get() - targetSpeed.get();
		}
		else
		{
			return 0.0F;
		}
	}


	/**
	 * Helper method to get an appropriate name for a property based on the supplied word. For example,
	 *
	 * @param name - A word specific to the property you are attempting to get a name for.
	 * @return - A generated string similar to {@code mtms.yourProperty}.
	 */
	private String getPropertyNameFrom(String name)
	{
		return getClass().getSimpleName().toLowerCase() + "." + name;
	}


	/**
	 * This method is automatically called when a property added to {@link MTMS#monitoredProperties} is changed. Ideally this method should be used to trigger screen updates (assuming the property that has changed is worthy of being updated
	 * on the screen(s) in real time).
	 *
	 * @param property - The specific property that was changed causing this method to be called.
	 */
	public void onPropertyChanged(IPartProperty<?> property)
	{
		/*
		 * Ensure the train is not null.
		 */
		Train train = this.parentCab.getTrain();
		if (train != null && !train.worldObj.isRemote)
		{
			if (property instanceof PropButton)
			{
				PropButton button = (PropButton)property;

				if (button.cabKey == null)
				{
					return;
				}

				if (button.cabKey.equals(PartTypeCab.ECabKey.ALERTER))
				{
					overrideModeButtonPressed.set(button.isActive());
				}
			}
		}
	}


	/**
	 * This method is called when the parent Train instance is about to be destroyed. It is critical that any forced chunks loaded by MTMS are cancelled here, otherwise the chunks will stay loaded and will reduce the remaining number of
	 * free chunks for other trains.
	 */
	public void onTrainDead()
	{
		/*
		 * The parent train is going to be destroyed, make sure all chunks this MTMS has loaded are unloaded.
		 */
		//		int count = 0;

		for (Ticket ticket : chunkTickets)
		{
			ForgeChunkManager.releaseTicket(ticket);
			//			count++;
		}
	}


	/**
	 * This method should be called by the parent cab during client and server update ticks. This allows the MTMS system to collect information about the train and to control different functions (braking, acceleration, etc).
	 */
	public void onUpdate()
	{
		/*
		 * Ensure the cab is not null before proceeding.
		 */
		if (parentCab == null)
		{
			return;
		}

		/*
		 * Ensure the train is not null.
		 */
		Train train = parentCab.getTrain();
		if (train == null)
		{
			return;
		}

		/*
		 * Server-Side
		 */
		if (!train.worldObj.isRemote)
		{
			/*
			 * Autodrive Reverser & Throttle
			 */
			if (autodriveEnable.get() && parentCab.getIsActive())
			{
				parentCab.reverser.set(EReverser.FORWARD);
				parentCab.controlPower.set(1.0F);
			}

			/*
			 * Update speed and acceleration.
			 */
			{
				float currentSpeed = (float)Math.abs((parentCab.getTrain().getLastSpeed()) * 3.6F * 20.0F);
				trainSpeed.set(currentSpeed);
			}

			/*
			 * Update realtime information
			 */
			{
				ConcatenatedTypesList<PartTypeBogie> trainBogies = parentCab.getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
				ConcatenatedTypesList<APartTypeEngine> trainEngines = parentCab.getTrain().getVehiclePartTypes(OrderedTypesList.ASELECTOR_ENGINE);

				/*
				 * Braking Values
				 */
				{
					boolean cabInverse = VehicleHelper.getAngle(null, PartHelper.getViewVec(parentCab.getParent(), true)) > 90.0;
					PartTypeBogie thisBogie = cabInverse ? trainBogies.get(trainBogies.size() - 1) : trainBogies.get(0);
					PartTypeBogie nextBogie = null;

					if (trainBogies.size() <= 1)
					{
						realtimeBrakeCylinderPressure1.set((float)Math.round(thisBogie.pneumaticsBrakeCylinder.getPressure()));
						realtimeBrakeCylinderPressure2.set((float)Math.round(thisBogie.pneumaticsBrakeCylinder.getPressure()));
					}
					else
					{
						nextBogie = cabInverse ? trainBogies.get(trainBogies.size() - 2) : trainBogies.get(1);

						realtimeBrakeCylinderPressure1.set((float)Math.round(thisBogie.pneumaticsBrakeCylinder.getPressure()));
						realtimeBrakeCylinderPressure2.set((float)Math.round(nextBogie.pneumaticsBrakeCylinder.getPressure()));
					}

					realtimeBrakePipePressure.set((float)Math.round(thisBogie.pneumaticsBrakePipe.getPressure()));
					realtimeMainReservoirPressure.set((float)Math.round(thisBogie.pneumaticsMainReservoirPipe.getPressure()));
				}

				/*
				 * Acceleration & Retardation
				 */
				{
					float maximumPossibleTractiveEffort = 0.0F;
					float maximumPossibleRetardation = 0.0F;
					float currentRetardation = 0.0F;
					for (PartTypeBogie bogie : trainBogies)
					{
						maximumPossibleRetardation += (float)bogie.brakeForce.get();
						currentRetardation += bogie.getDecelerationForce();

						if (bogie.getHasMotor())
						{
							maximumPossibleTractiveEffort += (float)bogie.tractiveEffort.get();
						}
					}

					float currentPower = 0.0F;
					double trainMovementDirection = Math.signum(parentCab.getTrain().getLastSpeed());
					for (APartTypeEngine engine : trainEngines)
					{
						/*
						 * Motor is accelerating
						 */
						if ((trainMovementDirection >= 0.0F && engine.getProducedForce() > 0.0F) || (trainMovementDirection <= 0.0F && engine.getProducedForce() < 0.0F))
						{
							currentPower += Math.abs(engine.getProducedForce());
						}

						/*
						 * Motor is dynamic-braking
						 */
						else
						{
							currentRetardation += Math.abs(engine.getProducedForce());
						}
					}

					trainEffortAcceleration.set((maximumPossibleTractiveEffort != 0.0F) ? (currentPower / maximumPossibleTractiveEffort) : 0.0F);
					trainEffortRetardation.set((maximumPossibleRetardation != 0.0F) ? (currentRetardation / maximumPossibleRetardation) : 0.0F);
				}
			}

			/*
			 * Update AFB
			 */
			{
				/*
				 * Allow the Cruise Control to be manually operated only when the train is not in autodrive mode.
				 */
				if (autodriveState.get() == EAutodriveState.INACTIVE)
				{
					float cruiseControlDemand = parentCab.controlCruiseControl.get();

					if (cruiseControlDemand > 0.0F)
					{
						cruiseControlEnabled.set(true);
						cruiseControlTargetSpeed.set(cruiseControlDemand);
					}
					else if (cruiseControlDemand < 0.0F)
					{
						cruiseControlEnabled.set(false);
						cruiseControlTargetSpeed.set(Math.abs(cruiseControlDemand));
					}
					else
					{
						cruiseControlEnabled.set(false);
						cruiseControlTargetSpeed.set(0.0F);
					}
				}
			}

			/*
			 * Update Light Mode
			 */
			{
				currentLightMode.set(parentCab.lightMode.get());
			}

			/*
			 * Update Train Information
			 */
			informationCounter++;
			if (informationCounter >= 9)
			{
				informationCounter = 0;

				ArrayList<VehicleData> carriages = train.getVehicles();
				{
					/*
					 * Pneumatics Values & Doors
					 */
					{
						ArrayList<float[]> brakeCylinderValues = new ArrayList<float[]>();
						ArrayList<float[]> brakePipeValues = new ArrayList<float[]>();
						ArrayList<float[]> doors = new ArrayList<float[]>();
						ArrayList<float[]> mainReservoirPipeValues = new ArrayList<float[]>();
						ArrayList<float[]> pantographs = new ArrayList<float[]>();
						ArrayList<float[]> parkBrakes = new ArrayList<float[]>();

						for (VehicleData carriage : carriages)
						{
							/*
							 * Pneumatics (Including Park Brakes)
							 */
							{
								/*
								 * Get the list of bogies within this carriage only.
								 */
								OrderedTypesList<PartTypeBogie> carriageBogies = carriage.getTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);

								float[] bogieBrakeCylinderValues = new float[carriageBogies.size()];
								float[] bogieBrakePipeValues = new float[carriageBogies.size()];
								float[] bogieMainReservoirPipeValues = new float[carriageBogies.size()];
								float[] bogieParkBrakeValues = new float[carriageBogies.size()];

								/*
								 * Go through the list of bogies in this carriage and add their pneumatics
								 * values to the arrays.
								 */
								for (int bogieIndex = 0; bogieIndex < carriageBogies.size(); bogieIndex++)
								{
									PartTypeBogie bogie = carriageBogies.get(bogieIndex);

									bogieBrakeCylinderValues[bogieIndex] = Math.round(bogie.pneumaticsBrakeCylinder.getPressure());
									bogieBrakePipeValues[bogieIndex] = Math.round(bogie.pneumaticsBrakePipe.getPressure());
									bogieMainReservoirPipeValues[bogieIndex] = Math.round(bogie.pneumaticsMainReservoirPipe.getPressure());
									bogieParkBrakeValues[bogieIndex] = bogie.pneumaticsParkBrakeCylinder.getPressure() > 300.0F ? 0.0F : 1.0F;
								}

								brakeCylinderValues.add(bogieBrakeCylinderValues);
								brakePipeValues.add(bogieBrakePipeValues);
								mainReservoirPipeValues.add(bogieMainReservoirPipeValues);
								parkBrakes.add(bogieParkBrakeValues);
							}

							/*
							 * Doors
							 */
							{
								OrderedTypesList<PartTypeDoor> carriageDoors = carriage.getTypes(OrderedTypesList.SELECTOR_DOOR);

								float[] carriageDoorsStatuses = new float[carriageDoors.size()];

								/*
								 * Go through all the doors in the carriage and retrieve the values of whether
								 * they are open or closed.
								 */
								for (int carriageDoorIndex = 0; carriageDoorIndex < carriageDoors.size(); carriageDoorIndex++)
								{
									PartTypeDoor door = carriageDoors.get(carriageDoorIndex);

									float doorValue = 0.5F;

									if (door.doorStateL.get() > 0.0F && door.doorStateR.get() == 0.0F)
									{
										doorValue = door.getDoInvertDoorStates() ? 2.0F : 1.0F;
									}
									else if (door.doorStateL.get() == 0.0F && door.doorStateR.get() > 0.0F)
									{
										doorValue = door.getDoInvertDoorStates() ? 1.0F : 2.0F;
									}
									else if (door.doorStateL.get() > 0.0F && door.doorStateR.get() > 0.0F)
									{
										doorValue = 3.0F;
									}

									carriageDoorsStatuses[carriageDoorIndex] = doorValue;
								}

								doors.add(carriageDoorsStatuses);
							}

							/*
							 * Pantographs
							 */
							{
								OrderedTypesList<PartTypePantograph> carriagePantographs = carriage.getTypes(OrderedTypesList.SELECTOR_PANTOGRAPH);

								float[] pantographStatuses = new float[carriagePantographs.size()];

								for (int carriagePantographIndex = 0; carriagePantographIndex < carriagePantographs.size(); carriagePantographIndex++)
								{
									PartTypePantograph pantograph = carriagePantographs.get(carriagePantographIndex);

									if (pantograph.getParent().getRotation()[1] >= 180.0F)
									{
										pantographStatuses[carriagePantographIndex] = Math.min(pantograph.getExtension() * -1.0F, -0.1F);
									}
									else
									{
										pantographStatuses[carriagePantographIndex] = Math.max(pantograph.getExtension(), 0.1F);
									}
								}

								pantographs.add(pantographStatuses);
							}
						}

						this.listBrakeCylinders.set(serialise(brakeCylinderValues));
						this.listBrakePipe.set(serialise(brakePipeValues));
						this.listDoors.set(serialise(doors));
						this.listMainReservoirPipe.set(serialise(mainReservoirPipeValues));
						this.listPantographs.set(serialise(pantographs));
						this.listParkBrakes.set(serialise(parkBrakes));
					}
				}

				/*
				 * Train Length/Mass
				 */
				trainLength.set(train.getLength());
				trainWagonCount.set(train.getVehicles().size());
				float totalMass = 0.0F;
				for (VehicleData vehicle : carriages)
				{
					for (VehParBase part : vehicle.getParts())
					{
						totalMass += part.getMass();
					}
				}
				trainMass.set(totalMass);
			}


			/*
			 * Tick MTCS Logic
			 */
			int mtcsTickTarget;
			switch (operationMode.get())
			{
				default:
				case ISOLATION:
					mtcsTickTarget = 20;
					break;

				case REVERSING:
					mtcsTickTarget = 15;
					break;

				case FULL_SUPERVISION:
				case LIMITED_SUPERVISION:
				case ON_SIGHT:
					mtcsTickTarget = 10;
					break;
			}
			if (mtcsTickCounter >= mtcsTickTarget)
			{
				operationMode.set(updateOperationMode());
				mtcsTickCounter = 0;
			}
			mtcsTickCounter++;

			switch (operationMode.get())
			{
				case ISOLATION:
				{
					autodriveState.set(EAutodriveState.INACTIVE);
					penaltyBrake.set(false);

					speedLimit.set(-1.0F);
					trackSpeed.set(40.0F);

					if (!parentCab.getIsActive())
					{
						if (chunkTickets.size() > 0 && dormantCounter >= 100)
						{
							for (int index = 0; index < chunkTickets.size(); index++)
							{
								ForgeChunkManager.releaseTicket(chunkTickets.get(index));
								chunkTickets.remove(index);
								index--;
							}
						}
					}

					if (dormantCounter < 100)
					{
						dormantCounter++;
					}

					if (autodriveState.get() != EAutodriveState.INACTIVE)
					{
						autodriveState.set(EAutodriveState.INACTIVE);
					}
					break;
				}

				case REVERSING:
				case ON_SIGHT:
				{
					speedLimit.set(40.0F);
					trackSpeed.set(40.0F);

					if (autodriveState.get() != EAutodriveState.INACTIVE)
					{
						autodriveState.set(EAutodriveState.INACTIVE);
					}
					break;
				}

				case FULL_SUPERVISION:
				{
					/*
					 * Only when MTCS is operating in Full Supervision mode, can the autodrive system be enabled.
					 */
					if (autodriveEnable.get())
					{
						if (autodriveState.get() == EAutodriveState.INACTIVE)
						{
							autodriveState.set(EAutodriveState.NORMAL);
						}

						/*
						 * Update this train's entity ID in the train database.
						 */
						TrainDatabase.getTrainDatabase(train.worldObj).updateAutodriveTrainTime(train.getEntityId(), train.worldObj.getTotalWorldTime());
					}

					if (lastLandscape.get() != null)
					{
						landscapeOffset.set(landscapeOffset.get() + ((trainSpeed.get() / (3.6F * 20.0F)) * ModCenter.cfg.trains.velocityScale));

						/*
						 * Calculate the deceleration rate of the train.
						 */
						ConcatenatedTypesList<PartTypeBogie> brakingBogies = train.getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
						float totalTrainMass = train.getMass() * 1000.0F;
						float totalBrakeForce = 0.0F;

						for (PartTypeBogie bogie : brakingBogies)
						{
							totalBrakeForce += (bogie.brakeForce.get() * 1000.0F);
						}

						calculatedDeceleration = totalBrakeForce / totalTrainMass;

						/*
						 * Aim for a braking curve that uses around 50% of the normal braking force of the train.
						 */
						calculatedDeceleration *= 0.5F;

						//float inTheEnd = parentCab.getPulse().landmark.getTargetSpeed(calculatedDeceleration);//lastLandscape.get().getMaximumSpeed(calculatedDeceleration, landscapeOffset.get());
						//float inTheEnd = lastLandscape.get().getMaximumSpeed(calculatedDeceleration, landscapeOffset.get());


						/*if (overrideModeEnabled.get())
						{
							inTheEnd = 40.0F;
							endTarget[0] = 40.0F;
							endTarget[1] = -1.0F;
						}

						speedLimit.set((float)Math.round(parentCab.getPulse().landmark.getResult().getSpeed()));


						if (inTheEnd >= 0.0F)
						{
							/*
							 * Braking for a landmark.
							 *
							targetSpeed.set(inTheEnd);
							secondsUntilBrakingCurve.set(0.0F);
						}
						else
						{
							/*
							 * Cruising at track speed.
							 *
							if (inTheEnd >= 0.0F)
							{
								targetSpeed.set(inTheEnd);
								secondsUntilBrakingCurve.set(endTarget[1]);
							}
							else
							{
								targetSpeed.set(-1.0F);
								secondsUntilBrakingCurve.set(-1.0F);
							}
						}*/

						/*
						 * Observe the minimum speed limit.
						 */
						if (speedLimit.get() > 0 && speedLimit.get() < MIN_SPEED_LIMIT)
						{
							speedLimit.set(MIN_SPEED_LIMIT);
						}

						/*
						 * Find a new speed limit.
						 */


						ALandmark nextLandmark = parentCab.getNextLandmark();
						if (nextLandmark != null)
						{
							float newSpeedLimit = nextLandmark.getSpeedLimit();
							if (newSpeedLimit > 0.0F)
							{
								speedLimit.set(newSpeedLimit);
							} else {
								speedLimit.set(320.0F);
							}
						}




						Landmark closestLandmark = lastLandscape.get().getClosestLandmark();
						if (closestLandmark != null)
						{
							if (closestLandmark.getLocation() - landscapeOffset.get() < 5.0F && closestLandmark.getLength() < 0.0F)
							{
								float newTrackSpeed = closestLandmark.getSpeedLimit();

								/*
								 * When approaching landmarks with a 0 speed limit (e.g. a signal at STOP) set
								 * the new speed limit to 40, to allow the train to continue past the
								 * signal/landmark at a reasonable speed.
								 */
								if (newTrackSpeed <= 0.0F)
								{
									newTrackSpeed = 40.0F;
								}

								/*
								 * The track speed is increasing.
								 */
								if (newTrackSpeed > trackSpeed.get())
								{
									trackSpeed.set(newTrackSpeed);

									// Note - The track speed increase sound effect is handled on the client side only.
								}

								/*
								 * The track speed is dropping.
								 */
								else if (newTrackSpeed < trackSpeed.get())
								{
									trackSpeed.set(newTrackSpeed);
								}
							}
						}
					}

					/*
					 * It would be an anomaly if this else was triggered, but I'm putting it here anyway just in case.
					 */
					else
					{
						speedLimit.set(0.0F);
						targetSpeed.set(-1.0F);
					}

					updateChunkLoading();
					updateAutodriveSystem();

					/*
					 * Autodrive Deadman Suppression
					 */
					if (autodriveState.get() != EAutodriveState.INACTIVE)
					{
						/*
						 * Ensure the deadman system does not function while the train is in autodrive mode.
						 */
						parentCab.alerter.reset();
					}
					break;
				}

				case LIMITED_SUPERVISION:
				{
					if (lastLandscape.get() == null)
					{
						speedLimit.set(0.0F);
						targetSpeed.set(-1.0F);
						break;
					}

					// Due to the lack of braking curves, just lock the speed limit to whatever the track speed is.
					speedLimit.set(Math.max(trackSpeed.get(), (float)MIN_SPEED_LIMIT));

					// Find new track speeds.
					Landmark closestLandmark = lastLandscape.get().getClosestLandmark();
					if (closestLandmark == null)
					{
						break;
					}

					if (closestLandmark.getLocation() - landscapeOffset.get() < 5.0F && closestLandmark.getLength() < 0.0F)
					{
						float newTrackSpeed = closestLandmark.getSpeedLimit();

						// If the train reaches a 0 speed limit (e.g. signal displaying STOP), set the track speed back to 40 so they are not stuck at a speed limit of 0.
						if (newTrackSpeed <= 0.0F)
						{
							newTrackSpeed = 40.0F;
						}

						// Assign the new track speed.
						if (newTrackSpeed != trackSpeed.get())
						{
							trackSpeed.set(newTrackSpeed);
						}
					}

					break;
				}
			}










			/*
			 * Penalty Valve Operation
			 */
			if (!operationMode.get().equals(EMTMSOperationMode.ISOLATION) && speedLimit.get() >= 0.0F && parentCab.getIsActive())
			{
				/*
				 * Calculate whether the automatic penalty brake should be applied. This works with electro-pneumatic braking only.
				 * There is a 'hysteresis' so the train will slow down to below the speed limit before the brake is automatically released, to prevent
				 * continuous speeding.
				 */
				if (getOverspeedAmount() >= 8.0F && !penaltyBrake.get())
				{
					penaltyBrake.set(true);
				}
				else if (getOverspeedAmount() <= 0.0F && penaltyBrake.get())
				{
					penaltyBrake.set(false);
				}

				/*
				 * If the train is speeding by a dramatic amount, then potentially trigger the cab's actual emergency brake.
				 * If this is triggered, then the train will come to a complete stop before the brakes can be released.
				 */
				if (getOverspeedAmount() > 12.0F)
				{
					penaltyCounter += 1;
				}
				else
				{
					/*
					 * The counter is reset once the train is no longer overspeeding by the penalty margin.
					 */
					penaltyCounter = 0;
				}

				/*
				 * Only trigger the emergency brake if the train has been speeding for at least a couple ticks.
				 */
				if (penaltyCounter >= 2)
				{
					parentCab.setEmergency();
				}
			}
			else
			{
				penaltyCounter = 0;
			}

			/*
			 * Isolation Counter
			 */

			if (operationMode.get() != EMTMSOperationMode.ISOLATION)
			{
				dormantCounter = 0;
			}

			/*
			 * Override Mode Functionality
			 */
			tickOverrideModeSystem();
		}

		/*
		 * Client-Side
		 */
		else
		{

			/*
			 * Track Speed Changes (Sound effects)
			 */
			if (!operationMode.get().equals(EMTMSOperationMode.ISOLATION))
			{
				boolean playUpdateSound = false;
				boolean playOverrideSound = false;

				if(penaltyBrake.get() && !brakeInterventionPrevious)
				{
					playUpdateSound = true;
				}
				brakeInterventionPrevious = penaltyBrake.get();

				if (trackSpeed.get() > trackSpeedPrevious && !operationMode.get().equals(EMTMSOperationMode.LIMITED_SUPERVISION))
				{
					playUpdateSound = true;
				}
				trackSpeedPrevious = trackSpeed.get();

				if (targetSpeed.get() > targetSpeedPrevious && !operationMode.get().equals(EMTMSOperationMode.LIMITED_SUPERVISION))
				{
					playUpdateSound = true;
				}
				targetSpeedPrevious = targetSpeed.get();

				if (overrideModeEnabled.get() && !overrideModeEnabledPrevious)
				{
					playOverrideSound = true;
				}
				overrideModeEnabledPrevious = overrideModeEnabled.get();

				if (parentCab.getPassenger() != null)
				{
					if (playUpdateSound && playUpdateSoundTimeout <= 0)
					{
						parentCab.getPassenger().playSound(String.format("%s:vehPar_cab_mtcsUpdate", ModData.ID), 1.0F, 1.0F);
						playUpdateSoundTimeout = 10;
					}

					if (playOverrideSound)
					{
						parentCab.getPassenger().playSound(String.format("%s:vehPar_cab_mtcsOverride", ModData.ID), 1.0F, 1.0F);
					}
				}

				if (playUpdateSoundTimeout > 0)
				{
					playUpdateSoundTimeout--;
				}
			}
			else
			{
				playUpdateSoundTimeout = 0;
			}
			/*
			 * Update all screens by at least a minimum frequency.
			 */
			repaintCounter++;
			{
				if (repaintCounter >= 4)
				{
					repaintPointer++;
					repaintCounter = 0;
				}
			}
		}
	}

	/**
	 * Helper method to serialise a list of float arrays into a {@code String}.
	 */
	public static String serialise(ArrayList<float[]> values)
	{
		StringBuilder output = new StringBuilder();

		for (int carriage = 0; carriage < values.size(); carriage++)
		{
			if (values.get(carriage).length == 0)
			{
				output.append("e");
			}
			else
			{
				for (int bogie = 0; bogie < values.get(carriage).length; bogie++)
				{
					output.append(String.valueOf(values.get(carriage)[bogie]));

					if (bogie < values.get(carriage).length - 1)
					{
						output.append("b");
					}
				}
			}

			if (carriage < values.size() - 1)
			{
				output.append("c");
			}
		}

		return output.toString();
	}


	private void tickOverrideModeSystem()
	{
		overrideModeCanBegin.set(false);

		if (overrideModeEnabled.get())
		{
			overrideModeButtonCounter.set(0);

			if (parentCab.getReverserState() != EReverser.FORWARD)
			{
				overrideModeEnabled.set(false);
				return;
			}

			if (!isLandscapeSufficientForOverride(lastLandscape.get()))
			{
				overrideModeEnabled.set(false);
				return;
			}

			// Override should function normally here.
			speedLimit.set(40.0F);

			return;
		}

		if (!(operationMode.get() == EMTMSOperationMode.FULL_SUPERVISION))
		{
			return;
		}

		if (autodriveEnable.get())
		{
			return;
		}

		// Override Mode is disabled. We want to try to start it.

		if (trainSpeed.get() != 0.0F)
		{
			overrideModeButtonCounter.set(0);
			return;
		}

		if (!isLandscapeSufficientForOverride(lastLandscape.get()))
		{
			return;
		}

		overrideModeCanBegin.set(true);

		if (overrideModeButtonPressed.get())
		{
			if (overrideModeButtonCounter.get() >= 60)
			{
				overrideModeEnabled.set(true);

			}

			overrideModeButtonCounter.set(overrideModeButtonCounter.get() + 1);
		}
		else
		{
			overrideModeButtonCounter.set(0);
		}

	}


	private boolean isLandscapeSufficientForOverride(Landscape landscape)
	{
		if (landscape == null)
		{
			return false;
		}

		ArrayList<Landmark> allLandmarks = landscape.getAllLandmarks();
		ArrayList<Landmark> appropriateSignals = new ArrayList<Landmark>();

		for (Landmark landmark : allLandmarks)
		{
			if (landmark.getLocation() - landscapeOffset.get() > 20.0F)
			{
				continue;
			}

			if (landmark.getSpeedLimit() != 0.0F)
			{
				continue;
			}

			appropriateSignals.add(landmark);
		}

		return appropriateSignals.size() > 0;
	}


	/**
	 * Helper method to unserialise a {@code String} into an ArrayList of float arrays.
	 */
	public static ArrayList<float[]> unserialise(String input)
	{
		ArrayList<float[]> values = new ArrayList<float[]>();

		/*
		 * Null Check
		 */
		if (input.trim().isEmpty())
		{
			/*
			 * If the input string is empty, return the empty array list.
			 */
			return values;
		}

		String[] carriages = input.split("c");

		for (String carriage : carriages)
		{
			if (carriage == "e")
			{
				values.add(new float[] { });
				continue;
			}

			String[] bogies = carriage.split("b");
			float[] bogieValues = new float[bogies.length];

			for (int bogie = 0; bogie < bogies.length; bogie++)
			{
				try
				{
					bogieValues[bogie] = Float.parseFloat(bogies[bogie]);
				}
				catch (Exception ex)
				{
					/*
					 * Float couldn't be parsed. Just put in a 0.0F.
					 */
					bogieValues[bogie] = 0.0F;
				}
			}

			values.add(bogieValues);
		}

		return values;
	}


	private void updateAutodriveSystem()
	{
		switch (autodriveState.get())
		{
			case NORMAL:
			{

				/*
				 * Determine the next station.
				 */
				{
					ArrayList<Landmark> landmarks = lastLandscape.get().getAllLandmarks();
					ArrayList<Landmark> stations = new ArrayList<Landmark>();

					/*
					 * Find all stations in front of the train.
					 */
					for (Landmark landmark : landmarks)
					{
						if (landmark.getIsStation() || landmark.getIsTermination())
						{
							stations.add(landmark);
						}
					}

					Landmark closestStation = null;

					/*
					 * Find the closest station.
					 */
					for (Landmark station : stations)
					{
						if (closestStation == null)
						{
							closestStation = station;
						}
						else if (station.getLocation() - landscapeOffset.get() < closestStation.getLocation() - landscapeOffset.get())
						{
							closestStation = station;
						}
					}

					if (closestStation != null)
					{
						/*
						 * Station has been determined ahead, so ensure braking is correct.
						 */
						Landscape dummyLandscape = new Landscape(closestStation.getLocation() + 10.0F);
						dummyLandscape.addLandmark(new Landmark(dummyLandscape.getTotalLength(), false, false, 0.0F, 0.0F));
						float maximumStationSpeed = dummyLandscape.getMaximumSpeed(calculatedDeceleration, landscapeOffset.get());

						if (speedLimit.get() > maximumStationSpeed)
						{
							speedLimit.set(maximumStationSpeed);
						}

						if (targetSpeed.get() != 0.0F)
						{
							targetSpeed.set(0.0F);
						}
					}
					else
					{
						/*
						 * If this point is reached, MTMS cannot find a station in front of the train. Don't do anything - just let the train cruise at normal speed.
						 */
					}

					/*
					 * Stopping at a station.
					 */
					if (closestStation != null)
					{
						if (closestStation.getIsStation() && closestStation.getLocation() - landscapeOffset.get() <= 5.0F)
						{
							autodriveState.set(EAutodriveState.STATION_ARRIVAL);
						}
						else if (closestStation.getIsTermination() && closestStation.getLocation() - landscapeOffset.get() <= 5.5F)
						{
							autodriveState.set(EAutodriveState.TERMINATING);
						}
					}
				}

				/*
				 * Use cruise control as speed limiter.
				 */
				{
					cruiseControlEnabled.set(true);

					if (speedLimit.get() < trackSpeed.get())
					{
						/*
						 * Braking
						 */
						cruiseControlTargetSpeed.set(speedLimit.get() - 6.0F);
					}
					else
					{
						/*
						 * Cruising
						 */
						cruiseControlTargetSpeed.set(speedLimit.get() - 1.0F);
					}
				}
				break;
			}

			case STATION_ARRIVAL:
			{
				/*
				 * Stop the train.
				 */
				cruiseControlTargetSpeed.set(0.0F);

				/*
				 * Count how long the train is stopped at the station.
				 */
				if (trainSpeed.get() == 0.0F)
				{
					autodriveStationArrivalCounter.set(autodriveStationArrivalCounter.get() + 1);
				}
				else
				{
					autodriveStationArrivalCounter.set(0);
				}

				/*
				 * Depart the platform after a certain timeout.
				 */
				if (autodriveStationArrivalCounter.get() >= 200)
				{
					autodriveState.set(EAutodriveState.STATION_DEPARTURE);
					autodriveStationArrivalCounter.set(0);
				}

				break;
			}

			case STATION_DEPARTURE:
			{
				/*
				 * Use cruise control as speed limiter.
				 */
				{
					cruiseControlEnabled.set(true);

					if (speedLimit.get() < trackSpeed.get())
					{
						/*
						 * Braking
						 */
						cruiseControlTargetSpeed.set(speedLimit.get() - 6.0F);
					}
					else
					{
						/*
						 * Cruising
						 */
						cruiseControlTargetSpeed.set(speedLimit.get() - 1.0F);
					}
				}

				/*
				 * Timeout station departure.
				 */
				if (trainSpeed.get() != 0.0F)
				{
					autodriveStationDepartureCounter.set(autodriveStationDepartureCounter.get() + 1);
				}
				else
				{
					autodriveStationDepartureCounter.set(0);
				}

				if (autodriveStationDepartureCounter.get() >= 200)
				{
					autodriveState.set(EAutodriveState.NORMAL);
				}
				break;
			}

			case TERMINATING:
			{
				/*
				 * Apply the brakes.
				 */
				cruiseControlTargetSpeed.set(0.0F);

				/*
				 * Only come out of termination mode once there is a different active cab.
				 */
				if (!parentCab.getIsActive())
				{
					autodriveState.set(EAutodriveState.INACTIVE);
				}
				break;
			}

			case INACTIVE:
			default:
			{
				break;
			}
		}
	}


	private void updateChunkLoading()
	{
		if (lastLandscape.get() == null || chunkTickets == null)
		{
			return;
		}

		ArrayList<ChunkCoordIntPair> unrevisedCurrentChunks;

		if (autodriveEnable.get())
		{
			unrevisedCurrentChunks = lastLandscape.get().getChunkList();
		}
		else
		{
			/*
			 * Leave the array empty, so the chunkloading system will just unload any previous loaded chunks.
			 */
			unrevisedCurrentChunks = new ArrayList<ChunkCoordIntPair>();
		}

		ArrayList<ChunkCoordIntPair> currentChunks = new ArrayList<ChunkCoordIntPair>();
		for (ChunkCoordIntPair chunk : unrevisedCurrentChunks)
		{
			if (!currentChunks.contains(chunk))
			{
				currentChunks.add(chunk);
			}
		}

		for (ChunkCoordIntPair chunk : currentChunks)
		{
			/*
			 * Compare with current chunk tickets.
			 */
			boolean alreadyLoaded = false;

			NBTTagCompound ticketNBT;
			for (Ticket ticket : chunkTickets)
			{
				ticketNBT = ticket.getModData();

				if (ticketNBT.getInteger("chunkX") == chunk.chunkXPos && ticketNBT.getInteger("chunkZ") == chunk.chunkZPos)
				{
					/*
					 * Chunk already added.
					 */
					alreadyLoaded = true;
					break;
				}
			}

			/*
			 * If a chunk provided in the landscape hasn't been loaded, get a new ticket from forge.
			 */
			if (!alreadyLoaded)
			{
				Ticket newTicket = ForgeChunkManager.requestTicket(ModCenter.instance, parentCab.getTrain().worldObj, ForgeChunkManager.Type.NORMAL);

				if (newTicket != null)
				{
					newTicket.getModData().setInteger("chunkX", chunk.chunkXPos);
					newTicket.getModData().setInteger("chunkZ", chunk.chunkZPos);
					newTicket.getModData().setInteger("train", parentCab.getTrain().getEntityId());
					chunkTickets.add(newTicket);
					ForgeChunkManager.forceChunk(newTicket, chunk);
				}
			}
		}

		/*
		 * Now we need to determine if there are any chunks that are no longer required.
		 * Iterate through the current list of chunk tickets and compare them to the list provided by the landscape instance (which is the list of chunks that should be loaded right
		 * now).
		 */
		for (int ticketIndex = 0; ticketIndex < chunkTickets.size(); ticketIndex++)
		{
			Ticket ticket = chunkTickets.get(ticketIndex);

			/*
			 * Loop Variables
			 */
			boolean stillNeeded = false;
			NBTTagCompound ticketNBT = ticket.getModData();

			for (ChunkCoordIntPair chunk : unrevisedCurrentChunks)
			{
				if (ticketNBT.getInteger("chunkX") == chunk.chunkXPos && ticketNBT.getInteger("chunkZ") == chunk.chunkZPos)
				{
					stillNeeded = true;
					break;
				}
				else
				{
					continue;
				}
			}

			if (!stillNeeded)
			{
				/*
				 * We've found a registered ticket that is no longer needed, i.e. doesn't currently exist in the list of chunks to be loaded in the Landscape instance.
				 */
				ForgeChunkManager.releaseTicket(ticket);
				chunkTickets.remove(ticketIndex);

				ticketIndex--;
			}
		}

		/*
		 * Update public properties to be displayed on the DDUs
		 */
		autodriveChunksFree.set(ForgeChunkManager.ticketCountAvailableFor(ModCenter.instance, this.parentCab.getTrain().worldObj));
		autodriveChunksLoaded.set(chunkTickets.size());
	}


	/**
	 * Determines what the appropriate MTMS mode should be based on the current available information and circumstances, and returns the resulting mode as an Enum.
	 */
	private EMTMSOperationMode updateOperationMode()
	{
		if (isolateMTCS.get() || !parentCab.getIsActive())
		{
			return EMTMSOperationMode.ISOLATION;
		}

		if (parentCab.getReverserState() == EReverser.BACKWARD)
		{
			return EMTMSOperationMode.REVERSING;
		}

		if (parentCab.getReverserState() == EReverser.FORWARD)
		{
			if (lastLandscape.get() != null)
			{
				if (limitedSupervision.get())
				{
					return EMTMSOperationMode.LIMITED_SUPERVISION;
				}

				return EMTMSOperationMode.FULL_SUPERVISION;
			}

			return EMTMSOperationMode.ON_SIGHT;
		}

		return EMTMSOperationMode.ISOLATION;
	}

}
