package zoranodensha.api.vehicles.part.type.cab;

import java.util.List;

import javax.annotation.Nonnull;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import zoranodensha.api.vehicles.part.type.PartTypeCab;
import zoranodensha.api.vehicles.part.type.seat.EntitySeat;



/**
 * Player command to set the destination of a cab.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class CommandDestination implements ICommand
{
	@Override
	public List<?> addTabCompletionOptions(ICommandSender iCommandSender, String[] s)
	{
		return null;
	}

	@Override
	public boolean canCommandSenderUseCommand(ICommandSender iCommandSender)
	{
		return true;
	}

	@Override
	public int compareTo(@Nonnull Object obj)
	{
		if (obj instanceof ICommand)
		{
			return getCommandName().compareTo(((ICommand)obj).getCommandName());
		}
		return 0;
	}

	@Override
	public List<?> getCommandAliases()
	{
		return null;
	}

	@Override
	public String getCommandName()
	{
		return "setdest";
	}

	@Override
	public String getCommandUsage(ICommandSender iCommandSender)
	{
		return String.format("%s.usage", getClass().getName().toLowerCase());
	}

	@Override
	public boolean isUsernameIndex(String[] s, int index)
	{
		return false;
	}

	@Override
	public void processCommand(ICommandSender iCommandSender, String[] params)
	{
		if (iCommandSender instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer)iCommandSender;
			if (player.ridingEntity instanceof EntitySeat)
			{
				EntitySeat seat = (EntitySeat)player.ridingEntity;
				if (seat.getPartType() instanceof PartTypeCab)
				{
					PartTypeCab cab = (PartTypeCab)seat.getPartType();
					if (params.length > 0)
					{
						String dests = "";
						for (int i = 0; i < params.length; ++i)
						{
							dests += params[i];
							if (i + 1 < params.length)
							{
								dests += " ";
							}
						}
						cab.setDestination(dests.split(",|, "));
					}
					else
					{
						cab.setDestination((String[])null);
					}
					return;
				}
			}
			throw new CommandException(String.format("%s.nocab", getClass().getName().toLowerCase()));
		}
	}
}