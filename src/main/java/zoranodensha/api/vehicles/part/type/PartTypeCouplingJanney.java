package zoranodensha.api.vehicles.part.type;

import net.minecraft.entity.player.EntityPlayer;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;

/**
 * Janney couplings.<br>
 * Couple automatically, decouple manually.
 */
public class PartTypeCouplingJanney extends APartTypeCoupling.APartTypeCouplingAuto
{
    public PartTypeCouplingJanney(VehParBase parent)
    {
        super(parent, zoranodensha.api.vehicles.part.type.PartTypeCouplingJanney.class.getSimpleName());
    }

    @Override
    public boolean getIsCompatible(APartTypeCoupling coupling)
    {
        return (coupling instanceof zoranodensha.api.vehicles.part.type.PartTypeCouplingJanney);
    }

    @Override
    public boolean onActivated(EntityPlayer player)
    {
        Train train = parent.getTrain();
        if (parent.getIsFinished() && train != null)
        {
            if (player.isSneaking() && !train.worldObj.isRemote)
            {
                return getIsCoupled() && doTryDecouple();
            }
        }
        return false;
    }
}