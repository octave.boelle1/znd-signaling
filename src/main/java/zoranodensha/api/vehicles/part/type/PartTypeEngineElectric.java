package zoranodensha.api.vehicles.part.type;

import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;
import zoranodensha.api.vehicles.part.type.cab.PropReverser.EReverser;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;



/**
 * Electric engines, using power input from pantographs.
 */
public class PartTypeEngineElectric extends APartTypeEngine
{
	/**
	 * Efficiency of this engine.
	 */
	public final PropFloatBounded efficiency;
	/**
	 * Whether this engine should make any noise.
	 */
	public final PropBoolean hasSounds;
	/**
	 * Motor activity value, bounded between {@code 0.0F} and {@code 1.0F}.
	 */
	public final PropFloatBounded motorActivity;
	/**
	 * A velocity value that represents the change of the motorActivity value, used to make the motors react more realistically.
	 */
	private final PropFloat motorActivityVelocity;
	/**
	 * Motor polarity (whether it's accelerating or braking), between {@code -1.0F} for dynamic braking and {@code 1.0F} for accelerating.
	 */
	public final PropFloatBounded motorPolarity;
	/**
	 * Responsiveness of this engine.
	 */
	public final PropFloatBounded responsiveness;


	public PartTypeEngineElectric(VehParBase parent)
	{
		super(parent, PartTypeEngineElectric.class.getSimpleName());

		addProperty(efficiency = (PropFloatBounded)new PropFloatBounded(parent, 0.85F, 0.01F, 1.0F, "efficiency").setConfigurable());
		addProperty(hasSounds = (PropBoolean)new PropBoolean(parent, false, "hasSounds").setConfigurable().setSyncDir(ESyncDir.CLIENT));
		addProperty(motorActivity = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, 1.0F, "motorActivity").setSyncDir(ESyncDir.CLIENT));
		addProperty(motorActivityVelocity = new PropFloat(parent, "motorActivityVelocity"));
		addProperty(motorPolarity = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, -1.0F, 1.0F, "motorPolarity").setSyncDir(ESyncDir.CLIENT));
		addProperty(responsiveness = (PropFloatBounded)new PropFloatBounded(parent, 0.5F, 0.001F, 1.0F, "responsiveness").setConfigurable());
	}


	private void tickPowerOutput()
	{
		float force = 0.0F;
		PartTypeCab cab = parent.getTrain().getActiveCab();
		EReverser cabReverser;

		/*
		 * Determine the total amount of tractive effort possible with the powered bogies in this vehicle.
		 */
		OrderedTypesList<PartTypeBogie> bogies = this.getVehicle().getTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
		float vehicleMaxTraction = 0.0F;
		for (PartTypeBogie bogie : bogies)
		{
			if (bogie.getHasMotor())
			{
				vehicleMaxTraction += bogie.tractiveEffort.get();
			}
		}

		/*
		 * Determine the total amount of power (kW) available in this vehicle.
		 * This value will be used to balance the load between all available engines, even if they have different power levels.
		 */
		OrderedTypesList<APartTypeEngine> engines = this.getVehicle().getTypes(OrderedTypesList.ASELECTOR_ENGINE);
		float vehicleTotalPower = 0.0F;
		for (APartTypeEngine engine : engines)
		{
			vehicleTotalPower += (float)engine.outputKW.get();
		}

		/*
		 * Disable motors if there are no powered bogies in this vehicle, or none of the engines can produce power.
		 */
		if (vehicleMaxTraction <= 0.0F || vehicleTotalPower <= 0.0F)
		{
			producedForce.set(0.0F);
			return;
		}

		/*
		 * Disable motors if there is no active cab.
		 */
		if (cab == null)
		{
			producedForce.set(0.0F);
			return;
		}

		cabReverser = cab.getReverserState();

		/*
		 * Motor Accelerating
		 */
		if (motorPolarity.get() >= 0.95F)
		{
			final float currentSpeed = (float)Math.abs(parent.getTrain().getLastSpeed()) * 20.0F;
			final float fastestMaxEffort = vehicleTotalPower / vehicleMaxTraction;

			float currentMaxTraction;
			if (currentSpeed > fastestMaxEffort)
			{
				currentMaxTraction = vehicleTotalPower / currentSpeed;
			}
			else
			{
				currentMaxTraction = vehicleMaxTraction;
			}

			force = currentMaxTraction * motorActivity.get();

			// Scale with other engines in vehicle
			force *= ((float)outputKW.get() / vehicleTotalPower);

			// TODO @JAFFA - DON'T FORGET EFFICIENCY!

			if (!cabReverser.isForwardMovement())
			{
				force *= -1.0F;
			}
		}

		/*
		 * Motor Dynamically Braking
		 */
		if (motorPolarity.get() <= -0.95F)
		{
			float trainSpeed = (float)Math.abs(parent.getTrain().getLastSpeed() * 20.0F * 3.6F);

			/*
			 * Cap the measured speed to 30 km/h, which will stop the dynamic braking effectiveness to skyrocket at high speeds.
			 */
			trainSpeed = Math.min(trainSpeed, 30.0F);

			if (trainSpeed >= 5.0F)
			{
				force = (float)(trainSpeed * -2.5F) * efficiency.get() * motorActivity.get();

				if (force < -outputKW.get())
				{
					force = -outputKW.get();
				}
				else if (force > outputKW.get())
				{
					force = outputKW.get();
				}
			}
			else
			{
				force = 0.0F;
			}
		}

		producedForce.set(force);
	}

	@Override
	public float getProducedForce()
	{
		return producedForce.get();
	}

	@Override
	public void onTick()
	{
		/* Only update on server worlds. */
		Train train = getTrain();
		if (train == null || train.worldObj.isRemote || !train.addedToChunk)
		{
			return;
		}

		/* Retrieve active cab and, if it exists, its reverser state and throttle level. */
		EReverser reverser = EReverser.LOCKED;
		float throttle = 0.0F;
		float dynamicBrake = 0.0F;
		{
			PartTypeCab activeCab = train.getActiveCab();
			if (activeCab != null)
			{
				reverser = activeCab.getReverserState();
				throttle = activeCab.controlPower.get();
				dynamicBrake = activeCab.controlBrakeDynamic.get();
			}
		}

		/*
		 * Determine the current 'activity' of the motor.
		 * The activity will increase during throttle application,
		 * and will return to 0 when idling or when the reverser is locked.
		 */

		/* Determine target activity the motor will try to achieve. */
		float activityTarget = 0.0F;
		float polarityTarget = 0.0F;

		if (reverser.isNeutral() || reverser == EReverser.LOCKED || !enabled.get())
		{
			activityTarget = 0.0F;
			polarityTarget = 0.0F;
		}
		else
		{
			if (throttle > 0.0F && dynamicBrake <= 0.0F)
			{
				activityTarget = throttle;
				polarityTarget = 1.0F;
			}

			if (throttle <= 0.0F && dynamicBrake > 0.0F && (train.getLastSpeed() * 20.0F * 3.6F) >= 7.0F)
			{
				activityTarget = dynamicBrake;
				polarityTarget = -1.0F;
			}

			if (throttle > 0.0F && dynamicBrake > 0.0F)
			{
				activityTarget = 0.0F;
				polarityTarget = 0.0F;
			}

			if (throttle <= 0.0F && dynamicBrake <= 0.0F)
			{
				activityTarget = 0.0F;
				polarityTarget = 0.0F;
			}
		}

		/*
		 * Determine the change in activity & polarity for the traction motors.
		 */

		/* The motor polarity is correct. */
		if (motorPolarity.get() == polarityTarget)
		{
			if (motorActivity.get() != activityTarget)
			{
				float x = (float)Math.abs(getTrain().getLastSpeed()) * 20.0F * 0.05F;
				if (x < 0.01F)
				{
					x = 0.005F;
				}
				else if (x > 0.05F)
				{
					x = 0.05F;
				}

				motorActivityVelocity.set((activityTarget - motorActivity.get()) * x);

				if (motorActivityVelocity.get() > 0.05F)
				{
					motorActivityVelocity.set(0.05F);
				}
				else if (motorActivityVelocity.get() < -0.05F)
				{
					motorActivityVelocity.set(-0.05F);
				}
			}
		}

		/* The motor polarity needs to change. */
		else
		{
			motorActivityVelocity.set((0.0F - motorActivity.get()) * 0.05F);

			if (motorActivityVelocity.get() > 0.05F)
			{
				motorActivityVelocity.set(0.05F);
			}
			else if (motorActivityVelocity.get() < -0.05F)
			{
				motorActivityVelocity.set(-0.05F);
			}

			if (Math.abs(motorActivity.get()) < 0.05F)
			{
				if (motorPolarity.get() > polarityTarget)
				{
					motorPolarity.set(motorPolarity.get() - 0.04F);
				}

				if (motorPolarity.get() < polarityTarget)
				{
					motorPolarity.set(motorPolarity.get() + 0.04F);
				}
			}
		}

		/*
		 * Motor activity velocity
		 */
		motorActivity.set(motorActivity.get() + motorActivityVelocity.get());

		tickPowerOutput();
	}
}