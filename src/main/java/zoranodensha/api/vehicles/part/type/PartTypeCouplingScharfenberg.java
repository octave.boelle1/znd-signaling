package zoranodensha.api.vehicles.part.type;

import net.minecraft.entity.player.EntityPlayer;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;

/**
 * Scharfenberg couplings.<br>
 * Couple and decouple automatically.
 */
public class PartTypeCouplingScharfenberg extends APartTypeCoupling.APartTypeCouplingAuto
{
    public PartTypeCouplingScharfenberg(VehParBase parent)
    {
        super(parent, zoranodensha.api.vehicles.part.type.PartTypeCouplingScharfenberg.class.getSimpleName());
    }

    @Override
    public boolean getIsCompatible(APartTypeCoupling coupling)
    {
        return (coupling instanceof zoranodensha.api.vehicles.part.type.PartTypeCouplingScharfenberg);
    }

    @Override
    public boolean onActivated(EntityPlayer player)
    {
        return false;
    }

    @Override
    public boolean onDecouple()
    {
        Train train = parent.getTrain();
        if (parent.getIsFinished() && train != null && !train.worldObj.isRemote)
        {
            return doTryDecouple();
        }
        return false;
    }
}