package zoranodensha.api.vehicles.part.type;

import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropEnum;



/**
 * Part types which can be loaded.<br>
 * <br>
 * This type provides a state label which declares whether a part may be (un-)loaded.
 * It does not specify <i>what</i> is loaded.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class PartTypeLoadable extends APartType
{
	/** Current {@link zoranodensha.api.vehicles.part.type.PartTypeLoadable.ELoadState load state} indicating whether this part may be (un-)loaded. */
	public final PropEnum<ELoadState> loadState;



	public PartTypeLoadable(VehParBase parent)
	{
		super(parent, PartTypeLoadable.class.getSimpleName());
		addProperty(loadState = (PropEnum<ELoadState>)new PropEnum<ELoadState>(parent, ELoadState.NONE, "loadState").setSyncDir(ESyncDir.BOTH).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
	}


	/**
	 * Return the current load state.
	 * 
	 * @return The current {@link ELoadState load state}.
	 */
	public ELoadState getLoadState()
	{
		return loadState.get();
	}

	/**
	 * Changes to the next {@link zoranodensha.api.vehicles.part.type.PartTypeLoadable.ELoadState load state}.
	 */
	public void nextLoadState()
	{
		int index = getLoadState().ordinal();
		if (index + 1 < ELoadState.values().length)
		{
			setLoadState(ELoadState.values()[index + 1]);
		}
		else
		{
			setLoadState(ELoadState.values()[0]);
		}
	}

	/**
	 * Override the current load state.
	 * 
	 * @param loadState - New {@link ELoadState load state} to set.
	 */
	public void setLoadState(ELoadState loadState)
	{
		this.loadState.set(loadState);
	}



	/**
	 * All available load states.
	 */
	public enum ELoadState
	{
		/** Don't load or unload. */
		NONE,
		/** Load this part. */
		LOAD,
		/** Unload this part. */
		UNLOAD,
		/** Load and unload simultaneously. */
		BOTH;


		/**
		 * @return {@code true} if loading is allowed.
		 */
		public boolean doLoad()
		{
			return (this == LOAD || this == BOTH);
		}

		/**
		 * @return {@code true} if unloading is allowed.
		 */
		public boolean doUnload()
		{
			return (this == UNLOAD || this == BOTH);
		}
	}
}