package zoranodensha.api.vehicles.part.type;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Activatable;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_PositionListener;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Tickable;
import zoranodensha.api.vehicles.part.type.seat.EntitySeat;
import zoranodensha.api.vehicles.part.type.seat.PropSeat;



/**
 * Seats where passengers (or drivers) can sit in.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class PartTypeSeat extends APartType implements IPartType_Activatable, IPartType_PositionListener, IPartType_Tickable
{
	/** Y-offset of an {@link net.minecraft.entity.Entity Entity} riding this seat. */
	public final PropFloat mountedYOffset;
	/** This part's seat entity. */
	public final PropSeat seat;



	public PartTypeSeat(VehParBase parent)
	{
		this(parent, PartTypeSeat.class.getSimpleName());
	}

	protected PartTypeSeat(VehParBase parent, String name)
	{
		super(parent, name);
		addProperty(mountedYOffset = (PropFloat)new PropFloat(parent, 0.7F, "mountedOffsetY").setConfigurable());
		addProperty(seat = new PropSeat(parent, "seatEntity"));
	}

	/**
	 * Called on all seats when a train falls.
	 */
	public void fall(float distance)
	{
		if (getSeat() != null)
		{
			getSeat().fall(distance);
		}
	}

	/**
	 * Relay-method from {@link net.minecraft.entity.Entity#getMountedYOffset() Entity#getMountedYOffset()}.<br>
	 * Return the offset on the Y-axis (height) that a riding Entity on this seat has.
	 */
	public double getMountedYOffset()
	{
		return mountedYOffset.get();
	}

	/**
	 * Return the passenger that currently occupies this seat.
	 * 
	 * @return {@link net.minecraft.entity.Entity Entity} occupying this seat, or {@code null} if none is there.
	 */
	public Entity getPassenger()
	{
		return (getSeat() != null ? getSeat().riddenByEntity : null);
	}

	/**
	 * Getter for the value of {@link #seat}.
	 */
	public EntitySeat getSeat()
	{
		return seat.get();
	}

	@Override
	public boolean onActivated(EntityPlayer player)
	{
		/* Ensure the player exists and isn't sneaking. */
		if (player == null || player.isSneaking())
		{
			return false;
		}

		/* Also make sure our parent part is finished and we are on a non-remote world in a spawned train. */
		Train train = getTrain();
		if (!getParent().getIsFinished() || train == null || !train.addedToChunk || train.worldObj.isRemote)
		{
			return false;
		}

		/*
		 * Find nearby door.
		 */
		for (VehParBase part : getSection().parts)
		{
			/* If a door was found, let it handle further action (e.g. doors opening). */
			PartTypeDoor partTypeDoor = part.getPartType(PartTypeDoor.class);
			if (partTypeDoor != null)
			{
				return partTypeDoor.onActivated(player);
			}
		}

		/*
		 * If no door was found, handle interaction on our own. Try to seat the player.
		 */
		return setPassenger(player);
	}

	@Override
	public void onTick()
	{
		EntitySeat seat = getSeat();
		if (seat != null)
		{
			/* Remove references if passenger has died. */
			if (seat.riddenByEntity != null && seat.riddenByEntity.isDead)
			{
				if (seat == seat.riddenByEntity.ridingEntity)
				{
					seat.riddenByEntity.ridingEntity = null;
				}

				seat.riddenByEntity = null;
				seat.setDead();
				setSeat(null);
			}

			/* Also remove references if the seat is dead. */
			else if (seat.isDead)
			{
				if (seat.riddenByEntity != null)
				{
					if (seat == seat.riddenByEntity.ridingEntity)
					{
						seat.riddenByEntity.ridingEntity = null;
					}
					seat.riddenByEntity = null;
				}
				setSeat(null);
			}

			/* Update seat position. */
			seat.prevPosX = seat.posX;
			seat.prevPosY = seat.posY;
			seat.prevPosZ = seat.posZ;
		}
	}

	/**
	 * Try to assign the given passenger to this seat. Check for permissions prior to assigning.
	 *
	 * @param entity - The {@link net.minecraft.entity.Entity Entity} to assign. Might be {@code null}.
	 * @return {@code true} if something changed.
	 */
	public boolean setPassenger(Entity entity)
	{
		/* If either this seat is not finished or there is no train, abort. */
		if (!parent.getIsFinished() || parent.getTrain() == null)
		{
			return false;
		}

		/* If the given passenger is null, remove seat. */
		if (entity == null)
		{
			EntitySeat seat = getSeat();
			if (seat != null)
			{
				if (seat.riddenByEntity != null)
				{
					seat.riddenByEntity.ridingEntity = null;
					seat.riddenByEntity = null;
				}

				if (!seat.worldObj.isRemote)
				{
					seat.setDead();
				}

				setSeat(null);
				return true;
			}
		}

		/* Otherwise check whether there's a seat entity existent. If not, spawn. */
		else if (getSeat() == null)
		{
			Train train = parent.getTrain();
			if (!train.worldObj.isRemote)
			{
				/* Ensure the seat was actually spawned. If not, kill seat and reject. */
				EntitySeat seat = new EntitySeat(this);
				if (train.worldObj.spawnEntityInWorld(seat) && setSeat(seat))
				{
					entity.mountEntity(seat);
				}
				else
				{
					seat.setDead();
					return false;
				}
			}

			return true;
		}

		return false;
	}

	@Override
	public void setPosition(double x, double y, double z)
	{
		if (getSeat() != null)
		{
			getSeat().setPosition(x, y, z);
		}
	}

	/**
	 * Setter for the {@link #seat}.
	 * 
	 * @return {@code true} if the given seat was applied.
	 */
	public boolean setSeat(EntitySeat seat)
	{
		return this.seat.set(seat);
	}
}