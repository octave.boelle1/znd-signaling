package zoranodensha.api.vehicles.part.type;

import java.util.Random;

import net.minecraft.util.MathHelper;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;
import zoranodensha.api.vehicles.part.type.cab.PropReverser.EReverser;
import zoranodensha.api.vehicles.part.property.PropInteger;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;



/**
 * Diesel engines, burning diesel fluid to create force.
 */
public class PartTypeEngineDiesel extends APartTypeEngine
{
	/** Random number generator, used to randomise RPM decay. */
	private static final Random ran = new Random();

	/** Current RPM of the engine. */
	public final PropFloat currentRPM;

	/** Engine efficiency. 75% is average for a railway diesel engine. */
	public final PropFloatBounded efficiency;
	/** This engine's responsiveness, which affects how quickly the RPM changes. */
	public final PropFloatBounded responsiveness;

	/**
	 * A {@code float} ranging from {@code 0.0F} to {@code 1.0F} which represents how much the electrical generator is generating electricity from the engine.
	 */
	public final PropFloatBounded generatorActivity;

	/**
	 * This engine's idling RPM.
	 */
	public final PropInteger idleRPM;
	/** Maximum service RPM of the engine. This is the RPM when the throttle is at the maximum value. */
	public final PropInteger maxRPM;


	public PartTypeEngineDiesel(VehParBase parent)
	{
		super(parent, PartTypeEngineDiesel.class.getSimpleName());

		addProperty(currentRPM = (PropFloat)new PropFloat(parent, 0.0F, "rpm").setSyncDir(ESyncDir.CLIENT).setValidTagCalls(EValidTagCalls.PACKET_SAVE));
		addProperty(efficiency = (PropFloatBounded)new PropFloatBounded(parent, 0.01F, 1.0F, "efficiency").setConfigurable());
		addProperty(responsiveness = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, 1.0F, "responsiveness").setConfigurable());
		addProperty(generatorActivity = (PropFloatBounded)new PropFloatBounded(parent, 0.0F, 1.0F, "generatorActivity").setValidTagCalls(EValidTagCalls.PACKET).setSyncDir(ESyncDir.CLIENT));
		addProperty(idleRPM = new PropInteger(parent, "rpmIdle"));
		addProperty(maxRPM = new PropInteger(parent, "rpmMaximum"));
	}


	/**
	 * Getter to access the current RPM value.
	 */
	public float getCurrentRPM()
	{
		return currentRPM.get();
	}

	@Override
	public float getProducedForce()
	{
		return producedForce.get();
	}

	private void tickPowerOutput()
	{
		if (getTrain() == null || getTrain().getActiveCab() == null)
		{
			producedForce.set(0.0F);
			return;
		}

		float force = 0.0F;
		PartTypeCab cab = parent.getTrain().getActiveCab();
		EReverser cabReverser;

		/*
		 * Determine the total amount of tractive effort possible with the powered bogies in this vehicle.
		 */
		OrderedTypesList<PartTypeBogie> bogies = this.getVehicle().getTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
		float vehicleMaxTraction = 0.0F;
		for (PartTypeBogie bogie : bogies)
		{
			if (bogie.getHasMotor())
			{
				vehicleMaxTraction += bogie.tractiveEffort.get();
			}
		}

		/*
		 * Determine the total amount of power (kW) available in this vehicle.
		 * This value will be used to balance the load between all available engines, even if they have different power levels.
		 */
		OrderedTypesList<APartTypeEngine> engines = this.getVehicle().getTypes(OrderedTypesList.ASELECTOR_ENGINE);
		float vehicleTotalPower = 0.0F;
		for (APartTypeEngine engine : engines)
		{
			vehicleTotalPower += (float)engine.outputKW.get();
		}

		/*
		 * Disable motors if there are no powered bogies in this vehicle, or none of the engines can produce power.
		 */
		if (vehicleMaxTraction <= 0.0F || vehicleTotalPower <= 0.0F)
		{
			producedForce.set(0.0F);
			return;
		}

		/*
		 * Disable motors if there is no active cab.
		 */
		if (cab == null)
		{
			producedForce.set(0.0F);
			return;
		}

		cabReverser = cab.getReverserState();

		/* The current power being produced by the engine at this moment. */
		float power;
		final float currentSpeed = (float)Math.abs(getTrain().getLastSpeed()) * 20.0F;
		final float fastestMaxEffort = vehicleTotalPower / vehicleMaxTraction;

		float currentMaxTraction;
		if (currentSpeed > fastestMaxEffort)
		{
			currentMaxTraction = vehicleTotalPower / currentSpeed;
		}
		else
		{
			currentMaxTraction = vehicleMaxTraction;
		}

		/* Calculate the current horsepower. */
		if (currentRPM.get() >= idleRPM.get())
		{
			power = currentMaxTraction * (currentRPM.get() / maxRPM.get());
		}
		else
		{
			power = 0.0F;
		}

		if (power > currentMaxTraction)
		{
			power = currentMaxTraction;
		}

		// Balance with other engines in vehicle
		power *= ((float)outputKW.get() / vehicleTotalPower);

		/* Clamp and invert if necessary. */
		if (!cabReverser.isForwardMovement())
		{
			power *= -1.0F;
		}

		power *= generatorActivity.get();

		/* Return. */
		producedForce.set(power);
	}

	@Override
	public void onTick()
	{
		/* Only update on server worlds. */
		Train train = getTrain();
		if (train == null || train.worldObj.isRemote || !train.addedToChunk)
		{
			return;
		}

		/* Retrieve active cab and, if it exists, its throttle level. */
		float throttle = 0.0F;
		{
			PartTypeCab activeCab = train.getActiveCab();
			if (activeCab != null)
			{
				if (activeCab.getReverserState().equals(EReverser.FORWARD) || activeCab.getReverserState().equals(EReverser.BACKWARD))
				{
					throttle = activeCab.controlPower.get();

					if (throttle < 0.0F)
					{
						throttle = 0.0F;
					}
				}
			}
		}

		/*
		 * Update the generator activity.
		 */
		generatorActivity.set(generatorActivity.get() + (((throttle != 0.0F ? 1.0F : 0.0F) - generatorActivity.get()) / 20.0F));

		/*
		 * Update current RPM each tick.
		 */
		{
			/* Get previous tick's RPM value. */
			float prevRPM = currentRPM.get();

			float governor;

			/* Determine the target RPM. */
			if (enabled.get())
			{
				governor = idleRPM.get() + (throttle * (maxRPM.get() - idleRPM.get()));
			}
			else
			{
				governor = 0.0F;
			}

			/* Get the maximum rate of RPM change, based on the responsiveness value. */
			float revvingSpeed = 5.0F * responsiveness.get();

			/* Get how much the RPM needs to move and then clamp it to the maximum revving speed. */
			float rpmMovement = (governor - currentRPM.get()) / 5.0F;
			rpmMovement -= (ran.nextFloat() * 1.2F);
			rpmMovement = MathHelper.clamp_float(rpmMovement, -revvingSpeed, revvingSpeed);

			/* Move the RPM. */
			prevRPM = MathHelper.clamp_float(prevRPM + rpmMovement, 0.0F, maxRPM.get());

			currentRPM.set(prevRPM);
		}

		/*
		 * Tick the resulting power output.
		 */
		tickPowerOutput();
	}
}