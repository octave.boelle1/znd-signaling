package zoranodensha.api.vehicles.part.type.cab;

import zoranodensha.api.structures.signals.TrainPulse;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.property.PropEnum;
import zoranodensha.api.vehicles.part.type.PartTypeCab;
import zoranodensha.api.vehicles.part.type.cab.PropReverser.EReverser;



/**
 * Reverser property for cab types.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class PropReverser extends PropEnum<EReverser>
{
	public PropReverser(PartTypeCab cab, String name)
	{
		super(cab.getParent(), EReverser.LOCKED, name);
		setValidTagCalls(EValidTagCalls.PACKET_SAVE);
		setSyncDir(ESyncDir.CLIENT);
	}


	@Override
	public boolean set(Object property)
	{
		/* Ensure the reverser state doesn't change while moving. */
		if (property instanceof EReverser)
		{
			Train train = getParent().getTrain();
			if (train != null && train.addedToChunk && !train.worldObj.isRemote)
			{
				/* Abort if train is moving and either the reverser direction would change, or the reverser would be locked. */
				EReverser rev = (EReverser)property;
				if (train.getLastSpeed() != 0.0 && (get().isForwardMovement() != rev.isForwardMovement() || EReverser.LOCKED == rev))
				{
					return false;
				}
			}
		}
		return super.set(property);
	}



	/**
	 * Enumeration containing valid reverser states.
	 */
	public enum EReverser
	{
		/** Locked (default) state */
		LOCKED,
		/** Move backwards */
		BACKWARD,
		/** Neutral level, lock movement. (Previous: {@link #BACKWARD}) */
		NEUTRAL_BACKWARD,
		/** Neutral level, lock movement. (Previous: {@link #FORWARD}) */
		NEUTRAL_FORWARD,
		/** Move forward */
		FORWARD;


		/**
		 * @return {@code true} if this state dictates forward movement.
		 */
		public boolean isForwardMovement()
		{
			return (this == FORWARD || this == NEUTRAL_FORWARD);
		}

		/**
		 * @return {@code true} if this state is neutral.
		 */
		public boolean isNeutral()
		{
			return (this == NEUTRAL_BACKWARD || this == NEUTRAL_FORWARD);
		}

		/**
		 * Returns the logically subsequent reverser state.
		 */
		public EReverser next()
		{
			switch (this)
			{
				case LOCKED:
					return BACKWARD;

				case BACKWARD:
					return NEUTRAL_BACKWARD;

				case NEUTRAL_FORWARD:
				case NEUTRAL_BACKWARD:
					return FORWARD;

				default:
					return this;
			}
		}

		/**
		 * Returns the logically previous reverser state.
		 */
		public EReverser prev()
		{
			switch (this)
			{
				case BACKWARD:
					return LOCKED;

				case NEUTRAL_FORWARD:
				case NEUTRAL_BACKWARD:
					return BACKWARD;

				case FORWARD:
					return NEUTRAL_FORWARD;

				default:
					return this;
			}
		}
	}
}