package zoranodensha.api.vehicles.part.type;

import org.apache.logging.log4j.Level;

import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.handlers.PneumaticsHandler;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.property.PropVessel;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Tickable;
import zoranodensha.api.vehicles.part.util.ConcatenatedTypesList;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.common.core.ModCenter;



/**
 * A part type representing a component which creates compressed air to be used by pneumatic components on the train.
 * 
 * @author Jaffa
 */
public class PartTypeCompressor extends APartType implements IPartType_Tickable
{
	/** The base speed for all compressors, in litres per tick. */
	protected static final float COMPRESSOR_BASE_SPEED = 500.0F;

	/*
	 * Configurable Properties
	 */
	/** A multiplier for the speed of the compressor. */
	public final PropFloatBounded compressorSpeed;
	/** The compressor will switch off when the main reservoir reaches this pressure. */
	public final PropIntegerBounded maximumPressure;
	/** The compressor will turn on when the main reservoir drops below this pressure. */
	public final PropIntegerBounded minimumPressure;

	/*
	 * Other Properties
	 */
	/** A flag indicating whether this compressor is running. */
	public final PropBoolean compressorEnabled;
	/** Atmospheric air will be compressed by the compressor and placed into this reservoir. From this reservoir, the air pressure is sent throughout the train. */
	public final PropVessel compressorMainReservoir;



	/**
	 * Initialises a new instance of the {@link zoranodensha.api.vehicles.part.type.PartTypeCompressor} class.
	 * 
	 * @param parent - The parent vehicle part which holds this instance.
	 */
	public PartTypeCompressor(VehParBase parent)
	{
		super(parent, PartTypeCompressor.class.getSimpleName());

		/*
		 * Configurable Properties
		 */
		addProperty(compressorSpeed = (PropFloatBounded)new PropFloatBounded(parent, 1.0F, 0.1F, 10.0F, "compressorSpeedMultiplier").setConfigurable());
		addProperty(maximumPressure = (PropIntegerBounded)new PropIntegerBounded(parent, 950, 200, 2000, "compressorMaximumPressure").setConfigurable());
		addProperty(minimumPressure = (PropIntegerBounded)new PropIntegerBounded(parent, 750, 100, 1900, "compressorMinimumPressure").setConfigurable());

		/*
		 * Other Properties
		 */
		addProperty(compressorEnabled = (PropBoolean)new PropBoolean(parent, false, "compressorEnabled").setSyncDir(ESyncDir.BOTH));
		addProperty(compressorMainReservoir = new PropVessel(parent, 300.0F, 1000.0F, 750.0F, "compressorMainReservoir"));
	}

	@Override
	public void onTick()
	{
		Train train = getTrain();

		/*
		 * Server-side logic only
		 */
		if (!train.worldObj.isRemote)
		{
			/*
			 * Get the nearest available bogie.
			 */
			ConcatenatedTypesList<PartTypeBogie> bogies = train.getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
			PartTypeBogie closestBogie = null;
			if (bogies.size() >= 2)
			{
				closestBogie = bogies.getClosestPartType(this.getLocalX());
			}
			else
			{
				closestBogie = bogies.get(0);
			}

			/*
			 * Run a quick null check for the bogie.
			 */
			if (closestBogie != null)
			{

				/*
				 * While the compressor is switched on, increase the air pressure of the output.
				 */
				if (compressorEnabled.get())
				{
					if (!compressorMainReservoir.isFull())
					{
						compressorMainReservoir.increase(COMPRESSOR_BASE_SPEED * compressorSpeed.get());
					}
				}

				/*
				 * Transfer the air pressure inside this compressor to the nearest available bogie.
				 */
				if (compressorMainReservoir.getPressure() > 0.0F)
				{
					for (int iteration = 0; iteration < 5; iteration++)
					{
						PneumaticsHandler.INSTANCE.equalise(compressorMainReservoir, closestBogie.pneumaticsMainReservoirPipe, 0.0F);
					}
				}

				/*
				 * This section of code is for the 'compressor governor' - switches the compressor on or off depending on the pressure in the main reservoir.
				 */
				if (compressorEnabled.get())
				{
					if (compressorMainReservoir.getPressure() >= maximumPressure.get())
						compressorEnabled.set(false);
				}
				else
				{
					if (compressorMainReservoir.getPressure() < minimumPressure.get())
						compressorEnabled.set(true);
				}

			}

			/*
			 * We can't get a bogie instance, so just turn off the compressor by default.
			 */
			else
			{
				compressorEnabled.set(false);
			}
		}
	}
}
