package zoranodensha.api.vehicles.part.type.cab;

import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.APartProperty;
import zoranodensha.api.vehicles.part.type.cab.mrtms.Landmark;
import zoranodensha.api.vehicles.part.type.cab.mrtms.Landscape;



public class PropLandscape extends APartProperty<Landscape>
{
	private static final String TOTAL_LENGTH = "_totalLength";
	private static final String LANDMARKS_COUNT = "_landmarksCount";
	private static final String LANDMARK_LENGTH = "_landmarkLength";
	private static final String LANDMARK_LOCATION = "_landmarkLocation";
	private static final String LANDMARK_SPEED_LIMIT = "_landmarkSpeedLimit";
	private static final String LANDMARK_STATION = "_landmarkIsStation";
	private static final String LANDMARK_TERMINATION = "_landmarkIsTermination";

	/**
	 * Constructor for a Landscape part property (for cabs).
	 *
	 * @param parent - Parent vehicle part of this vehicle part property.
	 * @param name - Name of this property.
	 */
	public PropLandscape(VehParBase parent, String name)
	{
		super(parent, Landscape.class, name);
	}

	@Override
	protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
	{
		if (!nbt.hasKey(nbtKey) || !nbt.getBoolean(nbtKey))
		{
			set(null);
			return true;
		}

		Landscape readLandscape = new Landscape(nbt.getFloat(nbtKey + TOTAL_LENGTH));

		Landmark readLandmark;
		for (int i = 0; i < nbt.getInteger(nbtKey + LANDMARKS_COUNT); i++)
		{
			readLandmark = new Landmark(nbt.getFloat(nbtKey + LANDMARK_LOCATION + i), nbt.getBoolean(nbtKey + LANDMARK_STATION + i), nbt.getBoolean(nbtKey + LANDMARK_TERMINATION + i), nbt.getFloat(nbtKey + LANDMARK_LENGTH + i),
										nbt.getFloat(nbtKey + LANDMARK_SPEED_LIMIT + i));

			readLandscape.addLandmark(readLandmark);
		}

		set(readLandscape);
		return true;
	}

	@Override
	protected void writeToNBT(NBTTagCompound nbt, String nbtKey)
	{
		Landscape writeLandscape = get();

		if (writeLandscape == null)
		{
			nbt.setBoolean(nbtKey, false);
			return;
		}

		nbt.setBoolean(nbtKey, true);
		nbt.setFloat(nbtKey + TOTAL_LENGTH, writeLandscape.getTotalLength());
		nbt.setInteger(nbtKey + LANDMARKS_COUNT, writeLandscape.getAllLandmarks().size());

		Landmark writeLandmark;
		for (int i = 0; i < writeLandscape.getAllLandmarks().size(); i++)
		{
			writeLandmark = writeLandscape.getAllLandmarks().get(i);

			nbt.setFloat(nbtKey + LANDMARK_LOCATION + i, writeLandmark.getLocation());
			nbt.setBoolean(nbtKey + LANDMARK_STATION + i, writeLandmark.getIsStation());
			nbt.setBoolean(nbtKey + LANDMARK_TERMINATION + i, writeLandmark.getIsTermination());
			nbt.setFloat(nbtKey + LANDMARK_LENGTH + i, writeLandmark.getLength());
			nbt.setFloat(nbtKey + LANDMARK_SPEED_LIMIT + i, writeLandmark.getSpeedLimit());
		}
	}
}
