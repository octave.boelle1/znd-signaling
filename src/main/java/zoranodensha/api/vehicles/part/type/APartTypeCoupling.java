package zoranodensha.api.vehicles.part.type;

import net.minecraft.util.AxisAlignedBB;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.handlers.CouplingHandler;
import zoranodensha.api.vehicles.handlers.MovementHandler;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Activatable;



/**
 * Parts implementing this type may be used by the {@link zoranodensha.api.vehicles.handlers.CouplingHandler CouplingHandler} to join trains together.
 * 
 * @author Leshuwa Kaiheiwa
 */
public abstract class APartTypeCoupling extends APartType implements IPartType_Activatable
{
	/** Globally used default coupling height. Feel free to change your coupling's local Y-offset anyways. Usage still recommended. */
	public static final float COUPLING_DEFAULT_OFFSET_Y = 0.65F;
	
	/** Distance this coupling expands beyond its bounding box, in meters. Influences by the parent part's scale along the X-axis. */
	public final PropFloatBounded expansion;
	/** Maximum length of this coupling when extended, in meters. */
	public final PropFloat length;



	public APartTypeCoupling(VehParBase parent, String name)
	{
		super(parent, name);

		/* Expansion defaults to zero. */
		addProperty(expansion = new PropFloatBounded(parent, 0.0F, 2.0F, "expansion"));

		/* Default length is defined as half of the parent's default bounding box X-size. */
		AxisAlignedBB parentBox = getParent().initBoundingBox();
		addProperty(length = new PropFloatBounded(parent, (float)((parentBox.maxX - parentBox.minX) / 2.0), 0.0F, 10.0F, "length"));
		
		/* If the parent doesn't contain a bogie type, set the parent part's offset to a preset value for easier alignment with other vehicles. */
		if (getParent().getPartType(PartTypeBogie.class) == null)
		{
			float[] offset = getParent().getOffset();
			getParent().setOffset(offset[0], COUPLING_DEFAULT_OFFSET_Y, offset[2]);
		}
	}


	/**
	 * Called to try to couple this coupling, if applicable.
	 * 
	 * @return {@code true} if a coupling connection was successfully established.
	 */
	protected boolean doTryCouple()
	{
		return CouplingHandler.INSTANCE.tryCouple(this);
	}

	/**
	 * Called to try to decouple this coupling, if applicable.
	 * 
	 * @return {@code true} if there was a connection existent which was successfully dissolved.
	 */
	protected boolean doTryDecouple()
	{
		return CouplingHandler.INSTANCE.tryDecouple(this);
	}

	/**
	 * Returns the distance this coupling may expand beyond its bounding box, in meters.<br>
	 * This is used to expand the parent part's bounding box to check whether this coupling connects to another coupling.
	 * 
	 * @return Distance this coupling may expand, in meters.
	 */
	public float getExpansion()
	{
		return expansion.get() * getParent().getScale()[0];
	}

	/**
	 * Check whether the given coupler may connect to this coupler. Run permission checks here.<br>
	 * Actual coupling is handled by the active {@link zoranodensha.api.vehicles.handlers.CouplingHandler CouplingHandler}.
	 *
	 * @param coupling - The other coupler to couple to.
	 * @return {@code true} if coupling may happen, {@code false} if not.
	 */
	public abstract boolean getIsCompatible(APartTypeCoupling coupling);

	/**
	 * Returns whether this coupling couples to another vehicle.<br>
	 * More formally, returns whether this coupling's parent vehicle has a neighbouring vehicle on the same side as this coupling.
	 * 
	 * @return {@code true} if this coupling is active.
	 */
	public boolean getIsCoupled()
	{
		/* Get references to parent vehicle and train. */
		VehicleData vehicle = getVehicle();
		if (vehicle == null)
		{
			return false;
		}

		Train train = vehicle.getTrain();
		if (train == null || train.getVehicleCount() < 2)
		{
			return false;
		}

		/* If this coupling is the front coupling of its vehicle.. */
		if (vehicle.getCouplingFront() == this)
		{
			/* ..return true if the vehicle has a neighbour in front of itself. */
			return (train.getVehicleAt(train.getVehicleIndex(vehicle) - 1) != null);
		}

		/* If this coupling is the back coupling of its vehicle.. */
		if (vehicle.getCouplingBack() == this)
		{
			/* ..return true if the vehicle has a neighbour behind it. */
			return (train.getVehicleAt(train.getVehicleIndex(vehicle) + 1) != null);
		}

		return false;
	}

	/**
	 * Return the length of this coupling.<br>
	 * Used to calculate correct distances between couplings.
	 * 
	 * @return The length of this coupling.
	 */
	public float getLength()
	{
		return length.get();
	}

	/**
	 * <p>
	 * Check whether the given coupling may connect to this coupling.
	 * </p>
	 * 
	 * <p>
	 * Run permission checks here.<br>
	 * Actual coupling is handled by the active {@link zoranodensha.api.vehicles.handlers.CouplingHandler CouplingHandler}.
	 * </p>
	 *
	 * @param coupling - The other coupling to couple to.
	 * @return {@code true} if coupling may happen, {@code false} if not.
	 */
	public boolean getMayConnect(APartTypeCoupling coupling)
	{
		if (getParent().getIsFinished())
		{
			return getIsCompatible(coupling) && !CouplingHandler.INSTANCE.getHasBroken(this, coupling);
		}
		return false;
	}

	/**
	 * Called to ask this coupling to automatically terminate current coupler connection, if possible.
	 * 
	 * @return {@code true} if the coupler connection was successfully terminated, {@code false} if not or if there was no connection in the first place.
	 */
	public boolean onDecouple()
	{
		return false;
	}



	/**
	 * Base class for automatic couplings.
	 */
	public static abstract class APartTypeCouplingAuto extends APartTypeCoupling implements IPartType_Tickable
	{
		public APartTypeCouplingAuto(VehParBase parent, String name)
		{
			super(parent, name);
		}
		
		@Override
		public boolean getMayConnect(APartTypeCoupling coupling)
		{
			if (getParent().getIsFinished())
			{
				return getIsCompatible(coupling) && CouplingHandler.INSTANCE.getCloseEnoughToCouple(this, coupling);
			}
			return false;
		}
		
		@Override
		public void onTick()
		{
			Train train = getTrain();
			if (getParent().getIsFinished() && train != null && !train.worldObj.isRemote)
			{
				/* Try to couple with whatever is nearby if vehicle isn't standing still. */
				if (Math.abs(train.getLastSpeed()) > MovementHandler.MOVEMENT_THRESHOLD_XZ || train.isCollided)
				{
					doTryCouple();
				}
			}
		}
	}
}