package zoranodensha.api.vehicles.part.type;

import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropEnum;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;



/**
 * Lamp part type.<br>
 * These parts provide functionality for renderers which display light flares and light beams.
 * 
 * @author Jaffa
 * @author Leshuwa Kaiheiwa
 */
public class PartTypeLamp extends APartType
{
	/** Light flare behaviour depicting when light flares should be rendered. */
	public final PropEnum<EFlareBehaviour> behaviour;
	/** Property holding whether this part should render the light beam. */
	public final PropBoolean beamEnabled;
	/** Property holding whether this part should render the light flare. */
	public final PropBoolean flareEnabled;
	/** Height of the light beam cone. */
	public final PropFloat beamLength;
	/** Base radius of the light beam cone. */
	public final PropFloat beamRadius;
	/** Light flare's angle of tilt (in degrees) relative to the light cone. Useful when placing this part over a tilted headlight, normally making the flare not fit properly. */
	public final PropFloat flareTilt;



	public PartTypeLamp(VehParBase parent)
	{
		super(parent, PartTypeLamp.class.getSimpleName());

		addProperty(behaviour = (PropEnum<EFlareBehaviour>)new PropEnum<EFlareBehaviour>(parent, EFlareBehaviour.FRONT_HIGH, "behaviour").setConfigurable());
		addProperty(beamEnabled = (PropBoolean)new PropBoolean(parent, "beamEnabled").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(flareEnabled = (PropBoolean)new PropBoolean(parent, "flareEnabled").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(beamLength = (PropFloat)new PropFloatBounded(parent, 25.0F, 0.0F, 100.0F, "beamLength").setConfigurable());
		addProperty(beamRadius = (PropFloat)new PropFloatBounded(parent, 0.0F, 5.0F, 20.0F, "beamRadius").setConfigurable());
		addProperty(flareTilt = (PropFloat)new PropFloat(parent, 0.0F, "flareTilt").setConfigurable());
	}


	/**
	 * Return the selected flare behavior of this lamp.
	 * 
	 * @return This lamp's {@link zoranodensha.api.vehicles.part.type.PartTypeLamp.EFlareBehaviour flare behavior}.
	 */
	public EFlareBehaviour getFlareBehaviour()
	{
		return behaviour.get();
	}

	/**
	 * Set whether the light beam is active and should be rendered on client side.
	 * 
	 * @param isActive - {@code true} if the light beam is active.
	 */
	public void setBeamEnabled(boolean isActive)
	{
		beamEnabled.set(isActive);
	}

	/**
	 * Set whether the light flare is active and should be rendered on client side.
	 * 
	 * @param isActive - {@code true} if the light flare is active.
	 */
	public void setFlareEnabled(boolean isActive)
	{
		flareEnabled.set(isActive);
	}

	/**
	 * Determine the correct state for this lamp from the given data.
	 * 
	 * @param lightMode - Current light {@link zoranodensha.api.vehicles.part.type.PartTypeLamp.ELightMode mode}.
	 * @param beamsEnabled - {@code true} if beams should be displayed where applicable.
	 */
	public void setFromLightMode(ELightMode lightMode, boolean beamsEnabled)
	{
		/* Assign respective values depending on this lamp's flare behavior. */
		switch (getFlareBehaviour())
		{
			case ALWAYS_HIGH: {
				setBeamEnabled(beamsEnabled && (lightMode == ELightMode.FRONT));
				setFlareEnabled(true);
				break;
			}

			case ALWAYS: {
				setBeamEnabled(false);
				setFlareEnabled(true);
				break;
			}

			case FRONT_HIGH: {
				setBeamEnabled(beamsEnabled && (lightMode == ELightMode.FRONT));
				setFlareEnabled(lightMode == ELightMode.FRONT);
				break;
			}

			case FRONT: {
				setBeamEnabled(false);
				setFlareEnabled(lightMode == ELightMode.FRONT);
				break;
			}
			
			case HIGH: {
				setBeamEnabled(beamsEnabled && (lightMode == ELightMode.FRONT));
				setFlareEnabled(beamsEnabled && (lightMode == ELightMode.FRONT));
				break;
			}

			case REAR: {
				setBeamEnabled(false);
				setFlareEnabled(lightMode == ELightMode.REAR);
				break;
			}
		}
	}



	/**
	 * Standard light modes.
	 */
	public enum ELightMode
	{
		/** Lights are off. */
		OFF,
		/** Rear lights (usually red). */
		REAR,
		/** Front lights (usually white). */
		FRONT
	}

	/**
	 * Various behavior settings for this lamp and its light flares/ beams.
	 */
	public enum EFlareBehaviour
	{
		/**
		 * This flare is turned on in any rear mode.
		 */
		REAR,

		/**
		 * This flare is turned on in any forward mode.
		 */
		FRONT,

		/**
		 * This flare is turned on in any forward mode.<br>
		 * Allows the flare to emit a light beam as well.
		 */
		FRONT_HIGH,
		
		/**
		 * This flare is only turned on while in forward mode with high beams on.
		 */
		HIGH,

		/**
		 * This flare is always turned on, even when the lights are in the 'off' mode.
		 */
		ALWAYS,

		/**
		 * This flare is always turned on, even when the lights are in the 'off' mode.<br>
		 * Unlike other modes, this will allow the flare to emit a light beam as well.
		 */
		ALWAYS_HIGH
	}
}