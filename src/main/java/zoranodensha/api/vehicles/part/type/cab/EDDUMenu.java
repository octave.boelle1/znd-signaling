package zoranodensha.api.vehicles.part.type.cab;

/**
 * An enum representing all available menus. Each menu has an index which can be
 * accessed with {@link EDDUMenu#getIndex()}.
 * 
 * @author Jaffa
 */
public enum EDDUMenu
{
	/*
	 * Operation Menus
	 */
	MENU_OPERATION(0),
	MENU_OPERATION_DETAILED(1),

	/*
	 * Menus and Settings
	 */
	MENU_SETTINGS(51),

	/*
	 * Information Menus
	 */
	MENU_INFORMATION(100),
	MENU_INFORMATION_DETAILED(101);



	/**
	 * The index of the menu. This is the number that is assigned to
	 * {@link DDU#menu} to change the current menu.
	 */
	private final int index;



	/**
	 * Private constructor for the EDDUMenu class.
	 * 
	 * @param index - The index of the screen.
	 */
	private EDDUMenu(int index)
	{
		this.index = index;
	}

	/**
	 * Gets the index of this menu. This integer can be used to change the current
	 * screen by assigning {@link DDU#menu} to what this method returns.
	 * 
	 * @return - An integer representing the index of this menu.
	 */
	public int getIndex()
	{
		return index;
	}
}