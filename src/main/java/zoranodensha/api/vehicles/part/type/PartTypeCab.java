package zoranodensha.api.vehicles.part.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import javax.annotation.Nullable;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import zoranodensha.api.structures.signals.*;
import zoranodensha.api.structures.signals.landmark.ALandmark;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.IDriverKey;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.handlers.PneumaticsHandler;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.*;
import zoranodensha.api.vehicles.part.type.APartTypeCoupling.APartTypeCouplingAuto;
import zoranodensha.api.vehicles.part.type.PartTypeDoor.EDoorState;
import zoranodensha.api.vehicles.part.type.PartTypeLamp.ELightMode;
import zoranodensha.api.vehicles.part.type.cab.*;
import zoranodensha.api.vehicles.part.type.cab.PropButton.PropButtonLock;
import zoranodensha.api.vehicles.part.type.cab.PropReverser.EReverser;
import zoranodensha.api.vehicles.part.type.cab.mrtms.Landscape;
import zoranodensha.api.vehicles.part.util.ConcatenatedTypesList;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.api.vehicles.util.VehicleHelper;



/**
 * <p>
 * This part type provides a common cab implementation.
 * </o>
 *
 * <p>
 * Zora no Densha provides key detection for a specified set of keys. Any key presses of a player sitting in this part type will be received and handled here. There is no need for custom key handling or packet transfer.
 * </p>
 * <p>
 * This cab type provides following functionality:
 * <ul>
 * <li><b>Alerter timer with confirmation button</b></li>
 * <li><b>Destination declaration using the</b> {@code /setdest} <b>command</b></li>
 * <li><b>Reverser and dynamic throttle</b></li>
 * <li>Various buttons, such as:</li>
 * <ul>
 * <li>Automatic decoupling</li>
 * <li><b>Door state toggle</b></li>
 * <li><b>Emergency brake</b></li>
 * <li><b>Head lights</b></li>
 * <li><b>Horn</b></li>
 * <li>Interior lights (cab)</li>
 * <li>Interior lights (train)</li>
 * <li>Various multi-purpose buttons</li>
 * </ul>
 * </ul>
 *
 * <p>
 * Some functionality may not be supported by certain types of cabs.<br>
 * However, any functionality marked in bold <b>must</b> be supported by the implementing cab.
 * </p>
 *
 * @author Leshuwa Kaiheiwa
 */
public class PartTypeCab extends PartTypeSeat implements IPulseEmitter
{
	/**
	 * Minecraft Train Management System
	 */
	public final MTMS mtms;

	/*
	 * Important Cab Controls
	 */
	/**
	 * This cab's {@link zoranodensha.api.vehicles.part.type.cab.PropReverser.EReverser reverser} state.
	 */
	public final PropReverser reverser;


	/**
	 * <p>The dynamic brake system control value of this cab. This value controls the regenerative/rheostatic braking system on passenger trains and/or the dynamic brake system on freight trains. Should range from {@code 0.0F} to
	 * {@code 1.0F} (<b>dynamic brake off</b> to <b>dynamic brake fully applied</b> respectively).</p>
	 * <p>This variable is manipulated by the parent cab vehicle part class. If the parent cab in question does not contain a dynamic brake controller, this property will remain at {@code 0.0F}.</p>
	 */
	public final PropFloat controlBrakeDynamic;

	/**
	 * <p>The electro-pneumatic braking system control value of this cab. This value controls the electronically-controlled pneumatic brake on passenger trains. Should range from {@code 0.0F} to
	 * {@code 1.0F} (<b>electro-pneumatic brake released</b> to <b>electro-pneumatic brake fully applied</b> respectively).</p>
	 * <p>This variable is manipulated by the parent cab vehicle part class. If the parent cab in question does not contain an electro-pneumatic brake controller (i.e. freight locomotive cabs), this property will remain at
	 * {@code 0.0F}.</p>
	 */
	public final PropFloat controlBrakeElectroPneumatic;

	/**
	 * <p>This cab's emergency brake control value. This value should be {@code true} when the controls of the cab are in a position where the emergency brake should be applied.</p>
	 * <p>This variable is manipulated by the parent cab vehicle part class. If the parent cab in question does not contain an emergency brake valve or some kind (that would be pretty sketchy), this property will remain at
	 * {@code false}.</p>
	 */
	public final PropBoolean controlBrakeEmergency;

	/**
	 * <p>The independent braking system control value of this cab. This value controls the electronically-controlled pneumatic brake only on the bogies in the same vehicle as this cab. Should
	 * range from {@code 0.0F} to {@code 1.0F} (<b>independent brake released</b> to <b>independent brake fully applied</b> respectively).</p>
	 * <p>This variable is manipulated by the parent cab vehicle part class. If the parent cab in question does not contain an independent brake controller, this property will remain at {@code 0.0F}.</p>
	 */
	public final PropFloat controlBrakeIndependent;

	/**
	 * <p>The pneumatic braking system control value of this cab. This value controls the purely-air-driven pneumatic brake on freight trains (and occasionally on passenger trains as a backup). Should range from {@code 0.0F} to
	 * {@code 1.0F} (<b>pneumatic brake released</b> to <b>pneumatic brake fully applied</b> respectively).</p>
	 * <p>This variable is manipulated by the parent cab vehicle part class. If the parent cab in question does not contain a pneumatic brake controller, this property will remain at {@code 0.0F}.</p>
	 */
	public final PropFloat controlBrakePneumatic;

	/**
	 * <p>The driver-controlled cruise control target speed of this cab. This value influences the MTMS cruise control speed. This value is not bounded - {@code <0.0F} indicates that cruise control should be disabled, {@code 0.0F}
	 * indicates that the train should stop using the cruise control system, and any other value determines the cruise control target speed with the {@code float} representing {@code km/h}.</p>
	 * <p>This variable is manipulated by the parent cab vehicle part class. If the parent cab in question does not contain a driver-controllable cruise control lever/system, this property will remain at {@code -1.0F}.</p>
	 */
	public final PropFloat controlCruiseControl;

	/**
	 * <p>The power/throttle control value of this cab. Should range from {@code 0.0F} to {@code 1.0F} (<b>off</b> and <b>maximum throttle</b> respectively).</p>
	 * <p>This variable is manipulated by the parent cab vehicle part class. If the parent cab in question does not contain a throttle/power controller, this property will remain at {@code 0.0F}.</p>
	 */
	public final PropFloat controlPower;


	/*
	 * Custom property classes.
	 */
	/**
	 * Alerter timer of this cab.
	 */
	public final PropAlerter alerter;
	/**
	 * Destination declared by this cab.
	 */
	public final PropDestination destination;


	private final TrainPulse pulse;
	/**
	 * Currently hovered button, as determined in {@link #onUpdateHovering()}. May be {@code null}.<br>
	 * <i>Note: this property is not - and must not be - registered via
	 * {@link #addProperty(IPartProperty) addProperty()}.</i>
	 */
	@SideOnly(Side.CLIENT) public PropButton hoveredButton;
	/**
	 * {@code true} while the action button (usually left mouse button) is being pressed on the {@link #hoveredButton}.
	 */
	@SideOnly(Side.CLIENT) public boolean isHoverPressed;

	/*
	 * Pneumatics
	 */
	/**
	 * The vessel which the cab controls to manipulate the brake pipe of the train during pneumatic braking.
	 */
	public final PropVessel brakeValve;
	/**
	 * A small volume of air which controls the brake pipe pressure.
	 */
	public final PropVessel equalisingReservoir;
	/**
	 * The current position of the brake pipe relay valve. Negative values vent the brake pipe and positive values charge the brake pipe.
	 */
	public final PropFloat.PropFloatBounded relayValve;


	/*
	 * Standard property classes.
	 */
	public final PropString uuid;
	/**
	 * {@code true} if this cab is allowed to generate its own compressed air to bypass the requirement for a compressor in the train.
	 */
	private final PropBoolean arcadeCompressor;
	/**
	 * {@code true} while this cab enforces a train halt.
	 */
	private final PropBoolean forceStop;
	/**
	 * This cab's current {@link zoranodensha.api.vehicles.part.type.PartTypeLamp.ELightMode light mode}.
	 */
	public final PropEnum<ELightMode> lightMode;

	/*
	 * Button properties.
	 */
	public final PropButton button_afb_decrease;
	public final PropButton button_afb_increase;
	public final PropButtonLock button_afb_toggle;
	public final PropButton button_alerter;
	public final PropButton button_brake_decrease;
	public final PropButton button_brake_increase;
	public final PropButton button_brakeDynamic_decrease;
	public final PropButton button_brakeDynamic_increase;
	public final PropButton button_brakeIndependent_decrease;
	public final PropButton button_brakeIndependent_increase;
	public final PropButtonLock button_brakeMode;
	public final PropButton button_decouple;
	public final PropButtonLock button_doors_left;
	public final PropButtonLock button_doors_right;
	public final PropButtonLock button_emergency_brake;
	public final PropButton button_horn;
	public final PropButtonLock button_lights_beams;
	public final PropButtonLock button_lights_cab;
	public final PropButtonLock button_lights_train;
	public final PropButton button_motor_disable;
	public final PropButton button_motor_enable;
	public final PropButtonLock button_multi_purpose_1;
	public final PropButtonLock button_multi_purpose_2;
	public final PropButtonLock button_multi_purpose_3;
	public final PropButtonLock button_multi_purpose_4;
	public final PropButtonLock button_pantograph_back;
	public final PropButtonLock button_pantograph_front;
	public final PropButton button_parkBrake_apply;
	public final PropButton button_parkBrake_release;
	public final PropButton button_reverser_backward;
	public final PropButton button_reverser_forward;
	public final PropButton button_throttle_decrease;
	public final PropButton button_throttle_increase;

	/**
	 * {@link java.util.HashMap HashMap} holding all mappings between {@link ECabKey keys} and {@link zoranodensha.api.vehicles.part.type.cab.PropButton buttons}.
	 */
	protected final HashMap<ECabKey, PropButton> buttonMap;

	/*
	 * Miscellaneous Values
	 */
	/**
	 * A counter that makes sure the cab does not perform any intensive code too often.
	 */
	private int compressorUpdateCounter = 60;
	private int mtcsLandscapeCounter = 40;
	private UUID pulseID;

	public PartTypeCab(VehParBase parent)
	{
		this(parent, PartTypeCab.class.getSimpleName());
	}

	public PartTypeCab(VehParBase parent, String name)
	{
		super(parent, name);


		mtms = new MTMS(this);


		this.addPulse(pulse = new TrainPulse(this));



		/* Initialise button map. */
		buttonMap = new HashMap<ECabKey, PropButton>();

		/* Create UUID */
		addProperty(uuid = (PropString)new PropString(parent, "cabUUID")
		{
			@Override
			protected boolean readFromNBT(NBTTagCompound nbt, String nbtKey)
			{
				if (nbt.hasKey(nbtKey))
				{
					set(nbt.getString(nbtKey));
				}
				else
				{
					set(UUID.randomUUID().toString());
				}
				return true;
			}
		}.setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));

		/*
		 * Register all cab functionality properties.
		 */
		addProperty(alerter = new PropAlerter(this, "alerter"));
		addProperty(destination = new PropDestination(this, "destination"));
		addProperty(forceStop = (PropBoolean)new PropBoolean(parent, "doForceStop").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(lightMode = (PropEnum<ELightMode>)new PropEnum<ELightMode>(parent, ELightMode.OFF, "lightMode").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));

		/*
		 * Register control properties.
		 */
		addProperty(reverser = new PropReverser(this, "reverser"));
		addProperty(controlBrakeDynamic = (PropFloat)new PropFloat.PropFloatBounded(parent, 0.0F, 0.0F, 1.0F, "controlBrakeDynamic").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(controlBrakeElectroPneumatic = (PropFloat)new PropFloat.PropFloatBounded(parent, 0.0F, 0.0F, 1.0F, "controlBrakeElectroPneumatic").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(controlBrakeEmergency = (PropBoolean)new PropBoolean(parent, false, "controlBrakeEmergency").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(controlBrakeIndependent = (PropFloat)new PropFloat.PropFloatBounded(parent, 0.0F, 0.0F, 1.0F, "controlBrakeIndependent").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(controlBrakePneumatic = (PropFloat)new PropFloat.PropFloatBounded(parent, 0.0F, 0.0F, 1.0F, "controlBrakePneumatic").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(controlCruiseControl = (PropFloat)new PropFloat(parent, -1.0F, "controlCruiseControl").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(controlPower = (PropFloat)new PropFloat.PropFloatBounded(parent, 0.0F, 0.0F, 1.0F, "controlPower").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));

		/*
		 * Pneumatics
		 */
		addProperty(arcadeCompressor = new PropBoolean(parent, "arcadeCompressor"));
		addProperty(brakeValve = (PropVessel)new PropVessel(parent, 20.0F, 1000.0F, 0.0F, "brakeValve").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(equalisingReservoir = (PropVessel)new PropVessel(parent, 5.0F, 1000.0F, 0.0F, "equalisingReservoir").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));
		addProperty(relayValve = (PropFloat.PropFloatBounded)new PropFloat.PropFloatBounded(parent, 0.0F, -1.0F, 1.0F, "relayValve").setValidTagCalls(EValidTagCalls.PACKET_SAVE).setSyncDir(ESyncDir.CLIENT));

		/*
		 * Initialise buttons.
		 */
		addProperty(button_alerter = new PropButton(this, ECabKey.ALERTER)
		{
			@Override
			protected void onPressed()
			{
				/* Reset alerter while respective button is pressed. */
				alerter.reset();
			}
		});

		/*
		 * Main Control Levers
		 */
		addProperty(button_afb_decrease = new PropButton(this, ECabKey.AFB_DECREASE));
		addProperty(button_afb_increase = new PropButton(this, ECabKey.AFB_INCREASE));
		addProperty(button_afb_toggle = new PropButtonLock(this, ECabKey.AFB_TOGGLE));
		addProperty(button_brake_decrease = new PropButton(this, ECabKey.BRAKE_DECREASE));
		addProperty(button_brakeDynamic_decrease = new PropButton(this, ECabKey.BRAKE_DYNAMIC_DECREASE));
		addProperty(button_brakeDynamic_increase = new PropButton(this, ECabKey.BRAKE_DYNAMIC_INCREASE));
		addProperty(button_brakeIndependent_decrease = new PropButton(this, ECabKey.BRAKE_INDEPENDENT_DECREASE));
		addProperty(button_brakeIndependent_increase = new PropButton(this, ECabKey.BRAKE_INDEPENDENT_INCREASE));
		addProperty(button_brake_increase = new PropButton(this, ECabKey.BRAKE_INCREASE));
		addProperty(button_throttle_decrease = new PropButton(this, ECabKey.THROTTLE_DECREASE));
		addProperty(button_throttle_increase = new PropButton(this, ECabKey.THROTTLE_INCREASE));

		addProperty(button_brakeMode = new PropButtonLock(this, ECabKey.BRAKE_MODE));

		addProperty(button_decouple = new PropButton(this, ECabKey.DECOUPLE)
		{
			@Override
			protected void onPressed()
			{
				VehicleSection section = getSection();
				if (section != null && getTrain().getLastSpeed() == 0.0)
				{
					/* Decouple the closest coupling upon button press. */
					OrderedTypesList<APartTypeCouplingAuto> couplingsAuto = section.parts.filter(OrderedTypesList.ASELECTOR_COUPLING_AUTO);
					if (!couplingsAuto.isEmpty())
					{
						couplingsAuto.getClosestPartType(getLocalX()).onDecouple();
					}
				}
			}
		});

		addProperty(button_doors_left = new PropButtonLock(this, ECabKey.DOORS_LEFT));
		addProperty(button_doors_right = new PropButtonLock(this, ECabKey.DOORS_RIGHT));
		addProperty(button_emergency_brake = new PropButtonLock(this, ECabKey.EMERGENCY_BRAKE)
		{
			@Override
			protected void onPressed()
			{
				/* Enable emergency state on server side when pressed. */
				setEmergency();
			}
		});

		addProperty(button_horn = new PropButton(this, ECabKey.HORN)
		{
			@Override
			protected void onPressed()
			{
				VehicleSection section = getSection();
				if (section != null)
				{
					/* Select closest horn part - if existent - and play horn. */
					OrderedTypesList<PartTypeHorn> horns = section.parts.filter(OrderedTypesList.SELECTOR_HORN);
					if (!horns.isEmpty())
					{
						horns.getClosestPartType(getLocalX()).soundHorn();
					}
				}
			}
		});

		addProperty(button_lights_beams = new PropButtonLock(this, ECabKey.LIGHTS_BEAMS)
		{
			@Override
			protected void onPressed()
			{
				/* Update all lamps which point in the same direction as this cab. */
				onUpdateLamps();
			}

			@Override
			protected void onReleased()
			{
				/* Update all lamps which point in the same direction as this cab. */
				onUpdateLamps();
			}
		});

		addProperty(button_lights_cab = new PropButtonLock(this, ECabKey.LIGHTS_CAB));
		// addProperty(button_lights_headlights = new PropButton(this, ECabKey.LIGHTS_HEADLIGHTS)
		// {
		// @Override
		// public String getTooltip()
		// {
		// /* Append the light mode to the head light button label. */
		// String mode = StatCollector.translateToLocal(String.format("%s.tooltip.lightmode.%s", PartTypeCab.class.getName().toLowerCase(), lightMode.get().toString().toLowerCase()));
		// return String.format("%s (%s)", super.getTooltip(), mode);
		// }
		//
		// @Override
		// protected void onPressed()
		// {
		// /* Switch to next light mode when pressed. */
		// ELightMode[] lightModes = ELightMode.values();
		// int nextMode = (lightMode.get().ordinal() + 1) % lightModes.length;
		// lightMode.set(lightModes[nextMode]);
		//
		// /* Update all lamps which point in the same direction as this cab. */
		// onUpdateLamps();
		// }
		// });

		/*
		 * Motors
		 */
		addProperty(button_motor_disable = new PropButton(this, ECabKey.MOTOR_DISABLE)
		{
			@Override
			protected void onPressed()
			{
				if (getParent().getTrain() != null)
				{
					/*
					 * Get all the engines/motors in the train.
					 */
					ConcatenatedTypesList<APartTypeEngine> engines = getParent().getTrain().getVehiclePartTypes(OrderedTypesList.ASELECTOR_ENGINE);

					/*
					 * Go through all the engines/motors and switch them off.
					 */
					for (APartTypeEngine engine : engines)
					{
						engine.enabled.set(false);
					}
				}
			}
		});
		addProperty(button_motor_enable = new PropButton(this, ECabKey.MOTOR_ENABLE)
		{
			@Override
			protected void onPressed()
			{
				if (getParent().getTrain() != null)
				{
					/*
					 * Get all the engines/motors in the train.
					 */
					ConcatenatedTypesList<APartTypeEngine> engines = getParent().getTrain().getVehiclePartTypes(OrderedTypesList.ASELECTOR_ENGINE);

					/*
					 * Go through all the engines/motors and switch them on.
					 */
					for (APartTypeEngine engine : engines)
					{
						engine.enabled.set(true);
					}
				}
			}
		});

		addProperty(button_lights_train = new PropButtonLock(this, ECabKey.LIGHTS_TRAIN));
		addProperty(button_multi_purpose_1 = new PropButtonLock(this, ECabKey.MULTI_PURPOSE_1));
		addProperty(button_multi_purpose_2 = new PropButtonLock(this, ECabKey.MULTI_PURPOSE_2));
		addProperty(button_multi_purpose_3 = new PropButtonLock(this, ECabKey.MULTI_PURPOSE_3));
		addProperty(button_multi_purpose_4 = new PropButtonLock(this, ECabKey.MULTI_PURPOSE_4));
		addProperty(button_pantograph_back = new PropButtonLock(this, ECabKey.PANTOGRAPH_BACK));
		addProperty(button_pantograph_front = new PropButtonLock(this, ECabKey.PANTOGRAPH_FRONT));
		addProperty(button_reverser_backward = new PropButton(this, ECabKey.REVERSER_BACKWARD)
		{
			@Override
			protected void onPressed()
			{
				reverser.set(reverser.get().prev());
			}
		});
		addProperty(button_parkBrake_apply = new PropButton(this, ECabKey.PARK_BRAKE_APPLY)
		{
			@Override
			protected void onPressed()
			{
				if (getParent().getTrain() != null)
				{
					/*
					 * Get all the bogies in the train.
					 */
					ConcatenatedTypesList<PartTypeBogie> allBogies = getParent().getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);

					/*
					 * Trip the park brake release breaker on each bogie.
					 */
					for (PartTypeBogie bogie : allBogies)
					{
						bogie.breakerParkBrakeRelease.trip();
					}
				}
			}
		});
		addProperty(button_parkBrake_release = new PropButton(this, ECabKey.PARK_BRAKE_RELEASE)
		{
			@Override
			protected void onPressed()
			{
				if (getParent().getTrain() != null)
				{
					/*
					 * Get all the bogies in the train.
					 */
					ConcatenatedTypesList<PartTypeBogie> allBogies = getParent().getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);

					/*
					 * Set the park brake release breaker on each bogie.
					 */
					for (PartTypeBogie bogie : allBogies)
					{
						bogie.breakerParkBrakeRelease.set();
					}
				}
			}
		});

		addProperty(button_reverser_forward = new PropButton(this, ECabKey.REVERSER_FORWARD)
		{
			@Override
			protected void onPressed()
			{
				reverser.set(reverser.get().next());
			}
		});
	}


	@Override
	protected void addProperty(IPartProperty<?> property)
	{
		super.addProperty(property);

		/* In case a button was added, re-register all buttons into button map. */
		if (property instanceof PropButton)
		{
			buttonMap.clear();

			for (IPartProperty<?> prop : getProperties())
			{
				if (prop instanceof PropButton)
				{
					PropButton button = (PropButton)prop;
					if (button.cabKey != null)
					{
						buttonMap.put(button.cabKey, button);
					}
				}
			}
		}
	}


	/**
	 * Return a new String array holding the destination data.
	 *
	 * @return Destination data of this cab in a new array. May be {@code null}.
	 */
	public String[] getDestination()
	{
		return destination.getDestination();
	}


	/**
	 * Getter to determine whether this cab is enforcing a train halt.
	 */
	public boolean getDoForceStop()
	{
		return forceStop.get();
	}


	/**
	 * Returns whether this is the parent train's {@link zoranodensha.api.vehicles.Train#getActiveCab() active cab}.
	 *
	 * @return {@code true} if this is an active cab.
	 */
	public boolean getIsActive()
	{
		return (getTrain() != null) && (getTrain().getActiveCab() == this);
	}


	/**
	 * Returns whether this cab allows another cab to become the new active cab in a train as defined in {@link zoranodensha.api.vehicles.Train#updateActiveCab() updateActiveCab()}.<br>
	 * <br>
	 * Use this method to check for preconditions before switching active cabs, e.g. to check if a player has turned off the engines or if the driver's key was pulled.
	 *
	 * @return {@code true} if another cab may override this cab as active cab.
	 */
	public boolean getMayOverrideActive()
	{
		if (!mtms.autodriveEnable.get())
		{
			return (getPassenger() == null);
		}

		if (mtms.autodriveState.get() == EAutodriveState.TERMINATING)
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	/**
	 * Determines whether the {@link #forceStop} flag may be reset and thus whether an applied emergency brake may be released.
	 *
	 * @return {@code true} if the emergency brake may be released.
	 */
	public boolean getMayReleaseForceStop()
	{
		float throttle = 0.0F;
		double trainSpeed = getTrain().getLastSpeed();

		throttle = controlPower.get();

		return (trainSpeed == 0.0D) && (throttle == 0.0F);
	}

	/**
	 * Return the current reverser state.
	 *
	 * @return The current {@link #reverser} state
	 */
	public EReverser getReverserState()
	{
		return reverser.get();
	}


	/**
	 * Called whenever the given key's state changes.<br>
	 * <br>
	 * <i>Client-side only.</i>
	 *
	 * @param cabKey - {@link ECabKey Key} whose state changed.
	 * @param isKeyDown - {@code true} if the key's respective key binding is pressed.
	 */
	@SideOnly(Side.CLIENT)
	public void onClientKeyChange(ECabKey cabKey, boolean isKeyDown)
	{
		buttonMap.get(cabKey).onClientKeyChange(isKeyDown);
	}

	@Override
	public void onTick()
	{
		super.onTick();

		/* Abort if the train isn't added to its chunk yet. */
		Train train = getTrain();
		if (!train.addedToChunk)
		{
			return;
		}


		/*
		 * Reset controls, so they can be modified later by the Vehicle Part
		 */
		{
			controlBrakeDynamic.set(0.0F);
			controlBrakeElectroPneumatic.set(0.0F);
			controlBrakeEmergency.set(false);
			controlBrakePneumatic.set(0.0F);
			controlCruiseControl.set(-1.0F);
			controlPower.set(0.0F);
		}

		// Tick the parent vehicle part instance (if it supports it).
		if (getParent() instanceof IVehiclePartCab_Tickable)
		{
			((IVehiclePartCab_Tickable)getParent()).tick();
		}

		if (train.worldObj.isRemote)
		{
			onTickClientSide(train);
		}
		else
		{
			onTickServerSide(train);
		}

		mtms.onUpdate();

		controlPower.set(Math.min(controlPower.get(), mtms.getMaximumThrottle()));
		controlBrakeElectroPneumatic.set(Math.max(controlBrakeElectroPneumatic.get(), mtms.getMinimumBrake()));
		controlBrakeDynamic.set(Math.max(controlBrakeDynamic.get(), mtms.getMinimumBrake()));

		// Ensure power cannot be applied whilst the emergency brakes are enabled.
		if (getDoForceStop() && controlPower.get() > 0.0F)
		{
			controlPower.set(0.0F);
		}
	}

	/**
	 * Update tick that is only called on the client side.
	 */
	protected void onTickClientSide(Train train)
	{
		/*
		 * Update mouse hover.
		 */
		onUpdateHovering();

		/*
		 * Automatically change the marker/head lights.
		 */
		{
			boolean shouldBeWhite = false;
			boolean shouldBeEnabled = false;

			/*
			 * Determine if the lights are in a position in the train where they should be enabled (at the ends of the train only, NOT in the middle)
			 */
			int vehicleIndex = getTrain().getVehicleIndex(this.getVehicle());
			if ((vehicleIndex == 0 && getLocalX() > getSection().centerX) || (vehicleIndex + 1 >= getTrain().getVehicleCount() && getLocalX() < getSection().centerX))
			{
				shouldBeEnabled = true;
			}

			if (getIsActive())
			{
				switch (getReverserState())
				{
					case FORWARD:
					case NEUTRAL_FORWARD:
					{
						shouldBeWhite = true;
						break;
					}

					default:
					{
						break;
					}
				}
			}

			if (!shouldBeEnabled)
			{
				if (!lightMode.get().equals(ELightMode.OFF))
				{
					lightMode.set(ELightMode.OFF);
					onUpdateLamps();
				}
			}
			else
			{
				if (shouldBeWhite)
				{
					if (!lightMode.get().equals(ELightMode.FRONT))
					{
						lightMode.set(ELightMode.FRONT);
						onUpdateLamps();
					}
				}
				else
				{
					if (!lightMode.get().equals(ELightMode.REAR))
					{
						lightMode.set(ELightMode.REAR);
						onUpdateLamps();
					}
				}
			}
			if (Minecraft.getMinecraft().gameSettings.showDebugInfo) {

				if (getIsActive() && this.reverser.get().equals(EReverser.FORWARD) && !mtms.isolateMTCS.get()) {

					// Update the TrainPulse
					{
						ConcatenatedTypesList<PartTypeBogie> bogies = getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
						boolean cabInverse = VehicleHelper.getAngle(null, PartHelper.getViewVec(getParent(), true)) > 90.0;
						PartTypeBogie reference = cabInverse ? bogies.get(bogies.size() - 1) : bogies.get(0);

						for (APulse pulse : pulses.values()) {
							pulse.tickPulse(new MovementVector(reference.prevPos.get(), reference.dir.get()), 0);
						}
					}

				}
			}
		}
	}


	/**
     * Update tick that is called only on the server-side.
     */
	protected void onTickServerSide(Train train)
	{
		/*
		 * Run alerter and force-stop updates.
		 */
		/* If the train is moving and either this is the train's active cab or the alerter has been running, update alerter. */
		if ((getIsActive() || alerter.getIsRunning()) && train.getLastSpeed() != 0.0)
		{
			alerter.update();
		}
		else
		{
			/* Otherwise, reset alerter timer. */
			alerter.reset();

			/* Additionally, if the train stands still and throttle and reverser are in neutral, disable force stop. */
			if (getMayReleaseForceStop())
			{
				forceStop.set(false);
			}
		}


		/*
		 * Run Arcade Compressor Check
		 */
		if (compressorUpdateCounter <= 0)
		{
			ConcatenatedTypesList<PartTypeCompressor> trainCompressors = parent.getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_COMPRESSOR);

			arcadeCompressor.set(trainCompressors.size() <= 0);
			compressorUpdateCounter = 60;
		}
		else
		{
			compressorUpdateCounter--;
		}





		/*
		 * Update the MTMS Landscape information (what the system 'sees' out the front of the train).
		 */
		if (getIsActive() && this.reverser.get().equals(EReverser.FORWARD) && !mtms.isolateMTCS.get())
		{



			mtcsLandscapeCounter--;

			if (mtcsLandscapeCounter <= 0)
			{

				/*
				 * Attempt to get the front bogie of the train.
				 */
				ConcatenatedTypesList<PartTypeBogie> bogies = getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
				boolean cabInverse = VehicleHelper.getAngle(null, PartHelper.getViewVec(getParent(), true)) > 90.0;
				PartTypeBogie reference = cabInverse ? bogies.get(bogies.size() - 1) : bogies.get(0);


				long startTime = System.nanoTime();

				/*
				 * Get the landscape.
				 */
				BalisePulse pulse = new BalisePulse(reference.prevPos.get(), reference.dir.get());
				Landscape landscape = pulse.getLandscape(getTrain().worldObj, mtms.trainLength.get(), mtms.trainSpeed.get(), mtms.autodriveEnable.get());

/*
				// Stop measuring execution time
				long endTime = System.nanoTime();

				// Calculate the execution time in milliseconds
				long pathTime = (endTime - startTime);

				System.out.println("Pathfinding took " + pathTime + " nanoseconds.");*/

				mtms.lastLandscape.set(landscape);
				mtms.landscapeOffset.set(0.0F);
				// Update the TrainPulse




					for (APulse trainPulse : pulses.values()) {
						trainPulse.tickPulse(new MovementVector(reference.prevPos.get(), reference.dir.get()), 0);
					}




				mtcsLandscapeCounter = 20;
			}
		}
		else
		{
			/* Safety systems are disabled. */

			/* Remove any information referring to the outside world so MTMS can't use it. */
			mtms.lastLandscape.set(null);
			mtms.landscapeOffset.set(0.0F);

			mtcsLandscapeCounter = 20;

			/* If there is an alerter, constantly reset it so it stays disabled. */
			if (alerter != null)
			{
				alerter.reset();
			}
		}
	}

	/**
     * Separate method to update the train's brakes.<br>
     * <br>
     * Usually, this gets called during train updates if this cab is the parent train's active cab. However, if the alerter has run out this method may be called even when this cab isn't the currently active cab.
     */
	public void onUpdateBrakes()
	{
		/* Safety check to ensure the parent train exists. */
		Train train = getTrain();
		if (train == null)
		{
			return;
		}


		/*
		 * Pneumatics
		 */
		if (!train.worldObj.isRemote)
		{
			ConcatenatedTypesList<PartTypeBogie> trainBogies = train.getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
			{
				PartTypeBogie thisBogie = trainBogies.getClosestPartType(this.getLocalX());
				PneumaticsHandler pneumaticsHandler = PneumaticsHandler.INSTANCE;


				/*
				 * Sound Effect Values
				 */
				{
					brakeValve.valueModification = 0.0F;
					brakeValve.valueBefore = brakeValve.getPressure();
					equalisingReservoir.valueModification = 0.0F;

					brakeValve.setHasChanged(true);
					equalisingReservoir.setHasChanged(true);
				}

				/*
				 * Arcade Compressor
				 */
				if (arcadeCompressor.get())
				{
					if (thisBogie.pneumaticsMainReservoirPipe.getPressure() < 850.0F)
					{
						thisBogie.pneumaticsMainReservoirPipe.setPressure(850.0F);
					}
				}


				/*
				 * Brake Stand Logic
				 */
				float brakeLever = Math.abs(controlBrakePneumatic.get());
				float pilotValveTargetPressure = 500.0F - (brakeLever * 170.0F) - (brakeLever != 0.0F ? 10.0F : 0.0F);
				float pilotValveOffset = equalisingReservoir.getPressure() - pilotValveTargetPressure;

				/*
				 * Pilot Valve Charging
				 */
				if (pilotValveOffset < -2.0F)
				{
					pneumaticsHandler.equalise(thisBogie.pneumaticsMainReservoirPipe, equalisingReservoir, 30.0F);
				}

				/*
				 * Pilot Valve Venting
				 */
				//				if (button_brakeMode.get() || getReverserState() == EReverser.LOCKED) TODO @Jaffa - Review
				{
					if (pilotValveOffset > 2.0F)
					{
						pneumaticsHandler.vent(equalisingReservoir, 40.0F);
					}
				}

				/*
				 * Emergency Valve
				 */
				final boolean emergencyValveOpen = (controlBrakeEmergency.get() || forceStop.get() || getReverserState().equals(EReverser.LOCKED));
				if (emergencyValveOpen)
				{
					pneumaticsHandler.vent(brakeValve, 2000.0F);
				}
				/*
				 * Relay Valve
				 */
				{
					float relayValveTarget = (equalisingReservoir.getPressure() - brakeValve.getPressure()) * 0.05F;
					relayValve.set(relayValve.get() + ((relayValveTarget - relayValve.get()) * 0.1F));

					if (relayValve.get() > 0.0F && !emergencyValveOpen)
					{
						pneumaticsHandler.equalise(thisBogie.pneumaticsMainReservoirPipe, brakeValve, Math.abs(relayValve.get()) * 175.0F);
					}
					else if (relayValve.get() < 0.0F)
					{
						pneumaticsHandler.vent(brakeValve, Math.abs(relayValve.get()) * 175.0F);
					}
				}

				/*
				 * The final step is to connect the cab's brake valve instance to the train's brake pipe.
				 */
				{
					for (int iteration = 0; iteration < 15; iteration++)
					{
						pneumaticsHandler.equalise(brakeValve, thisBogie.pneumaticsBrakePipe, 300.0F);
					}
				}

				/*
				 * Sound effect values
				 */
				{
					brakeValve.valueAfter = brakeValve.getPressure();
				}
			}
		}
	}


	/**
     * Updates cab interaction with the mouse, by hovering over respective controls.<br>
     * <br>
     * <i>Client-side only.</i>
     */
	@SideOnly(Side.CLIENT)
	protected void onUpdateHovering()
	{
		/*
		 * If the local player isn't passenger of this cab, abort.
		 */
		if (getPassenger() != Minecraft.getMinecraft().thePlayer)
		{
			return;
		}

		/* Cache and update player position. */
		EntityLivingBase viewEntity = Minecraft.getMinecraft().renderViewEntity;
		double posX = viewEntity.posX;
		double posY = viewEntity.posY;
		double posZ = viewEntity.posZ;
		getSeat().updateRiderPosition();

		/*
		 * Try to determine a selected button from all available properties.
		 */
		PropButton propButtonSelected = null;
		{
			/* Get the local client's view entity. */
			float[] scale = getParent().getScale();

			/*
			 * Retrieve view origin (origVector), view vector (lookVector) and get part
			 * orientation vector (partVector).
			 */
			Vec3 origVector = PartHelper.getPosition(getParent()).subtract(viewEntity.getPosition(1.0F));
			Vec3 partVector = PartHelper.getViewVec(getParent(), true);
			Vec3 lookVector = viewEntity.getLook(1.0F).normalize();

			/*
			 * Lower the look vector and add the part's local view vector onto the player's
			 * view vector to get the correct view.
			 * Rotation around the Z axis is done here to account for the tilt of the cab when going up/down slopes,
			 * so the tooltips still line up.
			 */
			lookVector.yCoord += (getMountedYOffset() * 0.36 + 0.1625) * scale[1];
			lookVector.rotateAroundZ(getParent().getSection().getRotationPitch() * VehicleHelper.RAD_FACTOR);
			lookVector.rotateAroundY((float)(-1.0F * VehicleHelper.getAngle(null, partVector) * VehicleHelper.RAD_FACTOR));

			/*
			 * Iterate through all properties and compare each valid button to the currently
			 * selected button.
			 */
			double minDist = Double.MAX_VALUE;
			for (IPartProperty<?> iPartProperty : getParent().getProperties())
			{
				if (iPartProperty instanceof PropButton)
				{
					/*
					 * Cast the button. Check whether it currently accepts input and if it specifies
					 * a hitbox.
					 */
					PropButton propButton = (PropButton)iPartProperty;
					if (!propButton.isInputAllowed() || propButton.getHitbox() == null)
					{
						continue;
					}

					/* Retrieve the button's hitbox and move according to part scale. */
					AxisAlignedBB hitBox = propButton.getHitbox().copy();
					{
						// @formatter:off
						Vec3 pos = VehicleHelper.getPosition(hitBox);
						hitBox.offset((pos.xCoord * scale[0]) - pos.xCoord, (pos.yCoord * scale[1]) - pos.yCoord, (pos.zCoord * scale[2]) - pos.zCoord);
						// @formatter:on
					}

					/* Ray trace the button hitbox. */
					MovingObjectPosition movObjPos = hitBox.calculateIntercept(origVector, lookVector);
					if (movObjPos != null)
					{
						double dist = origVector.distanceTo(movObjPos.hitVec);
						if (dist < minDist)
						{
							minDist = dist;
							propButtonSelected = propButton;
						}
					}
				}
			}
		}

		/* Reset player position. */
		viewEntity.posX = posX;
		viewEntity.posY = posY;
		viewEntity.posZ = posZ;

		/*
		 * Update the hovered button.
		 */
		if (hoveredButton != null)
		{
			/*
			 * If there is a button press ongoing and either the mouse was released or the
			 * hovered button changes, notify button of key unpress.
			 */
			if (isHoverPressed && (!Minecraft.getMinecraft().gameSettings.keyBindAttack.getIsKeyPressed() || hoveredButton != propButtonSelected))
			{
				/* Clear hover flag. */
				isHoverPressed = false;

				/* Notify of key change. */
				if (hoveredButton.cabKey != null)
				{
					onClientKeyChange(hoveredButton.cabKey, isHoverPressed);
				}
				else
				{
					hoveredButton.onClientKeyChange(isHoverPressed);
				}
			}
		}

		/* Assign new hovered button. */
		hoveredButton = propButtonSelected;

		/*
		 * If a button was found and there is no key press ongoing, check whether the
		 * player is pressing the button.
		 */
		if (hoveredButton != null && !isHoverPressed)
		{
			/* If the action key is pressed, notify button. */
			if (Minecraft.getMinecraft().gameSettings.keyBindAttack.getIsKeyPressed())
			{
				/* Set hover flag. */
				isHoverPressed = true;

				/* Notify of key change. */
				if (hoveredButton.cabKey != null)
				{
					onClientKeyChange(hoveredButton.cabKey, isHoverPressed);
				}
				else
				{
					hoveredButton.onClientKeyChange(isHoverPressed);
				}
			}
		}
	}


	/**
	 * Helper method to update all {@link zoranodensha.api.vehicles.part.type.PartTypeLamp lamps} in this {{@link #getSection() section}, if applicable.
	 */
	protected void onUpdateLamps()
	{
		/* Ensure the section exists. */
		VehicleSection section = getSection();
		if (section == null)
		{
			return;
		}

		/* Retrieve all lamps and check whether there exists any. */
		OrderedTypesList<PartTypeLamp> lamps = section.parts.filter(OrderedTypesList.SELECTOR_LAMP);
		if (lamps.isEmpty())
		{
			return;
		}

		/* Check whether their rotation matches ours and, if so, apply new light data. */
		final float thisLampYaw = getParent().getRotation()[1];
		for (PartTypeLamp lamp : lamps)
		{
			if (Math.abs(lamp.getParent().getRotation()[1] - thisLampYaw) <= 90.0F)
			{
				lamp.setFromLightMode(lightMode.get(), button_lights_beams.isActive());
			}
		}
	}

	/**
	 * Called during update routines by the parent {@link zoranodensha.api.vehicles.Train train} to load this cab's data into the train.<br>
	 * <br>
	 * Override state data of part types in the entire train here.
	 */
	public void onUpdateTrain()
	{
		/*
		 * Update brake data.
		 */
		onUpdateBrakes();

		/*
		 * Update any other data.
		 */

		/* Prepare some data beforehand. */
		Train train = getTrain();
		final EDoorState doorState = EDoorState.fromFlags(button_doors_left.get(), button_doors_right.get());
		ConcatenatedTypesList<PartTypeDoor> doors = train.getVehiclePartTypes(OrderedTypesList.SELECTOR_DOOR);

		/*
		 * TODO @Jaffa - The next 3 blocks of code could be combined or simplified drastically.
		 */

		/* Doors - Manual Control */
		if (!doors.isEmpty())
		{
			for (PartTypeDoor door : doors)
			{
				door.setDoorState(doorState);
			}
		}

		/* Doors - Autodrive System */
		if (mtms.autodriveState.get() == EAutodriveState.STATION_ARRIVAL && getTrain().getLastSpeed() == 0.0F)
		{
			if (!doors.isEmpty())
			{
				for (PartTypeDoor door : doors)
				{
					door.setDoorState(EDoorState.UNLOCK_ALL);
				}
			}
		}

		/* Doors - Automatic Opening Option */
		if (mtms.automaticDoorOpen.get())
		{
			if (!doors.isEmpty())
			{
				for (PartTypeDoor door : doors)
				{
					switch (door.getDoorState())
					{
						case UNLOCK_ALL:
						case UNLOCK_LEFT:
						case UNLOCK_RIGHT:
						{
							door.setOpen(PartTypeDoor.EDoorSide.LEFT, true);
							door.setOpen(PartTypeDoor.EDoorSide.RIGHT, true);
							break;
						}
						default:
							break;
					}
				}
			}
		}

		/* Pantographs */
		for (VehicleData vehicle : train)
		{
			for (VehicleSection section : vehicle)
			{
				/* For each vehicle section containing pantographs.. */
				OrderedTypesList<PartTypePantograph> pantographs = section.parts.filter(OrderedTypesList.SELECTOR_PANTOGRAPH);
				if (!pantographs.isEmpty())
				{
					/* ..calculate the center of the section.. */
					AxisAlignedBB minBB = section.parts.get(0).getLocalBoundingBox();
					AxisAlignedBB maxBB = section.parts.get(section.parts.size() - 1).getLocalBoundingBox();
					float localCenterX = (float)((minBB.minX + maxBB.maxX) / 2);

					/* ..determine whether to inverse the direction, depending on where the cab is looking at.. */
					boolean isInverse = VehicleHelper.getAngle(null, PartHelper.getViewVec(getParent(), true)) > 90.0;

					/* ..and then toggle pantographs on either side. */
					final boolean backUp = button_pantograph_back.get();
					final boolean frontUp = button_pantograph_front.get();

					for (PartTypePantograph pantograph : pantographs)
					{
						if (pantograph.getParent().getOffset()[0] < localCenterX)
						{
							pantograph.setDoExtend(isInverse ? frontUp : backUp);
						}
						else
						{
							pantograph.setDoExtend(isInverse ? backUp : frontUp);
						}
					}
				}
			}
		}
	}

	/**
	 * Assign an array holding destination data to this cab.<br>
	 * <br>
	 * The destination info may be displayed by applicable parts in the train.
	 *
	 * @param dest - An array of destinations, or {@code null} if there is none set.
	 */
	public void setDestination(@Nullable String... dest)
	{
		destination.setDestination(dest);
	}

	/**
	 * Called to set the cab's controls into an emergency state.
	 */
	public void setEmergency()
	{
		/* Enforce a stop only if we aren't already standing still. */
		if (!getMayReleaseForceStop())
		{
			forceStop.set(true);
			onUpdateBrakes();
		}
	}

	@Override
	public boolean setPassenger(Entity entity)
	{
		/* Run permission checks before calling super class method. */
		if (getParent().getIsFinished() && entity instanceof EntityLivingBase)
		{
			EntityLivingBase entityLiving = (EntityLivingBase)entity;
			ItemStack heldItem = entityLiving.getHeldItem();

			if (heldItem != null && heldItem.getItem() instanceof IDriverKey)
			{
				if (((IDriverKey)heldItem.getItem()).getCanAccess(this, entityLiving))
				{
					return super.setPassenger(entity);
				}
			}
		}

		return false;
	}

	/**
	 * Called to check for driver alertness.<br>
	 * <br>
	 * This may be called especially by signals or magnets at dangerous places.
	 */
	public void setRequestAlertness()
	{
		/* Abort if we are on a remote world, the train isn't moving, or if this isn't the train's active cab. */
		Train train = getTrain();
		if (train != null && !train.worldObj.isRemote && train.getLastSpeed() != 0.0 && getIsActive())
		{
			alerter.requestAlertness();
		}
	}

	/**
	 * Called whenever the parent train tries to set this cab as active cab.<br> Check for cab-dependent preconditions here (e.g. presence of a driver or a key).
	 *
	 * @return {@code true} if this cab may become the new active cab.
	 * @see zoranodensha.api.vehicles.Train#getActiveCab() getActiveCab()
	 */
	public boolean trySetActive()
	{
		/*
		 * When the autodrive systme is disabled, simply check that there is a player in the cab.
		 */
		if (!(mtms.autodriveEnable.get() && mtms.autodriveState.get() != EAutodriveState.TERMINATING))
		{
			return (getPassenger() instanceof EntityPlayer);
		}

		/*
		 * Get a list of all the cabs in the train
		 */
		ConcatenatedTypesList<PartTypeCab> cabs = getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_CAB);

		/*
		 * Get the index of the current active cab (what the train is switching from) and this cab (what the train is trying to switch to) so we can compare them.
		 */
		PartTypeCab frontCab = cabs.get(0);
		PartTypeCab rearCab = cabs.get(cabs.size() - 1);

		boolean thisIsFrontCab = this.equals(frontCab);
		boolean thisIsRearCab = this.equals(rearCab);

		//		int activeCabIndex = cabs.indexOf(getTrain().getActiveCab());
		//		int thisCabIndex = cabs.indexOf(this);

		/*
		 * Allow the parent train to change to this cab only if we are at the end of the cabs list.
		 */
		return (thisIsRearCab || thisIsFrontCab);
	}

	@Override
	public Vec3 getEmitterPosition() {
		ConcatenatedTypesList<PartTypeBogie> bogies = getTrain().getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
		boolean cabInverse = VehicleHelper.getAngle(null, PartHelper.getViewVec(getParent(), true)) > 90.0;
		PartTypeBogie reference = cabInverse ? bogies.get(bogies.size() - 1) : bogies.get(0);

		return reference.prevServerPos;
	}

	@Override
	public World getWorld() {
		return getTrain().worldObj;
	}

	@Override
	public Train getEmitterEntity() {
		return getTrain();
	}

	/**
	 * Cab keys handled by the default cab part type.<br>
	 * <br>
	 * Cabs will be notified upon state changes of these keys.<br> Zora no Densha handles key bindings and presses internally in an attempt to maintain a global keyboard layout across all implemented cabs.
	 */
	public enum ECabKey
	{
		/**
		 * Decrease the AFB speed limit.
		 */
		AFB_DECREASE,
		/**
		 * Increase the AFB speed limit.
		 */
		AFB_INCREASE,
		/**
		 * Toggle the Cruise Control system.
		 */
		AFB_TOGGLE,
		/**
		 * Confirm driver alertness.
		 */
		ALERTER,
		/**
		 * Decrease brake controller.
		 */
		BRAKE_DECREASE,
		/**
		 * Decrease dynamic brake controller.
		 */
		BRAKE_DYNAMIC_DECREASE,
		/**
		 * Increase dynamic brake controller.
		 */
		BRAKE_DYNAMIC_INCREASE,
		/**
		 * Increase brake controller.
		 */
		BRAKE_INCREASE,
		/**
		 * Decrease independent brake controller.
		 */
		BRAKE_INDEPENDENT_DECREASE,
		/**
		 * Increase independent brake controller.
		 */
		BRAKE_INDEPENDENT_INCREASE,
		/**
		 * Switches between different braking modes, usually between Electro-Pneumatic and Pneumatic modes of operation.
		 */
		BRAKE_MODE,
		/**
		 * Deactivate automatic couplings. Mostly common in cabs of trains with auto-couplers.
		 */
		DECOUPLE,
		/**
		 * (Un-)lock doors on the left.
		 */
		DOORS_LEFT,
		/**
		 * (Un-)lock doors on the right.
		 */
		DOORS_RIGHT,
		/**
		 * Emergency brake.
		 */
		EMERGENCY_BRAKE,
		/**
		 * Sound the cab's horn.
		 */
		HORN,
		/**
		 * Change whether head lights shall, if active, render beams.
		 */
		LIGHTS_BEAMS,
		/**
		 * Toggle cab lighting.
		 */
		LIGHTS_CAB,
		/**
		 * Toggle train lighting.
		 */
		LIGHTS_TRAIN,
		/**
		 * Generic button to disable/shut down any motors on the train.
		 */
		MOTOR_DISABLE,
		/**
		 * Generic button to enable/start up any motors on the train.
		 */
		MOTOR_ENABLE,
		/**
		 * Multi-purpose key 1.
		 */
		MULTI_PURPOSE_1,
		/**
		 * Multi-purpose key 2.
		 */
		MULTI_PURPOSE_2,
		/**
		 * Multi-purpose key 3.
		 */
		MULTI_PURPOSE_3,
		/**
		 * Multi-purpose key 4.
		 */
		MULTI_PURPOSE_4,
		/**
		 * Raises back pantographs.
		 */
		PANTOGRAPH_BACK,
		/**
		 * Raises front pantographs.
		 */
		PANTOGRAPH_FRONT,
		/**
		 * Applies the parking brakes along the entire train.
		 */
		PARK_BRAKE_APPLY,
		/**
		 * Releases the parking brakes along the entire train.
		 */
		PARK_BRAKE_RELEASE,
		/**
		 * Move the reverser one level backwards.
		 */
		REVERSER_BACKWARD,
		/**
		 * Move the reverser one level forward.
		 */
		REVERSER_FORWARD,
		/**
		 * Decrease the throttle.
		 */
		THROTTLE_DECREASE,
		/**
		 * Increase the throttle.
		 */
		THROTTLE_INCREASE,
	}



	HashMap<UUID, APulse> pulses = new HashMap<>();
    ArrayList<PulseNotification> notifications = new ArrayList<>();


	public void notifyPulse(PulseNotification notification) {
		notifications.add(notification);
	}

	public APulse getPulse(UUID pulseID) {
		return pulses.get(pulseID);
	}

	public APulse getPulse() {
		return pulses.get(pulseID);
	}

	public void addPulse(APulse pulse) {
		pulses.put(pulse.getID(), pulse);
		pulseID = pulse.getID();
	}

	public HashMap<UUID, APulse> getPulses() {
		return pulses;
	}

	public void clear() {
		for (APulse pulse : pulses.values()) {
			pulse.clear();
		}
		pulses.clear();
	}

	public void removePulse(UUID id) {
		pulses.remove(id);
	}

	public ALandmark getNextLandmark() {
		ALandmark nextLandmark = null;
		for (APulse pulse : pulses.values()) {
			nextLandmark = pulse.landmark;
		}

		return nextLandmark;
	}
}