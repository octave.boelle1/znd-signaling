package zoranodensha.api.vehicles.part;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Vec3;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.util.APILogger;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.IXMLProperty;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropVec3f;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.APartType;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_MassListener;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_PositionListener;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.util.expr.Expression;
import zoranodensha.api.vehicles.util.VehicleHelper;



/**
 * <h1>Vehicle Part Base</h1>
 * <hr>
 *
 * <p>
 * Basic vehicle part containing various {@link zoranodensha.api.vehicles.part.type.APartType part types}.<br> Aforementioned part types define which functionality this vehicle part provides.
 * </p>
 *
 * @author Leshuwa Kaiheiwa
 */
public class VehParBase
{
	/** NBT key for vehicle parts. */
	public static final String NBT_KEY = "IVehiclePart_Instance";

	/*
	 *
	 * Final fields, assigned upon initialisation.
	 *
	 *
	 */

	/** List of vehicle {@link zoranodensha.api.vehicles.part.type.APartType part types}, each of them containing part functionality. Only once instance per part type is allowed. */
	private final HashSet<APartType> partTypes = new HashSet<APartType>();
	/** Property map, holding all registered {@link zoranodensha.api.vehicles.part.property.IPartProperty properties}. */
	private final HashMap<Integer, IPartProperty<?>> properties = new HashMap<Integer, IPartProperty<?>>();
	/** List of {@link zoranodensha.api.vehicles.part.property.IXMLProperty properties} defined independently by the part's XML file. Will be empty if this is not an XML-based part. */
	private final ArrayList<IXMLProperty<?>> xmlProperties = new ArrayList<IXMLProperty<?>>();
	/** Non-static mathematical {@link zoranodensha.api.vehicles.part.util.expr.Expression expressions} which contain property references and thus require updates each tick. */
	private final ArrayList<Expression> expressions = new ArrayList<Expression>();

	/*
	 * Lists to keep track of listener part types.
	 */
	private final ArrayList<IPartType_MassListener> massListeners = new ArrayList<IPartType_MassListener>();
	private final ArrayList<IPartType_PositionListener> positionListeners = new ArrayList<IPartType_PositionListener>();

	/*
	 * Part properties and values.
	 */

	/** This part's global bounding box. Added {@code final} modifier to ensure {@code null}-safety. */
	protected final AxisAlignedBB boundingBox = AxisAlignedBB.getBoundingBox(0, 0, 0, 0, 0, 0);

	/** Default {@link #boundingBox} width. */
	protected final float width;
	/** Default {@link #boundingBox} height. */
	protected final float height;
	/** Default mass of this vehicle part, in metric tonnes ({@code 1t = 1000kg}). */
	protected final float mass;
	/** Whether this part will collide with in-world objects. */
	protected final PropBoolean collision;

	/** Name of this part. */
	protected final String name;

	/** Property holding boolean flag of whether this part is finished. */
	protected final PropBoolean isFinished;

	/** Property holding this part's local offset. */
	protected final PropVec3f offset;
	/** Property holding this part's local rotation. */
	protected final PropVec3f rotation;
	/** Property holding this part's local scale. */
	protected final PropVec3f scale;

	/*
	 *
	 * Dynamic fields.
	 *
	 *
	 */
	/** This part's identifier. It is used to distinguish it from other parts in a vehicle. Assigned by the parent this part may belong to. */
	private int id;

	/** Array of OreDictionary names specifying this part's recipe, if it is an XML-based vehicle part. Otherwise {@code null}. */
	private String[][] recipeMatrix;

	/** The parent section this part belongs to. May be {@code null}, but not if {@link #vehicle} is not {@code null}. */
	private VehicleSection section;

	/** The parent vehicle this part belongs to. May be {@code null}. */
	private VehicleData vehicle;

	/** Client-sided renderer for this part. Initialise in {@link #getRenderer()}. */
	@SideOnly(Side.CLIENT) protected IVehiclePartRenderer renderer;



	/*
	 *
	 * Constructors
	 *
	 */

	/**
	 * Create a new vehicle part with given properties.
	 *
	 * @param name - The unique name of this vehicle part.
	 * @param width - Bounding box width.
	 * @param height - Bounding box height.
	 * @param mass - Mass of this part (in metric Tonnes, {@code 1t = 1000kg}).
	 */
	public VehParBase(String name, float width, float height, float mass)
	{
		this.name = name;
		this.width = width;
		this.height = height;
		this.mass = mass;

		/*
		 * Append properties to the property map using addProperty().
		 */

		/*
		 * Reject modification of the rotation property if there is a bogie type in this vehicle part.
		 * To achieve this effect without writing a new class, we create an anonymous class.
		 */
		addProperty(rotation = (PropVec3f)new PropVec3f(this, "rotation")
		{
			@Override
			public boolean set(Object property)
			{
				if (property instanceof float[])
				{
					PartTypeBogie partTypeBogie = getPartType(PartTypeBogie.class);
					if (partTypeBogie != null)
					{
						property = new float[3];
					}
				}
				return super.set(property);
			}
		}.setConfigurable());

		/*
		 * The local offset triplet needs to restrict custom offset if there's a bogie type in this part.
		 */
		addProperty(offset = (PropVec3f)new PropVec3f(this, "offset")
		{
			@Override
			public boolean set(Object property)
			{
				if (property instanceof float[])
				{
					/* If the parent part contains a bogie type, restrict custom offset to local X-axis. */
					PartTypeBogie partTypeBogie = getPartType(PartTypeBogie.class);
					if (partTypeBogie != null)
					{
						float[] newOffset = (float[])property;
						newOffset[1] = partTypeBogie.getOffsetY() * getScale()[1];
						newOffset[2] = 0.0F;
					}
				}
				return super.set(property);
			}
		}.setConfigurable());

		/*
		 * Similar situation as above:
		 * The scale triplet needs a check in its setter for whether values are not 0.
		 * Additionally, if this part contains a bogie type, update the Y-offset after scale change, too.
		 */
		addProperty(scale = (PropVec3f)new PropVec3f(this, 1, 1, 1, "scale")
		{
			@Override
			public boolean set(Object property)
			{
				if (property instanceof float[])
				{
					/* Ensure either axis isn't close or equal to zero. */
					for (float f : (float[])property)
					{
						if (Math.abs(f) < 0.001F)
							return false;
					}
				}

				/* If the scale changed, update offset if required. */
				if (super.set(property))
				{
					if (getPartType(PartTypeBogie.class) != null)
					{
						setOffset(getOffset());
					}
					return true;
				}
				return false;
			}
		}.setConfigurable());

		/*
		 * Notify the parent train if this property changed.
		 */
		addProperty(isFinished = new PropBoolean(this, "isFinished")
		{
			@Override
			public boolean set(Object property)
			{
				if (super.set(property))
				{
					Train train = getTrain();
					if (train != null)
					{
						train.refreshProperties();
					}
					return true;
				}
				return false;
			}
		});

		/*
		 * Vehicle parts that should not have collision enabled (for example decorative parts/text) can
		 * set this property to false.
		 */
		addProperty(collision = (PropBoolean)new PropBoolean(this, true, "collision").setConfigurable());

		/*
		 * Initialise bounding box.
		 */
		getBoundingBox().setBB(initBoundingBox());
	}


	/*
	 *
	 * Methods
	 *
	 */

	/**
	 * Checks whether the given expression contains a reference and, if so, adds it to the {@link #expressions list} of expressions requiring updates.
	 *
	 * @param expression - {@link zoranodensha.api.vehicles.part.util.expr.Expression Expression} to potentially add to the list.
	 */
	public void addExpression(Expression expression)
	{
		if (expression.getHasReference())
		{
			expressions.add(expression);
		}
	}

	/**
	 * Called by vehicle part types <b>during instantiation</b> in order to place the calling part type into this vehicle part's list of part types.
	 *
	 * @param type - {@link zoranodensha.api.vehicles.part.type.APartType Part type} to add to this vehicle part.
	 * @throws IllegalArgumentException If this part already contained a part type of the given kind.
	 */
	public void addPartType(APartType type)
	{
		if (!partTypes.contains(type))
		{
			partTypes.add(type);
		}
		else
		{
			throw new IllegalArgumentException("A vehicle part may not contain the same part type twice!");
		}

		/*
		 * Append to respective listener types.
		 */
		if (type instanceof IPartType_MassListener)
		{
			massListeners.add((IPartType_MassListener)type);
		}
		if (type instanceof IPartType_PositionListener)
		{
			positionListeners.add((IPartType_PositionListener)type);
		}
	}

	/**
	 * Helper method to place the given property in the property map.
	 *
	 * @param prop - The {@link zoranodensha.api.vehicles.part.property.IPartProperty property} to place in the map.
	 */
	public void addProperty(IPartProperty<?> prop)
	{
		/* Only place the property in the map if its key is unique. */
		if (properties.containsKey(prop.getKey()))
		{
			throw new IllegalArgumentException(String.format("Property key is already assigned! [Key: %d, Property Type: %s]", prop.getKey(), prop.getClass()));
		}
		properties.put(prop.getKey(), prop);
	}

	/**
	 * Adds the given XML-property to this part.
	 *
	 * @param prop - {@link zoranodensha.api.vehicles.part.property.IXMLProperty XML-property} to add.
	 */
	public void addPropertyFromXML(IXMLProperty<?> prop)
	{
		addProperty(prop);
		xmlProperties.add(prop);
	}

	/**
	 * Creates a deep-copy of this vehicle part.
	 *
	 * @return A deep copy of this part.
	 * @throws RuntimeException - If the copy operation failed to instantiate either a new copy of this part's class or either part type.
	 */
	@SuppressWarnings("unchecked")
	public <T extends VehParBase> T copy()
	{
		/*
		 * Retrieve constructor of this class and create copy.
		 */
		T copy;
		try
		{
			if (getIsXMLPart())
			{
				/* Treat XML-based parts as special case. */
				copy = (T)getClass().getConstructor(String.class, float.class, float.class, float.class).newInstance(name, width, height, mass);
			}
			else
			{
				/* Otherwise assume existence of an empty constructor to create copy. */
				copy = (T)getClass().newInstance();
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException(e);
		}

		/*
		 * Some extra cases in case this is an XML-based vehicle part.
		 */
		if (getIsXMLPart())
		{
			/* Set the same recipe matrix in the copy as in this part. */
			copy.setRecipe(recipeMatrix);

			/* Copy part types to the copy part. */
			for (APartType type : getPartTypes())
			{
				try
				{
					type.getClass().getDeclaredConstructor(VehParBase.class).newInstance(copy);
				}
				catch (Exception e)
				{
					throw new RuntimeException(e);
				}
			}

			/* Manually create XML properties in the copy part. */
			if (!getXMLProperties().isEmpty())
			{
				for (IXMLProperty<?> customProp : getXMLProperties())
				{
					IXMLProperty<?> propCopy = customProp.copy(copy);
					propCopy.setSyncDir(customProp.getSyncDir());
					propCopy.setValidTagCalls(customProp.getValidTagCalls());

					if (customProp.getIsConfigurable())
					{
						propCopy.setConfigurable();
					}

					copy.addPropertyFromXML(propCopy);
				}
			}
		}

		/*
		 * After the copy has been created, copy data from this instance to the copy using NBT write and read operations.
		 */
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt, ETagCall.SAVE);
		copy.readFromNBT(nbt, ETagCall.SAVE);

		/*
		 * On client side, copy render data.
		 */
		if (Side.CLIENT.equals(FMLCommonHandler.instance().getEffectiveSide()))
		{
			copy.setRenderer(getRenderer().copy(copy));
		}

		return copy;
	}

	/**
	 * Return this part's bounding box with global positioning.
	 *
	 * @return {@link net.minecraft.util.AxisAlignedBB Bounding box} offset by its position.
	 */
	public AxisAlignedBB getBoundingBox()
	{
		return boundingBox;
	}

	/**
	 * Return whether this part should collide with in-world objects.
	 *
	 * @return - A {@code boolean}.
	 */
	public boolean getCollides()
	{
		return collision.get();
	}

	/**
	 * Returns the {@link #expressions list} of expressions which require updates.
	 *
	 * @return List of ticked expressions.
	 */
	public ArrayList<Expression> getExpressions()
	{
		return expressions;
	}

	/**
	 * Return the ID of this part. Internally handled and assigned, used to identify vehicle parts during packet handling.
	 *
	 * @return The ID of this part, or {@code 0} if undefined.
	 */
	public final int getID()
	{
		/*
		 * Every part gets assigned an identifier by the train it belongs to.
		 * The ID should be stored without any alteration to avoid bugs or confusion with other parts.
		 */
		return id;
	}

	/**
	 * Return {@code true} if this part is finished.
	 */
	public boolean getIsFinished()
	{
		/*
		 * Upon spawning a vehicle (from survival mode), every vehicle part is a dummy part.
		 * Only after the respective part's item has been applied to the part, it should be used. As long as the
		 * isFinished flag is false, the part should not allow any kind of action (e.g. doors opening).
		 */
		return isFinished.get();
	}

	/**
	 * Returns whether this is an XML-based vehicle part.<br> More precisely, determines the return value of {@link #getClass()} is the {@link VehParBase} class which is used as basic XML part.
	 *
	 * @return {@code true} if this is an XML-based part.
	 */
	public final boolean getIsXMLPart()
	{
		return (getClass() == VehParBase.class);
	}

	/**
	 * Return this part's bounding box with local positioning.
	 *
	 * @return {@link net.minecraft.util.AxisAlignedBB Bounding box} offset by this part's local {@link #offset translation}.
	 */
	public AxisAlignedBB getLocalBoundingBox()
	{
		/*
		 * Since in pretty much all cases a part's bounding box is centered on the part's origin,
		 * Zora no Densha offers a default implementation that uses the part's scaled half width and height.
		 */
		return initBoundingBox().offset(getOffset()[0], getOffset()[1], getOffset()[2]);
	}

	/**
	 * Return the mass (in Tonnes; 1t = 1000kg) of this part.
	 */
	public float getMass()
	{
		float mass = this.mass * getWeightScale();
		if (!massListeners.isEmpty())
		{
			for (IPartType_MassListener listener : massListeners)
			{
				mass = listener.getMass(mass);
			}
		}
		return mass;
	}

	/**
	 * Return the unique name of this vehicle part instance.<br> It will be used to identify vehicle parts of the same type and to register vehicle parts to the game.
	 */
	public String getName()
	{
		/*
		 * The name of a vehicle part should have a prefix indicating the mod it belongs to, something like
		 * zoranodensha_vehParExample
		 */
		return name;
	}

	/**
	 * Get the offset of this vehicle part.<br>
	 * <br>
	 * By convention, offset is defined as float array with three indices; X-axis, Y-axis, and Z-axis.
	 */
	public float[] getOffset()
	{
		/*
		 * The position in the Model Editor is also called "offset". Return the offset as float array with 3 indices. Never return null.
		 */
		return offset.get();
	}

	/**
	 * Retrieve a specific part type from this vehicle part.
	 *
	 * @return The specified {@link zoranodensha.api.vehicles.part.type.APartType part type}, or {@code null} if none was found.
	 */
	public <T extends APartType> T getPartType(Class<T> partTypeClass)
	{
		for (APartType partType : getPartTypes())
		{
			if (partTypeClass.isInstance(partType))
			{
				return partTypeClass.cast(partType);
			}
		}
		return null;
	}

	/**
	 * Returns all {@link #partTypes part types} of this vehicle part.
	 */
	public HashSet<APartType> getPartTypes()
	{
		return partTypes;
	}

	/**
	 * Return all {@link zoranodensha.api.vehicles.part.property.IPartProperty properties} held by this vehicle part.
	 *
	 * @return A {@link java.util.Collection Collection} holding all properties of this part. May be {@code null}.
	 */
	public Collection<IPartProperty<?>> getProperties()
	{
		return properties.values();
	}

	/**
	 * Return a renderer which renders this part.
	 *
	 * @return This part's {@link zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer renderer}.
	 */
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return renderer;
	}

	/**
	 * Get the rotation of this vehicle part.<br>
	 * <br>
	 * By convention, rotation is defined as float array with three indices, in order roll, yaw, pitch.
	 */
	public float[] getRotation()
	{
		/*
		 * As with offset, every part has a rotation about each axis, X (roll), Y (yaw), Z (pitch). All angles are in degrees, the float array has three indices and is never null.
		 */
		return rotation.get();
	}

	/**
	 * Get the scale of this vehicle part.<br>
	 * <br>
	 * By convention, scale is defined as float array with three indices; along X-axis, Y-axis, and Z-axis.
	 */
	public float[] getScale()
	{
		/*
		 * Just like offset and rotation, the part's scale is a float array with three indices, each index representing scale along X, Y, and Z, respectively.
		 * Unlike rotation and offset, the default scale array is initialised to (1F, 1F, 1F).
		 * Behaviour for negative scales is left up to the implementation, but is generally undefined.
		 */
		return scale.get();
	}

	/**
	 * Calculates the height of this part, multiplied by its Y-{@link #scale}.
	 *
	 * @return Scaled part height.
	 */
	public float getScaledHeight()
	{
		return height * getScale()[1];
	}

	/**
	 * Calculates the width of this part, multiplied by the smaller of either part's X- or Z-{@link #scale}.
	 *
	 * @return Scaled part width.
	 */
	public float getScaledWidth()
	{
		return width * Math.min(getScale()[0], getScale()[2]);
	}

	/**
	 * Returns the vehicle section this part belongs to.
	 *
	 * @return The {@link zoranodensha.api.vehicles.VehicleSection section} that is parent of this vehicle part. May be {@code null}.
	 */
	public VehicleSection getSection()
	{
		return section;
	}

	/**
	 * Returns the train this part belongs to, if existent.<br> This is a handy way to access a {@link net.minecraft.world.World World} object.
	 *
	 * @return The {@link zoranodensha.api.vehicles.Train train} this vehicle part belongs to. May be {@code null}.
	 */
	public Train getTrain()
	{
		return (getVehicle() != null) ? getVehicle().getTrain() : null;
	}

	/**
	 * Returns the language key used to translate this part's name into a human readable name.<br> Note that translation requires an entry in a .lang file.
	 *
	 * @return This part's unlocalised name.
	 */
	public String getUnlocalizedName()
	{
		return "vehiclePart." + getName() + ".name";
	}

	/**
	 * Returns the vehicle this part belongs to, if existent.
	 *
	 * @return The {@link zoranodensha.api.vehicles.VehicleData vehicle} this part belongs to. May be {@code null}.
	 */
	public VehicleData getVehicle()
	{
		return vehicle;
	}

	/**
	 * Calculates the product of all three {@link #getScale() scale} values and returns it.<br> Used to scale the {@link #mass} of this vehicle part.
	 *
	 * @return The product of all three scaled axes.
	 */
	public float getWeightScale()
	{
		float[] scale = getScale();
		return scale[0] * scale[1] * scale[2];
	}

	/**
	 * Returns all {@link #xmlProperties XML-properties} which were defined independently from part types through the part's XML file.
	 */
	public ArrayList<IXMLProperty<?>> getXMLProperties()
	{
		return xmlProperties;
	}

	@Override
	public int hashCode()
	{
		return getName().hashCode();
	}

	/**
	 * Initialises a new bounding box with this part's current {@link #getScaledWidth() width} and {@link #getScaledHeight() height}.
	 *
	 * @return The initialised {@link net.minecraft.util.AxisAlignedBB bounding box}.
	 */
	public AxisAlignedBB initBoundingBox()
	{
		float scaledHeightBy2 = getScaledHeight() / 2.0F;
		float scaledWidthBy2 = getScaledWidth() / 2.0F;
		return AxisAlignedBB.getBoundingBox(-scaledWidthBy2, -scaledHeightBy2, -scaledWidthBy2, scaledWidthBy2, scaledHeightBy2, scaledWidthBy2);
	}

	/**
	 * Called when this part is added to (or removed from) a vehicle section.
	 *
	 * @param section - The {@link zoranodensha.api.vehicles.VehicleSection section} this part was added to. May be {@code null} to notify of removal.
	 */
	public void onAddedToSection(VehicleSection section)
	{
		this.section = section;
	}

	/**
	 * Called when this part is added to a vehicle.
	 *
	 * @param vehicle - The {@link zoranodensha.api.vehicles.VehicleData vehicle} this part was added to.
	 */
	public void onAddedToVehicle(VehicleData vehicle)
	{
		this.vehicle = vehicle;
	}

	/**
	 * Called by the parent {@link #getVehicle() vehicle} when the train containing the vehicle (and thus, this part) changed.
	 *
	 * @param train - The new {@link zoranodensha.api.vehicles.Train train} this part now belongs to. May be {@code null}.
	 */
	public void onAddedToTrain(Train train)
	{
		;
	}

	/**
	 * Called whenever the given property has changed, i.e. when its {@link zoranodensha.api.vehicles.part.property.IPartProperty#set(Object) set()} method has been called and the property value has been successfully overridden.
	 *
	 * @param property - The {@link zoranodensha.api.vehicles.part.property.IPartProperty property} which changed.
	 */
	public void onPropertyChanged(IPartProperty<?> property)
	{
		;
	}

	/**
	 * Called to check whether there are any {@link zoranodensha.api.vehicles.part.util.expr.Expression expressions} in the {@link #expressions list} of expressions and, if so, to update them.
	 */
	public void onUpdateExpressions()
	{
		if (!expressions.isEmpty())
		{
			for (Expression expression : expressions)
			{
				expression.onUpdate();
			}
		}
	}

	/**
	 * Called to read all custom properties of this part from the given NBT tag.
	 *
	 * @param nbt - The {@link net.minecraft.nbt.NBTTagCompound NBTTag} to read from.
	 * @param callType - The {@link zoranodensha.api.util.ETagCall call type} that can be used to write/ read case-dependent data.
	 */
	public void readFromNBT(NBTTagCompound nbt, ETagCall callType)
	{
		/*
		 * Read part data here.
		 * The ETagCompoundCall indicates whether this is a call to read from a packet, a save file, or an item stack.
		 */

		/*
		 * Since the ID is recalculated every time the train - and thus its parts - are read from NBT, we only read the ID if this is a sync packet.
		 */
		if (ETagCall.PACKET == callType)
		{
			int id = nbt.getInteger("id");
			if (id > 0)
			{
				setID(id);
			}
		}

		/* Read property map from NBT. */
		for (IPartProperty<?> prop : properties.values())
		{
			prop.readFromNBT(nbt, callType);
		}
	}

	/**
	 * Called by Zora no Densha to register all recipes for this vehicle part.
	 *
	 * @param itemStack - {@link net.minecraft.item.ItemStack ItemStack} containing this part's NBT tag.
	 * @throws NullPointerException If the {@link #recipeMatrix} is {@code null}.
	 */
	public void registerItemRecipe(ItemStack itemStack)
	{
		/* Crash if there is no recipe. */
		if (recipeMatrix == null)
		{
			throw new NullPointerException("Tried to register a vehicle part without recipe data.");
		}

		/*
		 * Convert recipe matrix to Ore Dictionary recipe and register item recipe.
		 */
		ArrayList<Object> recipe = new ArrayList<Object>();
		{
			String[] s = new String[3];
			String index;

			for (int i = 0; i < 3; ++i)
			{
				/* Initialise recipe row. */
				s[i] = "";

				/* Match recipe data to matrix entry. */
				for (int j = 0; j < 3; ++j)
				{
					if (recipeMatrix[i][j] == null || recipeMatrix[i][j].isEmpty())
					{
						s[i] += " ";
					}
					else
					{
						s[i] += (index = Integer.toString(i * 3 + j));
						recipe.add(index.charAt(0));
						recipe.add(recipeMatrix[i][j]);
					}
				}
			}

			for (int i = s.length - 1; i >= 0; --i)
			{
				recipe.add(0, s[i]);
			}
		}

		/*
		 * Register recipe and describe caught exception if applicable.
		 */
		try
		{
			GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, recipe.toArray()));
		}
		catch (RuntimeException e)
		{
			APILogger.warn(this, String.format("Caught exception while registering XML part recipe for part '%s'. The recipe probably contains an invalid OreDictionary name!", getName()));
			APILogger.catching(e);
			throw e;
		}
	}

	/**
	 * Set the ID of this part. Internally handled and assigned, the implementing class only has to save the value and ensure it is correctly returned in {@link #getID()}.
	 */
	public final void setID(int id)
	{
		/*
		 * Overwrite the current ID field and set the given ID as new part identifier.
		 */
		this.id = id;
	}

	/**
	 * Set whether this part is finished.
	 *
	 * @param isFinished - {@code true} if the part is finished, {@code false} otherwise.
	 */
	public void setIsFinished(boolean isFinished)
	{
		/*
		 * Called to notify this part that it was applied and may be used now.
		 */
		this.isFinished.set(isFinished);
	}

	/**
	 * Set the offset of this vehicle part.<br>
	 * <br>
	 * By convention, offset is defined as float array with three indices; X-axis, Y-axis, and Z-axis.
	 */
	public void setOffset(float... values)
	{
		/*
		 * Called to apply a new offset to this part.
		 * If this vehicle part contains a bogie, we sure to allow modifications only on local X-axis.
		 * This is to prevent bogies not being on tracks by default.
		 */
		offset.set(values);
	}

	/**
	 * Set the part's in-world position to the given coordinates.<br> Used to offset the part's bounding box.
	 */
	public void setPosition(double x, double y, double z)
	{
		/*
		 * Careful! This one is different from values like offset or rotation, because the values given here are positions in the world.
		 * Instead, recalculate the bounding box (see above) without (!) applying the local offset, and offset the bounding box by the given values.
		 */

		if (!positionListeners.isEmpty())
		{
			for (IPartType_PositionListener listener : positionListeners)
			{
				listener.setPosition(x, y, z);
			}
		}
		getBoundingBox().setBB(initBoundingBox().offset(x, y, z));
	}

	/**
	 * Set the custom property for the given key.<br>
	 * <br>
	 * Unlike direct accesses to properties, calls to this method should always check whether the respective property needs synchronisation, if desired.
	 */
	public void setProperty(int key, Object val)
	{
		/*
		 * Apply a property for the given key.
		 * The respective wrapper type handles checks for whether the given property is valid.
		 */
		IPartProperty<?> property = properties.get(key);
		if (property != null)
		{
			/* Check for synchronisation packet. Handle if applicable. */
			if (val instanceof NBTTagCompound)
			{
				NBTTagCompound nbt = (NBTTagCompound)val;
				if (nbt.hasKey("propID") && nbt.getInteger("propID") == property.getKey())
				{
					/* Read the property from this NBT tag. Basically a substitute for #set() */
					property.readFromNBT(nbt, ETagCall.PACKET);
					return;
				}
			}

			/* If the new value turns out not be a synchronisation packet, try to assign. Property handles synch itself. */
			property.set(val);
		}
	}

	/**
	 * Override this part's {@link #recipeMatrix} matrix..<br> Called when a vehicle part is being created from an XML node tree.
	 *
	 * @param recipeMatrix - 3x3 String matrix of OreDictionary item names.
	 */
	public void setRecipe(String[][] recipeMatrix)
	{
		this.recipeMatrix = recipeMatrix;
	}

	/**
	 * Assign the given {@link zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer renderer} to this part.
	 */
	@SideOnly(Side.CLIENT)
	public void setRenderer(IVehiclePartRenderer renderer)
	{
		this.renderer = renderer;
	}

	/**
	 * Set the rotation of this vehicle part.<br>
	 * <br>
	 * By convention, rotation is defined as float array with three indices; roll, yaw, pitch.
	 */
	public void setRotation(float... values)
	{
		/*
		 * Apply new rotation to this part. Just as with setOffset(), you may do checks on the float array, but generally we don't have to.
		 */
		rotation.set(values);
	}

	/**
	 * Set the scale of this vehicle part.<br>
	 * <br>
	 * By convention, scale is defined as float array with three indices; X-axis, Y-axis, and Z-axis.
	 */
	public void setScale(float... values)
	{
		/*
		 * Set the scale of this part. Checks are handled by the property wrapper.
		 */
		scale.set(values);
	}

	@Override
	public String toString()
	{
		/* Common data */
		String clazz = getClass().getSimpleName();
		String uvid = (getVehicle() != null) ? getVehicle().getUVID().toString() : "~NULL~";
		String train = (getTrain() != null) ? String.valueOf(getTrain().getEntityId()) : "~NULL~";

		/* Specific data */
		String name = getName();
		int id = getID();
		Vec3 gpos = VehicleHelper.getPosition(boundingBox);
		float[] lpos = getOffset();

		return String.format("%s[UVID=%s | Train=%s | Name=%s | ID=%d | %.2fx %.2fy %.2fz | %.2flx %.2fly %.2flz]", clazz, uvid, train, name, id, gpos.xCoord, gpos.yCoord, gpos.zCoord, lpos[0], lpos[1], lpos[2]);
	}

	/**
	 * Called to write all custom properties of this vehicle part to the given NBT tag.
	 *
	 * @param nbt - {@link net.minecraft.nbt.NBTTagCompound NBT tag} to write to.
	 * @param callType - The {@link zoranodensha.api.util.ETagCall call type} that can be used to write/ read case-dependent data.
	 */
	public void writeToNBT(NBTTagCompound nbt, ETagCall callType)
	{
		/*
		 * Write this part to NBT, depending on the given call type.
		 */

		/* Write part ID if it's a packet call. (Why? - see further above.) */
		if (ETagCall.PACKET == callType)
		{
			nbt.setInteger("id", getID());
		}

		/* Write property map to NBT. */
		for (IPartProperty<?> prop : properties.values())
		{
			prop.writeToNBT(nbt, callType);
		}
	}
}