package zoranodensha.api.vehicles.part.util;

import java.util.Comparator;

import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.util.OrderedList;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.ITickableProperty;
import zoranodensha.api.vehicles.part.type.APartType;
import zoranodensha.api.vehicles.part.util.OrderedTypesList.ITypesSelector;



/**
 * <p>
 * Ordered list holding {@link zoranodensha.api.vehicles.part.VehParBase vehicle parts}.
 * </p>
 * 
 * <p>
 * The contained elements are sorted in <i>descending</i> order from front to back, depending on the
 * {@link zoranodensha.api.vehicles.part.VehParBase#getOffset() local X-offset} of the part.
 * </p>
 * 
 * @author Leshuwa Kaiheiwa
 */
public class OrderedPartsList extends OrderedList<VehParBase>
{
	/*
	 * Various selectors.
	 */
	public static final IPartsSelector SELECTOR_ANY = new SelectorAny();
	public static final IPartsSelector SELECTOR_EXPRESSION = new SelectorExpression();

	public static final IPartsSelector SELECTOR_PROPS_TICKABLE = new SelectorTickableProps();
	public static final IPartsSelector SELECTOR_PROPS_SYNCHRONISED = new SelectorSynchronisedProps();

	public static final IPartsSelector SELECTOR_FINISHED = new SelectorFinished(true);
	public static final IPartsSelector SELECTOR_UNFINISHED = new SelectorFinished(false);



	public OrderedPartsList()
	{
		super(VehParBase.class, new Comparator<VehParBase>()
		{
			@Override
			public int compare(VehParBase partA, VehParBase partB)
			{
				return Float.compare(partA.getOffset()[0], partB.getOffset()[0]);
			}
		});
	}

	/**
	 * Adds all entries from this list to the result if they are accepted by the given selector.
	 * 
	 * @param selector - {@link IPartsSelector Selector} to filter this list.
	 * @return The resulting list.
	 */
	public OrderedPartsList filter(IPartsSelector selector)
	{
		OrderedPartsList res = new OrderedPartsList();
		if (isEmpty())
		{
			return res;
		}

		for (VehParBase e : this)
		{
			if (selector.addToList(e))
			{
				res.add(e);
			}
		}

		return res;
	}

	/**
	 * Adds all entries from this list to the result if they are accepted by the given selector.
	 * 
	 * @param selector - {@link zoranodensha.api.vehicles.part.util.OrderedTypesList.ITypesSelector Selector} to filter this list.
	 * @return The resulting list.
	 */
	@SuppressWarnings("unchecked")
	public <P extends APartType> OrderedTypesList<P> filter(ITypesSelector<P> selector)
	{
		OrderedTypesList<P> res = new OrderedTypesList<P>(selector.getTypeClass());
		if (isEmpty())
		{
			return res;
		}

		for (VehParBase e : this)
		{
			for (APartType partType : e.getPartTypes())
			{
				if (selector.addToList(partType))
				{
					res.add((P)partType);
				}
			}
		}

		return res;
	}

	/**
	 * Searches this list for the part closest to the specified local X coordinate.
	 * 
	 * @param localX - Local X-coordinate used as pivot point.
	 * @return The closest part in this list, or {@code null} if this list is empty.
	 */
	public VehParBase getClosestPart(float localX)
	{
		/* Return null if the list is empty. */
		if (isEmpty())
		{
			return null;
		}

		/* Handle corner cases. */
		if (localX >= get(0).getOffset()[0])
		{
			return get(0);
		}
		else if (localX <= get(size() - 1).getOffset()[0])
		{
			return get(size() - 1);
		}

		/* Initialise left and right bounds. */
		int mid = 0;
		int leftBound = 0;
		int rightBound = size();
		VehParBase elem;

		/* While left bound is smaller than right bound: */
		while (leftBound < rightBound)
		{
			/* Determine middle element index and retrieve element. */
			mid = (leftBound + rightBound) / 2;
			elem = get(mid);

			/* Special case: If we encounter an element whose position matches exactly the reference, we return it. */
			if (elem.getOffset()[0] == localX)
			{
				return elem;
			}

			/* If the element's X position is smaller than the reference value, assign middle index as new right bound. */
			if (elem.getOffset()[0] < localX)
			{
				rightBound = mid;
			}

			/* Otherwise assign as left bound. */
			else
			{
				leftBound = mid + 1;
			}
		}

		/* After leaving the loop, return element at middle index. */
		return get(mid);
	}

	/**
	 * Determines the part whose in-world position is closest to the given coordinate triplet.
	 * 
	 * @return The {@link zoranodensha.api.vehicles.part.VehParBase vehicle part} closest to the specified position, or {@code null} if this list is empty.
	 */
	public VehParBase getClosestPart(double posX, double posY, double posZ)
	{
		/* Return null if this list is empty. */
		if (isEmpty())
		{
			return null;
		}

		/* Prepare iteration. */
		VehParBase closest = null;
		double minDistSq = Double.MAX_VALUE;
		double curDistSq;

		/* Iterate over all parts in this list. */
		for (VehParBase part : this)
		{
			/* If the current part is closer to the position than the previous part, save its reference. */
			curDistSq = PartHelper.getPosition(part).squareDistanceTo(posX, posY, posZ);
			if (curDistSq < minDistSq)
			{
				closest = part;
				minDistSq = curDistSq;
			}
		}

		/* Return the closest part. */
		return closest;
	}



	/**
	 * Selector interface to select specific {@link zoranodensha.api.vehicles.part.VehParBase vehicle parts}
	 * from a {@link OrderedPartsList parts list}.
	 */
	public interface IPartsSelector
	{
		/**
		 * Determine whether the given part is accepted by this selector.
		 * 
		 * @param part - {@link zoranodensha.api.vehicles.part.VehParBase Part} to filter.
		 * @return {@code true} if this selector accepts the given part, {@code false} if not.
		 */
		boolean addToList(VehParBase part);
	}

	/**
	 * Select any vehicle part.
	 */
	private static class SelectorAny implements IPartsSelector
	{
		@Override
		public boolean addToList(VehParBase part)
		{
			return true;
		}
	}

	/**
	 * Select vehicle parts which contain expressions requiring updates.
	 */
	private static class SelectorExpression implements IPartsSelector
	{
		@Override
		public boolean addToList(VehParBase part)
		{
			return !part.getExpressions().isEmpty();
		}
	}

	/**
	 * Select vehicle parts which are (not) finished.
	 */
	private static class SelectorFinished implements IPartsSelector
	{
		private final boolean isFinishedFlag;



		public SelectorFinished(boolean isFinishedFlag)
		{
			this.isFinishedFlag = isFinishedFlag;
		}

		@Override
		public boolean addToList(VehParBase part)
		{
			return (part.getIsFinished() == isFinishedFlag);
		}
	}

	/**
	 * Select vehicle parts with one or more synchronised properties.
	 */
	private static class SelectorSynchronisedProps implements IPartsSelector
	{
		@Override
		public boolean addToList(VehParBase part)
		{
			for (IPartProperty<?> prop : part.getProperties())
			{
				if (prop.getSyncDir() != ESyncDir.NOSYNC && prop.getIsValidTagCall(ETagCall.PACKET))
				{
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * Select vehicle parts with one or more tickable properties.
	 */
	private static class SelectorTickableProps implements IPartsSelector
	{
		@Override
		public boolean addToList(VehParBase part)
		{
			for (IPartProperty<?> prop : part.getProperties())
			{
				if (prop instanceof ITickableProperty)
				{
					return true;
				}
			}
			return false;
		}
	}
}