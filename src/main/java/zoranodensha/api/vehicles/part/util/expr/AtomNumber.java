package zoranodensha.api.vehicles.part.util.expr;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;



/**
 * Numbers inside an expression.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class AtomNumber implements IExpressionComponent
{
	/** The value of this number. */
	private final double value;



	public AtomNumber(String value)
	{
		this.value = Double.parseDouble(value);
	}

	protected AtomNumber(double value)
	{
		this.value = value;
	}

	@Override
	public boolean getHasReference()
	{
		return false;
	}

	@Override
	public boolean getIsTrue()
	{
		return Math.abs(getValue()) > 0.00001;
	}

	@Override
	public double getValue()
	{
		return value;
	}

	@Override
	public String toString()
	{
		DecimalFormat decimalFormat = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
		decimalFormat.setMaximumFractionDigits(340); /* 340 = DecimalFormat.DOUBLE_FRACTION_DIGITS */
		return decimalFormat.format(getValue());
	}



	/**
	 * Special case of a number, pi.
	 */
	public static class AtomNumberPI extends AtomNumber
	{
		public AtomNumberPI()
		{
			super(Math.PI);
		}

		@Override
		public String toString()
		{
			return "PI";
		}
	}
}