package zoranodensha.api.vehicles.part.util.expr.ref;

import zoranodensha.api.vehicles.part.property.PropShort;



/**
 * Reference to a {@link zoranodensha.api.vehicles.part.property.PropShort Short property}.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class RefShort extends AReference
{
	/** The referenced {@link zoranodensha.api.vehicles.part.property.PropShort property}. */
	private final PropShort reference;



	public RefShort(PropShort reference)
	{
		this.reference = reference;
	}

	@Override
	public double getValue()
	{
		return reference.get();
	}
}