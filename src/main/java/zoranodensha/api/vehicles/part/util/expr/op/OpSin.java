package zoranodensha.api.vehicles.part.util.expr.op;

import zoranodensha.api.vehicles.part.util.expr.IExpressionComponent;

public class OpSin extends AOperator
{
	public OpSin(IExpressionComponent[] arguments)
	{
		super(arguments);
	}

	@Override
	protected double evaluate()
	{
		return Math.sin(arguments[0].getValue());
	}
	
	@Override
	public String toString()
	{
		return String.format("sin(%s)", arguments[0]);
	}
}