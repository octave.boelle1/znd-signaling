package zoranodensha.api.vehicles.part.util;

import java.util.Comparator;

import zoranodensha.api.util.OrderedList;
import zoranodensha.api.vehicles.part.type.APartType;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Activatable;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_MassListener;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_PositionListener;
import zoranodensha.api.vehicles.part.type.APartType.IPartType_Tickable;
import zoranodensha.api.vehicles.part.type.APartTypeCoupling;
import zoranodensha.api.vehicles.part.type.APartTypeCoupling.APartTypeCouplingAuto;
import zoranodensha.api.vehicles.part.type.APartTypeEngine;
import zoranodensha.api.vehicles.part.type.APartTypeTransport;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.type.PartTypeCab;
import zoranodensha.api.vehicles.part.type.PartTypeCompressor;
import zoranodensha.api.vehicles.part.type.PartTypeCouplingChain;
import zoranodensha.api.vehicles.part.type.PartTypeCouplingJanney;
import zoranodensha.api.vehicles.part.type.PartTypeCouplingScharfenberg;
import zoranodensha.api.vehicles.part.type.PartTypeDoor;
import zoranodensha.api.vehicles.part.type.PartTypeEngineDiesel;
import zoranodensha.api.vehicles.part.type.PartTypeEngineElectric;
import zoranodensha.api.vehicles.part.type.PartTypeHorn;
import zoranodensha.api.vehicles.part.type.PartTypeLamp;
import zoranodensha.api.vehicles.part.type.PartTypeLoadable;
import zoranodensha.api.vehicles.part.type.PartTypePantograph;
import zoranodensha.api.vehicles.part.type.PartTypeSeat;
import zoranodensha.api.vehicles.part.type.PartTypeTransportItem;
import zoranodensha.api.vehicles.part.type.PartTypeTransportTank;



/**
 * <p>
 * Ordered list holding {@link zoranodensha.api.vehicles.part.type.APartType part types}.
 * </p>
 * 
 * <p>
 * The contained elements are sorted in <i>descending</i> order from front to back, depending on the
 * {@link zoranodensha.api.vehicles.part.VehParBase#getOffset() local X-offset} of the part type's
 * {@link zoranodensha.api.vehicles.part.type.APartType#getParent() parent} part.
 * </p>
 * 
 * @author Leshuwa Kaiheiwa
 */
public class OrderedTypesList<T extends APartType> extends OrderedList<T>
{
	/*
	 * Generic selectors
	 */
	public static final ITypesSelector<APartType> SELECTOR_ANY = new SelectorAny();

	/*
	 * Bogie selectors
	 */
	public static final ITypesSelector<PartTypeBogie> SELECTOR_BOGIE_DUMMY = new SelectorBogie(true);
	public static final ITypesSelector<PartTypeBogie> SELECTOR_BOGIE_NODUMMY = new SelectorBogie(false);

	/*
	 * Interface selectors
	 */
	public static final ITypesSelector<APartType> SELECTOR_ACTIVATABLE = new SelectorClass(IPartType_Activatable.class);
	public static final ITypesSelector<APartType> SELECTOR_MASS_LISTENER = new SelectorClass(IPartType_MassListener.class);
	public static final ITypesSelector<APartType> SELECTOR_POSITION_LISTENER = new SelectorClass(IPartType_PositionListener.class);
	public static final ITypesSelector<APartType> SELECTOR_TICKABLE = new SelectorClass(IPartType_Tickable.class);

	/*
	 * Abstract type selectors
	 */
	public static final ITypesSelector<APartTypeCoupling> ASELECTOR_COUPLING = new SelectorType<APartTypeCoupling>(APartTypeCoupling.class);
	public static final ITypesSelector<APartTypeCouplingAuto> ASELECTOR_COUPLING_AUTO = new SelectorType<APartTypeCouplingAuto>(APartTypeCouplingAuto.class);
	public static final ITypesSelector<APartTypeEngine> ASELECTOR_ENGINE = new SelectorType<APartTypeEngine>(APartTypeEngine.class);
	public static final ITypesSelector<APartTypeTransport> ASELECTOR_TRANSPORT = new SelectorType<APartTypeTransport>(APartTypeTransport.class);

	/*
	 * Type selectors
	 */
	public static final ITypesSelector<PartTypeBogie> SELECTOR_BOGIE = new SelectorType<PartTypeBogie>(PartTypeBogie.class);
	public static final ITypesSelector<PartTypeCab> SELECTOR_CAB = new SelectorType<PartTypeCab>(PartTypeCab.class);
	public static final ITypesSelector<PartTypeCompressor> SELECTOR_COMPRESSOR = new SelectorType<PartTypeCompressor>(PartTypeCompressor.class);
	public static final ITypesSelector<PartTypeCouplingChain> SELECTOR_COUPLING_CHAIN = new SelectorType<PartTypeCouplingChain>(PartTypeCouplingChain.class);
	public static final ITypesSelector<PartTypeCouplingJanney> SELECTOR_COUPLING_JANNEY = new SelectorType<PartTypeCouplingJanney>(PartTypeCouplingJanney.class);
	public static final ITypesSelector<PartTypeCouplingScharfenberg> SELECTOR_COUPLING_SCHARFENBERG = new SelectorType<PartTypeCouplingScharfenberg>(PartTypeCouplingScharfenberg.class);
	public static final ITypesSelector<PartTypeDoor> SELECTOR_DOOR = new SelectorType<PartTypeDoor>(PartTypeDoor.class);
	public static final ITypesSelector<PartTypeEngineDiesel> SELECTOR_ENGINE_DIESEL = new SelectorType<PartTypeEngineDiesel>(PartTypeEngineDiesel.class);
	public static final ITypesSelector<PartTypeEngineElectric> SELECTOR_ENGINE_ELECTRIC = new SelectorType<PartTypeEngineElectric>(PartTypeEngineElectric.class);
	public static final ITypesSelector<PartTypeHorn> SELECTOR_HORN = new SelectorType<PartTypeHorn>(PartTypeHorn.class);
	public static final ITypesSelector<PartTypeLamp> SELECTOR_LAMP = new SelectorType<PartTypeLamp>(PartTypeLamp.class);
	public static final ITypesSelector<PartTypeLoadable> SELECTOR_LOADABLE = new SelectorType<PartTypeLoadable>(PartTypeLoadable.class);
	public static final ITypesSelector<PartTypePantograph> SELECTOR_PANTOGRAPH = new SelectorType<PartTypePantograph>(PartTypePantograph.class);
	public static final ITypesSelector<PartTypeSeat> SELECTOR_SEAT = new SelectorSeat(false);
	public static final ITypesSelector<PartTypeSeat> SELECTOR_SEAT_NO_CAB = new SelectorSeat(true);
	public static final ITypesSelector<PartTypeTransportItem> SELECTOR_TRANSPORT_ITEM = new SelectorType<PartTypeTransportItem>(PartTypeTransportItem.class);
	public static final ITypesSelector<PartTypeTransportTank> SELECTOR_TRANSPORT_TANK = new SelectorType<PartTypeTransportTank>(PartTypeTransportTank.class);



	public OrderedTypesList(Class<T> type)
	{
		super(type, new Comparator<T>()
		{
			@Override
			public int compare(T typeA, T typeB)
			{
				return Float.compare(typeA.getParent().getOffset()[0], typeB.getParent().getOffset()[0]);
			}
		});
	}


	/**
	 * Adds all entries from this list to the result if they are accepted by the given selector.
	 * 
	 * @param selector - {@link ITypesSelector Selector} to filter this list.
	 * @return The resulting list.
	 */
	@SuppressWarnings("unchecked")
	public <P extends APartType> OrderedTypesList<P> filter(ITypesSelector<P> selector)
	{
		OrderedTypesList<P> res = new OrderedTypesList<P>(selector.getTypeClass());
		if (isEmpty())
		{
			return res;
		}

		for (T e : this)
		{
			if (selector.addToList(e))
			{
				res.add((P)e);
			}
		}

		return res;
	}

	/**
	 * Searches this list for the part type closest to the specified local X coordinate.<br>
	 * More precisely, selects the part type whose parent's local X position is closest to the given coordinate.
	 * 
	 * @param localX - Local X-coordinate used as pivot point.
	 * @return The closest {@link zoranodensha.api.vehicles.part.type.APartType part type} in this list, or {@code null} if this list is empty.
	 */
	public T getClosestPartType(float localX)
	{
		/* Return null if the list is empty. */
		if (isEmpty())
		{
			return null;
		}

		/* Handle corner cases. */
		if (localX >= get(0).getParent().getOffset()[0])
		{
			return get(0);
		}
		else if (localX <= get(size() - 1).getParent().getOffset()[0])
		{
			return get(size() - 1);
		}

		/* Initialise left and right bounds. */
		int mid = 0;
		int leftBound = 0;
		int rightBound = size();
		T elem;

		/*
		 * While left bound is smaller than right bound:
		 */
		while (leftBound < rightBound)
		{
			/* Determine middle element index and retrieve element. */
			mid = (leftBound + rightBound) / 2;
			elem = get(mid);

			/* Special case: If we encounter an element whose position matches exactly the reference, we return it. */
			if (elem.getParent().getOffset()[0] == localX)
			{
				return elem;
			}

			/* If the element's X position is smaller than the reference value, assign middle index as new right bound. */
			if (elem.getParent().getOffset()[0] < localX)
			{
				rightBound = mid;
			}

			/* Otherwise assign as left bound. */
			else
			{
				leftBound = mid + 1;
			}
		}

		/* After leaving the loop, return element at middle index. */
		return get(mid);
	}

	/**
	 * Determines the part type whose parent's in-world position is closest to the given coordinate triplet.
	 * 
	 * @return The {@link zoranodensha.api.vehicles.part.type.APartType part type} closest to the specified position, or {@code null} if this list is empty.
	 */
	public T getClosestPartType(double posX, double posY, double posZ)
	{
		/* Return null if this list is empty. */
		if (isEmpty())
		{
			return null;
		}

		/* Prepare iteration. */
		T closest = null;
		double minDistSq = Double.MAX_VALUE;
		double curDistSq;

		/* Iterate over all part types in this list. */
		for (T type : this)
		{
			/* If the current part type is closer to the position than the previous part type, save its reference. */
			curDistSq = PartHelper.getPosition(type.getParent()).squareDistanceTo(posX, posY, posZ);
			if (curDistSq < minDistSq)
			{
				closest = type;
				minDistSq = curDistSq;
			}
		}

		/* Return the closest part type. */
		return closest;
	}



	/**
	 * Selector interface to select specific {@link zoranodensha.api.vehicles.part.type.APartType part types}
	 * from a {@link OrderedTypesList types list}.
	 */
	public interface ITypesSelector<P extends APartType>
	{
		/**
		 * Determine whether the given part is accepted by this selector.
		 * 
		 * @param type - {@link zoranodensha.api.vehicles.part.type.APartType Part type} to filter.
		 * @return {@code true} if this selector accepts the given part, {@code false} if not.
		 */
		boolean addToList(APartType type);

		/**
		 * Return the type class of the part type this selector accepts.
		 * 
		 * @return The accepted type's class.
		 */
		Class<P> getTypeClass();
	}

	/**
	 * Select any part type.
	 */
	private static class SelectorAny implements ITypesSelector<APartType>
	{
		@Override
		public boolean addToList(APartType type)
		{
			return true;
		}

		@Override
		public Class<APartType> getTypeClass()
		{
			return APartType.class;
		}
	}

	/**
	 * Select (non-)dummy bogie types.
	 */
	private static class SelectorBogie implements ITypesSelector<PartTypeBogie>
	{
		private final boolean isDummyFlag;



		public SelectorBogie(boolean isDummyFlag)
		{
			this.isDummyFlag = isDummyFlag;
		}

		@Override
		public boolean addToList(APartType type)
		{
			return (type instanceof PartTypeBogie) && ((PartTypeBogie)type).getIsDummy() == isDummyFlag;
		}

		@Override
		public Class<PartTypeBogie> getTypeClass()
		{
			return PartTypeBogie.class;
		}
	}

	/**
	 * Select part types implementing the given class type.<br>
	 * Used in conjunction with interfaces.
	 */
	public static class SelectorClass implements ITypesSelector<APartType>
	{
		private final Class<?> typeClass;



		public SelectorClass(Class<?> typeClass)
		{
			this.typeClass = typeClass;
		}

		@Override
		public boolean addToList(APartType type)
		{
			return typeClass.isInstance(type);
		}

		@Override
		public Class<APartType> getTypeClass()
		{
			return APartType.class;
		}
	}

	/**
	 * Select the specified part type.
	 */
	public static class SelectorSeat implements ITypesSelector<PartTypeSeat>
	{
		private final boolean noCabs;



		public SelectorSeat(boolean noCabs)
		{
			this.noCabs = noCabs;
		}

		@Override
		public boolean addToList(APartType type)
		{
			if (noCabs && type instanceof PartTypeCab)
			{
				return false;
			}
			return (type instanceof PartTypeSeat);
		}

		@Override
		public Class<PartTypeSeat> getTypeClass()
		{
			return PartTypeSeat.class;
		}
	}

	/**
	 * Select the specified part type.
	 */
	public static class SelectorType<P extends APartType> implements ITypesSelector<P>
	{
		private final Class<P> typeClass;



		public SelectorType(Class<P> typeClass)
		{
			this.typeClass = typeClass;
		}

		@Override
		public boolean addToList(APartType type)
		{
			return typeClass.isInstance(type);
		}

		@Override
		public Class<P> getTypeClass()
		{
			return typeClass;
		}
	}
}