package zoranodensha.api.vehicles.part.util.expr.op;

import zoranodensha.api.vehicles.part.util.expr.IExpressionComponent;



public class OpGreaterThan extends AOperator
{
	public OpGreaterThan(IExpressionComponent[] arguments)
	{
		super(arguments);
	}

	@Override
	protected double evaluate()
	{
		return getIsTrue() ? 1 : 0;
	}

	@Override
	public boolean getIsTrue()
	{
		return Double.compare(arguments[0].getValue(), arguments[1].getValue()) > 0;
	}

	@Override
	public String toString()
	{
		String arg0 = (arguments[0] instanceof AOperator) ? "(%s)" : "%s";
		String arg1 = (arguments[1] instanceof AOperator) ? "(%s)" : "%s";
		return String.format("%s > %s", String.format(arg0, arguments[0]), String.format(arg1, arguments[1]));
	}
}