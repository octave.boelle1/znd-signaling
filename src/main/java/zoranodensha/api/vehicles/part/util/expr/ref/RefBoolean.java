package zoranodensha.api.vehicles.part.util.expr.ref;

import zoranodensha.api.vehicles.part.property.PropBoolean;



/**
 * Reference to a {@link zoranodensha.api.vehicles.part.property.PropBoolean Boolean property}.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class RefBoolean extends AReference
{
	/** The referenced {@link zoranodensha.api.vehicles.part.property.PropBoolean property}. */
	private final PropBoolean reference;



	public RefBoolean(PropBoolean reference)
	{
		this.reference = reference;
	}
	
	@Override
	public boolean getIsTrue()
	{
		return reference.get();
	}

	@Override
	public double getValue()
	{
		return reference.get() ? 1 : 0;
	}
}