package zoranodensha.api.vehicles.part.util;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.part.type.APartType;
import zoranodensha.api.vehicles.part.util.OrderedTypesList.ITypesSelector;



/**
 * <p>
 * List implementation which concatenates lists of {@link zoranodensha.api.vehicles.part.type.APartType part types}.<br>
 * Order of elements corresponds to the order in the parent vehicles's {@link zoranodensha.api.vehicles.VehicleList vehicle list}.
 * </p>
 *
 * <p>
 * Note:<br>
 * This list creates a private copy of the vehicle list of the train it initially belonged to.
 * Modifications of the parent train's vehicle list are not reflected in this list's iteration behaviour.
 * </p>
 * 
 * @author Leshuwa Kaiheiwa
 */
public class ConcatenatedTypesList<T extends APartType> extends AbstractList<T>
{
	/** List of {@link zoranodensha.api.vehicles.VehicleData vehicles} whose parts are referenced by this list. */
	private final ArrayList<VehicleData> vehicles;
	/** {@link zoranodensha.api.vehicles.part.util.OrderedTypesList.ITypesSelector Selector} to choose which {@link zoranodensha.api.vehicles.part.type.APartType part types} are in this list. */
	private final ITypesSelector<T> selector;
	/** Number of total elements in this parts list. We assume this list to be trashed if either child list is modified. */
	private final int totalSize;



	/**
	 * Create a concatenated types list.
	 * 
	 * @param train - The {@link zoranodensha.api.vehicles.Train train} whose {@link #vehicles} are going to be iterated over.
	 * @param selector - The {@link #selector} that defines which types are contained in this list.
	 */
	public ConcatenatedTypesList(Train train, ITypesSelector<T> selector)
	{
		this.vehicles = train.getVehicles();
		this.selector = selector;

		int size = 0;
		for (VehicleData vehicle : vehicles)
		{
			size += vehicle.getTypes(selector).size();
		}

		this.totalSize = size;
	}

	@Override
	public T get(int index)
	{
		if (index >= 0 && index < size())
		{
			for (VehicleData vehicle : vehicles)
			{
				OrderedTypesList<T> typesListVehicle = vehicle.getTypes(selector);
				if (index >= typesListVehicle.size())
				{
					index -= typesListVehicle.size();
				}
				else
				{
					return typesListVehicle.get(index);
				}
			}
		}

		throw new IndexOutOfBoundsException("Index: " + index + ", Size:" + size());
	}

	/**
	 * Searches this list for the part type closest to the specified local X coordinate.<br>
	 * More precisely, selects the part type whose parent's local X position is closest to the given coordinate.
	 * 
	 * @param localX - Local X-coordinate used as pivot point.
	 * @return The closest {@link zoranodensha.api.vehicles.part.type.APartType part type} in this list, or {@code null} if this list is empty.
	 */
	public T getClosestPartType(float localX)
	{
		Iterator<VehicleData> itera = vehicles.iterator();
		VehicleData vehicle;

		/* For each vehicle in the list.. */
		while (itera.hasNext())
		{
			vehicle = itera.next();

			/*
			 * ..consider its parts if the last part's local offset is greater than the given local offset, or if there are no more vehicles in this list.
			 */
			if (vehicle.getPartAt(vehicle.getPartCount() - 1).getOffset()[0] >= localX || !itera.hasNext())
			{
				return vehicle.getTypes(selector).getClosestPartType(localX);
			}
		}

		return null;
	}

	/**
	 * Determines the part type whose parent's in-world position is closest to the given coordinate triplet.
	 * 
	 * @return The {@link zoranodensha.api.vehicles.part.type.APartType part type} closest to the specified position, or {@code null} if this list is empty.
	 */
	public T getClosestPartType(double posX, double posY, double posZ)
	{
		/* Return null if this list is empty. */
		if (isEmpty())
		{
			return null;
		}

		/* Prepare iteration. */
		T closest = null;
		double minDistSq = Double.MAX_VALUE;
		double curDistSq;

		/* Iterate over all part types in this list. */
		for (T type : this)
		{
			/* If the current part type is closer to the position than the previous part type, save its reference. */
			curDistSq = PartHelper.getPosition(type.getParent()).squareDistanceTo(posX, posY, posZ);
			if (curDistSq < minDistSq)
			{
				closest = type;
				minDistSq = curDistSq;
			}
		}

		/* Return the closest part type. */
		return closest;
	}

	@Override
	public Iterator<T> iterator()
	{
		return new Itera();
	}

	@Override
	public ListIterator<T> listIterator()
	{
		return new ListItera();
	}

	@Override
	public int size()
	{
		return totalSize;
	}



	private class Itera implements Iterator<T>
	{
		private Iterator<VehicleData> vehiclesItera = vehicles.iterator();
		private Iterator<T> typesListItera;



		public Itera()
		{
			nextTypesList();
		}


		@Override
		public boolean hasNext()
		{
			return (typesListItera != null) && typesListItera.hasNext();
		}

		@Override
		public T next()
		{
			/*
			 * Just call for the next types list.
			 * If the current iterator has remaining elements, it won't be skipped.
			 */
			T elem = typesListItera.next();
			nextTypesList();
			return elem;
		}

		/**
		 * Prepare the next iterator over a part types list. Skip empty iterators.
		 */
		private void nextTypesList()
		{
			while (vehiclesItera.hasNext() && (typesListItera == null || !typesListItera.hasNext()))
			{
				typesListItera = vehiclesItera.next().getTypes(selector).iterator();
			}
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException("remove");
		}
	}

	private class ListItera implements ListIterator<T>
	{
		private ListIterator<VehicleData> vehiclesItera = vehicles.listIterator();
		private ListIterator<T> typesListItera;
		private int currentTotalIndex = 0;



		@Override
		public void add(T e)
		{
			throw new UnsupportedOperationException("add");
		}

		@Override
		public boolean hasNext()
		{
			return (typesListItera != null) && typesListItera.hasNext();
		}

		@Override
		public boolean hasPrevious()
		{
			return (typesListItera != null) && typesListItera.hasPrevious();
		}

		@Override
		public T next()
		{
			/*
			 * Just call for the next types list.
			 * If the current iterator has remaining elements, it won't be skipped.
			 */
			T elem = typesListItera.next();
			nextTypesList();
			++currentTotalIndex;
			return elem;
		}

		@Override
		public int nextIndex()
		{
			return currentTotalIndex;
		}

		/**
		 * Prepare the next iterator over a part types list. Skip empty iterators.
		 */
		private void nextTypesList()
		{
			while (vehiclesItera.hasNext() && (typesListItera == null || !typesListItera.hasNext()))
			{
				typesListItera = vehiclesItera.next().getTypes(selector).listIterator();
			}
		}

		@Override
		public T previous()
		{
			/*
			 * Just call for the previous types list.
			 * If the current iterator has remaining elements, it won't be skipped.
			 */
			T elem = typesListItera.previous();
			previousPartsList();
			--currentTotalIndex;
			return elem;
		}

		@Override
		public int previousIndex()
		{
			return currentTotalIndex - 1;
		}

		/**
		 * Prepare the next iterator over a part types list. Skip empty iterators.
		 */
		private void previousPartsList()
		{
			while (vehiclesItera.hasPrevious() && (typesListItera == null || !typesListItera.hasPrevious()))
			{
				typesListItera = vehiclesItera.previous().getTypes(selector).listIterator();
			}
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException("remove");
		}

		@Override
		public void set(T e)
		{
			throw new UnsupportedOperationException("set");
		}
	}
}