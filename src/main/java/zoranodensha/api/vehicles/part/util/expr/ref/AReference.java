package zoranodensha.api.vehicles.part.util.expr.ref;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Locale;

import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.APartProperty;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropByte;
import zoranodensha.api.vehicles.part.property.PropDouble;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.property.PropInteger;
import zoranodensha.api.vehicles.part.property.PropLong;
import zoranodensha.api.vehicles.part.property.PropShort;
import zoranodensha.api.vehicles.part.rendering.ARenderTag;
import zoranodensha.api.vehicles.part.util.expr.IExpressionComponent;



/**
 * This atom references a property value.
 * 
 * @author Leshuwa Kaiheiwa
 */
public abstract class AReference implements IExpressionComponent
{
	private static final HashMap<Class<? extends APartProperty<?>>, Class<? extends AReference>> typesMap = new HashMap<Class<? extends APartProperty<?>>, Class<? extends AReference>>();
	static
	{
		typesMap.put(PropBoolean.class, RefBoolean.class);
		typesMap.put(PropByte.class, RefByte.class);
		typesMap.put(PropDouble.class, RefDouble.class);
		typesMap.put(PropFloat.class, RefFloat.class);
		typesMap.put(PropInteger.class, RefInteger.class);
		typesMap.put(PropLong.class, RefLong.class);
		typesMap.put(PropShort.class, RefShort.class);
	}



	/**
	 * Tries to create a reference to a property for the given name from the given vehicle part.
	 * 
	 * @param name - Name of the property to retrieve. Accepts both {@code $propName} and {@code $typeName.propName} format.
	 * @param part - {@link zoranodensha.api.vehicles.part.VehParBase Vehicle part} whose properties are to be referenced.
	 * @return A {@link AReference reference} to the selected property, or {@code null} if it couldn't be found.
	 */
	public static AReference getForName(String name, VehParBase part)
	{
		for (Class<? extends APartProperty<?>> clazz : typesMap.keySet())
		{
			APartProperty<?> prop = ARenderTag.findProperty(name, clazz, part);
			if (prop != null)
			{
				try
				{
					return typesMap.get(clazz).getConstructor(clazz).newInstance(prop);
				}
				catch (Exception e)
				{
					/* Silent catch. */
				}
			}
		}
		return null;
	}

	@Override
	public boolean getHasReference()
	{
		return true;
	}

	@Override
	public boolean getIsTrue()
	{
		return Math.abs(getValue()) > 0.00001;
	}

	@Override
	public String toString()
	{
		DecimalFormat decimalFormat = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
		decimalFormat.setMaximumFractionDigits(340); /* 340 = DecimalFormat.DOUBLE_FRACTION_DIGITS */
		return decimalFormat.format(getValue());
	}
}