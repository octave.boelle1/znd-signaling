package zoranodensha.api.vehicles.part.util.expr.ref;

import zoranodensha.api.vehicles.part.property.PropFloat;



/**
 * Reference to a {@link zoranodensha.api.vehicles.part.property.PropFloat Float property}.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class RefFloat extends AReference
{
	/** The referenced {@link zoranodensha.api.vehicles.part.property.PropFloat property}. */
	private final PropFloat reference;



	public RefFloat(PropFloat reference)
	{
		this.reference = reference;
	}

	@Override
	public double getValue()
	{
		return reference.get();
	}
}