package zoranodensha.api.vehicles.part.util.expr.op;

import zoranodensha.api.vehicles.part.util.expr.IExpressionComponent;



public class OpAbs extends AOperator
{
	public OpAbs(IExpressionComponent[] arguments)
	{
		super(arguments);
	}

	@Override
	protected double evaluate()
	{
		return Math.abs(arguments[0].getValue());
	}

	@Override
	public String toString()
	{
		return String.format("abs(%s)", arguments[0]);
	}
}