package zoranodensha.api.vehicles.part.util.expr;

import java.util.ArrayList;
import java.util.HashMap;

import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.expr.AtomNumber.AtomNumberPI;
import zoranodensha.api.vehicles.part.util.expr.op.AOperator;
import zoranodensha.api.vehicles.part.util.expr.op.OpAbs;
import zoranodensha.api.vehicles.part.util.expr.op.OpAdd;
import zoranodensha.api.vehicles.part.util.expr.op.OpAnd;
import zoranodensha.api.vehicles.part.util.expr.op.OpCos;
import zoranodensha.api.vehicles.part.util.expr.op.OpDivide;
import zoranodensha.api.vehicles.part.util.expr.op.OpEquals;
import zoranodensha.api.vehicles.part.util.expr.op.OpGreaterThan;
import zoranodensha.api.vehicles.part.util.expr.op.OpLessThan;
import zoranodensha.api.vehicles.part.util.expr.op.OpMax;
import zoranodensha.api.vehicles.part.util.expr.op.OpMin;
import zoranodensha.api.vehicles.part.util.expr.op.OpModulo;
import zoranodensha.api.vehicles.part.util.expr.op.OpMultiply;
import zoranodensha.api.vehicles.part.util.expr.op.OpNegate;
import zoranodensha.api.vehicles.part.util.expr.op.OpNot;
import zoranodensha.api.vehicles.part.util.expr.op.OpOr;
import zoranodensha.api.vehicles.part.util.expr.op.OpPower;
import zoranodensha.api.vehicles.part.util.expr.op.OpSin;
import zoranodensha.api.vehicles.part.util.expr.op.OpSqrt;
import zoranodensha.api.vehicles.part.util.expr.op.OpSubtract;
import zoranodensha.api.vehicles.part.util.expr.op.OpUnequals;
import zoranodensha.api.vehicles.part.util.expr.ref.AReference;



/**
 * Class for parsing expressions.<br>
 * The expression will then be stored as a binary tree, where its nodes are operations
 * and its leaves are atomic values, either static or referenced.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class Expression
{
	/** Mapping between all {@link zoranodensha.api.vehicles.part.util.expr.op.AOperator operators} and their respective String representation. */
	private static final HashMap<String, Object[]> OPERATORS = new HashMap<String, Object[]>();
	static
	{
		OPERATORS.put("==", new Object[] { OpEquals.class, 8, 2 });
		OPERATORS.put("!=", new Object[] { OpUnequals.class, 8, 2 });
		OPERATORS.put("<", new Object[] { OpLessThan.class, 7, 2 });
		OPERATORS.put(">", new Object[] { OpGreaterThan.class, 7, 2 });
		OPERATORS.put("OR", new Object[] { OpOr.class, 6, 2 });
		OPERATORS.put("AND", new Object[] { OpAnd.class, 6, 2 });
		OPERATORS.put("NOT", new Object[] { OpNot.class, 5, 1 });
		OPERATORS.put("+", new Object[] { OpAdd.class, 4, 2 });
		OPERATORS.put("-", new Object[] { OpSubtract.class, 4, 2 });
		OPERATORS.put("*", new Object[] { OpMultiply.class, 3, 2 });
		OPERATORS.put("/", new Object[] { OpDivide.class, 3, 2 });
		OPERATORS.put("%", new Object[] { OpModulo.class, 3, 2 });
		OPERATORS.put("^", new Object[] { OpPower.class, 2, 2 });
		OPERATORS.put("min", new Object[] { OpMin.class, 1, 2 });
		OPERATORS.put("max", new Object[] { OpMax.class, 1, 2 });
		OPERATORS.put("abs", new Object[] { OpAbs.class, 1, 1 });
		OPERATORS.put("cos", new Object[] { OpCos.class, 1, 1 });
		OPERATORS.put("sin", new Object[] { OpSin.class, 1, 1 });
		OPERATORS.put("sqrt", new Object[] { OpSqrt.class, 1, 1 });
	}

	/** Pattern matching against all numeric digits, from {@code 0} to {@code 9}. */
	private static final String PATTERN_DIGITS = "\\d";
	/** Pattern matching against all latin alphabet characters, from {@code a} to {@code z} and {@code A} to {@code Z}. */
	private static final String PATTERN_ALPHABET = "[a-zA-Z_]";

	/** The root {@link zoranodensha.api.vehicles.part.util.expr.IExpressionComponent component} of this expression. */
	private final IExpressionComponent expression;

	/** Previously known result of the evaluation of this expression. */
	private double prevValue;
	/** Result of the evaluation of this expression. */
	private double value;



	/**
	 * Parses and creates a new mathematical expression from the given String argument.
	 * 
	 * @param expr - Expression to parse.
	 * @param part - The {@link zoranodensha.api.vehicles.part.VehParBase vehicle part} whose XML file specified this expression.
	 * @throws IllegalArgumentException If the expression was not successfully parsed.
	 */
	public Expression(String expr, VehParBase part)
	{
		expression = parse(expr, part);
		prevValue = value = expression.getValue();

		part.addExpression(this);
	}


	/**
	 * Returns whether this expression contains any (sub-)component referencing a value.
	 * 
	 * @return {@code true} if this expression contains at least one referenced value.
	 */
	public boolean getHasReference()
	{
		return expression.getHasReference();
	}

	/**
	 * Returns whether the value of this expression may be logically interpreted as {@code true}.
	 * 
	 * @return {@code true} if the evaluation resulted in a non-zero value.
	 */
	public boolean getIsTrue()
	{
		return expression.getIsTrue();
	}

	/**
	 * Returns the previous value of this expression.
	 * 
	 * @return Previous expression value.
	 */
	public double getPrevValue()
	{
		return prevValue;
	}

	/**
	 * Returns the value of this expression.
	 * 
	 * @return Expression value.
	 */
	public double getValue()
	{
		return value;
	}

	/**
	 * Select the next operator string with lowest order from the given list of expression parts.
	 * 
	 * @param expressionParts - List of expression parts to choose from.
	 * @return The next available operator string, or {@code null} if there is none left.
	 */
	private static String nextOperator(ArrayList<Object> expressionParts)
	{
		String operator = null;
		{
			for (Object obj : expressionParts)
			{
				if (obj instanceof String)
				{
					String op = (String)obj;
					if (operator == null || (Integer)OPERATORS.get(op)[1] < (Integer)OPERATORS.get(operator)[1])
					{
						operator = op;
					}
				}
			}
		}
		return operator;
	}

	/**
	 * Called by the parent {@link zoranodensha.api.vehicles.part.VehParBase vehicle part} to update this expression's value.
	 */
	public void onUpdate()
	{
		if (getHasReference())
		{
			prevValue = value;
			value = expression.getValue();
		}
	}

	/**
	 * Helper method to parse the given String expression.
	 * 
	 * @param expr - Expression to parse.
	 * @param part - The {@link zoranodensha.api.vehicles.part.VehParBase vehicle part} whose XML file specified the given expression.
	 * @return The given expression's parsed root {@link zoranodensha.api.vehicles.part.util.expr.IExpressionComponent component}.
	 * 
	 * @throws IllegalArgumentException If the expression was not successfully parsed.
	 */
	@SuppressWarnings("unchecked")
	private static IExpressionComponent parse(String expr, VehParBase part)
	{
		/* Trim the string to get rid of trailing whitespace. */
		expr = expr.trim();

		/* List of parts of this expression, including operators, sub-level expressions, numbers, or references. */
		ArrayList<Object> expressionParts = new ArrayList<Object>();

		/*
		 * Parse all operators on the expression's top level, as well as all expressions of the following level.
		 */
		int beginIndex;
		int index = 0;

		while (index < expr.length())
		{
			/* Remember beginning index. It is needed to assert progress. */
			beginIndex = index;

			/* Skip white space. */
			index = parseWhitespace(expr, index);
			if (index != beginIndex)
			{
				continue;
			}

			/* Parse brackets first. */
			index = parseBracket(expr, index, expressionParts, part);
			if (index != beginIndex)
			{
				continue;
			}

			/* Read numbers. */
			index = parseNumber(expr, index, expressionParts);
			if (index != beginIndex)
			{
				continue;
			}

			/* Read property reference. */
			index = parseReference(expr, index, expressionParts, part);
			if (index != beginIndex)
			{
				continue;
			}

			/* Read operator. */
			index = parseOperator(expr, index, expressionParts);
			if (index != beginIndex)
			{
				continue;
			}

			/* If there was no progress made, then the current character is invalid. Throw exception. */
			throw new IllegalArgumentException(String.format("Invalid character '%s' in expression '%s' at position %d!", String.valueOf(expr.charAt(index)), expr, (index + 1)));
		}

		/*
		 * If there are operators, assemble all operations from parsed operators and sub-expressions.
		 */
		for (String nextOperator = nextOperator(expressionParts); nextOperator != null; nextOperator = nextOperator(expressionParts))
		{
			int indexOfOperator = expressionParts.indexOf(nextOperator);
			switch ((Integer)OPERATORS.get(nextOperator)[2])
			{
				case 2:
					/* A minus sign may also interpreted as single-argument operator for negation. In this case, fall through into following case. */
					if (!"-".equals(nextOperator) || indexOfOperator > 0)
					{
						/* Differentiate between min() and max() function calls.. */
						if ("min".equals(nextOperator) || "max".equals(nextOperator))
						{
							if (indexOfOperator + 2 < expressionParts.size())
							{
								Object arg1 = expressionParts.get(indexOfOperator + 1);
								if (arg1 instanceof IExpressionComponent)
								{
									Object arg2 = expressionParts.get(indexOfOperator + 2);
									if (arg2 instanceof IExpressionComponent)
									{
										try
										{
											/* Retrieve operator instance and place operator at current index. Remove arguments from list. */
											IExpressionComponent[] args = new IExpressionComponent[] { (IExpressionComponent)arg1, (IExpressionComponent)arg2 };
											AOperator operator = ((Class<? extends AOperator>)OPERATORS.get(nextOperator)[0]).getConstructor(IExpressionComponent[].class).newInstance(new Object[] { args });
											expressionParts.set(indexOfOperator, operator);
											expressionParts.remove(indexOfOperator + 1);
											expressionParts.remove(indexOfOperator + 1);
											break;
										}
										catch (Exception e)
										{
											throw new IllegalArgumentException(String.format("Error while parsing operator '%s' in (sub-)expression '%s': %s", nextOperator, expr, e.getMessage()), e);
										}
									}
								}
							}
						}

						/* ..and other two-argument operators. */
						else
						{
							if (indexOfOperator + 1 < expressionParts.size() && indexOfOperator > 0)
							{
								Object arg1 = expressionParts.get(indexOfOperator - 1);
								if (arg1 instanceof IExpressionComponent)
								{
									Object arg2 = expressionParts.get(indexOfOperator + 1);
									if (arg2 instanceof IExpressionComponent)
									{
										try
										{
											/* Retrieve operator instance and place operator at current index. Remove arguments from list. */
											IExpressionComponent[] args = new IExpressionComponent[] { (IExpressionComponent)arg1, (IExpressionComponent)arg2 };
											AOperator operator = ((Class<? extends AOperator>)OPERATORS.get(nextOperator)[0]).getConstructor(IExpressionComponent[].class).newInstance(new Object[] { args });
											expressionParts.set(indexOfOperator, operator);
											expressionParts.remove(indexOfOperator + 1);
											expressionParts.remove(indexOfOperator - 1);
											break;
										}
										catch (Exception e)
										{
											throw new IllegalArgumentException(String.format("Error while parsing operator '%s' in (sub-)expression '%s': %s", nextOperator, expr, e.getMessage()), e);
										}
									}
								}
							}
						}

						/* Throw exception in case either argument couldn't be found or didn't match. */
						throw new IllegalArgumentException(String.format("Couldn't match argument for operator '%s' in (sub-)expression '%s'!", nextOperator, expr));
					}

				case 1:
					/* Handle single-argument operators here. Special case is the minus sign, which negates its argument value. */
					if (indexOfOperator + 1 < expressionParts.size())
					{
						Object arg = expressionParts.get(indexOfOperator + 1);
						if (arg instanceof IExpressionComponent)
						{
							try
							{
								/* Handle class retrieval for the minus sign operator as special case. */
								Class<? extends AOperator> clazz = (Class<? extends AOperator>)OPERATORS.get(nextOperator)[0];
								if ("-".equals(nextOperator))
								{
									clazz = OpNegate.class;
								}

								/* Retrieve operator instance and place operator at current index. Remove arguments from list. */
								IExpressionComponent[] args = new IExpressionComponent[] { (IExpressionComponent)arg };
								AOperator operator = clazz.getConstructor(IExpressionComponent[].class).newInstance(new Object[] { args });
								expressionParts.set(indexOfOperator, operator);
								expressionParts.remove(indexOfOperator + 1);
								break;
							}
							catch (Exception e)
							{
								throw new IllegalArgumentException(String.format("Error while parsing operator '%s' in (sub-)expression '%s': %s", nextOperator, expr, e.getMessage()), e);
							}
						}
					}

					/* Throw exception in case the argument couldn't be found or didn't match. */
					throw new IllegalArgumentException(String.format("Couldn't match argument for operator '%s' in (sub-)expression '%s'!", nextOperator, expr));

				default:
					throw new RuntimeException(String.format("Unknown number of arguments for operator %s encountered! This should NEVER happen!", nextOperator));
			}
		}

		/*
		 * At this point there should be only EXACTLY one item in the list remaining.
		 */
		if (expressionParts.size() != 1)
		{
			String listDump = "";
			for (Object obj : expressionParts)
			{
				listDump += String.format((listDump.isEmpty() ? "'%s'" : ", '%s'"), obj);
			}
			throw new RuntimeException(String.format("Expression was incorrectly parsed! Expression list dump: [%s]", listDump));
		}

		/* Also ensure the remaining item actually is an expression component. */
		Object obj = expressionParts.get(0);
		if (!(obj instanceof IExpressionComponent))
		{
			throw new RuntimeException(String.format("Remaining item (%s) was not an expression component. This should NOT happen!", obj));
		}

		return (IExpressionComponent)obj;
	}

	/**
	 * Try to parse a bracket from the given expression, starting at the given index.<br>
	 * Abort if unsuccessful.
	 * 
	 * @param expr - Expression to parse from.
	 * @param index - Index at which to check for a bracket.
	 * @param expressionParts - List of expression parts.
	 * @param part - The {@link zoranodensha.api.vehicles.part.VehParBase vehicle part} whose XML file specified the given (sub-)expression.
	 * @return The index after the closing bracket.
	 */
	private static int parseBracket(String expr, int index, ArrayList<Object> expressionParts, VehParBase part)
	{
		int beginIndex = index;
		int openBrackets = 0;
		char charAt;

		do
		{
			charAt = expr.charAt(index);

			if (charAt == '(')
			{
				++openBrackets;
				++index;
			}
			else if (charAt == ')')
			{
				if (openBrackets > 0)
				{
					--openBrackets;
					++index;
				}
				else
				{
					throw new IllegalArgumentException(String.format("Mismatched bracket in (sub-)expression '%s' at position %d!", expr, (index + 1)));
				}
			}
			else if (openBrackets > 0)
			{
				++index;
			}
		}
		while (index < expr.length() && openBrackets > 0);

		/* If begin and current index differ, we found something. */
		if (beginIndex != index)
		{
			String s = expr.substring(beginIndex + 1, index - 1);
			if (s.contains(","))
			{
				/* If there is a comma in-between, just assume that there was either a minimum or maximum function call before. */
				expressionParts.add(parse(s.split(",")[0], part));
				expressionParts.add(parse(s.split(",")[1], part));
			}
			else
			{
				expressionParts.add(parse(s, part));
			}
		}

		return index;
	}

	/**
	 * Try to parse a number from the given expression, starting at the given index.<br>
	 * Abort if unsuccessful.
	 * 
	 * @param expr - Expression to parse from.
	 * @param index - Index at which to check for a number.
	 * @param expressionParts - List of expression parts.
	 * @return The index after the number's last digit.
	 */
	private static int parseNumber(String expr, int index, ArrayList<Object> expressionParts)
	{
		/* Special case: Check whether we encounter PI. */
		if ((index + 2) <= expr.length() && "PI".equalsIgnoreCase(expr.substring(index, index + 2)))
		{
			expressionParts.add(new AtomNumberPI());
			return (index + 2);
		}

		/* Otherwise read numbers with digits and dots. */
		int beginIndex = index;
		char charAt;

		do
		{
			charAt = expr.charAt(index);
			if (charAt == '.')
			{
				if (!expr.substring(beginIndex, index).contains("."))
				{
					++index;
				}
				else
				{
					throw new IllegalArgumentException(String.format("Malformatted number (duplicate dot) in (sub-)expression '%s' at position %d!", expr, (index + 1)));
				}
			}
			else if (Character.toString(charAt).matches(PATTERN_DIGITS))
			{
				++index;
			}
			else
			{
				break;
			}
		}
		while (index < expr.length());

		/* If begin and current index differ, we found something. */
		if (beginIndex != index)
		{
			String number = expr.substring(beginIndex, index);
			try
			{
				expressionParts.add(new AtomNumber(number));
			}
			catch (NumberFormatException e)
			{
				throw new IllegalArgumentException(String.format("Malformatted number '%s'!", number), e);
			}
		}

		return index;
	}

	/**
	 * Try to parse an operator from the given expression, starting at the given index.<br>
	 * Abort if unsuccessful.
	 * 
	 * @param expr - Expression to parse from.
	 * @param index - Index at which to check for an operator.
	 * @param expressionParts - List of expression parts.
	 * @return The index after the operator.
	 */
	private static int parseOperator(String expr, int index, ArrayList<Object> expressionParts)
	{
		for (String op : OPERATORS.keySet())
		{
			if (expr.startsWith(op, index))
			{
				expressionParts.add(op);
				index += op.length();
				break;
			}
		}
		return index;
	}

	/**
	 * Try to parse a property reference from the given expression, starting at the given index.<br>
	 * Abort if unsuccessful.
	 * 
	 * @param expr - Expression to parse from.
	 * @param index - Index at which to check for a reference.
	 * @param expressionParts - List of expression parts.
	 * @param part - The {@link zoranodensha.api.vehicles.part.VehParBase vehicle part} whose XML file specified the given (sub-)expression.
	 * @return The index after the reference's last letter.
	 */
	private static int parseReference(String expr, int index, ArrayList<Object> expressionParts, VehParBase part)
	{
		boolean isReading = false;
		int beginIndex = index;
		char charAt;

		do
		{
			charAt = expr.charAt(index);
			if (charAt == '$')
			{
				if (!isReading)
				{
					isReading = true;
					++index;
				}
				else
				{
					throw new IllegalArgumentException(String.format("Excess property reference token in (sub-)expression '%s' at position %d!", expr, (index + 1)));
				}
			}
			else if (isReading && charAt == '.')
			{
				if (expr.substring(beginIndex, index).contains("."))
				{
					throw new IllegalArgumentException(String.format("Duplicate property reference in (sub-)expression '%s' at position %d!", expr, (index + 1)));
				}
				else if (expr.substring(beginIndex, index).isEmpty())
				{
					throw new IllegalArgumentException(String.format("No part type specified for property reference in (sub-)expression '%s' at position %d!", expr, (index + 1)));
				}
				else
				{
					++index;
				}
			}
			else if (isReading && Character.toString(charAt).matches(PATTERN_ALPHABET))
			{
				++index;
			}
			else
			{
				break;
			}
		}
		while (index < expr.length());

		/* If begin and current index differ, we found something. */
		if (beginIndex != index)
		{
			String name = expr.substring(beginIndex, index);
			AReference reference = AReference.getForName(name, part);

			if (reference != null)
			{
				expressionParts.add(reference);
			}
			else
			{
				throw new NullPointerException(String.format("Couldn't reference property '%s'!", name));
			}
		}

		return index;
	}

	/**
	 * Skip the white space starting at the given index.
	 * 
	 * @param expr - Expression to work with.
	 * @param index - Index of the first white space character.
	 * @return The index of the last white space character.
	 */
	private static int parseWhitespace(String expr, int index)
	{
		while (index < expr.length() && Character.isWhitespace(expr.charAt(index)))
		{
			++index;
		}
		return index;
	}

	@Override
	public String toString()
	{
		return String.format("[%s] %s", getClass().getSimpleName(), expression);
	}
}