package zoranodensha.api.vehicles.part.util.expr.op;

import zoranodensha.api.vehicles.part.util.expr.IExpressionComponent;

public class OpSqrt extends AOperator
{
	public OpSqrt(IExpressionComponent[] arguments)
	{
		super(arguments);
	}

	@Override
	protected double evaluate()
	{
		return Math.sqrt(arguments[0].getValue());
	}

	@Override
	public String toString()
	{
		return String.format("sqrt(%s)", arguments[0]);
	}
}