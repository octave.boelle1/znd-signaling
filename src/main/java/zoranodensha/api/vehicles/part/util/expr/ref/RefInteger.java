package zoranodensha.api.vehicles.part.util.expr.ref;

import zoranodensha.api.vehicles.part.property.PropInteger;



/**
 * Reference to a {@link zoranodensha.api.vehicles.part.property.PropInteger Integer property}.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class RefInteger extends AReference
{
	/** The referenced {@link zoranodensha.api.vehicles.part.property.PropInteger property}. */
	private final PropInteger reference;



	public RefInteger(PropInteger reference)
	{
		this.reference = reference;
	}

	@Override
	public double getValue()
	{
		return reference.get();
	}
}