package zoranodensha.api.vehicles.part.util;

import java.util.List;

import javax.annotation.Nullable;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.APartType;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.util.OrderedTypesList.ITypesSelector;
import zoranodensha.api.vehicles.util.PositionStackEntry;
import zoranodensha.api.vehicles.util.TrackObj;
import zoranodensha.api.vehicles.util.VehicleHelper;



/**
 * Class containing various helper methods related to vehicle parts.
 */
public final class PartHelper
{
	/**
	 * Calculates the brightness at the given vehicle part's position.<br>
	 * <i>Client-side only.</i>
	 * 
	 * @param part - {@link zoranodensha.api.vehicles.part.VehParBase Vehicle part} whose brightness shall be calculated.
	 * @return The given part's brightness.
	 */
	@SideOnly(Side.CLIENT)
	public static final int getBrightness(VehParBase part)
	{
		Vec3 pos = getPosition(part);
		int brightness = 0;
		int x = MathHelper.floor_double(pos.xCoord);
		int z = MathHelper.floor_double(pos.zCoord);

		if (part.getTrain().worldObj.blockExists(x, 0, z))
		{
			brightness = part.getTrain().worldObj.getLightBrightnessForSkyBlocks(x, MathHelper.floor_double(pos.yCoord), z, 0);
		}

		return brightness;
	}

	/**
	 * Returns a position stack entry holding the last tick's bogie data. If the last tick's entry was {@code null}, creates a new entry with current position and rotation data.
	 *
	 * @param bogie - {@link zoranodensha.api.vehicles.part.type.PartTypeBogie Bogie} whose last position is requested.
	 * @return {@link zoranodensha.api.vehicles.util.PositionStackEntry PositionStackEntry} holding the last tick's bogie position and rotation.
	 */
	public static final PositionStackEntry getLastBogiePSE(PartTypeBogie bogie)
	{
		return new PositionStackEntry(bogie.getPrevPosX(), bogie.getPrevPosY(), bogie.getPrevPosZ(), bogie.getRotationRoll(), bogie.getRotationYaw(), bogie.getRotationPitch());
	}

	/**
	 * Locates the Nth part in the given train, starting from the train's local front.
	 *
	 * @return The {@link zoranodensha.api.vehicles.part.type.APartType part type} at the Nth position, or {@code null} if none was found.
	 */
	public static final <T extends APartType> T getNthPartType(int index, Train train, ITypesSelector<T> selector)
	{
		/* If the index is smaller than or equal to zero, it's invalid. */
		if (index <= 0)
		{
			return null;
		}

		/* If the index exceeds the parts list's size, return null. */
		ConcatenatedTypesList<T> current = train.getVehiclePartTypes(selector);
		if (current.size() < index)
		{
			return null;
		}

		return current.get(index - 1);
	}

	/**
	 * Calculates the position of the given vehicle part from its bounding box and returns it as a vector.
	 *
	 * @param part - The {@link zoranodensha.api.vehicles.part.VehParBase part} whose position is to be calculated. May be {@code null}.
	 * @return The part's position as {@link net.minecraft.util.Vec3 Vec3}, or the origin vector.
	 */
	public static final Vec3 getPosition(@Nullable VehParBase part)
	{
		if (part != null)
		{
			/* We assume that the part has a bounding box. This should ALWAYS apply. */
			return VehicleHelper.getPosition(part.getBoundingBox());
		}
		return Vec3.createVectorHelper(0.0D, 0.0D, 0.0D);
	}

	/**
	 * Find and return the track the given bogie sits on.
	 *
	 * @param bogie - The {@link zoranodensha.api.vehicles.part.type.PartTypeBogie bogie} searching for its respective track.
	 * @return A {@link zoranodensha.api.vehicles.util.TrackObj reference} of the track this bogie sits on, or {@code null} if this bogie derailed.
	 */
	public static final TrackObj getTrackBelow(PartTypeBogie bogie)
	{
		Train vehicle = bogie.getParent().getTrain();
		if (vehicle != null)
		{
			int x = MathHelper.floor_double(bogie.getPosX());
			int y = MathHelper.floor_double(bogie.getPosY());
			int z = MathHelper.floor_double(bogie.getPosZ());
			TrackObj track;

			for (int i = 0; i <= 1; ++i)
			{
				track = TrackObj.isTrack(vehicle.worldObj, x, y - i, z, bogie);
				if (track != null)
				{
					return track;
				}
			}
		}
		return null;
	}

	/**
	 * Retrieves the view vector for the given vehicle part.
	 * 
	 * @param part - {@link zoranodensha.api.vehicles.part.VehParBase vehicle part} whose view vector shall be calculated.
	 * @param localView - {@code true} if the view vector shall be calculated locally (i.e. without section rotation).
	 * @return A view {@link net.minecraft.util.Vec3 vector} pointing in the part's view direction.
	 */
	public static final Vec3 getViewVec(VehParBase part, boolean localView)
	{
		/*
		 * Create default vector.
		 */
		Vec3 vec = Vec3.createVectorHelper(1.0D, 0.0D, 0.0D);
		float roll = part.getRotation()[0];
		float yaw = part.getRotation()[1];
		float pitch = part.getRotation()[2];

		/*
		 * If parent vehicle exists, find the section this part belongs to and add its rotation.
		 */
		if (!localView && part.getTrain() != null)
		{
			VehicleSection section = part.getSection();
			if (section != null)
			{
				roll += section.getRotationRoll();
				yaw += section.getRotationYaw();
				pitch += section.getRotationPitch();
			}
		}

		/* Apply rotation. */
		if (pitch != 0.0F)
		{
			vec.rotateAroundZ(-pitch * VehicleHelper.RAD_FACTOR);
		}
		if (yaw != 0.0F)
		{
			vec.rotateAroundY(-yaw * VehicleHelper.RAD_FACTOR);
		}
		if (roll != 0.0F)
		{
			vec.rotateAroundX(-roll * VehicleHelper.RAD_FACTOR);
		}

		return vec;
	}

	/**
	 * Runs a ray trace through this vehicle with the given arguments.
	 *
	 * @param hitCandidates - List that will be filled with possibly hit objects. Will be ignored if {@code null}.
	 * @param pos - The world position of the ray start.
	 * @param ray - The world position of the ray end.
	 * @param selectedPart - The currently selected part. May be {@code null}.
	 * @param parts - A list of {@link zoranodensha.api.vehicles.part.VehParBase vehicle parts} to ray trace through.
	 * @return The part that was closest to the given position.
	 */
	public static VehParBase rayTraceParts(@Nullable List<VehParBase> hitCandidates, Vec3 pos, Vec3 ray, @Nullable VehParBase selectedPart, List<VehParBase> parts)
	{
		MovingObjectPosition movObjPos;
		AxisAlignedBB aabb;
		double d0;
		double d1 = Double.MAX_VALUE;

		for (VehParBase part : parts)
		{
			aabb = part.getBoundingBox();
			if (aabb.isVecInside(pos))
			{
				if (hitCandidates != null)
				{
					hitCandidates.add(part);
				}

				d0 = pos.distanceTo(PartHelper.getPosition(part));

				if (d0 < d1 || d1 == Double.MAX_VALUE)
				{
					selectedPart = part;
					d1 = d0;
				}
			}
			else if ((movObjPos = aabb.calculateIntercept(pos, ray)) != null)
			{
				if (hitCandidates != null)
				{
					hitCandidates.add(part);
				}

				d0 = pos.distanceTo(movObjPos.hitVec);

				if (d0 < d1 || d1 == Double.MAX_VALUE)
				{
					selectedPart = part;
					d1 = d0;
				}
				else if (d0 == d1)
				{
					/*
					 * If distances are equal, compare part position and choose the closest part.
					 */
					double d2 = movObjPos.hitVec.distanceTo(PartHelper.getPosition(part));
					double d3 = movObjPos.hitVec.distanceTo(PartHelper.getPosition(selectedPart));

					if (d2 < d3)
					{
						selectedPart = part;
					}
				}
			}
		}
		return selectedPart;
	}
}