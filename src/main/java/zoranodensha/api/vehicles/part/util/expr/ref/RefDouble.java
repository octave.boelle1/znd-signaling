package zoranodensha.api.vehicles.part.util.expr.ref;

import zoranodensha.api.vehicles.part.property.PropDouble;



/**
 * Reference to a {@link zoranodensha.api.vehicles.part.property.PropDouble Double property}.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class RefDouble extends AReference
{
	/** The referenced {@link zoranodensha.api.vehicles.part.property.PropDouble property}. */
	private final PropDouble reference;



	public RefDouble(PropDouble reference)
	{
		this.reference = reference;
	}

	@Override
	public double getValue()
	{
		return reference.get();
	}
}