package zoranodensha.api.vehicles.part.xml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.util.ResourceLocation;
import zoranodensha.api.util.APILogger;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.VehParBaseRenderer;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.IXMLProperty;
import zoranodensha.api.vehicles.part.type.APartType;



/**
 * Utility class used to import XML-based {@link zoranodensha.api.vehicles.part.VehParBase vehicle parts}.<br>
 * <br>
 * All XML-based vehicle parts are placed in a ZIP-file containing three components per part;<br>
 * - A model (which may be parsed by Forge's {@link net.minecraftforge.client.model.AdvancedModelLoader AdvancedModelLoader})<br>
 * - Its respective texture<br>
 * - The XML-file describing the part's properties.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class XMLPartFactory
{
	/** Static XML-file parser. */
	private static final XMLParser XML_PARSER = new XMLParser();
	/** Default name of part nodes in XML documents. This node must be the document's root node. */
	private static final String XML_PART_NODE_NAME = "part";
	/** Node name of {@link zoranodensha.api.vehicles.part.type.APartType part types}. The exact type is specified as imminent {@code name} node. */
	private static final String XML_TYPE_NODE_NAME = "type";
	/**
	 * Name of nodes declaring a {@link zoranodensha.api.vehicles.part.property.IPartProperty property}.
	 * For custom properties, these tags must carry exactly two children called {@code name} and {@code type}, respectively.
	 */
	private static final String XML_TYPE_PROP = "prop";



	/**
	 * Helper method to determine whether we are in a client environment.
	 * 
	 * @return {@code true} if this is a client environment.
	 */
	private static boolean getIsClient()
	{
		return Side.CLIENT.equals(FMLCommonHandler.instance().getEffectiveSide());
	}

	/**
	 * Parses all XML-based vehicle parts from the given ZIP-file.<br>
	 * Also loads their respective models and textures into the parts' {@link zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer renderers}.
	 * 
	 * @param file - {@link java.io.File file} to parse the part from.
	 * @return The array list of parsed {@link zoranodensha.api.vehicles.part.VehParBase vehicle parts}, or {@code null} if the specified file wasn't valid.
	 */
	public static ArrayList<VehParBase> parse(File file)
	{
		/*
		 * Ensure the file is valid.
		 */
		if (!file.isFile() || !file.exists())
		{
			return null;
		}

		/*
		 * Try to convert into ZipFile type.
		 */
		ZipFile zipFile = null;
		try
		{
			try
			{
				zipFile = new ZipFile(file);
			}
			catch (Exception e)
			{
				return null;
			}

			/* Log attempt of parsing ZIP-file. */
			APILogger.debug(XMLPartFactory.class, String.format("Attempting to parse ZIP-file '%s'.", file.getPath()));

			/*
			 * Read parts and language file(s) from file.
			 */
			final ArrayList<VehParBase> partsList = new ArrayList<VehParBase>();
			{
				Enumeration<? extends ZipEntry> entries = zipFile.entries();
				while (entries.hasMoreElements())
				{
					/* Current ZIP entry. Skip directories. */
					final ZipEntry entry = entries.nextElement();
					if (entry.isDirectory())
					{
						continue;
					}

					/*
					 * Parse vehicle part if the current entry is an XML file.
					 */
					final String entryPath = entry.getName();
					if (entryPath.endsWith(".xml"))
					{
						/* Log parse attempt. */
						APILogger.debug(XMLPartFactory.class, String.format("Attempting to parse XML file '%s'.", entry.getName()));

						/* Parse part from entry. */
						try
						{
							VehParBase part = parsePart(entry, zipFile);
							if (part != null)
							{
								partsList.add(part);
							}
						}
						catch (Exception e)
						{
							APILogger.warn(XMLPartFactory.class, "Caught exception while parsing vehicle part from ZIP:");
							APILogger.catching(e);
						}
					}

					/*
					 * Parse language file location if this is a LANG file.
					 */
					else if (entryPath.endsWith(".lang") && getIsClient())
					{
						ResourceLocation langFileLocation = new ResourceLocation(XMLResourcePack.INSTANCE.getPackName(), zipFile.getName() + ":" + entry.getName());
						String lang = (entryPath.contains("/")) ? entryPath.substring(entryPath.lastIndexOf("/") + 1) : entryPath;
						lang = lang.substring(0, lang.indexOf(".lang"));

						XMLResourcePack.INSTANCE.addLanguageFile(lang, langFileLocation);
					}
				}
			}

			/* Log number of parsed parts. */
			APILogger.debug(XMLPartFactory.class, String.format("Read %d XML part(s) from ZIP-file.", partsList.size()));

			return partsList;
		}
		finally
		{
			/* Try to close the ZIP file */
			if (zipFile != null)
			{
				try
				{
					zipFile.close();
				}
				catch (IOException e)
				{
					;
				}
			}
		}
	}

	/**
	 * Parses the given file directory for any XML-based vehicle parts.
	 * 
	 * @param dir - {@link java.io.File File} directory to parse.
	 * @return {@link java.util.ArrayList Array list} holding all parsed parts.
	 */
	public static ArrayList<VehParBase> parseDirectory(File dir)
	{
		ArrayList<VehParBase> parsedParts = new ArrayList<VehParBase>();
		{
			if (dir.isDirectory())
			{
				File[] files = dir.listFiles();
				if (files != null && files.length > 0)
				{
					for (File file : files)
					{
						/* Ensure the file even exists. */
						if (!file.exists())
						{
							continue;
						}

						/* If the file is a directory, investigate directory recursively. */
						if (file.isDirectory())
						{
							parsedParts.addAll(parseDirectory(file));
						}

						/* Otherwise, try to parse if file has ZIP suffix. */
						else if (file.getName().endsWith(".zip"))
						{
							ArrayList<VehParBase> partsInFile = parse(file);
							if (partsInFile != null && !partsInFile.isEmpty())
							{
								parsedParts.addAll(partsInFile);
							}
						}
					}
				}
			}
		}
		return parsedParts;
	}

	/**
	 * Try to read an XML-based vehicle part from the given entry in the given ZIP-file.
	 * 
	 * @param zipEntry - {@link java.util.zip.ZipEntry ZIP-entry} to parse.
	 * @param zipFile - The entry's respective {@link java.util.zip.ZipFile ZIP-file}.
	 * @return The parsed {@link zoranodensha.api.vehicles.part.VehParBase vehicle part}, or {@code null} if none was found.
	 * 
	 * @throws Exception If either IO operation fails.
	 */
	protected static VehParBase parsePart(ZipEntry zipEntry, ZipFile zipFile) throws Exception
	{
		/*
		 * Parse part as XML node tree.
		 */
		XMLTreeNode xmlRoot = XML_PARSER.parseXML(zipEntry, zipFile);
		if (xmlRoot == null)
		{
			APILogger.warn(XMLPartFactory.class, "Dropping XML file that couldn't be parsed.");
			return null;
		}
		else if (!XML_PART_NODE_NAME.equals(xmlRoot.getName()))
		{
			APILogger.warn(XMLPartFactory.class, String.format("Dropping XML file that did not contain root tag '%s'.", XML_PART_NODE_NAME));
			return null;
		}

		/*
		 * Load all nodes which are required by default.
		 */
		HashMap<EDefaultTags, XMLTreeNode> defaultNodes = new HashMap<EDefaultTags, XMLTreeNode>();
		for (EDefaultTags defaultTag : EDefaultTags.values())
		{
			XMLTreeNode node = xmlRoot.getChildByName(defaultTag.tagName);
			if (defaultTag.getIsNodeValid(node))
			{
				defaultNodes.put(defaultTag, node);
			}
			else
			{
				APILogger.warn(XMLPartFactory.class, String.format("Dropping XML file '%s' with missing or invalid tag '%s'.", zipEntry.getName(), defaultTag.tagName));
				return null;
			}
		}

		/*
		 * On client, locate and read resources (texture and model).
		 */
		ResourceLocation modelLocation = null;
		ResourceLocation textureLocation = null;

		if (getIsClient())
		{
			String modelPath = defaultNodes.get(EDefaultTags.MODEL_PATH).getValue();
			String texturePath = defaultNodes.get(EDefaultTags.TEXTURE_PATH).getValue();

			modelLocation = XMLResourcePack.INSTANCE.loadResource(zipFile, modelPath);
			if (modelLocation == null || !XMLResourcePack.INSTANCE.resourceExists(modelLocation))
			{
				APILogger.warn(XMLPartFactory.class, String.format("Dropping XML file '%s' whose model couldn't be loaded from '%s'.", zipEntry.getName(), modelPath));
				return null;
			}

			textureLocation = XMLResourcePack.INSTANCE.loadResource(zipFile, defaultNodes.get(EDefaultTags.TEXTURE_PATH).getValue());
			if (textureLocation == null || !XMLResourcePack.INSTANCE.resourceExists(textureLocation))
			{
				APILogger.warn(XMLPartFactory.class, String.format("Dropping XML file '%s' whose texture couldn't be loaded from '%s'.", zipEntry.getName(), texturePath));
				return null;
			}
		}

		/*
		 * Convert node tree and resources into an actual vehicle part.
		 */

		/* Create basic vehicle part from parsed default properties. */
		//@formatter:off
		VehParBase part = new VehParBase(defaultNodes.get(EDefaultTags.NAME).getValue(),
		                                 (float)defaultNodes.get(EDefaultTags.WIDTH).getValueAsDouble(),
		                                 (float)defaultNodes.get(EDefaultTags.HEIGHT).getValueAsDouble(),
		                                 (float)defaultNodes.get(EDefaultTags.MASS).getValueAsDouble());
		//@formatter:on

		/* Parse recipe matrix. */
		ArrayList<XMLTreeNode> itemNodes = defaultNodes.get(EDefaultTags.RECIPE).getChildren();
		{
			String[][] recipe = new String[3][3];
			boolean isEmpty = true;

			for (int i = 0; i < 3; ++i)
			{
				for (int j = 0; j < 3; ++j)
				{
					XMLTreeNode itemNode = itemNodes.get(i * 3 + j);
					if (!itemNode.getValue().isEmpty())
					{
						recipe[i][j] = itemNode.getValue();
						isEmpty &= recipe[i][j].isEmpty();
					}
				}
			}

			if (isEmpty)
			{
				APILogger.warn(XMLPartFactory.class, String.format("Dropping XML file '%s' which specified an empty recipe.", zipEntry.getName()));
				return null;
			}

			part.setRecipe(recipe);
		}

		/* Iterate through all direct children of the root node for potential part types. */
		for (XMLTreeNode childNode : xmlRoot.getChildren())
		{
			if (XML_TYPE_NODE_NAME.equals(childNode.getName()))
			{
				parsePartType(childNode, part, zipFile);
			}
		}

		/* Also parse any custom properties. */
		for (XMLTreeNode propNode : xmlRoot.getChildren())
		{
			if (XML_TYPE_PROP.equals(propNode.getName()))
			{
				XMLPropertyRegistry.parseCustomProperty(propNode, part);
			}
		}

		/* On client side create renderer file and put resources. */
		if (getIsClient())
		{
			parsePart_Renderer(part, modelLocation, textureLocation, defaultNodes.get(EDefaultTags.RENDERER));
		}

		return part;
	}

	/**
	 * Parse and assign a renderer file for the given part from supplied data.
	 * <i>Client-side only.</i>
	 * 
	 * @param part - {@link zoranodensha.api.vehicles.part.VehParBase Vehicle part} to parse the renderer for.
	 * @param model - {@link net.minecraft.util.ResourceLocation Resource location} pointing to the renderer's model file.
	 * @param texture - Resource location pointing to the renderer's texture file.
	 * @param rendererNode - Root {@link zoranodensha.api.vehicles.part.xml.XMLTreeNode node} of the part's render data.
	 */
	@SideOnly(Side.CLIENT)
	protected static void parsePart_Renderer(VehParBase part, ResourceLocation model, ResourceLocation texture, XMLTreeNode rendererNode)
	{
		VehParBaseRenderer renderer = new VehParBaseRenderer(part, model, texture);
		part.setRenderer(renderer);
		renderer.parseRenderDataFromNode(rendererNode);
	}

	/**
	 * Parse the given tree node for a {@link zoranodensha.api.vehicles.part.type.APartType part type}.<br>
	 * The parsed part type will be automatically registered to the supplied parent part.
	 * If any issues occur while parsing <i>fields</i> of the respective part type, the values will be ignored.
	 * 
	 * @param typeNode - The part type's root {@link zoranodensha.api.vehicles.part.xml.XMLTreeNode node} to parse.
	 * @param parent - Parent {@link zoranodensha.api.vehicles.part.VehParBase part} of the part type to parse.
	 * @param zipFile - {@link java.util.zip.ZipFile ZIP-file} containing the part we're parsing. Used for resource references.
	 */
	public static void parsePartType(XMLTreeNode typeNode, VehParBase parent, ZipFile zipFile)
	{
		/*
		 * Retrieve type name from its respective node.
		 */
		XMLTreeNode typeNameNode = typeNode.getChildByName(EDefaultTags.NAME.tagName);
		if (typeNameNode == null)
		{
			APILogger.warn(XMLPartFactory.class, "Dropping vehicle part type without name.");
			return;
		}

		/*
		 * Search for a suitable part type from all registered types.
		 */
		String typeName = typeNameNode.getValue();
		Class<? extends APartType> typeClass = null;

		for (Class<? extends APartType> clazz : APartType.getRegisteredPartTypes())
		{
			if (typeName.equals(clazz.getSimpleName()))
			{
				typeClass = clazz;
				break;
			}
		}

		if (typeClass == null)
		{
			APILogger.warn(XMLPartFactory.class, String.format("Dropping vehicle part type '%s' which doesn't exist.", typeName));
			return;
		}

		/*
		 * Instantiate part type from its class.
		 */
		APartType type;
		{
			try
			{
				type = typeClass.getDeclaredConstructor(VehParBase.class).newInstance(parent);
			}
			catch (Exception e)
			{
				APILogger.warn(XMLPartFactory.class, String.format("Exception while creating part type from '%s': %s", typeClass.getName(), e.getMessage()));
				APILogger.catching(e);
				return;
			}
		}

		/*
		 * Parse property defaults from XML nodes into type properties.
		 */
		for (XMLTreeNode propNode : typeNode.getChildren())
		{
			/* Skip if this isn't a property node. */
			if (!XML_TYPE_PROP.equals(propNode.getName()))
			{
				continue;
			}

			/* Retrieve the name node of this property node. */
			XMLTreeNode propNameNode = propNode.getChildByName(EDefaultTags.NAME.tagName);
			if (propNameNode == null || propNameNode.getValue().isEmpty())
			{
				APILogger.warn(XMLPartFactory.class, String.format("Skipping unnamed property with value '%s'.", propNode.getValue()));
				continue;
			}

			/* Get property from part type for respective property name. */
			String propName = propNameNode.getValue();
			IXMLProperty<?> prop = null;

			for (IPartProperty<?> property : type.getProperties())
			{
				if (property instanceof IXMLProperty && propName.equals(property.getName()))
				{
					prop = (IXMLProperty<?>)property;
					break;
				}
			}

			if (prop == null)
			{
				APILogger.warn(XMLPartFactory.class, String.format("Unable to retrieve property '%s' from type '%s'.", propName, type.getName()));
				continue;
			}

			/* Assign parsed value to property. */
			String propValue = propNode.getValue();
			if (!prop.parse(propValue, zipFile))
			{
				APILogger.info(XMLPartFactory.class, String.format("Property '%s' of type '%s' didn't accept value '%s'.", propName, prop.getClass().getSimpleName(), propValue));
			}
		}
	}
}