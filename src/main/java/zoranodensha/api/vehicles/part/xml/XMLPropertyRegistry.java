package zoranodensha.api.vehicles.part.xml;

import java.lang.reflect.Constructor;
import java.util.ArrayList;

import zoranodensha.api.util.APILogger;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.IXMLProperty;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.property.PropFloat.PropFloatBounded;
import zoranodensha.api.vehicles.part.property.PropInteger;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.property.PropString;
import zoranodensha.api.vehicles.part.type.cab.PropButton;
import zoranodensha.api.vehicles.part.type.cab.PropButton.PropButtonLock;



/**
 * Registry for custom part properties which may be independently defined in part XML files.
 * 
 * @see zoranodensha.api.vehicles.part.property.APartProperty APartProperty
 * 
 * @author Leshuwa Kaiheiwa
 */
public class XMLPropertyRegistry
{
	/** All registered properties. */
	private static final ArrayList<RegistryEntry<?>> REGISTRY_ENTRIES = new ArrayList<RegistryEntry<?>>();
	static
	{
		/*
		 * Boolean type
		 */
		register(boolean.class, PropBoolean.class, null, null, "bool", "boolean");

		/*
		 * Button type
		 */
		register(boolean.class, PropButton.class, null, null, "button");

		/*
		 * Color type
		 */
		register(ColorRGBA.class, PropColor.class, null, null, "color", "colour");

		/*
		 * Float type (bounded)
		 */
		register(float.class, PropFloat.class, PropFloatBounded.class, new Parser<Float>()
		{
			@Override
			public Float max()
			{
				return Float.MAX_VALUE;
			}

			@Override
			public Float min()
			{
				return -Float.MAX_VALUE;
			}

			@Override
			public Float parse(String s)
			{
				try
				{
					return Float.parseFloat(s);
				}
				catch (Exception e)
				{
					return null;
				}
			}
		}, "float");

		/*
		 * Integer type (bounded)
		 */
		register(int.class, PropInteger.class, PropIntegerBounded.class, new Parser<Integer>()
		{
			@Override
			public Integer max()
			{
				return Integer.MAX_VALUE;
			}

			@Override
			public Integer min()
			{
				return Integer.MIN_VALUE;
			}

			@Override
			public Integer parse(String s)
			{
				try
				{
					return Integer.parseInt(s);
				}
				catch (Exception e)
				{
					return null;
				}
			}
		}, "int", "integer");
		
		/*
		 * Lever type (actually just a button which remains in its position)
		 */
		register(boolean.class, PropButtonLock.class, null, null, "lever");

		/*
		 * String type
		 */
		register(String.class, PropString.class, null, null, "string");
	}



	/**
	 * Parse the given XML node for a custom property definition.
	 * 
	 * @param propNode - XML {@link zoranodensha.api.vehicles.part.xml.XMLTreeNode node} to parse for a custom property.
	 * @param part - The currently parsed {@link zoranodensha.api.vehicles.part.VehParBase vehicle part}.
	 */
	public static void parseCustomProperty(XMLTreeNode propNode, VehParBase part)
	{
		/* Retrieve name node. */
		XMLTreeNode propNameNode = propNode.getChildByName(EDefaultTags.NAME.tagName);
		if (propNameNode == null || propNameNode.getValue().isEmpty())
		{
			APILogger.warn(XMLPropertyRegistry.class, String.format("Skipping unnamed custom property with value '%s'.", propNode.getValue()));
			return;
		}

		/* Retrieve type node. */
		XMLTreeNode propTypeNode = propNode.getChildByName("type");
		if (propTypeNode == null || propTypeNode.getValue().isEmpty())
		{
			APILogger.warn(XMLPropertyRegistry.class, String.format("Skipping custom property '%s' with value '%s' without type declaration.", propNameNode.getValue(), propNode.getValue()));
			return;
		}

		/* Try to retrieve optional bounds definitions. */
		XMLTreeNode maxBoundNode = propNode.getChildByName("max");
		XMLTreeNode minBoundNode = propNode.getChildByName("min");

		/* Instantiate property from given information. */
		for (RegistryEntry<?> registryEntry : REGISTRY_ENTRIES)
		{
			if (!registryEntry.typeName.equalsIgnoreCase(propTypeNode.getValue()))
			{
				continue;
			}

			IXMLProperty<?> property;
			try
			{
				if (registryEntry.propertyConstructorBounded != null && registryEntry.propertyParser != null && (maxBoundNode != null || minBoundNode != null))
				{
					Object min = (minBoundNode != null) ? registryEntry.propertyParser.parse(minBoundNode.getValue()) : null;
					if (min == null)
					{
						min = registryEntry.propertyParser.min();
					}

					Object max = (maxBoundNode != null) ? registryEntry.propertyParser.parse(maxBoundNode.getValue()) : null;
					if (max == null)
					{
						max = registryEntry.propertyParser.max();
					}

					property = registryEntry.propertyConstructorBounded.newInstance(part, min, max, propNameNode.getValue());
				}
				else
				{
					property = registryEntry.propertyConstructor.newInstance(part, propNameNode.getValue());
				}

				part.addPropertyFromXML(property);
			}
			catch (Exception e)
			{
				APILogger.warn(XMLPropertyRegistry.class, String.format("Caught exception while trying to create custom part property: %s", e.getMessage()));
				APILogger.catching(e);
				return;
			}

			XMLTreeNode isConfigurableNode = propNode.getChildByName("canEdit");
			if (isConfigurableNode != null)
			{
				if (Boolean.parseBoolean(isConfigurableNode.getValue()))
				{
					property.setConfigurable();
				}
				else
				{
					APILogger.info(XMLPropertyRegistry.class, String.format("Value '%s' has no effect on %s node.", isConfigurableNode.getValue(), "canEdit"));
				}
			}

			XMLTreeNode syncNode = propNode.getChildByName("syncTo");
			if (syncNode != null)
			{
				ESyncDir syncDir = null;
				for (ESyncDir eSyncDir : ESyncDir.values())
				{
					if (eSyncDir.toString().equals(syncNode.getValue()))
					{
						syncDir = eSyncDir;
						break;
					}
				}

				if (syncDir != null)
				{
					property.setSyncDir(syncDir);
				}
				else
				{
					APILogger.info(XMLPropertyRegistry.class, String.format("Invalid synchronisation node specified: '%s'", syncNode.getValue()));
				}
			}

			XMLTreeNode isItemTagNode = propNode.getChildByName("noItemSave");
			if (isItemTagNode != null)
			{
				if (Boolean.parseBoolean(isItemTagNode.getValue()))
				{
					property.setValidTagCalls(EValidTagCalls.PACKET_SAVE);
				}
				else
				{
					APILogger.info(XMLPropertyRegistry.class, String.format("Value '%s' has no effect on %s node.", isItemTagNode.getValue(), "noItemSave"));
				}
			}

			property.set(propNode.getValue());
			return;
		}
	}

	/**
	 * Register the given property class for given type name, optionally with a paired bounded property type.
	 * 
	 * @param type - Class type of the (primitive) value wrapped by the registered property.
	 * @param clazz - Class of the {@link zoranodensha.api.vehicles.part.property.IPartProperty property} type to register.
	 * @param clazzBounded - Class of the registered property's bounded version. May be {@code null}.
	 * @param parser - Used to parse property bounds where applicable. May be {@code null}.
	 * @param typeNames - Name(s) of the type, will be required as value for the XML tag's {@code type} node. <i>Case insensitive.</i>
	 */
	public static final <T> void register(Class<?> type, Class<? extends IXMLProperty<T>> clazz, Class<? extends IXMLProperty<T>> clazzBounded, Parser<T> parser, String... typeNames)
	{
		for (String typeName : typeNames)
		{
			try
			{
				RegistryEntry<T> registryEntry = new RegistryEntry<T>(typeName, type, clazz, clazzBounded, parser);
				if (registryEntry.propertyConstructor == null)
				{
					throw new NullPointerException("Could not retrieve specified property's default constructor!");
				}
				REGISTRY_ENTRIES.add(registryEntry);
			}
			catch (Exception e)
			{
				APILogger.warn(XMLPropertyRegistry.class, String.format("Catching exception while trying to register a property with name '%s' and class '%s':", typeName, clazz));
				APILogger.catching(e);
			}
		}
	}



	private static class RegistryEntry<T>
	{
		/** Type name of the registered property. */
		public final String typeName;
		/** A helper class used to parse potential property boundaries, used alongside {@link #propertyConstructorBounded}. */
		public final Parser<T> propertyParser;
		/** Constructor of the registered {@link zoranodensha.api.vehicles.part.property.IPartProperty property} class. */
		public final Constructor<? extends IXMLProperty<T>> propertyConstructor;
		/** The registered property's corresponding bounded type. May be {@code null}. */
		public final Constructor<? extends IXMLProperty<T>> propertyConstructorBounded;



		/**
		 * Create a new registry entry for given arguments.
		 * 
		 * @throws SecurityException - If there was a security exception while trying to retrieve a class' constructor.
		 * @throws NoSuchMethodException - If retrieval of the property's constructor failed.
		 * @throws IllegalArgumentException If either {@code name} or {@code clazz} are {@code null}.
		 */
		public RegistryEntry(String name, Class<?> type, Class<? extends IXMLProperty<T>> clazz, Class<? extends IXMLProperty<T>> clazzBounded, Parser<T> parser) throws SecurityException, NoSuchMethodException
		{
			if (name == null || clazz == null)
			{
				throw new IllegalArgumentException("Invalid registration for XML-custom property!");
			}

			this.typeName = name;
			this.propertyParser = parser;

			/* Retrieve constructors. */
			Constructor<? extends IXMLProperty<T>> propertyConstructor = clazz.getConstructor(VehParBase.class, String.class);
			Constructor<? extends IXMLProperty<T>> propertyConstructorBounded = null;
			if (clazzBounded != null && parser != null)
			{
				propertyConstructorBounded = clazzBounded.getConstructor(VehParBase.class, type, type, String.class);
			}

			this.propertyConstructor = propertyConstructor;
			this.propertyConstructorBounded = propertyConstructorBounded;
		}
	}

	/**
	 * Helper class to parse a given String for the specified object type.
	 * 
	 * @author Leshuwa Kaiheiwa
	 */
	public static abstract class Parser<T>
	{
		/**
		 * Return the maximum value, if existent.<br>
		 * Otherwise return {@code null}.
		 */
		public T max()
		{
			return null;
		}

		/**
		 * Return the minimum value, if existent.<br>
		 * Otherwise return {@code null}.
		 */
		public T min()
		{
			return null;
		}

		/**
		 * Parse the given String.<br>
		 * Return {@code null} if unsuccessful.
		 */
		public abstract T parse(String s);
	}
}