package zoranodensha.api.vehicles.part.xml;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.management.modelmbean.XMLParseException;



/**
 * XML file parser, used to parse XML files into {@link zoranodensha.api.vehicles.part.xml.XMLTreeNode XML nodes}.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class XMLParser
{
	/**
	 * Array of possible parse actions, in order of importance.
	 */
	private final AParseAction[] actions = new AParseAction[] {

			/*
			 * Comments.
			 */
			new AParseAction()
			{
				@Override
				public int parse()
				{
					if (isParsingComment)
					{
						if (parseBufferSize >= 3 && parseBuffer.startsWith("-->"))
						{
							isParsingComment = false;
							return 3;
						}
						else if (parseBuffer.startsWith("\n"))
						{
							line += 1;
							pos = 0;
						}
						return 1;
					}
					else if (parseBufferSize >= 4 && "<!--".equals(parseBuffer))
					{
						isParsingComment = true;
						return 4;
					}
					return 0;
				}
			},

			/*
			 * New lines and carriage return.
			 */
			new AParseAction()
			{
				@Override
				public int parse()
				{
					if (parseBuffer.startsWith("\n"))
					{
						line += 1;
						pos = 0;
						return 1;
					}
					else if (parseBuffer.startsWith("\r"))
					{
						return 1;
					}
					return 0;
				}
			},

			/*
			 * Spaces.
			 */
			new AParseAction()
			{
				@Override
				public int parse() throws XMLParseException
				{
					int codePoint = parseBuffer.codePointAt(0);
					if (Character.isWhitespace(codePoint))
					{
						/* Ignore spaces if there is no active node (indicating white space in front of actual XML tree data). */
						if (currentNode == null)
						{
							return 1;
						}

						if (parseState != null)
						{
							switch (parseState)
							{
								default:
									break;

								case ATTRIB_NAME:
									/* Ignore spaces while parsing attribute names. Lock name if not empty. */
									if (!currentNode.getName().isEmpty())
									{
										currentNode.lockName();
									}
									return 1;

								case ATTRIB_VALUE:
									/* Parse spaces if there is a delimiter, i.e. when reading attribute values. */
									if (valueDelimiterCodePoint != -1)
									{
										return 0;
									}
									if (parseBufferSize >= 2 && Character.isLetter(parseBuffer.codePointAt(1)))
									{
										currentNode.lockName();
										nodePush();
										setParseState(EState.ATTRIB_NAME);
									}
									return 1;

								case TAG_NAME_CLOSING:
									/* Spaces in closing tags raise an exception. */
									throw new XMLParseException(String.format("Illegal character: '%s' (Line=%d, Pos=%d)", String.copyValueOf(Character.toChars(codePoint)), line, pos));

								case TAG_NAME_OPENING:
									/* Ignore spaces while parsing tag names. Lock name if not empty. */
									if (!currentNode.getName().isEmpty())
									{
										currentNode.lockName();
									}
									if (parseBufferSize >= 2 && Character.isLetter(parseBuffer.codePointAt(1)))
									{
										nodePush();
										setParseState(EState.ATTRIB_NAME);
									}
									return 1;
							}
						}
					}
					return 0;
				}
			},

			/*
			 * Closing tags.
			 */
			new AParseAction()
			{
				@Override
				public int parse() throws XMLParseException
				{
					if (isNotParsingValue() && parseBufferSize >= 2 && parseBuffer.startsWith("</"))
					{
						setParseState(EState.TAG_NAME_CLOSING);
						parsedText = "";
						return 2;
					}
					return 0;
				}
			},

			/*
			 * Self-closing tags.
			 */
			new AParseAction()
			{
				@Override
				public int parse() throws XMLParseException
				{
					if (isNotParsingValue() && parseBufferSize >= 2 && parseBuffer.startsWith("/>"))
					{
						currentNode.lockName();
						currentNode.lockValue();
						nodePop();
						setParseState(null);
						return 2;
					}
					return 0;
				}
			},

			/*
			 * Tag opening.
			 */
			new AParseAction()
			{
				@Override
				public int parse() throws XMLParseException
				{
					if (isNotParsingValue() && parseBuffer.startsWith("<"))
					{
						nodePush();
						setParseState(EState.TAG_NAME_OPENING);
						return 1;
					}
					return 0;
				}
			},

			/*
			 * Tag closing.
			 */
			new AParseAction()
			{
				@Override
				public int parse() throws XMLParseException
				{
					if (isNotParsingValue() && parseBuffer.startsWith(">"))
					{
						if (parseState == EState.TAG_NAME_CLOSING)
						{
							if (!currentNode.getName().equals(parsedText))
							{
								throw new XMLParseException(String.format("Mismatched tags! '%s' vs '%s' (Line=%d, Pos=%d)", currentNode.getName(), parsedText, line, pos));
							}

							nodePop();
							parsedText = null;
						}

						setParseState(null);
						return 1;
					}
					return 0;
				}
			},

			/*
			 * Tag attribute values.
			 */
			new AParseAction()
			{
				@Override
				public int parse() throws XMLParseException
				{
					if (isNotParsingValue() && parseBuffer.startsWith("="))
					{
						currentNode.lockName();
						setParseState(EState.ATTRIB_VALUE);
						return 1;
					}
					return 0;
				}
			},

			/*
			 * Appends parsed characters to their respective destinations.
			 */
			new AParseAction()
			{
				@Override
				public int parse() throws XMLParseException
				{
					int codePoint = parseBuffer.codePointAt(0);
					if (parseState != null)
					{
						switch (parseState)
						{
							default:
								break;

							case ATTRIB_NAME:
							case TAG_NAME_OPENING:
								if (!currentNode.addToName(codePoint))
								{
									throw new XMLParseException(String.format("Illegal character: '%s' (Line=%d, Pos=%d)", String.copyValueOf(Character.toChars(codePoint)), line, pos));
								}
								break;

							case ATTRIB_VALUE:
								if (valueDelimiterCodePoint == -1)
								{
									if (currentNode.getValue().isEmpty() && (parseBuffer.startsWith("\"") || parseBuffer.startsWith("'")))
									{
										valueDelimiterCodePoint = codePoint;
									}
									else
									{
										throw new XMLParseException(String.format("Illegal character: '%s' (Line=%d, Pos=%d)", String.copyValueOf(Character.toChars(codePoint)), line, pos));
									}
								}
								else if (valueDelimiterCodePoint == codePoint)
								{
									valueDelimiterCodePoint = -1;
									currentNode.lockValue();
									nodePop();
								}
								else if (!currentNode.addToValue(codePoint))
								{
									throw new XMLParseException(String.format("Illegal character: '%s' (Line=%d, Pos=%d)", String.copyValueOf(Character.toChars(codePoint)), line, pos));
								}
								break;

							case TAG_NAME_CLOSING:
								parsedText += parseBuffer.substring(0, 1);
								break;
						}
					}
					else if (currentNode != null)
					{
						if (Character.isWhitespace(codePoint))
						{
							if (!currentNode.getValue().isEmpty() && !currentNode.isValueLocked())
							{
								if (!currentNode.addToValue(codePoint))
								{
									throw new XMLParseException(String.format("Illegal character: '%s' (Line=%d, Pos=%d)", String.copyValueOf(Character.toChars(codePoint)), line, pos));
								}
							}
						}
						else if (!currentNode.addToValue(codePoint))
						{
							throw new XMLParseException(String.format("Illegal character: '%s' (Line=%d, Pos=%d)", String.copyValueOf(Character.toChars(codePoint)), line, pos));
						}
					}
					return 1;
				}
			} };


	/** Line number we're at. */
	private int line = 1;
	/** Character position we're at. */
	private int pos = 1;
	/** The {@link #parseBuffer} length. */
	private int parseBufferSize = 0;
	/** Code point to limit values nested in tags. */
	private int valueDelimiterCodePoint = -1;
	/** {@code true} while a comment is being parsed. */
	private boolean isParsingComment = false;

	/** Four characters parsed, front-most being the current one. */
	private String parseBuffer = "";
	/** Parsed text, used while parsing a closing tag's name. */
	private String parsedText = null;

	/** Current active {@link XMLTreeNode node}. May be {@code null}. */
	private XMLTreeNode currentNode = null;
	/** Parse state of the tag currently being parsed. May be {@code null} if not applicable. */
	private EState parseState = null;



	/**
	 * Resets this parser's state.
	 */
	private void init()
	{
		line = 1;
		pos = 1;
		parseBufferSize = 0;
		valueDelimiterCodePoint = -1;
		isParsingComment = false;

		parseBuffer = "";
		parsedText = null;

		currentNode = null;
		parseState = null;
	}

	/**
	 * Returns whether a value is right now being parsed.
	 */
	private boolean isNotParsingValue()
	{
		return (parseState != EState.ATTRIB_VALUE) || (valueDelimiterCodePoint == -1);
	}

	/**
	 * Pops the current node and walks one step up on the node tree.
	 */
	private void nodePop()
	{
		if (currentNode.getParent() != null)
		{
			currentNode = currentNode.getParent();
		}
	}

	/**
	 * Pushes a new node onto the node tree and sets it as active node.
	 */
	private void nodePush()
	{
		XMLTreeNode childNode = new XMLTreeNode();
		if (currentNode != null)
		{
			currentNode.addChild(childNode);
		}
		currentNode = childNode;
	}

	/**
	 * Creates an input stream from the given ZIP-entry in the given ZIP-file and tries to parse it for an XML document tree.
	 * 
	 * @return The parsed XML document's root node.
	 * @throws IOException If an error occurred during either IO operation.
	 * @throws XMLParseException If the XML file couldn't be parsed.
	 */
	public XMLTreeNode parseXML(ZipEntry entry, ZipFile zipFile) throws IOException, XMLParseException
	{
		return parseXML(zipFile.getInputStream(entry));
	}

	/**
	 * Parses the given input stream as XML-document and creates a {@link XMLTreeNode}-based document tree.
	 * 
	 * @return The parsed XML document's root node.
	 * @throws IOException If an error occurred during either IO operation.
	 * @throws XMLParseException If the XML file couldn't be parsed.
	 */
	public XMLTreeNode parseXML(InputStream inputStream) throws IOException, XMLParseException
	{
		/* Reset this parser beforehand. */
		init();

		InputStreamReader inputStreamReader;
		BufferedReader bufferedReader = null;

		try
		{
			/*
			 * Prepare loop and streams.
			 */
			inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
			bufferedReader = new BufferedReader(inputStreamReader);

			/*
			 * Parse the file and create XML node tree.
			 */
			for (int newChar; (newChar = bufferedReader.read()) != -1 || !parseBuffer.isEmpty(); )
			{
				/*
				 * Fill text before doing anything else. Break loop if end of file was reached.
				 */
				if (newChar != -1)
				{
					parseBuffer += String.copyValueOf(Character.toChars(newChar));
					if (parseBuffer.length() < 4)
					{
						continue;
					}
				}
				parseBufferSize = parseBuffer.length();

				/*
				 * Parse the buffered text.
				 */
				int parsedCharacterCount = 0;
				for (AParseAction action : actions)
				{
					parsedCharacterCount = action.parse();
					if (parsedCharacterCount > 0)
					{
						break;
					}
				}

				/* Finally, drop the parsed text characters and continue loop. */
				pos += parsedCharacterCount;
				parseBuffer = parseBuffer.substring(parsedCharacterCount);
			}
		}
		finally
		{
			if (bufferedReader != null)
			{
				bufferedReader.close();
			}
		}

		/* Determine root node, then lock all child nodes to prevent later changes. */
		if (currentNode != null)
		{
			while (currentNode.getParent() != null)
			{
				nodePop();
			}
			currentNode.lockAll();
		}

		return currentNode;
	}

	/**
	 * Tries to override the current parse state with the given value.
	 * 
	 * @param newState - New parse state. May be {@code null}.
	 * @throws XMLParseException If the new state isn't compatible with the previous.
	 */
	private void setParseState(EState newState) throws XMLParseException
	{
		String err = null;

		if (newState == null)
		{
			if (parseState == EState.ATTRIB_NAME)
			{
				err = "Missing attribute value before closing tag!";
			}
		}
		else
		{
			switch (newState)
			{
				default:
					break;

				case ATTRIB_NAME:
					if (parseState != EState.ATTRIB_VALUE && parseState != EState.TAG_NAME_OPENING)
					{
						err = "Malformatted tag!";
					}
					break;

				case ATTRIB_VALUE:
					if (parseState != EState.ATTRIB_NAME)
					{
						err = "Malformatted tag!";
					}
					break;

				case TAG_NAME_CLOSING:
				case TAG_NAME_OPENING:
					if (parseState != null)
					{
						err = "Tag already open!";
					}
					break;
			}
		}

		if (err != null)
		{
			throw new XMLParseException(String.format("%s (Line=%d, Pos=%d)", err, line, pos));
		}

		parseState = newState;
	}



	/**
	 * Abstract base class for actions executed while parsing.
	 */
	private static abstract class AParseAction
	{
		/**
		 * Execute this parse action and return the number of parsed code points, if any.
		 * 
		 * @return The number of parsed code points.
		 * @throws XMLParseException If an error occurred while parsing.
		 */
		public abstract int parse() throws XMLParseException;
	}


	/**
	 * States used while parsing a single tag.
	 */
	private enum EState
	{
		ATTRIB_NAME,
		ATTRIB_VALUE,
		TAG_NAME_CLOSING,
		TAG_NAME_OPENING
	}
}