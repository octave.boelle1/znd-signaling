package zoranodensha.api.vehicles.part.xml;

/**
 * Names of all tags used for renderer definition.
 * 
 * @author Leshuwa Kaiheiwa
 */
public enum ERendererTags
{
	/** Render color tag. Accepts references to color properties like {@code $color}, as well as HEX and RGB values. */
	COLOUR("colour"),
	/** Renders its child tags if its logical expression evaluates to {@code true}. */
	IF("if"),
	/** Wraps the following render tag operations into a matrix call. This is essential to avoid operations mixing with each other where not desired. */
	MATRIX("matrix"),
	/** Model reference tag, specifies actual model (piece) rendering. */
	OBJ("obj"),
	/** Position translation along X,Y,Z on active matrix. */
	OFFSET("offset"),
	/** Rotation about specified axes on active matrix. */
	ROTATION("rotation"),
	/** Scale along specified axes. */
	SCALE("scale");

	/** XML tag name. */
	public final String tagName;



	ERendererTags(String tagName)
	{
		this.tagName = tagName;
	}

	/**
	 * Parses the given String for whether it contains either of this enum's values.
	 * 
	 * @param s - The string to parse.
	 * @return The matching {@link ERendererTags render tag type}, or {@code null} if the given String was invalid.
	 */
	public static ERendererTags fromString(String s)
	{
		if (s != null)
		{
			/* Match the given String against all renderer tags. */
			for (ERendererTags rendererTag : values())
			{
				if (rendererTag.tagName.equals(s))
				{
					return rendererTag;
				}
			}

			/* If no match was found until here, check for color name ambiguity. */
			if (COLOUR.tagName.replace("u", "").equals(s))
			{
				return COLOUR;
			}
		}
		return null;
	}
}