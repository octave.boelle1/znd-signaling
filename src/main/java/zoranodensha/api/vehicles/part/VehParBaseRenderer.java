package zoranodensha.api.vehicles.part;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.rendering.ARenderTag;
import zoranodensha.api.vehicles.part.rendering.ATransformation.Offset;
import zoranodensha.api.vehicles.part.rendering.ATransformation.Rotation;
import zoranodensha.api.vehicles.part.rendering.ATransformation.Scale;
import zoranodensha.api.vehicles.part.rendering.Colour;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.rendering.If;
import zoranodensha.api.vehicles.part.rendering.Matrix;
import zoranodensha.api.vehicles.part.rendering.Obj;
import zoranodensha.api.vehicles.part.xml.ERendererTags;
import zoranodensha.api.vehicles.part.xml.XMLTreeNode;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;



/**
 * Renderer for OBJ-model based basic {@link zoranodensha.api.vehicles.part.VehParBase vehicle parts}.
 * 
 * @author Leshuwa Kaiheiwa
 */
@SideOnly(Side.CLIENT)
public class VehParBaseRenderer implements IVehiclePartRenderer
{
	/** Parent {@link zoranodensha.api.vehicles.part.VehParBase vehicle part} of this renderer. */
	public final VehParBase part;
	/** The rendered {@link zoranodensha.api.vehicles.rendering.ObjModelSpeedy OBJ-model}. */
	public final ObjModelSpeedy model;
	/** Texture {@link net.minecraft.util.ResourceLocation location} of the rendered model. */
	public final ResourceLocation texture;

	/** {@code true} to render on the opaque render pass. */
	protected boolean renderOnPass_0;
	/** {@code true} to render on the transparent render pass. */
	protected boolean renderOnPass_1;
	/** {@link zoranodensha.api.vehicles.part.rendering.ARenderTag Render tags} called to render the part model. May be {@code null}. */
	protected ARenderTag[] renderTags;

	/** Render data {@link zoranodensha.api.vehicles.part.xml.XMLTreeNode node} containing raw rendering data. May be {@code null}. */
	private XMLTreeNode renderNode;
	/** Currently active {@link zoranodensha.api.util.ColorRGBA color}, used while rendering to determine whether model faces may be rendered in a given render pass. May be {@code null}. */
	private ColorRGBA activeColor;



	public VehParBaseRenderer(VehParBase part, ResourceLocation model, ResourceLocation texture)
	{
		this.part = part;
		this.model = new ObjModelSpeedy(model);
		this.texture = texture;
	}

	private VehParBaseRenderer(VehParBase part, ObjModelSpeedy model, ResourceLocation texture)
	{
		this.part = part;
		this.model = model;
		this.texture = texture;
	}


	/**
	 * Creates a copy of this renderer for the given part.<br>
	 * The copied renderer contains references to the same properties as this renderer.
	 * 
	 * @param copyPart - The new renderer's parent {@link zoranodensha.api.vehicles.part.VehParBase vehicle part}.
	 * @return A copy of this renderer containing the same rendering instructions as this renderer, or {@code null} if unsuccessful.
	 */
	@Override
	public VehParBaseRenderer copy(VehParBase copyPart)
	{
		VehParBaseRenderer copyRenderer = new VehParBaseRenderer(copyPart, model, texture);
		copyRenderer.renderOnPass_0 = renderOnPass_0;
		copyRenderer.renderOnPass_1 = renderOnPass_1;
		copyRenderer.parseRenderDataFromNode(renderNode);
		return copyRenderer;
	}

	/**
	 * Return the currently active render color.
	 * 
	 * @return Currently active {@link zoranodensha.api.util.ColorRGBA color}. May be {@code null}.
	 */
	public ColorRGBA getActiveColor()
	{
		return activeColor;
	}

	/**
	 * Parse the given node for render data.
	 * 
	 * @param node - {@link zoranodensha.api.vehicles.part.xml.XMLTreeNode Node} containing the part's render data.
	 * @return An array of all {@link zoranodensha.api.vehicles.part.rendering.ARenderTag render tags} on the same level as the given node's children.
	 */
	public ARenderTag[] parseRenderData(XMLTreeNode node)
	{
		ArrayList<ARenderTag> renderTagList = new ArrayList<ARenderTag>();
		{
			for (XMLTreeNode childNode : node.getChildren())
			{
				ARenderTag renderTag = parseRenderTag(childNode);
				if (renderTag != null)
				{
					ARenderTag[] childRenderTags = parseRenderData(childNode);
					if (childRenderTags != null)
					{
						for (ARenderTag childRenderTag : childRenderTags)
						{
							renderTag.addChild(childRenderTag);
						}
					}
					renderTagList.add(renderTag);
				}
			}
		}
		return (renderTagList.isEmpty()) ? null : renderTagList.toArray(new ARenderTag[0]);
	}

	/**
	 * Parses the given node for render data.
	 */
	public void parseRenderDataFromNode(XMLTreeNode renderNode)
	{
		if (renderNode != null)
		{
			this.renderNode = renderNode;
			this.renderTags = parseRenderData(renderNode);
		}
	}

	/**
	 * Parses the specified XML tree node for a render tag.
	 * 
	 * @param node - {@link zoranodensha.api.vehicles.part.xml.XMLTreeNode Node} containing render data.
	 * @return {@link zoranodensha.api.vehicles.part.rendering.ARenderTag Render tag parsed from the given node}.
	 */
	public ARenderTag parseRenderTag(XMLTreeNode node)
	{
		ERendererTags renderTag = ERendererTags.fromString(node.getName());
		if (renderTag != null)
		{
			switch (renderTag)
			{
				case COLOUR:
					return new Colour(node, this);
					
				case IF:
					return new If(node, this);

				case MATRIX:
					return new Matrix(node);

				case OBJ:
					return new Obj(node, this);

				case OFFSET:
					return new Offset(node, this);

				case ROTATION:
					return new Rotation(node, this);

				case SCALE:
					return new Scale(node, this);
			}
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return true;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (renderTags != null)
		{
			Minecraft.getMinecraft().renderEngine.bindTexture(texture);
			GL11.glPushMatrix();
			{
				for (ARenderTag renderTag : renderTags)
				{
					renderTag.apply(partialTick, null);
				}
			}
			GL11.glPopMatrix();
		}
	}

	@Override
	public void renderPartItem(ItemRenderType type)
	{
		if (renderTags != null)
		{
			Minecraft.getMinecraft().renderEngine.bindTexture(texture);
			GL11.glPushMatrix();
			{
				for (ARenderTag renderTag : renderTags)
				{
					renderTag.apply(1.0F, type);
				}
			}
			GL11.glPopMatrix();
		}
	}

	/**
	 * Override the reference to the currently active {@link zoranodensha.api.util.ColorRGBA color} and apply.
	 */
	public void setActiveColor(ColorRGBA activeColor)
	{
		this.activeColor = activeColor;
		if (this.activeColor != null)
		{
			this.activeColor.apply();
		}
	}
}