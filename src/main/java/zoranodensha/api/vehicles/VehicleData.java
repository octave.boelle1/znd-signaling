package zoranodensha.api.vehicles;

import static zoranodensha.api.vehicles.VehicleData.EDataKey.CREATOR;
import static zoranodensha.api.vehicles.VehicleData.EDataKey.ID;
import static zoranodensha.api.vehicles.VehicleData.EDataKey.INVERSE;
import static zoranodensha.api.vehicles.VehicleData.EDataKey.NAME;
import static zoranodensha.api.vehicles.VehicleData.EDataKey.OFFSET;
import static zoranodensha.api.vehicles.VehicleData.EDataKey.PARTS;

import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;

import javax.annotation.Nonnull;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import zoranodensha.api.util.APILogger;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.boundingBox.IBoundingBoxProvider;
import zoranodensha.api.vehicles.boundingBox.VTBoundingBox;
import zoranodensha.api.vehicles.handlers.SectionDivisionHandler;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.APartType;
import zoranodensha.api.vehicles.part.type.APartTypeCoupling;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.util.AVehiclePartRegistry;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.api.vehicles.part.util.OrderedPartsList.IPartsSelector;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.api.vehicles.part.util.OrderedTypesList.ITypesSelector;
import zoranodensha.api.vehicles.util.VehicleHelper;



/**
 * Wrapper class holding vehicle data, such as vehicle parts, UVID, or creator and vehicle name.<br>
 * All data wrapped by this class is <b>insensitive</b> to {@link zoranodensha.api.util.ETagCall NBT call types}.
 * 
 * @author Leshuwa Kaiheiwa
 */
public class VehicleData implements IBoundingBoxProvider, Iterable<VehicleSection>
{
	/**
	 * <p>
	 * Map linking all {@link VehicleData vehicles} which are part of spawned trains.<br>
	 * This is used to give a massive client-sided performance boost to train coupling, as newly coupled trains
	 * now don't need to send their entire vehicle data to all clients anymore. Less lag, happier players.<br>
	 * Initialised by Zora no Densha.
	 * </p>
	 *
	 * <i>Client-side only.</i>
	 */
	@SideOnly(Side.CLIENT) public static HashMap<Integer, VehicleData> globalVehicleMap;

	/** Previous identifier assigned to a vehicle. */
	private static int lastVehicleID = 1;

	/*
	 * Vehicle part and type caches for quicker access.
	 */
	/** Cache holding lists of vehicle {@link zoranodensha.api.vehicles.part.VehParBase parts}. */
	private final HashMap<IPartsSelector, OrderedPartsList> partsCache = new HashMap<IPartsSelector, OrderedPartsList>();
	/** Cache holding lists of vehicle part {@link zoranodensha.api.vehicles.part.type.APartType types}. */
	private final HashMap<ITypesSelector<?>, OrderedTypesList<? extends APartType>> typesCache = new HashMap<ITypesSelector<?>, OrderedTypesList<? extends APartType>>();

	/*
	 * Basic vehicle data.
	 */
	/** {@link zoranodensha.api.vehicles.UVID Unique Vehicle ID}. */
	private final UVID uvid;
	/** Vehicle parts list. */
	private final VehiclePartsList parts;
	/** {@link zoranodensha.api.vehicles.boundingBox.VTBoundingBox Bounding box} of this vehicle. */
	private final VTBoundingBox<VehicleSection, VehicleData> boundingBox;

	/** Unique identifier number of this vehicle. Defined on server side and synchronised to clients. */
	private int vehicleID = lastVehicleID++;
	/** Creator name. */
	private String creatorName;
	/** Vehicle name. */
	private String vehicleName;
	/** The front {@link zoranodensha.api.vehicles.VehicleSection section} of this vehicle. May be {@code null}. */
	private VehicleSection section;
	/** The front {@link zoranodensha.api.vehicles.part.type.APartTypeCoupling coupling} of this vehicle. May be {@code null}. */
	private APartTypeCoupling couplingF;
	/** The back {@link zoranodensha.api.vehicles.part.type.APartTypeCoupling coupling} of this vehicle. May be {@code null}. */
	private APartTypeCoupling couplingB;

	/*
	 * (Semi-)dynamic data.
	 */
	/** Parent {@link zoranodensha.api.vehicles.Train train} this vehicle belongs to. May be {@code null}. */
	private Train train;
	/** {@code true} if the part position along X has been inverted. */
	private boolean isInverse;
	/** The vehicle's local X offset. */
	private float offsetX;
	/** Next unused identifier for vehicle {@link zoranodensha.api.vehicles.part.VehParBase parts} in this vehicle. */
	private int nextPartID;



	/**
	 * Default constructor.
	 */
	public VehicleData()
	{
		this(new UVID(), null, null, null);
	}

	/**
	 * Create a new instance of this class with data read from the given NBT tag.
	 * 
	 * @see #readFromNBT(NBTTagCompound, ETagCall)
	 */
	public VehicleData(NBTTagCompound nbt, ETagCall callType)
	{
		this();
		readFromNBT(nbt, callType);
	}

	/**
	 * Create a new instance of this class with given data.
	 */
	public VehicleData(UVID uvid, OrderedPartsList parts, String creatorName, String vehicleName)
	{
		this.uvid = uvid;
		this.parts = new VehiclePartsList();
		if (parts != null && !parts.isEmpty())
		{
			this.parts.addAll(parts);
		}

		this.boundingBox = new VTBoundingBox<VehicleSection, VehicleData>(this);
		this.creatorName = creatorName;
		this.vehicleName = vehicleName;
	}


	/**
	 * Adds the given vehicle part to this vehicle and overrides the part's ID.
	 * 
	 * @param part - {@link zoranodensha.api.vehicles.part.VehParBase Vehicle part} to add.
	 * @return {@code true} if the given part was added to this vehicle.
	 */
	public boolean addPart(VehParBase part)
	{
		if (getTrain() != null)
		{
			return false;
		}

		part.setID(++nextPartID);
		return parts.add(part);
	}

	@Override
	public boolean equals(Object obj)
	{
		return (obj instanceof VehicleData && obj.hashCode() == hashCode());
	}

	/**
	 * Getter for this vehicle's {@link #boundingBox}.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public VTBoundingBox<VehicleSection, VehicleData> getBoundingBox()
	{
		return boundingBox;
	}

	/**
	 * Returns the back coupling of this vehicle, if existent.
	 * 
	 * @return {@link zoranodensha.api.vehicles.part.type.APartTypeCoupling Coupling} at the back of this vehicle. May be {@code null}.
	 */
	public APartTypeCoupling getCouplingBack()
	{
		return couplingB;
	}

	/**
	 * Returns the front coupling of this vehicle, if existent.
	 * 
	 * @return {@link zoranodensha.api.vehicles.part.type.APartTypeCoupling Coupling} at the front of this vehicle. May be {@code null}.
	 */
	public APartTypeCoupling getCouplingFront()
	{
		return couplingF;
	}

	/**
	 * Returns the username of this vehicle's creator.
	 */
	public String getCreatorName()
	{
		return creatorName;
	}

	/**
	 * Returns whether this vehicle contains any vehicle parts.
	 * 
	 * @return {@code true} if this vehicle contains vehicle parts.
	 */
	public boolean getHasParts()
	{
		return !getParts().isEmpty();
	}

	/**
	 * Returns the unique identifier number of this vehicle.<br>
	 * Unlike the {@link #uvid}, this number is guaranteed to be unique across the running game instance.
	 */
	public int getID()
	{
		return vehicleID;
	}

	/**
	 * Getter for the {@link #isInverse} flag.
	 */
	public boolean getIsInverse()
	{
		return isInverse;
	}

	/**
	 * Getter for {@link #offsetX}.
	 */
	public float getOffset()
	{
		return offsetX;
	}

	/**
	 * Returns the vehicle part at the given index.
	 * 
	 * @param index - Index of the requested part.
	 * @return The requested {@link zoranodensha.api.vehicles.part.VehParBase vehicle part} or {@code null} if the index was invalid.
	 */
	public VehParBase getPartAt(int index)
	{
		if (index < 0 || index > getPartCount())
		{
			return null;
		}
		return getParts().get(index);
	}

	/**
	 * Finds and returns the part with given part ID in this vehicle.
	 * 
	 * @param partID - The part's identifier.
	 * @return The vehicle {@link zoranodensha.api.vehicles.part.VehParBase part} matching the given identifier, or {@code null} if none was found.
	 */
	public VehParBase getPartByID(int partID)
	{
		for (VehParBase part : getParts())
		{
			if (part.getID() == partID)
			{
				return part;
			}
		}
		return null;
	}

	/**
	 * Returns the number of vehicle parts in this vehicle.
	 */
	public int getPartCount()
	{
		return getParts().size();
	}

	/**
	 * Returns this vehicle's {@link #parts parts list}.<br>
	 * The returned list is the immediate parts list of this vehicle and should <b>not</b> be modified.
	 */
	public VehiclePartsList getParts()
	{
		return parts;
	}

	/**
	 * Returns a new list containing all vehicle parts in this vehicle matching the given selector.
	 * 
	 * @param selector - {@link zoranodensha.api.vehicles.part.util.OrderedPartsList.IPartsSelector Selector} to filter the resulting list.
	 * @return The resulting list.
	 */
	public OrderedPartsList getParts(IPartsSelector selector)
	{
		/* If there is no cached list, calculate and store in cache. */
		if (!partsCache.containsKey(selector))
		{
			partsCache.put(selector, getParts().filter(selector));
		}

		/* Retrieve the list matching this selector and return a clone. */
		return (OrderedPartsList)partsCache.get(selector).clone();
	}

	/**
	 * Returns the first (and thus front-most) section of this vehicle.
	 * 
	 * @return This vehicle's front {@link zoranodensha.api.vehicles.VehicleSection section}. May be {@code null}.
	 */
	public VehicleSection getSection()
	{
		return section;
	}

	/**
	 * Returns the parent train this vehicle belongs to.
	 * 
	 * @return The parent {@link zoranodensha.api.vehicles.Train train} of this vehicle. May be {@code null}.
	 */
	public Train getTrain()
	{
		return train;
	}

	/**
	 * Returns a new list containing all vehicle part types in this vehicle matching the given selector.
	 * 
	 * @param selector - {@link zoranodensha.api.vehicles.part.util.OrderedTypesList.ITypesSelector Selector} to filter the resulting list.
	 * @return The resulting list.
	 */
	@SuppressWarnings("unchecked")
	public <P extends APartType> OrderedTypesList<P> getTypes(ITypesSelector<P> selector)
	{
		/* If there is no cached list, calculate and store in cache. */
		if (!typesCache.containsKey(selector))
		{
			typesCache.put(selector, getParts().filter(selector));
		}

		/* Retrieve the list matching this selector and return a clone. */
		return (OrderedTypesList<P>)typesCache.get(selector).clone();
	}

	/**
	 * Getter for this vehicle's {@link #uvid UVID}.
	 */
	public UVID getUVID()
	{
		return uvid;
	}

	/**
	 * Returns the name of this vehicle.
	 */
	public String getVehicleName()
	{
		return vehicleName;
	}

	@Override
	public int hashCode()
	{
		return getUVID().hashCode();
	}

	/**
	 * Flips the vehicle by 180°.
	 */
	private void invert()
	{
		float[] arr;
		float x, z;

		for (VehParBase part : getParts())
		{
			/* Rotate part by 180° about Y axis. */
			arr = part.getOffset();
			x = -arr[0];
			z = -arr[2];

			/* Apply new position and offset. */
			part.setOffset(x, arr[1], z);

			/* Add 180° to yaw rotation value. */
			arr = part.getRotation();
			part.setRotation(arr[0], VehicleHelper.normalise(arr[1] + 180.0F), arr[2]);
		}

		/* Finally, update inversion flag. */
		isInverse = !isInverse;
	}

	/**
	 * Returns whether the given part belongs to this vehicle.
	 * 
	 * @return {@code true} if this vehicle is a parent of the given {@link zoranodensha.api.vehicles.part.VehParBase part}.
	 */
	public boolean isParentOf(VehParBase part)
	{
		return getParts().contains(part);
	}

	@Nonnull
	@Override
	public Iterator<VehicleSection> iterator()
	{
		/*
		 * Iterates from front to back within this vehicle.
		 */
		return new Iterator<VehicleSection>()
		{
			private VehicleSection curr = getSection();



			@Override
			public boolean hasNext()
			{
				return (curr != null);
			}

			@Override
			public VehicleSection next()
			{
				if (!hasNext())
				{
					throw new NoSuchElementException();
				}

				VehicleSection ret = curr;
				curr = curr.sectionB;
				return ret;
			}

			@Override
			public void remove()
			{
				throw new UnsupportedOperationException();
			}
		};
	}

	/**
	 * Moves this vehicle's parts by the given offset.
	 *
	 * @param offset - Distance to move the parts, in metres.
	 */
	private void offset(float offset)
	{
		float[] arr;
		for (VehParBase part : getParts())
		{
			arr = part.getOffset();
			part.setOffset(arr[0] + offset, arr[1], arr[2]);
		}
		offsetX += offset;
	}

	/**
	 * Helper method to undo all changes done to parts.
	 * 
	 * @see #pushChanges(boolean, float)
	 */
	public void popChanges()
	{
		if (getTrain() != null)
		{
			throw new IllegalStateException("Tried to modify vehicle while it's part of a vehicle list!");
		}

		offset(-getOffset());

		if (getIsInverse())
		{
			invert();
		}

		getParts().onListChanged();
	}

	/**
	 * Applies changes to this vehicle data. Inversion is applied before offset.<br>
	 * <br>
	 * More formally, inversion rotates all parts' XZ positions around the local origin by 180°
	 * and adds 180° to the parts' rotation along Y.
	 * 
	 * @param invert - {@code true} to invert all parts.
	 * @param offset - Offset to be applied.
	 */
	public void pushChanges(boolean invert, float offset)
	{
		if (getTrain() != null)
		{
			throw new IllegalStateException("Tried to modify vehicle while it's part of a vehicle list!");
		}

		if (invert)
		{
			invert();
		}

		if (offset != 0.0F)
		{
			offset(offset);
		}

		getParts().onListChanged();
	}

	/**
	 * Initialises vehicle data from the given NBT tag.<br>
	 * <br>
	 * A train populates its array of vehicle data instances <b>before</b> reading each instance from NBT.
	 * 
	 * @param nbt - {@link net.minecraft.nbt.NBTTagCompound NBT tag} to read from.
	 * @param callType - {@link zoranodensha.api.util.ETagCall NBT call type}, handed down to parts.
	 */
	public void readFromNBT(NBTTagCompound nbt, ETagCall callType)
	{
		/* Override the vehicle ID if the tag contains such entry. This happens only on clients. */
		if (ETagCall.PACKET == callType && nbt.hasKey(ID.key))
		{
			int newVehicleID = nbt.getInteger(ID.key);
			if (newVehicleID > 0)
			{
				vehicleID = newVehicleID;
			}
		}

		/* Clear parts list first. */
		parts.clear();

		/* Read creator/vehicle names and UVID first. */
		creatorName = nbt.getString(CREATOR.key);
		vehicleName = nbt.getString(NAME.key);
		uvid.readFromNBT(nbt);

		/* Read part inversion flag and offset. */
		isInverse = nbt.getBoolean(INVERSE.key);
		offsetX = nbt.getFloat(OFFSET.key);

		/* Finally read and initialise parts. */
		parts.setMuteListChanges(true);
		{
			NBTTagList tagList = nbt.getTagList(PARTS.key, 10);
			NBTTagCompound nbt0;
			VehParBase part;
			String s;

			for (int i = tagList.tagCount() - 1; i >= 0; --i)
			{
				nbt0 = tagList.getCompoundTagAt(i);
				s = nbt0.getString(VehParBase.NBT_KEY);
				part = AVehiclePartRegistry.getPart(s);

				if (part != null)
				{
					addPart(part);
					part.readFromNBT(nbt0, callType);
				}
				else
				{
					APILogger.warn(VehicleData.class, String.format("Dropping vehicle part of type '%s' while reading from NBT.", s));
				}
			}
		}
		parts.setMuteListChanges(false);
	}

	/**
	 * Override the {@link #creatorName name} of this vehicle's creator.
	 */
	public void setCreatorName(@Nonnull String creatorName)
	{
		this.creatorName = creatorName;
	}

	/**
	 * Overrides the parent of this vehicle with the given reference.
	 * 
	 * @param train - The new parent {@link zoranodensha.api.vehicles.Train train}. May be {@code null}.
	 */
	public void setTrain(Train train)
	{
		this.train = train;

		for (VehParBase part : getParts())
		{
			part.onAddedToTrain(train);
		}
	}

	/**
	 * Override this vehicle's {@link #vehicleName name}.
	 */
	public void setVehicleName(@Nonnull String vehicleName)
	{
		this.vehicleName = vehicleName;
	}

	@Override
	public String toString()
	{
		/* Common data */
		String clazz = getClass().getSimpleName();
		String uvid = getUVID().toString();
		String train = (getTrain() != null) ? String.valueOf(getTrain().getEntityId()) : "~NULL~";

		/* Specific data */
		String couplingF = "~NULL~";
		String couplingB = "~NULL~";

		if (this.couplingF != null)
		{
			VehParBase part = this.couplingF.getParent();
			float[] partPos = part.getOffset();
			couplingF = String.format("%d(%.2flx %.2fly %.2flz)", part.getID(), partPos[0], partPos[1], partPos[2]);
		}

		if (this.couplingB != null)
		{
			VehParBase part = this.couplingB.getParent();
			float[] partPos = part.getOffset();
			couplingB = String.format("%d(%.2flx %.2fly %.2flz)", part.getID(), partPos[0], partPos[1], partPos[2]);
		}

		return String.format("%s[ID=%d | UVID=%s | Train=%s | Name=%s | By=%s | Inv=%b Off=%.2flx | F-Coupler=%s | B-Coupler=%s]", clazz, getID(), uvid, train, getVehicleName(), getCreatorName(), getIsInverse(), getOffset(), couplingF, couplingB);
	}

	/**
	 * Writes all vehicle data to the given NBT tag compound.
	 * 
	 * @param nbt - {@link net.minecraft.nbt.NBTTagCompound NBT tag} to write to.
	 * @param callType - {@link zoranodensha.api.util.ETagCall NBT call type}, handed down to parts.
	 */
	public void writeToNBT(NBTTagCompound nbt, ETagCall callType)
	{
		/* First, write vehicle ID if this is a sync packet to clients. */
		if (ETagCall.PACKET == callType && getTrain() != null && !getTrain().worldObj.isRemote)
		{
			nbt.setInteger(ID.key, getID());
		}

		/* Write creator/vehicle names and UVID next. */
		if (getCreatorName() != null && !getCreatorName().isEmpty())
		{
			nbt.setString(CREATOR.key, getCreatorName());
		}

		if (getVehicleName() != null && !getVehicleName().isEmpty())
		{
			nbt.setString(NAME.key, getVehicleName());
		}

		getUVID().writeToNBT(nbt);

		/* Write part inversion flag and offset. */
		nbt.setBoolean(INVERSE.key, getIsInverse());
		nbt.setFloat(OFFSET.key, getOffset());

		/* Finally, write parts. */
		NBTTagList tagList = new NBTTagList();
		NBTTagCompound nbt0;

		for (VehParBase part : getParts())
		{
			part.writeToNBT(nbt0 = new NBTTagCompound(), callType);
			nbt0.setString(VehParBase.NBT_KEY, part.getName());
			tagList.appendTag(nbt0);
		}

		nbt.setTag(PARTS.key, tagList);
	}



	/**
	 * Custom list implementation for {@link VehicleData vehicles}.
	 * 
	 * @author Leshuwa Kaiheiwa
	 */
	public class VehiclePartsList extends OrderedPartsList
	{
		/** {@code true} while changes to this list should be ignored in {@link #onListChanged()}. */
		private boolean muteListChanges;



		@Override
		public boolean add(VehParBase part)
		{
			if (super.add(part))
			{
				part.onAddedToVehicle(VehicleData.this);
				return true;
			}
			return false;
		}

		@Override
		protected void onListChanged()
		{
			/* Call super method and clear caches. */
			super.onListChanged();
			partsCache.clear();
			typesCache.clear();
			
			if (getTrain() != null)
			{
				getTrain().getVehicleList().clearCaches();
			}

			/* Ignore this call while reading a vehicle from NBT. */
			if (muteListChanges)
			{
				return;
			}

			/*
			 * Refresh vehicle sections.
			 */

			/* Clear section data. */
			if (getSection() != null)
			{
				/* Clear section bounding box.. */
				section.getBoundingBox().clear();

				/* ..and all parts' references. */
				for (VehParBase part : getParts())
				{
					part.onAddedToSection(null);
				}

				/* Also notify bogie part types of removal. */
				for (PartTypeBogie bogie : getSection())
				{
					bogie.onAddedToSection(null);
				}
			}

			/* Hand actual section division down to the respective handler method. */
			section = SectionDivisionHandler.INSTANCE.getSectionedVehicle(VehicleData.this);

			/*
			 * Refresh coupling references.
			 */

			couplingF = null;
			couplingB = null;

			/* Determine front and back couplings. */
			OrderedTypesList<APartTypeCoupling> couplings = getTypes(OrderedTypesList.ASELECTOR_COUPLING);
			if (!couplings.isEmpty())
			{
				/* If there's at least two couplings, assign front- and back-most couplings respectively. */
				if (couplings.size() > 1)
				{
					couplingF = couplings.get(0);
					couplingB = couplings.get(couplings.size() - 1);
				}
				else
				{
					/* Otherwise, get the front- and back-most parts in the vehicle. */
					APartTypeCoupling coupling = couplings.get(0);
					VehParBase partF = getPartAt(0);
					VehParBase partB = getPartAt(getPartCount() - 1);

					/* Determine the distance to either side. */
					float distToFront = Math.abs(coupling.getParent().getOffset()[0] - partF.getOffset()[0]);
					float distToBack = Math.abs(coupling.getParent().getOffset()[0] - partB.getOffset()[0]);

					/* And assign the coupling to either front or back, depending on which end it is closer to. */
					if (distToBack > distToFront)
					{
						couplingF = coupling;
					}
					else
					{
						couplingB = coupling;
					}
				}
			}
		}

		/*
		 * Overridden to remove vehicle references from the removed parts.
		 */
		@Override
		public VehParBase remove(int index)
		{
			VehParBase part = super.remove(index);
			part.onAddedToVehicle(null);
			return part;
		}

		/**
		 * Set whether changes to this list should are ignored by {@link #onListChanged()}.<br>
		 * Unmuting list changes (i.e. with a {@code false} parameter value) triggers a refresh call to {@link #onListChanged()}.
		 * 
		 * @param muteListChanges - {@code true} to mute changes to this list.
		 */
		protected void setMuteListChanges(boolean muteListChanges)
		{
			this.muteListChanges = muteListChanges;
			if (!this.muteListChanges)
			{
				onListChanged();
			}
		}
	}


	/**
	 * Enum containing NBT keys used when writing/reading data to/from NBT.
	 */
	public enum EDataKey
	{
		/** {@code String} Creator name */
		CREATOR("CreatorName"),

		/** {@code int} Unique vehicle identifier */
		ID("VehicleID"),

		/** {@code boolean} Part inversion flag */
		INVERSE("IsInverse"),

		/** {@code String} Vehicle name */
		NAME("VehicleName"),

		/** {@code float} Part offset */
		OFFSET("OffsetX"),

		/** {@code NBTTagList} Vehicle parts */
		PARTS("VehicleParts");

		public final String key;



		EDataKey(String key)
		{
			this.key = key;
		}


		@Override
		public String toString()
		{
			return key;
		}

		/**
		 * Tries to assign a key to the given number.
		 * 
		 * @return The matching {@link zoranodensha.api.vehicles.VehicleData.EDataKey key}, or {@code null}.
		 */
		public static EDataKey fromInt(int i)
		{
			for (EDataKey key : values())
			{
				if (key.ordinal() == i)
				{
					return key;
				}
			}

			return null;
		}
	}
}