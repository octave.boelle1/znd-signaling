package zoranodensha.api.vehicles.boundingBox;

import static java.lang.Double.MAX_VALUE;

import java.lang.reflect.Field;
import java.util.Iterator;

import net.minecraft.entity.Entity;
import net.minecraft.launchwrapper.Launch;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import zoranodensha.api.util.APILogger;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;



/**
 * <h1>VT-Bounding Box</h1>
 * <hr>
 * 
 * <p>
 * This bounding box type acts as provider of standard bounding box functionality in {@link zoranodensha.api.vehicles.Train trains}
 * and {@link zoranodensha.api.vehicles.VehicleData vehicles}.<br>
 * Calls to this box are delegated down to their respective children which then determine whether or not to handle the interaction and,
 * if positive, delegate the interaction further down to the actual bounding boxes. Before actually considering child boxes, this box
 * first ensures that the call is valid for the wrapper (i.e. the box) itself.
 * </p>
 * 
 * <p>
 * The value of a {@link zoranodensha.api.vehicles.Train train's} bounding box is set via reflection, due to the {@code final} declaration of
 * {@link net.minecraft.entity.Entity#boundingBox boundingBox}.
 * </p>
 * 
 * <p>
 * <b>Important Note</b>
 * Methods which create a new instance of a bounding box upon their return do <b>not</b> create an instance of this class.
 * Instead, they return instances of the {@link zoranodensha.api.vehicles.boundingBox.MultiBoundingBox MultiBoundingBox} class.<br>
 * The following methods show this behaviour:<br>
 * <ul>
 * <li>{@link #contract(double, double, double)}</li>
 * <li>{@link #copy()}</li>
 * <li>{@link #expand(double, double, double)}</li>
 * <li>{@link #getOffsetBoundingBox(double, double, double)}</li>
 * <li>{@link #offset(double, double, double)}</li>
 * <ul>
 * </p>
 *
 * @author Leshuwa Kaiheiwa
 */
public class VTBoundingBox<C extends IBoundingBoxProvider, P extends Iterable<C>> extends AxisAlignedBB implements IBoundingBoxUpdateReceiver
{
	/** Parent of this box. */
	final P parent;



	/**
	 * Constructs a new {@link VTBoundingBox bounding box} for the given parent.
	 *
	 * @param parent - The box' parent.
	 */
	public VTBoundingBox(P parent)
	{
		super(0, 0, 0, 0, 0, 0);
		this.parent = parent;
	}


	@Override
	public MultiBoundingBox addCoord(double x, double y, double z)
	{
		MultiBoundingBox copy = new MultiBoundingBox();
		copy.minX = (x < 0.0) ? (minX + x) : minX;
		copy.minY = (y < 0.0) ? (minY + y) : minY;
		copy.minZ = (z < 0.0) ? (minZ + z) : minZ;
		copy.maxX = (x > 0.0) ? (maxX + x) : maxX;
		copy.maxY = (y > 0.0) ? (maxY + y) : maxY;
		copy.maxZ = (z > 0.0) ? (maxZ + z) : maxZ;

		for (C child : parent)
		{
			copy.addChild(child.getBoundingBox().addCoord(x, y, z));
		}

		return copy;
	}

	@Override
	public MovingObjectPosition calculateIntercept(Vec3 vecPos, Vec3 vecLook)
	{
		MovingObjectPosition movObjPos = super.calculateIntercept(vecPos, vecLook);
		if (movObjPos != null)
		{
			for (C child : parent)
			{
				movObjPos = child.getBoundingBox().calculateIntercept(vecPos, vecLook);
				if (movObjPos != null)
				{
					return movObjPos;
				}
			}
		}
		return null;
	}

	@Override
	public double calculateXOffset(AxisAlignedBB aabb, double offset)
	{
		if (aabb.maxY > minY && aabb.minY < maxY)
		{
			if (aabb.maxZ > minZ && aabb.minZ < maxZ)
			{
				for (C child : parent)
				{
					offset = child.getBoundingBox().calculateXOffset(aabb, offset);
				}
			}
		}
		return offset;
	}

	@Override
	public double calculateYOffset(AxisAlignedBB aabb, double offset)
	{
		if (aabb.maxX > minX && aabb.minX < maxX)
		{
			if (aabb.maxZ > minZ && aabb.minZ < maxZ)
			{
				for (C child : parent)
				{
					offset = child.getBoundingBox().calculateYOffset(aabb, offset);
				}
			}
		}
		return offset;
	}

	@Override
	public double calculateZOffset(AxisAlignedBB aabb, double offset)
	{
		if (aabb.maxX > minX && aabb.minX < maxX)
		{
			if (aabb.maxY > minY && aabb.minY < maxY)
			{
				for (C child : parent)
				{
					offset = child.getBoundingBox().calculateZOffset(aabb, offset);
				}
			}
		}
		return offset;
	}

	@Override
	public MultiBoundingBox contract(double x, double y, double z)
	{
		return expand(-x, -y, -z);
	}

	@Override
	public MultiBoundingBox copy()
	{
		MultiBoundingBox copy = new MultiBoundingBox();
		copy.minX = minX;
		copy.minY = minY;
		copy.minZ = minZ;
		copy.maxX = maxX;
		copy.maxY = maxY;
		copy.maxZ = maxZ;

		for (C child : parent)
		{
			copy.addChild(child.getBoundingBox().copy());
		}

		return copy;
	}

	@Override
	public MultiBoundingBox expand(double x, double y, double z)
	{
		MultiBoundingBox copy = new MultiBoundingBox();
		copy.minX = minX - x;
		copy.minY = minY - y;
		copy.minZ = minZ - z;
		copy.maxX = maxX + x;
		copy.maxY = maxY + y;
		copy.maxZ = maxZ + z;

		for (C child : parent)
		{
			copy.addChild(child.getBoundingBox().expand(x, y, z));
		}

		return copy;
	}

	@Override
	public MultiBoundingBox getOffsetBoundingBox(double x, double y, double z)
	{
		MultiBoundingBox copy = new MultiBoundingBox();
		copy.minX = minX + x;
		copy.minY = minY + y;
		copy.minZ = minZ + z;
		copy.maxX = maxX + x;
		copy.maxY = maxY + y;
		copy.maxZ = maxZ + z;

		for (C child : parent)
		{
			copy.addChild(child.getBoundingBox().getOffsetBoundingBox(x, y, z));
		}

		return copy;
	}

	@Override
	public boolean intersectsWith(AxisAlignedBB aabb)
	{
		if (super.intersectsWith(aabb))
		{
			if (aabb instanceof MultiBoundingBox)
			{
				return intersectsWithMBB((MultiBoundingBox)aabb);
			}
			else if (aabb instanceof VTBoundingBox)
			{
				return intersectsWithVTB((VTBoundingBox<?, ?>)aabb);
			}
			else
			{
				for (C child : parent)
				{
					if (child.getBoundingBox().intersectsWith(aabb))
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Determines whether the given multi-bounding box intersects with this box.
	 * 
	 * @param mbb - {@link zoranodensha.api.vehicles.boundingBox.MultiBoundingBox MultiBoundingBox} to check for intersection.
	 * @return {@code true} if it intersects with this box, {@code false} otherwise.
	 */
	private boolean intersectsWithMBB(MultiBoundingBox mbb)
	{
		for (AxisAlignedBB childOther : mbb)
		{
			for (C childThis : parent)
			{
				if (childThis.getBoundingBox().intersectsWith(childOther))
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Determines whether the given VT-bounding box intersects with this box.
	 * 
	 * @param vtbb - {@link zoranodensha.api.vehicles.boundingBox.VTBoundingBox VTBoundingBox} to check for intersection.
	 * @return {@code true} if it intersects with this box, {@code false} otherwise.
	 */
	private <C1 extends IBoundingBoxProvider, P1 extends Iterable<C1>> boolean intersectsWithVTB(VTBoundingBox<C1, P1> vtbb)
	{
		for (C1 childOther : vtbb.parent)
		{
			for (C childThis : parent)
			{
				if (childThis.getBoundingBox().intersectsWith(childOther.getBoundingBox()))
				{
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean isVecInside(Vec3 vec3)
	{
		if (super.isVecInside(vec3))
		{
			for (C child : parent)
			{
				if (child.getBoundingBox().isVecInside(vec3))
				{
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public VTBoundingBox<C, P> offset(double x, double y, double z)
	{
		this.minX += x;
		this.minY += y;
		this.minZ += z;
		this.maxX += x;
		this.maxY += y;
		this.maxZ += z;

		for (C child : parent)
		{
			child.getBoundingBox().offset(x, y, z);
		}

		return this;
	}

	@Override
	public void onUpdate()
	{
		/*
		 * We create an empty bounding box where its minimum values are the biggest double values, and its maximum values are
		 * the smallest double values. Therefore, any other bounding box matched against this box will override this box' bounds.
		 */
		AxisAlignedBB aabb = AxisAlignedBB.getBoundingBox(MAX_VALUE, MAX_VALUE, MAX_VALUE, -MAX_VALUE, -MAX_VALUE, -MAX_VALUE);

		/* Retrieve smallest minimum and largest maximum values from all child boxes. */
		boolean isInitialised = false;
		for (C child : parent)
		{
			aabb = child.getBoundingBox().func_111270_a(aabb);
			isInitialised = true;
		}

		/* Override if either value changed. */
		if (isInitialised)
		{
			setBB(aabb);

			/* Also update the World's entity radius if this is a train's bounding box. */
			if (parent instanceof Train)
			{
				double max = (maxX - minX);
				max = Math.max(max, maxY - minY);
				max = Math.max(max, maxZ - minZ);

				if (World.MAX_ENTITY_RADIUS < max)
				{
					World.MAX_ENTITY_RADIUS = MathHelper.ceiling_double_int(max);
					APILogger.info(this, String.format("Increased World#MAX_ENTITY_RADIUS to %.4f", World.MAX_ENTITY_RADIUS));
				}
			}
		}
	}

	@Override
	public void setBB(AxisAlignedBB aabb)
	{
		super.setBB(aabb);

		/*
		 * If the other box is an MBB, just assume it has the same number of
		 * children and assign their boxes to our children's boxes.
		 */
		if (aabb instanceof MultiBoundingBox)
		{
			Iterator<AxisAlignedBB> iteraOther = ((MultiBoundingBox)aabb).iterator();
			Iterator<C> iteraThis = parent.iterator();

			while (iteraThis.hasNext() && iteraOther.hasNext())
			{
				iteraThis.next().getBoundingBox().setBB(iteraOther.next());
			}
		}
	}

	@Override
	public String toString()
	{
		return String.format("%s[Parent=%s | (%.2fx %.2fy %.2fz) -> (%.2fx %.2fy %.2fz)]", getClass().getSimpleName(), parent, minX, minY, minZ, maxX, maxY, maxZ);
	}

	/**
	 * Assign the given bounding box to the given train's bounding box field.
	 *
	 * @param train - {@link Train Train} to place the bounding box into.
	 * @param vtbb - {@link VTBoundingBox Bounding box} to set.
	 */
	public static final void setTrainVTBB(Train train, VTBoundingBox<VehicleData, Train> vtbb) throws Exception
	{
		/* Determine the field name, depending on the environment. */
		final String fieldName = Boolean.TRUE.equals(Launch.blackboard.get("fml.deobfuscatedEnvironment")) ? "boundingBox" : "field_70121_D";

		/* Get the field from inside the class. Throw an exception of retrieval failed. */
		Field boundingBox = Entity.class.getDeclaredField(fieldName);
		if (boundingBox == null)
		{
			throw new NullPointerException(String.format("Couldn't find field '%s' to override train bounding box!", fieldName));
		}

		/* Access and override the field. */
		boundingBox.setAccessible(true);
		boundingBox.set(train, vtbb);
	}
}