package zoranodensha.api.vehicles.boundingBox;

import static java.lang.Double.MAX_VALUE;

import java.util.ArrayList;
import java.util.Iterator;

import javax.annotation.Nonnull;

import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;



/**
 * <h1>Multi Bounding Box</h1>
 * <hr>
 * 
 * <p>
 * Handles a list of child boxes, where each child may also be an instance of this class.
 * This offers the possibility to create a tree of bounding boxes, where each level of children is wrapped by a parent box
 * which is used to check whether any of the children are applicable for a method call.
 * </p>
 * 
 * <p>
 * Unlike {@link zoranodensha.api.vehicles.boundingBox.VTBoundingBox Vehicle-Train Bounding Boxes}, this box keeps references
 * to the children's actual bounding boxes.
 * </p>
 * 
 * @author Leshuwa Kaiheiwa
 */
public class MultiBoundingBox extends AxisAlignedBB implements IBoundingBoxUpdateReceiver, Iterable<AxisAlignedBB>
{
	/** This bounding box' child boxes. */
	private final ArrayList<AxisAlignedBB> children = new ArrayList<AxisAlignedBB>();



	/**
	 * Create an empty multi-bounding box.
	 */
	public MultiBoundingBox()
	{
		super(0, 0, 0, 0, 0, 0);
	}


	/**
	 * Adds the given bounding box as child to this multi-bounding box.
	 *
	 * @param child - The {@link net.minecraft.util.AxisAlignedBB bounding box} to add.
	 */
	public void addChild(AxisAlignedBB child)
	{
		children.add(child);
	}
	
	@Override
	public MultiBoundingBox addCoord(double x, double y, double z)
	{
		MultiBoundingBox copy = new MultiBoundingBox();
		copy.minX = (x < 0.0) ? (minX + x) : minX;
		copy.minY = (y < 0.0) ? (minY + y) : minY;
		copy.minZ = (z < 0.0) ? (minZ + z) : minZ;
		copy.maxX = (x > 0.0) ? (maxX + x) : maxX;
		copy.maxY = (y > 0.0) ? (maxY + y) : maxY;
		copy.maxZ = (z > 0.0) ? (maxZ + z) : maxZ;
		
		for (AxisAlignedBB child : this)
		{
			copy.addChild(child.addCoord(x, y, z));
		}
		
		return copy;
	}

	@Override
	public MovingObjectPosition calculateIntercept(Vec3 vecPos, Vec3 vecLook)
	{
		MovingObjectPosition movObjPos = super.calculateIntercept(vecPos, vecLook);
		if (movObjPos != null)
		{
			for (AxisAlignedBB box : this)
			{
				movObjPos = box.calculateIntercept(vecPos, vecLook);
				if (movObjPos != null)
				{
					return movObjPos;
				}
			}
		}
		return null;
	}

	@Override
	public double calculateXOffset(AxisAlignedBB aabb, double offset)
	{
		if (aabb.maxY > minY && aabb.minY < maxY)
		{
			if (aabb.maxZ > minZ && aabb.minZ < maxZ)
			{
				for (AxisAlignedBB box : this)
				{
					offset = box.calculateXOffset(aabb, offset);
				}
			}
		}
		return offset;
	}

	@Override
	public double calculateYOffset(AxisAlignedBB aabb, double offset)
	{
		if (aabb.maxX > minX && aabb.minX < maxX)
		{
			if (aabb.maxZ > minZ && aabb.minZ < maxZ)
			{
				for (AxisAlignedBB box : this)
				{
					offset = box.calculateYOffset(aabb, offset);
				}
			}
		}
		return offset;
	}

	@Override
	public double calculateZOffset(AxisAlignedBB aabb, double offset)
	{
		if (aabb.maxX > minX && aabb.minX < maxX)
		{
			if (aabb.maxY > minY && aabb.minY < maxY)
			{
				for (AxisAlignedBB box : this)
				{
					offset = box.calculateZOffset(aabb, offset);
				}
			}
		}
		return offset;
	}

	@Override
	public MultiBoundingBox contract(double x, double y, double z)
	{
		return expand(-x, -y, -z);
	}

	@Override
	public MultiBoundingBox copy()
	{
		MultiBoundingBox copy = new MultiBoundingBox();
		copy.minX = minX;
		copy.minY = minY;
		copy.minZ = minZ;
		copy.maxX = maxX;
		copy.maxY = maxY;
		copy.maxZ = maxZ;

		for (AxisAlignedBB child : this)
		{
			copy.addChild(child.copy());
		}

		return copy;
	}

	/**
	 * Clears the list of {@link #children}, cascading the call down to the end of its sub-tree.
	 */
	public void clear()
	{
		for (AxisAlignedBB child : this)
		{
			if (child instanceof MultiBoundingBox)
			{
				((MultiBoundingBox)child).clear();
			}
		}

		children.clear();
	}

	@Override
	public MultiBoundingBox expand(double x, double y, double z)
	{
		MultiBoundingBox copy = new MultiBoundingBox();
		copy.minX = minX - x;
		copy.minY = minY - y;
		copy.minZ = minZ - z;
		copy.maxX = maxX + x;
		copy.maxY = maxY + y;
		copy.maxZ = maxZ + z;

		for (AxisAlignedBB child : children)
		{
			copy.addChild(child.expand(x, y, z));
		}

		return copy;
	}

	@Override
	public MultiBoundingBox getOffsetBoundingBox(double x, double y, double z)
	{
		MultiBoundingBox copy = new MultiBoundingBox();
		copy.minX = minX + x;
		copy.minY = minY + y;
		copy.minZ = minZ + z;
		copy.maxX = maxX + x;
		copy.maxY = maxY + y;
		copy.maxZ = maxZ + z;

		for (AxisAlignedBB child : this)
		{
			copy.addChild(child.getOffsetBoundingBox(x, y, z));
		}

		return copy;
	}

	/**
	 * Returns the number of children of this Multi Bounding Box.
	 */
	public int getChildCount()
	{
		return children.size();
	}

	@Override
	public boolean intersectsWith(AxisAlignedBB aabb)
	{
		if (super.intersectsWith(aabb))
		{
			if (aabb instanceof MultiBoundingBox)
			{
				return intersectsWithMBB((MultiBoundingBox)aabb);
			}
			else if (aabb instanceof VTBoundingBox)
			{
				return intersectsWithVTB((VTBoundingBox<?, ?>)aabb);
			}
			else
			{
				for (AxisAlignedBB child : this)
				{
					if (child.intersectsWith(aabb))
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Determines whether the given multi-bounding box intersects with this box.
	 * 
	 * @param mbb - {@link zoranodensha.api.vehicles.boundingBox.MultiBoundingBox MultiBoundingBox} to check for intersection.
	 * @return {@code true} if it intersects with this box, {@code false} otherwise.
	 */
	private boolean intersectsWithMBB(MultiBoundingBox mbb)
	{
		for (AxisAlignedBB childOther : mbb)
		{
			for (AxisAlignedBB childThis : this)
			{
				if (childThis.intersectsWith(childOther))
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Determines whether the given VT-bounding box intersects with this box.
	 * 
	 * @param vtbb - {@link zoranodensha.api.vehicles.boundingBox.VTBoundingBox VTBoundingBox} to check for intersection.
	 * @return {@code true} if it intersects with this box, {@code false} otherwise.
	 */
	private <C extends IBoundingBoxProvider, P extends Iterable<C>> boolean intersectsWithVTB(VTBoundingBox<C, P> vtbb)
	{
		for (C childOther : vtbb.parent)
		{
			for (AxisAlignedBB childThis : this)
			{
				if (childThis.intersectsWith(childOther.getBoundingBox()))
				{
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean isVecInside(Vec3 vec3)
	{
		if (super.isVecInside(vec3))
		{
			for (AxisAlignedBB child : this)
			{
				if (child.isVecInside(vec3))
				{
					return true;
				}
			}
		}
		return false;
	}

	@Override
	@Nonnull
	public Iterator<AxisAlignedBB> iterator()
	{
		/*
		 * Wrapped iterator to allow read-only access from outside.
		 */
		return new Iterator<AxisAlignedBB>()
		{
			private final Iterator<AxisAlignedBB> wrappedIterator = children.iterator();



			@Override
			public boolean hasNext()
			{
				return wrappedIterator.hasNext();
			}

			@Override
			public AxisAlignedBB next()
			{
				return wrappedIterator.next();
			}

			@Override
			public void remove()
			{
				throw new UnsupportedOperationException();
			}
		};
	}

	@Override
	public MultiBoundingBox offset(double x, double y, double z)
	{
		this.minX += x;
		this.minY += y;
		this.minZ += z;
		this.maxX += x;
		this.maxY += y;
		this.maxZ += z;

		for (AxisAlignedBB child : this)
		{
			child.offset(x, y, z);
		}

		return this;
	}

	@Override
	public void onUpdate()
	{
		/*
		 * We create an empty bounding box where its minimum values are the biggest double values, and its maximum values are
		 * the smallest double values. Therefore, any other bounding box matched against this box will override this box' bounds.
		 */
		AxisAlignedBB aabb = AxisAlignedBB.getBoundingBox(MAX_VALUE, MAX_VALUE, MAX_VALUE, -MAX_VALUE, -MAX_VALUE, -MAX_VALUE);

		/* Retrieve smallest minimum and largest maximum values from all child boxes. */
		boolean isInitialised = false;
		for (AxisAlignedBB child : this)
		{
			if (child instanceof MultiBoundingBox)
			{
				((MultiBoundingBox)child).onUpdate();
			}

			aabb = child.func_111270_a(aabb);
			isInitialised = true;
		}

		/* Override if either value changed. */
		if (isInitialised)
		{
			setBB(aabb);
		}
	}

	@Override
	public void setBB(AxisAlignedBB aabb)
	{
		super.setBB(aabb);

		/*
		 * If the other box is an MBB, just assume it has the same number of
		 * children and assign their boxes to our children.
		 */
		if (aabb instanceof MultiBoundingBox)
		{
			Iterator<AxisAlignedBB> iteraOther = ((MultiBoundingBox)aabb).iterator();
			Iterator<AxisAlignedBB> iteraThis = iterator();

			while (iteraThis.hasNext() && iteraOther.hasNext())
			{
				iteraThis.next().setBB(iteraOther.next());
			}
		}
	}

	@Override
	public String toString()
	{
		return String.format("%s[Children=%d | (%.2fx %.2fy %.2fz) -> (%.2fx %.2fy %.2fz)]", getClass().getSimpleName(), getChildCount(), minX, minY, minZ, maxX, maxY, maxZ);
	}
}