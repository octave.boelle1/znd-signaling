package zoranodensha.api.vehicles.boundingBox;

import java.util.ArrayList;

import net.minecraft.util.AxisAlignedBB;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.vehicleParts.common.parts.basic.VehParBasicText;



/**
 * <h1>Section Bounding Box</h1>
 * <hr>
 *
 * <p>
 * Bounding box dedicated to a specific {@link zoranodensha.api.vehicles.VehicleSection vehicle section}.<br> Contains a bounding box B-tree, where each tree node contains up to B {@link MultiBoundingBox entries}.
 * </p>
 *
 * <p>
 * <b>Asymptotic complexity</b>
 * Let {@code B} be the maximum {@link zoranodensha.api.vehicles.boundingBox.SectionBoundingBox#MAX_NUM_ELEMENTS number}, of elements and {@code K} be the lowest level of the B-tree. Furthermore, let {@code V} be the number of vehicles in a
 * train.<br> Assume a ray trace which has at least one result.<br> In worst-case complexity, the first such result is found after {@code 1 + V + B*K} comparisons.
 * </p>
 *
 * @author Leshuwa Kaiheiwa
 */
public class SectionBoundingBox extends MultiBoundingBox
{
	/** Maximum number of {@link zoranodensha.api.vehicles.part.VehParBase vehicle parts} per multi-bounding box. */
	public static final int MAX_NUM_ELEMENTS = 8;

	/** This box' parent {@link zoranodensha.api.vehicles.VehicleSection vehicle section}. */
	private final VehicleSection section;


	/**
	 * Create a new section bounding box with the given vehicle section as its parent.
	 *
	 * @param section - Parent {@link zoranodensha.api.vehicles.VehicleSection vehicle section} of this box.
	 */
	public SectionBoundingBox(VehicleSection section)
	{
		this.section = section;
		initialise();
	}

	/**
	 * Called to initialise this bounding box tree hierarchy.
	 */
	private void initialise()
	{
		/*
		 * Clear previous tree data and initialise new data.
		 */
		clear();

		ArrayList<AxisAlignedBB> upperLevel = new ArrayList<AxisAlignedBB>();
		ArrayList<AxisAlignedBB> currentLevel = new ArrayList<AxisAlignedBB>();
		{
			for (VehParBase part : section.parts)
			{
				if (!part.getCollides())
				{
					continue;
				}

				currentLevel.add(part.getBoundingBox());
			}
		}

		/*
		 * Build the tree bottom-up.
		 */
		while (currentLevel.size() > SectionBoundingBox.MAX_NUM_ELEMENTS)
		{
			/*
			 * Compress the current level into nodes on the upper level.
			 */
			MultiBoundingBox upperLevelNode = new MultiBoundingBox();

			/* For each element on the current tree level.. */
			while (!currentLevel.isEmpty())
			{
				/* ..append to the upper level's node.. */
				upperLevelNode.addChild(currentLevel.remove(0));

				/* ..and flush the upper level's node if required. */
				if (upperLevelNode.getChildCount() >= MAX_NUM_ELEMENTS || currentLevel.isEmpty())
				{
					upperLevel.add(upperLevelNode);

					if (!currentLevel.isEmpty())
					{
						upperLevelNode = new MultiBoundingBox();
					}
				}
			}

			/*
			 * The upper level is the next iteration's current level. Swap list pointers.
			 */
			ArrayList<AxisAlignedBB> tmp = currentLevel;
			currentLevel = upperLevel;
			upperLevel = tmp;
			upperLevel.clear();
		}

		/*
		 * The current level now contains children of the tree's root; append them to this node.
		 */
		for (AxisAlignedBB child : currentLevel)
		{
			addChild(child);
		}
	}
}