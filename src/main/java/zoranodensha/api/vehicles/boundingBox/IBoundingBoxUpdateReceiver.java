package zoranodensha.api.vehicles.boundingBox;

/**
 * Interface used by Zora no Densha trains in their bounding box hierarchy to receive bounding box updates.  
 * 
 * @author Leshuwa Kaiheiwa
 */
public interface IBoundingBoxUpdateReceiver
{
	/**
	 * Update this bounding box and its children, then recalculate the surrounding box.
	 */
	void onUpdate();
}