package zoranodensha.client;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.resources.IReloadableResourceManager;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.client.resources.IResourcePack;
import net.minecraft.client.resources.SimpleReloadableResourceManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.common.MinecraftForge;
import org.apache.logging.log4j.Level;
import paulscode.sound.SoundSystemConfig;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.type.seat.EntitySeat;
import zoranodensha.api.vehicles.part.xml.XMLResourcePack;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.client.cfg.ModConfigClient;
import zoranodensha.client.gui.table.model.VehicleModel;
import zoranodensha.client.render.entity.EntityRendererSCP;
import zoranodensha.client.render.entity.EntityRendererTrackOutline;
import zoranodensha.client.render.entity.EntityRendererVehicleModel;
import zoranodensha.client.render.items.ItemBlockRenderer;
import zoranodensha.client.render.items.ItemBlueprintRenderer;
import zoranodensha.client.render.items.ItemPartRenderer;
import zoranodensha.client.render.items.ItemSignalRenderer;
import zoranodensha.client.render.items.ItemSpeedSignRenderer;
import zoranodensha.client.render.items.ItemVehicleRenderer;
import zoranodensha.client.render.tileEntity.TESRBallastGravel;
import zoranodensha.client.render.tileEntity.TESREngineerTable;
import zoranodensha.client.render.tileEntity.TESRPlatform;
import zoranodensha.client.render.tileEntity.TESRRailwaySign;
import zoranodensha.client.render.tileEntity.TESRRefinery;
import zoranodensha.client.render.tileEntity.TESRRetorter;
import zoranodensha.client.render.tileEntity.TESRSignal;
import zoranodensha.client.render.tileEntity.TESRSpeedSign;
import zoranodensha.client.render.tileEntity.TESRTrackBase;
import zoranodensha.client.render.tileEntity.TESRTrackDecoration;
import zoranodensha.common.blocks.tileEntity.TileEntityBallastGravel;
import zoranodensha.common.blocks.tileEntity.TileEntityEngineerTable;
import zoranodensha.common.blocks.tileEntity.TileEntityPlatform;
import zoranodensha.common.blocks.tileEntity.TileEntityRailwaySign;
import zoranodensha.common.blocks.tileEntity.TileEntityRefinery;
import zoranodensha.common.blocks.tileEntity.TileEntityRetorter;
import zoranodensha.common.blocks.tileEntity.TileEntitySignal;
import zoranodensha.common.blocks.tileEntity.TileEntitySpeedSign;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBaseGag;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackDecoration;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModProxyCommon;
import zoranodensha.common.core.asm.Accessors;
import zoranodensha.common.core.asm.ESrgNames;
import zoranodensha.common.core.cfg.ModConfig;
import zoranodensha.common.core.registry.ModPartRegistry;
import zoranodensha.common.entity.EntityTrackOutline;
import zoranodensha.common.entity.SCP4633;
import zoranodensha.common.util.Helpers;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Cross10_1131_1131;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Cross5_1131_1131;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Cross_0000_0000;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Cross_0000_1131;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Cross_4500_4500;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Curve10_1843_4500;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Curve_0000_1131;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Curve_1131_1843;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Curve_1843_4500;
import zoranodensha.trackpack.client.RailwaySectionRenderer_SCurve_0000_0000;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Straight2_0000_0000;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Straight2_4500_4500;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Straight4_0000_0000;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Straight4_4500_4500;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Straight8_0000_0000;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Straight8_4500_4500;
import zoranodensha.trackpack.client.RailwaySectionRenderer_StraightBuffer_0000_0000;
import zoranodensha.trackpack.client.RailwaySectionRenderer_StraightBuffer_1131_1131;
import zoranodensha.trackpack.client.RailwaySectionRenderer_StraightBuffer_1843_1843;
import zoranodensha.trackpack.client.RailwaySectionRenderer_StraightBuffer_4500_4500;
import zoranodensha.trackpack.client.RailwaySectionRenderer_StraightMaint_0000_0000;
import zoranodensha.trackpack.client.RailwaySectionRenderer_StraightSlope16_0000_0000;
import zoranodensha.trackpack.client.RailwaySectionRenderer_StraightSlope_0000_0000;
import zoranodensha.trackpack.client.RailwaySectionRenderer_StraightTrans_0000_0000;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Straight_0000_0000;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Straight_1131_1131;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Straight_1843_1843;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Straight_4500_4500;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Switch_0000_1131;
import zoranodensha.trackpack.client.RailwaySectionRenderer_Switch_1131_0000;
import zoranodensha.trackpack.common.section.ZnD_Cross10_1131_1131;
import zoranodensha.trackpack.common.section.ZnD_Cross5_1131_1131;
import zoranodensha.trackpack.common.section.ZnD_Cross_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Cross_0000_1131;
import zoranodensha.trackpack.common.section.ZnD_Cross_4500_4500;
import zoranodensha.trackpack.common.section.ZnD_Curve10_1843_4500;
import zoranodensha.trackpack.common.section.ZnD_Curve_0000_1131;
import zoranodensha.trackpack.common.section.ZnD_Curve_1131_1843;
import zoranodensha.trackpack.common.section.ZnD_Curve_1843_4500;
import zoranodensha.trackpack.common.section.ZnD_SCurve_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Straight2_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Straight2_4500_4500;
import zoranodensha.trackpack.common.section.ZnD_Straight4_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Straight4_4500_4500;
import zoranodensha.trackpack.common.section.ZnD_Straight8_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Straight8_4500_4500;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_1131_1131;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_1843_1843;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_4500_4500;
import zoranodensha.trackpack.common.section.ZnD_StraightMaint_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_StraightSlope16_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_StraightSlope_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_StraightTrans_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Straight_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Straight_1131_1131;
import zoranodensha.trackpack.common.section.ZnD_Straight_1843_1843;
import zoranodensha.trackpack.common.section.ZnD_Straight_4500_4500;
import zoranodensha.trackpack.common.section.ZnD_Switch_0000_1131;
import zoranodensha.trackpack.common.section.ZnD_Switch_1131_0000;
import zoranodensha.vehicleParts.presets.APreset;
import zoranodensha.vehicleParts.presets.BR101;



@SideOnly(Side.CLIENT)
public class ModProxyClient extends ModProxyCommon
{
	@Override
	public ModConfig getModConfig()
	{
		return new ModConfigClient();
	}

	@Override
	public EntityPlayer getPlayer(MessageContext context)
	{
		return (Side.CLIENT.equals(context.side) ? Minecraft.getMinecraft().thePlayer : super.getPlayer(context));
	}

	@Override
	@SuppressWarnings("unchecked")
	public void registerAddOns()
	{
		/* Append resource pack for XML-based vehicle parts to Minecraft using reflection. */
		try
		{
			XMLResourcePack.addToList((List<IResourcePack>)Accessors.getField(Minecraft.getMinecraft(), ESrgNames.F_defaultResourcePacks).get(Minecraft.getMinecraft()));

			IResourceManager resourceManager = Minecraft.getMinecraft().getResourceManager();
			if (resourceManager instanceof SimpleReloadableResourceManager)
			{
				((SimpleReloadableResourceManager)resourceManager).reloadResourcePack(XMLResourcePack.INSTANCE);
			}
			else
			{
				ModCenter.log.warn(String.format("Unable to reload %s: Minecraft resource manager isn't of type %s", XMLResourcePack.class.getSimpleName(), SimpleReloadableResourceManager.class.getSimpleName()));
			}
		}
		catch (Exception e)
		{
			ModCenter.log.warn("Unable to prepare XML-based vehicle parts: ", e);
		}

		/* Now call super class to register everything else (including XML-based parts). */
		super.registerAddOns();

		/* Phase 4: Parse Preset data */
		parsePresets();
	}

	@Override
	protected void registerDefault_IRailwaySection(ArrayList<IRailwaySection> iRailwaySections)
	{
		iRailwaySections.add(new ZnD_Cross_0000_0000(new RailwaySectionRenderer_Cross_0000_0000()));
		iRailwaySections.add(new ZnD_Cross_0000_1131(new RailwaySectionRenderer_Cross_0000_1131()));
		iRailwaySections.add(new ZnD_Cross_4500_4500(new RailwaySectionRenderer_Cross_4500_4500()));
		iRailwaySections.add(new ZnD_Cross10_1131_1131(new RailwaySectionRenderer_Cross10_1131_1131()));
		iRailwaySections.add(new ZnD_Cross5_1131_1131(new RailwaySectionRenderer_Cross5_1131_1131()));
		iRailwaySections.add(new ZnD_Curve_0000_1131(new RailwaySectionRenderer_Curve_0000_1131()));
		iRailwaySections.add(new ZnD_Curve_1131_1843(new RailwaySectionRenderer_Curve_1131_1843()));
		iRailwaySections.add(new ZnD_Curve_1843_4500(new RailwaySectionRenderer_Curve_1843_4500()));
		iRailwaySections.add(new ZnD_Curve10_1843_4500(new RailwaySectionRenderer_Curve10_1843_4500()));
		iRailwaySections.add(new ZnD_SCurve_0000_0000(new RailwaySectionRenderer_SCurve_0000_0000()));
		iRailwaySections.add(new ZnD_Straight_0000_0000(new RailwaySectionRenderer_Straight_0000_0000()));
		iRailwaySections.add(new ZnD_Straight_1131_1131(new RailwaySectionRenderer_Straight_1131_1131()));
		iRailwaySections.add(new ZnD_Straight_1843_1843(new RailwaySectionRenderer_Straight_1843_1843()));
		iRailwaySections.add(new ZnD_Straight_4500_4500(new RailwaySectionRenderer_Straight_4500_4500()));
		iRailwaySections.add(new ZnD_Straight2_4500_4500(new RailwaySectionRenderer_Straight2_4500_4500()));
		iRailwaySections.add(new ZnD_Straight4_4500_4500(new RailwaySectionRenderer_Straight4_4500_4500()));
		iRailwaySections.add(new ZnD_Straight8_4500_4500(new RailwaySectionRenderer_Straight8_4500_4500()));
		iRailwaySections.add(new ZnD_Straight2_0000_0000(new RailwaySectionRenderer_Straight2_0000_0000()));
		iRailwaySections.add(new ZnD_Straight4_0000_0000(new RailwaySectionRenderer_Straight4_0000_0000()));
		iRailwaySections.add(new ZnD_Straight8_0000_0000(new RailwaySectionRenderer_Straight8_0000_0000()));
		iRailwaySections.add(new ZnD_StraightMaint_0000_0000(new RailwaySectionRenderer_StraightMaint_0000_0000()));
		iRailwaySections.add(new ZnD_StraightBuffer_0000_0000(new RailwaySectionRenderer_StraightBuffer_0000_0000()));
		iRailwaySections.add(new ZnD_StraightBuffer_1131_1131(new RailwaySectionRenderer_StraightBuffer_1131_1131()));
		iRailwaySections.add(new ZnD_StraightBuffer_1843_1843(new RailwaySectionRenderer_StraightBuffer_1843_1843()));
		iRailwaySections.add(new ZnD_StraightBuffer_4500_4500(new RailwaySectionRenderer_StraightBuffer_4500_4500()));
		iRailwaySections.add(new ZnD_StraightSlope_0000_0000(new RailwaySectionRenderer_StraightSlope_0000_0000()));
		iRailwaySections.add(new ZnD_StraightSlope16_0000_0000(new RailwaySectionRenderer_StraightSlope16_0000_0000()));
		iRailwaySections.add(new ZnD_StraightTrans_0000_0000(new RailwaySectionRenderer_StraightTrans_0000_0000()));
		iRailwaySections.add(new ZnD_Switch_0000_1131(new RailwaySectionRenderer_Switch_0000_1131()));
		iRailwaySections.add(new ZnD_Switch_1131_0000(new RailwaySectionRenderer_Switch_1131_0000()));
	}

	@Override
	public void registerEventHandlers()
	{
		MinecraftForge.EVENT_BUS.register(new ClientEventHandler());
		FMLCommonHandler.instance().bus().register(new ClientEventHandler());
	}

	@Override
	public void registerRenderers()
	{
		/* Tile Entity Special Renderers */
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBallastGravel.class, new TESRBallastGravel());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityEngineerTable.class, new TESREngineerTable());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityPlatform.class, new TESRPlatform());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityRailwaySign.class, new TESRRailwaySign());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityRefinery.class, new TESRRefinery());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityRetorter.class, new TESRRetorter());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySignal.class, new TESRSignal());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySpeedSign.class, new TESRSpeedSign());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTrackDecoration.class, new TESRTrackDecoration());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTrackBase.class, new TESRTrackBase());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTrackBaseGag.class, new TESRTrackBase());

		/* Item Renderers */
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModCenter.BlockBallastGravelCorner), new ItemBlockRenderer(new TESRBallastGravel()));
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModCenter.BlockEngineerTable), new ItemBlockRenderer(new TESREngineerTable()));
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(ModCenter.BlockPlatform), new ItemBlockRenderer(new TESRPlatform()));
		MinecraftForgeClient.registerItemRenderer(ModCenter.ItemEngineersBlueprint, new ItemBlueprintRenderer());
		MinecraftForgeClient.registerItemRenderer(ModCenter.ItemTrain, new ItemVehicleRenderer());
		MinecraftForgeClient.registerItemRenderer(ModCenter.ItemVehiclePart, new ItemPartRenderer());
		MinecraftForgeClient.registerItemRenderer(ModCenter.ItemSignal, new ItemSignalRenderer());
		MinecraftForgeClient.registerItemRenderer(ModCenter.ItemSpeedSign, new ItemSpeedSignRenderer());

		/* Entity Renderers */
		RenderingRegistry.registerEntityRenderingHandler(EntityTrackOutline.class, new EntityRendererTrackOutline());
		RenderingRegistry.registerEntityRenderingHandler(VehicleModel.class, new EntityRendererVehicleModel());
		RenderingRegistry.registerEntityRenderingHandler(Train.class, new TrainRenderer());
		RenderingRegistry.registerEntityRenderingHandler(EntitySeat.class, new Render()
		{
			@Override
			public void doRender(Entity entity, double x, double y, double z, float partialTick, float f)
			{
				;
			}

			@Override
			protected ResourceLocation getEntityTexture(Entity entity)
			{
				return null;
			}
		});

		/* Halloween easter eggs. */
		if (ModCenter.cfg.easterEggs.enableHalloween)
		{
			RenderingRegistry.registerEntityRenderingHandler(SCP4633.class, new EntityRendererSCP());
		}

		/*
		 * Routine to increase the limit of simultaneous sound effects in the game.
		 */
		final int soundChannelMultiplier = 4; // Only adjust this value
		final int normalChannelTarget = 28 * soundChannelMultiplier;
		final int streamingChannelTarget = 4 * soundChannelMultiplier;
		if (SoundSystemConfig.getNumberNormalChannels() < normalChannelTarget)
		{
			int previousCount = SoundSystemConfig.getNumberNormalChannels();
			SoundSystemConfig.setNumberNormalChannels(normalChannelTarget);
			ModCenter.log.printf(Level.INFO, "Increased Sound System normal channel count from %d to %d", previousCount, normalChannelTarget);
		}
		if (SoundSystemConfig.getNumberStreamingChannels() < streamingChannelTarget)
		{
			int previousCount = SoundSystemConfig.getNumberStreamingChannels();
			SoundSystemConfig.setNumberStreamingChannels(streamingChannelTarget);
			ModCenter.log.printf(Level.INFO, "Increased Sound System streaming channel count from %d to %d", previousCount, streamingChannelTarget);
		}

		/* Register resource manager reload listener. */
		((IReloadableResourceManager)Minecraft.getMinecraft().getResourceManager()).registerReloadListener(ResourceManagerReloadListener.INSTANCE);
	}

	public static void parsePresets()
	{
		/* Add all default presets. */
		ArrayList<NBTTagCompound> defaults = APreset.getPresetList(false);
		for (NBTTagCompound nbt : defaults)
		{
			nbt.setInteger(ModPartRegistry.PRESET_KEY, 2);
		}

		/* Parse /Add-Ons for further presets. */
		Iterator<File> iteraFiles = Helpers.parseFor(ModPartRegistry.PRESET_SUFFIX, ModCenter.DIR_ADDONS, 5).iterator();
		ArrayList<NBTTagCompound> nbtList = new ArrayList<NBTTagCompound>();
		NBTTagCompound nbt;
		File file;

		while (iteraFiles.hasNext())
		{
			nbt = ModPartRegistry.presetImport(file = iteraFiles.next());
			if (nbt != null)
			{
				nbt.setInteger(ModPartRegistry.PRESET_KEY, 1);
				nbtList.add(nbt);
			}
			else
			{
				ModCenter.log.warn("[ModProxy] Failed to load Preset. (File: " + (file != null ? file.getAbsolutePath() : "null") + ")");
			}
		}
		nbtList.addAll(defaults);

		/* Convert all presets to ItemStacks. */
		ArrayList<ItemStack> itemList = new ArrayList<ItemStack>();
		ItemStack itemStack;

		for (int i = nbtList.size() - 1; i >= 0; --i)
		{
			nbt = nbtList.get(i);
			itemStack = new ItemStack(ModCenter.ItemTrain, 0, 0);
			itemStack.setTagCompound(nbt);
			itemList.add(itemStack);
		}
		TileEntityEngineerTable.presetsImport = itemList;

		/* Initialise display ItemStack. */
		ModCreativeTabs.modTabGenStack = new ItemStack(ModCenter.ItemTrain, 1, 0);
		ModCreativeTabs.modTabGenStack.setTagCompound((new BR101(true)).nbt);
	}
}
