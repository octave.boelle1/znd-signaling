package zoranodensha.client.render.tileEntity;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import zoranodensha.common.blocks.BlockRefinery;
import zoranodensha.common.blocks.tileEntity.TileEntityRefinery;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class TESRRefinery extends TileEntitySpecialRenderer
{
	private static final ResourceLocation textures = new ResourceLocation(ModData.ID, "textures/blocks/refinery.png");
	private static final IModelCustom model = AdvancedModelLoader.loadModel(new ResourceLocation(ModData.ID, "models/blocks/refinery.obj"));



	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float scale)
	{
		if (tileEntity instanceof TileEntityRefinery && tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) == BlockRefinery.HEIGHT)
		{
			bindTexture(textures);

			GL11.glPushMatrix();
			GL11.glTranslated(x + 0.5D, y + 1.0D - BlockRefinery.HEIGHT, z + 0.5D);

			if (ModCenter.cfg.rendering.detailOther > 0)
			{
				model.renderAll();
			}
			else
			{
				model.renderPart("cube");
			}

			GL11.glPopMatrix();
		}
	}
}
