package zoranodensha.client.render.tileEntity;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import zoranodensha.common.blocks.tileEntity.TileEntityBallastGravel;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class TESRBallastGravel extends TileEntitySpecialRenderer
{
	private static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/blocks/blockBallastGravel.png");
	private static final IModelCustom model = AdvancedModelLoader.loadModel(new ResourceLocation(ModData.ID, "models/blocks/ballastGravel.obj"));
	private static final String[] parts = ModCenter.cfg.rendering.detailOther > 0 ? new String[] { "end_fancy", "outer_fancy", "inner_fancy" } : new String[] { "end", "outer", "inner" };



	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float scale)
	{
		if (tileEntity instanceof TileEntityBallastGravel)
		{
			int meta = tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord);

			if (meta > 0)
			{
				GL11.glPushMatrix();
				GL11.glTranslated(x + 0.5D, y, z + 0.5D);
				GL11.glRotatef((meta % 2 != 0) ? ((meta % 4 * 90.0F) - 180.0F) : (meta % 4 * 90.0F), 0.0F, 1.0F, 0.0F);
				bindTexture(texture);
				model.renderPart(parts[--meta / 4]);
				GL11.glPopMatrix();
			}
		}
	}
}
