package zoranodensha.client.render.tileEntity.models;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;



@SideOnly(Side.CLIENT)
public class ModelEngineerTable extends ModelBase
{
	ModelRenderer Stand1;
	ModelRenderer Stand2;
	ModelRenderer Stand3;
	ModelRenderer Stand4;
	ModelRenderer Stand5;
	ModelRenderer Table;



	public ModelEngineerTable()
	{
		textureWidth = 64;
		textureHeight = 32;

		Stand1 = new ModelRenderer(this, 0, 11);
		Stand1.addBox(-7F, -1F, 0F, 14, 1, 1);
		Stand1.setRotationPoint(0F, 0F, 0F);
		Stand1.setTextureSize(64, 32);
		Stand1.mirror = true;
		setRotation(Stand1, 0F, 0F, 0F);
		Stand2 = new ModelRenderer(this, 0, 13);
		Stand2.addBox(-7.5F, -4F, 0F, 1, 12, 1);
		Stand2.setRotationPoint(0F, 0F, 0F);
		Stand2.setTextureSize(64, 32);
		Stand2.mirror = true;
		setRotation(Stand2, 0F, 0F, 0F);
		Stand3 = new ModelRenderer(this, 0, 13);
		Stand3.addBox(6.5F, -4F, 0F, 1, 12, 1);
		Stand3.setRotationPoint(0F, 0F, 0F);
		Stand3.setTextureSize(64, 32);
		Stand3.mirror = true;
		setRotation(Stand3, 0F, 0F, 0F);
		Stand4 = new ModelRenderer(this, 0, 13);
		Stand4.addBox(0F, 0F, 0F, 1, 10, 1);
		Stand4.setRotationPoint(-7.5F, 8F, -6F);
		Stand4.setTextureSize(64, 32);
		Stand4.mirror = true;
		setRotation(Stand4, 1.570796F, 0F, 0F);
		Stand5 = new ModelRenderer(this, 0, 13);
		Stand5.addBox(0F, 0F, 0F, 1, 10, 1);
		Stand5.setRotationPoint(6.5F, 8F, -6F);
		Stand5.setTextureSize(64, 32);
		Stand5.mirror = true;
		setRotation(Stand5, 1.570796F, 0F, 0F);
		Table = new ModelRenderer(this, 0, 0);
		Table.addBox(-8F, 0F, -6F, 16, 1, 10);
		Table.setRotationPoint(0F, -4.8F, 0F);
		Table.setTextureSize(64, 32);
		Table.mirror = true;
		setRotation(Table, 0.2094395F, 0F, 0F);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);

		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		Stand1.render(f5);
		Stand2.render(f5);
		Stand3.render(f5);
		Stand4.render(f5);
		Stand5.render(f5);
		Table.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
	{
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	}
}
