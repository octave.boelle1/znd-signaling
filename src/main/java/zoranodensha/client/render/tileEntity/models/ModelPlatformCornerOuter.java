package zoranodensha.client.render.tileEntity.models;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;



@SideOnly(Side.CLIENT)
public class ModelPlatformCornerOuter extends ModelBase
{
	ModelRenderer Base;
	ModelRenderer FrontPanel1A;
	ModelRenderer FrontPanel1B;
	ModelRenderer FrontPanel2A;
	ModelRenderer FrontPanel2B;



	public ModelPlatformCornerOuter()
	{
		textureWidth = 64;
		textureHeight = 64;

		Base = new ModelRenderer(this, 0, 0);
		Base.addBox(0F, -4F, 0F, 8, 12, 8);
		Base.setRotationPoint(0F, 0F, 0F);
		Base.setTextureSize(64, 64);
		Base.mirror = true;
		setRotation(Base, 0F, 0F, 0F);
		FrontPanel1A = new ModelRenderer(this, 0, 33);
		FrontPanel1A.addBox(-2F, 3F, -2F, 9, 5, 2);
		FrontPanel1A.setRotationPoint(0F, 0F, 0F);
		FrontPanel1A.setTextureSize(64, 64);
		FrontPanel1A.mirror = true;
		setRotation(FrontPanel1A, 0F, 0F, 0F);
		FrontPanel1B = new ModelRenderer(this, 0, 20);
		FrontPanel1B.addBox(-2F, 3F, 0F, 2, 5, 7);
		FrontPanel1B.setRotationPoint(0F, 0F, 0F);
		FrontPanel1B.setTextureSize(64, 64);
		FrontPanel1B.mirror = true;
		setRotation(FrontPanel1B, 0F, 0F, 0F);
		FrontPanel2A = new ModelRenderer(this, 0, 40);
		FrontPanel2A.addBox(-2F, -4F, -2F, 10, 2, 2);
		FrontPanel2A.setRotationPoint(0F, 0F, 0F);
		FrontPanel2A.setTextureSize(64, 64);
		FrontPanel2A.mirror = true;
		setRotation(FrontPanel2A, 0F, 0F, 0F);
		FrontPanel2B = new ModelRenderer(this, 0, 44);
		FrontPanel2B.addBox(-2F, -4F, 0F, 2, 2, 8);
		FrontPanel2B.setRotationPoint(0F, 0F, 0F);
		FrontPanel2B.setTextureSize(64, 64);
		FrontPanel2B.mirror = true;
		setRotation(FrontPanel2B, 0F, 0F, 0F);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		Base.render(f5);
		FrontPanel1A.render(f5);
		FrontPanel1B.render(f5);
		FrontPanel2A.render(f5);
		FrontPanel2B.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
	{
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	}

}
