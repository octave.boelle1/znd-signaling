package zoranodensha.client.render.tileEntity.models;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;



@SideOnly(Side.CLIENT)
public class ModelRailwaySign extends ModelBase
{
	public ModelRenderer Pillar;
	public ModelRenderer PillarTop;
	public ModelRenderer SignTop;
	public ModelRenderer SignTopLckT;
	public ModelRenderer SignTopLckB;
	public ModelRenderer SignBot;
	public ModelRenderer SignBotLckT;
	public ModelRenderer SignBotLckB;



	public ModelRailwaySign()
	{
		textureWidth = 128;
		textureHeight = 128;

		Pillar = new ModelRenderer(this, 0, 0);
		Pillar.addBox(-1F, -32F, -1F, 2, 56, 2);
		Pillar.setRotationPoint(0F, 0F, 0F);
		Pillar.setTextureSize(128, 128);
		Pillar.mirror = true;
		setRotation(Pillar, 0F, 0F, 0F);
		PillarTop = new ModelRenderer(this, 0, 0);
		PillarTop.addBox(0F, 0F, 0F, 1, 1, 1);
		PillarTop.setRotationPoint(-0.5F, -32.5F, -0.5F);
		PillarTop.setTextureSize(128, 128);
		PillarTop.mirror = true;
		setRotation(PillarTop, 0F, 0F, 0F);
		SignTop = new ModelRenderer(this, 9, 5);
		SignTop.addBox(-3F, -28F, -2F, 6, 12, 1);
		SignTop.setRotationPoint(0F, 0F, 0F);
		SignTop.setTextureSize(128, 128);
		SignTop.mirror = true;
		setRotation(SignTop, 0F, 0F, 0F);
		SignTopLckT = new ModelRenderer(this, 9, 0);
		SignTopLckT.addBox(-1.5F, -15F, -1.7F, 3, 1, 3);
		SignTopLckT.setRotationPoint(0F, 0F, 0F);
		SignTopLckT.setTextureSize(128, 128);
		SignTopLckT.mirror = true;
		setRotation(SignTopLckT, 0F, 0F, 0F);
		SignTopLckB = new ModelRenderer(this, 9, 0);
		SignTopLckB.addBox(-1.5F, -6F, -1.7F, 3, 1, 3);
		SignTopLckB.setRotationPoint(0F, 0F, 0F);
		SignTopLckB.setTextureSize(128, 128);
		SignTopLckB.mirror = true;
		setRotation(SignTopLckB, 0F, 0F, 0F);
		SignBot = new ModelRenderer(this, 9, 19);
		SignBot.addBox(-3F, -16F, -2F, 6, 12, 1);
		SignBot.setRotationPoint(0F, 0F, 0F);
		SignBot.setTextureSize(128, 128);
		SignBot.mirror = true;
		setRotation(SignBot, 0F, 0F, 0F);
		SignBotLckT = new ModelRenderer(this, 9, 0);
		SignBotLckT.addBox(-1.5F, -27F, -1.7F, 3, 1, 3);
		SignBotLckT.setRotationPoint(0F, 0F, 0F);
		SignBotLckT.setTextureSize(128, 128);
		SignBotLckT.mirror = true;
		setRotation(SignBotLckT, 0F, 0F, 0F);
		SignBotLckB = new ModelRenderer(this, 9, 0);
		SignBotLckB.addBox(-1.5F, -18F, -1.7F, 3, 1, 3);
		SignBotLckB.setRotationPoint(0F, 0F, 0F);
		SignBotLckB.setTextureSize(128, 128);
		SignBotLckB.mirror = true;
		setRotation(SignBotLckB, 0F, 0F, 0F);
	}

	public ModelRenderer getTextureField(ModelRailwaySign model, int textureX, int textureY, float posY)
	{
		ModelRenderer texField = new ModelRenderer(model, textureX, textureY);
		texField.addBox(-3F, posY, -2.01F, 6, 12, 0);
		texField.setRotationPoint(0F, 0F, 0F);
		texField.setTextureSize(128, 128);
		texField.mirror = true;
		model.setRotation(texField, 0F, 0F, 0F);

		return texField;
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		super.render(entity, f, f1, f2, f3, f4, f5);

		setRotationAngles(f, f1, f2, f3, f4, f5, entity);
		Pillar.render(f5);
		PillarTop.render(f5);
		SignTop.render(f5);
		SignTopLckT.render(f5);
		SignTopLckB.render(f5);
		SignBot.render(f5);
		SignBotLckT.render(f5);
		SignBotLckB.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z)
	{
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	@Override
	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity entity)
	{
		super.setRotationAngles(f, f1, f2, f3, f4, f5, entity);
	}
}
