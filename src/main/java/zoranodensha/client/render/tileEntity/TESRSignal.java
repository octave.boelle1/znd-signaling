package zoranodensha.client.render.tileEntity;

import java.util.ArrayList;

import net.minecraftforge.client.IItemRenderer;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import zoranodensha.api.structures.signals.ISignal;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.client.render.items.ItemSignalRenderer;
import zoranodensha.common.blocks.tileEntity.TileEntitySignal;
import zoranodensha.common.core.ModData;
import zoranodensha.signals.SignalLamp;
import zoranodensha.vehicleParts.client.render.RenderUtil;



@SideOnly(Side.CLIENT)
public class TESRSignal extends ABackfallTESR
{
	private static final IModelCustom flareModel = AdvancedModelLoader.loadModel(new ResourceLocation(ModData.ID, "models/signals/ModelFX_Flare.obj"));
	private static final ResourceLocation textureAntiVignette = new ResourceLocation(ModData.ID, "textures/vehicleParts/lamp/antivignette.png");



	/**
	 * Helper method, used on clients only, to get the distance from the client-side
	 * player to the specified tile entity.
	 * 
	 * @param tileEntity - The tile entity to measure the distance to.
	 * @return - The distance from the client-side player to the tile entity, as a
	 *         {@code float}.
	 */
	@SideOnly(Side.CLIENT)
	public static float getDistanceFromPlayer(TileEntity tileEntity)
	{
		return (float)Vec3	.createVectorHelper(Minecraft.getMinecraft().thePlayer.posX, Minecraft.getMinecraft().thePlayer.posY, Minecraft.getMinecraft().thePlayer.posZ)
							.distanceTo(Vec3.createVectorHelper(tileEntity.xCoord + 0.5D, tileEntity.yCoord, tileEntity.zCoord + 0.5D));
	}

	/**
	 * Helper method to render a signal lamp flare.
	 * 
	 * @param signal - The {@link zoranodensha.api.structures.signals.ISignal}
	 *            instance of the signal.
	 * @param flareX - The X position of the surface of the lamp.
	 * @param flareY - The Y position of the surface of the lamp.
	 * @param flareZ - The Z position of the surface of the lamp.
	 * @param lamp - The {@link zoranodensha.signals.SignalLamp} instance of
	 *            the particular lamp to be rendered.
	 * @param partialTick - The current partial tick state. This will allow the
	 *            animations to appear smoother.
	 * @param angle - The angle (in degrees) to tilt the flare backwards or
	 *            forwards to adjust for signal tilt.
	 */
	@SideOnly(Side.CLIENT)
	public static void renderLampFlare(ISignal signal, SignalLamp lamp, float flareX, float flareY, float flareZ, float partialTick, float angle)
	{
		if (MinecraftForgeClient.getRenderPass() < 1)
			return;
		if (lamp.getIntensity(partialTick) <= 0.09F)
			return;

		/*
		 * Get the distance from the player to the signal.
		 */
		float distance = getDistanceFromPlayer(signal.getTileEntity());

		/*
		 * If the distance is far enough, render the lamp flare.
		 */
		float minimumDistance = 2.0F;
		if (distance > minimumDistance)
		{

			// Determine the magnitude of the flare, based on the distance.
			float magnitude = (distance - minimumDistance) / 25.0F;
			// Clamp.
			if (magnitude > 1.0F)
				magnitude = 1.0F;

			/*
			 * Negative-check as a fail-safe.
			 */
			if (magnitude > 0.0F)
			{
				GL11.glPushMatrix();
				{
					GL11.glPushAttrib(GL11.GL_ALPHA_TEST_FUNC);
					GL11.glPushAttrib(GL11.GL_ALPHA_TEST_REF);
					GL11.glPushAttrib(GL11.GL_LIGHTING);
					GL11.glPushAttrib(GL11.GL_BLEND);

					RenderUtil.lightmapPush();
					{
						RenderUtil.lightmapBright();

						// Disable lighting & enable alpha blending.
						GL11.glAlphaFunc(GL11.GL_GREATER, 0.01F);
						GL11.glDisable(GL11.GL_LIGHTING);
						GL11.glEnable(GL11.GL_BLEND);

						ColorRGBA lampColor = ColorRGBA.WHITE;
						lampColor = new ColorRGBA(lamp.getColor().getR(), lamp.getColor().getG(), lamp.getColor().getB(), lamp.getColor().getA());
						lampColor.setAlpha(lamp.getIntensity(partialTick) * 0.75F * magnitude);

						// Incandescent Bulb
						if (!lamp.getLampType())
						{
							lampColor.setAlpha(lampColor.getA() * 0.5F);
						}

						lampColor.apply();

						/*
						 * Flare Translation
						 */
						GL11.glTranslatef(flareX, flareY, flareZ);

						/*
						 * Flare Rotation
						 */
						GL11.glRotatef(angle, 0.0F, 0.0F, 1.0F);

						// Render the lamp.
						Minecraft.getMinecraft().renderEngine.bindTexture(textureAntiVignette);
						flareModel.renderAll();

					}
					RenderUtil.lightmapPop();

					GL11.glPopAttrib(); /* GL11.GL_BLEND */
					GL11.glPopAttrib(); /* GL11.GL_LIGHTING */
					GL11.glPopAttrib(); /* GL11.GL_ALPHA_TEST_REF */
					GL11.glPopAttrib(); /* GL11.GL_ALPHA_TEST_FUNC */
				}
				GL11.glPopMatrix();
			}

		}

	}


	/**
	 * Helper method to render the signal nameplate text of a signal at the
	 * specified coordinates.
	 * 
	 * @param nameplateID - The ID to render.
	 * @param positionX - The X offset of the nameplate.
	 * @param positionY - The Y offset of the nameplate.
	 * @param positionZ - The Z offset of the nameplate.
	 * @param angle - The tilt/angle of the nameplate, along the X axis
	 *            (pitch).
	 * @param color - The colour to use when rendering the text.
	 * @param scale - A custom scale to use for the text. Set to {@code 1.0F}
	 *            to leave undisturbed.
	 */
	public static void renderSignalID(String ID, float offsetX, float offsetY, float offsetZ, float tilt, int color, float scale)
	{
		GL11.glPushMatrix();
		{
			GL11.glTranslatef(offsetX, offsetY, offsetZ);
			GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
			GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
			GL11.glRotatef(tilt, 1.0F, 0.0F, 0.0F);
			GL11.glScalef(0.008F * scale, 0.008F * scale, 0.008F * scale);

			FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
			final boolean unicode = fontRenderer.getUnicodeFlag();

			fontRenderer.drawString(ID, 0, 0, color);
			fontRenderer.setUnicodeFlag(unicode);
		}
		GL11.glPopMatrix();
	}


	/**
	 * Helper method to render the signal speed limit indicator text at the
	 * specified coordinates.
	 * 
	 * @param indication - The speed limit indication ({@code int}) to display.
	 * @param positionX - The X offset of the indicator.
	 * @param positionY - The Y offset of the indicator.
	 * @param positionZ - The Z offset of the indicator.
	 * @param warning - {@code boolean} indicating whether the indication is of
	 *            the 'warning' type, using yellow text.
	 * @param illuminated - {@code boolean} indicating whether the text should be
	 *            illuminated.
	 * @param transition - Used for animation - fades the text in and out.
	 */
	@SideOnly(Side.CLIENT)
	public void renderSignalIndicator(int indication, float positionX, float positionY, float positionZ, boolean warning, boolean illuminated, float transition)
	{
		GL11.glPushAttrib(GL11.GL_BLEND);
		{
			GL11.glEnable(GL11.GL_BLEND);

			GL11.glPushMatrix();
			{
				int alpha = (int)(transition * (illuminated ? 255 : 180));

				GL11.glTranslatef(positionX, positionY, positionZ);
				GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
				GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);

				if (indication / 10 >= 10)
				{
					GL11.glScalef(0.012F, 0.024F, 0.024F);
				}
				else
				{
					GL11.glScalef(0.024F, 0.024F, 0.024F);
				}

				if (illuminated)
				{
					GL11.glPushAttrib(GL11.GL_LIGHTING);
					RenderUtil.lightmapPush();

					GL11.glDisable(GL11.GL_LIGHTING);
					RenderUtil.lightmapBright();
				}
				{

					GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

					FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
					final boolean unicode = fontRenderer.getUnicodeFlag();

					fontRenderer.drawString(String.valueOf(indication / 10), 0, 0, warning ? 0xEEEE00 | (alpha << 24) : 0xEEEEEE | (alpha << 24));
					fontRenderer.setUnicodeFlag(unicode);

				}
				if (illuminated)
				{
					RenderUtil.lightmapPop();
					GL11.glPopAttrib();
				}
			}
			GL11.glPopMatrix();

		}
		GL11.glPopAttrib();
	}


	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float partialTick)
	{
		if (tileEntity instanceof TileEntitySignal)
		{
			ISignal signal = ((TileEntitySignal)tileEntity).getSignal();

			if (signal != null)
			{
				GL11.glPushMatrix();
				{
					/*
					 * Position
					 */
					GL11.glTranslated(x + 0.5D, y, z + 0.5D);

					/*
					 * Rotation
					 */
					int meta = tileEntity.getBlockMetadata();
					GL11.glRotatef(270.0F + (meta * -22.5F), 0.0F, 1.0F, 0.0F);

					/*
					 * Render the signal :3
					 */
					signal.render(partialTick, false);
				}
				GL11.glPopMatrix();

				/*
				 * If the F3 menu is open, we can attempt to render any signalling debug info.
				 */
				if (Minecraft.getMinecraft().gameSettings.showDebugInfo)
				{
					ArrayList<Double> debugPath = signal.getDebugPath();

					if (debugPath != null)
					{
						double pathX = 0.0D;
						double pathY = 0.0D;
						double pathZ = 0.0D;

						GL11.glPushMatrix();
						RenderUtil.lightmapPush();
						GL11.glPushAttrib(GL11.GL_ENABLE_BIT);
						{
							GL11.glDisable(GL11.GL_TEXTURE_2D);
							GL11.glDisable(GL11.GL_LIGHTING);
							GL11.glEnable(GL11.GL_LINE_SMOOTH);
							RenderUtil.lightmapBright();
							{

								switch (signal.getIndication().getBlocksFree())
								{
									case 0:
										GL11.glColor3f(1.0F, 0.0F, 0.0F);
										break;

									case 1:
										GL11.glColor3f(1.0F, 1.0F, 0.0F);
										break;

									case 2:
										GL11.glColor3f(0.0F, 1.0F, 0.0F);
										break;

									default:
										GL11.glColor3f(1.0F, 1.0F, 1.0F);
										break;
								}
								GL11.glLineWidth(3.0F);

								Entity player = Minecraft.getMinecraft().thePlayer;
								double offsetX, offsetY, offsetZ;
								{
									offsetX = player.prevPosX + ((player.posX - player.prevPosX) * partialTick);
									offsetY = player.prevPosY + ((player.posY - player.prevPosY) * partialTick);
									offsetZ = player.prevPosZ + ((player.posZ - player.prevPosZ) * partialTick);
								}
								GL11.glTranslated(-offsetX, -offsetY, -offsetZ);
								GL11.glBegin(GL11.GL_LINE_STRIP);
								{
									for (int i = 0; i < debugPath.size(); i += 3)
									{
										pathX = debugPath.get(i);
										pathY = debugPath.get(i + 1) + 0.2F + (signal.getIndication().getBlocksFree() * 0.125F);
										pathZ = debugPath.get(i + 2);

										GL11.glVertex3d(pathX, pathY, pathZ);
									}
								}
								GL11.glEnd();

							}
						}
						RenderUtil.lightmapPop();
						GL11.glPopAttrib();
						GL11.glPopMatrix();
					}
				}
			}
			else
			{
				renderQuestionMark(x, y, z);
			}
		}
		else
		{
			renderQuestionMark(x, y, z);
		}
	}

}
