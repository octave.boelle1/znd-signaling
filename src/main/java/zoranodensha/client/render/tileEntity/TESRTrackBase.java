package zoranodensha.client.render.tileEntity;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.tileentity.TileEntity;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.api.structures.tracks.ARailwaySectionRenderer;
import zoranodensha.api.structures.tracks.IRailwaySectionRenderer;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBaseGag;
import zoranodensha.common.core.ModCenter;



@SideOnly(Side.CLIENT)
public class TESRTrackBase extends ABackfallTESR
{
	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float f)
	{
		if (tileEntity instanceof ITrackBase)
		{
			IRailwaySection section = ((ITrackBase)tileEntity).getInstanceOfShape();
			if (section != null)
			{
				if (tileEntity instanceof TileEntityTrackBaseGag)
				{
					return;
				}
				
				IRailwaySectionRenderer tesr = section.getRender();
				if (tesr != null)
				{
					GL11.glPushMatrix();
					tesr.renderTrack(ModCenter.cfg.rendering.detailTracks, 0, section, (ITrackBase)tileEntity, (float)x, (float)y, (float)z, f);
					GL11.glPopMatrix();
				}
			}
			else
			{
				renderQuestionMark(x, y, z);
			}
		}
	}
}
