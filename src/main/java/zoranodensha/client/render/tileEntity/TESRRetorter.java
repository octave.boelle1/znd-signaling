package zoranodensha.client.render.tileEntity;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import zoranodensha.common.blocks.tileEntity.TileEntityRetorter;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class TESRRetorter extends TileEntitySpecialRenderer
{
	private static final ResourceLocation textures = new ResourceLocation(ModData.ID, "textures/blocks/retorter.png");
	private static final IModelCustom model = AdvancedModelLoader.loadModel(new ResourceLocation(ModData.ID, "models/blocks/retorter.obj"));



	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float scale)
	{
		if (tileEntity instanceof TileEntityRetorter && tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) < 2)
		{
			bindTexture(textures);

			GL11.glPushMatrix();
			GL11.glTranslated(x + 0.5D, y, z + 0.5D);
			GL11.glRotatef((tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) % 2) * 90.0F + 90.0F, 0.0F, 1.0F, 0.0F);

			if (ModCenter.cfg.rendering.detailOther > 0)
			{
				model.renderAll();
			}
			else
			{
				model.renderPart("cube");
			}

			GL11.glPopMatrix();
		}
	}
}
