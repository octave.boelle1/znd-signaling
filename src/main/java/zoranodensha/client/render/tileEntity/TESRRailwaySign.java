package zoranodensha.client.render.tileEntity;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import zoranodensha.client.render.tileEntity.models.ModelRailwaySign;
import zoranodensha.common.blocks.BlockRailwaySign;
import zoranodensha.common.blocks.tileEntity.TileEntityRailwaySign;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class TESRRailwaySign extends TileEntitySpecialRenderer
{
	private static final ResourceLocation textures = new ResourceLocation(ModData.ID, "textures/blocks/railwaySign.png");
	private static final ModelRailwaySign model = new ModelRailwaySign();



	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float scale)
	{
		if (tileEntity instanceof TileEntityRailwaySign && tileEntity.getWorldObj().getBlockMetadata(tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord) == 0)
		{
			TileEntityRailwaySign tileEntitySign = (TileEntityRailwaySign)tileEntity;
			ModelRenderer texField;

			bindTexture(textures);

			GL11.glPushMatrix();
			{
				GL11.glTranslated(x + 0.5D, y + 0.75D, z + 0.5D);
				GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
				GL11.glScalef(0.5F, 0.5F, 0.5F);

				int[] i;

				if (tileEntitySign.getSignID(0) > -1)
				{
					GL11.glPushMatrix();
					{
						model.Pillar.isHidden = true;
						model.PillarTop.isHidden = true;
						model.SignBot.isHidden = true;
						model.SignTop.isHidden = false;
						model.SignTopLckT.isHidden = true;
						model.SignTopLckB.isHidden = true;
						model.SignBotLckT.isHidden = false;
						model.SignBotLckB.isHidden = false;

						i = BlockRailwaySign.ERailwaySign.toSign(tileEntitySign.getSignID(0)).toCoordinate();

						texField = model.getTextureField(model, i[0], i[1], -28.0F);
						GL11.glRotatef(tileEntitySign.getRotationMetadata(0) % 4 * 90.0F + 180.0F, 0.0F, 1.0F, 0.0F);
						model.render(null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
						texField.render(0.0625F);
					}
					GL11.glPopMatrix();
				}

				if (tileEntitySign.getSignID(1) > -1)
				{
					GL11.glPushMatrix();
					{
						model.Pillar.isHidden = true;
						model.PillarTop.isHidden = true;
						model.SignBot.isHidden = false;
						model.SignTop.isHidden = true;
						model.SignTopLckT.isHidden = false;
						model.SignTopLckB.isHidden = false;
						model.SignBotLckT.isHidden = true;
						model.SignBotLckB.isHidden = true;

						i = BlockRailwaySign.ERailwaySign.toSign(tileEntitySign.getSignID(1)).toCoordinate();

						texField = model.getTextureField(model, i[0], i[1], -16.0F);
						GL11.glRotatef(tileEntitySign.getRotationMetadata(1) % 4 * 90.0F + 180.0F, 0.0F, 1.0F, 0.0F);
						model.render(null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
						texField.render(0.0625F);
					}
					GL11.glPopMatrix();
				}

				GL11.glPushMatrix();
				{
					model.Pillar.isHidden = false;
					model.PillarTop.isHidden = false;
					model.SignBot.isHidden = true;
					model.SignTop.isHidden = true;
					model.SignTopLckT.isHidden = true;
					model.SignTopLckB.isHidden = true;
					model.SignBotLckT.isHidden = true;
					model.SignBotLckB.isHidden = true;
					model.render(null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
				}
				GL11.glPopMatrix();
			}
			GL11.glPopMatrix();
			
			/*
			 * FIXME A memory leak is caused by something to do with this variable.
			 */
			texField = null;
		}
	}
}
