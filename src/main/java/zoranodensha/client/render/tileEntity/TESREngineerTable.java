package zoranodensha.client.render.tileEntity;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.MinecraftForgeClient;
import zoranodensha.client.render.entity.EntityRendererVehicleModel;
import zoranodensha.client.render.tileEntity.models.ModelEngineerTable;
import zoranodensha.common.blocks.tileEntity.TileEntityEngineerTable;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class TESREngineerTable extends TileEntitySpecialRenderer
{
	private static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/blocks/engineerTable.png");
	private static final ModelEngineerTable model = new ModelEngineerTable();



	@Override
	public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float f)
	{
		if (tileEntity == null)
		{
			GL11.glEnable(GL11.GL_LIGHTING);
		}
		
		Minecraft.getMinecraft().renderEngine.bindTexture(texture);

		GL11.glPushMatrix();
		GL11.glTranslated(x + 0.5D, y + 0.5D, z + 0.5D);
		GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
		
		if (tileEntity instanceof TileEntityEngineerTable)
		{
			GL11.glRotatef((tileEntity.getBlockMetadata() * 90.0F) + 180.0F, 0.0F, 1.0F, 0.0F);
			GL11.glTranslatef(0.0F, 0.0F, 0.25F);
		}
		else
		{
			GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
		}

		model.render(null, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0625F);
		GL11.glPopMatrix();

		if (tileEntity instanceof TileEntityEngineerTable)
		{
			TileEntityEngineerTable tileEntityEngineerTable = (TileEntityEngineerTable)tileEntity;
			if (!tileEntityEngineerTable.presets.isEmpty())
			{
				ItemStack itemStack = tileEntityEngineerTable.presets.get(0);
				IItemRenderer itemRenderer = MinecraftForgeClient.getItemRenderer(itemStack, ItemRenderType.ENTITY);

				if (itemRenderer != null)
				{
					int meta = tileEntity.getBlockMetadata();

					GL11.glPushMatrix();
					GL11.glTranslated(x + 0.5D, y + 0.825D, z + 0.5D);

					if (meta % 2 != 0)
					{
						GL11.glRotatef((meta * 90.0F), 0.0F, 1.0F, 0.0F);
					}
					else
					{
						GL11.glRotatef((meta * 90.0F) + 180.0F, 0.0F, 1.0F, 0.0F);
					}

					GL11.glRotatef(-10.0F, 1.0F, 0.0F, 0.0F);
					GL11.glTranslatef(0.0F, 0.0F, 0.25F);
					GL11.glScalef(0.1F, 0.1F, 0.1F);
//					GL11.glDisable(GL11.GL_LIGHTING);
					EntityRendererVehicleModel.ignorePartState = true;
					itemRenderer.renderItem(ItemRenderType.ENTITY, itemStack);
					EntityRendererVehicleModel.ignorePartState = false;
//					GL11.glEnable(GL11.GL_LIGHTING);
					GL11.glPopMatrix();
				}
			}
		}
	}
}
