package zoranodensha.client.render.items;

import org.lwjgl.opengl.GL11;

import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.structures.signals.ASignalRegistry;
import zoranodensha.api.structures.signals.ISignal;

public class ItemSignalRenderer implements IItemRenderer
{

	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type)
	{
		return type.equals(ItemRenderType.INVENTORY) || type.equals(ItemRenderType.EQUIPPED_FIRST_PERSON) || type.equals(ItemRenderType.EQUIPPED) || type.equals(ItemRenderType.ENTITY);
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack itemStack, Object... data)
	{
		ISignal item;

		String signalName = "";
		if (itemStack.hasTagCompound())
		{
			signalName = itemStack.getTagCompound().getString("name");
		}

		item = ASignalRegistry.getSignalFromName(signalName);

		if (item == null)
		{
			return;
		}

		GL11.glPushAttrib(GL11.GL_LIGHTING);
		{
			GL11.glEnable(GL11.GL_LIGHTING);

			GL11.glPushMatrix();
			{
				switch (type)
				{
					case ENTITY:
						GL11.glScalef(1.0F, 1.0F, 1.0F);
						GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
						break;

					case EQUIPPED_FIRST_PERSON:
						GL11.glScalef(0.50F, 0.50F, 0.50F);
						GL11.glRotatef(110.0F, 0.0F, 1.0F, 0.0F);
						GL11.glTranslatef(0.8F, 0.4F, 0.5F);
						break;

					case EQUIPPED:
						GL11.glScalef(0.75F, 0.75F, 0.75F);
						break;

					case INVENTORY:
						GL11.glScalef(8.0F, 8.0F, 8.0F);
						GL11.glTranslatef(1.0F, 2.0F, 0.0F);
						GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
						GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
						break;
				}

				GL11.glPushAttrib(GL11.GL_DEPTH_TEST);
				{
					GL11.glEnable(GL11.GL_DEPTH_TEST);
					item.renderItem(type);
				}
				GL11.glPopAttrib();

			}
			GL11.glPopMatrix();
		}
		GL11.glPopAttrib();

	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper)
	{
		return false;
	}

}
