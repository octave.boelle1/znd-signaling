package zoranodensha.client.render;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;



/**
 * Interface implemented by objects which may cache their vertex states during rendering.<br>
 * This is part of a slightly larger attempt to fix issues with shaders and Zora no Densha's
 * triangle-based vertex states.
 */
public interface ICachedVertexState
{
	/**
	 * Every element implementing this interface registers itself into this list.<br>
	 * Whenever a vertex state refresh happens, {@link #onVertexStateReset()} will be called on all valid elements.
	 */
	public static final ArrayList<WeakReference<ICachedVertexState>> objectList = new ArrayList<WeakReference<ICachedVertexState>>();



	/**
	 * Called to reset all vertex states of this object.<br>
	 * <i>Client-side only.</i>
	 */
	@SideOnly(Side.CLIENT)
	void onVertexStateReset();
}