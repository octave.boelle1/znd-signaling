package zoranodensha.client.cfg;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.common.core.cfg.Ctgy_BlocksItems;
import zoranodensha.common.core.cfg.Ctgy_Compat;
import zoranodensha.common.core.cfg.Ctgy_EasterEggs;
import zoranodensha.common.core.cfg.Ctgy_General;
import zoranodensha.common.core.cfg.Ctgy_WorldGen;
import zoranodensha.common.core.cfg.ModConfig;



@SideOnly(Side.CLIENT)
public class ModConfigClient extends ModConfig
{
	@Override
	public void init()
	{
		blocks_items = new Ctgy_BlocksItems();
		cabControls = new Ctgy_CabControls();
		compat = new Ctgy_Compat();
		easterEggs = new Ctgy_EasterEggs();
		general = new Ctgy_General();
		modelEditor = new Ctgy_ModelEditor();
		rendering = new Ctgy_Rendering();
		trains = new Ctgy_TrainsClient();
		worldGen = new Ctgy_WorldGen();

		blocks_items.load();
		cabControls.load();
		compat.load();
		easterEggs.load();
		general.load();
		modelEditor.load();
		rendering.load();
		trains.load();
		worldGen.load();

		if (config.hasChanged())
		{
			config.save();
		}
	}
}