package zoranodensha.client.cfg;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.client.gui.table.buttons.EModelEditorKeys;
import zoranodensha.common.core.cfg.AConfigCategory;



@SideOnly(Side.CLIENT)
public class Ctgy_ModelEditor extends AConfigCategory
{
	public boolean enableInvertMouseButtons;
	public boolean enableInvertMouseX;
	public boolean enableInvertMouseY;



	public Ctgy_ModelEditor()
	{
		super("model_editor");
	}

	@Override
	protected void loadCategory()
	{
		enableInvertMouseButtons = getBoolean("invMouseButton", false);
		enableInvertMouseX = getBoolean("invMouseX", false);
		enableInvertMouseY = getBoolean("invMouseY", false);

		for (EModelEditorKeys binding : EModelEditorKeys.values())
		{
			String val = getString(binding.desc, Keyboard.getKeyName(binding.keyDefault));
			int key = Keyboard.getKeyIndex(val.toUpperCase());

			if (key != 0)
			{
				binding.setKey(key);
			}
		}
	}
}