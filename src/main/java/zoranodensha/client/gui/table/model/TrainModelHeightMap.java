package zoranodensha.client.gui.table.model;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.util.ForgeDirection;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.ERailwayState;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.registry.ModTrackRegistry;



/**
 * A height map that the Engineer's Table GUI's train model is rendered into.
 */
@SideOnly(Side.CLIENT)
public class TrainModelHeightMap
{
	/** The map data as integer array */
	private int[][][] map = new int[][][] {
			{ { 7, 0 }, { 5, 0 }, { 5, 0 }, { 2, 0 }, { 2, 0 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 1, 0 }, { 2, 0 }, { 3, 0 }, { 3, 0 }, { 5, 0 }, { 7, 0 } },

			{ { 7, 0 }, { 5, 0 }, { 5, 0 }, { 2, 0 }, { 2, 0 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, { 3, 0 }, { 3, 0 }, { 5, 0 }, { 7, 0 } },

			{ { 8, 0 }, { 5, 0 }, { 4, 0 }, { 2, 0 }, { 2, 0 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 0, 0 }, { 1, 0 }, { 3, 0 }, { 4, 0 }, { 4, 0 }, { 6, 0 } },

			{ { 8, 0 }, { 5, 0 }, { 4, 0 }, { 2, 0 }, { 2, 0 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 1, 0 }, { 2, 0 }, { 3, 0 }, { 4, 0 }, { 5, 0 }, { 7, 0 } },

			{ { 8, 0 }, { 5, 0 }, { 4, 0 }, { 2, 0 }, { 1, 0 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 1, 0 }, { 3, 0 }, { 3, 0 }, { 4, 0 }, { 5, 0 }, { 7, 0 } },

			{ { 8, 0 }, { 6, 0 }, { 4, 0 }, { 2, 0 }, { 1, 0 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 1, 0 }, { 3, 0 }, { 3, 0 }, { 5, 0 }, { 5, 0 }, { 7, 0 } },

			{ { 8, 0 }, { 6, 0 }, { 3, 0 }, { 2, 0 }, { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 0, 0 }, { 3, 0 }, { 3, 0 }, { 5, 0 }, { 5, 0 }, { 7, 0 } },

			{ { 8, 0 }, { 6, 0 }, { 3, 0 }, { 1, 0 }, { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 0, 0 }, { 2, 0 }, { 4, 0 }, { 5, 0 }, { 6, 0 }, { 8, 0 } },

			{ { 7, 0 }, { 6, 0 }, { 3, 0 }, { 1, 0 }, { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 0, 0 }, { 2, 0 }, { 4, 0 }, { 5, 0 }, { 7, 0 }, { 8, 0 } },

			{ { 7, 0 }, { 5, 0 }, { 2, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 0, 0 }, { 2, 0 }, { 4, 0 }, { 5, 0 }, { 7, 0 }, { 8, 0 } },

			{ { 7, 0 }, { 5, 0 }, { 2, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 0, 0 }, { 3, 0 }, { 4, 0 }, { 5, 0 }, { 7, 0 }, { 8, 0 } },

			{ { 8, 0 }, { 5, 0 }, { 2, 0 }, { 1, 0 }, { 1, 0 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 1, 0 }, { 3, 0 }, { 4, 0 }, { 5, 0 }, { 6, 0 }, { 8, 0 } },

			{ { 8, 0 }, { 4, 0 }, { 2, 0 }, { 2, 0 }, { 1, 0 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 1, 0 }, { 3, 0 }, { 3, 0 }, { 5, 0 }, { 7, 0 }, { 8, 0 } },

			{ { 8, 0 }, { 4, 0 }, { 3, 0 }, { 2, 0 }, { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 0, 0 }, { 3, 0 }, { 4, 0 }, { 5, 0 }, { 7, 0 }, { 8, 0 } },

			{ { 7, 0 }, { 5, 0 }, { 3, 0 }, { 2, 0 }, { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 0, 0 }, { 3, 0 }, { 4, 0 }, { 5, 0 }, { 7, 0 }, { 8, 0 } },

			{ { 7, 0 }, { 6, 0 }, { 3, 0 }, { 2, 0 }, { 1, 0 }, { 1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 1, 0 }, { 2, 0 }, { 3, 0 }, { 5, 0 }, { 6, 0 }, { 7, 0 } },

			{ { 7, 0 }, { 6, 0 }, { 3, 0 }, { 3, 0 }, { 1, 0 }, { 0, 0 }, { 0, 0 }, { 0, 1 }, { 0, 1 }, { 0, 1 }, { 0, 0 }, { 1, 0 }, { 2, 0 }, { 3, 0 }, { 5, 0 }, { 7, 0 }, { 8, 0 } } };

	private final Block fillerBlock;
	private final RenderBlocks renderBlocks;
	private final World world;



	public TrainModelHeightMap(Block fillerBlock, World world)
	{
		this.fillerBlock = fillerBlock;
		renderBlocks = new RenderBlocks(new BlockAccess());
		this.world = world;
	}

	/**
	 * Return a block mapped to the given integer i.
	 */
	protected Block getBlock(int i)
	{
		switch (i)
		{
			default:
				return fillerBlock;

			case 0:
				return Blocks.grass;

			case 1:
				return ModCenter.BlockBallastGravel;
		}
	}

	/**
	 * Renders this height map without TileEntities.
	 */
	public void render(int renderPass)
	{
		Block block;

		GL11.glTranslatef(-(map.length / 2.0F), 0.0F, -(map[0].length / 2.0F));

		for (int x = 0; x < map.length; ++x)
		{
			for (int z = 0; z < map[x].length; ++z)
			{
				block = getBlock(map[x][z][1]);

				if (block.canRenderInPass(renderPass) && !block.hasTileEntity(0))
				{
					boolean flag = true;
					renderBlocks.setRenderAllFaces(true);

					for (int y = map[x][z][0] - 1; y >= -1; --y)
					{
						if (flag)
						{
							renderBlocks.renderBlockByRenderType(block, x, y, z);
							flag = false;
						}
						else
						{
							renderBlocks.renderBlockByRenderType(fillerBlock, x, y, z);
						}
					}
				}
			}
		}
	}

	/**
	 * Renders this height map's TileEntities and track.
	 */
	public void renderTileEntityAndTrack(int renderPass)
	{
		TileEntityTrackBase track = new TileEntityTrackBase(0, ERailwayState.FINISHED, EDirection.NONE, ModTrackRegistry.getSection("ZnD_Straight_0000_0000"));
		track.setWorldObj(world);
		track.setField(0, 0);
		track.setField(1, 0);

		GL11.glTranslatef(-(map.length / 2.0F), 0.0F, -(map[0].length / 2.0F));

		for (int x = 0; x < map.length; ++x)
		{
			for (int z = 0; z < map[x].length; ++z)
			{
				if (z == map[x].length / 2)
				{
					TileEntityRendererDispatcher.instance.renderTileEntityAt(track, x, map[x][z][0], z, 0.0F);
				}
			}
		}
	}



	protected class BlockAccess implements IBlockAccess
	{
		@Override
		public boolean extendedLevelsInChunkCache()
		{
			return false;
		}

		@Override
		public BiomeGenBase getBiomeGenForCoords(int x, int z)
		{
			return BiomeGenBase.forest;
		}

		@Override
		public Block getBlock(int x, int y, int z)
		{
			return Blocks.air;
		}

		@Override
		public int getBlockMetadata(int x, int y, int z)
		{
			return 0;
		}

		@Override
		public int getHeight()
		{
			return 128;
		}

		@Override
		@SideOnly(Side.CLIENT)
		public int getLightBrightnessForSkyBlocks(int x, int y, int z, int i)
		{
			return 15;
		}

		@Override
		public TileEntity getTileEntity(int x, int y, int z)
		{
			return null;
		}

		@Override
		public boolean isAirBlock(int x, int y, int z)
		{
			return false;
		}

		@Override
		public int isBlockProvidingPowerTo(int x, int y, int z, int i)
		{
			return 0;
		}

		@Override
		public boolean isSideSolid(int x, int y, int z, ForgeDirection side, boolean _default)
		{
			return false;
		}
	}
}
