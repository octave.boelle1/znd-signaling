package zoranodensha.client.gui.table.model;

import java.util.Iterator;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.common.util.VecDir;
import zoranodensha.common.util.ZnDMathHelper;



/**
 * Object transformation class for scaling.
 */
@SideOnly(Side.CLIENT)
public class ObjTransform_Rotate extends ObjTransform
{
	private float dotVoVn;

	private final Vector3f mousePosInit;
	private final Vector3f centerPos;
	private final Vector3f viewVec;
	private VecDir lastMouseVec;



	public ObjTransform_Rotate(int mode, float objX, float objY, float objZ, VecDir mouseVec, Camera camera, Pivot pivot, OrderedPartsList parts)
	{
		super(mode, objX, objY, objZ, pivot, parts);

		viewVec = camera.getViewVec();
		centerPos = (this.mode == 2 ? new Vector3f(this.pivot.x, this.pivot.y, this.pivot.z) : VehicleModel.getCenter(this.parts));
		refreshData(mouseVec);
		mousePosInit = mousePos;
	}

	@Override
	public void applyTransformation()
	{
		refreshParts();

		Vector3f vec0 = Vector3f.sub(centerPos, mousePosInit, null);
		Vector3f vec1 = Vector3f.sub(centerPos, mousePos, null);
		Vector3f vec2 = Vector3f.cross(vec0, vec1, null);

		Quaternion quat = Quaternion.normalise(	new Quaternion(	((axis <= 1) ? vec2.x : 0.0F), ((axis % 2 == 0) ? vec2.y : 0.0F), ((axis % 3 == 0) ? vec2.z : 0.0F),
																(vec0.length() * vec1.length()) + Vector3f.dot(vec0, vec1)),
												null);

		VehParBase part;
		Iterator<VehParBase> itera = parts.iterator();
		float[] rotation;

		refreshData(lastMouseVec);

		while (itera.hasNext())
		{
			part = itera.next();

			if (mode > 0)
			{
				rotation = part.getOffset();
				vec0 = new Vector3f(rotation[0], rotation[1], rotation[2]);
				vec1 = new Vector3f(centerPos);

				vec2 = Vector3f.sub(vec0, vec1, null);
				vec2 = rotateVecByQuat(quat, vec2);
				vec0 = vec1.translate(vec2.x, vec2.y, vec2.z);

				rotation[0] = vec0.x;
				rotation[1] = vec0.y;
				rotation[2] = vec0.z;
				part.setOffset(rotation);
			}

			float x_gimbX = (float)(Math.asin(2.0F * (quat.z * quat.y - quat.x * quat.w)) * ZnDMathHelper.DEG_MULTIPLIER);
			float y_gimbX = (float)(Math.atan2(2.0F * (quat.y * quat.w + quat.z * quat.x), 1.0F - 2.0F * (quat.y * quat.y + quat.x * quat.x)) * ZnDMathHelper.DEG_MULTIPLIER);
			float z_gimbX = (float)(Math.atan2(2.0F * (quat.z * quat.w + quat.y * quat.x), 1.0F - 2.0F * (quat.z * quat.z + quat.x * quat.x)) * ZnDMathHelper.DEG_MULTIPLIER);

			float x_gimbY = (float)(Math.atan2(2.0F * (quat.x * quat.w + quat.z * quat.y), 1.0F - 2.0F * (quat.x * quat.x + quat.y * quat.y)) * ZnDMathHelper.DEG_MULTIPLIER);
			float y_gimbY = (float)(Math.asin(2.0F * (quat.z * quat.x - quat.y * quat.w)) * ZnDMathHelper.DEG_MULTIPLIER);
			float z_gimbY = (float)(Math.atan2(2.0F * (quat.z * quat.w + quat.y * quat.x), 1.0F - 2.0F * (quat.z * quat.z + quat.y * quat.y)) * ZnDMathHelper.DEG_MULTIPLIER);

			float x_gimbZ = (float)(Math.atan2(2.0F * (quat.x * quat.w + quat.y * quat.z), 1.0F - 2.0F * (quat.x * quat.x + quat.z * quat.z)) * ZnDMathHelper.DEG_MULTIPLIER);
			float y_gimbZ = (float)(Math.atan2(2.0F * (quat.y * quat.w + quat.x * quat.z), 1.0F - 2.0F * (quat.y * quat.y + quat.z * quat.z)) * ZnDMathHelper.DEG_MULTIPLIER);
			float z_gimbZ = (float)(Math.asin(2.0F * (quat.x * quat.y - quat.z * quat.w)) * ZnDMathHelper.DEG_MULTIPLIER);

			rotation = part.getRotation();
			rotation[0] += getValueMostPresent(x_gimbX, x_gimbY, x_gimbZ, quat);
			rotation[1] += getValueMostPresent(y_gimbX, y_gimbY, y_gimbZ, quat);
			rotation[2] += getValueMostPresent(z_gimbX, z_gimbY, z_gimbZ, quat);
			part.setRotation(rotation[0], rotation[1], rotation[2]);
		}
	}

	@Override
	public boolean isClipboard()
	{
		return false;
	}

	private void refreshData(VecDir mouseVec)
	{
		dotVoVn = Vector3f.dot(centerPos, viewVec);
		updateMouse(mouseVec);
	}

	@Override
	public void renderAxis()
	{
		if (axis == 0)
		{
			return;
		}

		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glBegin(GL11.GL_LINES);

		switch (axis)
		{
			case 1:
				GL11.glColor4f(1.0F, 0.2F, 0.2F, 1.0F);
				GL11.glVertex3f(centerPos.x + 100.0F, centerPos.y, centerPos.z);
				GL11.glVertex3f(centerPos.x - 100.0F, centerPos.y, centerPos.z);
				break;

			case 2:
				GL11.glColor4f(0.2F, 1.0F, 0.2F, 1.0F);
				GL11.glVertex3f(centerPos.x, centerPos.y + 100.0F, centerPos.z);
				GL11.glVertex3f(centerPos.x, centerPos.y - 100.0F, centerPos.z);
				break;

			case 3:
				GL11.glColor4f(0.2F, 0.2F, 1.0F, 1.0F);
				GL11.glVertex3f(centerPos.x, centerPos.y, centerPos.z + 100.0F);
				GL11.glVertex3f(centerPos.x, centerPos.y, centerPos.z - 100.0F);
				break;
		}

		GL11.glEnd();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glDisable(GL11.GL_BLEND);
	}

	@Override
	public void updateMouse(VecDir vec)
	{
		lastMouseVec = vec;
		mousePos = vec.getOffset(vec.getOnNormal((dotVoVn - Vector3f.dot(viewVec, vec.point)) / Vector3f.dot(viewVec, vec.normal)));
	}
}