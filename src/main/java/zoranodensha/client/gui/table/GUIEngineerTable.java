package zoranodensha.client.gui.table;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import zoranodensha.api.structures.EEngineerTableTab;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.client.gui.table.buttons.ButtonEngineerTable;
import zoranodensha.client.gui.table.buttons.ButtonPopUpDragDownMenu;
import zoranodensha.client.gui.table.buttons.ButtonPopUpExitConfirm;
import zoranodensha.client.gui.table.buttons.EButtonDragDownType;
import zoranodensha.client.gui.table.buttons.IButtonPopUp;
import zoranodensha.client.gui.table.model.TrainModelRender;
import zoranodensha.client.gui.table.model.VehicleModel;
import zoranodensha.client.render.entity.EntityRendererVehicleModel;
import zoranodensha.common.blocks.tileEntity.TileEntityEngineerTable;
import zoranodensha.common.containers.ContainerEngineerTable;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.network.packet.gui.PacketGUIEngineerTable;



@SideOnly(Side.CLIENT)
public class GUIEngineerTable extends GuiContainer
{
	public static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/guis/engineerTableGui.png");
	public static int mouseOffX;
	public static int mouseOffY;
	private boolean isAction;
	private boolean isPostModelRender;
	private boolean ignoreSlotClicks;
	private int lastMouseButton = -1;
	private TileEntityEngineerTable tileEntity;


	private TrainModelRender trainRender = new TrainModelRender();

	public IButtonPopUp buttonPopUp;



	public GUIEngineerTable(EntityPlayer player, TileEntityEngineerTable tileEntity)
	{
		this(player.inventory, tileEntity);

		tileEntity.tab = EEngineerTableTab.TRAINS;
		ItemStack itemStack = player.getHeldItem();

		if (itemStack != null && ModCenter.ItemTrain.equals(itemStack.getItem()) && itemStack.hasTagCompound())
		{
			NBTTagCompound nbt = itemStack.getTagCompound();

			if (nbt.hasKey(Train.EDataKey.VEHICLE_DATA.key) && !nbt.getBoolean(Train.EDataKey.IS_EPIC.key))
			{
				trainRender.model.readFromNBT(nbt);
			}
		}
	}

	public GUIEngineerTable(InventoryPlayer inventory, TileEntityEngineerTable tileEntity)
	{
		super(new ContainerEngineerTable(inventory, tileEntity));

		this.tileEntity = tileEntity;
		xSize = 176;
		ySize = 218;
	}

	@Override
	protected void actionPerformed(GuiButton button)
	{
		int lastPage;

		switch (button.id)
		{
			case 0:
				lastPage = tileEntity.page;
				--tileEntity.page;

				if (tileEntity.page < 0)
				{
					tileEntity.page = 0;
				}

				if (lastPage != tileEntity.page)
				{
					ModCenter.snw.sendToServer(new PacketGUIEngineerTable((byte)3));
				}
				break;

			case 1:
				lastPage = tileEntity.page;
				++tileEntity.page;

				int max = tileEntity.getPageLimit();

				if (tileEntity.page > max)
				{
					tileEntity.page = max;
				}

				if (lastPage != tileEntity.page)
				{
					ModCenter.snw.sendToServer(new PacketGUIEngineerTable((byte)4));
				}
				break;

			default:
				if (buttonPopUp != null)
				{
					buttonPopUp.actionPerformed(this, button);
					ignoreSlotClicks = true;
				}
				else
				{
					trainRender.buttonPartEditL.actionPerformed(this, button);
					trainRender.buttonPartEditR.actionPerformed(this, button);
				}
				break;
		}
	}

	/**
	 * Clears the model, selection, and transformation data.
	 */
	public void clearModel()
	{
		VehicleModel model = getRender().model;
		model.clearPartsList();
		model.selection.clear();
		model.objTrans = null;

		trainRender.setChangesMade(false);
		trainRender.undoList.clear();
	}

	@Override
	public void drawDefaultBackground()
	{
		if (!isPostModelRender)
		{
			super.drawDefaultBackground();
		}
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int x, int y)
	{
		if (isPostModelRender)
		{
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			return;
		}

		int xPos = getXPos();
		int yPos = getYPos();

		GL11.glPushMatrix();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

		mc.getTextureManager().bindTexture(texture);
		drawTexturedModalRect(xPos, yPos, 0, 0, xSize, ySize);
		drawTexturedModalRect(xPos + 98 + (25 * tileEntity.tab.toInt()), yPos + 4, 232, 13, 24, 24);

		if (tileEntity.isProcessing())
		{
			drawTexturedModalRect(xPos + 47, yPos + 110, 237, 0, tileEntity.getProgress() + 1, 13);
		}

		if (tileEntity.tab.isTrainTab())
		{
			GL11.glColor4f(0.25F, 0.25F, 0.25F, 1.0F);

			for (int i = 0; i < 5; ++i)
			{
				drawTexturedModalRect(xPos + 6, yPos + 6 + (i * 18), 3, ySize, 90, 18);
			}
		}

		GL11.glPopMatrix();
	}

	@Override
	public void drawScreen(int x, int y, float f)
	{
		isPostModelRender = false;
		EntityRendererVehicleModel.ignorePartState = true;
		super.drawScreen(x, y, f);
		EntityRendererVehicleModel.ignorePartState = false;

		if (tileEntity.tab.isTrainTab())
		{
			float scaleX = (float)mc.displayWidth / (float)width;
			float scaleY = (float)mc.displayHeight / (float)height;

			trainRender.drawScreen(f, getXPos() + 6, getYPos() + 123, scaleX, scaleY);
			isPostModelRender = true;

			EntityRendererVehicleModel.ignorePartState = true;
			super.drawScreen(x, y, f);
			EntityRendererVehicleModel.ignorePartState = false;

			trainRender.renderSelectionData(this, f, getXPos() + 7, getYPos() + 135, scaleX, scaleY, x, y);

			GL11.glDisable(GL12.GL_RESCALE_NORMAL);
			RenderHelper.disableStandardItemLighting();
			GL11.glDisable(GL11.GL_DEPTH_TEST);

			if (buttonPopUp != null)
			{
				GL11.glPushMatrix();
				buttonPopUp.draw(x, y, this);
				GL11.glPopMatrix();
			}

			GL11.glEnable(GL11.GL_DEPTH_TEST);
			RenderHelper.enableStandardItemLighting();
			GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		}
	}

	@Override
	protected boolean func_146978_c(int slotX, int slotY, int sizeX, int sizeY, int mouseX, int mouseY)
	{
		if (trainRender.renderSelectionData())
		{
			int xPos = getXPos();
			int yPos = getYPos();

			if (mouseX >= xPos + 7 && mouseX <= xPos + xSize - 7 && mouseY >= yPos + 135 && mouseY <= yPos + ySize - 7)
			{
				return false;
			}
		}

		return (buttonPopUp == null && super.func_146978_c(slotX, slotY, sizeX, sizeY, mouseX, mouseY));
	}

	public TrainModelRender getRender()
	{
		return trainRender;
	}

	public int getXPos()
	{
		return (width - xSize) / 2;
	}

	public int getYPos()
	{
		return (height - ySize) / 2;
	}

	@Override
	protected void handleMouseClick(Slot slot, int slotID, int i, int j)
	{
		if (!ignoreSlotClicks)
		{
			super.handleMouseClick(slot, slotID, i, j);
		}
	}

	@Override
	public void handleInput()
	{
		if (tileEntity.tab.isTrainTab())
		{
			int deltaM = Mouse.getDWheel();
			float deltaX = mc.mouseHelper.deltaX = Mouse.getDX();
			float deltaY = mc.mouseHelper.deltaY = Mouse.getDY();
			int lastMouseX = Mouse.getX();
			int lastMouseY = Mouse.getY();

			ScaledResolution scaledResolution = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
			int mouseX = (lastMouseX * scaledResolution.getScaledWidth() / mc.displayWidth) - getXPos();
			int mouseY = (scaledResolution.getScaledHeight() - lastMouseY * scaledResolution.getScaledHeight() / mc.displayHeight - 1) - getYPos();

			if (getRender().model.objTrans == null)
			{
				mouseOffX = 0;
				mouseOffY = 0;
			}

			if (mouseX < 6 || mouseX > 96 || mouseY < 6 || mouseY > 96)
			{
				if (isAction || (getRender().model.objTrans != null && !getRender().model.objTrans.isClipboard()))
				{
					int off = 90 * scaledResolution.getScaleFactor();
					boolean wasGrabbed = Mouse.isGrabbed();
					Mouse.setGrabbed(false);

					if (mouseX < 6)
					{
						Mouse.setCursorPosition(lastMouseX + off, lastMouseY);
						mouseOffX -= off;
					}
					else if (mouseX > 96)
					{
						Mouse.setCursorPosition(lastMouseX - off, lastMouseY);
						mouseOffX += off;
					}
					else if (mouseY < 6)
					{
						Mouse.setCursorPosition(lastMouseX, lastMouseY - off);
						mouseOffY += off;
					}
					else if (mouseY > 96)
					{
						Mouse.setCursorPosition(lastMouseX, lastMouseY + off);
						mouseOffY -= off;
					}

					Mouse.setGrabbed(wasGrabbed);
				}
				else if (isMouseDownL())
				{
					trainRender.buttonPartEditL.mouseClickMove(mouseX, mouseY);
					trainRender.buttonPartEditR.mouseClickMove(mouseX, mouseY);
				}

				if (deltaM != 0)
				{
					final int xPos = 7;
					final int yPos = 135;

					if (mouseX > xPos && mouseX < xPos + 54 && mouseY > yPos && mouseY < yPos + 76)
					{
						trainRender.buttonPartEditL.scroll(-deltaM);
					}
					else if (mouseX > xPos + 54 && mouseX < xPos + 162 && mouseY > yPos + 18 && mouseY < yPos + 76)
					{
						trainRender.buttonPartEditR.scroll(-deltaM);
					}
				}
			}
			else
			{
				if (isAction)
				{
					if (isMouseDownL())
					{
						if (!ModCenter.cfg.modelEditor.enableInvertMouseY)
						{
							deltaY = -deltaY;
						}

						if (!ModCenter.cfg.modelEditor.enableInvertMouseX)
						{
							deltaX = -deltaX;
						}

						trainRender.camera.rotate(deltaY, -deltaX);
					}
					else if (isMouseDownR())
					{
						if (!ModCenter.cfg.modelEditor.enableInvertMouseY)
						{
							deltaY = -deltaY;
						}

						if (!ModCenter.cfg.modelEditor.enableInvertMouseX)
						{
							deltaX = -deltaX;
						}

						trainRender.camera.offset(deltaX, -deltaY);
					}
				}

				if (deltaM != 0)
				{
					trainRender.camera.zoom((mc.gameSettings.mouseSensitivity + 0.2F) * deltaM * 0.01F);
				}
			}
		}

		super.handleInput();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void initGui()
	{
		super.initGui();

		int xPos = getXPos();
		int yPos = getYPos();

		buttonList.clear();
		buttonList.add(new ButtonEngineerTable(0, xPos + 108, yPos + 29, "-"));
		buttonList.add(new ButtonEngineerTable(1, xPos + 152, yPos + 29, "+"));

		if (buttonPopUp != null)
		{
			buttonPopUp.addButtons(buttonList, buttonList.size());
		}

		if (tileEntity.tab.isTrainTab())
		{
			trainRender.buttonPartEditL.addButtons(xPos + 7, yPos + 135, buttonList, buttonList.size());
			trainRender.buttonPartEditR.addButtons(xPos + 7, yPos + 135, buttonList, buttonList.size());
		}
	}

	@Override
	protected void keyTyped(char chara, int key)
	{
		if (tileEntity.tab.isTrainTab())
		{
			if (key != 1 && buttonPopUp != null)
			{
				buttonPopUp.keyTyped(chara, key);
			}
			else if (key == 1)
			{
				if (buttonPopUp != null)
				{
					buttonPopUp = null;
					initGui();
				}
				else if (trainRender.model.objTrans != null)
				{
					trainRender.selectionClearObjTrans();
				}
				else if (trainRender.getChangesMade())
				{
					buttonPopUp = new ButtonPopUpExitConfirm(2, (width / 2) - 45, (height / 2) - 16, null);
					initGui();
				}
				else
				{
					mc.thePlayer.closeScreen();
				}
			}
			else
			{
				trainRender.keyTyped(chara, key);
			}
		}
		else if (key == 1 || key == mc.gameSettings.keyBindInventory.getKeyCode())
		{
			mc.thePlayer.closeScreen();
		}
	}

	@Override
	protected void mouseClicked(int x, int y, int button)
	{
		super.mouseClicked(x, y, button);
		ignoreSlotClicks = false;

		if (buttonPopUp != null)
		{
			buttonPopUp.mouseClicked(x, y, button);
		}

		if (tileEntity.tab.isTrainTab())
		{
			trainRender.mouseClicked(x, y, button);
		}

		x -= getXPos();
		y -= getYPos();

		if (y > 4 && y < 27)
		{
			int i = (x > 98 && x < 121) ? 0 : (x > 123 && x < 146) ? 1 : (x > 148 && x < 171) ? 2 : -1;

			if (i > -1 && i != tileEntity.tab.toInt())
			{
				if (tileEntity.tab.isTrainTab() && trainRender.getChangesMade())
				{
					buttonPopUp = new ButtonPopUpExitConfirm(2, (width / 2) - 45, (height / 2) - 16, EEngineerTableTab.getFromInt(i));
					initGui();
				}
				else
				{
					tileEntity.tab = EEngineerTableTab.getFromInt(i);
					ModCenter.snw.sendToServer(new PacketGUIEngineerTable((byte)i));
				}

				return;
			}
		}

		if (!tileEntity.tab.isTrainTab() || x < 6 || x > 96 || y < 6 || y > 96)
		{
			return;
		}

		if (isMouseDownL())
		{
			lastMouseButton = 0;
		}
		else if (isMouseDownR())
		{
			lastMouseButton = 1;
		}
		else
		{
			lastMouseButton = -1;
		}
	}

	@Override
	public void mouseClickMove(int x, int y, int button, long delta)
	{
		super.mouseClickMove(x, y, button, delta);

		if (!tileEntity.tab.isTrainTab() || (x -= getXPos()) < 6 || x > 96 || (y -= getYPos()) < 6 || y > 96)
		{
			return;
		}

		if (!(isAction = buttonPopUp == null && (Mouse.isButtonDown(0) || Mouse.isButtonDown(1))))
		{
			lastMouseButton = -1;
		}
	}

	@Override
	public void mouseMovedOrUp(int x, int y, int button)
	{
		super.mouseMovedOrUp(x, y, button);

		if (!tileEntity.tab.isTrainTab())
		{
			return;
		}

		int xPos = getXPos();
		int yPos = getYPos();

		if (!isAction && buttonPopUp == null && (x -= xPos) >= 6 && x <= 96 && (y -= yPos) >= 6 && y <= 96)
		{
			getRender().rayTace(lastMouseButton, GuiScreen.isShiftKeyDown());

			if (lastMouseButton == 1)
			{
				if (trainRender.model.getVehicleParts().isEmpty())
				{
					buttonPopUp = new ButtonPopUpDragDownMenu(	null, 4, xPos + x, yPos + y, EButtonDragDownType.MENU_TOGGLE_GRID, EButtonDragDownType.MENU_TOGGLE_MODE,
																EButtonDragDownType.MENU_TOGGLE_ORTHO, EButtonDragDownType.MENU_TOGGLE_PIVOT, EButtonDragDownType.MENU_TOGGLE_WORLD);
				}
				else
				{
					EButtonDragDownType[] buttons;

					/* If there are no non-dummy bogies, don't allow saving the vehicle as preset. */
					if (trainRender.model.getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY).isEmpty())
					{
						buttons = new EButtonDragDownType[] { EButtonDragDownType.MENU_CLEAR_MODEL, EButtonDragDownType.MENU_TOGGLE_GRID, EButtonDragDownType.MENU_TOGGLE_MODE,
								EButtonDragDownType.MENU_TOGGLE_ORTHO, EButtonDragDownType.MENU_TOGGLE_PIVOT, EButtonDragDownType.MENU_TOGGLE_WORLD };
					}
					else
					{
						buttons = new EButtonDragDownType[] { EButtonDragDownType.MENU_CLEAR_MODEL, EButtonDragDownType.MENU_SAVE_AS_PRESET, EButtonDragDownType.MENU_TOGGLE_GRID,
								EButtonDragDownType.MENU_TOGGLE_MODE, EButtonDragDownType.MENU_TOGGLE_ORTHO, EButtonDragDownType.MENU_TOGGLE_PIVOT, EButtonDragDownType.MENU_TOGGLE_WORLD };
					}

					buttonPopUp = new ButtonPopUpDragDownMenu(null, 4, xPos + x, yPos + y, buttons);
				}

				initGui();
			}
		}

		lastMouseButton = -1;
		isAction = false;
	}

	public void setTab(EEngineerTableTab newTab)
	{
		tileEntity.tab = newTab;
		tileEntity.nbtTrain = null;
	}

	private static boolean isMouseDownL()
	{
		return Mouse.isButtonDown(ModCenter.cfg.modelEditor.enableInvertMouseButtons ? 1 : 0);
	}

	private static boolean isMouseDownR()
	{
		return Mouse.isButtonDown(ModCenter.cfg.modelEditor.enableInvertMouseButtons ? 0 : 1);
	}
}
