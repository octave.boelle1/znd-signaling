package zoranodensha.client.gui.table.buttons;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.resources.I18n;
import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.client.gui.table.model.TrainModelRender;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.network.packet.gui.PacketGUIEngineerTablePreset;
import zoranodensha.common.util.Helpers;



@SideOnly(Side.CLIENT)
public class ButtonPopUpSaveName implements IButtonPopUp
{
	private static final String TEXTFIELD_DEFAULT = I18n.format(ModData.ID + ".text.guiEngineer.saveName");
	private int xPos;
	private int yPos;
	private int width;
	private GuiTextField input;
	private GuiButton button;



	public ButtonPopUpSaveName(int xPos, int yPos)
	{
		this.xPos = xPos;
		this.yPos = yPos;
		width = 120;
	}

	@Override
	public void actionPerformed(GUIEngineerTable gui, GuiButton button)
	{
		if (this.button != null && button.id == this.button.id)
		{
			String name = input.getText();

			if (!Helpers.isUndefined(name))
			{
				TrainModelRender render = gui.getRender();
				NBTTagCompound nbt = new NBTTagCompound();
				String creator = Minecraft.getMinecraft().thePlayer.getCommandSenderName();

				render.selectionClear(true);
				render.model.refreshProperties();

				VehicleData vehicle = render.model.getVehicleAt(0);
				vehicle.setVehicleName(name);
				vehicle.setCreatorName(creator);

				render.model.writeEntityToNBT(nbt, ETagCall.ITEM);
				render.setChangesMade(false);

				ModCenter.snw.sendToServer(new PacketGUIEngineerTablePreset(nbt, (byte)1));
				gui.buttonPopUp = null;
				gui.initGui();
			}
		}
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addButtons(List buttonList, int index)
	{
		input = new GuiTextField(Minecraft.getMinecraft().fontRenderer, xPos + 5, yPos + 8, 85, 16);
		input.setFocused(false);
		input.setText(TEXTFIELD_DEFAULT);
		input.setTextColor(7368816);

		buttonList.add(button = new GuiButton(index, xPos + 95, yPos + 6, 20, 20, "Ok"));
	}

	@Override
	public void draw(int mouseX, int mouseY, GUIEngineerTable gui)
	{
		gui.mc.getTextureManager().bindTexture(GUIEngineerTable.texture);
		gui.drawTexturedModalRect(xPos, yPos, 0, 0, width / 2, 5);
		gui.drawTexturedModalRect(xPos + (width / 2), yPos, 176 - (width / 2), 0, width / 2, 5);
		gui.drawTexturedModalRect(xPos, yPos + 5, 0, 218, width, 22);
		gui.drawTexturedModalRect(xPos, yPos + 27, 0, 213, width / 2, 5);
		gui.drawTexturedModalRect(xPos + (width / 2), yPos + 27, 176 - (width / 2), 213, width / 2, 5);

		button.drawButton(gui.mc, mouseX, mouseY);
		input.drawTextBox();
	}

	@Override
	public void keyTyped(char c, int i)
	{
		if (input.isFocused())
		{
			input.textboxKeyTyped(c, i);
		}
	}

	@Override
	public void mouseClicked(int x, int y, int button)
	{
		input.mouseClicked(x, y, button);

		if (input.isFocused())
		{
			if (TEXTFIELD_DEFAULT.equals(input.getText()))
			{
				input.setText("");
				input.setTextColor(14737632);
			}
		}
		else if ("".equals(input.getText()))
		{
			input.setText(TEXTFIELD_DEFAULT);
			input.setTextColor(7368816);
		}
	}
}
