package zoranodensha.client.gui.table.buttons.partEdit;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.client.gui.table.model.TrainModelRender;
import zoranodensha.client.gui.table.model.VehicleModel;
import zoranodensha.common.core.ModCenter;



@SideOnly(Side.CLIENT)
public class ButtonPartEditName extends GuiButton
{
	private final TrainModelRender trainRender;
	private ItemStack itemStack = new ItemStack(ModCenter.ItemVehiclePart);
	public VehParBase currentlySelected;
	public int index;
	/** 0 - No button, 1 - Top button, -1 - Bottom button. */
	public int lastButton;



	public ButtonPartEditName(int id, int posX, int posY, TrainModelRender trainRender)
	{
		super(id, posX, posY, 106, 16, "");

		this.trainRender = trainRender;
		itemStack.setTagCompound(new NBTTagCompound());
		refreshSelected();
	}

	@Override
	public void drawButton(Minecraft mc, int x, int y)
	{
		if (visible && trainRender.renderSelectionData())
		{
			GL11.glEnable(GL11.GL_BLEND);
			OpenGlHelper.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA, GL11.GL_ONE, GL11.GL_ZERO);

			VehicleModel model = trainRender.model;
			int size = model.selection.size();
			boolean flag = model.objTrans != null && !model.objTrans.isClipboard();

			refreshSelected();

			String name = I18n.format(currentlySelected != null ? currentlySelected.getUnlocalizedName() : itemStack.getUnlocalizedName() + ".name");
			int width0 = mc.fontRenderer.getStringWidth(name);
			int width1 = mc.fontRenderer.getStringWidth("..");

			if (width0 > 48 && width0 > width1)
			{
				name = mc.fontRenderer.trimStringToWidth(name, 48 - width1).trim() + "..";
			}

			if (flag)
			{
				size += model.objTrans.parts.size();
			}

			this.drawButton(mc, x, y, xPosition, yPosition + 2, "+");
			this.drawButton(mc, x, y, xPosition, yPosition + 10, "-");
			drawString(mc.fontRenderer, (index + 1) + "/" + size, xPosition + 8, yPosition + ((height - 8) / 2) + 1, 14737632);
			drawString(mc.fontRenderer, name, xPosition + 42, yPosition + ((height - 8) / 2) + 1, 14737632);
			RenderItem.getInstance().renderItemAndEffectIntoGUI(mc.fontRenderer, mc.getTextureManager(), itemStack, xPosition + 90, yPosition + 1);

			mouseDragged(mc, x, y);
		}
	}

	private void drawButton(Minecraft mc, int x, int y, int xPos, int yPos, String s)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(GUIEngineerTable.texture);

		int width = 8;
		int height = 8;
		field_146123_n = (x >= xPos && y >= yPos && x < xPos + width && y < yPos + height);

		drawCenteredString(mc.fontRenderer, s, xPos + (width / 2), yPos + ((height - 8) / 2), (packedFGColour != 0 ? packedFGColour : !enabled ? 10526880 : field_146123_n ? 16777120 : 14737632));
	}

	@Override
	public boolean mousePressed(Minecraft mc, int x, int y)
	{
		if (enabled && visible && x >= xPosition && x < xPosition + 8 && y >= yPosition + 2)
		{
			if (y < yPosition + 10)
			{
				lastButton = 1;
			}
			else if (y < yPosition + 18)
			{
				lastButton = -1;
			}
			else
			{
				lastButton = 0;
			}
		}
		else
		{
			lastButton = 0;
		}

		return (lastButton != 0);
	}

	public void refreshSelected()
	{
		VehicleModel model = trainRender.model;
		boolean flag = model.objTrans != null && !model.objTrans.isClipboard();
		int sizeSelect = model.selection.size();
		int sizeParts = (flag ? model.objTrans.parts.size() : 0);

		if (index >= sizeSelect + sizeParts)
		{
			index = 0;
		}
		else if (index < 0)
		{
			index = sizeSelect + sizeParts;
		}

		VehParBase newPart = (index < sizeSelect) ? model.selection.get(index) : flag && sizeParts < (index - sizeSelect) ? model.objTrans.parts.get(index - sizeSelect) : null;
		if (newPart != currentlySelected)
		{
			currentlySelected = newPart;
		}

		if (currentlySelected != null)
		{
			ItemStack itemStack = new ItemStack(ModCenter.ItemVehiclePart);
			NBTTagCompound nbt = new NBTTagCompound();
			nbt.setString(VehParBase.NBT_KEY, currentlySelected.getName());
			currentlySelected.writeToNBT(nbt, ETagCall.ITEM);
			itemStack.setTagCompound(nbt);

			this.itemStack = itemStack;
		}
	}
}
