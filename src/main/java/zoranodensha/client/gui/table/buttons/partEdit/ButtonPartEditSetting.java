package zoranodensha.client.gui.table.buttons.partEdit;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiTextField;
import zoranodensha.api.structures.tracks.Expression;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.property.APartProperty;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropByte;
import zoranodensha.api.vehicles.part.property.PropCharacter;
import zoranodensha.api.vehicles.part.property.PropDouble;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.property.PropInteger;
import zoranodensha.api.vehicles.part.property.PropLong;
import zoranodensha.api.vehicles.part.property.PropShort;
import zoranodensha.api.vehicles.part.property.PropString;
import zoranodensha.client.gui.table.buttons.partEdit.PropWrapper.WrapperBoolean;
import zoranodensha.client.gui.table.buttons.partEdit.PropWrapper.WrapperByte;
import zoranodensha.client.gui.table.buttons.partEdit.PropWrapper.WrapperCharacter;
import zoranodensha.client.gui.table.buttons.partEdit.PropWrapper.WrapperDouble;
import zoranodensha.client.gui.table.buttons.partEdit.PropWrapper.WrapperFloat;
import zoranodensha.client.gui.table.buttons.partEdit.PropWrapper.WrapperInteger;
import zoranodensha.client.gui.table.buttons.partEdit.PropWrapper.WrapperLong;
import zoranodensha.client.gui.table.buttons.partEdit.PropWrapper.WrapperShort;
import zoranodensha.client.render.ICachedVertexState;



@SideOnly(Side.CLIENT)
public abstract class ButtonPartEditSetting<T> extends GuiTextField
{
	public static final float BOUND_TO_ZERO = 0.0005F;
	public static final HashMap<Class<?>, Class<?>> classMap = new HashMap<Class<?>, Class<?>>();
	public static final HashMap<Class<?>, Class<?>> typeMap = new HashMap<Class<?>, Class<?>>();

	static
	{
		classMap.put(Boolean.class, ButtonPartEditSettingBoolean.class);
		classMap.put(Boolean[].class, ButtonPartEditSettingBooleanArr.class);
		classMap.put(Byte.class, ButtonPartEditSettingByte.class);
		classMap.put(Byte[].class, ButtonPartEditSettingByteArr.class);
		classMap.put(Character.class, ButtonPartEditSettingChar.class);
		classMap.put(Character[].class, ButtonPartEditSettingCharArr.class);
		classMap.put(Double.class, ButtonPartEditSettingDouble.class);
		classMap.put(Double[].class, ButtonPartEditSettingDoubleArr.class);
		classMap.put(Float.class, ButtonPartEditSettingFloat.class);
		classMap.put(Float[].class, ButtonPartEditSettingFloatArr.class);
		classMap.put(Integer.class, ButtonPartEditSettingInteger.class);
		classMap.put(Integer[].class, ButtonPartEditSettingIntegerArr.class);
		classMap.put(Long.class, ButtonPartEditSettingLong.class);
		classMap.put(Long[].class, ButtonPartEditSettingLongArr.class);
		classMap.put(Short.class, ButtonPartEditSettingShort.class);
		classMap.put(Short[].class, ButtonPartEditSettingShortArr.class);
		classMap.put(String.class, ButtonPartEditSettingString.class);
		classMap.put(String[].class, ButtonPartEditSettingStringArr.class);
		classMap.put(ColorRGBA.class, ButtonPartEditSettingColorRGB.class);
		classMap.put(Enum.class, ButtonPartEditSettingEnum.class);

		typeMap.put(boolean[].class, WrapperBoolean.class);
		typeMap.put(byte[].class, WrapperByte.class);
		typeMap.put(char[].class, WrapperCharacter.class);
		typeMap.put(double[].class, WrapperDouble.class);
		typeMap.put(float[].class, WrapperFloat.class);
		typeMap.put(int[].class, WrapperInteger.class);
		typeMap.put(long[].class, WrapperLong.class);
		typeMap.put(short[].class, WrapperShort.class);
	}

	protected int posX;
	protected final int widthHalf;
	protected FontRenderer fontRenderer;
	protected String settingName;
	protected APartProperty<T> prop;
	protected T settingCurrent;



	public ButtonPartEditSetting(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<T> prop, String settingName)
	{
		super(fontRenderer, posX + widthHalf, posY, (widthHalf * 2) - 10, 12);

		setEnableBackgroundDrawing(false);
		setMaxStringLength(999);

		this.posX = posX;
		this.widthHalf = widthHalf;
		this.fontRenderer = fontRenderer;
		this.prop = prop;
		this.settingName = settingName;
		this.settingCurrent = prop.get();
	}

	public abstract int addToList(List<ButtonPartEditSetting<?>> buttonList);

	@Override
	public void drawTextBox()
	{
		if (getVisible())
		{
			GL11.glPushMatrix();
			GL11.glScalef(0.5F, 0.5F, 1.0F);

			this.posX *= 2;
			xPosition *= 2;
			yPosition *= 2;

			super.drawTextBox();

			String name = this.settingName;
			int widthString = this.fontRenderer.getStringWidth(name);
			int widthMantissa = this.fontRenderer.getStringWidth("..");

			if (widthString > this.widthHalf && widthString > widthMantissa)
			{
				name = this.fontRenderer.trimStringToWidth(name, this.widthHalf - widthMantissa).trim() + "..";
			}

			drawString(this.fontRenderer, this.settingName, this.posX + 1, yPosition, 14737632);

			this.posX /= 2;
			xPosition /= 2;
			yPosition /= 2;

			GL11.glPopMatrix();
		}
	}

	@Override
	public void mouseClicked(int x, int y, int button)
	{
		if (getVisible())
		{
			int heightPrev = height;
			height /= 2;

			super.mouseClicked(x, y, button);

			height = heightPrev;
		}
	}

	/**
	 * Called to parse the unselected text box.
	 */
	protected abstract void parse(boolean checkParent);

	protected abstract void refreshData();

	@Override
	public void setFocused(boolean isFocused)
	{
		boolean wasFocused = isFocused();

		super.setFocused(isFocused);

		if (!isFocused && wasFocused)
		{
			this.parse(true);
			this.setPropertyInPart();
			this.setCursorPositionZero();
		}
		else if (isFocused && !wasFocused)
		{
			this.refreshData();
		}
	}

	/**
	 * Called to write the previously parsed property to the cached IVehiclePart instance.
	 */
	protected abstract void setPropertyInPart();

	@Override
	public void setText(String text)
	{
		int cursorPos = getCursorPosition();
		int selectEnd = getSelectionEnd();

		super.setText(text);

		setCursorPosition(cursorPos);
		setSelectionPos(selectEnd);
	}

	public static final ButtonPartEditSetting<?> getButtonFromObject(FontRenderer fontRenderer, int posX, int posY, int widthHalf, IPartProperty<?> prop, String settingName,
			ButtonPartEditSettingArray<?> parent)
	{
		Object obj = prop.get();
		if (obj == null)
		{
			return null;
		}

		Class<?> type = typeMap.get(obj.getClass());
		if (type != null)
		{
			try
			{
				prop = (PropWrapper<?>)type.cast(type.getDeclaredConstructor(APartProperty.class).newInstance(prop));
				obj = prop.get();
			}
			catch (Exception e)
			{
				;
			}
		}

		Class<?> clazz = classMap.get(obj.getClass());
		if (clazz == null && obj instanceof Enum)
		{
			clazz = classMap.get(Enum.class);
		}

		if (clazz != null)
		{
			try
			{
				if (parent != null)
				{
					return (ButtonPartEditSetting<?>)clazz.cast(clazz	.getDeclaredConstructor(FontRenderer.class, int.class, int.class, int.class, APartProperty.class, String.class,
																								ButtonPartEditSettingArray.class)
																		.newInstance(fontRenderer, posX, posY, widthHalf, prop, settingName, parent));
				}

				return (ButtonPartEditSetting<?>)clazz.cast(clazz	.getDeclaredConstructor(FontRenderer.class, int.class, int.class, int.class, APartProperty.class, String.class)
																	.newInstance(fontRenderer, posX, posY, widthHalf, prop, settingName));
			}
			catch (Exception e)
			{
				;
			}
		}
		return null;
	}



	// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
	// Base classes that can be extended to avoid multiple identical methods in different files. //
	// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //


	@SideOnly(Side.CLIENT)
	public static abstract class ButtonPartEditSettingArray<U> extends ButtonPartEditSetting<U[]>
	{
		final ButtonPartEditSetting<?>[] buttons;



		public ButtonPartEditSettingArray(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<U[]> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);

			this.buttons = new ButtonPartEditSetting<?>[settingCurrent.length];
			for (int i = 0; i < this.buttons.length; ++i)
			{
				this.buttons[i] = ButtonPartEditSetting.getButtonFromObject(fontRenderer, posX, posY, widthHalf, getSubProperty(settingCurrent[i]), settingName, this);
			}
		}

		/**
		 * Create a sub-property of the given property type.
		 */
		protected abstract APartProperty<U> getSubProperty(U value);

		@Override
		public int addToList(List<ButtonPartEditSetting<?>> buttonList)
		{
			for (int i = 0; i < this.buttons.length; ++i)
			{
				buttonList.add(this.buttons[i]);
			}
			return this.buttons.length;
		}

		@Override
		public void drawTextBox()
		{
		}

		@Override
		public void mouseClicked(int x, int y, int button)
		{
			super.mouseClicked(x, y, button);
			refreshData();
		}

		@Override
		protected void parse(boolean checkParent)
		{
			for (int i = 0; i < this.buttons.length; ++i)
			{
				this.buttons[i].parse(false);
			}
		}
	}

	@SideOnly(Side.CLIENT)
	public static abstract class ButtonPartEditSettingSeparate<U> extends ButtonPartEditSetting<U>
	{
		protected ButtonPartEditSettingArray<U> parent;



		public ButtonPartEditSettingSeparate(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<U> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);

			this.parent = null;
		}

		@Override
		public int addToList(List<ButtonPartEditSetting<?>> buttonList)
		{
			buttonList.add(this);

			return 1;
		}

		@Override
		public void mouseClicked(int x, int y, int button)
		{
			super.mouseClicked(x, y, button);

			if (!isFocused())
			{
				refreshData();
			}
		}

		@Override
		protected void parse(boolean checkParent)
		{
			if (this.parent != null)
			{
				this.parent.parse(false);
			}
		}

		public void setPosition(int posX, int posY, int index)
		{
			xPosition = posX + widthHalf;
			yPosition = posY + (index * 6);
		}

		@Override
		protected void setPropertyInPart()
		{
			if (this.parent != null)
			{
				this.parent.setPropertyInPart();
			}
			else
			{
				U obj = prop.get();
				if (obj == null || settingCurrent.getClass().isInstance(obj))
				{
					prop.set(settingCurrent);
				}
				
				if (prop.getParent() instanceof ICachedVertexState)
				{
					((ICachedVertexState)prop.getParent()).onVertexStateReset();
				}
			}
		}
	}


	// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
	// Classes of all primitive types (plus, additionally, String) as well as their array types. //
	// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //


	/**
	 * Class to edit settings of type Boolean.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingBoolean extends ButtonPartEditSettingSeparate<Boolean>
	{
		public ButtonPartEditSettingBoolean(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Boolean> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);

			setText(Boolean.toString(settingCurrent));
			setCursorPosition(0);
		}

		public ButtonPartEditSettingBoolean(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Boolean> prop, String settingName, ButtonPartEditSettingArray<Boolean> parent)
		{
			this(fontRenderer, posX, posY, widthHalf, prop, settingName);

			this.parent = parent;
		}

		@Override
		protected void parse(boolean checkParent)
		{
			if (checkParent && parent != null)
			{
				super.parse(checkParent);
				return;
			}

			if (Boolean.parseBoolean(getText()))
			{
				setText(Boolean.TRUE.toString());
				settingCurrent = true;
			}
			else
			{
				setText(Boolean.FALSE.toString());
				settingCurrent = false;
			}
		}

		@Override
		protected void refreshData()
		{
			if (parent != null)
			{
				parent.refreshData();
			}
			else
			{
				settingCurrent = prop.get();
				setText(settingCurrent.toString());
			}
		}
	}

	/**
	 * Class to edit settings of type Boolean Array.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingBooleanArr extends ButtonPartEditSettingArray<Boolean>
	{
		public ButtonPartEditSettingBooleanArr(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Boolean[]> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);
		}

		@Override
		protected APartProperty<Boolean> getSubProperty(Boolean value)
		{
			return new PropBoolean(prop.getParent(), value, "child")
			{
				@Override
				public boolean getIsConfigurable()
				{
					return prop.getIsConfigurable();
				}
			};
		}

		@Override
		protected void refreshData()
		{
			Boolean[] arr = prop.get();

			if (arr != null)
			{
				for (int i = 0; i < buttons.length; ++i)
				{
					((ButtonPartEditSettingBoolean)buttons[i]).settingCurrent = arr[i];
					buttons[i].setText(buttons[i].settingCurrent.toString());
				}
			}
		}

		@Override
		protected void setPropertyInPart()
		{
			Boolean[] arr = prop.get();
			if (arr != null)
			{
				Boolean[] val = new Boolean[arr.length];
				for (int i = 0; i < arr.length; ++i)
				{
					val[i] = (Boolean)buttons[i].settingCurrent;
				}
				prop.set(val);
				
				if (prop.getParent() instanceof ICachedVertexState)
				{
					((ICachedVertexState)prop.getParent()).onVertexStateReset();
				}
			}
		}
	}

	/**
	 * Class to edit settings of type Byte.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingByte extends ButtonPartEditSettingSeparate<Byte>
	{
		public ButtonPartEditSettingByte(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Byte> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);

			setText(Byte.toString(settingCurrent));
			setCursorPosition(0);
		}

		public ButtonPartEditSettingByte(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Byte> prop, String settingName, ButtonPartEditSettingArray<Byte> parent)
		{
			this(fontRenderer, posX, posY, widthHalf, prop, settingName);

			this.parent = parent;
		}

		@Override
		protected void parse(boolean checkParent)
		{
			if (checkParent && parent != null)
			{
				super.parse(checkParent);
				return;
			}

			byte b;

			try
			{
				b = Byte.parseByte(getText());
			}
			catch (Throwable t)
			{
				b = (byte)0;
			}

			setText(Byte.toString(b));
			settingCurrent = b;
		}

		@Override
		protected void refreshData()
		{
			if (parent != null)
			{
				parent.refreshData();
			}
			else
			{
				settingCurrent = prop.get();
				setText(settingCurrent.toString());
			}
		}
	}

	/**
	 * Class to edit settings of type Byte Array.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingByteArr extends ButtonPartEditSettingArray<Byte>
	{
		public ButtonPartEditSettingByteArr(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Byte[]> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);
		}

		@Override
		protected APartProperty<Byte> getSubProperty(Byte value)
		{
			return new PropByte(prop.getParent(), value, "child")
			{
				@Override
				public boolean getIsConfigurable()
				{
					return prop.getIsConfigurable();
				}
			};
		}

		@Override
		protected void refreshData()
		{
			Byte[] arr = prop.get();
			if (arr != null)
			{
				for (int i = 0; i < buttons.length; ++i)
				{
					((ButtonPartEditSettingByte)buttons[i]).settingCurrent = arr[i];
					buttons[i].setText(buttons[i].settingCurrent.toString());
				}
			}
		}

		@Override
		protected void setPropertyInPart()
		{
			Byte[] arr = prop.get();
			if (arr != null)
			{
				Byte[] val = new Byte[arr.length];
				for (int i = 0; i < arr.length; ++i)
				{
					val[i] = (Byte)buttons[i].settingCurrent;
				}
				prop.set(val);
				
				if (prop.getParent() instanceof ICachedVertexState)
				{
					((ICachedVertexState)prop.getParent()).onVertexStateReset();
				}
			}
		}
	}

	/**
	 * Class to edit settings of type Char.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingChar extends ButtonPartEditSettingSeparate<Character>
	{
		public ButtonPartEditSettingChar(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Character> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);

			setText(Character.toString(settingCurrent));
			setCursorPosition(0);
		}

		public ButtonPartEditSettingChar(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Character> prop, String settingName, ButtonPartEditSettingArray<Character> parent)
		{
			this(fontRenderer, posX, posY, widthHalf, prop, settingName);

			this.parent = parent;
		}

		@Override
		protected void parse(boolean checkParent)
		{
			if (checkParent && parent != null)
			{
				super.parse(checkParent);
				return;
			}

			char c;

			try
			{
				c = getText().charAt(0);
			}
			catch (Throwable t)
			{
				c = '\u0000';
			}

			setText(Character.toString(c));
			settingCurrent = c;
		}

		@Override
		protected void refreshData()
		{
			if (parent != null)
			{
				parent.refreshData();
			}
			else
			{
				settingCurrent = prop.get();
				setText(settingCurrent.toString());
			}
		}
	}

	/**
	 * Class to edit settings of type Char Array.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingCharArr extends ButtonPartEditSettingArray<Character>
	{
		public ButtonPartEditSettingCharArr(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Character[]> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);
		}

		@Override
		protected APartProperty<Character> getSubProperty(Character value)
		{
			return new PropCharacter(prop.getParent(), value, "child")
			{
				@Override
				public boolean getIsConfigurable()
				{
					return prop.getIsConfigurable();
				}
			};
		}

		@Override
		protected void refreshData()
		{
			Character[] arr = prop.get();
			if (arr != null)
			{
				for (int i = 0; i < buttons.length; ++i)
				{
					((ButtonPartEditSettingChar)buttons[i]).settingCurrent = arr[i];
					buttons[i].setText(buttons[i].settingCurrent.toString());
				}
			}
		}

		@Override
		protected void setPropertyInPart()
		{
			Character[] arr = prop.get();
			if (arr != null)
			{
				Character[] val = new Character[arr.length];
				for (int i = 0; i < arr.length; ++i)
				{
					val[i] = (Character)buttons[i].settingCurrent;
				}
				prop.set(val);
				
				if (prop.getParent() instanceof ICachedVertexState)
				{
					((ICachedVertexState)prop.getParent()).onVertexStateReset();
				}
			}
		}
	}

	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = BigDecimal.valueOf(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	public static float round(float value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = BigDecimal.valueOf(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.floatValue();
	}


	/**
	 * Class to edit settings of type Double.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingDouble extends ButtonPartEditSettingSeparate<Double>
	{
		private Double remainder = 0D;

		public ButtonPartEditSettingDouble(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Double> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);
			//Double rounded = round(settingCurrent, 1);
			//remainder = settingCurrent - rounded;
			setText(Double.toString(settingCurrent));
			setCursorPosition(0);
		}

		public ButtonPartEditSettingDouble(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Double> prop, String settingName, ButtonPartEditSettingArray<Double> parent)
		{
			this(fontRenderer, posX, posY, widthHalf, prop, settingName);

			this.parent = parent;
		}

		@Override
		protected void parse(boolean checkParent)
		{
			if (checkParent && parent != null)
			{
				super.parse(checkParent);
				return;
			}

			double d;
			String text = getText();

			try
			{
				d = Double.parseDouble(text);
			}
			catch (Throwable t0)
			{
				if (text != null)
				{
					try
					{
						d = (new Expression(text)).value(0.0D);
					}
					catch (Throwable t1)
					{
						d = 0.0D;
					}
				}
				else
				{
					d = 0.0D;
				}
			}

			if (d < BOUND_TO_ZERO && d > -BOUND_TO_ZERO)
			{
				d = 0.0D;
			}

			setText(Double.toString(d));
			settingCurrent = d;
		}

		@Override
		protected void refreshData()
		{
			if (parent != null)
			{
				parent.refreshData();
			}
			else
			{
				settingCurrent = prop.get();
				setText(settingCurrent.toString());
			}
		}
	}

	/**
	 * Class to edit settings of type Double Array.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingDoubleArr extends ButtonPartEditSettingArray<Double>
	{
		public ButtonPartEditSettingDoubleArr(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Double[]> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);
		}

		@Override
		protected APartProperty<Double> getSubProperty(Double value)
		{
			return new PropDouble(prop.getParent(), value, "child")
			{
				@Override
				public boolean getIsConfigurable()
				{
					return prop.getIsConfigurable();
				}
			};
		}

		@Override
		protected void refreshData()
		{
			Double[] arr = prop.get();
			if (arr != null)
			{
				for (int i = 0; i < buttons.length; ++i)
				{
					((ButtonPartEditSettingDouble)buttons[i]).settingCurrent = arr[i];
					buttons[i].setText(buttons[i].settingCurrent.toString());
				}
			}
		}

		@Override
		protected void setPropertyInPart()
		{
			Double[] arr = prop.get();
			if (arr != null)
			{
				Double[] val = new Double[arr.length];
				for (int i = 0; i < arr.length; ++i)
				{
					val[i] = (Double)buttons[i].settingCurrent;
				}
				prop.set(val);
				
				if (prop.getParent() instanceof ICachedVertexState)
				{
					((ICachedVertexState)prop.getParent()).onVertexStateReset();
				}
			}
		}
	}

	/**
	 * Class to edit settings of type Float.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingFloat extends ButtonPartEditSettingSeparate<Float>
	{

		public ButtonPartEditSettingFloat(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Float> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);
			//float rounded = round(settingCurrent, 1);
			//remainder = settingCurrent - rounded;
			setText(Float.toString(settingCurrent));
			setCursorPosition(0);
		}

		public ButtonPartEditSettingFloat(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Float> prop, String settingName, ButtonPartEditSettingArray<Float> parent)
		{
			this(fontRenderer, posX, posY, widthHalf, prop, settingName);

			this.parent = parent;
		}

		@Override
		protected void parse(boolean checkParent)
		{
			if (checkParent && parent != null)
			{
				super.parse(checkParent);
				return;
			}

			float f;
			String text = getText();

			try
			{
				f = Float.parseFloat(text);
			}
			catch (Throwable t0)
			{
				if (text != null)
				{
					try
					{
						f = (float)(new Expression(text)).value(0.0D);
					}
					catch (Throwable t1)
					{
						f = 0.0F;
					}
				}
				else
				{
					f = 0.0F;
				}
			}

			if (f < BOUND_TO_ZERO && f > -BOUND_TO_ZERO)
			{
				f = 0.0F;
			}

			setText(Float.toString(f));
			settingCurrent = f;
		}

		@Override
		protected void refreshData()
		{
			if (parent != null)
			{
				parent.refreshData();
			}
			else
			{
				settingCurrent = prop.get();
				setText(settingCurrent.toString());
			}
		}
	}

	/**
	 * Class to edit settings of type Float Array.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingFloatArr extends ButtonPartEditSettingArray<Float>
	{
		public ButtonPartEditSettingFloatArr(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Float[]> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);
		}

		@Override
		protected APartProperty<Float> getSubProperty(Float value)
		{
			return new PropFloat(prop.getParent(), value, "child")
			{
				@Override
				public boolean getIsConfigurable()
				{
					return prop.getIsConfigurable();
				}
			};
		}

		@Override
		protected void refreshData()
		{
			Float[] arr = prop.get();
			if (arr != null)
			{
				for (int i = 0; i < buttons.length; ++i)
				{
					if (buttons[i].isFocused())
					{
						continue;
					}

					((ButtonPartEditSettingFloat)buttons[i]).settingCurrent = arr[i];
					buttons[i].setText(buttons[i].settingCurrent.toString());
				}
			}
		}

		@Override
		protected void setPropertyInPart()
		{
			Float[] arr = prop.get();
			if (arr != null)
			{
				Float[] val = new Float[arr.length];
				for (int i = 0; i < arr.length; ++i)
				{
					val[i] = (Float)buttons[i].settingCurrent;
				}
				prop.set(val);
				
				if (prop.getParent() instanceof ICachedVertexState)
				{
					((ICachedVertexState)prop.getParent()).onVertexStateReset();
				}
			}
		}
	}

	/**
	 * Class to edit settings of type Integer.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingInteger extends ButtonPartEditSettingSeparate<Integer>
	{
		public ButtonPartEditSettingInteger(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Integer> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);

			setText(Integer.toString(settingCurrent));
			setCursorPosition(0);
		}

		public ButtonPartEditSettingInteger(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Integer> prop, String settingName, ButtonPartEditSettingArray<Integer> parent)
		{
			this(fontRenderer, posX, posY, widthHalf, prop, settingName);

			this.parent = parent;
		}

		@Override
		protected void parse(boolean checkParent)
		{
			if (checkParent && parent != null)
			{
				super.parse(checkParent);
				return;
			}

			int i;
			String text = getText();

			try
			{
				i = Integer.decode(text);
			}
			catch (Throwable t0)
			{
				if (text != null)
				{
					try
					{
						i = (int)Math.round((new Expression(text)).value(0.0D));
					}
					catch (Throwable t1)
					{
						i = 0;
					}
				}
				else
				{
					i = 0;
				}
			}

			setText(Integer.toString(i));
			settingCurrent = i;
		}

		@Override
		protected void refreshData()
		{
			if (parent != null)
			{
				parent.refreshData();
			}
			else
			{
				settingCurrent = prop.get();
				setText(settingCurrent.toString());
			}
		}
	}

	/**
	 * Class to edit settings of type Integer Array.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingIntegerArr extends ButtonPartEditSettingArray<Integer>
	{
		public ButtonPartEditSettingIntegerArr(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Integer[]> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);
		}

		@Override
		protected APartProperty<Integer> getSubProperty(Integer value)
		{
			return new PropInteger(prop.getParent(), value, "child")
			{
				@Override
				public boolean getIsConfigurable()
				{
					return prop.getIsConfigurable();
				}
			};
		}

		@Override
		protected void refreshData()
		{
			Integer[] arr = prop.get();
			if (arr != null)
			{
				for (int i = 0; i < buttons.length; ++i)
				{
					((ButtonPartEditSettingInteger)buttons[i]).settingCurrent = arr[i];
					buttons[i].setText(buttons[i].settingCurrent.toString());
				}
			}
		}

		@Override
		protected void setPropertyInPart()
		{
			Integer[] arr = prop.get();
			if (arr != null)
			{
				Integer[] val = new Integer[arr.length];
				for (int i = 0; i < arr.length; ++i)
				{
					val[i] = (Integer)buttons[i].settingCurrent;
				}
				prop.set(val);
				
				if (prop.getParent() instanceof ICachedVertexState)
				{
					((ICachedVertexState)prop.getParent()).onVertexStateReset();
				}
			}
		}
	}

	/**
	 * Class to edit settings of type Long.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingLong extends ButtonPartEditSettingSeparate<Long>
	{
		public ButtonPartEditSettingLong(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Long> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);

			setText(Long.toString(settingCurrent));
			setCursorPosition(0);
		}

		public ButtonPartEditSettingLong(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Long> prop, String settingName, ButtonPartEditSettingArray<Long> parent)
		{
			this(fontRenderer, posX, posY, widthHalf, prop, settingName);

			this.parent = parent;
		}

		@Override
		protected void parse(boolean checkParent)
		{
			if (checkParent && parent != null)
			{
				super.parse(checkParent);
				return;
			}

			long l;
			String text = getText();

			try
			{
				l = Long.decode(text);
			}
			catch (Throwable t0)
			{
				if (text != null)
				{
					try
					{
						l = Math.round((new Expression(text)).value(0.0D));
					}
					catch (Throwable t1)
					{
						l = 0L;
					}
				}
				else
				{
					l = 0L;
				}
			}

			setText(Long.toString(l));
			settingCurrent = l;
		}

		@Override
		protected void refreshData()
		{
			if (parent != null)
			{
				parent.refreshData();
			}
			else
			{
				settingCurrent = prop.get();
				setText(settingCurrent.toString());
			}
		}
	}

	/**
	 * Class to edit settings of type Long Array.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingLongArr extends ButtonPartEditSettingArray<Long>
	{
		public ButtonPartEditSettingLongArr(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Long[]> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);
		}

		@Override
		protected APartProperty<Long> getSubProperty(Long value)
		{
			return new PropLong(prop.getParent(), value, "child")
			{
				@Override
				public boolean getIsConfigurable()
				{
					return prop.getIsConfigurable();
				}
			};
		}

		@Override
		protected void refreshData()
		{
			Long[] arr = prop.get();
			if (arr != null)
			{
				for (int i = 0; i < buttons.length; ++i)
				{
					((ButtonPartEditSettingLong)buttons[i]).settingCurrent = arr[i];
					buttons[i].setText(buttons[i].settingCurrent.toString());
				}
			}
		}

		@Override
		protected void setPropertyInPart()
		{
			Long[] arr = prop.get();
			if (arr != null)
			{
				Long[] val = new Long[arr.length];
				for (int i = 0; i < arr.length; ++i)
				{
					val[i] = (Long)buttons[i].settingCurrent;
				}
				prop.set(val);
				
				if (prop.getParent() instanceof ICachedVertexState)
				{
					((ICachedVertexState)prop.getParent()).onVertexStateReset();
				}
			}
		}
	}

	/**
	 * Class to edit settings of type Short.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingShort extends ButtonPartEditSettingSeparate<Short>
	{
		public ButtonPartEditSettingShort(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Short> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);

			setText(Short.toString(settingCurrent));
			setCursorPosition(0);
		}

		public ButtonPartEditSettingShort(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Short> prop, String settingName, ButtonPartEditSettingArray<Short> parent)
		{
			this(fontRenderer, posX, posY, widthHalf, prop, settingName);

			this.parent = parent;
		}

		@Override
		protected void parse(boolean checkParent)
		{
			if (checkParent && parent != null)
			{
				super.parse(checkParent);
				return;
			}

			short s;

			try
			{
				s = Short.parseShort(getText());
			}
			catch (Throwable t)
			{
				s = (short)0;
			}

			setText(Short.toString(s));
			settingCurrent = s;
		}

		@Override
		protected void refreshData()
		{
			if (parent != null)
			{
				parent.refreshData();
			}
			else
			{
				settingCurrent = prop.get();
				setText(settingCurrent.toString());
			}
		}
	}

	/**
	 * Class to edit settings of type Short Array.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingShortArr extends ButtonPartEditSettingArray<Short>
	{
		public ButtonPartEditSettingShortArr(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Short[]> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);
		}

		@Override
		protected APartProperty<Short> getSubProperty(Short value)
		{
			return new PropShort(prop.getParent(), value, "child")
			{
				@Override
				public boolean getIsConfigurable()
				{
					return prop.getIsConfigurable();
				}
			};
		}

		@Override
		protected void refreshData()
		{
			Short[] arr = prop.get();
			if (arr != null)
			{
				for (int i = 0; i < buttons.length; ++i)
				{
					((ButtonPartEditSettingShort)buttons[i]).settingCurrent = arr[i];
					buttons[i].setText(buttons[i].settingCurrent.toString());
				}
			}
		}

		@Override
		protected void setPropertyInPart()
		{
			Short[] arr = prop.get();
			if (arr != null)
			{
				Short[] val = new Short[arr.length];
				for (int i = 0; i < arr.length; ++i)
				{
					val[i] = (Short)buttons[i].settingCurrent;
				}
				prop.set(val);
				
				if (prop.getParent() instanceof ICachedVertexState)
				{
					((ICachedVertexState)prop.getParent()).onVertexStateReset();
				}
			}
		}
	}

	/**
	 * Class to edit settings of type String.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingString extends ButtonPartEditSettingSeparate<String>
	{
		public ButtonPartEditSettingString(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<String> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);

			setText(settingCurrent);
			setCursorPosition(0);
		}

		public ButtonPartEditSettingString(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<String> prop, String settingName, ButtonPartEditSettingArray<String> parent)
		{
			this(fontRenderer, posX, posY, widthHalf, prop, settingName);

			this.parent = parent;
		}

		@Override
		protected void parse(boolean checkParent)
		{
			if (checkParent && parent != null)
			{
				super.parse(checkParent);
				return;
			}

			settingCurrent = getText();
		}

		@Override
		protected void refreshData()
		{
			if (parent != null)
			{
				parent.refreshData();
			}
			else
			{
				settingCurrent = prop.get();
				setText(settingCurrent.toString());
			}
		}
	}

	/**
	 * Class to edit settings of type String Array.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingStringArr extends ButtonPartEditSettingArray<String>
	{
		public ButtonPartEditSettingStringArr(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<String[]> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);
		}

		@Override
		protected APartProperty<String> getSubProperty(String value)
		{
			return new PropString(prop.getParent(), value, "child")
			{
				@Override
				public boolean getIsConfigurable()
				{
					return prop.getIsConfigurable();
				}
			};
		}

		@Override
		protected void refreshData()
		{
			String[] arr = prop.get();
			if (arr != null)
			{
				for (int i = 0; i < buttons.length; ++i)
				{
					((ButtonPartEditSettingString)buttons[i]).settingCurrent = arr[i];
					buttons[i].setText(buttons[i].settingCurrent.toString());
				}
			}
		}

		@Override
		protected void setPropertyInPart()
		{
			String[] arr = prop.get();
			if (arr != null)
			{
				String[] val = new String[arr.length];
				for (int i = 0; i < arr.length; ++i)
				{
					val[i] = (String)buttons[i].settingCurrent;
				}
				prop.set(val);
				
				if (prop.getParent() instanceof ICachedVertexState)
				{
					((ICachedVertexState)prop.getParent()).onVertexStateReset();
				}
			}
		}
	}

	/**
	 * Class to edit settings of type ColorRGB.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingColorRGB extends ButtonPartEditSettingSeparate<ColorRGBA>
	{
		private ColorRGBA color;



		public ButtonPartEditSettingColorRGB(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<ColorRGBA> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);

			color = settingCurrent;
			setText(color.toString());
			setCursorPosition(0);
		}

		@Override
		protected void parse(boolean checkParent)
		{
			settingCurrent = new ColorRGBA(getText());
			color.set(settingCurrent.toHEX());
		}

		@Override
		protected void refreshData()
		{
			ColorRGBA col = prop.get();
			if (col != null)
			{
				setText(col.toString());
				color = col;
			}
		}
	}

	/**
	 * Class to edit settings of type Enum.
	 */
	@SideOnly(Side.CLIENT)
	public static class ButtonPartEditSettingEnum extends ButtonPartEditSettingSeparate<Enum<?>>
	{
		public ButtonPartEditSettingEnum(FontRenderer fontRenderer, int posX, int posY, int widthHalf, APartProperty<Enum<?>> prop, String settingName)
		{
			super(fontRenderer, posX, posY, widthHalf, prop, settingName);

			setText(String.valueOf(settingCurrent.ordinal()));
			setCursorPosition(0);
		}

		@Override
		protected void parse(boolean checkParent)
		{
			String text = getText();
			int i;

			try
			{
				i = Integer.decode(text);
			}
			catch (Throwable t0)
			{
				if (text != null)
				{
					try
					{
						i = (int)Math.round((new Expression(text)).value(0.0D));
					}
					catch (Throwable t1)
					{
						i = 0;
					}
				}
				else
				{
					i = 0;
				}
			}

			Enum<?>[] enumValues = settingCurrent.getClass().getEnumConstants();
			if (i < 0)
			{
				i = 0;
			}
			else if (i >= enumValues.length)
			{
				i = enumValues.length - 1;
			}

			settingCurrent = enumValues[i];
			setText(String.valueOf(settingCurrent.ordinal()));
		}

		@Override
		protected void refreshData()
		{
			settingCurrent = prop.get();
			setText(String.valueOf(settingCurrent.ordinal()));
		}
	}
}