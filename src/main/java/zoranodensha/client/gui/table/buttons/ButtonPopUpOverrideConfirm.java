package zoranodensha.client.gui.table.buttons;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.resources.I18n;
import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.registry.ModPartRegistry;



@SideOnly(Side.CLIENT)
public class ButtonPopUpOverrideConfirm extends ButtonPopUpYesNo
{
	private final GUIEngineerTable guiRef;
	private final NBTTagCompound nbt;



	public ButtonPopUpOverrideConfirm(int id, int xPos, int yPos, GUIEngineerTable gui, NBTTagCompound nbt)
	{
		super(id, xPos, yPos, I18n.format(ModData.ID + ".text.guiEngineer.overrideConfirm"));

		guiRef = gui;
		this.nbt = nbt;
	}

	@Override
	protected void actionPerformedYes(GUIEngineerTable gui, GuiButton button)
	{
		ModPartRegistry.presetExport(nbt, guiRef, true);
		guiRef.buttonPopUp = null;
		guiRef.initGui();
	}
}
