package zoranodensha.client.gui.table.buttons.partEdit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.MathHelper;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.client.gui.table.buttons.ButtonScrollbar;
import zoranodensha.client.gui.table.buttons.partEdit.ButtonPartEditSetting.ButtonPartEditSettingSeparate;
import zoranodensha.client.gui.table.model.TrainModelRender;



/**
 * This ButtonPartEdit class is responsible for rendering of and interaction with settings, colors, etc.
 */
@SideOnly(Side.CLIENT)
public class ButtonPartEditR extends ButtonPartEdit
{
	private static final int MAX_INDICES = 9;

	private boolean refreshPropertiesList;
	private VehParBase lastSelected;
	public ButtonPartEditName buttonName;

	/** An ArrayList containing all ButtonPartEditSetting instances. */
	private ArrayList<ButtonPartEditSetting<?>> buttonSettingList = new ArrayList<ButtonPartEditSetting<?>>();
	/** Same as above, however without ButtonPartEditSettingArray instances. */
	private ArrayList<ButtonPartEditSettingSeparate<?>> buttonSettingList2 = new ArrayList<ButtonPartEditSettingSeparate<?>>();



	public ButtonPartEditR(TrainModelRender trainRender)
	{
		super(trainRender);
	}

	@Override
	public void actionPerformed(GUIEngineerTable gui, GuiButton button)
	{
		if (button.id == buttonName.id && buttonName.lastButton != 0)
		{
			buttonName.index += buttonName.lastButton;
			buttonName.lastButton = 0;
			buttonName.refreshSelected();
		}
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addButtons(int xPos, int yPos, List list, int index)
	{
		list.add(scrollbar = new ButtonScrollbar(index, xPos + 157, yPos + 19, 56, scroll, trainRender));
		list.add(buttonName = new ButtonPartEditName(index, xPos + 55, yPos, trainRender));
		lastSelected = null;
	}

	@Override
	public boolean isTextboxSelected()
	{
		for (ButtonPartEditSettingSeparate<?> button : buttonSettingList2)
		{
			if (button.isFocused())
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public void keyTyped(char c, int key)
	{
		if (buttonSettingList.isEmpty())
		{
			return;
		}

		if (Keyboard.KEY_TAB == key)
		{
			boolean focusNext = false;
			for (ButtonPartEditSetting<?> button : buttonSettingList)
			{
				if (!button.getVisible())
				{
					continue;
				}

				if (focusNext)
				{
					focusNext = false;
					trainRender.mouseClicked(button.xPosition, button.yPosition, 1);
				}
				else if (button.isFocused())
				{
					trainRender.pushUndo();
					focusNext = true;
					button.parse(true);
				}
			}

			if (focusNext)
			{
				ButtonPartEditSetting<?> button = buttonSettingList.get(0);
				trainRender.mouseClicked(button.xPosition, button.yPosition, 1);
			}
		}
		else if (Keyboard.KEY_RETURN == key)
		{
			for (ButtonPartEditSetting<?> button : buttonSettingList)
			{
				if (button.isFocused())
				{
					trainRender.pushUndo();
					button.setFocused(false);
					button.parse(true);
					refreshPropertiesList = true;
					button.setFocused(true);
					trainRender.mouseClicked(button.xPosition, button.yPosition, 1);
				}
			}
		}
		else
		{
			for (ButtonPartEditSetting<?> button : buttonSettingList)
			{
				if (button.isFocused())
				{
					button.textboxKeyTyped(c, key);
				}
			}
		}
	}

	@Override
	public void mouseClicked(int x, int y, int clickedButton)
	{
		for (ButtonPartEditSetting<?> button : buttonSettingList)
		{
			button.mouseClicked(x, y, clickedButton);
		}
	}

	@Override
	public void mouseClickMove(int x, int y)
	{
		if (scrollbar != null && scrollbar.visible)
		{
			super.mouseClickMove(x, y);
		}
	}

	@Override
	public void render(GUIEngineerTable gui, int xPos, int yPos, float scaleX, float scaleY, int mouseX, int mouseY)
	{
		if (buttonName.currentlySelected != null && (lastSelected != buttonName.currentlySelected || refreshPropertiesList))
		{
			VehParBase part = lastSelected = buttonName.currentlySelected;
			Collection<IPartProperty<?>> properties = part.getProperties();

			if (properties != null && !properties.isEmpty())
			{
				String s;
				int offset = 0;

				buttonSettingList.clear();
				for (IPartProperty<?> prop : properties)
				{
					if (!prop.getIsConfigurable())
					{
						continue;
					}
					s = I18n.format(prop.getUnlocalisedName());

					ButtonPartEditSetting<?> button = ButtonPartEditSetting.getButtonFromObject(fontRenderer, xPos + 57, yPos + 21 + (offset * 6), 52, prop, s, null);
					if (button != null)
					{
						offset += button.addToList(buttonSettingList);
					}
				}
			}

			buttonSettingList2.clear();
			for (ButtonPartEditSetting<?> button : buttonSettingList)
			{
				if (button instanceof ButtonPartEditSettingSeparate)
				{
					buttonSettingList2.add((ButtonPartEditSettingSeparate<?>)button);
				}
			}

			scrollbar.setVisible(buttonSettingList2.size() > MAX_INDICES);
			refreshPropertiesList = false;
		}

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		gui.mc.renderEngine.bindTexture(GUIEngineerTable.texture);

		int xPos0 = xPos + 54;

		/* Top Panel */
		gui.drawTexturedModalRect(xPos0, yPos, 0, 240, 108, 9);
		gui.drawTexturedModalRect(xPos0, yPos + 9, 0, 247, 108, 9);

		/* Bottom Panel */
		gui.drawTexturedModalRect(xPos0, yPos + 18, 0, 240, 108, 15);
		gui.drawTexturedModalRect(xPos0, yPos + 33, 0, 241, 108, 14);
		gui.drawTexturedModalRect(xPos0, yPos + 47, 0, 241, 108, 14);
		gui.drawTexturedModalRect(xPos0, yPos + 61, 0, 241, 108, 15);

		/* Index description and scroll bar */
		Minecraft mc = Minecraft.getMinecraft();
		buttonName.drawButton(mc, mouseX, mouseY);
		scrollbar.drawButton(mc, mouseX, mouseY);

		/* Index settings */
		int size = buttonSettingList2.size();
		int index = (size >= MAX_INDICES ? MathHelper.ceiling_float_int(scroll * (size - MAX_INDICES)) : 0);
		int count = 0;
		int i = 0;

		Iterator<ButtonPartEditSettingSeparate<?>> itera = buttonSettingList2.iterator();
		ButtonPartEditSettingSeparate<?> button;

		while (itera.hasNext())
		{
			button = itera.next();

			if (i == index && count < MAX_INDICES)
			{
				button.setPosition(xPos + 57, yPos + 21, count);
				button.setVisible(true);
				button.drawTextBox();
				++count;
				++index;
			}
			else
			{
				button.setVisible(false);
				button.setFocused(false);
			}

			++i;
		}
	}
}
