package zoranodensha.client.gui.table.buttons.partEdit;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.client.gui.table.buttons.ButtonScrollbar;
import zoranodensha.client.gui.table.model.TrainModelRender;



/**
 * The menu that appears in the Engineer's Table's GUI when selecting one or more IVehicleParts in the model editor.
 */
@SideOnly(Side.CLIENT)
public abstract class ButtonPartEdit
{
	protected final FontRenderer fontRenderer;
	protected final TrainModelRender trainRender;
	protected ButtonScrollbar scrollbar;
	protected float scroll;



	public ButtonPartEdit(TrainModelRender trainRender)
	{
		this.trainRender = trainRender;
		fontRenderer = trainRender.mc.fontRenderer;
	}

	public abstract void actionPerformed(GUIEngineerTable gui, GuiButton button);

	@SuppressWarnings({ "rawtypes" })
	public abstract void addButtons(int xPos, int yPos, List list, int index);

	/**
	 * Return true if any Textbox is selected.
	 */
	public abstract boolean isTextboxSelected();

	public abstract void keyTyped(char c, int i);

	public abstract void mouseClicked(int x, int y, int button);

	public void mouseClickMove(int x, int y)
	{
		if (scrollbar != null)
		{
			scroll = scrollbar.scroll;
		}
	}

	public abstract void render(GUIEngineerTable gui, int xPos, int yPos, float scaleX, float scaleY, int mouseX, int mouseY);

	public void scroll(int deltaY)
	{
		if (scrollbar != null)
		{
			scrollbar.scroll(deltaY);
			scroll = scrollbar.scroll;
		}
	}
}
