package zoranodensha.client.gui.table.buttons;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.client.gui.table.model.TrainModelRender;



@SideOnly(Side.CLIENT)
public class ButtonScrollbar extends GuiButton
{
	private static final int cursorHeight = 16;

	private final TrainModelRender trainRender;

	/** Clamped within 0.0F (top) and 1.0F (bottom). */
	public float scroll;
	/** When true, follows mouse Y-movement and reacts accordingly. */
	private boolean isGrabbed;
	private int clickY = -1;



	public ButtonScrollbar(int id, int posX, int posY, int height, float scroll, TrainModelRender trainRender)
	{
		super(id, posX, posY, 4, height, "");

		this.scroll = scroll;
		this.trainRender = trainRender;
	}

	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY)
	{
		if (visible && trainRender.renderSelectionData())
		{
			mc.getTextureManager().bindTexture(GUIEngineerTable.texture);
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

			drawTexturedModalRect(xPosition, yPosition + getCursorPos(), 228, 13, width, cursorHeight);
			mouseDragged(mc, mouseX, mouseY);
		}
	}

	/**
	 * Returns the Y-position of the cursor.
	 */
	private int getCursorPos()
	{
		return ((int)(scroll * (height - cursorHeight)));
	}

	@Override
	protected void mouseDragged(Minecraft mc, int x, int y)
	{
		int diffY = y - clickY;

		if (visible && isGrabbed)
		{
			scroll += (float)diffY / (float)height;

			if (scroll > 1.0F)
			{
				scroll = 1.0F;
			}
			else if (scroll < 0.0F)
			{
				scroll = 0.0F;
			}
		}

		clickY = y;
	}

	@Override
	public boolean mousePressed(Minecraft mc, int x, int y)
	{
		clickY = y;

		int yPos = yPosition + getCursorPos();

		return isGrabbed = (enabled && visible && x >= xPosition && x < xPosition + width && y >= yPos && y < yPos + cursorHeight);
	}

	@Override
	public void mouseReleased(int x, int y)
	{
		isGrabbed = false;
	}

	/**
	 * Called to check whether the given mouse position is on top of this element, and if so, to scroll on the element.
	 */
	public void scroll(int deltaY)
	{
		if (visible)
		{
			scroll += deltaY * 0.05F / height;

			if (scroll > 1.0F)
			{
				scroll = 1.0F;
			}
			else if (scroll < 0.0F)
			{
				scroll = 0.0F;
			}
		}
	}

	public void setVisible(boolean isVisible)
	{
		visible = isVisible;

		if (!visible)
		{
			scroll = 0.0F;
		}
	}
}
