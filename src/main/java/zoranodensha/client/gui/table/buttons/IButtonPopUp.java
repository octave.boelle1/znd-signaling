package zoranodensha.client.gui.table.buttons;

import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import zoranodensha.client.gui.table.GUIEngineerTable;



/**
 * All pop-up buttons used in GUIEngineerTable or its components implement this interface.
 * There may only be one IButtonPopUp instance per time.
 */
@SideOnly(Side.CLIENT)
public interface IButtonPopUp
{
	/**
	 * Relayed method call from GuiScreen's {@code actionPerformed}.
	 */
	void actionPerformed(GUIEngineerTable gui, GuiButton button);

	/**
	 * Called to add any buttons to the given List.
	 */
	@SuppressWarnings("rawtypes")
	void addButtons(List buttonList, int index);

	/**
	 * Check if the mouse is within bounds and draw.
	 */
	void draw(int mouseX, int mouseY, GUIEngineerTable gui);

	/**
	 * Relayed method from GuiScreen's {@code keyTyped}.
	 */
	void keyTyped(char c, int i);

	/**
	 * Relayed method from GuiScreen's {@code mouseClicked}.
	 */
	void mouseClicked(int x, int y, int button);
}
