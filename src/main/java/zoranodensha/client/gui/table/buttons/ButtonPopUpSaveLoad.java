package zoranodensha.client.gui.table.buttons;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.common.core.ModData;



public class ButtonPopUpSaveLoad extends ButtonPopUpYesNo
{
	private final ItemStack itemStack;



	public ButtonPopUpSaveLoad(int id, int xPos, int yPos, ItemStack itemStack)
	{
		super(id, xPos, yPos, I18n.format(ModData.ID + ".text.guiEngineer.saveLoad"));

		this.itemStack = itemStack;
	}

	@Override
	protected void actionPerformedNo(GUIEngineerTable gui, GuiButton button)
	{
		gui.clearModel();
		gui.getRender().loadModelFromNBT(itemStack.getTagCompound());
		gui.buttonPopUp = null;
		gui.initGui();
	}

	@Override
	protected void actionPerformedYes(GUIEngineerTable gui, GuiButton button)
	{
		gui.buttonPopUp = new ButtonPopUpSaveName((gui.width / 2) - 45, (gui.height / 2) - 16);
		gui.initGui();
	}
}
