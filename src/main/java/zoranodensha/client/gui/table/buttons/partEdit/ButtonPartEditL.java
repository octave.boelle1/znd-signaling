package zoranodensha.client.gui.table.buttons.partEdit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.client.gui.table.buttons.partEdit.ButtonPartEditSetting.ButtonPartEditSettingFloat;
import zoranodensha.client.gui.table.model.Pivot;
import zoranodensha.client.gui.table.model.TrainModelRender;



/**
 * This ButtonPartEdit class is responsible for rendering of and interaction with position, rotation, scale, etc.
 */
@SideOnly(Side.CLIENT)
public class ButtonPartEditL extends ButtonPartEdit
{
	private ButtonPartEditSettingFloat[] buttons = new ButtonPartEditSettingFloat[0];
	private ArrayList<VehParBase> lastSelection = new ArrayList<VehParBase>();
	private final VehiclePartDummy dummyPart = new VehiclePartDummy();



	public ButtonPartEditL(TrainModelRender trainRender)
	{
		super(trainRender);
	}

	@Override
	public void actionPerformed(GUIEngineerTable gui, GuiButton button)
	{
	}

	@Override
	@SuppressWarnings({ "rawtypes" })
	public void addButtons(int xPos, int yPos, List list, int index)
	{
		for (int i = 0, k = 0, l = 0; i < buttons.length; ++i, k = i / 3, l = i % 3)
		{
			buttons[i] = new ButtonPartEditSettingFloat(fontRenderer, xPos + 1, yPos + 3 + (i * 6) + (k * 6), 28, new PropFloat(dummyPart, "dummy"), (k == 0 ? "Offset " : k == 1 ? "Rotation " : "Pivot ") + (l == 0 ? "X" : l == 1 ? "Y" : "Z"));
		}
	}

	@Override
	public boolean isTextboxSelected()
	{
		for (int i = 0; i < buttons.length; ++i)
		{
			if (buttons[i].isFocused())
			{
				return true;
			}
		}

		return false;
	}

	@Override
	public void keyTyped(char c, int key)
	{
		float lastVal;

		if (Keyboard.KEY_TAB == key)
		{
			boolean focusNext = false;

			for (int i = 0; i < buttons.length; ++i)
			{
				if (focusNext)
				{
					focusNext = false;
					trainRender.mouseClicked(buttons[i].xPosition, buttons[i].yPosition, 1);
				}
				else if (buttons[i].isFocused())
				{
					lastVal = buttons[i].settingCurrent;
					focusNext = true;
					buttons[i].parse(true);
					refreshButton(lastVal, i);
				}
			}

			if (focusNext && buttons.length > 0)
			{
				trainRender.mouseClicked(buttons[0].xPosition, buttons[0].yPosition, 1);
			}
		}
		else if (Keyboard.KEY_RETURN == key)
		{
			for (int i = 0; i < buttons.length; ++i)
			{
				if (buttons[i].isFocused())
				{
					lastVal = buttons[i].settingCurrent;
					buttons[i].parse(true);
					refreshButton(lastVal, i);
					trainRender.mouseClicked(buttons[i].xPosition, buttons[i].yPosition, 1);
				}
			}
		}
		else
		{
			for (int i = 0; i < buttons.length; ++i)
			{
				if (buttons[i].isFocused())
				{
					buttons[i].textboxKeyTyped(c, key);
				}
			}
		}
	}

	@Override
	public void mouseClicked(int x, int y, int button)
	{
		float lastValue;

		for (int i = 0; i < buttons.length; ++i)
		{
			lastValue = buttons[i].settingCurrent;
			buttons[i].mouseClicked(x, y, button);
			refreshButton(lastValue, i);
		}
	}

	private void refreshButton(float lastValue, int index)
	{
		if (lastValue == buttons[index].settingCurrent)
		{
			return;
		}

		float diff = buttons[index].settingCurrent - lastValue;

		if (index >= 6)
		{
			if (trainRender.pivot != null)
			{
				switch (index)
				{
					case 6:
						trainRender.pivot.x += diff;
						break;

					case 7:
						trainRender.pivot.y += diff;
						break;

					case 8:
						trainRender.pivot.z += diff;
						break;
				}
			}
		}
		else
		{
			final boolean doChangeOffset = (index < 3);
			final int i = index % 3;
			float[] f;

			for (VehParBase part : lastSelection)
			{
				f = doChangeOffset ? part.getOffset() : part.getRotation();
				f[i] += diff;
			}
		}
	}

	public void refreshData(boolean refreshPosition, int xPos, int yPos)
	{
		float[] f0 = new float[trainRender.pivot != null ? 9 : 6];
		float[] f1;
		int indicesOff = 0;
		int indicesRot = 0;
		lastSelection.clear();
		lastSelection.addAll(trainRender.model.selection);

		if (refreshPosition)
		{
			buttons = new ButtonPartEditSettingFloat[f0.length];
		}

		Iterator<VehParBase> itera = lastSelection.iterator();
		VehParBase part;

		while (itera.hasNext())
		{
			part = itera.next();

			if ((f1 = part.getOffset()) != null)
			{
				f0[0] += f1[0];
				f0[1] += f1[1];
				f0[2] += f1[2];
				++indicesOff;
			}

			if ((f1 = part.getRotation()) != null)
			{
				f0[3] += f1[0];
				f0[4] += f1[1];
				f0[5] += f1[2];
				++indicesRot;
			}
		}

		if (indicesOff > 1)
		{
			float f = 1.0F / indicesOff;
			f0[0] *= f;
			f0[1] *= f;
			f0[2] *= f;
		}

		if (indicesRot > 1)
		{
			float f = 1.0F / indicesRot;
			f0[3] *= f;
			f0[4] *= f;
			f0[5] *= f;
		}

		if (f0.length == 9)
		{
			Pivot pivot = trainRender.pivot;
			f0[6] = pivot.x;
			f0[7] = pivot.y;
			f0[8] = pivot.z;
		}

		if (refreshPosition)
		{
			for (int i = 0, k = 0, l = 0; i < buttons.length; ++i, k = i / 3, l = i % 3)
			{
				buttons[i] =
						new ButtonPartEditSettingFloat(fontRenderer, xPos + 1, yPos + 3 + (i * 6) + (k * 6), 28, new PropFloat(dummyPart, f0[i], "dummy"), (k == 0 ? "Offset " : k == 1 ? "Rotation " : "Pivot ") + (l == 0 ? "X" : l == 1 ? "Y" : "Z"));
			}
		}
		else
		{
			for (int i = Math.min(buttons.length, f0.length) - 1; i >= 0; --i)
			{
				if (!buttons[i].isFocused())
				{
					buttons[i].settingCurrent = f0[i];
					buttons[i].setPropertyInPart();
					buttons[i].refreshData();
				}
			}
		}
	}

	@Override
	public void render(GUIEngineerTable gui, int xPos, int yPos, float scaleX, float scaleY, int mouseX, int mouseY)
	{
		refreshData((lastSelection.hashCode() != trainRender.model.selection.hashCode()), xPos, yPos);

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		gui.mc.renderEngine.bindTexture(GUIEngineerTable.texture);

		int xPos0 = xPos + 27;
		int yPos0 = yPos + 66;

		/* Left Panel */
		gui.drawTexturedModalRect(xPos, yPos, 0, 240, 27, 10);
		gui.drawTexturedModalRect(xPos0, yPos, 81, 240, 27, 10);
		gui.drawTexturedModalRect(xPos, yPos0, 0, 246, 27, 10);
		gui.drawTexturedModalRect(xPos0, yPos0, 81, 246, 27, 10);

		for (int i = 0; i < 4; ++i)
		{
			yPos0 = yPos + 10 + (i * 14);
			gui.drawTexturedModalRect(xPos, yPos0, 0, 241, 27, 14);
			gui.drawTexturedModalRect(xPos0, yPos0, 81, 241, 27, 14);
		}

		for (int i = 0; i < buttons.length; ++i)
		{
			buttons[i].drawTextBox();
		}
	}



	@SideOnly(Side.CLIENT)
	private class VehiclePartDummy extends VehParBase
	{
		public VehiclePartDummy()
		{
			super("dummy", 0, 0, 0);
		}

		@Override
		public AxisAlignedBB getBoundingBox()
		{
			return initBoundingBox();
		}

		@Override
		public boolean getIsFinished()
		{
			return false;
		}

		@Override
		public String getName()
		{
			return "";
		}

		@Override
		public float[] getOffset()
		{
			return null;
		}

		@Override
		public IVehiclePartRenderer getRenderer()
		{
			return null;
		}

		@Override
		public float[] getRotation()
		{
			return null;
		}

		@Override
		public float[] getScale()
		{
			return null;
		}

		@Override
		public Train getTrain()
		{
			return null;
		}

		@Override
		public String getUnlocalizedName()
		{
			return "";
		}

		@Override
		public float getMass()
		{
			return 0.0F;
		}

		@Override
		public AxisAlignedBB initBoundingBox()
		{
			return AxisAlignedBB.getBoundingBox(0, 0, 0, 0, 0, 0);
		}

		@Override
		public void readFromNBT(NBTTagCompound nbt, ETagCall callType)
		{
		}

		@Override
		public void registerItemRecipe(ItemStack itemStack)
		{
		}

		@Override
		public void setIsFinished(boolean isFinished)
		{
		}

		@Override
		public void setOffset(float... values)
		{
		}

		@Override
		public void setPosition(double x, double y, double z)
		{
		}

		@Override
		public void setProperty(int key, Object property)
		{
		}

		@Override
		public void setRotation(float... values)
		{
		}

		@Override
		public void setScale(float... values)
		{
		}

		@Override
		public void writeToNBT(NBTTagCompound nbt, ETagCall callType)
		{
		}
	}
}
