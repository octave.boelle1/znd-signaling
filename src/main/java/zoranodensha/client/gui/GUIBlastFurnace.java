package zoranodensha.client.gui;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import zoranodensha.common.blocks.tileEntity.TileEntityBlastFurnace;
import zoranodensha.common.containers.ContainerBlastFurnace;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class GUIBlastFurnace extends GuiContainer
{
	private static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/guis/blastFurnaceGui.png");
	private TileEntityBlastFurnace tileEntity;



	public GUIBlastFurnace(InventoryPlayer inventory, TileEntityBlastFurnace tileEntity)
	{
		super(new ContainerBlastFurnace(inventory, tileEntity));

		this.tileEntity = tileEntity;
		xSize = 176;
		ySize = 166;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int mouseX, int mouseY)
	{
		int xPos = (width - xSize) / 2;
		int yPos = (height - ySize) / 2;
		int i2 = tileEntity.getBurnTimeScaled();

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(texture);
		drawTexturedModalRect(xPos, yPos, 0, 0, xSize, ySize);

		if (tileEntity.data[0] > 0)
		{
			drawTexturedModalRect(xPos + 56, yPos + 48 - i2, 176, 12 - i2, 14, i2 + 1);
			drawTexturedModalRect(xPos + 79, yPos + 34, 176, 14, tileEntity.getProcessScaled() + 1, 16);
		}
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int x, int y)
	{
		String s = I18n.format(tileEntity.hasCustomInventoryName() ? tileEntity.getInventoryName() : I18n.format(tileEntity.getInventoryName()));
		fontRendererObj.drawString(s, (xSize / 2) - (fontRendererObj.getStringWidth(s) / 2), 6, 4210752);
		fontRendererObj.drawString(I18n.format("container.inventory"), 8, ySize - 94, 4210752);
	}
}
