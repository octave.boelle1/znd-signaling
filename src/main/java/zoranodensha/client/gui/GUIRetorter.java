package zoranodensha.client.gui;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidTank;
import zoranodensha.common.blocks.tileEntity.TileEntityRetorter;
import zoranodensha.common.containers.ContainerRetorter;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public class GUIRetorter extends GuiContainer
{
	private static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/guis/retorterGui.png");
	private TileEntityRetorter tileEntity;



	public GUIRetorter(InventoryPlayer inventory, TileEntityRetorter tileEntity)
	{
		super(new ContainerRetorter(inventory, tileEntity));

		this.tileEntity = tileEntity;
		xSize = 176;
		ySize = 166;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int mouseX, int mouseY)
	{
		int xPos = (width - xSize) / 2;
		int yPos = (height - ySize) / 2;
		int i0 = tileEntity.getBurnTimeScaled();

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(texture);
		drawTexturedModalRect(xPos, yPos, 0, 0, xSize, ySize);

		if (tileEntity.data[0] > 0)
		{
			drawTexturedModalRect(xPos + 42, yPos + 48 - i0, 176, 55 - i0, 14, i0 + 1);
			drawTexturedModalRect(xPos + 65, yPos + 34, 176, 57, (tileEntity.data[2] * 24 / 200) + 1, 16);
		}

		FluidTank fluidTank = tileEntity.getTank();
		if (fluidTank != null && fluidTank.getFluid() != null)
		{
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			mc.getTextureManager().bindTexture(TextureMap.locationBlocksTexture);
			i0 = fluidTank.getFluidAmount() / 340;
			IIcon icon = fluidTank.getFluid().getFluid().getIcon();
			Tessellator tessellator = Tessellator.instance;

			for (int i1 = 0; i1 < i0; i1 += 16)
			{
				int i2 = MathHelper.clamp_int((i0 - i1), 0, 16);

				tessellator.startDrawingQuads();
				tessellator.addVertexWithUV(xPos + 98, yPos + 66 - i1, 0, icon.getMinU(), icon.getInterpolatedV(0));
				tessellator.addVertexWithUV(xPos + 114, yPos + 66 - i1, 0, icon.getInterpolatedU(16), icon.getInterpolatedV(0));
				tessellator.addVertexWithUV(xPos + 114, yPos + 66 - i1 - i2, 0, icon.getInterpolatedU(16), icon.getInterpolatedV(i2));
				tessellator.addVertexWithUV(xPos + 98, yPos + 66 - i1 - i2, 0, icon.getMinU(), icon.getInterpolatedV(i2));
				tessellator.draw();
			}
		}

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(texture);
		drawTexturedModalRect(xPos + 98, yPos + 21, 176, 0, 6, 43);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		String s = I18n.format(tileEntity.hasCustomInventoryName() ? tileEntity.getInventoryName() : I18n.format(tileEntity.getInventoryName()));
		fontRendererObj.drawString(s, (xSize / 2) - (fontRendererObj.getStringWidth(s) / 2), 6, 4210752);
		fontRendererObj.drawString(I18n.format("container.inventory"), 8, ySize - 94, 4210752);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float f)
	{
		super.drawScreen(mouseX, mouseY, f);

		int xPos = (width - xSize) / 2;
		int yPos = (height - ySize) / 2;

		if (tileEntity.getTank().getFluid() != null && mouseX > xPos + 96 && mouseX < xPos + 115 && mouseY > yPos + 17 && mouseY < yPos + 67)
		{
			List<String> listInfo = new ArrayList<String>();
			listInfo.add(tileEntity.getTank().getFluid().getFluid().getLocalizedName(tileEntity.getTank().getFluid()));
			listInfo.add("§7" + tileEntity.getTank().getFluidAmount() + " / " + tileEntity.getTank().getCapacity() + " mB");

			drawHoveringText(listInfo, mouseX, mouseY, fontRendererObj);
		}
	}
}
