package zoranodensha.client;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.client.resources.IResourceManagerReloadListener;

/**
 * Listener for resource manager reload.<br>
 * Keeps track of the most recent reload time stamp, so tile entities can reset their cached vertex state data.
 * 
 * @author Leshuwa Kaiheiwa
 */
@SideOnly(Side.CLIENT)
public final class ResourceManagerReloadListener implements IResourceManagerReloadListener
{
	/** Static instance of this listener. */
	public static final ResourceManagerReloadListener INSTANCE = new ResourceManagerReloadListener();

	/** {@link java.lang.System#currentTimeMillis() System time stamp} of the last resource manager reload. */
	@SideOnly(Side.CLIENT) private long lastResourceManagerReload;



	/**
	 * Private constructor for unique instantiation.
	 */
	private ResourceManagerReloadListener()
	{
		;
	}

	/**
	 * Returns the system time stamp of the last resource manager reload.
	 * 
	 * @return Last resource manager reload, or {@code 0L} if it was not reloaded yet.
	 */
	@SideOnly(Side.CLIENT)
	public static long getLastReloadTime()
	{
		return INSTANCE.lastResourceManagerReload;
	}

	@Override
	public void onResourceManagerReload(IResourceManager iResourceManager)
	{
		lastResourceManagerReload = System.currentTimeMillis();
	}
}