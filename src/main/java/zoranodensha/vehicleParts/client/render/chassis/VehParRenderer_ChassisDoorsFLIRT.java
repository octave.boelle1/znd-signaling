package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.Mat3x3;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_FLIRT;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisDoorsFLIRT;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisDoorsFLIRT extends AVehParRendererBase_FLIRT implements IVehiclePartRenderer
{
	/*
	 * Common pieces of all types.
	 */
	private static final ObjectList gen_stripes_top = model.makeGroup("Doors_Stripes_Top");
	private static final ObjectList gen_step = model.makeGroup("Doors_Step");

	/*
	 * Common pieces of single-door types.
	 */
	private static final ObjectList sg_chassis = model.makeGroup("Doors1_Chassis");
	private static final ObjectList sg_stripes_double = model.makeGroup("Doors1_Stripes_Double");
	private static final ObjectList sg_stripes_sides = model.makeGroup("Doors1_Stripes_Sides");
	private static final ObjectList sg_stripes_single = model.makeGroup("Doors1_Stripes_Single");

	/*
	 * High-level, single-door pieces.
	 */
	private static final ObjectList hiSg_door = model.makeGroup("DoorsHi1_Door");
	private static final ObjectList hiSg_grey = model.makeGroup("DoorsHi1_Grey");
	private static final ObjectList hiSg_inside = model.makeGroup("DoorsHi1_Inside");
	private static final ObjectList hiSg_window = model.makeGroup("DoorsHi1_Window");

	/*
	 * High-level, double-door pieces.
	 */
	private static final ObjectList loDb_chassis = model.makeGroup("DoorsLo2_Chassis");
	private static final ObjectList loDb_door = model.makeGroup("DoorsLo2_Door");
	private static final ObjectList loDb_grey = model.makeGroup("DoorsLo2_Grey");
	private static final ObjectList loDb_inside = model.makeGroup("DoorsLo2_Inside");
	private static final ObjectList loDb_stripes_double = model.makeGroup("DoorsLo2_Stripes_Double");
	private static final ObjectList loDb_stripes_sides = model.makeGroup("DoorsLo2_Stripes_Sides");
	private static final ObjectList loDb_stripes_single = model.makeGroup("DoorsLo2_Stripes_Single");
	private static final ObjectList loDb_window = model.makeGroup("DoorsLo2_Window");

	/*
	 * Low-level, single-door pieces.
	 */
	private static final ObjectList loSg_door = model.makeGroup("DoorsLo1_Door");
	private static final ObjectList loSg_grey = model.makeGroup("DoorsLo1_Grey");
	private static final ObjectList loSg_inside = model.makeGroup("DoorsLo1_Inside");
	private static final ObjectList loSg_window = model.makeGroup("DoorsLo1_Window");
	private static final Mat3x3 rotationMatrix = new Mat3x3().rotateY(180);

	/** The part to render. */
	final VehParChassisDoorsFLIRT part;
	private TesselatorVertexState vertexState_Opaque;
	private TesselatorVertexState vertexState_Transparent;



	public VehParRenderer_ChassisDoorsFLIRT(VehParChassisDoorsFLIRT part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisDoorsFLIRT)
		{
			return new VehParRenderer_ChassisDoorsFLIRT((VehParChassisDoorsFLIRT)copyPart);
		}
		return null;
	}

	/**
	 * Helper method to render doors on either side.
	 */
	private void renderDoors(Tessellator tessellator, float state, int sign)
	{
		/*
		 * Determine some prerequisites.
		 */
		int iterations = -1;
		float offsetScale = 1.4F;
		float initialOffsetX = 0.0F;
		ObjectList door, window;

		switch (part.prop_type.get())
		{
			default:
				door = hiSg_door;
				window = hiSg_window;
				break;

			case LO1:
				door = loSg_door;
				window = loSg_window;
				break;

			case LO2:
				offsetScale = 1.0F;
				initialOffsetX = 0.25F;
				iterations = 1;
				door = loDb_door;
				window = loDb_window;
				break;
		}

		if (state > 1.0F)
		{
			state = 1.0F;
		}

		/*
		 * Prepare door state level;
		 * 0.0 - 0.4 Extend step
		 * 0.4 - 0.6 Open door (Z)
		 * 0.6 - 1.0 Open door (X)
		 */
		float offX = (state >= 1.0F) ? 0.4F : (state > 0.6F) ? ((state - 0.6F) / 0.4F) * 0.4F : 0.0F;
		float offZ = (state > 0.6F) ? 0.02F : (state > 0.4F) ? ((state - 0.4F) / 0.2F) * 0.02F : 0.0F;
		offX *= offsetScale;

		switch (TrainRenderer.renderPass)
		{
			case 0:
				/* Render doors. */
				part.prop_colorDoors.apply(tessellator);
				for (int i = -1; i <= iterations; i += 2)
				{
					tessellator.setTranslation((initialOffsetX + offX) * i, 0.0F, sign * -(0.74F + offZ));
					if (sign == -1)
					{
						door.render(tessellator, rotationMatrix);
					}
					else
					{
						door.render(tessellator);
					}
				}

				/* Render step. */
				applyGrey(tessellator);
				float step = (state > 0.4F) ? 0.08F : (state > 0.0F) ? (state / 0.4F) * 0.08F : 0.0F;
				tessellator.setTranslation(0.0F, 0.0F, sign * -step);

				if (sign == -1)
				{
					gen_step.render(tessellator, rotationMatrix);
				}
				else
				{
					gen_step.render(tessellator);
				}
				break;

			case 1:
				part.colorWindows.apply(tessellator);
				for (int i = -1; i <= iterations; i += 2)
				{
					tessellator.setTranslation((initialOffsetX + offX) * i, 0.0F, sign * -(0.74F + offZ));
					if (sign == -1)
					{
						window.render(tessellator, rotationMatrix);
					}
					else
					{
						window.render(tessellator);
					}
				}
				break;
		}
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return true;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState_Opaque = null;
			vertexState_Transparent = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;

		switch (TrainRenderer.renderPass)
		{
			case 0: {
				tessellator.startDrawing(GL11.GL_TRIANGLES);
				{
					if (vertexState_Opaque == null)
					{
						/*
						 * Common pieces.
						 */
						part.colorStripesTop.apply(tessellator);
						gen_stripes_top.render(tessellator);

						/*
						 * Type-dependent pieces.
						 */
						switch (part.prop_type.get())
						{
							case HI1:
								part.colorOut.apply(tessellator);
								sg_chassis.render(tessellator);

								part.colorStripesDouble.apply(tessellator);
								sg_stripes_double.render(tessellator);

								part.colorSides.apply(tessellator);
								sg_stripes_sides.render(tessellator);

								part.colorStripesSingle.apply(tessellator);
								sg_stripes_single.render(tessellator);

								applyGrey(tessellator);
								hiSg_grey.render(tessellator);

								part.colorIn.apply(tessellator);
								hiSg_inside.render(tessellator);
								break;

							case LO1:
								part.colorOut.apply(tessellator);
								sg_chassis.render(tessellator);

								part.colorStripesDouble.apply(tessellator);
								sg_stripes_double.render(tessellator);

								part.colorSides.apply(tessellator);
								sg_stripes_sides.render(tessellator);

								part.colorStripesSingle.apply(tessellator);
								sg_stripes_single.render(tessellator);

								applyGrey(tessellator);
								loSg_grey.render(tessellator);

								part.colorIn.apply(tessellator);
								loSg_inside.render(tessellator);
								break;

							case LO2:
								part.colorOut.apply(tessellator);
								loDb_chassis.render(tessellator);

								part.colorStripesDouble.apply(tessellator);
								loDb_stripes_double.render(tessellator);

								part.colorSides.apply(tessellator);
								loDb_stripes_sides.render(tessellator);

								part.colorStripesSingle.apply(tessellator);
								loDb_stripes_single.render(tessellator);

								applyGrey(tessellator);
								loDb_grey.render(tessellator);

								part.colorIn.apply(tessellator);
								loDb_inside.render(tessellator);
								break;
						}

						renderDoors(tessellator, part.type_door.doorStateL(), 1);
						renderDoors(tessellator, part.type_door.doorStateR(), -1);

						tessellator.setTranslation(0, 0, 0);
						vertexState_Opaque = VertexStateCreator_Tris.getVertexState();
					}
					else
					{
						tessellator.setVertexState(vertexState_Opaque);
					}
				}
				tessellator.draw();
				break;
			}

			case 1: {
				tessellator.startDrawing(GL11.GL_TRIANGLES);
				{
					if (vertexState_Transparent == null)
					{
						renderDoors(tessellator, part.type_door.doorStateL(), 1);
						renderDoors(tessellator, part.type_door.doorStateR(), -1);

						tessellator.setTranslation(0, 0, 0);
						vertexState_Transparent = VertexStateCreator_Tris.getVertexState();
					}
					else
					{
						tessellator.setVertexState(vertexState_Transparent);
					}
				}
				tessellator.draw();
				break;
			}
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		{
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(0.0F, 0.5F, 0.0F);
				GL11.glScalef(0.675F, 0.675F, 0.675F);
			}
			else
			{
				GL11.glTranslatef(0.5F, 0.5F, 0.5F);
				GL11.glScalef(0.45F, 0.45F, 0.45F);
			}
			renderPart(1.0F);
		}
		GL11.glPopMatrix();
	}
}
