package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_DBpza;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisVestibuleRoofDBpza;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisVestibuleRoofDBpza extends AVehParRendererBase_DBpza implements IVehiclePartRenderer
{

	private static final ObjectList roof_chassis = model.makeGroup("VestibuleRoof_Chassis");
	private static final ObjectList roof_colorB = model.makeGroup("VestibuleRoof_ColorB");

	private static final ObjectList roofTransition_chassis = model.makeGroup("VestibuleRoofTransition_Chassis");
	private static final ObjectList roofTransition_colorB = model.makeGroup("VestibuleRoofTransition_ColorB");

	private final VehParChassisVestibuleRoofDBpza part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_ChassisVestibuleRoofDBpza(VehParChassisVestibuleRoofDBpza part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisVestibuleRoofDBpza)
		{
			return new VehParRenderer_ChassisVestibuleRoofDBpza((VehParChassisVestibuleRoofDBpza)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return pass == 0;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;

		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				if (part.prop_type.get() > 0)
				{
					part.colorOut.apply(tessellator);
					roofTransition_chassis.render(tessellator);

					part.colorRoof.apply(tessellator);
					roofTransition_colorB.render(tessellator);
				}
				else
				{
					part.colorOut.apply(tessellator);
					roof_chassis.render(tessellator);

					part.colorRoof.apply(tessellator);
					roof_colorB.render(tessellator);
				}

				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}

	@Override
	public void renderPartItem(ItemRenderType type)
	{
		GL11.glPushMatrix();
		{
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
				GL11.glScalef(1.5F, 1.5F, 1.5F);
			}
			GL11.glScalef(0.45F, 0.45F, 0.45F);
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			renderPart(1.0F);
		}
		GL11.glPopMatrix();
	}

}
