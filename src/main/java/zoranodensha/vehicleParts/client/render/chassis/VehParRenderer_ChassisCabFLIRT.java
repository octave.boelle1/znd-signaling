package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeCab;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_FLIRT;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisCabFLIRT;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisCabFLIRT extends AVehParRendererBase_FLIRT implements IVehiclePartRenderer
{
	/**
	 * No-door chassis pieces.
	 */
	private static final ObjectList noDoor_baseplate = model.makeGroup("CabNoDoors_Baseplate", "Cab_Baseplate", "Cab_Buffer");
	private static final ObjectList noDoor_chassis = model.makeGroup("CabNoDoors_Chassis", "Cab_Chassis");
	private static final ObjectList noDoor_inside = model.makeGroup("CabNoDoors_Inside", "Cab_Inside", "Cab_Door");
	private static final ObjectList noDoor_stripes_windows = model.makeGroup("CabNoDoors_Stripes_Windows", "Cab_Stripes_Windows");
	private static final ObjectList noDoor_windows = model.makeGroup("CabNoDoors_Windows", "Cab_Windows");

	/**
	 * Door chassis pieces.
	 */
	private static final ObjectList door_baseplate = model.makeGroup("CabDoors_Baseplate", "Cab_Baseplate", "Cab_Buffer");
	private static final ObjectList door_chassis = model.makeGroup("CabDoors_Chassis", "Cab_Chassis");
	private static final ObjectList door_doors = model.makeGroup("CabDoors_Doors");
	private static final ObjectList door_inside = model.makeGroup("CabDoors_Inside", "Cab_Inside", "Cab_Door");
	private static final ObjectList door_stripes_windows = model.makeGroup("Cab_Stripes_Windows");
	private static final ObjectList door_windows = model.makeGroup("CabDoors_Doors_Windows", "Cab_Windows");

	/**
	 * Parts that are rendered in either case.
	 */
	private static final ObjectList gen_board = model.makeGroup("Cab_Board");
	private static final ObjectList gen_desk = model.makeGroup("Cab_Desk");
	private static final ObjectList gen_grey = model.makeGroup("Cab_Grey");
	private static final ObjectList gen_lights = model.makeGroup("Cab_Lights");
	private static final ObjectList gen_stripes_front = model.makeGroup("Cab_Stripes_Front");
	private static final ObjectList gen_stripes_lights = model.makeGroup("Cab_Stripes_Lights");
	private static final ObjectList gen_stripes_sides = model.makeGroup("Cab_Stripes_Sides");
	private static final ObjectList gen_stripes_top = model.makeGroup("Cab_Stripes_Top");
	private static final ObjectList gen_windowFront = model.makeGroup("Cab_WindowFront");

	/** The part to render. */
	final VehParChassisCabFLIRT part;
	private TesselatorVertexState vertexState_Opaque;
	private TesselatorVertexState vertexState_OpaqueLight;
	private TesselatorVertexState vertexState_Transparent;



	public VehParRenderer_ChassisCabFLIRT(VehParChassisCabFLIRT part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisCabFLIRT)
		{
			return new VehParRenderer_ChassisCabFLIRT((VehParChassisCabFLIRT)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return true;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState_Opaque = null;
			vertexState_OpaqueLight = null;
			vertexState_Transparent = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		Train train = part.getTrain();

		switch (TrainRenderer.renderPass)
		{
			case 0: {
				/*
				 * Render chassis.
				 */
				tessellator.startDrawing(GL11.GL_TRIANGLES);
				{
					if (vertexState_Opaque == null)
					{
						/* Render common pieces. */
						tessellator.setColorOpaque_F(0.05F, 0.05F, 0.05F);
						gen_board.render(tessellator);

						applyGrey(tessellator);
						gen_grey.render(tessellator);

						tessellator.setColorOpaque_F(0.1F, 0.1F, 0.1F);
						gen_lights.render(tessellator);

						part.colorStripesFront.apply(tessellator);
						gen_stripes_front.render(tessellator);

						part.colorStripesLights.apply(tessellator);
						gen_stripes_lights.render(tessellator);

						part.colorSides.apply(tessellator);
						gen_stripes_sides.render(tessellator);

						part.colorStripesTop.apply(tessellator);
						gen_stripes_top.render(tessellator);

						/* Render type-specific pieces. */
						if (part.isDoorCab.get())
						{
							tessellator.setColorOpaque_F(0.12F, 0.12F, 0.12F);
							door_baseplate.render(tessellator);

							part.colorOut.apply(tessellator);
							door_chassis.render(tessellator);

							part.colorDoors.apply(tessellator);
							door_doors.render(tessellator);

							part.colorStripesWindows.apply(tessellator);
							door_stripes_windows.render(tessellator);
						}
						else
						{
							tessellator.setColorOpaque_F(0.12F, 0.12F, 0.12F);
							noDoor_baseplate.render(tessellator);

							part.colorOut.apply(tessellator);
							noDoor_chassis.render(tessellator);

							part.colorStripesWindows.apply(tessellator);
							noDoor_stripes_windows.render(tessellator);
						}

						vertexState_Opaque = VertexStateCreator_Tris.getVertexState();
					}
					else
					{
						tessellator.setVertexState(vertexState_Opaque);
					}
				}
				tessellator.draw();

				/*
				 * Render interior. Use interior lighting if applicable.
				 */
				RenderUtil.lightmapPush();
				{
					RenderUtil.lightmapBright(part);

					tessellator.startDrawing(GL11.GL_TRIANGLES);
					{
						if (vertexState_OpaqueLight == null)
						{
							tessellator.setColorOpaque_F(0.8F, 0.8F, 0.8F);
							tessellator.setTranslation(0.86F, -0.2F, 0.4475F);
							gen_desk.render(tessellator);
							tessellator.setTranslation(0.86F, -0.2F, -0.4475F);
							gen_desk.render(tessellator);
							tessellator.setTranslation(0, 0, 0);

							part.colorIn.apply(tessellator);
							(part.isDoorCab.get() ? door_inside : noDoor_inside).render(tessellator);

							vertexState_OpaqueLight = VertexStateCreator_Tris.getVertexState();
						}
						else
						{
							tessellator.setVertexState(vertexState_OpaqueLight);
						}
					}
					tessellator.draw();
				}
				RenderUtil.lightmapPop();

				/*
				 * Render destination.
				 */
				if (train != null && ModCenter.cfg.rendering.detailVehicles > 0)
				{
					PartTypeCab cab = train.getActiveCab();
					if (cab != null)
					{
						String[] arr = cab.getDestination();
						if (arr == null || arr.length == 0)
						{
							return;
						}

						String s = arr[0];
						if (s == null || s.isEmpty())
						{
							return;
						}

						GL11.glPushAttrib(GL11.GL_LIGHTING);
						GL11.glDisable(GL11.GL_LIGHTING);
						{
							FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
							final boolean isUnicode = fontRenderer.getUnicodeFlag();

							fontRenderer.setUnicodeFlag(true);
							s = fontRenderer.trimStringToWidth(s, 80);

							GL11.glColor3f(1.0F, 1.0F, 1.0F);
							GL11.glTranslatef(0.5F, 0.88F, 0.0F);
							GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
							GL11.glScalef(0.01F, -0.01F, 0.01F);

							RenderUtil.lightmapPush();
							{
								RenderUtil.lightmapBright();
								fontRenderer.drawString(s, -fontRenderer.getStringWidth(s) / 2, 0, 0xFFB90C);
								fontRenderer.setUnicodeFlag(isUnicode);
							}
							RenderUtil.lightmapPop();
						}
						GL11.glPopAttrib();
					}
				}
				break;
			}

			case 1: {
				/* Render windows. */
				tessellator.startDrawing(GL11.GL_TRIANGLES);
				{
					if (vertexState_Transparent == null)
					{
						part.colorWindows.apply(tessellator);
						(part.isDoorCab.get() ? door_windows : noDoor_windows).render(tessellator);

						part.colorWindows.apply(tessellator);
						gen_windowFront.render(tessellator);

						vertexState_Transparent = VertexStateCreator_Tris.getVertexState();
					}
					else
					{
						tessellator.setVertexState(vertexState_Transparent);
					}
				}
				tessellator.draw();

				break;
			}
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		GL11.glPushAttrib(GL11.GL_DEPTH_TEST);
		{
			GL11.glEnable(GL11.GL_DEPTH_TEST);
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(0F, 0.5F, 0F);
				GL11.glScalef(0.6F, 0.6F, 0.6F);
			}
			else
			{
				GL11.glTranslatef(0.5F, 0.5F, 0.5F);
				GL11.glScalef(0.4F, 0.4F, 0.4F);
			}
			renderPart(1.0F);
		}
		GL11.glPopAttrib();
		GL11.glPopMatrix();
	}
}
