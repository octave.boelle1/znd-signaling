package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.vehicleParts.common.parts.chassis.AVehParBaseplateModFreight;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateModFreightFlat;



@SideOnly(Side.CLIENT)
public class VehParRenderer_BaseplateModFreightFlat extends AVehParRendererBase_BaseplateModFreight implements IVehiclePartRenderer
{
	private static final ObjectList baseplate = model.makeGroup("Baseplate_Flat");



	public VehParRenderer_BaseplateModFreightFlat(AVehParBaseplateModFreight part)
	{
		super(part);
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof AVehParBaseplateModFreight)
		{
			return new VehParRenderer_BaseplateModFreightFlat((AVehParBaseplateModFreight)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		renderPlate(baseplate);

		if (part instanceof VehParBaseplateModFreightFlat && ((VehParBaseplateModFreightFlat)part).prop_hasUVID.get())
		{
			VehicleData vehicle = part.getVehicle();
			if (vehicle != null)
			{
				String uvid = vehicle.getUVID().toString();
				Minecraft mc = Minecraft.getMinecraft();
				int width = mc.fontRenderer.getStringWidth(uvid) / 2;

				GL11.glPushAttrib(GL11.GL_LIGHTING);
				GL11.glDisable(GL11.GL_LIGHTING);
				GL11.glPushMatrix();
				GL11.glColor3f(1.0F, 1.0F, 1.0F);
				GL11.glTranslatef(0.0F, 0.025F, 0.751F);
				GL11.glScalef(0.008F * 0.75F, -0.01F * 0.75F, 0.01F * 0.75F);
				mc.fontRenderer.drawString(uvid, -width, 0, 0xe2d5b5);
				GL11.glPopMatrix();
				GL11.glPopAttrib();
			}
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		renderItem(type, baseplate);
	}
}
