package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_BR101;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateBR101;



@SideOnly(Side.CLIENT)
public class VehParRenderer_BaseplateBR101 extends AVehParRendererBase_BR101 implements IVehiclePartRenderer
{
	private static final ObjectList base = model.makeGroup("Baseplate_Base");

	/** The part to render. */
	final VehParBaseplateBR101 part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_BaseplateBR101(VehParBaseplateBR101 part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParBaseplateBR101)
		{
			return new VehParRenderer_BaseplateBR101((VehParBaseplateBR101)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		/*
		 * Render baseplate
		 */
		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				part.color.apply(tessellator);
				base.render(tessellator);
				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();

		/*
		 * Render UVID, if applicable.
		 */
		if (part.hasUVID.get())
		{
			renderUVID();
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.45F, 0.45F, 0.45F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}

	/**
	 * Helper method to render the vehicle's UVID.<br>
	 * Runs checks beforehand.
	 */
	private void renderUVID()
	{
		VehicleData vehicle = part.getVehicle();
		if (vehicle == null)
		{
			return;
		}

		GL11.glPushAttrib(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_LIGHTING);
		{
			FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
			String uvid = vehicle.getUVID().toString();
			int stringX = -fontRenderer.getStringWidth(uvid) / 2;
			int colour = part.color.get().offset(0.5F).toAHEX();

			for (int i = 0; i < 2; ++i)
			{
				GL11.glPushMatrix();
				{
					if (i == 0)
					{
						GL11.glRotatef(180, 0, 1, 0);
					}

					GL11.glTranslatef(0.0F, 0.0F, 0.751F);
					GL11.glScalef(0.01F, -0.01F, 0.01F);
					fontRenderer.drawSplitString(uvid, stringX, 0, uvid.length() * 10, colour);
				}
				GL11.glPopMatrix();
			}
		}
		GL11.glPopAttrib();
	}
}
