package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeLamp.ELightMode;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.Mat3x3;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_BR101;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisCabBR101;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisCabBR101 extends AVehParRendererBase_BR101 implements IVehiclePartRenderer
{
	protected static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/vehicleParts/chassisCabBR101.png");

	private static final ObjectList chassis_Front = model.makeGroup("Cab_Chassis_Front");
	private static final ObjectList chassis_Grey = model.makeGroup("Cab_Chassis_Grey");
	private static final ObjectList chassis_Glass = model.makeGroup("Cab_Chassis_Glass");
	private static final ObjectList chassis_Sides = model.makeGroup("Cab_Chassis_Sides");
	private static final ObjectList chassis_Strip = model.makeGroup("Cab_Chassis_Strip");
	private static final ObjectList chassis_Stripes = model.makeGroup("Cab_Chassis_Stripes");

	private static final ObjectList doorL = model.makeGroup("Cab_DoorL");
	private static final ObjectList doorLInside = model.makeGroup("Cab_DoorL_Inside");
	private static final ObjectList doorR = model.makeGroup("Cab_DoorR");
	private static final ObjectList doorRInside = model.makeGroup("Cab_DoorR_Inside");

	private static final ObjectList inside = model.makeGroup("Cab_Inside");

	private static final ObjectList table = model.makeGroup("Cab_Inside_Table");

	private static final ObjectList lights_red_off = model.makeGroup("Cab_Lights_Red_0");
	private static final ObjectList lights_red_on = model.makeGroup("Cab_Lights_Red_1");
	private static final ObjectList lights_yellow_off = model.makeGroup("Cab_Lights_YellowBot_0", "Cab_Lights_YellowTop_0");
	private static final ObjectList lights_yellow_on = model.makeGroup("Cab_Lights_YellowBot_1", "Cab_Lights_YellowTop_1");

	/** The part to render. */
	final VehParChassisCabBR101 part;

	private TesselatorVertexState vertexState_Lights_Off;
	private TesselatorVertexState vertexState_Lights_On;
	private TesselatorVertexState vertexState_Opaque;
	private TesselatorVertexState vertexState_OpaqueLight;
	private TesselatorVertexState vertexState_Transparent;

	private final Mat3x3 rotationMatrixL = new Mat3x3().rotateY(95);
	private final Mat3x3 rotationMatrixR = new Mat3x3().rotateY(-95);



	public VehParRenderer_ChassisCabBR101(VehParChassisCabBR101 part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisCabBR101)
		{
			return new VehParRenderer_ChassisCabBR101((VehParChassisCabBR101)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return true;
	}

	public void renderLights(ELightMode mode, Tessellator tessellator)
	{
		/*
		 * Render active lights
		 */
		if (mode != ELightMode.OFF)
		{
			GL11.glPushAttrib(GL11.GL_LIGHTING);
			GL11.glDisable(GL11.GL_LIGHTING);
			{
				RenderUtil.lightmapPush();
				{
					RenderUtil.lightmapBright();

					tessellator.startDrawing(GL11.GL_TRIANGLES);
					if (vertexState_Lights_On == null)
					{
						tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
						switch (mode)
						{
							case FRONT:
								lights_yellow_on.render(tessellator);
								break;

							case REAR:
								lights_red_on.render(tessellator);
								break;
						}
						vertexState_Lights_On = VertexStateCreator_Tris.getVertexState();
					}
					else
					{
						tessellator.setVertexState(vertexState_Lights_On);
					}
					tessellator.draw();
				}
				RenderUtil.lightmapPop();
			}
			GL11.glPopAttrib();
		}

		/*
		 * Render inactive lights
		 */
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		if (vertexState_Lights_Off == null)
		{
			tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
			switch (mode)
			{
				case OFF:
					lights_yellow_off.render(tessellator);
					lights_red_off.render(tessellator);
					break;

				case FRONT:
					lights_red_off.render(tessellator);
					break;

				case REAR:
					lights_yellow_off.render(tessellator);
					break;
			}
			vertexState_Lights_Off = VertexStateCreator_Tris.getVertexState();
		}
		else
		{
			tessellator.setVertexState(vertexState_Lights_Off);
		}
		tessellator.draw();
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState_Lights_Off = null;
			vertexState_Lights_On = null;
			vertexState_Opaque = null;
			vertexState_OpaqueLight = null;
			vertexState_Transparent = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(texture);
		Tessellator tessellator = Tessellator.instance;

		switch (TrainRenderer.renderPass)
		{
			case 0: {
				/* Render chassis. */
				tessellator.startDrawing(GL11.GL_TRIANGLES);
				{
					if (vertexState_Opaque == null)
					{
						/* Chassis */
						tessellator.setColorOpaque_F(0.2F, 0.2F, 0.2F);
						chassis_Grey.render(tessellator);

						part.colorOut.apply(tessellator);
						chassis_Front.render(tessellator);

						part.colorChassisSide.apply(tessellator);
						chassis_Sides.render(tessellator);

						part.colorChassisStrip.apply(tessellator);
						chassis_Strip.render(tessellator);

						part.colorChassisStripes.apply(tessellator);
						chassis_Stripes.render(tessellator);

						/* Doors */
						part.colorDoors.apply(tessellator);

						tessellator.setTranslation(-0.67714D, -0.17521D, 0.73D);
						if (part.isDoorOpenL.get())
						{
							doorR.render(tessellator, rotationMatrixL);
						}
						else
						{
							doorR.render(tessellator);
						}

						tessellator.setTranslation(-0.67714D, -0.17521D, -0.73D);
						if (part.isDoorOpenR.get())
						{
							doorL.render(tessellator, rotationMatrixR);
						}
						else
						{
							doorL.render(tessellator);
						}

						tessellator.setTranslation(0, 0, 0);
						vertexState_Opaque = VertexStateCreator_Tris.getVertexState();
					}
					else
					{
						tessellator.setVertexState(vertexState_Opaque);
					}
				}
				tessellator.draw();

				RenderUtil.lightmapPush();
				{
					RenderUtil.lightmapBright(part);

					tessellator.startDrawing(GL11.GL_TRIANGLES);
					{
						if (vertexState_OpaqueLight == null)
						{
							tessellator.setColorOpaque_F(0.7F, 0.8F, 0.7F);
							table.render(tessellator);

							part.colorIn.apply(tessellator);
							inside.render(tessellator);

							/*
							 * Doors (Inside)
							 */
							part.colorDoors.apply(tessellator);

							tessellator.setTranslation(-0.67714D, -0.17521D, 0.73D);
							if (part.isDoorOpenL.get())
							{
								doorRInside.render(tessellator, rotationMatrixL);
							}
							else
							{
								doorRInside.render(tessellator);
							}

							tessellator.setTranslation(-0.67714D, -0.17521D, -0.73D);
							if (part.isDoorOpenR.get())
							{
								doorLInside.render(tessellator, rotationMatrixR);
							}
							else
							{
								doorLInside.render(tessellator);
							}
							tessellator.setTranslation(0, 0, 0);

							vertexState_OpaqueLight = VertexStateCreator_Tris.getVertexState();
						}
						else
						{
							tessellator.setVertexState(vertexState_OpaqueLight);
						}
					}
					tessellator.draw();

				}
				RenderUtil.lightmapPop();
				/* Render lights and doors. */
				renderLights(part.getLightMode(), tessellator);
				break;
			}

			case 1: {
				tessellator.startDrawing(GL11.GL_TRIANGLES);
				{
					if (vertexState_Transparent == null)
					{
						tessellator.setColorRGBA_F(0.2F, 0.2F, 0.2F, 0.75F);
						chassis_Glass.render(tessellator);
						vertexState_Transparent = VertexStateCreator_Tris.getVertexState();
					}
					else
					{
						tessellator.setVertexState(vertexState_Transparent);
					}
				}
				tessellator.draw();
				break;
			}
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		GL11.glPushAttrib(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		{
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
				GL11.glScalef(1.5F, 1.5F, 1.5F);
			}
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.5F, 0.5F, 0.5F);
			renderPart(1.0F);
		}
		GL11.glPopAttrib();
		GL11.glPopMatrix();
	}
}
