package zoranodensha.vehicleParts.client.render.chassis;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.vehicleParts.common.parts.chassis.AVehParBaseplateModFreight;



@SideOnly(Side.CLIENT)
public class VehParRenderer_BaseplateModFreightMid extends AVehParRendererBase_BaseplateModFreight implements IVehiclePartRenderer
{
	private static final ObjectList baseplate = model.makeGroup("Baseplate_Mid");



	public VehParRenderer_BaseplateModFreightMid(AVehParBaseplateModFreight part)
	{
		super(part);
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof AVehParBaseplateModFreight)
		{
			return new VehParRenderer_BaseplateModFreightMid((AVehParBaseplateModFreight)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		renderPlate(baseplate);
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		renderItem(type, baseplate);
	}
}
