package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_DBpza;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisBottomDBpza;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisBottomDBpza extends AVehParRendererBase_DBpza implements IVehiclePartRenderer
{
	//@formatter:off
	private static final ObjectList[] bottom_chassis = {
			model.makeGroup("BottomA_Chassis"),
			model.makeGroup("BottomB_Chassis"),
			model.makeGroup("BottomC_Chassis")
			};

	private static final ObjectList[] bottom_colorA = {
			model.makeGroup("BottomA_ColorA"),
			model.makeGroup("BottomB_ColorA"),
			model.makeGroup("BottomC_ColorA")
			};

	private static final ObjectList[] bottom_interior = {
			model.makeGroup("BottomA_Interior"),
			model.makeGroup("BottomB_Interior"),
			model.makeGroup("BottomC_Interior")
			};

	private static final ObjectList[] bottom_stripeBottom = {
			model.makeGroup("BottomA_StripeBottom"),
			model.makeGroup("BottomB_StripeBottom"),
			model.makeGroup("BottomC_StripeBottom")
			};

	private static final ObjectList[] bottom_windows = {
			null,
			model.makeGroup("BottomB_Windows"),
			model.makeGroup("BottomC_Windows")
			};
	//@formatter:on


	/** The part to render. */
	final VehParChassisBottomDBpza part;
	private TesselatorVertexState vertexState_Opaque;
	private TesselatorVertexState vertexState_Transparent;



	public VehParRenderer_ChassisBottomDBpza(VehParChassisBottomDBpza part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisBottomDBpza)
		{
			return new VehParRenderer_ChassisBottomDBpza((VehParChassisBottomDBpza)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return pass == 0 || pass == 1;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState_Opaque = null;
			vertexState_Transparent = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;

		int type = part.prop_type.get();

		switch (TrainRenderer.renderPass)
		{
			case 0: {
				/*
				 * Tessellator stuff.
				 */
				tessellator.startDrawing(GL11.GL_TRIANGLES);

				if (vertexState_Opaque == null)
				{
					part.colorOut.apply(tessellator);
					bottom_chassis[type].render(tessellator);

					part.colorIn.apply(tessellator);
					bottom_interior[type].render(tessellator);

					part.colorBaseplate.apply(tessellator);
					bottom_colorA[type].render(tessellator);

					part.colorStripeBottom.apply(tessellator);
					bottom_stripeBottom[type].render(tessellator);

					vertexState_Opaque = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexState_Opaque);
				}
				tessellator.draw();

				/*
				 * Render UVID, if applicable.
				 */
				if (part.hasUVID.get())
				{
					renderUVID();
				}
				break;
			}

			case 1: {
				if (type == 0)
				{
					break;
				}

				tessellator.startDrawing(GL11.GL_TRIANGLES);

				if (vertexState_Transparent == null)
				{
					tessellator.setColorRGBA_F(0.15F, 0.15F, 0.15F, 0.75F);
					bottom_windows[type].render(tessellator);

					vertexState_Transparent = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexState_Transparent);
				}

				tessellator.draw();

				break;
			}
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		/* Determine which model is rendered. */
		{
			long time = Minecraft.getMinecraft().theWorld.getTotalWorldTime();
			time %= (20 * 3);

			part.prop_type.set((int)(time / 20));
			part.updateVertexState = true;
		}

		/* Render part item. */
		GL11.glPushMatrix();
		{
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
				GL11.glScalef(1.5F, 1.5F, 1.5F);
			}
			GL11.glScalef(0.45F, 0.45F, 0.45F);
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			renderPart(1.0F);
		}
		GL11.glPopMatrix();
	}

	/**
	 * Helper method to render the vehicle's UVID.<br>
	 * Runs checks beforehand.
	 */
	private void renderUVID()
	{
		VehicleData vehicle = part.getVehicle();
		if (vehicle == null)
		{
			return;
		}

		GL11.glPushAttrib(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_LIGHTING);
		{
			FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
			String uvid = vehicle.getUVID().toString();
			int stringX = -fontRenderer.getStringWidth(uvid) / 2;
			int colour = part.colorBaseplate.get().offset(0.5F).toAHEX();

			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.0F, -0.3125F, 0.751F);
				GL11.glScalef(0.0075F, -0.01F, 0.01F);
				fontRenderer.drawSplitString(uvid, stringX, 0, uvid.length() * 10, colour);
			}
			GL11.glPopMatrix();
		}
		GL11.glPopAttrib();
	}
}
