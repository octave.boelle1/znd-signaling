package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_DBpza;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisStairsDBpza;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisStairsDBpza extends AVehParRendererBase_DBpza implements IVehiclePartRenderer
{
	//@formatter:off
	private static final ObjectList[] staircase_chassis = {
			model.makeGroup("StaircaseA_Chassis"),
			model.makeGroup("StaircaseB_Chassis")
	};

	private static final ObjectList[] staircase_colorA = {
			model.makeGroup("StaircaseA_ColorA"),
			model.makeGroup("StaircaseB_ColorA")
	};
	
	private static final ObjectList[] staircase_colorB = {
			model.makeGroup("StaircaseA_ColorB"),
			model.makeGroup("StaircaseB_ColorB")
	};

	private static final ObjectList[] staircase_interior = {
			model.makeGroup("StaircaseA_Interior"),
			model.makeGroup("StaircaseB_Interior")
	};

	private static final ObjectList[] staircase_stripeBottom = {
			model.makeGroup("StaircaseA_StripeBottom"),
			model.makeGroup("StaircaseB_StripeBottom")
	};

	private static final ObjectList[] staircase_stripeTop = {
			model.makeGroup("StaircaseA_StripeTop"),
			model.makeGroup("StaircaseB_StripeTop")
	};

	private static final ObjectList[] staircase_windows = {
			model.makeGroup("StaircaseA_Windows"),
			model.makeGroup("StaircaseB_Windows")
	};
	//@formatter:on


	/** The part to render. */
	private final VehParChassisStairsDBpza part;
	private TesselatorVertexState vertexState_Opaque;
	private TesselatorVertexState vertexState_Transparent;



	public VehParRenderer_ChassisStairsDBpza(VehParChassisStairsDBpza part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisStairsDBpza)
		{
			return new VehParRenderer_ChassisStairsDBpza((VehParChassisStairsDBpza)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return true;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState_Opaque = null;
			vertexState_Transparent = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;

		int type = part.prop_type.get();

		switch (TrainRenderer.renderPass)
		{
			case 0:
				tessellator.startDrawing(GL11.GL_TRIANGLES);

				if (vertexState_Opaque == null)
				{
					part.colorOut.apply(tessellator);
					staircase_chassis[type].render(tessellator);

					part.colorIn.apply(tessellator);
					staircase_interior[type].render(tessellator);

					part.colorBaseplate.apply(tessellator);
					staircase_colorA[type].render(tessellator);

					part.colorRoof.apply(tessellator);
					staircase_colorB[type].render(tessellator);

					part.colorStripeBottom.apply(tessellator);
					staircase_stripeBottom[type].render(tessellator);

					part.colorStripeTop.apply(tessellator);
					staircase_stripeTop[type].render(tessellator);

					vertexState_Opaque = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexState_Opaque);
				}

				tessellator.draw();
				break;

			case 1:
				tessellator.startDrawing(GL11.GL_TRIANGLES);

				if (vertexState_Transparent == null)
				{
					tessellator.setColorRGBA_F(0.15F, 0.15F, 0.15F, 0.75F);
					staircase_windows[type].render(tessellator);

					vertexState_Transparent = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexState_Transparent);
				}

				tessellator.draw();
				break;
		}
	}

	@Override
	public void renderPartItem(ItemRenderType type)
	{
		/* Determine which model is rendered. */
		{
			long time = Minecraft.getMinecraft().theWorld.getTotalWorldTime();
			time %= (20 * 2);

			part.prop_type.set((int)(time / 20));
			part.updateVertexState = true;
		}

		/* Render part item. */
		GL11.glPushMatrix();
		{
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
				GL11.glScalef(1.5F, 1.5F, 1.5F);
			}
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.5F, 0.5F, 0.5F);
			renderPart(1.0F);
		}
		GL11.glPopMatrix();
	}

}
