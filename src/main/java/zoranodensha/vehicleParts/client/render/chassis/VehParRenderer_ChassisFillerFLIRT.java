package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_FLIRT;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisFillerFLIRT;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisFillerFLIRT extends AVehParRendererBase_FLIRT implements IVehiclePartRenderer
{
	/*
	 * High-level elevated floor type.
	 */
	private static final ObjectList hi_chassis = model.makeGroup("FillerHiEl_Chassis");
	private static final ObjectList hi_grey = model.makeGroup("FillerHiEl_Grey");
	private static final ObjectList hi_inside = model.makeGroup("FillerHiEl_Inside");
	private static final ObjectList hi_stripes_double = model.makeGroup("FillerHiEl_Stripes_Double");
	private static final ObjectList hi_stripes_sides = model.makeGroup("FillerHiEl_Stripes_Sides");
	private static final ObjectList hi_stripes_single = model.makeGroup("FillerHiEl_Stripes_Single");
	private static final ObjectList hi_stripes_top = model.makeGroup("FillerHiEl_Stripes_Top");

	/*
	 * Medium-level floor type.
	 */
	private static final ObjectList md_chassis = model.makeGroup("FillerMd_Chassis");
	private static final ObjectList md_grey = model.makeGroup("FillerMd_Grey");
	private static final ObjectList md_inside = model.makeGroup("FillerMd_Inside");
	private static final ObjectList md_stripes_double = model.makeGroup("FillerMd_Stripes_Double");
	private static final ObjectList md_stripes_sides = model.makeGroup("FillerMd_Stripes_Sides");
	private static final ObjectList md_stripes_single = model.makeGroup("FillerMd_Stripes_Single");
	private static final ObjectList md_stripes_top = model.makeGroup("FillerMd_Stripes_Top");

	/** The part to render. */
	final VehParChassisFillerFLIRT part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_ChassisFillerFLIRT(VehParChassisFillerFLIRT part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisFillerFLIRT)
		{
			return new VehParRenderer_ChassisFillerFLIRT((VehParChassisFillerFLIRT)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				if (part.isHighFloor.get())
				{
					part.colorOut.apply(tessellator);
					hi_chassis.render(tessellator);

					applyGrey(tessellator);
					hi_grey.render(tessellator);

					part.colorIn.apply(tessellator);
					hi_inside.render(tessellator);

					part.colorStripesDouble.apply(tessellator);
					hi_stripes_double.render(tessellator);

					part.colorSides.apply(tessellator);
					hi_stripes_sides.render(tessellator);

					part.colorStripesSingle.apply(tessellator);
					hi_stripes_single.render(tessellator);

					part.colorStripesTop.apply(tessellator);
					hi_stripes_top.render(tessellator);
				}
				else
				{
					part.colorOut.apply(tessellator);
					md_chassis.render(tessellator);

					applyGrey(tessellator);
					md_grey.render(tessellator);

					part.colorIn.apply(tessellator);
					md_inside.render(tessellator);

					part.colorStripesDouble.apply(tessellator);
					md_stripes_double.render(tessellator);

					part.colorSides.apply(tessellator);
					md_stripes_sides.render(tessellator);

					part.colorStripesSingle.apply(tessellator);
					md_stripes_single.render(tessellator);

					part.colorStripesTop.apply(tessellator);
					md_stripes_top.render(tessellator);
				}

				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		GL11.glTranslatef(0.5F, 0.5F, 0.5F);
		GL11.glScalef(0.45F, 0.45F, 0.45F);

		renderPart(1.0F);
		GL11.glPopMatrix();
	}
}
