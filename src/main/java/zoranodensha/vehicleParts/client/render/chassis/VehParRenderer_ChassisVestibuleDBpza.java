package zoranodensha.vehicleParts.client.render.chassis;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_DBpza;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisVestibuleDBpza;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ChassisVestibuleDBpza extends AVehParRendererBase_DBpza implements IVehiclePartRenderer
{
	//@formatter:off
	private static final ObjectList[] vestibule_chassis = {
			model.makeGroup("VestibuleA_Chassis"),
			model.makeGroup("VestibuleB_Chassis"),
			model.makeGroup("VestibuleC_Chassis"),
			model.makeGroup("VestibuleD_Chassis"),
			model.makeGroup("VestibuleE_Chassis"),
			model.makeGroup("VestibuleF_Chassis")
			};

	private static final ObjectList[] vestibule_colorA = {
			model.makeGroup("VestibuleA_ColorA"),
			model.makeGroup("VestibuleB_ColorA"),
			model.makeGroup("VestibuleC_ColorA"),
			model.makeGroup("VestibuleD_ColorA"),
			model.makeGroup("VestibuleE_ColorA"),
			model.makeGroup("VestibuleF_ColorA")
			};

	private static final ObjectList[] vestibule_interior = {
			model.makeGroup("VestibuleA_Interior"),
			model.makeGroup("VestibuleB_Interior"),
			model.makeGroup("VestibuleC_Interior"),
			model.makeGroup("VestibuleD_Interior"),
			model.makeGroup("VestibuleE_Interior"),
			model.makeGroup("VestibuleF_Interior"),
			};

	private static final ObjectList[] vestibule_stripeBottom = {
			model.makeGroup("VestibuleA_StripeBottom"),
			model.makeGroup("VestibuleB_StripeBottom"),
			model.makeGroup("VestibuleC_StripeBottom"),
			model.makeGroup("VestibuleD_StripeBottom"),
			model.makeGroup("VestibuleE_StripeBottom"),
			model.makeGroup("VestibuleF_StripeBottom")
			};

	private static final ObjectList[] vestibule_stripeTop = {
			model.makeGroup("VestibuleA_StripeTop"),
			model.makeGroup("VestibuleB_StripeTop"),
			model.makeGroup("VestibuleC_StripeTop"),
			model.makeGroup("VestibuleD_StripeTop"),
			model.makeGroup("VestibuleE_StripeTop"),
			model.makeGroup("VestibuleF_StripeTop")
			};
	
	private static final ObjectList[] vestibule_windows = {
			null,
			model.makeGroup("VestibuleB_Windows"),
			model.makeGroup("VestibuleC_Windows"),
			model.makeGroup("VestibuleD_Windows"),
			model.makeGroup("VestibuleE_Windows"),
			null
			};
	//@formatter:on


	/** The part to render. */
	private final VehParChassisVestibuleDBpza part;
	private TesselatorVertexState vertexState_Opaque;
	private TesselatorVertexState vertexState_Transparent;



	public VehParRenderer_ChassisVestibuleDBpza(VehParChassisVestibuleDBpza part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParChassisVestibuleDBpza)
		{
			return new VehParRenderer_ChassisVestibuleDBpza((VehParChassisVestibuleDBpza)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		int type = part.prop_type.get();
		return (type != 0 && type != 5) || (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState_Opaque = null;
			vertexState_Transparent = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;

		int type = part.prop_type.get();

		switch (TrainRenderer.renderPass)
		{
			case 0:
				tessellator.startDrawing(GL11.GL_TRIANGLES);

				if (vertexState_Opaque == null)
				{
					part.colorOut.apply(tessellator);
					vestibule_chassis[type].render(tessellator);

					part.colorIn.apply(tessellator);
					vestibule_interior[type].render(tessellator);

					part.colorBaseplate.apply(tessellator);
					vestibule_colorA[type].render(tessellator);

					part.colorStripeBottom.apply(tessellator);
					vestibule_stripeBottom[type].render(tessellator);

					part.colorStripeTop.apply(tessellator);
					vestibule_stripeTop[type].render(tessellator);

					vertexState_Opaque = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexState_Opaque);
				}

				tessellator.draw();
				break;

			case 1:
				tessellator.startDrawing(GL11.GL_TRIANGLES);
			{
				if (vertexState_Transparent == null)
				{
					tessellator.setColorRGBA_F(0.15F, 0.15F, 0.15F, 0.75F);
					vestibule_windows[type].render(tessellator);

					vertexState_Transparent = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexState_Transparent);
				}
			}
				tessellator.draw();
				break;
		}
	}

	@Override
	public void renderPartItem(ItemRenderType type)
	{
		/* Determine which model is rendered. */
		{
			long time = Minecraft.getMinecraft().theWorld.getTotalWorldTime();
			time %= (20 * 6);

			part.prop_type.set((int)(time / 20));
			part.updateVertexState = true;
		}

		/* Render part item. */
		GL11.glPushMatrix();
		{
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
				GL11.glScalef(1.5F, 1.5F, 1.5F);
			}
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.6F, 0.6F, 0.6F);
			renderPart(1.0F);
		}
		GL11.glPopMatrix();
	}

}
