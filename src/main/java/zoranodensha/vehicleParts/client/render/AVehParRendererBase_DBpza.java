package zoranodensha.vehicleParts.client.render;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public abstract class AVehParRendererBase_DBpza
{
	public static final ObjModelSpeedy model = new ObjModelSpeedy(ModData.ID, ModCenter.DIR_RESOURCES_VEHPAR + "DBpza");
}