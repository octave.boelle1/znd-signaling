package zoranodensha.vehicleParts.client.render.basic;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer.IVehiclePartSpecialRenderer;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.basic.VehParBasicCube;



@SideOnly(Side.CLIENT)
public class VehParRenderer_BasicCube implements IVehiclePartSpecialRenderer
{
	/** The part to render. */
	final VehParBasicCube part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_BasicCube(VehParBasicCube part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParBasicCube)
		{
			return new VehParRenderer_BasicCube((VehParBasicCube)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (part.color.get().getA() < 1.0F) ? (pass == 1) : (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();

		if (vertexState == null)
		{
			/*
			 * Calculate texture (maxU, maxV, maxW) and vertex bounds (maxX, maxY, maxZ).
			 */
			float[] arr = part.getScale();
			float maxU = Math.abs(arr[0]) * 0.5F;
			float maxV = Math.abs(arr[1]) * 0.5F;
			float maxW = Math.abs(arr[2]) * 0.5F;
			float maxX = maxU;
			float maxY = maxV;
			float maxZ = maxW;

			if (maxU > 1.0F)
			{
				maxU = 1.0F;
			}
			if (maxV > 1.0F)
			{
				maxV = 1.0F;
			}
			if (maxW > 1.0F)
			{
				maxW = 1.0F;
			}

			/* Part colour */
			part.color.apply(tessellator);

			/*
			 * Render all faces in order.
			 */
			/* Top (+Y) */
			tessellator.setNormal(0.0F, 1.0F, 0.0F);
			tessellator.addVertexWithUV(maxX, maxY, maxZ, 0.0D, maxW); // Bottom-Right
			tessellator.addVertexWithUV(maxX, maxY, -maxZ, 0.0D, 0.0D); // Top-Right
			tessellator.addVertexWithUV(-maxX, maxY, -maxZ, maxU, 0.0D); // Top-Left
			tessellator.addVertexWithUV(-maxX, maxY, maxZ, maxU, maxW); // Bottom-Left

			/* Bottom (-Y) */
			tessellator.setNormal(0.0F, -1.0F, 0.0F);
			tessellator.addVertexWithUV(maxX, -maxY, -maxZ, maxU, 0.0D);
			tessellator.addVertexWithUV(maxX, -maxY, maxZ, maxU, maxW);
			tessellator.addVertexWithUV(-maxX, -maxY, maxZ, 0.0D, maxW);
			tessellator.addVertexWithUV(-maxX, -maxY, -maxZ, 0.0D, 0.0D);

			/* Front (+X) */
			tessellator.setNormal(1.0F, 0.0F, 0.0F);
			tessellator.addVertexWithUV(maxX, -maxY, -maxZ, maxW, maxV);
			tessellator.addVertexWithUV(maxX, maxY, -maxZ, maxW, 0.0D);
			tessellator.addVertexWithUV(maxX, maxY, maxZ, 0.0D, 0.0D);
			tessellator.addVertexWithUV(maxX, -maxY, maxZ, 0.0D, maxV);

			/* Back (-X) */
			tessellator.setNormal(-1.0F, 0.0F, 0.0F);
			tessellator.addVertexWithUV(-maxX, -maxY, maxZ, maxW, maxV);
			tessellator.addVertexWithUV(-maxX, maxY, maxZ, maxW, 0.0D);
			tessellator.addVertexWithUV(-maxX, maxY, -maxZ, 0.0D, 0.0D);
			tessellator.addVertexWithUV(-maxX, -maxY, -maxZ, 0.0D, maxV);

			/* Right (+Z) */
			tessellator.setNormal(0.0F, 0.0F, 1.0F);
			tessellator.addVertexWithUV(maxX, -maxY, maxZ, maxU, maxV);
			tessellator.addVertexWithUV(maxX, maxY, maxZ, maxU, 0.0D);
			tessellator.addVertexWithUV(-maxX, maxY, maxZ, 0.0D, 0.0D);
			tessellator.addVertexWithUV(-maxX, -maxY, maxZ, 0.0D, maxV);

			/* Left (-Z) */
			tessellator.setNormal(0.0F, 0.0F, -1.0F);
			tessellator.addVertexWithUV(maxX, maxY, -maxZ, maxU, maxV);
			tessellator.addVertexWithUV(maxX, -maxY, -maxZ, maxU, 0.0D);
			tessellator.addVertexWithUV(-maxX, -maxY, -maxZ, 0.0D, 0.0D);
			tessellator.addVertexWithUV(-maxX, maxY, -maxZ, 0.0D, maxV);

			/* Create vertex state. */
			vertexState = tessellator.getVertexState(0, 0, 0);
		}
		else
		{
			tessellator.setVertexState(vertexState);
		}
		tessellator.draw();
	}

	@Override
	public void renderPart_Post()
	{
	}

	@Override
	public boolean renderPart_Pre(float partialTick)
	{
		float[] arr = part.getOffset();
		GL11.glTranslatef(arr[0], arr[1], arr[2]);

		arr = part.getRotation();
		GL11.glRotatef(arr[1], 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(arr[2], 0.0F, 0.0F, 1.0F);
		GL11.glRotatef(arr[0], 1.0F, 0.0F, 0.0F);

		return true;
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.65F, 0.65F, 0.65F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}
}
