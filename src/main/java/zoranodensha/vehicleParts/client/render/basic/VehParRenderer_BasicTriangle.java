package zoranodensha.vehicleParts.client.render.basic;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer.IVehiclePartSpecialRenderer;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.basic.VehParBasicTriangle;



@SideOnly(Side.CLIENT)
public class VehParRenderer_BasicTriangle implements IVehiclePartSpecialRenderer
{
	/** The part to render. */
	final VehParBasicTriangle part;



	public VehParRenderer_BasicTriangle(VehParBasicTriangle part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParBasicTriangle)
		{
			return new VehParRenderer_BasicTriangle((VehParBasicTriangle)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (part.color.get().getA() < 1.0F) ? (pass == 1) : (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);

		/*
		 * Preparation Calculate maximum texture positions (maxU, maxV, maxW) for their respective axes. Also calculate maximum vertex positions (maxX, maxY, maxZ).
		 */
		float[] arr = part.getScale();
		float maxU = Math.abs(arr[0]) * 0.5F;
		float maxV = Math.abs(arr[1]) * 0.5F;
		float maxW = Math.abs(arr[2]) * 0.5F;
		float maxX = maxU;
		float maxY = maxV;
		float maxZ = maxW;

		if (maxU > 1.0F)
		{
			maxU = 1.0F;
		}

		if (maxV > 1.0F)
		{
			maxV = 1.0F;
		}

		if (maxW > 1.0F)
		{
			maxW = 1.0F;
		}

		/*
		 * Rendering Render all faces in order.
		 */
		Tessellator tessellator = Tessellator.instance;

		/* Bottom (-Y) */
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, -1.0F, 0.0F);
		tessellator.addVertexWithUV(maxX, -maxY, -maxZ, maxU, 0.0D);
		tessellator.addVertexWithUV(maxX, -maxY, maxZ, maxU, maxW);
		tessellator.addVertexWithUV(-maxX, -maxY, maxZ, 0.0D, maxW);
		tessellator.addVertexWithUV(-maxX, -maxY, -maxZ, 0.0D, 0.0D);
		tessellator.draw();

		/* Front (+X) */
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		tessellator.setNormal(1.0F, 0.0F, 0.0F);
		tessellator.addVertexWithUV(maxX, -maxY, -maxZ, maxW, maxV);
		tessellator.addVertexWithUV(maxX, maxY, 0.0D, maxW * 0.5D, 0.0D);
		tessellator.addVertexWithUV(maxX, -maxY, maxZ, 0.0D, maxV);
		tessellator.draw();

		/* Back (-X) */
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		tessellator.setNormal(-1.0F, 0.0F, 0.0F);
		tessellator.addVertexWithUV(-maxX, -maxY, maxZ, maxW, maxV);
		tessellator.addVertexWithUV(-maxX, maxY, 0.0D, maxW * 0.5D, 0.0D);
		tessellator.addVertexWithUV(-maxX, -maxY, -maxZ, 0.0D, maxV);
		tessellator.draw();
		boolean flag = maxV < 0.5D;

		/* Right (+Z) */
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 1.0F, 1.0F);
		tessellator.addVertexWithUV(maxX, -maxY, maxZ, maxU, flag ? (maxV + ((0.5D - maxV) * 0.5D)) : maxV);
		tessellator.addVertexWithUV(maxX, maxY, 0.0D, maxU, 0.0D);
		tessellator.addVertexWithUV(-maxX, maxY, 0.0D, 0.0D, 0.0D);
		tessellator.addVertexWithUV(-maxX, -maxY, maxZ, 0.0D, flag ? (maxV + ((0.5D - maxV) * 0.5D)) : maxV);
		tessellator.draw();

		/* Left (-Z) */
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 1.0F, -1.0F);
		tessellator.addVertexWithUV(maxX, maxY, 0.0D, maxU, flag ? (maxV + ((0.5D - maxV) * 0.5D)) : maxV);
		tessellator.addVertexWithUV(maxX, -maxY, -maxZ, maxU, 0.0D);
		tessellator.addVertexWithUV(-maxX, -maxY, -maxZ, 0.0D, 0.0D);
		tessellator.addVertexWithUV(-maxX, maxY, 0.0D, 0.0D, flag ? (maxV + ((0.5D - maxV) * 0.5D)) : maxV);
		tessellator.draw();
	}

	@Override
	public void renderPart_Post()
	{
	}

	@Override
	public boolean renderPart_Pre(float partialTick)
	{
		part.color.apply();

		float[] arr = part.getOffset();
		GL11.glTranslatef(arr[0], arr[1], arr[2]);

		arr = part.getRotation();
		GL11.glRotatef(arr[1], 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(arr[2], 0.0F, 0.0F, 1.0F);
		GL11.glRotatef(arr[0], 1.0F, 0.0F, 0.0F);

		return true;
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.65F, 0.65F, 0.65F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}
}
