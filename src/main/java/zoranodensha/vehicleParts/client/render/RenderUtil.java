package zoranodensha.vehicleParts.client.render;

import java.util.EmptyStackException;
import java.util.Random;
import java.util.Stack;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemCloth;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.ForgeHooksClient;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.PartTypeCab;
import zoranodensha.common.core.ModData;



/**
 * Most code is taken from Minecraft's {@link net.minecraft.client.renderer.entity.RenderItem RenderItem}, and thus no Copyright claims on this class file are made.
 */
@SideOnly(Side.CLIENT)
public abstract class RenderUtil
{
	public static final ResourceLocation texture_white = new ResourceLocation(ModData.ID, "textures/basic/white.png");
	public static final RenderBlocks renderBlocks = RenderBlocks.getInstance();

	private static final ResourceLocation RES_ITEM_GLINT = new ResourceLocation("textures/misc/enchanted_item_glint.png");
	private static final EntityItem entityItem = new EntityItem(Minecraft.getMinecraft().theWorld);
	private static final RenderManager renderManager = RenderManager.instance;
	private static final Random ran = new Random(187L);

	/**
	 * Lightmap stack used to remember existing lightmap settings when doing lightmap hacks.
	 */
	private static final Stack<LightmapState> lightmapStack = new Stack<LightmapState>();



	/**
	 * Sets Minecraft's lightmap settings accordingly so rendered objects appear as if they are in daylight (similar to disabling lighting). This method can be used when
	 * {@code GL11.glDisable(GL11.GL_LIGHTING)} does not produce a sufficiently 'bright' result.
	 */
	public static void lightmapBright()
	{
		int maxBright = (15 << 20) | (15 << 4);
		lightmapBright(maxBright % 65536, maxBright / 65536);
	}

	/**
	 * Overrides the current lightmap texture coordinates to the specified values.<br>
	 * Use this to specify brightness more precisely.
	 * 
	 * @see #lightmapBright()
	 */
	public static void lightmapBright(float u, float v)
	{
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, u, v);
	}

	/**
	 * Helper method to apply cab light for the given vehicle part.
	 */
	public static void lightmapBright(VehParBase part)
	{
		/* Ensure there exists a parent train. */
		Train train = part.getTrain();
		if (train == null)
		{
			return;
		}

		/* Update lightmap texture coordinates if the active cab intersects with the given part. */
		PartTypeCab activeCab = train.getActiveCab();
		if (activeCab != null)
		{
			if (activeCab.getParent() == part || activeCab.getParent().getBoundingBox().intersectsWith(part.getBoundingBox()))
			{
				if (activeCab.button_lights_cab.isActive())
				{
					/*
					 * Determine the new X UV coordinate.
					 */
					float newBrightnessX = OpenGlHelper.lastBrightnessX + 180.0F;

					/*
					 * Cap the X coordinate so it doesn't go out of bounds and cause a transparency issue.
					 */
					if (newBrightnessX > 240.0F)
					{
						newBrightnessX = 240.0F;
					}

					RenderUtil.lightmapBright(newBrightnessX, OpenGlHelper.lastBrightnessY);
				}
			}
		}
	}

	/**
	 * Pushes the current lightmap settings so they can be retrieved later.
	 */
	public static void lightmapPush()
	{
		lightmapStack.push(new LightmapState(OpenGlHelper.lastBrightnessX, OpenGlHelper.lastBrightnessY));
	}

	/**
	 * Pops the saved lightmap settings from when they were pushed with {@link RenderUtil#lightmapPush()}.<br>
	 * Applies the popped settings back to the lightmap.
	 * 
	 * @throws EmptyStackException If the lightmap stack is empty.
	 */
	public static void lightmapPop()
	{
		LightmapState top = lightmapStack.pop();
		OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, top.x, top.y);
	}


	/**
	 * Simplified copy of {@link RenderItem#renderDroppedItem(EntityItem, IIcon, int, float, float, float, float, int) renderDroppedItem()}.
	 *
	 * @param itemStack - The ItemStack to render
	 * @param iIcon - The IIcon to draw
	 * @param colR - Red color percentage
	 * @param colG - Green color percentage
	 * @param colB - Blue color percentage
	 * @param pass - The render pass
	 */
	private static void renderDroppedItem(ItemStack itemStack, IIcon iIcon, float colR, float colG, float colB, int pass)
	{
		Tessellator tessellator = Tessellator.instance;

		if (iIcon == null)
		{
			TextureManager texturemanager = Minecraft.getMinecraft().getTextureManager();
			ResourceLocation resourcelocation = texturemanager.getResourceLocation(itemStack.getItemSpriteNumber());
			iIcon = ((TextureMap)texturemanager.getTexture(resourcelocation)).getAtlasSprite("missingno");
		}

		float minU = iIcon.getMinU();
		float maxU = iIcon.getMaxU();
		float minV = iIcon.getMinV();
		float maxV = iIcon.getMaxV();

		if (renderManager.options.fancyGraphics)
		{
			GL11.glPushMatrix();
			float f9 = 0.0625F;
			float f10 = 0.021875F;
			GL11.glTranslatef(-0.5F, -0.25F, (f9 + f10) * 0.0F);

			switch (itemStack.getItemSpriteNumber())
			{
				case 0:
					renderManager.renderEngine.bindTexture(TextureMap.locationBlocksTexture);
					break;

				default:
					renderManager.renderEngine.bindTexture(TextureMap.locationItemsTexture);
					break;
			}

			GL11.glColor4f(colR, colG, colB, 1.0F);
			ItemRenderer.renderItemIn2D(tessellator, maxU, minV, minU, maxV, iIcon.getIconWidth(), iIcon.getIconHeight(), f9);

			if (itemStack.hasEffect(pass))
			{
				GL11.glDepthFunc(GL11.GL_EQUAL);
				GL11.glDisable(GL11.GL_LIGHTING);
				renderManager.renderEngine.bindTexture(RES_ITEM_GLINT);
				GL11.glEnable(GL11.GL_BLEND);
				GL11.glBlendFunc(GL11.GL_SRC_COLOR, GL11.GL_ONE);
				GL11.glColor4f(0.5F * 0.76F, 0.25F * 0.76F, 0.8F * 0.76F, 1.0F);
				GL11.glMatrixMode(GL11.GL_TEXTURE);

				GL11.glPushMatrix();
				GL11.glScalef(0.125F, 0.125F, 0.125F);
				GL11.glTranslatef((Minecraft.getSystemTime() % 3000L) / 3000.0F * 8.0F, 0.0F, 0.0F);
				GL11.glRotatef(-50.0F, 0.0F, 0.0F, 1.0F);
				ItemRenderer.renderItemIn2D(tessellator, 0.0F, 0.0F, 1.0F, 1.0F, 255, 255, f9);
				GL11.glPopMatrix();

				GL11.glPushMatrix();
				GL11.glScalef(0.125F, 0.125F, 0.125F);
				GL11.glTranslatef(-((Minecraft.getSystemTime() % 4873L) / 4873.0F * 8.0F), 0.0F, 0.0F);
				GL11.glRotatef(10.0F, 0.0F, 0.0F, 1.0F);
				ItemRenderer.renderItemIn2D(tessellator, 0.0F, 0.0F, 1.0F, 1.0F, 255, 255, f9);
				GL11.glPopMatrix();

				GL11.glMatrixMode(GL11.GL_MODELVIEW);
				GL11.glDisable(GL11.GL_BLEND);
				GL11.glEnable(GL11.GL_LIGHTING);
				GL11.glDepthFunc(GL11.GL_LEQUAL);
			}

			GL11.glPopMatrix();
		}
		else
		{
			GL11.glPushMatrix();
			GL11.glColor4f(colR, colG, colB, 1.0F);
			tessellator.startDrawingQuads();
			tessellator.setNormal(0.0F, 1.0F, 0.0F);
			tessellator.addVertexWithUV(-0.5F, -0.25F, 0.0D, minU, maxV);
			tessellator.addVertexWithUV(0.5F, -0.25F, 0.0D, maxU, maxV);
			tessellator.addVertexWithUV(0.5F, 0.75F, 0.0D, maxU, minV);
			tessellator.addVertexWithUV(-0.5F, 0.75F, 0.0D, minU, minV);
			tessellator.draw();
			GL11.glPopMatrix();
		}
	}

	/**
	 * Heavily simplified version of {@link RenderItem#doRender(EntityItem, double, double, double, float, float) doRender()}.
	 *
	 * @param itemStack - The ItemStack to render.
	 */
	public static void renderItemStack(ItemStack itemStack)
	{
		/* Check whether conditions are met. */
		TextureManager renderEngine = renderManager.renderEngine;
		if (itemStack == null || itemStack.stackSize <= 0 || renderEngine == null)
		{
			return;
		}

		/* Cache texture and prepary static EntityItem. */
		ResourceLocation texture = renderEngine.getResourceLocation(itemStack.getItemSpriteNumber());
		entityItem.setEntityItemStack(itemStack);

		/* Bind texture and prepare OpenGL. */
		renderEngine.bindTexture(texture);
		TextureUtil.func_152777_a(false, false, 1.0F);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);

		/* Search for the corresponding rendering mechanism. */
		if (ForgeHooksClient.renderEntityItem(entityItem, itemStack, 0.0F, 0.0F, ran, renderEngine, renderBlocks, 1))
		{
			;
		}
		else if (renderItemStackAsBlock(itemStack))
		{
			;
		}
		else
		{
			renderItemStackAsItem(itemStack);
		}

		/* Unbind texture, undo OpenGL changes. */
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		renderEngine.bindTexture(texture);
		TextureUtil.func_147945_b();
	}

	/**
	 * Check whether this ItemStack can be rendered as 3D item block. If so, render.
	 *
	 * @return True if ItemStack renders as 3D block.
	 */
	private static boolean renderItemStackAsBlock(ItemStack itemStack)
	{
		if (itemStack.getItemSpriteNumber() == 0 && itemStack.getItem() instanceof ItemBlock)
		{
			Block block = Block.getBlockFromItem(itemStack.getItem());
			int type = block.getRenderType();

			if (RenderBlocks.renderItemIn3d(type))
			{
				float scale;
				switch (type)
				{
					case 1:
					case 2:
					case 12:
					case 19:
						scale = 0.5F;
						break;

					default:
						scale = 0.25F;
						break;
				}

				if (block.getRenderBlockPass() > 0)
				{
					GL11.glAlphaFunc(GL11.GL_GREATER, 0.1F);
					GL11.glEnable(GL11.GL_BLEND);
					OpenGlHelper.glBlendFunc(770, 771, 1, 0);
				}

				GL11.glScalef(scale, scale, scale);
				renderBlocks.renderBlockAsItem(block, itemStack.getItemDamage(), 1.0F);

				if (block.getRenderBlockPass() > 0)
				{
					GL11.glDisable(GL11.GL_BLEND);
				}

				return true;
			}
		}
		return false;
	}

	/**
	 * Renders the given ItemStack as item.
	 */
	private static void renderItemStackAsItem(ItemStack itemStack)
	{
		Item item = itemStack.getItem();
		final float divBy255 = 1.0F / 255.0F;
		float colR;
		float colG;
		float colB;
		int col;
		GL11.glScalef(0.5F, 0.5F, 0.5F);

		if (item.requiresMultipleRenderPasses())
		{
			int maxPass = item.getRenderPasses(itemStack.getItemDamage());

			for (int pass = 0; pass < maxPass; ++pass)
			{
				col = item.getColorFromItemStack(itemStack, pass);
				colR = (col >> 16 & 255) * divBy255;
				colG = (col >> 8 & 255) * divBy255;
				colB = (col & 255) * divBy255;
				GL11.glColor4f(colR, colG, colB, 1.0F);
				renderDroppedItem(itemStack, item.getIcon(itemStack, pass), colR, colG, colB, pass);
			}
		}
		else
		{
			boolean isClothItem = item instanceof ItemCloth;
			if (isClothItem)
			{
				GL11.glAlphaFunc(GL11.GL_GREATER, 0.1F);
				GL11.glEnable(GL11.GL_BLEND);
				OpenGlHelper.glBlendFunc(770, 771, 1, 0);
			}

			col = item.getColorFromItemStack(itemStack, 0);
			renderDroppedItem(itemStack, itemStack.getIconIndex(), (col >> 16 & 255) * divBy255, (col >> 8 & 255) * divBy255, (col & 255) * divBy255, 0);

			if (isClothItem)
			{
				GL11.glDisable(GL11.GL_BLEND);
			}
		}
	}



	/**
	 * Lightmap Stack Object
	 * 
	 * @author Jaffa
	 */
	private static class LightmapState
	{
		public float x, y;



		public LightmapState(float x, float y)
		{
			this.x = x;
			this.y = y;
		}
	}

}
