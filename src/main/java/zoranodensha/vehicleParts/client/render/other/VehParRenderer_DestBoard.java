package zoranodensha.vehicleParts.client.render.other;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeCab;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.other.VehParDestBoard;



@SideOnly(Side.CLIENT)
public class VehParRenderer_DestBoard implements IVehiclePartRenderer
{
	/** Part model. */
	private static final ObjModelSpeedy model = new ObjModelSpeedy(ModData.ID, ModCenter.DIR_RESOURCES_VEHPAR + "ModelTrainPart_destBoards");

	/* Small screen. */
	private static final ObjectList frame_M = model.makeGroup("Frame_M");
	private static final ObjectList screen_M = model.makeGroup("Screen_M");

	/* Large screen. */
	private static final ObjectList frame_L = model.makeGroup("Frame_L");
	private static final ObjectList screen_L = model.makeGroup("Screen_L");

	/* Maximum line width of destination info. */
	private static final int MAX_LINE_WIDTH = 90;

	/** The part to render. */
	final VehParDestBoard part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_DestBoard(VehParDestBoard part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParDestBoard)
		{
			return new VehParRenderer_DestBoard((VehParDestBoard)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		/*
		 * Render board screen and frame.
		 */
		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				if (part.prop_isLarge.get())
				{
					part.color.apply(tessellator);
					frame_L.render(tessellator);

					tessellator.setColorOpaque_F(0.05F, 0.05F, 0.05F);
					screen_L.render(tessellator);
				}
				else
				{
					part.color.apply(tessellator);
					frame_M.render(tessellator);

					tessellator.setColorOpaque_F(0.05F, 0.05F, 0.05F);
					screen_M.render(tessellator);
				}
				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();

		/*
		 * Render text on board.
		 */
		Train parent = part.getTrain();
		if (parent != null && ModCenter.cfg.rendering.detailVehicles > 0)
		{
			PartTypeCab cab = parent.getActiveCab();
			if (cab != null)
			{
				String[] arr = cab.getDestination();
				if (arr == null || arr.length == 0)
				{
					return;
				}

				FontRenderer fontRenderer = Minecraft.getMinecraft().fontRenderer;
				final boolean isUnicode = fontRenderer.getUnicodeFlag();
				fontRenderer.setUnicodeFlag(true);

				/* Populate array with destination info, depending on board size. */
				String[] s = new String[Math.min(arr.length, part.prop_isLarge.get() ? 2 : 1)];
				for (int i = 0; i < s.length; ++i)
				{
					s[i] = (arr[i] == null) ? "" : (i == 0) ? fontRenderer.trimStringToWidth(arr[i], MAX_LINE_WIDTH) : arr[i];
				}

				/* If second element (=dest. info) exists, make it scrollable. */
				if (s.length > 1 && !s[1].isEmpty())
				{
					if (fontRenderer.getStringWidth(s[1]) >= MAX_LINE_WIDTH)
					{
						int offset = parent.ticksExisted / 4;

						for (int i = 0; i < 15; ++i)
						{
							s[1] += " ";
						}

						for (int i = offset % s[1].length(); i > 0; --i)
						{
							s[1] = s[1].substring(1) + s[1].substring(0, 1);
						}

						s[1] = fontRenderer.trimStringToWidth(s[1], MAX_LINE_WIDTH);
					}
				}

				/* Render text. */
				GL11.glPushAttrib(GL11.GL_LIGHTING);
				GL11.glDisable(GL11.GL_LIGHTING);
				{
					RenderUtil.lightmapPush();
					{
						RenderUtil.lightmapBright();

						GL11.glTranslatef(0.0F, (s.length > 1) ? 0.1F : 0.05F, 0.01F);
						GL11.glScalef(0.01F, -0.01F, 0.01F);

						if (s.length > 0 && !s[0].isEmpty())
						{
							fontRenderer.drawString(s[0], -fontRenderer.getStringWidth(s[0]) / 2, 0, 0xFFB90C);
						}
						if (s.length > 1 && !s[1].isEmpty())
						{
							GL11.glTranslatef(0.0F, 10.0F, 0.0F);
							fontRenderer.drawString(s[1], -fontRenderer.getStringWidth(s[1]) / 2, 0, 0xFFB90C);
						}
					}
					RenderUtil.lightmapPop();
				}
				GL11.glPopAttrib();

				fontRenderer.setUnicodeFlag(isUnicode);
			}
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glScalef(2.0F, 2.0F, 2.0F);
		{
			if (type == IItemRenderer.ItemRenderType.ENTITY)
			{
				GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			}
			else
			{
				GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			}
			renderPart(1.0F);
		}
		GL11.glPopMatrix();
	}
}