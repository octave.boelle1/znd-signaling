package zoranodensha.vehicleParts.client.render.other;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.other.VehParCompressor;



public class VehParRenderer_Compressor implements IVehiclePartRenderer
{
	/** The OBJ model of the compressor. */
	private static final ObjModelSpeedy model = new ObjModelSpeedy(ModData.ID, ModCenter.DIR_RESOURCES_VEHPAR + "ModelTrainPart_compressor");

	/*
	 * Object Lists
	 */
	private static final ObjectList objectButtonEmergency = model.makeGroup("ButtonEmergency");
	private static final ObjectList objectButtons = model.makeGroup("Buttons");
	private static final ObjectList objectCaps = model.makeGroup("Caps");
	private static final ObjectList objectControlPanel = model.makeGroup("ControlPanel");
	private static final ObjectList objectFrame = model.makeGroup("Frame");
	private static final ObjectList objectScreen = model.makeGroup("Screen");
	private static final ObjectList objectTank = model.makeGroup("Tank");

	/**
	 * The vertex state used for rendering.
	 * Persists over render ticks to improve render performance.
	 */
	private TesselatorVertexState vertexState;

	/**
	 * The vehicle part which this renderer refers to.
	 */
	final VehParCompressor part;



	/**
	 * Initialises a new instance of the {@link zoranodensha.vehicleParts.client.render.other.VehParRenderer_Compressor} class.
	 * 
	 * @param part - The vehicle part which this renderer should render.
	 */
	public VehParRenderer_Compressor(VehParCompressor part)
	{
		this.part = part;
	}

	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParCompressor)
		{
			return new VehParRenderer_Compressor((VehParCompressor)copyPart);
		}

		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return pass == 0;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				tessellator.setColorOpaque_F(0.8F, 0.1F, 0.1F); // Red Components
				objectButtonEmergency.render(tessellator);

				tessellator.setColorOpaque_F(0.1F, 0.1F, 0.6F); // Blue Components
				objectButtons.render(tessellator);

				tessellator.setColorOpaque_F(0.6F, 0.6F, 0.6F); // Silver Components
				objectCaps.render(tessellator);
				objectFrame.render(tessellator);

				tessellator.setColorOpaque_F(0.2F, 0.2F, 0.2F); // Gray Components
				objectControlPanel.render(tessellator);
				objectTank.render(tessellator);

				tessellator.setColorOpaque_F(0.7F, 0.7F, 0.7F); // Light Gray Components
				objectScreen.render(tessellator);

				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}

	@Override
	public void renderPartItem(ItemRenderType type)
	{
		GL11.glPushMatrix();
		{
			if (type.equals(IItemRenderer.ItemRenderType.ENTITY))
			{
				GL11.glTranslatef(0.25F, 0.0F, 0.25F);
				GL11.glScalef(1.5F, 1.5F, 1.5F);
			}
			else
			{
				GL11.glTranslatef(0.5F, 0.2F, 0.5F);
				GL11.glScalef(0.90F, 0.90F, 0.90F);
			}

			renderPart(1.0F);
		}
		GL11.glPopMatrix();
	}

}
