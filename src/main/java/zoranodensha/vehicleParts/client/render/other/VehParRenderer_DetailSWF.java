package zoranodensha.vehicleParts.client.render.other;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_SWF;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.other.VehParDetailSWF;
import zoranodensha.vehicleParts.presets.SlidingWallFreight_S.EDetailType;



@SideOnly(Side.CLIENT)
public class VehParRenderer_DetailSWF extends AVehParRendererBase_SWF implements IVehiclePartRenderer
{
	/*
	 * Objects
	 */
	private static final ObjectList detailMount = model.makeGroup("DetailB");
	private static final ObjectList detailHook = model.makeGroup("DetailC");
	private static final ObjectList detailValve = model.makeGroup("DetailD");

	private final VehParDetailSWF part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_DetailSWF(VehParDetailSWF part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParDetailSWF)
		{
			return new VehParRenderer_DetailSWF((VehParDetailSWF)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return pass == 0;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				part.color.apply(tessellator);

				switch (part.prop_type.get())
				{
					case HOOK:
						detailHook.render(tessellator);
						break;

					case MOUNT:
						detailMount.render(tessellator);
						break;

					case VALVE:
						detailValve.render(tessellator);
						break;
				}

				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}

	@Override
	public void renderPartItem(ItemRenderType type)
	{
		/* Determine which model is rendered. */
		{
			long time = Minecraft.getMinecraft().theWorld.getTotalWorldTime() % 60;
			if (time < 20)
			{
				part.prop_type.set(EDetailType.HOOK);
				part.updateVertexState = true;
			}
			else if (time < 40)
			{
				part.prop_type.set(EDetailType.MOUNT);
				part.updateVertexState = true;
			}
			else
			{
				part.prop_type.set(EDetailType.VALVE);
				part.updateVertexState = true;
			}
		}

		/* Actually render the model. */
		GL11.glPushMatrix();
		GL11.glTranslatef(0.5F, 0.5F, 0.5F);
		GL11.glScalef(2.5F, 2.5F, 2.5F);
		renderPart(1.0F);
		GL11.glPopMatrix();
	}

}
