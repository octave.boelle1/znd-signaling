package zoranodensha.vehicleParts.client.render.other;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_FLIRT;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.other.VehParElectricsFLIRT;
import zoranodensha.vehicleParts.common.parts.other.VehParElectricsFLIRT.EType;



@SideOnly(Side.CLIENT)
public class VehParRenderer_ElectricsFLIRT extends AVehParRendererBase_FLIRT implements IVehiclePartRenderer
{
	private static final ObjectList electricsL = model.makeGroup("ElectricsL");
	private static final ObjectList electricsM = model.makeGroup("ElectricsM");

	/** The part to render. */
	final VehParElectricsFLIRT part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_ElectricsFLIRT(VehParElectricsFLIRT part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParElectricsFLIRT)
		{
			return new VehParRenderer_ElectricsFLIRT((VehParElectricsFLIRT)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				applyGrey(tessellator);
				(part.prop_type.get() == EType.L ? electricsL : electricsM).render(tessellator);
				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.45F, 0.45F, 0.45F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}
}
