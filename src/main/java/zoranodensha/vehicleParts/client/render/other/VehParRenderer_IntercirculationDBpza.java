package zoranodensha.vehicleParts.client.render.other;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_DBpza;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.other.VehParIntercirculationDBpza;



@SideOnly(Side.CLIENT)
public class VehParRenderer_IntercirculationDBpza extends AVehParRendererBase_DBpza implements IVehiclePartRenderer
{
	private static final ObjectList intercirculation = model.makeGroup("Intercirculation");

	/** The part to render. */
	final VehParIntercirculationDBpza part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_IntercirculationDBpza(VehParIntercirculationDBpza part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParIntercirculationDBpza)
		{
			return new VehParRenderer_IntercirculationDBpza((VehParIntercirculationDBpza)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				tessellator.setColorOpaque_F(0.4F, 0.4F, 0.4F);
				intercirculation.render(tessellator);
				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		GL11.glTranslatef(0.5F, 0.5F, 0.5F);
		GL11.glScalef(0.45F, 0.45F, 0.45F);

		renderPart(1.0F);
		GL11.glPopMatrix();
	}
}
