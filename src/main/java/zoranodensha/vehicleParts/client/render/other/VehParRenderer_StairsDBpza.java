package zoranodensha.vehicleParts.client.render.other;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_DBpza;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.other.VehParStairsDBpza;



public class VehParRenderer_StairsDBpza extends AVehParRendererBase_DBpza implements IVehiclePartRenderer
{
	private static final ObjectList stairs = model.makeGroup("Stairs");
	private static final ObjectList baseplate = model.makeGroup("Stairs_ColorA");

	private final VehParStairsDBpza part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_StairsDBpza(VehParStairsDBpza part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParStairsDBpza)
		{
			return new VehParRenderer_StairsDBpza((VehParStairsDBpza)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return pass == 0;
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;

		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				part.color.apply(tessellator);
				stairs.render(tessellator);

				part.color_baseplate.apply(tessellator);
				baseplate.render(tessellator);

				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}

	@Override
	public void renderPartItem(ItemRenderType type)
	{
		GL11.glPushMatrix();

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(-0.5F, 0.0F, -0.5F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		GL11.glTranslatef(0.5F, 0.5F, 0.5F);
		GL11.glScalef(0.7F, 0.7F, 0.7F);
		GL11.glRotatef(90, 0, 1, 0);

		renderPart(1.0F);
		GL11.glPopMatrix();
	}

}
