package zoranodensha.vehicleParts.client.render.transport;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.init.Blocks;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.common.parts.transport.VehParInventory;



@SideOnly(Side.CLIENT)
public class VehParRenderer_Inventory implements IVehiclePartRenderer
{
	private final VehParInventory part;



	public VehParRenderer_Inventory(VehParInventory part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParInventory)
		{
			return new VehParRenderer_Inventory((VehParInventory)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		Train train = part.getTrain();
		if (train == null || !train.addedToChunk)
		{
			RenderBlocks.getInstance().renderBlockAsItem(Blocks.chest, 0, 1.0F);
		}
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		renderPart(1.0F);
		GL11.glPopMatrix();
	}
}