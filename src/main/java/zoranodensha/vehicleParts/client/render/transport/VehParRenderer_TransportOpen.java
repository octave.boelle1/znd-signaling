package zoranodensha.vehicleParts.client.render.transport;

import static zoranodensha.vehicleParts.client.render.RenderUtil.renderBlocks;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.AVehParRendererBase_ModFreight;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportOpen;



@SideOnly(Side.CLIENT)
public class VehParRenderer_TransportOpen extends AVehParRendererBase_ModFreight implements IVehiclePartRenderer
{
	private static final ObjectList extension = model.makeGroup("Extension_Open");

	/** The part to render. */
	final VehParTransportOpen part;
	private TesselatorVertexState vertexState;



	public VehParRenderer_TransportOpen(VehParTransportOpen part)
	{
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParTransportOpen)
		{
			return new VehParRenderer_TransportOpen((VehParTransportOpen)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		TextureManager renderEngine = Minecraft.getMinecraft().renderEngine;
		renderEngine.bindTexture(RenderUtil.texture_white);

		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				part.color.apply(tessellator);
				extension.render(tessellator);
				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();

		/* If gag part or render setting is turned off, return. */
		if (!part.getIsFinished() || ModCenter.cfg.rendering.detailVehicles <= 0)
		{
			return;
		}

		/* Also don't render if the part either has no vehicle, or the vehicle wasn't added to any chunk yet. */
		{
			Train parent = part.getTrain();
			if (parent == null || !parent.addedToChunk)
			{
				return;
			}
		}

		float fill = part.type_transportItem.getFillPercentage();
		if (fill <= 0.0F)
		{
			return;
		}

		ItemStack itemStack;
		Block block = null;
		int metadata = 0;

		for (int i = part.type_transportItem.getSizeInventory() - 1; i >= 0; --i)
		{
			itemStack = part.type_transportItem.getStackInSlot(i);
			if (itemStack != null && itemStack.getItem() instanceof ItemBlock)
			{
				block = Block.getBlockFromItem(itemStack.getItem());
				if (block.getRenderType() != -1)
				{
					/* Found the first Block in the inventory, use it to render freight. */
					metadata = itemStack.getItemDamage();
					break;
				}

				/* Whopsie, this one wasn't okay. Continue search. */
				block = null;
			}
		}

		if (block == null)
		{
			return;
		}


		/*
		 * Max. size values:
		 * 5.4 x
		 * 0.2 y
		 * 0.7 z
		 */
		float[] arr = part.getScale();
		float maxX = 5.4F * Math.abs(arr[0]);
		float maxZ = 0.7F * Math.abs(arr[2]);
		IIcon icon = renderBlocks.getBlockIconFromSideAndMetadata(block, 1, metadata);

		GL11.glPushMatrix();
		GL11.glScalef(1.0F / Math.abs(arr[0]), 1.0F / Math.abs(arr[1]), 1.0F / Math.abs(arr[2]));
		GL11.glTranslatef(0.0F, 0.2F * fill, 0.0F);
		{
			int j = block.getRenderColor(metadata);
			GL11.glColor4f((j >> 16 & 255) / 255.0F, (j >> 8 & 255) / 255.0F, (j & 255) / 255.0F, 1.0F);
			renderEngine.bindTexture(TextureMap.locationBlocksTexture);

			for (int i = 0; i < 4; ++i)
			{
				for (float posX = 0.0F; posX < maxX; posX += 1.0F)
				{
					for (float posZ = 0.0F; posZ < maxZ; posZ += 1.0F)
					{
						GL11.glPushMatrix();
						switch (i)
						{
							case 0:
								/* Render +X/+Z */
								GL11.glTranslatef(posX, 0.0F, posZ);
								drawFaceY(0.0F, Math.min((maxX - posX), 1.0F), 0.0F, Math.min((maxZ - posZ), 1.0F), icon);
								break;

							case 1:
								/* Render -X/+Z */
								GL11.glTranslatef(-posX - 1.0F, 0.0F, posZ);
								drawFaceY(1.0F - Math.min((maxX - posX), 1.0F), 1.0F, 0.0F, Math.min((maxZ - posZ), 1.0F), icon);
								break;

							case 2:
								/* Render +X/-Z */
								GL11.glTranslatef(posX, 0.0F, -posZ - 1.0F);
								drawFaceY(0.0F, Math.min((maxX - posX), 1.0F), 1.0F - Math.min((maxZ - posZ), 1.0F), 1.0F, icon);
								break;

							case 3:
								/* Render -X/-Z */
								GL11.glTranslatef(-posX - 1.0F, 0.0F, -posZ - 1.0F);
								drawFaceY(1.0F - Math.min((maxX - posX), 1.0F), 1.0F, 1.0F - Math.min((maxZ - posZ), 1.0F), 1.0F, icon);
								break;
						}
						GL11.glPopMatrix();
					}
				}
			}
		}
		GL11.glPopMatrix();
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		GL11.glScalef(0.4F, 0.4F, 0.4F);

		if (type == IItemRenderer.ItemRenderType.ENTITY)
		{
			GL11.glTranslatef(0.25F, 0.0F, 0.25F);
			GL11.glScalef(1.5F, 1.5F, 1.5F);
		}
		else
		{
			GL11.glTranslatef(0.5F, 0.5F, 0.5F);
			GL11.glScalef(0.45F, 0.45F, 0.45F);
		}

		renderPart(1.0F);
		GL11.glPopMatrix();
	}

	/**
	 * Draws (a part of) the top face of the given Block.
	 * <p>
	 * Most code copied from Minecraft's RenderBlocks.class, thus no credit is taken by Zora no Densha for this method.
	 */
	private static void drawFaceY(float minX, float maxX, float minZ, float maxZ, IIcon icon)
	{
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.setNormal(0.0F, 1.0F, 0.0F);

		if (renderBlocks.hasOverrideBlockTexture())
		{
			icon = renderBlocks.overrideBlockTexture;
		}

		double minU = icon.getInterpolatedU(minX * 16.0D);
		double maxU = icon.getInterpolatedU(maxX * 16.0D);
		double minV = icon.getInterpolatedV(minZ * 16.0D);
		double maxV = icon.getInterpolatedV(maxZ * 16.0D);

		if (minX < 0.0D || maxX > 1.0D)
		{
			minU = icon.getMinU();
			maxU = icon.getMaxU();
		}

		if (minZ < 0.0D || maxZ > 1.0D)
		{
			minV = icon.getMinV();
			maxV = icon.getMaxV();
		}

		double d7 = maxU;
		double d8 = minU;
		double d9 = minV;
		double d10 = maxV;

		switch (renderBlocks.uvRotateTop)
		{
			case 1:
				minU = icon.getInterpolatedU(minZ * 16.0D);
				minV = icon.getInterpolatedV(16.0D - (maxX * 16.0D));
				maxU = icon.getInterpolatedU(maxZ * 16.0D);
				maxV = icon.getInterpolatedV(16.0D - (minX * 16.0D));
				d9 = minV;
				d10 = maxV;
				d7 = minU;
				d8 = maxU;
				minV = maxV;
				maxV = d9;
				break;

			case 2:
				minU = icon.getInterpolatedU(16.0D - (maxZ * 16.0D));
				minV = icon.getInterpolatedV(minX * 16.0D);
				maxU = icon.getInterpolatedU(16.0D - (minZ * 16.0D));
				maxV = icon.getInterpolatedV(maxX * 16.0D);
				d7 = maxU;
				d8 = minU;
				minU = maxU;
				maxU = d8;
				d9 = maxV;
				d10 = minV;
				break;

			case 3:
				minU = icon.getInterpolatedU(16.0D - (minX * 16.0D));
				maxU = icon.getInterpolatedU(16.0D - (maxX * 16.0D));
				minV = icon.getInterpolatedV(16.0D - (minZ * 16.0D));
				maxV = icon.getInterpolatedV(16.0D - (maxZ * 16.0D));
				d7 = maxU;
				d8 = minU;
				d9 = minV;
				d10 = maxV;
				break;
		}

		double d11 = minX;
		double d12 = maxX;
		double d13 = 0.0D;// renderBlocks.renderMaxY;
		double d14 = minZ;
		double d15 = maxZ;

		if (renderBlocks.renderFromInside)
		{
			d11 = maxX;
			d12 = minX;
		}

		if (renderBlocks.enableAO)
		{
			tessellator.setColorOpaque_F(renderBlocks.colorRedTopLeft, renderBlocks.colorGreenTopLeft, renderBlocks.colorBlueTopLeft);
			tessellator.setBrightness(renderBlocks.brightnessTopLeft);
			tessellator.addVertexWithUV(d12, d13, d15, maxU, maxV);

			tessellator.setColorOpaque_F(renderBlocks.colorRedBottomLeft, renderBlocks.colorGreenBottomLeft, renderBlocks.colorBlueBottomLeft);
			tessellator.setBrightness(renderBlocks.brightnessBottomLeft);
			tessellator.addVertexWithUV(d12, d13, d14, d7, d9);

			tessellator.setColorOpaque_F(renderBlocks.colorRedBottomRight, renderBlocks.colorGreenBottomRight, renderBlocks.colorBlueBottomRight);
			tessellator.setBrightness(renderBlocks.brightnessBottomRight);
			tessellator.addVertexWithUV(d11, d13, d14, minU, minV);

			tessellator.setColorOpaque_F(renderBlocks.colorRedTopRight, renderBlocks.colorGreenTopRight, renderBlocks.colorBlueTopRight);
			tessellator.setBrightness(renderBlocks.brightnessTopRight);
			tessellator.addVertexWithUV(d11, d13, d15, d8, d10);
		}
		else
		{
			tessellator.addVertexWithUV(d12, d13, d15, maxU, maxV);
			tessellator.addVertexWithUV(d12, d13, d14, d7, d9);
			tessellator.addVertexWithUV(d11, d13, d14, minU, minV);
			tessellator.addVertexWithUV(d11, d13, d15, d8, d10);
		}

		tessellator.draw();
	}
}