package zoranodensha.vehicleParts.client.render;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.Tessellator;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;



@SideOnly(Side.CLIENT)
public abstract class AVehParRendererBase_FLIRT
{
	/** Vehicle model. */
	public static final ObjModelSpeedy model = new ObjModelSpeedy(ModData.ID, ModCenter.DIR_RESOURCES_VEHPAR + "FLIRT3");



	public static final void applyGrey()
	{
		GL11.glColor3f(0.15F, 0.15F, 0.15F);
	}

	public static final void applyGrey(Tessellator tessellator)
	{
		tessellator.setColorOpaque_F(0.15F, 0.15F, 0.15F);
	}

	public static final void applyGrey(Tessellator tessellator, float alpha)
	{
		tessellator.setColorRGBA_F(0.15F, 0.15F, 0.15F, alpha);
	}
}
