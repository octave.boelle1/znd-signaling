package zoranodensha.vehicleParts.client.render;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

import net.minecraft.util.ResourceLocation;
import zoranodensha.common.core.ModData;



public class GraphicsResources
{
	public static class MSMR
	{
		public static BufferedImage contrast;
		public static BufferedImage defaultBackground;
		public static BufferedImage defaultBackgroundDay;
		public static BufferedImage signalStrength;
		public static BufferedImage splash;
	}



	public static BufferedImage imageScreenGlare;
	public static BufferedImage imageScreenGlareSmall;
	public static BufferedImage imageScreenShade;
	public static BufferedImage imageZnD;
	public static BufferedImage imageError;

	public static BufferedImage[] imageMTCSIconsNight;
	public static BufferedImage[] imageMTCSIconsDay;
	public static HashMap<String, ResourceLocation> imageButtonIcons = new HashMap<String, ResourceLocation>();



	static
	{
		try
		{
			imageZnD = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/logo.png"));
			imageError = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/error.png"));
			imageScreenGlare = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/mtcs_screen_glare.png"));
			imageScreenGlareSmall = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/msmr_screen_glare.png"));
			imageScreenShade = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/mtcs_screen_shade.png"));

			BufferedImage mtcsIconsSpritesheet = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/mtcs_icons.png"));

			imageMTCSIconsNight = new BufferedImage[19];
			imageMTCSIconsDay = new BufferedImage[19];
			for (int i = 0; i < imageMTCSIconsNight.length; i++)
			{
				imageMTCSIconsNight[i] = mtcsIconsSpritesheet.getSubimage(i * 44, 0, 44, 44);
			}
			for (int i = 0; i < imageMTCSIconsDay.length; i++)
			{
				imageMTCSIconsDay[i] = mtcsIconsSpritesheet.getSubimage(i * 44, 44, 44, 44);
			}

			MSMR.contrast = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/msmr_contrast.png"));
			MSMR.defaultBackground = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/msmr_default.png"));
			MSMR.defaultBackgroundDay = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/msmr_default_day.png"));
			MSMR.signalStrength = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/msmr_signalStrength.png"));
			MSMR.splash = ImageIO.read(GraphicsResources.class.getResource("/assets/zoranodensha/textures/vehicleParts/cab/msmrSplash.png"));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}



	/**
	 * Tries to retrieve a resource location for the specified button name from the {@link #imageButtonIcons button icon map}.<br> Will attempt to load the image if it isn't present in the map.
	 *
	 * @return The matching icon.
	 */
	public static ResourceLocation getScreenButtonIcon(String buttonName)
	{
		ResourceLocation iconLoc = imageButtonIcons.get(buttonName);
		if (iconLoc == null)
		{
			iconLoc = new ResourceLocation(ModData.ID, "textures/vehicleParts/cab/" + buttonName + ".png");
			imageButtonIcons.put(buttonName, iconLoc);
		}
		return iconLoc;
	}
}