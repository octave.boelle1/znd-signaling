package zoranodensha.vehicleParts.client.render.coupler;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraftforge.client.IItemRenderer;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.Mat3x3;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.coupler.VehParCouplerJanney;



@SideOnly(Side.CLIENT)
public class VehParRenderer_CouplerJanney extends AVehParRendererBase_Coupler
{
	private static final ObjectList base, hook, pin;
	static
	{
		ObjModelSpeedy obj = new ObjModelSpeedy(ModData.ID, ModCenter.DIR_RESOURCES_VEHPAR + "ModelTrainPart_couplerJanney");
		base = obj.makeGroup("base");
		hook = obj.makeGroup("hook");
		pin = obj.makeGroup("pin");
	}

	/** The part to render. */
	final VehParCouplerJanney part;

	private TesselatorVertexState vertexState;
	private final Mat3x3 rotationMatrix = new Mat3x3().rotateY(-33);



	public VehParRenderer_CouplerJanney(VehParCouplerJanney part)
	{
		super(part);
		this.part = part;
	}


	@Override
	public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParCouplerJanney)
		{
			return new VehParRenderer_CouplerJanney((VehParCouplerJanney)copyPart);
		}
		return null;
	}

	@Override
	public boolean renderInPass(int pass)
	{
		return (pass == 0);
	}

	@Override
	public void renderPart(float partialTick)
	{
		if (part.updateVertexState)
		{
			vertexState = null;
			part.updateVertexState = false;
		}

		/* Apply texture and render base. */
		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				part.color.apply(tessellator);
				base.render(tessellator);

				if (part.type_coupling.getIsCoupled())
				{
					/* Render lowered pin. */
					tessellator.setTranslation(0.0D, -0.01D, 0.0D);
					pin.render(tessellator);

					/* Render hook. */
					tessellator.setTranslation(0.48D, 0.0D, 0.06D);
					hook.render(tessellator);
				}
				else
				{
					/* Render extended pin. */
					pin.render(tessellator);

					/* Render rotated hook at its position. */
					tessellator.setTranslation(0.48D, 0.0D, 0.06D);
					hook.render(tessellator, rotationMatrix);
				}

				tessellator.setTranslation(0, 0, 0);
				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}

	@Override
	public void renderPartItem(IItemRenderer.ItemRenderType type)
	{
		GL11.glPushMatrix();
		GL11.glScalef(3.0F, 3.0F, 3.0F);
		GL11.glTranslatef(-0.25F, 0.0F, 0.0F);
		renderPart(1.0F);
		GL11.glPopMatrix();
	}
}