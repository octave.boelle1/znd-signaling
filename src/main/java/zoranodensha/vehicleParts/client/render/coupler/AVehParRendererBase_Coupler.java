package zoranodensha.vehicleParts.client.render.coupler;

import org.lwjgl.opengl.GL11;

import net.minecraft.util.Vec3;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer.IVehiclePartSpecialRenderer;
import zoranodensha.api.vehicles.part.type.APartTypeCoupling;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.api.vehicles.rendering.TrainRenderer;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.vehicleParts.common.parts.coupler.AVehParCouplerBase;



/**
 * Base renderer class for couplers.<br>
 * Handles rotation alignment for coupler pairs of equal type.
 */
public abstract class AVehParRendererBase_Coupler implements IVehiclePartSpecialRenderer
{
	final AVehParCouplerBase coupler;
	protected MatrixData matrixData;



	public AVehParRendererBase_Coupler(AVehParCouplerBase coupler)
	{
		this.coupler = coupler;
	}


	/**
	 * Tries to calculate and apply a rotation based off an opposing coupler.
	 * 
	 * @return {@code true} if aforementioned rotation was successfully applied.
	 */
	protected boolean applyAlignedRotation()
	{
		if (matrixData != null)
		{
			/*
			 * Calculate rotation heading from this coupler to the other one.
			 * Also calculate offset along local X (post-rotation).
			 */
			float yaw = 0.0F;
			float pitch = 0.0F;
			double offset = 0.0D;
			{
				Vec3 pos0 = PartHelper.getPosition(coupler);
				Vec3 pos1 = PartHelper.getPosition(matrixData.couplerOther.getParent());

				double dX = pos1.xCoord - pos0.xCoord;
				double dY = pos1.yCoord - pos0.yCoord;
				double dZ = pos1.zCoord - pos0.zCoord;
				double d0 = (dX * dX) + (dZ * dZ);

				/* Calculate rotation values. */
				if (d0 > 0.001D)
				{
					yaw = (float)(-Math.atan2(dZ, dX) * VehicleHelper.DEG_FACTOR);

					if (Math.abs(dY) > 0.001D)
					{
						pitch = (float)(Math.atan(dY / Math.sqrt(d0)) * VehicleHelper.DEG_FACTOR);
					}
				}

				/* Calculate offset along local X. */
				offset = Math.sqrt(d0 + dY * dY);
				offset -= coupler.getLength() + matrixData.couplerOther.getLength();
				offset *= 0.5D;
			}

			GL11.glRotatef(yaw, 0.0F, 1.0F, 0.0F);
			GL11.glRotatef(pitch, 0.0F, 0.0F, 1.0F);
			GL11.glTranslated(offset, 0, 0);

			return true;
		}
		return false;
	}

	@Override
	public void renderPart_Post()
	{
		if (matrixData != null)
		{
			GL11.glRotatef(matrixData.yaw, 0.0F, -1.0F, 0.0F);
			GL11.glRotatef(matrixData.pitch, 0.0F, 0.0F, 1.0F);
			GL11.glRotatef(matrixData.roll, -1.0F, 0.0F, 0.0F);
			GL11.glTranslatef(-matrixData.section.centerX, -matrixData.section.centerY, -matrixData.section.centerZ);
			matrixData = null;
		}
	}

	@Override
	public boolean renderPart_Pre(float partialTick)
	{
		/* Get this coupling's section. */
		VehicleSection section = coupler.getSection();
		if (section == null)
		{
			return false;
		}

		/*
		 * Determine the other coupling that's connected to this coupling.
		 */
		APartTypeCoupling other;
		{
			Train train = section.getTrain();
			if (train == null)
			{
				return false;
			}

			VehicleData vehicle = section.getVehicle();
			int indexOfVehicle = train.getVehicleIndex(vehicle);

			/* If this coupler is the back coupling of its vehicle.. */
			if (vehicle.getCouplingBack() != null && vehicle.getCouplingBack().getParent() == coupler)
			{
				/* ..get the front coupler of the vehicle that's behind us, if possible. */
				VehicleData vehicleB = train.getVehicleAt(indexOfVehicle + 1);
				if (vehicleB == null)
				{
					return false;
				}
				other = vehicleB.getCouplingFront();
			}

			/* If it's the front coupler of its vehicle.. */
			else if (vehicle.getCouplingFront() != null && vehicle.getCouplingFront().getParent() == coupler)
			{
				/* ..get the back coupler of the vehicle that's in front of us, if possible. */
				VehicleData vehicleF = train.getVehicleAt(indexOfVehicle - 1);
				if (vehicleF == null)
				{
					return false;
				}
				other = vehicleF.getCouplingBack();
			}

			/* Abort otherwise. */
			else
			{
				return false;
			}
		}

		/* Ensure we actually have found the other coupling. */
		if (other == null)
		{
			return false;
		}

		/*
		 * Undo matrix translations.
		 */
		{
			matrixData = new MatrixData(section, other);
			GL11.glTranslatef(section.centerX, section.centerY, section.centerZ);
			GL11.glRotatef(matrixData.roll, 1.0F, 0.0F, 0.0F);
			GL11.glRotatef(matrixData.pitch, 0.0F, 0.0F, -1.0F);
			GL11.glRotatef(matrixData.yaw, 0.0F, 1.0F, 0.0F);
		}

		/*
		 * Prepare rendering.
		 */
		{
			/* Reset colour data. */
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

			/* Prepare offset. */
			float[] arr = coupler.getOffset();
			Vec3 offset = Vec3.createVectorHelper(arr[0] - section.centerX, arr[1] - section.centerY, arr[2] - section.centerZ);
			offset.rotateAroundZ(-matrixData.pitch * VehicleHelper.RAD_FACTOR);
			offset.rotateAroundY(-matrixData.yaw * VehicleHelper.RAD_FACTOR);
			offset.rotateAroundX(-matrixData.roll * VehicleHelper.RAD_FACTOR);
			GL11.glTranslated(offset.xCoord, offset.yCoord, offset.zCoord);

			/* Apply rotation. */
			if (!applyAlignedRotation())
			{
				arr = coupler.getRotation();
				GL11.glRotatef(arr[1], 0.0F, 1.0F, 0.0F);
				GL11.glRotatef(arr[2], 0.0F, 0.0F, 1.0F);
				GL11.glRotatef(arr[0], 1.0F, 0.0F, 0.0F);
			}

			/* Apply scale. */
			arr = coupler.getScale();
			GL11.glScalef(arr[0], arr[1], arr[2]);

			/* Check for face culling. */
			if (TrainRenderer.getInvertCullMode(arr[0], arr[1], arr[2]))
			{
				GL11.glCullFace(GL11.GL_FRONT);
			}
		}
		return true;
	}



	protected static class MatrixData
	{
		public final float roll;
		public final float yaw;
		public final float pitch;
		public final VehicleSection section;
		public final APartTypeCoupling couplerOther;



		public MatrixData(VehicleSection section, APartTypeCoupling couplerOther)
		{
			roll = TrainRenderer.interpolate(section.getRotationRoll(), section.getPrevRotationRoll());
			yaw = TrainRenderer.interpolate(section.getRotationYaw(), section.getPrevRotationYaw());
			pitch = TrainRenderer.interpolate(section.getRotationPitch(), section.getPrevRotationPitch());

			this.section = section;
			this.couplerOther = couplerOther;
		}
	}
}