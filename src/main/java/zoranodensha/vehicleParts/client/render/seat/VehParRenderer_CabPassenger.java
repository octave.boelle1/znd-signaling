package zoranodensha.vehicleParts.client.render.seat;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.shader.TesselatorVertexState;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.cab.AScreenRenderer;
import zoranodensha.api.vehicles.part.type.cab.PropButton;
import zoranodensha.api.vehicles.part.type.cab.PropReverser;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.api.vehicles.rendering.VertexStateCreator_Tris;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.GraphicsResources;
import zoranodensha.vehicleParts.client.render.RenderUtil;
import zoranodensha.vehicleParts.common.parts.seat.VehParCabPassenger;

import java.util.HashMap;
import java.util.Map;



@SideOnly(Side.CLIENT) public class VehParRenderer_CabPassenger extends AVehParRenderer_CabBase
{
	private static final ObjModelSpeedy model_cab = new ObjModelSpeedy(ModData.ID, ModCenter.DIR_RESOURCES_VEHPAR + "ModelTrainPart_cab");

	/*
	 * Texture Resource Locations
	 */
	private static final ResourceLocation textureArrow = new ResourceLocation(ModData.ID, "textures/vehicleParts/cab/arrow.png");
	private static final ResourceLocation textureNeutral = new ResourceLocation(ModData.ID, "textures/vehicleParts/cab/neutral.png");

	/*
	 * Object groups.
	 */
	private static final ObjModelSpeedy.ObjectList desk = model_cab.makeGroup("desk");
	private static final ObjModelSpeedy.ObjectList deskDark = model_cab.makeGroup("deskTrimDark", "phone");
	private static final ObjModelSpeedy.ObjectList deskMedium = model_cab.makeGroup("deskTrimMedium");
	private static final ObjModelSpeedy.ObjectList deskBlack = model_cab.makeGroup("deskTrimBlack");

	private static final ObjModelSpeedy.ObjectList button0 = model_cab.makeGroup("button0");
	private static final ObjModelSpeedy.ObjectList button1 = model_cab.makeGroup("button1");
	private static final ObjModelSpeedy.ObjectList button2 = model_cab.makeGroup("button2");
	private static final ObjModelSpeedy.ObjectList button3 = model_cab.makeGroup("button3");

	private static final ObjModelSpeedy.ObjectList radioButtons = model_cab.makeGroup("radioButtons");

	private static final ObjModelSpeedy.ObjectList button2Face = model_cab.makeGroup("button2Face");
	private static final ObjModelSpeedy.ObjectList button3Face = model_cab.makeGroup("button3Face");

	private static final ObjModelSpeedy.ObjectList gauge0 = model_cab.makeGroup("gauge0");
	private static final ObjModelSpeedy.ObjectList gauge1 = model_cab.makeGroup("gauge1");

	private static final ObjModelSpeedy.ObjectList indicator0 = model_cab.makeGroup("indicator0");
	private static final ObjModelSpeedy.ObjectList indicator1 = model_cab.makeGroup("indicator1");
	private static final ObjModelSpeedy.ObjectList indicator2 = model_cab.makeGroup("indicator2");

	private static final ObjModelSpeedy.ObjectList reverserD = model_cab.makeGroup("reverserD");
	private static final ObjModelSpeedy.ObjectList reverserN = model_cab.makeGroup("reverserN");
	private static final ObjModelSpeedy.ObjectList reverserR = model_cab.makeGroup("reverserR");

	private static final ObjModelSpeedy.ObjectList reverserDFace = model_cab.makeGroup("reverserDFace");
	private static final ObjModelSpeedy.ObjectList reverserNFace = model_cab.makeGroup("reverserNFace");
	private static final ObjModelSpeedy.ObjectList reverserRFace = model_cab.makeGroup("reverserRFace");

	private static final ObjModelSpeedy.ObjectList screen0 = model_cab.makeGroup("screen0");
	private static final ObjModelSpeedy.ObjectList screen1 = model_cab.makeGroup("screen1");
	private static final ObjModelSpeedy.ObjectList screen2 = model_cab.makeGroup("screen2");

	private static final ObjModelSpeedy.ObjectList[] screenButtons = new ObjModelSpeedy.ObjectList[22];
	private static final ObjModelSpeedy.ObjectList[] screenButtonOverlays = new ObjModelSpeedy.ObjectList[22];


	static
	{
		for (int i = 0; i < screenButtons.length; i++)
		{
			screenButtons[i] = model_cab.makeGroup("screenButton" + i);
		}

		for (int i = 0; i < screenButtonOverlays.length; i++)
		{
			screenButtonOverlays[i] = model_cab.makeGroup("screenButtonOverlay" + i);
		}
	}


	/*
	 * Lever controls.
	 */
	private static final ControlLever lever1 = new ControlLever(-0.48000F, 0.07600F, 0.1525F, 0, 0, -1, 0.23F, 0.23F, 0.23F, 0.0F, model_cab, "lever4");
	private static final ControlLever lever2 = new ControlLever(-0.48000F, 0.07600F, -0.1525F, 0, 0, -1, 0.23F, 0.23F, 0.23F, 0.0F, model_cab, "lever5");
	private static final ControlLever switch0 = new ControlLever(-0.47238F, 0.0848F, 0.39828F, 0, 0, -1, 0.3F, 0.3F, 0.3F, 28.5F, model_cab, "switch0");
	private static final ControlLever switch1 = new ControlLever(-0.48901F, 0.0848F, 0.36749F, 0, 0, -1, 0.3F, 0.3F, 0.3F, 28.5F, model_cab, "switch1");
	private static final ControlLever switch2 = new ControlLever(-0.54097F, 0.0848F, 0.27126F, 0, 0, -1, 0.3F, 0.3F, 0.3F, 28.5F, model_cab, "switch2");
	private static final ControlLever switch3 = new ControlLever(-0.5576F, 0.0848F, 0.24046F, 0, 0, -1, 0.3F, 0.3F, 0.3F, 28.5F, model_cab, "switch3");
	private static final ControlLever switch4 = new ControlLever(-0.57423F, 0.0848F, 0.20967F, 0, 0, -1, 0.3F, 0.3F, 0.3F, 28.5F, model_cab, "switch4");
	private static final ControlLever switch5 = new ControlLever(-0.59324F, 0.0848F, -0.17447F, 0, 0, -1, 0.3F, 0.3F, 0.3F, -28.5F, model_cab, "switch5");
	private static final ControlLever switch8 = new ControlLever(-0.3665F, 0.1008F, -0.4374F, 0, 0, -1, 0.9F, 0.9F, 0.9F, 0.0F, model_cab, "switch8");

	/**
	 * The cab to render.
	 */
	private final VehParCabPassenger cab;

	/**
	 * HashMap to keep track of which buttons we render at which indices.
	 */
	private final HashMap<PropButton.PropButtonScreen, Integer> buttonsDDUL = new HashMap<PropButton.PropButtonScreen, Integer>();
	private final HashMap<PropButton.PropButtonScreen, Integer> buttonsDDUR = new HashMap<PropButton.PropButtonScreen, Integer>();

	private TesselatorVertexState vertexState;
	private TesselatorVertexState vertexStateButtons;
	private TesselatorVertexState vertexStateDoorButtons;
	private TesselatorVertexState vertexStateGauge0;
	private TesselatorVertexState vertexStateGauge1;
	private TesselatorVertexState vertexStateIndicators;
	private TesselatorVertexState vertexStateLevers;
	private TesselatorVertexState vertexStateRadioButtons;
	private TesselatorVertexState vertexStateReverserOverlayA;
	private TesselatorVertexState vertexStateReverserOverlayN;
	private TesselatorVertexState vertexStateScreen0;
	private TesselatorVertexState vertexStateScreen1;
	private TesselatorVertexState vertexStateScreen2;
	private TesselatorVertexState vertexStateScreenButtons;


	public VehParRenderer_CabPassenger(VehParCabPassenger cab)
	{
		super(cab);

		this.cab = cab;
		this.cab.initControls();
		cab.dduL.setScreenRenderer(new RendererDDU(cab.dduL));
		cab.dduR.setScreenRenderer(new RendererDDU(cab.dduR));

		buttonsDDUL.put(cab.dduL.screenButton_brightness, 0);
		buttonsDDUL.put(cab.dduL.screenButton_contrast, 1);
		buttonsDDUL.put(cab.dduL.screenButton_swap, 4);
		buttonsDDUL.put(cab.dduL.screenButton_menu, 5);
		buttonsDDUL.put(cab.dduL.screenButton_up, 8);
		buttonsDDUL.put(cab.dduL.screenButton_left, 6);
		buttonsDDUL.put(cab.dduL.screenButton_right, 7);
		buttonsDDUL.put(cab.dduL.screenButton_down, 9);
		buttonsDDUL.put(cab.dduL.screenButton_enter, 10);

		buttonsDDUR.put(cab.dduR.screenButton_brightness, 0);
		buttonsDDUR.put(cab.dduR.screenButton_contrast, 1);
		buttonsDDUR.put(cab.dduR.screenButton_swap, 4);
		buttonsDDUR.put(cab.dduR.screenButton_menu, 5);
		buttonsDDUR.put(cab.dduR.screenButton_up, 8);
		buttonsDDUR.put(cab.dduR.screenButton_left, 6);
		buttonsDDUR.put(cab.dduR.screenButton_right, 7);
		buttonsDDUR.put(cab.dduR.screenButton_down, 9);
		buttonsDDUR.put(cab.dduR.screenButton_enter, 10);
	}

	@Override public IVehiclePartRenderer copy(VehParBase copyPart)
	{
		if (copyPart instanceof VehParCabPassenger)
		{
			return new VehParRenderer_CabPassenger((VehParCabPassenger)copyPart);
		}

		return null;
	}


	@Override public void renderPart(float partialTick)
	{
		/* Rotate the whole part by 180° about Y in order to align with the default view vector. */
		GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);

		final boolean playerSitting = cab.getEntityIsPlayer();

		/*
		 * Reset vertex states if required.
		 */
		if (part.updateVertexState)
		{
			vertexState = null;
			vertexStateButtons = null;
			vertexStateDoorButtons = null;
			vertexStateGauge0 = null;
			vertexStateGauge1 = null;
			vertexStateIndicators = null;
			vertexStateLevers = null;
			vertexStateRadioButtons = null;
			vertexStateReverserOverlayA = null;
			vertexStateReverserOverlayN = null;
			vertexStateScreen0 = null;
			vertexStateScreen1 = null;
			vertexStateScreen2 = null;
			vertexStateScreenButtons = null;
			/* Special case: Do NOT reset update vertex state flag here. Super class handles that before rendering seat. */
		}

		RenderUtil.lightmapPush();
		{
			/* Render cab. */
			GL11.glPushMatrix();
			GL11.glTranslatef(-0.075F, 0.0F, 0.0F);
			{
				GL11.glScalef(0.8F, 0.8F, 0.8F);
				Tessellator tessellator = Tessellator.instance;

				/* Enable bright lighting if the cab lights are on. */
				RenderUtil.lightmapBright(cab);

				/* Tessellate all model pieces that ignore lighting first. */
				if (playerSitting)
				{
					GL11.glPushAttrib(GL11.GL_LIGHTING);
					GL11.glDisable(GL11.GL_LIGHTING);
					{

						renderPiece_Screens(tessellator);
						renderPiece_ScreenButtons(tessellator);
						renderPiece_ReverserOverlays(tessellator);

						Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
						renderPiece_Indicators(tessellator);
						renderPiece_DoorButtonTops(tessellator);

					}
					GL11.glPopAttrib();
				}

				/* Tessellate all others. */
				renderPiece_Table(tessellator, playerSitting);

				if (playerSitting)
				{
					renderPiece_Levers(tessellator, partialTick);
					renderPiece_Buttons(tessellator);
					renderPiece_Gauges(tessellator);
				}
			}
			GL11.glPopMatrix();

			/* Finally render seat and passenger using current lighting settings. */
			super.renderPart(partialTick);
		}
		RenderUtil.lightmapPop();
	}


	/**
	 * Renders vertex states for buttons which don't light up.
	 *
	 * @param tessellator - What does this parameter do? I don't know, something to do with rendering literally everything.
	 */
	private void renderPiece_Buttons(Tessellator tessellator)
	{
		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);

		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexStateButtons == null)
			{
				tessellator.setColorOpaque_F(1.0F, 0.0F, 0.0F);
				renderButton(tessellator, button0, cab.type_cab.button_parkBrake_apply.isActive());

				tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
				renderButton(tessellator, button1, cab.type_cab.button_parkBrake_release.isActive());

				vertexStateButtons = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexStateButtons);
			}
		}
		tessellator.draw();

		tessellator.setTranslation(0, 0, 0);
	}


	/**
	 * Renders vertex states for the tops of the door buttons (which light up)
	 */
	private void renderPiece_DoorButtonTops(Tessellator tessellator)
	{
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexStateDoorButtons == null)
			{
				tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
				renderLightButton(tessellator, button2Face, cab.type_cab.button_doors_left.isActive());
				renderLightButton(tessellator, button3Face, cab.type_cab.button_doors_right.isActive());

				vertexStateDoorButtons = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexStateDoorButtons);
			}
		}
		tessellator.draw();

		/* Reset Translation */
		tessellator.setTranslation(0, 0, 0);
	}


	/**
	 * Renders vertex states for pressure gauges with respective textures.
	 */
	private void renderPiece_Gauges(Tessellator tessellator)
	{
		RenderUtil.lightmapPush();
		{
			RenderUtil.lightmapBright();

			/* Brake pipe pressure gauge. */
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, cab.gaugeBottom.getTextureID());
			tessellator.startDrawing(GL11.GL_TRIANGLES);
			{
				if (vertexStateGauge0 == null)
				{
					tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
					gauge0.render(tessellator);
					vertexStateGauge0 = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexStateGauge0);
				}
			}
			tessellator.draw();

			/* Cylinder pressure gauge. */
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, cab.gaugeTop.getTextureID());
			tessellator.startDrawing(GL11.GL_TRIANGLES);
			{
				if (vertexStateGauge1 == null)
				{
					tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
					gauge1.render(tessellator);
					vertexStateGauge1 = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexStateGauge1);
				}
			}
			tessellator.draw();
		}
		RenderUtil.lightmapPop();
	}


	/**
	 * Renders the indicator vertex state.
	 */
	private void renderPiece_Indicators(Tessellator tessellator)
	{
		RenderUtil.lightmapPush();
		{
			RenderUtil.lightmapBright();

			tessellator.startDrawing(GL11.GL_TRIANGLES);
			{
				if (vertexStateIndicators == null)
				{
					float colour;

					colour = cab.indicatorLightStaffResponsible ? 0.85F : 0.1F;
					tessellator.setColorOpaque_F(0.1F, 0.1F, colour);
					indicator0.render(tessellator);

					colour = cab.indicatorLightParkBrake ? 0.85F : 0.1F;
					tessellator.setColorOpaque_F(colour, colour, colour);
					indicator1.render(tessellator);

					tessellator.setColorOpaque_F(0.7F, 0.1F, 0.1F);
					indicator2.render(tessellator);

					vertexStateIndicators = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexStateIndicators);
				}
			}
			tessellator.draw();
		}
		RenderUtil.lightmapPop();
	}


	/**
	 * Renders the lever vertex state.
	 */
	private void renderPiece_Levers(Tessellator tessellator, float partialTick)
	{
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			// @formatter:off
			/* If either control is moving, render all controls dynamically. */
			if (cab.powerBrakeControl.getIsMoving()
					|| cab.airBrakeControl.getIsMoving()
					|| cab.hornControl.getIsMoving()
					|| cab.tcsControl.getIsMoving()
					|| cab.doorControl.getIsMoving()
					|| cab.lightBeamControl.getIsMoving()
					|| cab.cabLightControl.getIsMoving()
					|| cab.trainLightControl.getIsMoving())
			// @formatter:on
			{
				// knob0.render(tessellator, cab.headlightControl.getState(partialTick));
				lever1.render(tessellator, cab.powerBrakeControl.getState(partialTick));
				lever2.render(tessellator, cab.airBrakeControl.getState(partialTick));
				switch0.render(tessellator, cab.hornControl.getState(partialTick));
				switch1.render(tessellator, cab.tcsControl.getState(partialTick));
				switch2.render(tessellator, cab.lightBeamControl.getState(partialTick));
				switch3.render(tessellator, cab.cabLightControl.getState(partialTick));
				switch4.render(tessellator, cab.trainLightControl.getState(partialTick));
				//				switch5.render(tessellator, cab.brakeModeControl.getState(partialTick));
				switch8.render(tessellator, cab.doorControl.getState(partialTick));

				vertexStateLevers = null;
			}
			else
			{
				/* Otherwise, render controls statically. */
				if (vertexStateLevers == null)
				{
					// knob0.render(tessellator, cab.headlightControl.getState(1));
					lever1.render(tessellator, cab.powerBrakeControl.getState(1));
					lever2.render(tessellator, cab.airBrakeControl.getState(1));
					switch0.render(tessellator, cab.hornControl.getState(1));
					switch1.render(tessellator, cab.tcsControl.getState(1));
					switch2.render(tessellator, cab.lightBeamControl.getState(1));
					switch3.render(tessellator, cab.cabLightControl.getState(1));
					switch4.render(tessellator, cab.trainLightControl.getState(1));
					//					switch5.render(tessellator, cab.brakeModeControl.getState(1));
					switch8.render(tessellator, cab.doorControl.getState(1));

					vertexStateLevers = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexStateLevers);
				}
			}
		}
		tessellator.draw();
		tessellator.setTranslation(0, 0, 0);
	}


	/**
	 * Renders the reverser overlays vertex states.
	 */
	private void renderPiece_ReverserOverlays(Tessellator tessellator)
	{
		/* Render arrow buttons. */
		Minecraft.getMinecraft().renderEngine.bindTexture(textureArrow);
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexStateReverserOverlayA == null)
			{
				renderLightButton(tessellator, reverserDFace, cab.type_cab.getReverserState() == PropReverser.EReverser.FORWARD);
				renderLightButton(tessellator, reverserRFace, cab.type_cab.getReverserState() == PropReverser.EReverser.BACKWARD);
				vertexStateReverserOverlayA = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexStateReverserOverlayA);
			}
		}
		tessellator.draw();

		/* Render neutral button. */
		Minecraft.getMinecraft().renderEngine.bindTexture(textureNeutral);
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexStateReverserOverlayN == null)
			{
				renderLightButton(tessellator, reverserNFace, cab.type_cab.getReverserState().isNeutral());
				vertexStateReverserOverlayN = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexStateReverserOverlayN);
			}
		}
		tessellator.draw();

		/* Undo possible tessellator translations. */
		tessellator.setTranslation(0, 0, 0);
	}


	/**
	 * Renders the screen button vertex states.
	 */
	private void renderPiece_ScreenButtons(Tessellator tessellator)
	{
		/*
		 * Button bases
		 */
		GL11.glPushMatrix();
		{
			Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);

			tessellator.startDrawing(GL11.GL_TRIANGLES);
			{
				if (vertexStateScreenButtons == null)
				{
					tessellator.setColorOpaque_F(0.3F, 0.3F, 0.3F);
					for (int i = 0; i < screenButtons.length; i++)
					{
						screenButtons[i].render(tessellator);
					}

					vertexStateScreenButtons = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexStateScreenButtons);
				}
			}
			tessellator.draw();
		}
		GL11.glPopMatrix();

		/*
		 * MSM-R Radio Buttons
		 */
		GL11.glPushMatrix();
		{
			Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);

			tessellator.startDrawing(GL11.GL_TRIANGLES);
			{
				if (vertexStateRadioButtons == null)
				{
					tessellator.setColorOpaque_F(0.3F, 0.3F, 0.3F);
					radioButtons.render(tessellator);

					vertexStateRadioButtons = VertexStateCreator_Tris.getVertexState();
				}
				else
				{
					tessellator.setVertexState(vertexStateRadioButtons);
				}
			}
			tessellator.draw();
		}
		GL11.glPopMatrix();

		/*
		 * DDUL overlays
		 */
		GL11.glPushMatrix();
		{
			GL11.glPushAttrib(GL11.GL_LIGHTING);
			GL11.glDisable(GL11.GL_LIGHTING);
			{
				RenderUtil.lightmapPush();
				RenderUtil.lightmapBright();
				{
					GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

					for (Map.Entry<PropButton.PropButtonScreen, Integer> e : buttonsDDUL.entrySet())
					{
						PropButton.PropButtonScreen button = e.getKey();
						if (button.isInputAllowed())
						{
							Minecraft.getMinecraft().renderEngine.bindTexture(GraphicsResources.getScreenButtonIcon(button.getButtonTextureName()));
							screenButtonOverlays[e.getValue()].render();
						}
					}
				}
				RenderUtil.lightmapPop();
			}
			GL11.glPopAttrib();
		}
		GL11.glPopMatrix();

		/*
		 * DDUR overlays
		 */
		GL11.glPushMatrix();
		{
			GL11.glPushAttrib(GL11.GL_LIGHTING);
			GL11.glDisable(GL11.GL_LIGHTING);
			{
				RenderUtil.lightmapPush();
				RenderUtil.lightmapBright();
				{
					GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

					for (Map.Entry<PropButton.PropButtonScreen, Integer> e : buttonsDDUR.entrySet())
					{
						PropButton.PropButtonScreen button = e.getKey();
						if (button.isInputAllowed())
						{
							Minecraft.getMinecraft().renderEngine.bindTexture(GraphicsResources.getScreenButtonIcon(button.getButtonTextureName()));
							screenButtonOverlays[11 + e.getValue()].render();
						}
					}
				}
				RenderUtil.lightmapPop();
			}
			GL11.glPopAttrib();
		}
		GL11.glPopMatrix();
	}


	/**
	 * Renders the screen vertex states where applicable.
	 */
	private void renderPiece_Screens(Tessellator tessellator)
	{
		GL11.glPushMatrix();
		{
			RenderUtil.lightmapPush();
			RenderUtil.lightmapBright();
			{
				/* Render Left DDU. */
				AScreenRenderer rendererDDUL = cab.dduL.getScreenRenderer();
				{
					if (rendererDDUL.getTexture().getTextureID() == -1)
					{
						rendererDDUL.onRepaint();
					}

					GL11.glBindTexture(GL11.GL_TEXTURE_2D, rendererDDUL.getTexture().getTextureID());
					tessellator.startDrawing(GL11.GL_TRIANGLES);
					{
						if (vertexStateScreen0 == null)
						{
							tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
							screen0.render(tessellator);
							vertexStateScreen0 = VertexStateCreator_Tris.getVertexState();
						}
						else
						{
							tessellator.setVertexState(vertexStateScreen0);
						}
					}
					tessellator.draw();
				}

				/* Render Right DDU. */
				AScreenRenderer rendererDDUR = cab.dduR.getScreenRenderer();
				{
					if (rendererDDUR.getTexture().getTextureID() == -1)
					{
						rendererDDUR.onRepaint();
					}

					GL11.glBindTexture(GL11.GL_TEXTURE_2D, rendererDDUR.getTexture().getTextureID());
					tessellator.startDrawing(GL11.GL_TRIANGLES);
					{
						if (vertexStateScreen1 == null)
						{
							tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
							screen1.render(tessellator);
							vertexStateScreen1 = VertexStateCreator_Tris.getVertexState();
						}
						else
						{
							tessellator.setVertexState(vertexStateScreen1);
						}
					}
					tessellator.draw();
				}

				/* Render MSMR screen. */
				{
					tessellator.startDrawing(GL11.GL_TRIANGLES);
					{
						if (vertexStateScreen2 == null)
						{
							tessellator.setColorOpaque_F(0.0F, 0.0F, 0.0F);
							Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
							screen2.render(tessellator);
							vertexStateScreen2 = VertexStateCreator_Tris.getVertexState();
						}
						else
						{
							tessellator.setVertexState(vertexStateScreen2);
						}
					}
					tessellator.draw();
				}
			}
			RenderUtil.lightmapPop();
		}
		GL11.glPopMatrix();
	}


	/**
	 * Renders all other table parts that haven't been rendered by any other vertex state.
	 */
	private void renderPiece_Table(Tessellator tessellator, boolean renderScreenInfo)
	{
		tessellator.startDrawing(GL11.GL_TRIANGLES);
		{
			if (vertexState == null)
			{
				/* Render screens where required. */
				tessellator.setColorOpaque_F(0.0F, 0.0F, 0.0F);
				{
					if (!renderScreenInfo)
					{
						screen0.render(tessellator);
						screen1.render(tessellator);
						screen2.render(tessellator);
					}
				}

				/* Render table base. */
				{
					cab.color_desk.apply(tessellator);
					desk.render(tessellator);

					if (renderScreenInfo)
					{
						ColorRGBA.multiplierPush(0.12F, 0.12F, 0.12F, 1.0F);
						cab.color_desk.apply(tessellator);
						deskDark.render(tessellator);
						ColorRGBA.multiplierPop();

						ColorRGBA.multiplierPush(0.24F, 0.24F, 0.24F, 1.0F);
						cab.color_desk.apply(tessellator);
						deskMedium.render(tessellator);
						ColorRGBA.multiplierPop();

						tessellator.setColorOpaque_F(0.0F, 0.0F, 0.0F);
						deskBlack.render(tessellator);
					}
				}

				/* Reverser button bases. */
				if (renderScreenInfo)
				{
					tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
					{
						renderButton(tessellator, reverserD, cab.type_cab.getReverserState() == PropReverser.EReverser.FORWARD);
						renderButton(tessellator, reverserR, cab.type_cab.getReverserState() == PropReverser.EReverser.BACKWARD);
						renderButton(tessellator, reverserN, cab.type_cab.getReverserState().isNeutral());
						renderButton(tessellator, button2, cab.type_cab.button_doors_left.isActive());
						renderButton(tessellator, button3, cab.type_cab.button_doors_right.isActive());
					}
					tessellator.setTranslation(0, 0, 0);
				}

				vertexState = VertexStateCreator_Tris.getVertexState();
			}
			else
			{
				tessellator.setVertexState(vertexState);
			}
		}
		tessellator.draw();
	}


	/**
	 * Helper method to render buttons.
	 */
	private void renderButton(Tessellator tessellator, ObjModelSpeedy.ObjectList obj, boolean isDown)
	{
		tessellator.setTranslation(0.0D, (isDown ? -0.002D : 0.0D), 0.0D);
		obj.render(tessellator);
	}

	/**
	 * Helper method to render buttons.<br> If {@code isDown} is {@code true}, the button will light up.
	 */
	private void renderLightButton(Tessellator tessellator, ObjModelSpeedy.ObjectList obj, boolean isDown)
	{
		if (isDown)
			tessellator.setColorOpaque_F(1.0F, 1.0F, 1.0F);
		else
			tessellator.setColorOpaque_F(0.5F, 0.5F, 0.5F);

		tessellator.setTranslation(0.0D, (isDown ? -0.002D : 0.0D), 0.0D);
		obj.render(tessellator);
	}
}
