package zoranodensha.vehicleParts.client.render.seat;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.Tessellator;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.Mat3x3;
import zoranodensha.api.vehicles.rendering.ObjModelSpeedy.ObjectList;
import zoranodensha.vehicleParts.common.parts.seat.VehParSeat;



@SideOnly(Side.CLIENT)
public abstract class AVehParRenderer_CabBase extends VehParRenderer_Seat
{

	public AVehParRenderer_CabBase(VehParSeat part)
	{
		super(part);
	}


	/**
	 * Helper class that holds render information for a cab's levers.
	 */
	@SideOnly(Side.CLIENT)
	protected static class ControlLever
	{
		/** Local position of this control. */
		private final float x, y, z;
		/** Rotation axis levels. Determines which axis the rotation is applied about. */
		private final int rX, rY, rZ;
		/** Color components of this lever. */
		private final float colR, colG, colB;
		/** Twist of this lever. */
		private final float twist;
		/** The object(s) to render. */
		private final ObjectList obj;

		/** Static rotation matrix, reused for faster rendering. */
		public static final Mat3x3 matrix = new Mat3x3();


		public ControlLever(float x, float y, float z, int rX, int rY, int rZ, float colR, float colG, float colB, float twist, ObjModelSpeedy model, String... names)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.rX = rX;
			this.rY = rY;
			this.rZ = rZ;
			this.colR = colR;
			this.colG = colG;
			this.colB = colB;
			this.twist = twist;
			obj = model.makeGroup(names);
		}

		public float[] getPosition()
		{
			return new float[] { x, y, z };
		}

		public void render(Tessellator tessellator, float rotation)
		{
			tessellator.setColorOpaque_F(colR, colG, colB);

			transform(tessellator, rotation);

			obj.render(tessellator, matrix);
		}

		public void transform(Tessellator tessellator, float rotation)
		{
			// XXX tessellator.setTranslation(x - 0.40D, y + 0.05D, z);
			tessellator.setTranslation(x, y, z);
			matrix.reset();

			if (rZ != 0)
				matrix.rotateZ(rotation * rZ);

			if (rY != 0)
				matrix.rotateY(rotation * rY);

			if (rX != 0)
				matrix.rotateX(rotation * rX);

			if (twist != 0.0F)
				matrix.rotateY(twist);
		}
	}
}
