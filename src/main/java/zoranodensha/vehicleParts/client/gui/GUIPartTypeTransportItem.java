package zoranodensha.vehicleParts.client.gui;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import zoranodensha.api.vehicles.part.type.PartTypeLoadable;
import zoranodensha.api.vehicles.part.type.PartTypeTransportItem;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.gui.button.GuiButtonLoadState;
import zoranodensha.vehicleParts.common.container.ContainerPartTypeTransportItem;



/**
 * The class that renders the GUI of VehParTransportItem vehicle parts.
 * <p>
 * Most code is copied from Minecraft's class files;<br>
 * no credit is taken by Zora no Densha, all rights belong to the rightful owners of copied code.
 */
@SideOnly(Side.CLIENT)
public class GUIPartTypeTransportItem extends GuiContainer
{
	private static final ResourceLocation texture = new ResourceLocation("textures/gui/container/generic_54.png");

	private GuiButtonLoadState loadStateButton;
	private PartTypeTransportItem partTypeItem;
	private PartTypeLoadable partTypeLoadable;
	private InventoryPlayer playerInv;

	/** Number of rows in this inventory. Higher numbers create taller windows. */
	private int inventoryRows;



	public GUIPartTypeTransportItem(InventoryPlayer inventory, PartTypeTransportItem partType)
	{
		super(new ContainerPartTypeTransportItem(inventory, partType));

		partTypeItem = partType;
		partTypeLoadable = partType.getParent().getPartType(PartTypeLoadable.class);
		playerInv = inventory;
		allowUserInput = false;
		inventoryRows = MathHelper.ceiling_float_int(partType.getSizeInventory() / 9.0F);
		ySize = 114 + (inventoryRows * 18);
	}

	@Override
	protected void actionPerformed(GuiButton button)
	{
		if (partTypeLoadable != null)
		{
			switch (button.id)
			{
				case 0:
					partTypeLoadable.nextLoadState();
					initGui();
					break;
			}
		}
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int mouseX, int mouseY)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(texture);
		int k = (width - xSize) / 2;
		int l = (height - ySize) / 2;
		drawTexturedModalRect(k, l, 0, 0, xSize, (inventoryRows * 18) + 17);
		drawTexturedModalRect(k, l + (inventoryRows * 18) + 17, 0, 126, xSize, 96);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		fontRendererObj.drawString(partTypeItem.hasCustomInventoryName() ? partTypeItem.getInventoryName() : I18n.format(partTypeItem.getInventoryName(), new Object[0]), 8, 6, 4210752);
		fontRendererObj.drawString(playerInv.hasCustomInventoryName() ? playerInv.getInventoryName() : I18n.format(playerInv.getInventoryName(), new Object[0]), 8, ySize - 96 + 2, 4210752);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float f)
	{
		super.drawScreen(mouseX, mouseY, f);

		if (loadStateButton != null && partTypeLoadable != null && loadStateButton.getHoverState(mouseX, mouseY) == 2)
		{
			List<String> listInfo = new ArrayList<String>();
			listInfo.add(I18n.format(ModData.ID + ".text.guiVehParTransport." + partTypeLoadable.getLoadState().toString().toLowerCase()));
			drawHoveringText(listInfo, mouseX, mouseY, fontRendererObj);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void initGui()
	{
		super.initGui();

		if (partTypeLoadable != null)
		{
			loadStateButton = new GuiButtonLoadState(0, ((width - xSize) / 2) + xSize - 16, ((height - ySize) / 2) + 6, partTypeLoadable.getLoadState().ordinal());
			buttonList.clear();
			buttonList.add(loadStateButton);
		}
	}
}
