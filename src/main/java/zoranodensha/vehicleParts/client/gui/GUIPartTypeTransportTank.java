package zoranodensha.vehicleParts.client.gui;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidTank;
import zoranodensha.api.vehicles.part.type.PartTypeLoadable;
import zoranodensha.api.vehicles.part.type.PartTypeTransportTank;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.gui.button.GuiButtonLoadState;
import zoranodensha.vehicleParts.common.container.ContainerPartTypeTransportTank;



@SideOnly(Side.CLIENT)
public class GUIPartTypeTransportTank extends GuiContainer
{
	public static final ResourceLocation texture = new ResourceLocation(ModData.ID, "textures/guis/tankGui.png");

	private PartTypeLoadable partTypeLoadable;
	private PartTypeTransportTank partTypeTank;
	private GuiButtonLoadState loadStateButton;



	public GUIPartTypeTransportTank(InventoryPlayer inventory, PartTypeTransportTank partType)
	{
		super(new ContainerPartTypeTransportTank(inventory, partType));

		partTypeLoadable = partType.getParent().getPartType(PartTypeLoadable.class);
		partTypeTank = partType;
		xSize = 176;
		ySize = 166;
	}

	@Override
	protected void actionPerformed(GuiButton button)
	{
		/* Change load state if part type exists. */
		if (partTypeLoadable != null)
		{
			switch (button.id)
			{
				case 0:
					partTypeLoadable.nextLoadState();
					initGui();
					break;
			}
		}
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int mouseX, int mouseY)
	{
		int xPos = (width - xSize) / 2;
		int yPos = (height - ySize) / 2;

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(texture);
		drawTexturedModalRect(xPos, yPos, 0, 0, xSize, ySize);

		FluidTank fluidTank = partTypeTank.getTank();
		if (fluidTank != null && fluidTank.getFluid() != null)
		{
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			mc.getTextureManager().bindTexture(TextureMap.locationBlocksTexture);
			int i0 = MathHelper.ceiling_float_int(47 * partTypeTank.getFillPercentage());
			IIcon icon = fluidTank.getFluid().getFluid().getIcon();
			Tessellator tessellator = Tessellator.instance;

			for (int i1 = 0; i1 < i0; i1 += 16)
			{
				int i2 = MathHelper.clamp_int((i0 - i1), 0, 16);

				tessellator.startDrawingQuads();
				tessellator.addVertexWithUV(xPos + 99, yPos + 66 - i1, 0, icon.getMinU(), icon.getInterpolatedV(0));
				tessellator.addVertexWithUV(xPos + 115, yPos + 66 - i1, 0, icon.getInterpolatedU(16), icon.getInterpolatedV(0));
				tessellator.addVertexWithUV(xPos + 115, yPos + 66 - i1 - i2, 0, icon.getInterpolatedU(16), icon.getInterpolatedV(i2));
				tessellator.addVertexWithUV(xPos + 99, yPos + 66 - i1 - i2, 0, icon.getMinU(), icon.getInterpolatedV(i2));
				tessellator.draw();

				tessellator.startDrawingQuads();
				tessellator.addVertexWithUV(xPos + 16 + 99, yPos + 66 - i1, 0, icon.getMinU(), icon.getInterpolatedV(0));
				tessellator.addVertexWithUV(xPos + 16 + 115, yPos + 66 - i1, 0, icon.getInterpolatedU(16), icon.getInterpolatedV(0));
				tessellator.addVertexWithUV(xPos + 16 + 115, yPos + 66 - i1 - i2, 0, icon.getInterpolatedU(16), icon.getInterpolatedV(i2));
				tessellator.addVertexWithUV(xPos + 16 + 99, yPos + 66 - i1 - i2, 0, icon.getMinU(), icon.getInterpolatedV(i2));
				tessellator.draw();
			}
		}

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.getTextureManager().bindTexture(texture);
		drawTexturedModalRect(xPos + 99, yPos + 21, 176, 0, 8, 43);
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY)
	{
		String s = I18n.format(partTypeTank.getInventoryName());
		fontRendererObj.drawString(s, (xSize / 2) - (fontRendererObj.getStringWidth(s) / 2), 6, 4210752);
		fontRendererObj.drawString(I18n.format("container.inventory"), 8, ySize - 94, 4210752);
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float f)
	{
		super.drawScreen(mouseX, mouseY, f);

		int xPos = (width - xSize) / 2;
		int yPos = (height - ySize) / 2;

		FluidTank fluidTank = partTypeTank.getTank();
		if (fluidTank != null && mouseX > xPos + 97 && mouseX < xPos + 132 && mouseY > yPos + 17 && mouseY < yPos + 67)
		{
			List<String> listInfo = new ArrayList<String>();
			if (fluidTank.getFluid() != null) listInfo.add(fluidTank.getFluid().getFluid().getLocalizedName(fluidTank.getFluid()));
			listInfo.add("§7" + fluidTank.getFluidAmount() + " / " + fluidTank.getCapacity() + " mB");
			drawHoveringText(listInfo, mouseX, mouseY, fontRendererObj);
		}
		else if (loadStateButton != null && partTypeLoadable != null && loadStateButton.getHoverState(mouseX, mouseY) == 2)
		{
			List<String> listInfo = new ArrayList<String>();
			listInfo.add(I18n.format(ModData.ID + ".text.guiVehParTransport." + partTypeLoadable.getLoadState().toString().toLowerCase()));
			drawHoveringText(listInfo, mouseX, mouseY, fontRendererObj);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void initGui()
	{
		super.initGui();

		if (partTypeLoadable != null)
		{
			loadStateButton = new GuiButtonLoadState(0, ((width - xSize) / 2) + xSize - 16, ((height - ySize) / 2) + 6, partTypeLoadable.getLoadState().ordinal());
			buttonList.clear();
			buttonList.add(loadStateButton);
		}
	}
}
