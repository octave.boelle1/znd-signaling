package zoranodensha.vehicleParts.common.parts.basic;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.basic.VehParRenderer_BasicTriangle;



public class VehParBasicTriangle extends AVehParBasic
{
	public VehParBasicTriangle()
	{
		super("VehParBasicTriangle");
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_BasicTriangle(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, " P ", " D ", "PPP", 'D', "dye", 'P', "plateSteel"));
	}
}
