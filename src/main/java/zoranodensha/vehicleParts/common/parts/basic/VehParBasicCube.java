package zoranodensha.vehicleParts.common.parts.basic;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.basic.VehParRenderer_BasicCube;



public class VehParBasicCube extends AVehParBasic
{
	public VehParBasicCube()
	{
		super("VehParBasicCube");
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_BasicCube(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		ItemStack result = new ItemStack(itemStack.getItem(), 4, itemStack.getItemDamage());
		result.setTagCompound(itemStack.getTagCompound());
		GameRegistry.addRecipe(new ShapedOreRecipe(result, "   ", " C ", "   ", 'C', new ItemStack(ModCenter.ItemPart, 1, 7)));
	}
}
