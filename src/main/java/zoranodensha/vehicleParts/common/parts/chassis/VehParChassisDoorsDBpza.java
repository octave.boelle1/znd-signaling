package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.APartProperty;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropInteger;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisDoorsDBpza;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParChassisDoorsDBpza extends AVehParChassisDoorBase
{
	/** Property indicating the type/style of part. */
	public APartProperty<Integer> prop_type;
	public PropColor colorBaseplate;
	public PropColor colorStripeBottom;
	public PropColor colorStripeTop;



	/**
	 * Initialises a new instance of the {@link zoranodensha.vehicleParts.common.parts.chassis.VehParChassisDoorsDBpza} class.
	 */
	public VehParChassisDoorsDBpza()
	{
		this(false, 0);
	}

	/**
	 * Initialises a new instance of the {@link zoranodensha.vehicleParts.common.parts.chassis.VehParChassisDoorsDBpza} class.
	 * 
	 * @param reversed - This must be {@code} if this part is facing backwards relative to its normal direction. This is to ensure the doors open on the correct side.
	 * @param type - The starting type to assign to this part.
	 */
	public VehParChassisDoorsDBpza(boolean reversed, int type)
	{
		super("VehParChassisDoorsDBpza", 0.75F, 0.63622F, 1.8F);

		addProperty(prop_type = new PropInteger.PropIntegerBounded(this, type, 0, 1, "type").setConfigurable());
		addProperty(colorBaseplate = (PropColor)new PropColor(this, ELiveryColor.GREY_DARK.color(), "color_baseplate").setConfigurable());
		addProperty(colorStripeBottom = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_stripes_bot").setConfigurable());
		addProperty(colorStripeTop = (PropColor)new PropColor(this, ELiveryColor.NAVY.color(), "color_stripes_top").setConfigurable());

		prop_colorDoors.set(ELiveryColor.NAVY.color());
		colorOut.set(ELiveryColor.WHITE.color());
		colorIn.set(ELiveryColor.GREY_LIGHT.color());
		type_door.doorSpeed.set(0.016F);
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisDoorsDBpza(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "GPG", "GPG", "PPP", 'P', "plateSteel", 'G', "paneGlass"));
	}
}