package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisFrameSWF;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParBaseplateFrameSWF extends AVehParBaseImpl
{
	/** This part's color. */
	public PropColor color;
	/** Number of center pieces between the end pieces. */
	public PropIntegerBounded pieces;



	public VehParBaseplateFrameSWF()
	{
		super("VehParBaseplateFrameSWF", 0.65F, 0.15F, 0.5F);

		addProperty(color = (PropColor)new PropColor(this, ELiveryColor.GREY_DARK.color(), "color").setConfigurable());
		addProperty(pieces = (PropIntegerBounded)new PropIntegerBounded(this, 2, 0, 100, "pieces")
		{
			@Override
			public boolean set(Object property)
			{
				if (super.set(property))
				{
					if (getTrain() != null)
					{
						getTrain().refreshProperties();
					}
					return true;
				}
				return false;
			}
		}.setConfigurable());
	}

	@Override
	public float getMass()
	{
		return super.getMass() + (pieces.get() * 0.3F);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisFrameSWF(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "III", "S S", " S ", 'S', "plateSteel", 'I', "ingotSteel"));
	}
}