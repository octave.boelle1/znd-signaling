package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_BaseplateSWF;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParBaseplateSWF extends AVehParBaseImpl
{
	/** This part's color. */
	public PropColor color;
	/** Property holding whether this part is full length or not. */
	public PropBoolean isDouble;



	public VehParBaseplateSWF()
	{
		super("VehParBaseplateSWF", 0.4F, 0.05F, 0.2F);

		addProperty(color = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color").setConfigurable());
		addProperty(isDouble = (PropBoolean)new PropBoolean(this, true, "isDouble")
		{
			@Override
			public boolean set(Object property)
			{
				if (super.set(property))
				{
					if (getTrain() != null)
					{
						getTrain().refreshProperties();
					}
					return true;
				}
				return false;
			}
		}.setConfigurable());
	}

	@Override
	public float getMass()
	{
		int i = isDouble.get() ? 2 : 1;
		return super.getMass() * i;
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_BaseplateSWF(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "   ", "SSS", "I I", 'S', "plateSteel", 'I', "ingotSteel"));
	}
}