package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_BaseplateEndSWF;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParBaseplateEndSWF extends AVehParBaseImpl
{
	/* Baseplate color. */
	public PropColor color;
	/* Buffer color. */
	public PropColor colorBuffer;



	public VehParBaseplateEndSWF()
	{
		super("VehParBaseplateEndSWF", 0.24F, 0.1F, 0.27F);

		addProperty(color = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color").setConfigurable());
		addProperty(colorBuffer = (PropColor)new PropColor(this, new ColorRGBA(0.20F, 0.20F, 0.20F), "color_buffer").setConfigurable());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_BaseplateEndSWF(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "SSS", " B ", "   ", 'S', "plateSteel", 'B', "vehParBufferBR101"));
	}
}