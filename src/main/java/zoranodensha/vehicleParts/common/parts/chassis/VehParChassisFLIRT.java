package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropEnum;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisFLIRT;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParChassisFLIRT extends AVehParChassisBase
{
	/** Property holding whether to render the UVID of the part's vehicle. */
	public PropBoolean hasUVID;

	/** Outer sides' color, surrounding windows. */
	public PropColor colorSides;
	/** Color of double stripes. */
	public PropColor colorStripesDouble;
	/** Color of single stripe. */
	public PropColor colorStripesSingle;
	/** Color of top-most stripe. */
	public PropColor colorStripesTop;
	/** Window color. */
	public PropColor colorWindows;

	/** Property holding the {@link EChassis type of chassis}. */
	public PropEnum<EChassis> prop_type;



	public VehParChassisFLIRT()
	{
		super("VehParChassisFLIRT", 0.75F, 1.0F, 2.3F);

		colorIn.set(ELiveryColor.WHITE.color());
		colorOut.set(ELiveryColor.WHITE.color());

		addProperty(colorSides = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_sides").setConfigurable());
		addProperty(colorStripesDouble = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_stripes_double").setConfigurable());
		addProperty(colorStripesSingle = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_stripes_single").setConfigurable());
		addProperty(colorStripesTop = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_stripes_top").setConfigurable());
		addProperty(colorWindows = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color().setAlpha(0.75F), "color_windows").setConfigurable());
		addProperty(prop_type = (PropEnum<EChassis>)new PropEnum<EChassis>(this, EChassis.LO, "type").setConfigurable());

		addProperty(hasUVID = (PropBoolean)new PropBoolean(this, "hasUVID")
		{
			@Override
			public Boolean get()
			{
				return super.get() && (prop_type.get() != EChassis.TR);
			}

			@Override
			public boolean getIsConfigurable()
			{
				return super.getIsConfigurable() && (prop_type.get() != EChassis.TR);
			};
		}.setConfigurable());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisFLIRT(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, " D ", "PGP", " P ", 'P', "plateSteel", 'G', "paneGlass", 'D', "dyeGray"));
	}



	/**
	 * Various types of FLIRT chassis pieces, grouped by their floor's height.
	 */
	public static enum EChassis
	{
		/** High-floor pieces. */
		HI,
		/** High-floor elevated pieces. */
		HI_EL,
		/** Low-floor pieces. */
		LO,
		/** Low-floor pieces with destination board. */
		LO_BD,
		/** Floor pieces with medium height. */
		MD,
		/** Transition to elevated pieces. */
		TR;
	}
}