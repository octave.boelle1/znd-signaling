package zoranodensha.vehicleParts.common.parts.chassis;

import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.PartTypeDoor;



/**
 * Custom door type implementation overriding methods to play door sounds.
 */
public class PartTypeDoorImpl extends PartTypeDoor
{
	public PartTypeDoorImpl(VehParBase parent)
	{
		super(parent);

		/* Ensure the parent is a valid type. */
		if (!(parent instanceof AVehParChassisDoorBase))
		{
			throw new IllegalArgumentException("Supplied parent was no " + AVehParChassisDoorBase.class.getSimpleName() + "!");
		}
	}
	
	@Override
	protected void playOpeningChimeSound()
	{
		((AVehParChassisDoorBase)getParent()).prop_soundOpeningChime.play(90);
	}

	@Override
	protected void playDoorShutSound()
	{
		((AVehParChassisDoorBase)getParent()).prop_soundDoorShut.play();
	}

	@Override
	protected void playWarningChimeSound()
	{
		((AVehParChassisDoorBase)getParent()).prop_soundWarningChime.play();
	}
}