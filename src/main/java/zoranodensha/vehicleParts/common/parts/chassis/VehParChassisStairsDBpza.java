package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisStairsDBpza;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParChassisStairsDBpza extends AVehParChassisBase
{
	public PropIntegerBounded prop_type;
	public PropColor colorBaseplate;
	public PropColor colorStripeBottom;
	public PropColor colorStripeTop;
	public PropColor colorRoof;



	public VehParChassisStairsDBpza()
	{
		this(0);
	}

	public VehParChassisStairsDBpza(int type)
	{
		super("VehParChassisStairsDBpza", 0.75F, 1.28F, 2.55F);

		addProperty(prop_type = (PropIntegerBounded)new PropIntegerBounded(this, type, 0, 1, "type").setConfigurable());
		addProperty(colorBaseplate = (PropColor)new PropColor(this, ELiveryColor.GREY_DARK.color(), "color_baseplate").setConfigurable());
		addProperty(colorStripeBottom = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_stripes_bot").setConfigurable());
		addProperty(colorStripeTop = (PropColor)new PropColor(this, ELiveryColor.NAVY.color(), "color_stripes_top").setConfigurable());
		addProperty(colorRoof = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_roof").setConfigurable());

		colorOut.set(ELiveryColor.WHITE.color());
		colorIn.set(ELiveryColor.GREY_LIGHT.color());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisStairsDBpza(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "PSP", " P ", "PSP", 'P', "plateSteel", 'S', Blocks.glass_pane));
	}
}