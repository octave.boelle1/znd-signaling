package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropInteger.PropIntegerBounded;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisTopDBpza;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParChassisTopDBpza extends AVehParChassisBase
{
	public PropIntegerBounded prop_type;
	public PropColor prop_colorRoof;
	public PropColor prop_colorFloor;
	public PropColor prop_colorStripe;



	/**
	 * Initialises a new instance of the {@link zoranodensha.vehicleParts.common.parts.chassis.VehParChassisTopDBpza}.
	 */
	public VehParChassisTopDBpza()
	{
		this(0);
	}


	/**
	 * Initialises a new instance of the {@link zoranodensha.vehicleParts.common.parts.chassis.VehParChassisTopDBpza} class.
	 * 
	 * @param type - The starting type to assign to this part.
	 */
	public VehParChassisTopDBpza(int type)
	{
		super("VehParChassisTopDBpza", 0.75F, 0.63127F, 1.05F);

		addProperty(prop_type = (PropIntegerBounded)new PropIntegerBounded(this, type, 0, 2, "type").setConfigurable());
		addProperty(prop_colorRoof = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_roof").setConfigurable());
		addProperty(prop_colorFloor = (PropColor)new PropColor(this, ELiveryColor.GREY_DARK.color(), "color_floor").setConfigurable());
		addProperty(prop_colorStripe = (PropColor)new PropColor(this, ELiveryColor.NAVY.color(), "color_strip").setConfigurable());

		colorOut.set(ELiveryColor.WHITE.color());
		colorIn.set(ELiveryColor.GREY_LIGHT.color());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisTopDBpza(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "PPP", "PGP", " P ", 'P', "plateSteel", 'G', "paneGlass"));
	}
}