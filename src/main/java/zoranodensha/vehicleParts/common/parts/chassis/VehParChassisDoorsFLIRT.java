package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropEnum;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisDoorsFLIRT;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParChassisDoorsFLIRT extends AVehParChassisDoorBase
{
	/** Outer sides' color, surrounding windows. */
	public PropColor colorSides;
	/** Color of double stripes. */
	public PropColor colorStripesDouble;
	/** Color of single stripe. */
	public PropColor colorStripesSingle;
	/** Color of top-most stripe. */
	public PropColor colorStripesTop;
	/** Window color. */
	public PropColor colorWindows;

	/** Property holding the {@link EDoors type of door}. */
	public PropEnum<EDoors> prop_type;



	public VehParChassisDoorsFLIRT()
	{
		super("VehParChassisDoorsFLIRT", 0.75F, 1.0F, 2.4F);

		addProperty(colorSides = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_sides").setConfigurable());
		addProperty(colorStripesDouble = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_stripes_double").setConfigurable());
		addProperty(colorStripesSingle = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_stripes_single").setConfigurable());
		addProperty(colorStripesTop = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_stripes_top").setConfigurable());
		addProperty(colorWindows = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color().setAlpha(0.75F), "color_windows").setConfigurable());
		addProperty(prop_type = (PropEnum<EDoors>)new PropEnum<EDoors>(this, EDoors.LO2, "type").setConfigurable());

		colorIn.set(ELiveryColor.WHITE.color());
		colorOut.set(ELiveryColor.WHITE.color());
		prop_colorDoors.set(ELiveryColor.NAVY.color());
		type_door.doorShutAt.set(0.4F);
		type_door.doorSpeed.set(0.0175F);
	}


	@Override
	protected void addPropertySounds()
	{
		addProperty(prop_soundOpeningChime = new PropSound(this, ModData.ID + ":vehPar_chassisDoors_FLIRT3_openingchime", "soundOpeningChime"));
		addProperty(prop_soundWarningChime = new PropSound(this, ModData.ID + ":vehPar_chassisDoors_FLIRT3_warningchime", "soundWarningChime"));
		addProperty(prop_soundDoorShut = new PropSound(this, ModData.ID + ":vehPar_chassisDoors_FLIRT3_doorshut", "soundDoorShut"));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisDoorsFLIRT(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "PPP", "GPG", "GPG", 'P', "plateSteel", 'G', "paneGlass"));
	}



	/**
	 * Various door types of the FLIRT 3.
	 */
	public static enum EDoors
	{
		/** High-level floor, single door. */
		HI1,
		/** Low-level floor, single door. */
		LO1,
		/** Low-level floor, double door. */
		LO2;
	}
}