package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_BaseplateModFreightMid;



public class VehParBaseplateModFreightMid extends AVehParBaseplateModFreight
{
	public VehParBaseplateModFreightMid()
	{
		super("VehParBaseplateModFreightMid", 2.3F);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_BaseplateModFreightMid(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "PPP", "S S", "   ", 'P', "plateSteel", 'S', "ingotSteel"));
	}
}
