package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_ChassisFillerFLIRT;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParChassisFillerFLIRT extends AVehParChassisBase
{
	/** Outer sides' color, surrounding windows. */
	public PropColor colorSides;
	/** Color of double stripes. */
	public PropColor colorStripesDouble;
	/** Color of single stripe. */
	public PropColor colorStripesSingle;
	/** Color of top-most stripe. */
	public PropColor colorStripesTop;

	/** Property holding whether this part has a high-level floor. */
	public PropBoolean isHighFloor;



	public VehParChassisFillerFLIRT()
	{
		super("VehParChassisFillerFLIRT", 0.75F, 1.0F, 1.3F);

		colorIn.set(ELiveryColor.WHITE.color());
		colorOut.set(ELiveryColor.WHITE.color());

		addProperty(isHighFloor = (PropBoolean)new PropBoolean(this, "isHighFloor").setConfigurable());
		addProperty(colorSides = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_sides").setConfigurable());
		addProperty(colorStripesDouble = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_stripes_double").setConfigurable());
		addProperty(colorStripesSingle = (PropColor)new PropColor(this, ELiveryColor.GREY_MEDIUM.color(), "color_stripes_single").setConfigurable());
		addProperty(colorStripesTop = (PropColor)new PropColor(this, ELiveryColor.GREY_LIGHT.color(), "color_stripes_top").setConfigurable());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_ChassisFillerFLIRT(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "D  ", "PGP", " P ", 'P', "plateSteel", 'G', "paneGlass", 'D', "dyeGray"));
	}
}