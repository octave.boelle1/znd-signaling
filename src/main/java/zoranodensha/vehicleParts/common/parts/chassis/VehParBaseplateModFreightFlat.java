package zoranodensha.vehicleParts.common.parts.chassis;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.APartProperty;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.chassis.VehParRenderer_BaseplateModFreightFlat;



public class VehParBaseplateModFreightFlat extends AVehParBaseplateModFreight
{
	/** Property holding whether this part renders a UVID. */
	public APartProperty<Boolean> prop_hasUVID;



	public VehParBaseplateModFreightFlat()
	{
		super("VehParBaseplateModFreightFlat", 0.5F, 0.0475F, 1.025F);
		addProperty(prop_hasUVID = new PropBoolean(this, true, "hasUVID").setConfigurable());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_BaseplateModFreightFlat(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "PPP", " S ", "   ", 'P', "plateSteel", 'S', "ingotSteel"));
	}
}