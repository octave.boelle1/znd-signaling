package zoranodensha.vehicleParts.common.parts.engine;

import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;
import zoranodensha.vehicleParts.common.sounds.DieselEngineSounds;



public abstract class AVehParEngineDiesel extends AVehParBaseImpl
{
	/** This engine's part type. */
	public final PartTypeEngineDieselImpl type_engine;

	/** Sounds Wrapper */
	protected final DieselEngineSounds engineSounds;



	/**
	 * Abstract diesel engine class.
	 * 
	 * @param name - The unique name of this vehicle part.
	 * @param widthBy2 - Half of the bounding box' width.
	 * @param heightBy2 - Half of the bounding box' height.
	 * @param mass - Mass of this part (in metric Tonnes, {@code 1t = 1000kg}).
	 * @param power - The maximum output power of this engine ({@code kW}).
	 * @param efficiency - The efficiency of this engine. {@code 75%} is average for most diesel trains.
	 * @param responsiveness - The responsiveness of the engine, which will affect revving speed. {@code 1.0F} should be fine for most diesel engines.
	 * @param idleRPM - The RPM at which this engine will idle at {@code 0.0F} throttle.
	 * @param maxRPM - The RPM at which the engine will be at during full throttle. The redline may be higher than this.
	 */
	public AVehParEngineDiesel(String name, float widthBy2, float heightBy2, float mass, int power, float efficiency, float responsiveness, int idleRPM, int maxRPM)
	{
		super(name, widthBy2, heightBy2, mass);

		engineSounds = new DieselEngineSounds(this);
		type_engine = new PartTypeEngineDieselImpl(this);
		type_engine.outputKW.set(power);
		type_engine.efficiency.set(efficiency);
		type_engine.responsiveness.set(responsiveness);
		type_engine.idleRPM.set(idleRPM);
		type_engine.maxRPM.set(maxRPM);
	}
}