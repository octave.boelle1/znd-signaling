package zoranodensha.vehicleParts.common.parts.engine;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeEngineElectric;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.engine.VehParRenderer_EngineElectricBR101;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;
import zoranodensha.vehicleParts.common.sounds.ElectricEngineSoundsBR101;



public class VehParEngineElectricBR101 extends AVehParBaseImpl
{
	/**
	 * Part type of this engine.
	 */
	public final PartTypeEngineElectric type_engine;

	/**
	 * Wrapper class for the electric engine sounds.
	 */
	protected ElectricEngineSoundsBR101 engineSounds;


	public VehParEngineElectricBR101()
	{
		super("VehParEngineElectricBR101", 0.75F, 0.77F, 1.75F);

		type_engine = new PartTypeEngineElectric(this)
		{
			/**
			 * Hooking into the part type's onUpdate method to ensure the sounds are updated for this class.
			 */
			@Override
			public void onTick()
			{
				super.onTick();
				onUpdate();
			}

			;
		};

		/*
		 * Total power output of BR101's two traction motors distributed over three
		 * engine parts.
		 */
		type_engine.outputKW.set(6400 / 3);

		/*
		 * Create the engine sounds wrapper. This will add the necessary PropSound instances to this class.
		 */
		engineSounds = new ElectricEngineSoundsBR101(this, this.type_engine);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_EngineElectricBR101(this) : renderer);
	}

	/**
	 * This method is called to update the sound wrapper class for this vehicle part.
	 */
	protected void onUpdate()
	{
		if (engineSounds != null)
		{
			engineSounds.tick();
		}
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, " M ", "RCR", "RCR", 'M', new ItemStack(ModCenter.ItemPart, 1, 7), 'R', "dustRedstone", 'C', "circuitBasic"));
	}
}