package zoranodensha.vehicleParts.common.parts.other;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.other.VehParRenderer_Compressor;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;



public class VehParCompressor extends AVehParBaseImpl
{
	public final PartTypeCompressorImpl type_compressor;



	public VehParCompressor()
	{
		super("VehParCompressor", 0.300F, 0.250F, 0.100F);

		type_compressor = new PartTypeCompressorImpl(this);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_Compressor(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "III", "GSI", "III", 'I', Items.iron_ingot, 'G', Items.gold_ingot, 'S', Items.slime_ball));
	}

}
