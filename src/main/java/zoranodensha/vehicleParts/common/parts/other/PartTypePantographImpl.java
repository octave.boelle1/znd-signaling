package zoranodensha.vehicleParts.common.parts.other;

import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.PartTypePantograph;



/**
 * Pantograph type implementation hooking into the parent part's animation tick method.
 */
public class PartTypePantographImpl extends PartTypePantograph
{
	public PartTypePantographImpl(VehParBase parent)
	{
		super(parent);

		/* Ensure the parent is a valid type. */
		if (!(parent instanceof VehParPantograph))
		{
			throw new IllegalArgumentException("Supplied parent was no " + VehParPantograph.class.getSimpleName() + "!");
		}
	}

	@Override
	public void onTick()
	{
		/* On clients, tick the animation. */
		super.onTick();
		if (getParent().getTrain().worldObj.isRemote)
		{
			((VehParPantograph)getParent()).onAnimationTick();
		}
	}
}