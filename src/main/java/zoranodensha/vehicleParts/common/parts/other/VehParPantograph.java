package zoranodensha.vehicleParts.common.parts.other;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.structures.IPowerLine;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropEnum;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.client.entityFX.EParticles;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.util.Helpers;
import zoranodensha.common.util.ZnDMathHelper;
import zoranodensha.vehicleParts.client.render.other.VehParRenderer_Pantograph;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;



public class VehParPantograph extends AVehParBaseImpl
{
	/** This part's pantograph part type. */
	public final PartTypePantographImpl type_pantograph;

	/** Model type of this pantograph. */
	public PropEnum<EPantograph> prop_type;

	/*
	 * Colours
	 */
	public PropColor colorBaseFrame;
	public PropColor colorBaseInsulators;
	public PropColor colorBaseSprings;
	public PropColor colorArmBottom;
	public PropColor colorArmTop;
	public PropColor colorHeadFrame;
	public PropColor colorHeadGuides;


	/** Value representing how far the pantograph has extended. This is used for animation purposes and only exists on the client. */
	@SideOnly(Side.CLIENT) public float animationExtension;
	/** Value representing how far the pantograph was extended at the previous tick, used when interpolating animations. */
	@SideOnly(Side.CLIENT) public float animationExtensionLast;
	/** The current velocity of the pantograph (vertically). Used only for the animation and is only accessible on clients. */
	@SideOnly(Side.CLIENT) public float animationVelocity;

	/** A timer for overhead line spark spawning. If it is greater than 0, sparks will be spawned each update tick until it is 0 again. */
	@SideOnly(Side.CLIENT) private int sparkTimer;
	/** Counts the number of bumps during animation and stops the animation if necessary. */
	@SideOnly(Side.CLIENT) private int bumpCounter;



	public VehParPantograph()
	{
		super("VehParPantograph", 0.325F, 0.2F, 0.27F);

		type_pantograph = new PartTypePantographImpl(this);

		collision.set(false);

		addProperty(colorBaseInsulators = (PropColor)new PropColor(this, new ColorRGBA(0x909090FF), "color_base_ins").setConfigurable());
		addProperty(colorBaseFrame = (PropColor)new PropColor(this, new ColorRGBA(0x404040FF), "color_base_frame").setConfigurable());
		addProperty(colorBaseSprings = (PropColor)new PropColor(this, new ColorRGBA(0x303030FF), "color_base_springs").setConfigurable());
		addProperty(colorArmBottom = (PropColor)new PropColor(this, new ColorRGBA(0x404040FF), "color_arm_bot").setConfigurable());
		addProperty(colorArmTop = (PropColor)new PropColor(this, new ColorRGBA(0x383838FF), "color_arm_top").setConfigurable());
		addProperty(colorHeadFrame = (PropColor)new PropColor(this, new ColorRGBA(0x606060FF), "color_head_frame").setConfigurable());
		addProperty(colorHeadGuides = (PropColor)new PropColor(this, new ColorRGBA(0x505050FF), "color_head_guides").setConfigurable());
		addProperty(prop_type = (PropEnum<EPantograph>)new PropEnum<EPantograph>(this, EPantograph.HEAD1_ARM2, "type").setConfigurable());
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_Pantograph(this) : renderer);
	}

	@Override
	public AxisAlignedBB initBoundingBox()
	{
		float scaledWidthBy2 = getScaledWidth() / 2.0F;
		float scaledHeightOffset = 0.0F;

		/*
		 * Extend bounding box top depending on pantograph extension.
		 * We need a null-check here to avoid NPE during initialisation, as this is called in VehParBase before this part's constructor.
		 */
		if (type_pantograph != null && type_pantograph.getExtension() != 0.0F)
		{
			scaledHeightOffset = 2.5F * type_pantograph.getExtension() * (getScale()[1] / 2.0F);
		}

		return AxisAlignedBB.getBoundingBox(-scaledWidthBy2, 0.0, -scaledWidthBy2, scaledWidthBy2, getScaledHeight() + scaledHeightOffset, scaledWidthBy2);
	}

	@SideOnly(Side.CLIENT)
	protected void onAnimationTick()
	{
		animationExtensionLast = animationExtension;
		animationVelocity += (type_pantograph.getExtension() - animationExtension) / 16.0F;
		animationExtension += animationVelocity;
		animationVelocity *= 0.85F;

		if (bumpCounter > 1)
		{
			animationExtensionLast = animationExtension = type_pantograph.getExtension();
			animationVelocity = 0.0F;
			bumpCounter = 0;
		}
		else if (animationExtension < -0.0015F)
		{
			animationExtension = -0.0015F;
			animationVelocity *= -0.8F;
			++bumpCounter;
		}
		else if (animationExtension > 1.0015F)
		{
			animationExtension = 1.0015F;
			animationVelocity *= -0.8F;
			++bumpCounter;
		}

		/* Also update sparks, if applicable. */
		onUpdateSparkles();
	}

	@Override
	public void onPropertyChanged(IPartProperty<?> property)
	{
		super.onPropertyChanged(property);

		/* If the pantograph extension level changed on server side.. */
		if (getTrain() != null && !getTrain().worldObj.isRemote && property == type_pantograph.extension)
		{
			/* ..and the pantograph is now fully extended.. */
			if (type_pantograph.getIsPantographUp())
			{
				/* ..then play pantograph sounds. */
				playSoundPantographUp(getTrain().worldObj);
			}
		}
	}

	/**
	 * Called to handle pantograph sparks.
	 */
	@SideOnly(Side.CLIENT)
	private void onUpdateSparkles()
	{
		if (ModCenter.cfg.rendering.detailVehicles <= 0)
		{
			return;
		}

		/* At full extension and speeds around 80km/h. */
		if (type_pantograph.getIsPantographUp() && Math.abs(getTrain().getLastSpeed()) > 80.0 / (3.6 * 72.0))
		{
			if (sparkTimer == 0 && getTrain().worldObj.getWorldInfo().isRaining() && ZnDMathHelper.ran.nextInt(120) == 0)
			{
				sparkTimer = ZnDMathHelper.ran.nextInt(6);
			}

			if (sparkTimer > 0)
			{
				--sparkTimer;
				double posX = ((boundingBox.maxX - boundingBox.minX) * 0.5D) + boundingBox.minX;
				double posY = boundingBox.maxY;
				double posZ = ((boundingBox.maxZ - boundingBox.minZ) * 0.5D) + boundingBox.minZ;

				if (getTrain().worldObj.getBlock(MathHelper.floor_double(posX), MathHelper.floor_double(posY + 1.0D), MathHelper.floor_double(posZ)) instanceof IPowerLine)
				{
					Helpers.spawnParticles(EParticles.SPARKS, posX, posY, posZ);
				}
			}
		}
		else
		{
			sparkTimer = 0;
		}
	}

	/**
	 * Called to play the pantograph up sound.
	 */
	private void playSoundPantographUp(World world)
	{
		Vec3 vecPos = PartHelper.getPosition(this);
		world.playSoundEffect(vecPos.xCoord, vecPos.yCoord, vecPos.zCoord, ModData.ID + ":vehPar_pantograph_up", 1.0F, world.rand.nextFloat() * 0.1F + 0.9F);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "III", "  S", "PSC", 'I', "ingotIron", 'S', "ingotSteel", 'C', "circuitBasic", 'P', "plateSteel"));
	}



	public static enum EPantograph
	{
		/** Single head, single top-arm. */
		HEAD1_ARM1,

		/** Single head, double top-arm. */
		HEAD1_ARM2;
	}
}