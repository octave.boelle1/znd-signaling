package zoranodensha.vehicleParts.common.parts.other;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.other.VehParRenderer_DestBoard;
import zoranodensha.vehicleParts.common.parts.basic.AVehParBasic;



public class VehParDestBoard extends AVehParBasic
{
	/** Property holding whether the part is a large screen. */
	public PropBoolean prop_isLarge;



	public VehParDestBoard()
	{
		super("VehParDestBoard", 0.125F, 0.125F, 0.02F);
		addProperty(prop_isLarge = (PropBoolean)new PropBoolean(this, "isLarge").setConfigurable());
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_DestBoard(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, " C ", " M ", " D ", 'C', new ItemStack(ModCenter.ItemPart, 1, 7), 'M', new ItemStack(ModCenter.ItemPart, 1, 2), 'D', "dye"));
	}
}
