package zoranodensha.vehicleParts.common.parts.other;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.other.VehParRenderer_BufferBaseDBpza;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParBufferBaseDBpza extends AVehParBaseImpl
{
	public PropColor color;



	public VehParBufferBaseDBpza()
	{
		super("VehParBufferBaseDBpza", 0.1F, 0.1F, 0.2F);
		addProperty(color = (PropColor)new PropColor(this, ELiveryColor.GREY_DARK.color(), "color").setConfigurable());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_BufferBaseDBpza(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "   ", "   ", "PPP", 'P', "plateSteel"));
	}
}
