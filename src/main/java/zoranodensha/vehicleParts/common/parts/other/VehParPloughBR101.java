package zoranodensha.vehicleParts.common.parts.other;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.other.VehParRenderer_PloughBR101;
import zoranodensha.vehicleParts.common.parts.basic.AVehParBasic;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParPloughBR101 extends AVehParBasic
{
	public VehParPloughBR101()
	{
		super("VehParPloughBR101", 0.25F, 0.125F, 0.1F);
		color.set(ELiveryColor.GREY_DARK.color());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_PloughBR101(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "   ", "PSP", "   ", 'P', "plateSteel", 'S', "ingotSteel"));
	}
}
