package zoranodensha.vehicleParts.common.parts.other;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.other.VehParRenderer_IntercirculationDBpza;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;



public class VehParIntercirculationDBpza extends AVehParBaseImpl
{
	public VehParIntercirculationDBpza()
	{
		super("VehParIntercirculation", 0.105F, 0.67092F, 0.1F);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_IntercirculationDBpza(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "P P", "PSP", "P P", 'P', "sheetPlastic", 'S', "ingotSteel"));
	}
}
