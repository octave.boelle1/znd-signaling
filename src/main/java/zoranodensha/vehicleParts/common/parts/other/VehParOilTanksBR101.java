package zoranodensha.vehicleParts.common.parts.other;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.other.VehParRenderer_OilTanksBR101;
import zoranodensha.vehicleParts.common.parts.basic.AVehParBasic;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParOilTanksBR101 extends AVehParBasic
{
	public VehParOilTanksBR101()
	{
		super("VehParOilTanksBR101", 0.63F, 0.275F, 5.0F);
		color.set(ELiveryColor.GREY_DARK.color());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_OilTanksBR101(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "P P", "C P", " BP", 'P', new ItemStack(ModCenter.ItemPart, 1, 7), 'C', "circuitBasic", 'B', new ItemStack(Items.bucket, 1, 0)));
	}
}
