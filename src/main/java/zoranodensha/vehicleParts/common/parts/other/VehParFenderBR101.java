package zoranodensha.vehicleParts.common.parts.other;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.other.VehParRenderer_FenderBR101;
import zoranodensha.vehicleParts.common.parts.basic.AVehParBasic;
import zoranodensha.vehicleParts.presets.ELiveryColor;



public class VehParFenderBR101 extends AVehParBasic
{
	public VehParFenderBR101()
	{
		super("VehParFenderBR101", 0.05381F, 0.05485F, 0.015F);
		color.set(ELiveryColor.NAVY.color());
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_FenderBR101(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "   ", " P ", "   ", 'P', "plateSteel"));
	}
}
