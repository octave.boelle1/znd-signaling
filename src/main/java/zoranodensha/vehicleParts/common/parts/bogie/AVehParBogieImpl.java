package zoranodensha.vehicleParts.common.parts.bogie;

import java.lang.ref.WeakReference;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.vehicles.VehicleSection;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.client.render.ICachedVertexState;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;
import zoranodensha.vehicleParts.common.sounds.BogieSounds;



public abstract class AVehParBogieImpl extends AVehParBaseImpl
{
	/** Bogie part type, handling all bogie functionality. */
	public final PartTypeBogieImpl type_bogie;

	/** Previous value returned by {@link zoranodensha.api.vehicles.part.type.PartTypeBogie#getAccelerationForce(float) getAccelerationForce(float)}. */
	public PropFloat lastPowerProduced;
	/** Bogie sound wrapper, may be {@code null}. */
	protected BogieSounds bogieSounds;

	/** First section reference. */
	public VehicleSection sectionA;
	/** Second section reference. */
	public VehicleSection sectionB;

	/*
	 * Client-only values
	 */
	/** Previous tick's movement distance. */
	@SideOnly(Side.CLIENT) public double lastDistMoved;
	/** Current tick's movement distance. */
	@SideOnly(Side.CLIENT) public double distMoved;



	public AVehParBogieImpl(String name, float widthBy2, float heightBy2, float mass)
	{
		super(name, widthBy2, heightBy2, mass);

		addProperty(lastPowerProduced = (PropFloat)new PropFloat(this, "lastPowerProduced").setSyncDir(ESyncDir.CLIENT));

		type_bogie = new PartTypeBogieImpl(this)
		{
			@Override
			public float getAccelerationForce(float force)
			{
				float forcePrev = super.getAccelerationForce(force);
				lastPowerProduced.set(forcePrev);
				return forcePrev;
			}

			@Override
			public void onAddedToSection(VehicleSection newSection)
			{
				super.onAddedToSection(newSection);

				if (newSection == null)
				{
					sectionA = sectionB = null;
				}
				else if (sectionA == null)
				{
					sectionA = newSection;
				}
				else if (sectionB == null)
				{
					sectionB = newSection;
				}
			}

			@Override
			public void onTick()
			{
				super.onTick();
				onUpdate();
			}
		};
		ICachedVertexState.objectList.add(new WeakReference<ICachedVertexState>(this));
	}

	/**
	 * Helper method to initialise bogie sounds, called after the bogie has been fully initialised.
	 */
	protected abstract void initSounds();

	/**
	 * Updates sounds and movement distance.
	 */
	protected void onUpdate()
	{
		if (bogieSounds != null)
		{
			bogieSounds.onUpdate();
		}

		/*
		 * Update brake physics on server.
		 */
		if (getTrain().worldObj.isRemote)
		{
			/* Update client-sided movement flags. */
			lastDistMoved = distMoved;
			distMoved = getTrain().getLastSpeed();
		}
	}
}