package zoranodensha.vehicleParts.common.parts.bogie;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.bogie.VehParRenderer_AxisY25;
import zoranodensha.vehicleParts.common.sounds.BogieSounds;
import zoranodensha.vehicleParts.common.sounds.BogieSounds.EBogieStyle;



public class VehParAxisY25 extends AVehParBogieImpl
{

	/** Bogie frame color. */
	public PropColor color;
	/** Wheel color. */
	public PropColor colorWheels;



	public VehParAxisY25()
	{
		super("VehParAxisY25", 0.600F, 0.270F, 4.00F);

		addProperty(color = (PropColor)new PropColor(this, new ColorRGBA(0.22F, 0.22F, 0.22F), "color").setConfigurable());
		addProperty(colorWheels = (PropColor)new PropColor(this, new ColorRGBA(0.32F, 0.32F, 0.32F), "color_wheels").setConfigurable());

		setScale(1.0F, 1.0F, 0.8F);
		type_bogie.localY.set(0.27F);
		type_bogie.maxSpeed.set(120);
		type_bogie.brakeForce.set(25);
		initSounds();
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_AxisY25(this) : renderer);
	}

	@Override
	protected void initSounds()
	{
		bogieSounds = new BogieSounds(this, EBogieStyle.FREIGHT);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "LP ", "SCS", "APA", 'L', new ItemStack(ModCenter.ItemBucketOilLubricant), 'P', "plateSteel", 'S', "ingotSteel", 'A', Items.minecart, 'C', new ItemStack(ModCenter.ItemPart, 1, 7)));
	}
}