package zoranodensha.vehicleParts.common.parts.bogie;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeEngineElectric;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.common.core.ModCenter;
import zoranodensha.vehicleParts.client.render.bogie.VehParRenderer_AxisBR101;
import zoranodensha.vehicleParts.common.sounds.BogieSounds;
import zoranodensha.vehicleParts.common.sounds.BogieSounds.EBogieStyle;



public class VehParAxisBR101 extends AVehParBogieImpl
{

	public VehParAxisBR101()
	{
		super("VehParAxisEngine", 0.75F, 0.4195F, 17.0F);

		type_bogie.hasMotor.set(true);
		type_bogie.maxSpeed.set(220);
		type_bogie.localY.set(0.4195F);
		type_bogie.tractiveEffort.set(300 / 2);
		type_bogie.brakeForce.set(150 / 2);

		initSounds();
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_AxisBR101(this) : renderer);
	}


	@Override
	protected void initSounds()
	{
		bogieSounds = new BogieSounds(this, EBogieStyle.PASSENGER);
	}


	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "LCP", "SSS", "APA", 'L', new ItemStack(ModCenter.ItemBucketOilLubricant), 'P', new ItemStack(ModCenter.ItemPart, 1, 7), 'C', "circuitBasic", 'S', "ingotSteel", 'A', Items.minecart));
	}
}