package zoranodensha.vehicleParts.common.parts.bogie;

import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.common.core.ModData;



public class PartTypeBogieImpl extends PartTypeBogie
{

	public PartTypeBogieImpl(VehParBase parent)
	{
		super(parent);

		if (!(parent instanceof AVehParBogieImpl))
		{
			throw new IllegalArgumentException("Supplied parent was no " + AVehParBogieImpl.class.getSimpleName() + "!");
		}

		addProperty(sound_parkBrakeApply = new PropSound(parent, ModData.ID + ":vehPar_axis_parkBrake_apply", "soundParkBrakeApply"));
		addProperty(sound_parkBrakeRelease = new PropSound(parent, ModData.ID + ":vehPar_axis_parkBrake_release", "soundParkBrakeRelease"));
	}

}
