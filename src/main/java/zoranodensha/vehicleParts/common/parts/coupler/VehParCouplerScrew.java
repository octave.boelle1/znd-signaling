package zoranodensha.vehicleParts.common.parts.coupler;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.coupler.VehParRenderer_CouplerScrew;



/**
 * Screw couplers require manual interaction for any action.<br>
 * They <i>couple and decouple manually.</i>
 */
public class VehParCouplerScrew extends AVehParCouplerBase
{
	/** This coupling's part type. */
	public final PartTypeCouplingChainImpl type_coupling;



	public VehParCouplerScrew()
	{
		super("VehParCouplerScrew", 0.125F, 0.125F, 0.05F);

		type_coupling = new PartTypeCouplingChainImpl(this);
		type_coupling.expansion.set(1.500F);
	}

	@Override
	public float getLength()
	{
		return type_coupling.getLength();
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_CouplerScrew(this) : renderer);
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "P  ", " SS", "P  ", 'S', "ingotSteel", 'P', "plateSteel"));
	}
}