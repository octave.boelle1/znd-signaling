package zoranodensha.vehicleParts.common.parts.coupler;

import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;



public abstract class AVehParCouplerBase extends AVehParBaseImpl
{
	/** Property holding the coupling sound. */
	protected PropSound soundCouple;
	/** Property holding the decoupling sound. */
	protected PropSound soundDecouple;



	public AVehParCouplerBase(String name, float widthBy2, float heightBy2, float mass)
	{
		super(name, widthBy2, heightBy2, mass);

		setPosition(0.0, 0.8, 0.0);
		addProperty(soundCouple = new PropSound(this, ModData.ID + ":" + getCoupleSound(), "soundCouple"));
		addProperty(soundDecouple = new PropSound(this, ModData.ID + ":" + getDecoupleSound(), "soundDecouple"));
	}


	protected String getCoupleSound()
	{
		return "vehPar_coupler_generic_couplesound";
	}

	protected String getDecoupleSound()
	{
		return getCoupleSound();
	}

	/**
	 * Bridge method to access this coupling's part type's length.
	 * 
	 * @return This coupling's part type's {@link zoranodensha.api.vehicles.part.type.APartTypeCoupling#getLength() length}.
	 */
	public abstract float getLength();

	@Override
	public void onPropertyChanged(IPartProperty<?> property)
	{
		if (property != soundCouple && property != soundDecouple)
		{
			super.onPropertyChanged(property);
		}
	}

	/**
	 * Called to play a sound after a coupling-related action happened.
	 * 
	 * @param hasCoupled - {@code true} if the {@link #getCoupleSound() coupling sound} shall be played, {@code false} for {@link #getDecoupleSound() decoupling}.
	 */
	protected void playSound(boolean hasCoupled)
	{
		if (getTrain() != null && !getTrain().worldObj.isRemote)
		{
			(hasCoupled ? soundCouple : soundDecouple).play();
		}
	}
}