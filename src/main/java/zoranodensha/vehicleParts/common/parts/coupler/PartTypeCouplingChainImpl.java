package zoranodensha.vehicleParts.common.parts.coupler;

import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.PartTypeCouplingChain;



/**
 * Custom chain coupling part type hooking into the parent to play sounds where desired.
 */
public class PartTypeCouplingChainImpl extends PartTypeCouplingChain
{
	public PartTypeCouplingChainImpl(VehParBase parent)
	{
		super(parent);

		/* Ensure the parent is a valid type. */
		if (!(parent instanceof VehParCouplerScrew))
		{
			throw new IllegalArgumentException("Supplied parent was no " + VehParCouplerScrew.class.getSimpleName() + "!");
		}
	}

	@Override
	protected boolean doTryCouple()
	{
		if (super.doTryCouple())
		{
			((VehParCouplerScrew)getParent()).playSound(true);
			return true;
		}
		return false;
	}

	@Override
	protected boolean doTryDecouple()
	{
		if (super.doTryDecouple())
		{
			((VehParCouplerScrew)getParent()).playSound(false);
			return true;
		}
		return false;
	}
}