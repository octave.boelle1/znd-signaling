package zoranodensha.vehicleParts.common.parts.buffer;

import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;



public abstract class AVehParBuffer extends AVehParBaseImpl
{
	public PropColor color;



	public AVehParBuffer(String name, float mass)
	{
		this(name, 0.15F, 0.12F, mass);
	}

	public AVehParBuffer(String name, float widthBy2, float heightBy2, float mass)
	{
		super(name, widthBy2, heightBy2, mass);
		setPosition(0.0, 0.8, 0.0);
		addProperty(color = (PropColor)new PropColor(this, new ColorRGBA(0.27F, 0.27F, 0.27F), "color").setConfigurable());
	}
}