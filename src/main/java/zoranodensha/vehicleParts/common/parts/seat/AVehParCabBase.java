package zoranodensha.vehicleParts.common.parts.seat;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.property.PropSound.PropSoundRepeated;
import zoranodensha.api.vehicles.part.type.PartTypeCab;
import zoranodensha.api.vehicles.part.type.cab.AScreenRenderer;
import zoranodensha.api.vehicles.part.type.cab.IVehiclePartCab_Tickable;
import zoranodensha.api.vehicles.part.util.BufferedTexture;
import zoranodensha.common.core.ModData;
import zoranodensha.common.util.ZnDMathHelper;
import zoranodensha.vehicleParts.client.render.GraphicsResources;



public abstract class AVehParCabBase extends VehParSeat implements IVehiclePartCab_Tickable
{
	/**
	 * This cab's part type.
	 */
	public final PartTypeCab type_cab;

	/**
	 * {@code true} while this part is reading from NBT.
	 */
	protected boolean isReadingFromNBT;

	/**
	 * Property holding the alerter sound.
	 */
	protected final PropSound soundAlerter;


	public AVehParCabBase(String name, float mass)
	{
		super(name, PartTypeCab.class, mass);

		/*
		 * Type Reference
		 */
		type_cab = (PartTypeCab)type_seat;

		/*
		 * Properties
		 */
		addProperty(soundAlerter = new SoundAlerter(this));
	}


	/**
	 * Returns {@code true} if the riding entity is the local client player.
	 */
	@SideOnly(Side.CLIENT)
	public boolean getEntityIsPlayer()
	{
		return (type_seat.getPassenger() == Minecraft.getMinecraft().thePlayer);
	}


//	/**
//	 * <p>
//	 * This method should return the current position of the main brake lever.
//	 * </p>
//	 * <p>
//	 * <b>The value returned by this method may not always represent the physical position of the brake lever. The value may be modified by the cab's safety systems to automatically apply the
//	 * brakes.</b> If you need the accurate physical position of the brake lever, without adjustment from the cab safety systems, use {@link AVehParCabBase#getBrakePhysical()}.
//	 * </p>
//	 */
//	public float getBrakeLogical()
//	{
//		/* Retrieve current brake level. */
//		float brake = getBrakePhysical();
//
//		/*
//		 * Make sure the current braking level meets the minimum requirements of MTMS at
//		 * this time.
//		 */
//		brake = Math.max(brake, type_cab.mtms.getMinimumBrake());
//
//		return brake;
//	}


//	/**
//	 * <p>
//	 * Gets the current dynamic brake level requested by this cab. The value returned is the physical position of the dynamic brake lever, in some cases modified by the cab safety systems to initiate dynamic braking.
//	 * </p>
//	 *
//	 * @return - The current dynamic brake level requested by this cab, ranging from {@code 0.0F} (0%, OFF) to {@code 1.0F} (100%, MAXIMUM DYNAMIC BRAKING).
//	 */
//	public float getDynamicBrakeLogical()
//	{
//		/* Retrieve current brake level. */
//		float brake = getDynamicBrakePhysical();
//
//		/*
//		 * Make sure the current braking level meets the minimum requirements of MTMS at
//		 * this time.
//		 */
//		brake = Math.max(brake, type_cab.mtms.getMinimumBrake());
//
//		return brake;
//	}

//	/**
//	 * <p>
//	 * Gets the current physical position of the dynamic brake lever.
//	 * </p>
//	 * <p>
//	 * If this cab does not contain a dynamic brake lever, this method will return {@code 0.0F}.
//	 * </p>
//	 *
//	 * @return - The current physical position of the dynamic brake lever, ranging from {@code 0.0F} (0%, OFF) to {@code 1.0F} (100%, MAXIMUM DYNAMIC BRAKING).
//	 */
//	public abstract float getDynamicBrakePhysical();

	//	/**
	//	 * <p>
	//	 * Gets the current throttle level as is represented physically by the lever, unless a different value is required by the cab safety systems, in which case that value is returned instead.
	//	 * </p>
	//	 * <p>
	//	 * If the physical position of the lever is required without any safety system interference, use {@link AVehParCabBase#getThrottlePhysical()} instead.
	//	 * </p>
	//	 *
	//	 * @return - Current physical throttle lever position ranging from {@code 0.0F} (0%) to {@code 1.0F} (100%).
	//	 */
	//	public float getThrottleLogical()
	//	{
	//		/* Retrieve current value. */
	//		float throttle = getThrottlePhysical();
	//
	//		/*
	//		 * If the autodrive system is enabled, force maximum power.
	//		 */
	//		if (type_cab.mtms.autodriveState.get() != EAutodriveState.INACTIVE)
	//		{
	//			throttle = 1.0F;
	//		}
	//
	//		/*
	//		 * This will make sure that the train doesn't keep accelerating if the emergency brakes are applied.
	//		 */
	//		if (type_cab.getDoForceStop() && throttle > 0.0F)
	//		{
	//			throttle = 0.0F;
	//		}
	//
	//		/*
	//		 * Make sure that the current throttle does not exceed the maximum allowed
	//		 * throttle by MTMS.
	//		 */
	//		throttle = Math.min(throttle, type_cab.mtms.getMaximumThrottle());
	//
	//		return throttle;
	//	}

	/**
	 * Method called when all client-side cab controls should be initialised. All {@link CabControl} fields in the cab should be instantiated with their default starting values.
	 */
	public abstract void initControls();


	@Override
	public void onPropertyChanged(IPartProperty<?> property)
	{
		super.onPropertyChanged(property);

		/* Notify MTMS subsystem. */
		type_cab.mtms.onPropertyChanged(property);
	}


	@Override
	public void tick()
	{
		// Bail out if the parent train or world is null.
		if (getTrain() == null || getTrain().worldObj == null)
		{
			return;
		}

		// Update the parent part type controls on the server-side only.
		if (!getTrain().worldObj.isRemote)
		{
			updatePartTypeControls();
		}
	}


	@Override
	public void readFromNBT(NBTTagCompound nbt, ETagCall callType)
	{
		isReadingFromNBT = true;
		super.readFromNBT(nbt, callType);
		isReadingFromNBT = false;
	}


	/**
	 * <p>This method should contain code to update the appropriate control values (power, brake, etc) of the parent cab part-type. You only need to update values that correspond to a lever that exists in this particular cab
	 * implementation. For example, if the cab you are implementing only has a pneumatic brake lever, but NO dynamic brake lever, simply only update the pneumatic brake lever value and leave the dynamic brake one untouched (it will remain
	 * at {@code 0.0F}).</p>
	 * <p>This method is called on every server-side update tick.</p>
	 */
	protected abstract void updatePartTypeControls();


	/**
	 * Alerter sound class.
	 */
	public static class SoundAlerter extends PropSoundRepeated
	{
		public SoundAlerter(AVehParCabBase parent)
		{
			super(parent, ModData.ID + ":vehPar_cab_alerter", "soundAlerter");
		}

		@Override
		public Integer get()
		{
			AVehParCabBase cab = (AVehParCabBase)getParent();
			if (cab.getTrain() != null && cab.getTrain().worldObj.isRemote)
			{
				/* On client side, only play Alerter sound if the player is the driver. */
				if (!cab.getEntityIsPlayer())
				{
					return 0;
				}
			}
			return cab.type_cab.alerter.getDoAlert() ? -1 : 0;
		}
	}



	/**
	 * Client-sided helper class containing render data of physical controls in the cab.
	 */
	@SideOnly(Side.CLIENT)
	public static class CabControl
	{

		/**
		 * The current state of the control. Depending on the control, this may be an angle or displacement.
		 */
		private float state;
		private float lastState;
		private float target;
		private float velocity;

		private final float min, max;
		private final float maxChange;
		private final float floppiness;


		public CabControl(float minimum, float maximum, float floppiness, float maxChange, float state)
		{
			this.min = minimum;
			this.max = maximum;
			this.maxChange = maxChange;
			this.floppiness = floppiness;
			this.target = this.lastState = this.state = state;
		}


		public boolean getIsMoving()
		{
			return lastState != state;
		}

		public float getState()
		{
			return state;
		}

		public float getState(float partialTick)
		{
			return lastState + ((state - lastState) * partialTick);
		}

		public float getTarget()
		{
			return target;
		}

		public void setTarget(float newTarget)
		{
			this.target = newTarget;
		}

		public void tick()
		{
			lastState = state;
			velocity += (target - state) / floppiness;

			if (velocity < -maxChange)
				velocity = -maxChange;
			else if (velocity > maxChange)
				velocity = maxChange;

			state += velocity;

			if (state < min)
			{
				state = min;
				velocity *= -0.20F;
			}
			else if (state > max)
			{
				state = max;
				velocity *= -0.20F;
			}
			else
			{
				velocity *= 0.32F;
			}
		}

	}



	/**
	 * Pressure gauges displayed in the cab.
	 */
	@SideOnly(Side.CLIENT)
	public static class Gauge extends BufferedTexture
	{
		protected static final Font FONT_DEFAULT = new Font(Font.SANS_SERIF, Font.PLAIN, 14);
		protected static final Font FONT_MINI = new Font(Font.SANS_SERIF, Font.PLAIN, 11);

		private final String topText;
		private final String bottomText;

		private final float maximumValue;

		private Color needleAColor;
		private Color needleBColor;

		private float needleAValue = 0.0F;
		private float needleBValue = 0.0F;
		private float needleAVelocity = 0.0F;
		private float needleBVelocity = 0.0F;
		private float needleATargetValue = 0.0F;
		private float needleBTargetValue = 0.0F;

		private boolean hasNeedleB = false;


		/**
		 * Initialises a new instance of the {@link VehParCabLocomotive.Gauge} class, with a single needle.
		 *
		 * @param needleColor - The color of the needle.
		 * @param needleStartingValue - The starting value of the needle.
		 */
		@SideOnly(Side.CLIENT)
		public Gauge(String topText, String bottomText, float maximumValue, Color needleColor, float needleStartingValue)
		{
			super(160, 160);

			this.topText = topText;
			this.bottomText = bottomText;

			this.maximumValue = maximumValue;

			this.needleAValue = needleStartingValue;
			this.needleAColor = needleColor;

			hasNeedleB = false;

			repaint();
		}

		/**
		 * Initialises a new instance of the {@link VehParCabLocomotive.Gauge} class, with two needles.
		 *
		 * @param needleAColor - The colour of the 'A' needle.
		 * @param needleAStartingValue - The starting value of the 'A' needle.
		 * @param needleBColor - The color of the 'B' needle.
		 * @param needleBStartingValue - The starting value of the 'B' needle.
		 */
		@SideOnly(Side.CLIENT)
		public Gauge(String topText, String bottomText, float maximumValue, Color needleAColor, float needleAStartingValue, Color needleBColor, float needleBStartingValue)
		{
			super(160, 160);

			this.topText = topText;
			this.bottomText = bottomText;

			this.maximumValue = maximumValue;

			this.needleAValue = needleAStartingValue;
			this.needleAColor = needleAColor;

			this.needleBValue = needleBStartingValue;
			this.needleBColor = needleBColor;
			this.hasNeedleB = true;

			repaint();
		}


		@SideOnly(Side.CLIENT)
		public void repaint()
		{
			Graphics2D g = getGraphics();
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g.setBackground(new Color(0.1F, 0.1F, 0.1F));

			int centreX = getWidth() / 2;
			int centreY = getHeight() / 2;
			int maximumNotch = (int)Math.ceil(maximumValue);
			float notchRotation = 270.0F / (float)maximumNotch;

			g.clearRect(0, 0, getWidth(), getHeight());

			/*
			 * Notches
			 */
			AffineTransform transform = g.getTransform();
			{
				// Rotate to the starting angle, the first notch.
				g.rotate(ZnDMathHelper.RAD_MULTIPLIER * -135.0D, centreX, centreY);

				// Go through all notches and render them.
				for (int i = 0; i <= maximumNotch; i++)
				{
					g.setColor(Color.WHITE);
					g.fillRect(centreX - 1, centreY - 73, 2, 11);
					g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (notchRotation / 4.0F), centreX, centreY);

					for (int j = 0; j < 3; j++)
					{
						if (i < maximumNotch)
						{
							g.setColor(Color.LIGHT_GRAY);
							g.fillRect(centreX, centreY - 73, 1, 8);
							g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (notchRotation / 4.0F), centreX, centreY);
						}
					}
				}
			}
			g.setTransform(transform);

			/*
			 * Line
			 */
			// g.setColor(Color.BLACK);
			// g.drawArc(centreX - 55, centreY - 55, 110, 110, -45, 270);

			/*
			 * Numbers
			 */
			{
				g.setColor(Color.WHITE);
				g.setFont(FONT_DEFAULT);

				float rotation = -135.0F;

				for (int i = 0; i <= maximumNotch; i++)
				{
					int numberX = centreX + (int)(Math.sin(ZnDMathHelper.RAD_MULTIPLIER * rotation) * 53.0F);
					int numberY = centreX - (int)(Math.cos(ZnDMathHelper.RAD_MULTIPLIER * rotation) * 53.0F);

					AScreenRenderer.drawCenteredString(g, String.valueOf(i), numberX, numberY);

					rotation += notchRotation;
				}
			}

			/*
			 * Subtitle
			 */
			{
				g.setFont(FONT_MINI);
				g.setColor(Color.LIGHT_GRAY);
				AScreenRenderer.drawCenteredString(g, "ZnD", centreX, centreY + 45);
				AScreenRenderer.drawCenteredString(g, "x100 kPa", centreX, centreY + 60);

				// g.setFont(FONT_MINI);
				// g.setColor(Color.LIGHT_GRAY);
				//
				// if (!topText.isEmpty())
				// {
				// AScreenRenderer.drawCenteredString(g, topText, centreX, centreY + 50);
				// }
				//
				// if (!bottomText.isEmpty())
				// {f
				// AScreenRenderer.drawCenteredString(g, bottomText, centreX, centreY + 60);
				// }
			}

			/*
			 * Needle B
			 */
			if (hasNeedleB)
			{
				{
					g.setColor(needleBColor);

					/*
					 * Render the shadow
					 */
					AffineTransform t = g.getTransform();
					{
						g.translate(2.0F, 2.0F);
						g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (-135.0D + (needleBValue * notchRotation)), getWidth() / 2, getHeight() / 2);
						g.setColor(Color.BLACK);
						g.fillPolygon(new int[] { centreX, centreX + 2, centreX + 5, centreX + 9, centreX + 15, centreX, centreX - 15, centreX - 9, centreX - 5, centreX - 2 }, new int[] { centreY - 70, centreY, centreY + 10, centreY + 20, centreY + 30, centreY + 33, centreY + 30, centreY + 20, centreY + 10, centreY }, 10);
						g.fillOval(centreX - 6, centreY - 6, 12, 12);
					}
					g.setTransform(t);

					/*
					 * Render the shape of the needle.
					 */
					g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (-135.0D + (needleBValue * notchRotation)), getWidth() / 2, getHeight() / 2);
					g.setColor(needleBColor);
					g.fillPolygon(new int[] { centreX, centreX + 2, centreX + 5, centreX + 9, centreX + 15, centreX, centreX - 15, centreX - 9, centreX - 5, centreX - 2 },
								  new int[] { centreY - 70, centreY, centreY + 10, centreY + 20, centreY + 30, centreY + 33, centreY + 30, centreY + 20, centreY + 10, centreY }, 10);
					g.fillOval(centreX - 6, centreY - 6, 12, 12);

					/*
					 * Draw a darker outline of the needle to increase visibility.
					 */
					g.setColor(needleBColor.brighter());
					{
						g.drawPolygon(new int[] { centreX, centreX + 2, centreX + 5, centreX + 9, centreX + 15, centreX, centreX - 15, centreX - 0, centreX - 5, centreX - 2 },
									  new int[] { centreY - 70, centreY, centreY + 10, centreY + 20, centreY + 30, centreY + 33, centreY + 30, centreY + 20, centreY + 10, centreY }, 10);
					}

					/*
					 * Render a black 'pin' in the centre of the needle, for what it rotates on.
					 */
					g.setColor(Color.DARK_GRAY);
					g.fillOval(centreX - 2, centreY - 2, 4, 4);
				}
				g.setTransform(transform);
			}

			/*
			 * Needle A
			 */
			{
				/*
				 * Render the shadow
				 */
				AffineTransform t = g.getTransform();
				{
					g.translate(2.0F, 2.0F);
					g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (-135.0D + (needleAValue * notchRotation)), getWidth() / 2, getHeight() / 2);
					g.setColor(Color.BLACK);
					g.fillPolygon(new int[] { centreX, centreX + 2, centreX + 5, centreX + 9, centreX + 15, centreX, centreX - 15, centreX - 9, centreX - 5, centreX - 2 },
								  new int[] { centreY - 70, centreY, centreY + 10, centreY + 20, centreY + 30, centreY + 33, centreY + 30, centreY + 20, centreY + 10, centreY }, 10);
					g.fillOval(centreX - 6, centreY - 6, 12, 12);
				}
				g.setTransform(t);


				/*
				 * Render the needle's polygon shape.
				 */
				g.setColor(needleAColor);
				g.rotate(ZnDMathHelper.RAD_MULTIPLIER * (-135.0D + (needleAValue * notchRotation)), getWidth() / 2, getHeight() / 2);
				g.fillPolygon(new int[] { centreX, centreX + 2, centreX + 5, centreX + 9, centreX + 15, centreX, centreX - 15, centreX - 9, centreX - 5, centreX - 2 },
							  new int[] { centreY - 70, centreY, centreY + 10, centreY + 20, centreY + 30, centreY + 33, centreY + 30, centreY + 20, centreY + 10, centreY }, 10);
				g.fillOval(centreX - 6, centreY - 6, 12, 12);

				/*
				 * Render the needle's darker outline.
				 */
				g.setColor(needleAColor.brighter());
				{
					g.drawPolygon(new int[] { centreX, centreX + 2, centreX + 5, centreX + 9, centreX + 15, centreX, centreX - 15, centreX - 0, centreX - 5, centreX - 2 },
								  new int[] { centreY - 70, centreY, centreY + 10, centreY + 20, centreY + 30, centreY + 33, centreY + 30, centreY + 20, centreY + 10, centreY }, 10);
				}

				/*
				 * Render the needle's rotation pin in the centre.
				 */
				g.setColor(Color.DARK_GRAY);
				g.fillOval(centreX - 2, centreY - 2, 4, 4);

			}
			g.setTransform(transform);

			g.setColor(Color.WHITE);
			g.drawImage(GraphicsResources.imageScreenGlare, 0, 0, getWidth(), getHeight(), null);

			/*
			 * Reload the texture
			 */
			loadTexture();
		}


		@SideOnly(Side.CLIENT)
		public void setNeedleAValue(float newValue)
		{
			this.needleATargetValue = newValue;
		}

		public void setNeedleBValue(float newValue)
		{
			this.needleBTargetValue = newValue;
		}


		@SideOnly(Side.CLIENT)
		public void tick()
		{

			float needleAValuePrevious = needleAValue;
			float needleBValuePrevious = needleBValue;
			needleAVelocity += (needleATargetValue - needleAValue) / 3.0F;
			needleBVelocity += (needleBTargetValue - needleBValue) / 3.0F;
			needleAValue += needleAVelocity;
			needleBValue += needleBVelocity;

			if (needleAValue < -0.1F)
			{
				needleAValue = -0.1F;
				needleAVelocity = 0.0F;
			}
			else if (needleAValue > 11.0F)
			{
				needleAValue = 11.0F;
				needleAVelocity = 0.0F;
			}
			else
			{
				needleAVelocity *= 0.40F;
			}

			if (needleBValue < -0.1F)
			{
				needleBValue = -0.1F;
				needleBVelocity = 0.0F;
			}
			else if (needleBValue > 11.0F)
			{
				needleBValue = 11.0F;
				needleBVelocity = 0.0F;
			}
			else
			{
				needleBVelocity *= 0.40F;
			}

			if (needleAValuePrevious != needleAValue || needleBValuePrevious != needleBValue || getTextureID() == -1)
			{
				repaint();
			}
		}
	}
}
