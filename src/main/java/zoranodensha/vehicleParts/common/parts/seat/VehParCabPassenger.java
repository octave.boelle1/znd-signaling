package zoranodensha.vehicleParts.common.parts.seat;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.type.cab.*;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.seat.VehParRenderer_CabPassenger;
import zoranodensha.vehicleParts.common.sounds.CabSounds;

import java.awt.*;



/**
 * Modern cab with two screens, suited to passenger trains.
 */
public class VehParCabPassenger extends AVehParCabBase
{
	/**
	 * Property holding the desk's color.
	 */
	public final PropColor color_desk;

	/*
	 * Levers
	 */
	public final PropLever airBrakeController;
	public final PropLever powerBrakeController;

	public final PropLever airBrakeControllerSmooth;
	public final PropLever powerBrakeControllerSmooth;

	/*
	 * Cab Screens
	 */
	public final DDU dduL;
	public final DDU dduR;

	/**
	 * A wrapper class containing many sounds relating to the cab.
	 */
	protected CabSounds cabSounds;


	/*
	 * Cab Controls
	 */
	@SideOnly(Side.CLIENT) public CabControl airBrakeControl;
	@SideOnly(Side.CLIENT) public CabControl cabLightControl;
	@SideOnly(Side.CLIENT) public CabControl doorControl;
	@SideOnly(Side.CLIENT) public CabControl hornControl;
	@SideOnly(Side.CLIENT) public CabControl lightBeamControl;
	@SideOnly(Side.CLIENT) public CabControl tcsControl;
	@SideOnly(Side.CLIENT) public CabControl powerBrakeControl;
	@SideOnly(Side.CLIENT) public CabControl trainLightControl;
	@SideOnly(Side.CLIENT) public Gauge gaugeTop;
	@SideOnly(Side.CLIENT) public Gauge gaugeBottom;


	/*
	 * Other Junk
	 */
	/**
	 * A counter which ticks down to allow for logic that occurs on the client every 10 ticks.
	 */
	@SideOnly(Side.CLIENT) private int slowClientTick;
	@SideOnly(Side.CLIENT) public boolean indicatorLightParkBrake;
	@SideOnly(Side.CLIENT) public boolean indicatorLightStaffResponsible;


	public VehParCabPassenger()
	{
		super("VehParCabPassenger", 0.16F);

		/*
		 * Configurable Properties
		 */
		addProperty(color_desk = (PropColor)new PropColor(this, new ColorRGBA(0.9F, 0.9F, 0.9F), "color_desk").setConfigurable());

		/*
		 * Screens
		 */
		dduL = new DDU(type_cab.mtms, "left", EDDUMenu.MENU_OPERATION);
		dduR = new DDU(type_cab.mtms, "right", EDDUMenu.MENU_INFORMATION);

		/*
		 * Un-animated Controllers
		 */
		addProperty(airBrakeController = (PropLever)new PropLever(type_cab, "airBrakeController", 8, PropLever.DEFAULT_LEVER_SPEED).setSyncDir(ESyncDir.CLIENT));
		addProperty(powerBrakeController = (PropLever)new PropLever(type_cab, "masterController", 6, PropLever.DEFAULT_LEVER_SPEED, true).setSyncDir(ESyncDir.CLIENT));

		/*
		 * Animated Controllers
		 */
		addProperty(airBrakeControllerSmooth = (PropLever)new PropLever(type_cab, "airBrakeControllerSmooth", 8, PropLever.DEFAULT_LEVER_SPEED).setSyncDir(ESyncDir.SERVER));
		addProperty(powerBrakeControllerSmooth = (PropLever)new PropLever(type_cab, "masterControllerSmooth", 6, PropLever.DEFAULT_LEVER_SPEED, true).setSyncDir(ESyncDir.SERVER));

		/*
		 * Initialise the cab sounds wrapper.
		 */
		cabSounds = new CabSounds(this);

		/*
		 * Initialise button positions in helper method.
		 */
		initButtonPositions();
	}


	/**
	 * Helper method used to initialise all clickable button positions.
	 */
	private void initButtonPositions()
	{
		//@formatter:off

		/*
		 * posX		Forward/Backward (Blender +X), Add 0.4 to get MC value
		 * posY		Up/Down			 (Blender +Z), Minus 0.08 to get MC value
		 * posZ		Left/Right		 (Blender -Y), Same
		 */

		/* Mouse Control Coordinates */
		type_cab.button_horn.setPositionAndSize(posX(-0.0725), posY(0.0340), posZ(0.3980), PropButton.EButtonSize.MEDIUM);
		type_cab.button_alerter.setPositionAndSize(posX(-0.0890), posY(0.0340), posZ(0.3675), PropButton.EButtonSize.MEDIUM);
		type_cab.button_lights_beams.setPositionAndSize(posX(-0.1410), posY(0.0340), posZ(0.2710), PropButton.EButtonSize.MEDIUM);
		type_cab.button_lights_cab.setPositionAndSize(posX(-0.1575), posY(0.0340), posZ(0.2405), PropButton.EButtonSize.MEDIUM);
		type_cab.button_lights_train.setPositionAndSize(posX(-0.1740), posY(0.0340), posZ(0.2100), PropButton.EButtonSize.MEDIUM);

		/* Reverser Buttons */
		// TODO @Jaffa - Reimplement these
		//		type_cab.cabButton_reverser_backward.setPositionAndSize(posX(-0.1450), posY(0.0340), posZ(0.1400), PropButton.EButtonSize.MEDIUM);
		//		type_cab.cabButton_reverser_neutral.setPositionAndSize(posX(-0.1700), posY(0.0340), posZ(0.1400), PropButton.EButtonSize.MEDIUM);
		//		type_cab.cabButton_reverser_forward.setPositionAndSize(posX(-0.1950), posY(0.0340), posZ(0.1400), PropButton.EButtonSize.MEDIUM);

		/* Train Lights and Door Buttons */
		//		button_lights_headlights.setPositionAndSize(		posX(-0.1615), posY( 0.0340), posZ(-0.2333), EButtonSize.MEDIUM);
		type_cab.button_doors_left.setPositionAndSize(posX(0.0350), posY(0.0340), posZ(-0.4100), PropButton.EButtonSize.MEDIUM);
		type_cab.button_doors_right.setPositionAndSize(posX(0.0350), posY(0.0340), posZ(-0.4650), PropButton.EButtonSize.MEDIUM);

		/* Breakers and Switches */
//		type_cab.button_brakeMode.setPositionAndSize(posX(-0.1932), posY(0.0340), posZ(-0.1745), PropButton.EButtonSize.MEDIUM);
		type_cab.button_parkBrake_apply.setPositionAndSize(posX(-0.0950), posY(0.0342), posZ(-0.3659), PropButton.EButtonSize.SMALL);
		type_cab.button_parkBrake_release.setPositionAndSize(posX(-0.0900), posY(0.0342), posZ(-0.3923), PropButton.EButtonSize.SMALL);

		/* Left DDU Buttons (Left Side) */
		dduL.screenButton_brightness.setPositionAndSize(posX(-0.2600), posY(0.1650), posZ(0.1120), PropButton.EButtonSize.MEDIUM);
		dduL.screenButton_contrast.setPositionAndSize(posX(-0.2500), posY(0.1465), posZ(0.1120), PropButton.EButtonSize.MEDIUM);
		dduL.screenButton_swap.setPositionAndSize(posX(-0.2325), posY(0.0905), posZ(0.1120), PropButton.EButtonSize.MEDIUM);
		dduL.screenButton_menu.setPositionAndSize(posX(-0.2250), posY(0.0715), posZ(0.1120), PropButton.EButtonSize.MEDIUM);
		/* Left DDU Buttons (Right Side) */
		dduL.screenButton_left.setPositionAndSize(posX(-0.2600), posY(0.1650), posZ(-0.1120), PropButton.EButtonSize.MEDIUM);
		dduL.screenButton_right.setPositionAndSize(posX(-0.2500), posY(0.1465), posZ(-0.1120), PropButton.EButtonSize.MEDIUM);
		dduL.screenButton_up.setPositionAndSize(posX(-0.2465), posY(0.1277), posZ(-0.1120), PropButton.EButtonSize.MEDIUM);
		dduL.screenButton_down.setPositionAndSize(posX(-0.2395), posY(0.1095), posZ(-0.1120), PropButton.EButtonSize.MEDIUM);
		dduL.screenButton_enter.setPositionAndSize(posX(-0.2285), posY(0.0800), posZ(-0.1120), PropButton.EButtonSize.MEDIUM);

		/* Right DDU Buttons (Left Side) */
		dduR.screenButton_brightness.setPositionAndSize(posX(-0.2490), posY(0.1680), posZ(-0.2230), PropButton.EButtonSize.MEDIUM);
		dduR.screenButton_contrast.setPositionAndSize(posX(-0.2425), posY(0.1495), posZ(-0.2200), PropButton.EButtonSize.MEDIUM);
		dduR.screenButton_swap.setPositionAndSize(posX(-0.2185), posY(0.0905), posZ(-0.2080), PropButton.EButtonSize.MEDIUM);
		dduR.screenButton_menu.setPositionAndSize(posX(-0.2113), posY(0.0710), posZ(-0.2050), PropButton.EButtonSize.MEDIUM);
		/* Right DDU Buttons (Right Side) */
		dduR.screenButton_left.setPositionAndSize(posX(-0.1380), posY(0.1650), posZ(-0.4215), PropButton.EButtonSize.MEDIUM);
		dduR.screenButton_right.setPositionAndSize(posX(-0.1350), posY(0.1475), posZ(-0.4175), PropButton.EButtonSize.MEDIUM);
		dduR.screenButton_up.setPositionAndSize(posX(-0.1280), posY(0.1310), posZ(-0.4135), PropButton.EButtonSize.MEDIUM);
		dduR.screenButton_down.setPositionAndSize(posX(-0.1210), posY(0.1125), posZ(-0.4095), PropButton.EButtonSize.MEDIUM);
		dduR.screenButton_enter.setPositionAndSize(posX(-0.1080), posY(0.0805), posZ(-0.4035), PropButton.EButtonSize.MEDIUM);

		//@formatter:on
	}


	/**
	 * <p>Helper method to get the Entity riding in this cab's seat. May or may not be a player. May return {@code null} if there is no passenger.</p>
	 *
	 * @return - An {@link net.minecraft.entity.Entity} instance if there is a passenger, or else {@code null}.
	 */
	private Entity getPassenger()
	{
		if (type_cab == null)
		{
			return null;
		}

		return type_cab.getPassenger();
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		if (renderer == null)
		{
			renderer = new VehParRenderer_CabPassenger(this);
		}

		return renderer;
	}

	@Override
	public void initControls()
	{
		/* Button and lever controls */
		float throttle = powerBrakeControllerSmooth.get();
		float airBrake = airBrakeControllerSmooth.get();

		airBrakeControl = new CabControl(-30.0F, 30.0F, 1.15F, 6.0F, (airBrake > 0.0F ? 30.0F + (airBrake * -60.0F) : 30.0F));
		cabLightControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_lights_cab.isActive() ? 20.0F : 0.0F);
		doorControl = new CabControl(-25.0F, 25.0F, 2.0F, 8.0F, 0.0F);
		// headlightControl = new CabControl(-5.0F, 365.0F, 1.40F, 60.0F, ((type_cab.lightMode.get().ordinal() + 5) * 90.0F % 360.0F));
		hornControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_horn.isActive() ? 20.0F : 0.0F);
		lightBeamControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_lights_beams.isActive() ? 20.0F : 0.0F);
		tcsControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_alerter.isActive() ? 20.0F : 0.0F);
		powerBrakeControl = new CabControl(-30.0F, 30.0F, 1.15F, 6.0F, throttle * 60.0F);
		trainLightControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_lights_train.isActive() ? 20.0F : 0.0F);

		gaugeTop = new Gauge("bogie 1 cyl", "bogie 2 cyl", 5.0F, Color.RED, 0.0F, Color.YELLOW, 0.0F);
		gaugeBottom = new Gauge("brake pipe", "main reservoir", 10.0F, Color.RED, 0.0F, Color.YELLOW, 0.0F);
	}


	@Override
	public void onPropertyChanged(IPartProperty<?> property)
	{
		super.onPropertyChanged(property);

		/*
		 * This section handles client-side button changes.
		 */
		if (getTrain() == null)
		{
			return;
		}

		if (!getTrain().worldObj.isRemote)
		{
			return;
		}

		if (property instanceof PropButton)
		{
			PropButton button = (PropButton)property;

			/*
			 * DDU Buttons that beep.
			 */
			if (property instanceof PropButton.PropButtonScreen)
			{
				if (getEntityIsPlayer() && !isReadingFromNBT)
				{
					if (button.isActive())
					{
						type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_beep", 1.0F, 1.0F);
					}
				}
			}

			/* Alerter */
			else if (button == type_cab.button_alerter)
			{
				if (tcsControl != null)
				{
					tcsControl.setTarget(button.isActive() ? 20.0F : 0.0F);
				}
				if (button.isActive() && !isReadingFromNBT)
				{
					playSwitchSound(type_cab.button_alerter.isActive());
				}
			}

			/*
			 * Cruise Control Toggle
			 */
			else if (button == type_cab.button_afb_toggle)
			{
				if (getEntityIsPlayer() && !isReadingFromNBT)
				{
					type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_cruise3", 1.0F, 1.0F);
				}
			}

			/* Doors left */
			else if (button == type_cab.button_doors_left)
			{
				if (doorControl != null)
				{
					doorControl.setTarget(button.isActive() ? 20.0F : -20.0F);
				}
				if (getEntityIsPlayer() && !isReadingFromNBT)
				{
					type_cab.getPassenger().playSound(ModData.ID + (type_cab.button_doors_left.get() ? ":vehPar_cab_switch1" : ":vehPar_cab_switch0"), 1.0F, 1.0F);
				}
			}

			/* Doors right */
			else if (button == type_cab.button_doors_right)
			{
				if (doorControl != null)
				{
					doorControl.setTarget(button.isActive() ? 20.0F : -20.0F);
				}
				if (getEntityIsPlayer() && !isReadingFromNBT)
				{
					type_cab.getPassenger().playSound(ModData.ID + (type_cab.button_doors_right.get() ? ":vehPar_cab_switch1" : ":vehPar_cab_switch0"), 1.0F, 1.0F);
				}
			}

			/* Horn */
			else if (button == type_cab.button_horn)
			{
				if (hornControl != null)
				{
					hornControl.setTarget(button.isActive() ? 20.0F : 0.0F);
				}
				if (button.isActive() && !isReadingFromNBT)
				{
					playSwitchSound(type_cab.button_horn.isActive());
				}
			}

			/* Lights (beams) */
			else if (button == type_cab.button_lights_beams)
			{
				if (lightBeamControl != null)
				{
					lightBeamControl.setTarget(type_cab.button_lights_beams.isActive() ? 20.0F : 0.0F);
				}
				if (!isReadingFromNBT)
				{
					playSwitchSound(type_cab.button_lights_beams.isActive());
				}
			}

			/* Lights (cab) */
			else if (button == type_cab.button_lights_cab)
			{
				if (cabLightControl != null)
				{
					cabLightControl.setTarget(type_cab.button_lights_cab.isActive() ? 20.0F : 0.0F);
				}
				if (!isReadingFromNBT)
				{
					playSwitchSound(type_cab.button_lights_cab.isActive());
				}
			}

			/* Lights (train) */
			else if (button == type_cab.button_lights_train)
			{
				if (trainLightControl != null)
				{
					trainLightControl.setTarget(type_cab.button_lights_train.isActive() ? 20.0F : 0.0F);
				}
				if (getEntityIsPlayer() && !isReadingFromNBT)
				{
					playSwitchSound(type_cab.button_lights_train.isActive());
				}
			}

			/* Park Brake Apply Button */
			else if (button == type_cab.button_parkBrake_apply)
			{
				if (!isReadingFromNBT)
				{
					if (getEntityIsPlayer() && type_cab.button_parkBrake_apply.isActive())
					{
						type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_button", 1.0F, 1.0F);
					}
				}
			}

			/* Park Brake Release Button */
			else if (button == type_cab.button_parkBrake_release)
			{
				if (!isReadingFromNBT)
				{
					if (getEntityIsPlayer() && type_cab.button_parkBrake_release.isActive())
					{
						type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_button", 1.0F, 1.0F);
					}
				}
			}

			/* Controls */
			if (button == type_cab.button_brake_decrease)
			{
				airBrakeControllerSmooth.decreasing = button.isActive();
			}
			else if (button == type_cab.button_brake_increase)
			{
				airBrakeControllerSmooth.increasing = button.isActive();
			}
			else if (button == type_cab.button_throttle_decrease)
			{
				powerBrakeControllerSmooth.decreasing = button.isActive();
			}
			else if (button == type_cab.button_throttle_increase)
			{
				powerBrakeControllerSmooth.increasing = button.isActive();
			}
		}

		/*
		 * Other properties (Not buttons)
		 */
		else
		{
			/* Reverser */
			if (property == type_cab.reverser)
			{
				/* Play sound to player if applicable. */
				if (getEntityIsPlayer() && !isReadingFromNBT)
				{
					int reverserIndex;
					switch (type_cab.getReverserState())
					{
						default:
							reverserIndex = 0;
							break;

						case FORWARD:
							reverserIndex = 1;
							break;

						case BACKWARD:
							reverserIndex = 2;
							break;

						case NEUTRAL_BACKWARD:
						case NEUTRAL_FORWARD:
							reverserIndex = 3;
							break;
					}

					type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_reverser" + reverserIndex, 1.0F, 1.0F);
				}
			}

			/* Air Brake */
			else if (property == airBrakeController)
			{
				if (airBrakeControl != null)
				{
					float brake = airBrakeController.get();
					airBrakeControl.setTarget(brake != 0.0F ? 30.0F + (brake * -60.0F) : 30.0F);
				}
			}

			/* Throttle */
			else if (property == powerBrakeController)
			{
				if (powerBrakeControl != null)
				{
					float masterControllerValue = powerBrakeController.get();
					powerBrakeControl.setTarget(masterControllerValue * 30.0F);
				}
			}
		}
	}

	@Override
	public void tick()
	{
		super.tick();

		/*
		 * Update cab air sounds, etc
		 */
		if (cabSounds != null)
		{
			cabSounds.onUpdate();
		}

		/*
		 * Tick DDUs
		 */
		dduL.onUpdate();
		dduR.onUpdate();

		/*
		 * Tick client/server-only methods.
		 */
		Train train = getTrain();
		if (train.worldObj.isRemote)
		{
			if(getEntityIsPlayer())
			{
				tickClientOnly();
			}
		}
		else
		{
			tickServerOnly();
		}
	}


	private void tickClientOnly()
	{
		/*
		 * Tick the controls and play appropriate sound effects.
		 */
		Integer result;
		{
			/*
			 * Air Brake Controller
			 */
			result = airBrakeControllerSmooth.tick(true);
			if (result != null && getPassenger() != null)
			{
				if (airBrakeControllerSmooth.increasing)
				{
					switch (result)
					{
						case 1:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_brake0", 1.0F, 1.0F);
							break;
						}

						case 2:
						case 3:
						case 4:
						case 5:
						case 6:
						case 7:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_brake1", 1.0F, 1.0F);
							break;
						}

						case 8:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_brake2", 1.0F, 1.0F);
							break;
						}
					}
				}
				else if (airBrakeControllerSmooth.decreasing)
				{
					switch (result)
					{
						case 0:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_brake5", 1.0F, 1.0F);
							break;
						}

						case 1:
						case 2:
						case 3:
						case 4:
						case 5:
						case 6:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_brake4", 1.0F, 1.0F);
							break;
						}

						case 7:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_brake3", 1.0F, 1.0F);
							break;
						}
					}
				}
			}


			/*
			 * Master Controller (Combined power/brake lever)
			 */
			result = powerBrakeControllerSmooth.tick(true);
			if(result != null && getPassenger() != null)
			{
				if(powerBrakeControllerSmooth.increasing)
				{
					switch(result)
					{
						case -5:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cabPassenger_throttle5", 1.0F, 1.0F);
							break;
						}

						case 0:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cabPassenger_throttle2", 1.0F, 1.0F);
							break;
						}

						case 1:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cabPassenger_throttle0", 1.0F, 1.0F);
							break;
						}

						case 6:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cabPassenger_throttle1", 1.0F, 1.0F);
							break;
						}
					}
				}
				else if(powerBrakeControllerSmooth.decreasing)
				{
					switch(result)
					{
						case 0:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cabPassenger_throttle2", 1.0F, 1.0F);
							break;
						}

						case -1:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cabPassenger_throttle0", 1.0F, 1.0F);
							break;
						}

						case -5:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cabPassenger_throttle3", 1.0F, 1.0F);
							break;
						}

						case -6:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cabPassenger_throttle4", 1.0F, 1.0F);

							if(type_cab.brakeValve.getPressure() >= 250.0F)
							{
								getPassenger().playSound(ModData.ID + ":vehPar_cab_5", 1.0F, 1.0F);
							}
							break;
						}
					}
				}
			}
		}


		/*
		 * Create control and gauge instances if necessary.
		 */
		if(powerBrakeControl == null || gaugeTop == null)
		{
			initControls();
		}

		/*
		 * Update animated controls
		 */
		airBrakeControl.setTarget(30.0F + (airBrakeControllerSmooth.get() * -60.0F));
		airBrakeControl.tick();
		cabLightControl.tick();
		doorControl.tick();
		hornControl.tick();
		lightBeamControl.tick();
		tcsControl.tick();
		powerBrakeControl.setTarget(powerBrakeControllerSmooth.get() * 30.0F);
		powerBrakeControl.tick();
		trainLightControl.tick();


		/* Update gauges. */
		float parkBrakeCylinderPressure = 0.0F;
		{
			float auxReservoirPressure = 0.0F;
			float brakeCylinderPressure = 0.0F;
			float brakeCylinderPressureOther = 0.0F;
			float brakePipePressure = 0.0F;
			float mainReservoirPipePressure = 0.0F;

			/*
			 * Get the bogie underneath this cab.
			 */
			OrderedTypesList<PartTypeBogie> bogies = type_cab.getVehicle().getTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
			PartTypeBogie bogie = bogies.get(0);
			PartTypeBogie bogieNext = bogies.get(bogies.size() - 1);
			if (bogie != null)
			{
				/* Brake Cylinders (for each bogie) */
				brakeCylinderPressure = bogie.pneumaticsBrakeCylinder.getPressure();
				if (bogieNext != null)
				{
					brakeCylinderPressureOther = bogieNext.pneumaticsBrakeCylinder.getPressure();
				}
				else
				{
					brakeCylinderPressureOther = brakeCylinderPressure;
				}

				/* Main Reservoir */
				mainReservoirPipePressure = bogie.pneumaticsMainReservoirPipe.getPressure();

				/* Park Brake */
				parkBrakeCylinderPressure = bogie.pneumaticsParkBrakeCylinder.getPressure();
			}


			gaugeTop.setNeedleAValue(brakeCylinderPressure / 100.0F);
			gaugeTop.setNeedleBValue(brakeCylinderPressureOther / 100.0F);
			gaugeTop.tick();

			gaugeBottom.setNeedleAValue(this.type_cab.brakeValve.getPressure() / 100.0F);
			gaugeBottom.setNeedleBValue(mainReservoirPipePressure / 100.0F);
			gaugeBottom.tick();
		}

		/*
		 * Slow Client Tick
		 */
		slowClientTick--;
		if (slowClientTick <= 0)
		{
			slowClientTick = 10;

			/*
			 * Update the park brake indicator light.
			 */
			boolean indicatorLightParkBrakeOld = indicatorLightParkBrake;
			indicatorLightParkBrake = parkBrakeCylinderPressure < 275.0F;
			if (indicatorLightParkBrake != indicatorLightParkBrakeOld)
			{
				this.updateVertexState = true;
			}

			/*
			 * Update the staff-responsible indicator light.
			 */
			boolean indicatorLightStaffResponsibleOld = indicatorLightStaffResponsible;
			indicatorLightStaffResponsible = type_cab.mtms.operationMode.get() != EMTMSOperationMode.FULL_SUPERVISION;
			{
				if (indicatorLightStaffResponsible != indicatorLightStaffResponsibleOld)
				{
					this.updateVertexState = true;
				}
			}
		}
	}


	private void tickServerOnly()
	{
		/*
		 * Update the server-side control positions based on changes to the client-side smooth levers.
		 */
		/*
		 * Air Brake Controller
		 */
		if (airBrakeController.getCurrentNotch() != airBrakeControllerSmooth.getCurrentNotch())
		{
			airBrakeController.setNotch(airBrakeControllerSmooth.getCurrentNotch());
		}

		/*
		 * Power/Brake Controller
		 */
		if (powerBrakeController.getCurrentNotch() != powerBrakeControllerSmooth.getCurrentNotch())
		{
			powerBrakeController.setNotch(powerBrakeControllerSmooth.getCurrentNotch());
		}
	}


	@Override
	protected void updatePartTypeControls()
	{
		if(powerBrakeController.get() > 0.0F)
		{
			type_cab.controlPower.set(powerBrakeController.get());
		}

		if(powerBrakeController.get() < 0.0F)
		{
			type_cab.controlBrakeElectroPneumatic.set(Math.abs(powerBrakeController.get()));
			type_cab.controlBrakeDynamic.set(Math.abs(powerBrakeController.get()));
		}

		type_cab.controlBrakePneumatic.set(airBrakeController.get());

		if(powerBrakeController.get() <= -1.0F || airBrakeController.get() >= 1.0F)
		{
			type_cab.controlBrakeEmergency.set(true);
		}

		type_cab.controlCruiseControl.set(0.0F);

		type_cab.mtms.cabPowerLevel.set(Math.max(0.0F, powerBrakeController.get()));
		type_cab.mtms.cabBrakeLevel.set(Math.max(0.0F, powerBrakeController.get() * -1.0F));
	}


	/**
	 * <p>
	 * Helper method that plays a switch sound to the player in this cab.
	 * </p>
	 * <p>
	 * <i>Client-side only.</i>
	 * </p>
	 */
	@SideOnly(Side.CLIENT)
	private void playSwitchSound(boolean switchState)
	{
		if (getEntityIsPlayer())
		{
			type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_switch" + (switchState ? 1 : 0), 1.0F, 1.0F);
		}
	}


	private static float posX(double x)
	{
		return (float)-((x - 0.4) * 0.8 - 0.075);
	}

	private static float posY(double y)
	{
		return (float)((y + 0.075) * 0.8);
	}

	private static float posZ(double z)
	{
		return (float)-(z * 0.8);
	}

}
