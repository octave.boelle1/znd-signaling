package zoranodensha.vehicleParts.common.parts.seat;

import java.awt.Color;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import org.apache.logging.log4j.Level;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.EValidTagCalls;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.type.cab.*;
import zoranodensha.api.vehicles.part.type.cab.PropButton.PropButtonScreen;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.client.render.seat.VehParRenderer_CabLocomotive;
import zoranodensha.vehicleParts.common.sounds.CabSounds;



/**
 * Modern cab with two screens, suited to freight locomotives and long-distance passenger trains.
 */
public class VehParCabLocomotive extends AVehParCabBase
{
	/**
	 * Property holding the desk's color.
	 */
	public final PropColor color_desk;

	/*
	 * Levers
	 */
	public final PropLever afbController;
	public final PropLever airBrakeController;
	public final PropLever dynamicBrakeController;
	public final PropLever independentBrakeController;
	public final PropLever powerController;

	public PropLever afbControllerSmooth;
	public final PropLever airBrakeControllerSmooth;
	public final PropLever dynamicBrakeControllerSmooth;
	public final PropLever independentBrakeControllerSmooth;
	public final PropLever powerControllerSmooth;

	public final PropFloat independentBrakeLevel;

	/*
	 * Cab Screens
	 */
	public final DDU dduL;
	public final DDU dduR;

	/**
	 * A wrapper class containing many sounds relating to the cab.
	 */
	protected CabSounds cabSounds;


	/*
	 * Cab Controls
	 */
	@SideOnly(Side.CLIENT) public CabControl afbControl;
	@SideOnly(Side.CLIENT) public CabControl airBrakeControl;
	@SideOnly(Side.CLIENT) public CabControl brakeModeControl;
	@SideOnly(Side.CLIENT) public CabControl cabLightControl;
	@SideOnly(Side.CLIENT) public CabControl doorControl;
	@SideOnly(Side.CLIENT) public CabControl dynamicBrakeControl;
	@SideOnly(Side.CLIENT) public CabControl hornControl;
	@SideOnly(Side.CLIENT) public CabControl independentBrakeControl;
	@SideOnly(Side.CLIENT) public CabControl lightBeamControl;
	@SideOnly(Side.CLIENT) public CabControl tcsControl;
	@SideOnly(Side.CLIENT) public CabControl throttleControl;
	@SideOnly(Side.CLIENT) public CabControl trainLightControl;
	@SideOnly(Side.CLIENT) public Gauge gaugeTop;
	@SideOnly(Side.CLIENT) public Gauge gaugeBottom;


	/*
	 * Other Junk
	 */
	/**
	 * A counter which ticks down to allow for logic that occurs on the client every 10 ticks.
	 */
	@SideOnly(Side.CLIENT) private int slowClientTick;
	@SideOnly(Side.CLIENT) public boolean indicatorLightParkBrake;
	@SideOnly(Side.CLIENT) public boolean indicatorLightStaffResponsible;


	public VehParCabLocomotive()
	{
		super("VehParCab", 0.16F);

		/*
		 * Configurable Properties
		 */
		addProperty(color_desk = (PropColor)new PropColor(this, new ColorRGBA(0.9F, 0.9F, 0.9F), "color_desk").setConfigurable());

		/*
		 * Screens
		 */
		dduL = new DDU(type_cab.mtms, "left", EDDUMenu.MENU_OPERATION);
		dduR = new DDU(type_cab.mtms, "right", EDDUMenu.MENU_INFORMATION);

		/*
		 * Un-animated Controllers
		 */
		addProperty(afbController = new PropLever(type_cab, "afbController", 32, 0.008F));
		addProperty(airBrakeController = (PropLever)new PropLever(type_cab, "airBrake", 9, PropLever.DEFAULT_LEVER_SPEED).setSyncDir(ESyncDir.CLIENT));
		addProperty(dynamicBrakeController = (PropLever)new PropLever(type_cab, "dynamicBrake", 9, PropLever.DEFAULT_LEVER_SPEED).setSyncDir(ESyncDir.CLIENT));
		addProperty(independentBrakeController = (PropLever)new PropLever(type_cab, "independentBrake", 2, PropLever.DEFAULT_LEVER_SPEED * 2.0F).setSyncDir(ESyncDir.CLIENT));
		addProperty(powerController = (PropLever)new PropLever(type_cab, "throttle", 8, PropLever.DEFAULT_LEVER_SPEED).setSyncDir(ESyncDir.CLIENT));

		/*
		 * Animated Controllers
		 */
		addProperty(afbControllerSmooth = (PropLever)new PropLever(type_cab, "afbSmooth", 32, 0.008F).setSyncDir(ESyncDir.SERVER));
		addProperty(airBrakeControllerSmooth = (PropLever)new PropLever(type_cab, "airBrakeSmooth", 9, PropLever.DEFAULT_LEVER_SPEED).setSyncDir(ESyncDir.SERVER));
		addProperty(dynamicBrakeControllerSmooth = (PropLever)new PropLever(type_cab, "dynamicBrakeSmooth", 9, PropLever.DEFAULT_LEVER_SPEED).setSyncDir(ESyncDir.SERVER));
		addProperty(independentBrakeControllerSmooth = (PropLever)new PropLever(type_cab, "independentBrakeSmooth", 2, PropLever.DEFAULT_LEVER_SPEED * 2.0F).setSyncDir(ESyncDir.SERVER));
		addProperty(powerControllerSmooth = (PropLever)new PropLever(type_cab, "throttleSmooth", 8, PropLever.DEFAULT_LEVER_SPEED).setSyncDir(ESyncDir.SERVER));

		addProperty(independentBrakeLevel = (PropFloat.PropFloatBounded)new PropFloat.PropFloatBounded(this, 0.0F, 0.0F, 1.0F, "independentBrakeLevel").setValidTagCalls(EValidTagCalls.PACKET_SAVE));

		/*
		 * Initialise the cab sounds wrapper.
		 */
		cabSounds = new CabSounds(this);

		/*
		 * Initialise button positions in helper method.
		 */
		initButtonPositions();
	}

	/**
	 * Helper method used to initialise all button positions.
	 */
	private void initButtonPositions()
	{
		//@formatter:off

		/*
		 * posX		Forward/Backward (Blender +X), Add 0.4 to get MC value
		 * posY		Up/Down			 (Blender +Z), Minus 0.08 to get MC value
		 * posZ		Left/Right		 (Blender -Y), Same
		 */

		/* Mouse Control Coordinates */
		type_cab.button_horn.setPositionAndSize(posX(-0.0725), posY(0.0340), posZ(0.3980), PropButton.EButtonSize.MEDIUM);
		type_cab.button_alerter.setPositionAndSize(posX(-0.0890), posY(0.0340), posZ(0.3675), PropButton.EButtonSize.MEDIUM);
		type_cab.button_lights_beams.setPositionAndSize(posX(-0.1410), posY(0.0340), posZ(0.2710), PropButton.EButtonSize.MEDIUM);
		type_cab.button_lights_cab.setPositionAndSize(posX(-0.1575), posY(0.0340), posZ(0.2405), PropButton.EButtonSize.MEDIUM);
		type_cab.button_lights_train.setPositionAndSize(posX(-0.1740), posY(0.0340), posZ(0.2100), PropButton.EButtonSize.MEDIUM);

		/* Reverser Buttons */
		// TODO @Jaffa - Reimplement these
//		type_cab.cabButton_reverser_backward.setPositionAndSize(posX(-0.1450), posY(0.0340), posZ(0.1400), PropButton.EButtonSize.MEDIUM);
//		type_cab.cabButton_reverser_neutral.setPositionAndSize(posX(-0.1700), posY(0.0340), posZ(0.1400), PropButton.EButtonSize.MEDIUM);
//		type_cab.cabButton_reverser_forward.setPositionAndSize(posX(-0.1950), posY(0.0340), posZ(0.1400), PropButton.EButtonSize.MEDIUM);

		/* Train Lights and Door Buttons */
		//		button_lights_headlights.setPositionAndSize(		posX(-0.1615), posY( 0.0340), posZ(-0.2333), EButtonSize.MEDIUM);
		type_cab.button_doors_left.setPositionAndSize(posX(0.0350), posY(0.0340), posZ(-0.4100), PropButton.EButtonSize.MEDIUM);
		type_cab.button_doors_right.setPositionAndSize(posX(0.0350), posY(0.0340), posZ(-0.4650), PropButton.EButtonSize.MEDIUM);

		/* Breakers and Switches */
		type_cab.button_brakeMode.setPositionAndSize(posX(-0.1932), posY(0.0340), posZ(-0.1745), PropButton.EButtonSize.MEDIUM);
		type_cab.button_parkBrake_apply.setPositionAndSize(posX(-0.0950), posY(0.0342), posZ(-0.3659), PropButton.EButtonSize.SMALL);
		type_cab.button_parkBrake_release.setPositionAndSize(posX(-0.0900), posY(0.0342), posZ(-0.3923), PropButton.EButtonSize.SMALL);

		/* Left DDU Buttons (Left Side) */
		dduL.screenButton_brightness.setPositionAndSize(posX(-0.2600), posY(0.1650), posZ(0.1120), PropButton.EButtonSize.MEDIUM);
		dduL.screenButton_contrast.setPositionAndSize(posX(-0.2500), posY(0.1465), posZ(0.1120), PropButton.EButtonSize.MEDIUM);
		dduL.screenButton_swap.setPositionAndSize(posX(-0.2325), posY(0.0905), posZ(0.1120), PropButton.EButtonSize.MEDIUM);
		dduL.screenButton_menu.setPositionAndSize(posX(-0.2250), posY(0.0715), posZ(0.1120), PropButton.EButtonSize.MEDIUM);
		/* Left DDU Buttons (Right Side) */
		dduL.screenButton_left.setPositionAndSize(posX(-0.2600), posY(0.1650), posZ(-0.1120), PropButton.EButtonSize.MEDIUM);
		dduL.screenButton_right.setPositionAndSize(posX(-0.2500), posY(0.1465), posZ(-0.1120), PropButton.EButtonSize.MEDIUM);
		dduL.screenButton_up.setPositionAndSize(posX(-0.2465), posY(0.1277), posZ(-0.1120), PropButton.EButtonSize.MEDIUM);
		dduL.screenButton_down.setPositionAndSize(posX(-0.2395), posY(0.1095), posZ(-0.1120), PropButton.EButtonSize.MEDIUM);
		dduL.screenButton_enter.setPositionAndSize(posX(-0.2285), posY(0.0800), posZ(-0.1120), PropButton.EButtonSize.MEDIUM);

		/* Right DDU Buttons (Left Side) */
		dduR.screenButton_brightness.setPositionAndSize(posX(-0.2490), posY(0.1680), posZ(-0.2230), PropButton.EButtonSize.MEDIUM);
		dduR.screenButton_contrast.setPositionAndSize(posX(-0.2425), posY(0.1495), posZ(-0.2200), PropButton.EButtonSize.MEDIUM);
		dduR.screenButton_swap.setPositionAndSize(posX(-0.2185), posY(0.0905), posZ(-0.2080), PropButton.EButtonSize.MEDIUM);
		dduR.screenButton_menu.setPositionAndSize(posX(-0.2113), posY(0.0710), posZ(-0.2050), PropButton.EButtonSize.MEDIUM);
		/* Right DDU Buttons (Right Side) */
		dduR.screenButton_left.setPositionAndSize(posX(-0.1380), posY(0.1650), posZ(-0.4215), PropButton.EButtonSize.MEDIUM);
		dduR.screenButton_right.setPositionAndSize(posX(-0.1350), posY(0.1475), posZ(-0.4175), PropButton.EButtonSize.MEDIUM);
		dduR.screenButton_up.setPositionAndSize(posX(-0.1280), posY(0.1310), posZ(-0.4135), PropButton.EButtonSize.MEDIUM);
		dduR.screenButton_down.setPositionAndSize(posX(-0.1210), posY(0.1125), posZ(-0.4095), PropButton.EButtonSize.MEDIUM);
		dduR.screenButton_enter.setPositionAndSize(posX(-0.1080), posY(0.0805), posZ(-0.4035), PropButton.EButtonSize.MEDIUM);

		//@formatter:on
	}


	/**
	 * <p>Helper method to get the Entity riding in this cab's seat. May or may not be a player. May return {@code null} if there is no passenger.</p>
	 *
	 * @return - An {@link net.minecraft.entity.Entity} instance if there is a passenger, or else {@code null}.
	 */
	private Entity getPassenger()
	{
		if (type_cab == null)
		{
			return null;
		}

		return type_cab.getPassenger();
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		if (renderer == null)
		{
			renderer = new VehParRenderer_CabLocomotive(this);
		}
		return renderer;
	}


	/**
	 * Helper method to initialise all client-only cab controls.
	 */
	@SideOnly(Side.CLIENT)
	public void initControls()
	{
		/* Button and lever controls */
		float throttle = powerControllerSmooth.get();
		float airBrake = airBrakeControllerSmooth.get();
		float dynamicBrake = dynamicBrakeControllerSmooth.get();
		float independentBrake = independentBrakeControllerSmooth.get();

		afbControl = new CabControl(-30.0F, 30.0F, 1.30F, 8.0F, 0.0F);
		airBrakeControl = new CabControl(-30.0F, 30.0F, 1.15F, 6.0F, (airBrake > 0.0F ? 30.0F + (airBrake * -60.0F) : 30.0F));
		brakeModeControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, (type_cab.button_brakeMode.get() ? 20.0F : 0.0F));
		cabLightControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_lights_cab.isActive() ? 20.0F : 0.0F);
		doorControl = new CabControl(-25.0F, 25.0F, 2.0F, 8.0F, 0.0F);
		dynamicBrakeControl = new CabControl(-30.0F, 30.0F, 1.15F, 6.0F, (dynamicBrake > 0.0F ? 30.0F + (dynamicBrake * -60.0F) : 30.0F));
		// headlightControl = new CabControl(-5.0F, 365.0F, 1.40F, 60.0F, ((type_cab.lightMode.get().ordinal() + 5) * 90.0F % 360.0F));
		hornControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_horn.isActive() ? 20.0F : 0.0F);
		independentBrakeControl = new CabControl(-30.0F, 30.0F, 1.15F, 15.0F, (independentBrake > 0.0F ? 20.0F + (independentBrake * -40.0F) : 20.0F));
		lightBeamControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_lights_beams.isActive() ? 20.0F : 0.0F);
		tcsControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_alerter.isActive() ? 20.0F : 0.0F);
		throttleControl = new CabControl(-30.0F, 30.0F, 1.15F, 6.0F, (throttle > 0.0F ? -30.0F + (throttle * 60.0F) : -30.0F));
		trainLightControl = new CabControl(0.0F, 20.0F, 2.0F, 7.0F, type_cab.button_lights_train.isActive() ? 20.0F : 0.0F);

		gaugeTop = new Gauge("bogie 1 cyl", "bogie 2 cyl", 5.0F, Color.RED, 0.0F, Color.YELLOW, 0.0F);
		gaugeBottom = new Gauge("brake pipe", "main reservoir", 10.0F, Color.RED, 0.0F, Color.YELLOW, 0.0F);
	}


	@Override
	public void onPropertyChanged(IPartProperty<?> property)
	{
		super.onPropertyChanged(property);

		/*
		 * This section handles client-sided button changes.
		 */
		if (getTrain() != null && getTrain().worldObj.isRemote)
		{
			/*
			 * Button properties.
			 */
			if (property instanceof PropButton)
			{
				/* This is the button that changed. */
				PropButton button = (PropButton)property;

				/* Screen buttons trigger a beeping sound. */
				if (property instanceof PropButtonScreen)
				{
					if (getEntityIsPlayer() && !isReadingFromNBT)
					{
						if (button.isActive())
						{
							type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_beep", 1.0F, 1.0F);
						}
					}
				}

				/* Alerter */
				else if (button == type_cab.button_alerter)
				{
					if (tcsControl != null)
					{
						tcsControl.setTarget(button.isActive() ? 20.0F : 0.0F);
					}
					if (button.isActive() && !isReadingFromNBT)
					{
						playSwitchSound(type_cab.button_alerter.isActive());
					}
				}

				/*
				 * Brake Mode Switch
				 */
				else if (button == type_cab.button_brakeMode)
				{
					if (brakeModeControl != null)
					{
						brakeModeControl.setTarget(type_cab.button_brakeMode.get() ? 20.0F : 0.0F);
					}
					if (getEntityIsPlayer() && !isReadingFromNBT)
					{
						type_cab.getPassenger().playSound(ModData.ID + (type_cab.button_brakeMode.get() ? ":vehPar_cab_breaker1" : ":vehPar_cab_breaker0"), 1.0F, 1.0F);
					}
				}

				/*
				 * Cruise Control Toggle
				 */
				else if (button == type_cab.button_afb_toggle)
				{
					if (getEntityIsPlayer() && !isReadingFromNBT)
					{
						type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_cruise3", 1.0F, 1.0F);
					}
				}

				/* Doors left */
				else if (button == type_cab.button_doors_left)
				{
					if (doorControl != null)
					{
						doorControl.setTarget(button.isActive() ? 20.0F : -20.0F);
					}
					if (getEntityIsPlayer() && !isReadingFromNBT)
					{
						type_cab.getPassenger().playSound(ModData.ID + (type_cab.button_doors_left.get() ? ":vehPar_cab_switch1" : ":vehPar_cab_switch0"), 1.0F, 1.0F);
					}
				}

				/* Doors right */
				else if (button == type_cab.button_doors_right)
				{
					if (doorControl != null)
					{
						doorControl.setTarget(button.isActive() ? 20.0F : -20.0F);
					}
					if (getEntityIsPlayer() && !isReadingFromNBT)
					{
						type_cab.getPassenger().playSound(ModData.ID + (type_cab.button_doors_right.get() ? ":vehPar_cab_switch1" : ":vehPar_cab_switch0"), 1.0F, 1.0F);
					}
				}

				/* Horn */
				else if (button == type_cab.button_horn)
				{
					if (hornControl != null)
					{
						hornControl.setTarget(button.isActive() ? 20.0F : 0.0F);
					}
					if (button.isActive() && !isReadingFromNBT)
					{
						playSwitchSound(type_cab.button_horn.isActive());
					}
				}

				/* Lights (beams) */
				else if (button == type_cab.button_lights_beams)
				{
					if (lightBeamControl != null)
					{
						lightBeamControl.setTarget(type_cab.button_lights_beams.isActive() ? 20.0F : 0.0F);
					}
					if (!isReadingFromNBT)
					{
						playSwitchSound(type_cab.button_lights_beams.isActive());
					}
				}

				/* Lights (cab) */
				else if (button == type_cab.button_lights_cab)
				{
					if (cabLightControl != null)
					{
						cabLightControl.setTarget(type_cab.button_lights_cab.isActive() ? 20.0F : 0.0F);
					}
					if (!isReadingFromNBT)
					{
						playSwitchSound(type_cab.button_lights_cab.isActive());
					}
				}

				/* Lights (train) */
				else if (button == type_cab.button_lights_train)
				{
					if (trainLightControl != null)
					{
						trainLightControl.setTarget(type_cab.button_lights_train.isActive() ? 20.0F : 0.0F);
					}
					if (getEntityIsPlayer() && !isReadingFromNBT)
					{
						playSwitchSound(type_cab.button_lights_train.isActive());
					}
				}

				/* Park Brake Apply Button */
				else if (button == type_cab.button_parkBrake_apply)
				{
					if (!isReadingFromNBT)
					{
						if (getEntityIsPlayer() && type_cab.button_parkBrake_apply.isActive())
						{
							type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_button", 1.0F, 1.0F);
						}
					}
				}

				/* Park Brake Release Button */
				else if (button == type_cab.button_parkBrake_release)
				{
					if (!isReadingFromNBT)
					{
						if (getEntityIsPlayer() && type_cab.button_parkBrake_release.isActive())
						{
							type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_button", 1.0F, 1.0F);
						}
					}
				}

				/* Controls */
				else if (button == type_cab.button_afb_decrease)
				{
					afbControllerSmooth.decreasing = button.isActive();
				}
				else if (button == type_cab.button_afb_increase)
				{
					afbControllerSmooth.increasing = button.isActive();
				}
				else if (button == type_cab.button_brake_decrease)
				{
					airBrakeControllerSmooth.decreasing = button.isActive();
				}
				else if (button == type_cab.button_brake_increase)
				{
					airBrakeControllerSmooth.increasing = button.isActive();
				}
				else if (button == type_cab.button_brakeDynamic_decrease)
				{
					dynamicBrakeControllerSmooth.decreasing = button.isActive();
				}
				else if (button == type_cab.button_brakeDynamic_increase)
				{
					dynamicBrakeControllerSmooth.increasing = button.isActive();
				}
				else if (button == type_cab.button_brakeIndependent_decrease)
				{
					independentBrakeControllerSmooth.decreasing = button.isActive();
				}
				else if (button == type_cab.button_brakeIndependent_increase)
				{
					independentBrakeControllerSmooth.increasing = button.isActive();
				}
				else if (button == type_cab.button_throttle_decrease)
				{
					powerControllerSmooth.decreasing = button.isActive();
				}
				else if (button == type_cab.button_throttle_increase)
				{
					powerControllerSmooth.increasing = button.isActive();
				}
			}

			/*
			 * Other properties (Not Buttons)
			 */
			else
			{
				/* Reverser */
				if (property == type_cab.reverser)
				{
					/* Play sound to player if applicable. */
					if (getEntityIsPlayer() && !isReadingFromNBT)
					{
						int reverserIndex;
						switch (type_cab.getReverserState())
						{
							default:
								reverserIndex = 0;
								break;

							case FORWARD:
								reverserIndex = 1;
								break;

							case BACKWARD:
								reverserIndex = 2;
								break;

							case NEUTRAL_BACKWARD:
							case NEUTRAL_FORWARD:
								reverserIndex = 3;
								break;
						}

						type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_reverser" + reverserIndex, 1.0F, 1.0F);
					}
				}

				/* Air Brake */
				else if (property == airBrakeController)
				{
					if (airBrakeControl != null)
					{
						float brake = airBrakeController.get();
						airBrakeControl.setTarget(brake != 0.0F ? 30.0F + (brake * -60.0F) : 30.0F);
					}
				}

				/* Dynamic Brake */
				else if (property == dynamicBrakeController)
				{
					if (dynamicBrakeControl != null)
					{
						float brake = dynamicBrakeController.get();
						dynamicBrakeControl.setTarget(brake != 0.0F ? 30.0F + (brake * -60.0F) : 30.0F);
					}
				}

				//				/* Independent Brake */
				//				else if (property == independentBrakeController)
				//				{
				//					if (independentBrakeControl != null)
				//					{
				//						float brake = independentBrakeController.get();
				//						independentBrakeControl.setTarget(brake != 0.0F ? 20.0F + (brake * -40.0F) : 20.0F);
				//					}
				//				}

				/* Throttle */
				else if (property == powerController)
				{
					if (throttleControl != null)
					{
						float throttle = Math.abs(powerController.get());
						throttleControl.setTarget(throttle > 0.0F ? -30.0F + (throttle * 60.0F) : -30.0F);
					}
				}
			}
		}
	}

	@Override
	public void tick()
	{
		super.tick();

		/*
		 * Update sounds.
		 */
		if (cabSounds != null)
		{
			cabSounds.onUpdate();
		}

		/*
		 * DDU screens
		 */
		dduL.onUpdate();
		dduR.onUpdate();

		/*
		 * Tick client/server-only specific methods.
		 */
		Train train = getTrain();
		if (train.worldObj.isRemote)
		{
			if(getEntityIsPlayer())
			{
				tickClientOnly();
			}
		}
		else
		{
			tickServerOnly();
		}
	}

	private void tickClientOnly()
	{
		/*
		 * Advanced brake levers functionality
		 */
		if (airBrakeControllerSmooth.getCurrentNotch() <= 0)
		{
			if (!airBrakeControllerSmooth.increasing && !type_cab.button_brake_decrease.isActive())
			{
				airBrakeControllerSmooth.increasing = true;
			}
		}
		else
		{
			if (airBrakeControllerSmooth.increasing)
			{
				if (!type_cab.button_brake_increase.isActive())
				{
					airBrakeControllerSmooth.increasing = false;
				}
			}
		}
		if (independentBrakeControllerSmooth.getCurrentNotch() <= 0)
		{
			if (!independentBrakeControllerSmooth.increasing && !type_cab.button_brakeIndependent_decrease.isActive())
			{
				independentBrakeControllerSmooth.increasing = true;
			}
		}
		else if (independentBrakeControllerSmooth.getCurrentNotch() >= 2)
		{
			if (!independentBrakeControllerSmooth.decreasing && !type_cab.button_brakeIndependent_increase.isActive())
			{
				independentBrakeControllerSmooth.decreasing = true;
			}
		}
		else
		{
			if (independentBrakeControllerSmooth.increasing)
			{
				if (!type_cab.button_brakeIndependent_increase.isActive())
				{
					independentBrakeControllerSmooth.increasing = false;
				}
			}
			else if (independentBrakeControllerSmooth.decreasing)
			{
				if (!type_cab.button_brakeIndependent_decrease.isActive())
				{
					independentBrakeControllerSmooth.decreasing = false;
				}
			}
		}

		/*
		 * Tick the controls and play appropriate sound effects.
		 */
		{
			Integer result;

			/*
			 * Determine if the air brake and dynamic brake levers should be connected. This simulates physical lever
			 * functionality found on some real locomotives such as the BR 101.
			 */
			boolean brakeLeversConnected = true;
			{
				if (Math.round(airBrakeControllerSmooth.get() * 16.0F) != Math.round(dynamicBrakeControllerSmooth.get() * 16.0F))
				{
					brakeLeversConnected = false;
				}
				else
				{
					if (dynamicBrakeControllerSmooth.increasing || dynamicBrakeControllerSmooth.decreasing)
					{
						if (!airBrakeControllerSmooth.increasing && !airBrakeControllerSmooth.decreasing)
						{
							brakeLeversConnected = false;
						}
					}
				}
			}

			/*
			 * Air Brake Controller
			 */
			result = airBrakeControllerSmooth.tick(true);
			if (result != null && getPassenger() != null)
			{
				if (airBrakeControllerSmooth.increasing)
				{
					switch (result)
					{
						case 1:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_brake9", 1.0F, 1.0F);
							break;
						}

						case 2:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_brake0", 1.0F, 1.0F);
							break;
						}

						case 3:
						case 4:
						case 5:
						case 6:
						case 7:
						case 8:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_brake1", 1.0F, 1.0F);
							break;
						}

						case 9:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_brake2", 1.0F, 1.0F);
							break;
						}
					}
				}
				else if (airBrakeControllerSmooth.decreasing)
				{
					switch (result)
					{
						case 0:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_brake8", 1.0F, 1.0F);
							break;
						}

						case 1:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_brake5", 1.0F, 1.0F);
							break;
						}

						case 2:
						case 3:
						case 4:
						case 5:
						case 6:
						case 7:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_brake4", 1.0F, 1.0F);
							break;
						}

						case 8:
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_brake3", 1.0F, 1.0F);
							break;
						}
					}
				}
			}

			/*
			 * Dynamic Brake Controller
			 */
			if (brakeLeversConnected)
			{
				dynamicBrakeControllerSmooth.set(airBrakeControllerSmooth.get());
			}
			else
			{
				result = dynamicBrakeControllerSmooth.tick(true);
				if (result != null && getPassenger() != null)
				{
					if (dynamicBrakeControllerSmooth.increasing)
					{
						switch (result)
						{
							case 1:
							{
								getPassenger().playSound(ModData.ID + ":vehPar_cab_brake11", 1.0F, 1.0F);
								break;
							}

							case 2:
							case 3:
							case 4:
							case 5:
							case 6:
							case 7:
							case 8:
							{
								getPassenger().playSound(ModData.ID + ":vehPar_cab_brake6", 1.0F, 1.0F);
								break;
							}

							case 9:
							{
								getPassenger().playSound(ModData.ID + ":vehPar_cab_brake7", 1.0F, 1.0F);
								break;
							}
						}
					}
					else if (dynamicBrakeControllerSmooth.decreasing)
					{
						switch (result)
						{
							case 0:
							{
								getPassenger().playSound(ModData.ID + ":vehPar_cab_brake10", 1.0F, 1.0F);
								break;
							}

							case 1:
							case 2:
							case 3:
							case 4:
							case 5:
							case 6:
							case 7:
							case 8:
							{
								getPassenger().playSound(ModData.ID + ":vehPar_cab_brake6", 1.0F, 1.0F);
								break;
							}
						}
					}
				}
			}

			/*
			 * Independent Brake Controller
			 */
			result = independentBrakeControllerSmooth.tick(true);
			if (result != null && getPassenger() != null)
			{
				switch (result)
				{
					case 0:
					{
						getPassenger().playSound(ModData.ID + ":vehPar_cabLocomotive_independent0", 1.0F, 1.0F);
						break;
					}

					case 1:
					{
						getPassenger().playSound(ModData.ID + ":vehPar_cabLocomotive_independent1", 1.0F, 1.0F);
						break;
					}

					case 2:
					{
						getPassenger().playSound(ModData.ID + ":vehPar_cabLocomotive_independent2", 1.0F, 1.0F);
						break;
					}
				}
			}

			/*
			 * Power Controller
			 */
			result = powerControllerSmooth.tick(true);
			if (result != null && getPassenger() != null)
			{
				switch (result)
				{
					case 0:
					{
						getPassenger().playSound(ModData.ID + ":vehPar_cabLocomotive_throttle1", 1.0F, 1.0F);
						break;
					}

					case 1:
					{
						getPassenger().playSound(ModData.ID + ":vehPar_cabLocomotive_throttle0", 1.0F, 1.0F);
						break;
					}

					case 2:
					{
						if (powerControllerSmooth.increasing)
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cabLocomotive_throttle2", 1.0F, 1.0F);
						}
						break;
					}

				}
			}

			/*
			 * AFB Controller
			 */
			result = afbControllerSmooth.tick(true);
			if (result != null && getPassenger() != null)
			{
				switch (result)
				{
					case 0:
					{
						getPassenger().playSound(ModData.ID + ":vehPar_cab_cruise0", 1.0F, 1.0F);
						break;
					}

					case 1:
					{
						if (afbController.increasing)
						{
							getPassenger().playSound(ModData.ID + ":vehPar_cab_cruise1", 1.0F, 1.0F);
						}
						break;
					}

					case 32:
					{
						getPassenger().playSound(ModData.ID + ":vehPar_cab_cruise2", 1.0F, 1.0F);
						break;
					}
				}
			}
		}


		/* Instantiate control and gauge fields if necessary. */
		if (throttleControl == null || gaugeTop == null)
		{
			initControls();
		}

		//		/*
		//		 * Quieten Air conditioning volume in the cab.
		//		 */
		//		if (soundAirConditioning.sound != null)
		//		{
		//			if (soundAirConditioning.sound.getVolume() != 0.5F)
		//			{
		//				soundAirConditioning.sound.setVolume(0.5F);
		//			}
		//		}

		/*
		 * Update Controls
		 */

		afbControl.setTarget(-30.0F + (afbControllerSmooth.get() * 60.0F));
		afbControl.tick();
		airBrakeControl.setTarget(30.0F + (airBrakeControllerSmooth.get() * -60.0F));
		airBrakeControl.tick();
		brakeModeControl.tick();
		cabLightControl.tick();
		doorControl.tick();
		dynamicBrakeControl.setTarget(30.0F + (dynamicBrakeControllerSmooth.get() * -60.0F));
		dynamicBrakeControl.tick();
		hornControl.tick();
		independentBrakeControl.setTarget(20.0F + (independentBrakeControllerSmooth.get() * -40.0F));
		independentBrakeControl.tick();
		lightBeamControl.tick();
		tcsControl.tick();
		throttleControl.setTarget(-30.0F + (powerControllerSmooth.get() * 60.0F));
		throttleControl.tick();
		trainLightControl.tick();


		/* Update gauges. */
		float parkBrakeCylinderPressure = 0.0F;
		{
			float auxReservoirPressure = 0.0F;
			float brakeCylinderPressure = 0.0F;
			float brakeCylinderPressureOther = 0.0F;
			float brakePipePressure = 0.0F;
			float mainReservoirPipePressure = 0.0F;

			/*
			 * Get the bogie underneath this cab.
			 */
			OrderedTypesList<PartTypeBogie> bogies = type_cab.getVehicle().getTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY);
			PartTypeBogie bogie = bogies.get(0);
			PartTypeBogie bogieNext = bogies.get(bogies.size() - 1);
			if (bogie != null)
			{
				/* Brake Cylinders (for each bogie) */
				brakeCylinderPressure = bogie.pneumaticsBrakeCylinder.getPressure();
				if (bogieNext != null)
				{
					brakeCylinderPressureOther = bogieNext.pneumaticsBrakeCylinder.getPressure();
				}
				else
				{
					brakeCylinderPressureOther = brakeCylinderPressure;
				}

				/* Main Reservoir */
				mainReservoirPipePressure = bogie.pneumaticsMainReservoirPipe.getPressure();

				/* Park Brake */
				parkBrakeCylinderPressure = bogie.pneumaticsParkBrakeCylinder.getPressure();
			}


			gaugeTop.setNeedleAValue(brakeCylinderPressure / 100.0F);
			gaugeTop.setNeedleBValue(brakeCylinderPressureOther / 100.0F);
			gaugeTop.tick();

			gaugeBottom.setNeedleAValue(this.type_cab.brakeValve.getPressure() / 100.0F);
			gaugeBottom.setNeedleBValue(mainReservoirPipePressure / 100.0F);
			gaugeBottom.tick();
		}

		/*
		 * Slow Client Tick
		 */
		slowClientTick--;
		if (slowClientTick <= 0)
		{
			slowClientTick = 10;

			/*
			 * Update the park brake indicator light.
			 */
			boolean indicatorLightParkBrakeOld = indicatorLightParkBrake;
			indicatorLightParkBrake = parkBrakeCylinderPressure < 275.0F;
			if (indicatorLightParkBrake != indicatorLightParkBrakeOld)
			{
				this.updateVertexState = true;
			}

			/*
			 * Update the staff-responsible indicator light.
			 */
			boolean indicatorLightStaffResponsibleOld = indicatorLightStaffResponsible;
			indicatorLightStaffResponsible = type_cab.mtms.operationMode.get() != EMTMSOperationMode.FULL_SUPERVISION;
			{
				if (indicatorLightStaffResponsible != indicatorLightStaffResponsibleOld)
				{
					this.updateVertexState = true;
				}
			}
		}
	}

	private void tickServerOnly()
	{
		/*
		 * Update the server-side control positions based on changes to the client-side smooth levers.
		 */
		/*
		 * AFB Controller
		 */
		if (afbController.getCurrentNotch() != afbControllerSmooth.getCurrentNotch())
		{
			afbController.setNotch(afbControllerSmooth.getCurrentNotch());
		}

		/*
		 * Air Brake Controller
		 */
		if (airBrakeController.getCurrentNotch() != airBrakeControllerSmooth.getCurrentNotch())
		{
			airBrakeController.setNotch(airBrakeControllerSmooth.getCurrentNotch());
		}

		/*
		 * Dynamic Brake Controller
		 */
		if (dynamicBrakeController.getCurrentNotch() != dynamicBrakeControllerSmooth.getCurrentNotch())
		{
			dynamicBrakeController.setNotch(dynamicBrakeControllerSmooth.getCurrentNotch());
		}

		/*
		 * Independent Brake Controller
		 */
		if (independentBrakeController.getCurrentNotch() != independentBrakeControllerSmooth.getCurrentNotch())
		{
			independentBrakeController.setNotch(independentBrakeControllerSmooth.getCurrentNotch());
		}
		if (independentBrakeController.getCurrentNotch() == 0 && independentBrakeLevel.get() > 0.0F)
		{
			independentBrakeLevel.set(independentBrakeLevel.get() - 0.03F);
		}
		else if (independentBrakeController.getCurrentNotch() == 2 && independentBrakeLevel.get() < 1.0F)
		{
			independentBrakeLevel.set(independentBrakeLevel.get() + 0.03F);
		}

		/*
		 * Power Controller
		 */
		if (powerController.getCurrentNotch() != powerControllerSmooth.getCurrentNotch())
		{
			powerController.setNotch(powerControllerSmooth.getCurrentNotch());
		}
	}

	@Override
	protected void updatePartTypeControls()
	{
		type_cab.controlPower.set(powerController.get());
		type_cab.controlBrakeDynamic.set((dynamicBrakeController.getCurrentNotch() - 1) / 8.0F);
		type_cab.controlBrakeIndependent.set(independentBrakeLevel.get());

		if (type_cab.button_brakeMode.get())
		{
			type_cab.controlBrakePneumatic.set((airBrakeController.getCurrentNotch() - 1) / 8.0F);
		}
		else
		{
			type_cab.controlBrakeElectroPneumatic.set((airBrakeController.getCurrentNotch() - 1) / 8.0F);
		}

		type_cab.controlBrakeEmergency.set(airBrakeController.get() >= 1.0F);

		if (afbController.get() > 0.0F && type_cab.button_afb_toggle.get())
		{
			type_cab.controlCruiseControl.set(afbController.get() * 160.0F);
		}
		else
		{
			type_cab.controlCruiseControl.set(afbController.get() * -160.0F);
		}

		type_cab.mtms.cabPowerLevel.set(powerController.get());
		type_cab.mtms.cabBrakeLevel.set(Math.max((airBrakeController.getCurrentNotch() - 1) / 8.0F, (dynamicBrakeController.getCurrentNotch() - 1) / 8.0F));
	}

	/**
	 * <p>
	 * Helper method that plays a switch sound to the player in this cab.
	 * </p>
	 * <p>
	 * <i>Client-side only.</i>
	 * </p>
	 */
	@SideOnly(Side.CLIENT)
	private void playSwitchSound(boolean switchState)
	{
		if (getEntityIsPlayer())
		{
			type_cab.getPassenger().playSound(ModData.ID + ":vehPar_cab_switch" + (switchState ? 1 : 0), 1.0F, 1.0F);
		}
	}


	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "MCM", "COC", "PSP", 'P', "plateSteel", 'O', new ItemStack(ModCenter.ItemPart, 1, 0), 'S', "vehparSeat", 'C', "circuitBasic", 'M', new ItemStack(ModCenter.ItemPart, 1, 3)));
	}


	private static float posX(double x)
	{
		return (float)-((x - 0.4) * 0.8 - 0.075);
	}

	private static float posY(double y)
	{
		return (float)((y + 0.075) * 0.8);
	}

	private static float posZ(double z)
	{
		return (float)-(z * 0.8);
	}

}
