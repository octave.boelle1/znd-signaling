package zoranodensha.vehicleParts.common.parts.transport;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.part.rendering.IVehiclePartRenderer;
import zoranodensha.vehicleParts.client.render.transport.VehParRenderer_TransportOpen;



public class VehParTransportOpen extends AVehParTransportItem
{
	public VehParTransportOpen()
	{
		super("VehParTransportOpen", 0.75F, 0.24875F, 6.75F);
	}


	@Override
	@SideOnly(Side.CLIENT)
	public IVehiclePartRenderer getRenderer()
	{
		return (renderer == null ? renderer = new VehParRenderer_TransportOpen(this) : renderer);
	}

	@Override
	public boolean isItemValid(ItemStack itemStack)
	{
		if (itemStack != null && itemStack.getItem() instanceof ItemBlock && (Block.getBlockFromItem(itemStack.getItem())).isNormalCube())
		{
			ItemStack iteratedStack;
			for (int i = type_transportItem.getSizeInventory() - 1; i >= 0; --i)
			{
				iteratedStack = type_transportItem.getStackInSlot(i);
				if (iteratedStack != null)
				{
					return (itemStack.getItem() == iteratedStack.getItem()) && (itemStack.getItemDamage() == iteratedStack.getItemDamage());
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public void registerItemRecipe(ItemStack itemStack)
	{
		GameRegistry.addRecipe(new ShapedOreRecipe(itemStack, "   ", "P P", "PHP", 'P', "plateSteel", 'H', new ItemStack(Item.getItemFromBlock(Blocks.hopper))));
	}
}
