package zoranodensha.vehicleParts.common.parts.transport;

import net.minecraft.item.ItemStack;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.type.PartTypeLoadable;
import zoranodensha.vehicleParts.common.parts.AVehParBaseImpl;



public abstract class AVehParTransportItem extends AVehParBaseImpl
{
	/** Loadable part type, providing basic access to load state data. */
	public final PartTypeLoadable type_loadable;
	/** Item transport part type, handling inventory data. */
	public final PartTypeTransportItemImpl type_transportItem;

	/** Property holding this part's color. */
	public PropColor color;



	public AVehParTransportItem(String name, float widthBy2, float heightBy2, float mass)
	{
		super(name, widthBy2, heightBy2, mass);

		type_loadable = new PartTypeLoadable(this);
		type_transportItem = new PartTypeTransportItemImpl(this);

		addProperty(color = (PropColor)new PropColor(this, new ColorRGBA(0.32F, 0.32F, 0.32F), "color").setConfigurable());
	}

	/**
	 * Returns whether the given item may be stored in this inventory.<br>
	 * This can be used to run whitelist checks for item types. Allows any item by default.
	 * 
	 * @param itemStack - {@link net.minecraft.item.ItemStack ItemStack} to check for validity.
	 * @return {@code true} if the given item may be stored in this inventory.
	 */
	public boolean isItemValid(ItemStack itemStack)
	{
		return true;
	}
}