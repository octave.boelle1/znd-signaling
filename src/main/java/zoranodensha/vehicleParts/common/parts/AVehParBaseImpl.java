package zoranodensha.vehicleParts.common.parts;

import java.lang.ref.WeakReference;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.IPartProperty;
import zoranodensha.client.render.ICachedVertexState;
import zoranodensha.common.core.ModData;



public abstract class AVehParBaseImpl extends VehParBase implements ICachedVertexState
{
	/** {@code true} after a property has changed and requires client-sided vertex state updates. */
	@SideOnly(Side.CLIENT) public boolean updateVertexState;



	public AVehParBaseImpl(String name, float widthBy2, float heightBy2, float mass)
	{
		super(name, widthBy2 * 2, heightBy2 * 2, mass);
		ICachedVertexState.objectList.add(new WeakReference<ICachedVertexState>(this));
	}


	@Override
	public String getName()
	{
		return ModData.ID + "_" + super.getName();
	}

	@Override
	public void onAddedToTrain(Train train)
	{
		/* Update vertex state flag if the given train exists on client-side. */
		if (train != null && train.worldObj.isRemote)
		{
			updateVertexState = true;
		}
	}

	@Override
	public void onPropertyChanged(IPartProperty<?> property)
	{
		super.onPropertyChanged(property);

		/* Update vertex state flag if our train exists on client-side. */
		if (getTrain() != null && getTrain().worldObj.isRemote)
		{
			updateVertexState = true;
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void onVertexStateReset()
	{
		updateVertexState = true;
	}
}