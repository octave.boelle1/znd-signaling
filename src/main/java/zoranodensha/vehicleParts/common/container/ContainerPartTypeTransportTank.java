package zoranodensha.vehicleParts.common.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import zoranodensha.api.vehicles.part.type.PartTypeTransportTank;
import zoranodensha.common.containers.slot.SlotRestrictive;


/**
 * Container class for tank transport part types.<br>
 * These contain two slots for bucket input/ output, as well as default player inventory and hotbar slots.
 */
public class ContainerPartTypeTransportTank extends Container
{
	private PartTypeTransportTank partTypeTank;



	public ContainerPartTypeTransportTank(InventoryPlayer playerInv, PartTypeTransportTank partType)
	{
		partType.openInventory();
		partTypeTank = partType;

		/* Bucket input/ output slots. */
		addSlotToContainer(new SlotRestrictive(partType, 0, 44, 17));
		addSlotToContainer(new SlotRestrictive(partType, 1, 44, 53));

		/* Player inventory slots. */
		for (int row = 0; row < 3; ++row)
		{
			for (int column = 0; column < 9; ++column)
			{
				addSlotToContainer(new Slot(playerInv, column + row * 9 + 9, 8 + column * 18, 84 + row * 18));
			}
		}

		/* Player hotbar slots. */
		for (int row = 0; row < 9; ++row)
		{
			addSlotToContainer(new Slot(playerInv, row, 8 + row * 18, 142));
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer player)
	{
		return partTypeTank.isUseableByPlayer(player);
	}

	@Override
	public void onContainerClosed(EntityPlayer player)
	{
		super.onContainerClosed(player);
		partTypeTank.closeInventory();
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slotId)
	{
		ItemStack itemStack = null;
		Slot slot = (Slot)inventorySlots.get(slotId);
		if (slot != null && slot.getHasStack())
		{
			ItemStack itemStack0 = slot.getStack();
			itemStack = itemStack0.copy();
			if (slotId == 1)
			{
				if (!mergeItemStack(itemStack0, 2, 38, true))
				{
					return null;
				}
				slot.onSlotChange(itemStack0, itemStack);
			}
			else if (slotId > 1)
			{
				if (partTypeTank.isItemValidForSlot(0, itemStack0))
				{
					if (!mergeItemStack(itemStack0, 0, 1, false))
					{
						return null;
					}
				}
				else if (slotId > 1 && slotId < 29)
				{
					if (!mergeItemStack(itemStack0, 29, 38, false))
					{
						return null;
					}
				}
				else if (slotId > 28 && !mergeItemStack(itemStack0, 2, 29, false))
				{
					return null;
				}
			}
			else if (!mergeItemStack(itemStack0, 2, 38, false))
			{
				return null;
			}

			if (itemStack0.stackSize == 0)
			{
				slot.putStack((ItemStack)null);
			}
			else
			{
				slot.onSlotChanged();
			}

			if (itemStack0.stackSize == itemStack.stackSize)
			{
				return null;
			}

			slot.onPickupFromSlot(player, itemStack0);
		}

		return itemStack;
	}
}
