package zoranodensha.vehicleParts.common.sounds;

import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.type.PartTypeEngineElectric;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.common.parts.engine.VehParEngineElectricBR101;



public class ElectricEngineSoundsBR101 extends APartSounds
{
	protected final VehParEngineElectricBR101 engineVehiclePart;

	protected final PartTypeEngineElectric enginePartType;

	private int previousEngineActivity;

	protected PropSound sound_tractionCircuit_e, sound_tractionCircuit_i;

	protected PropSound sound_traction0_e, sound_traction0_i;
	protected PropSound sound_traction1_e, sound_traction1_i;
	protected PropSound sound_traction2_e, sound_traction2_i;

	protected PropSound sound_inverter0_e, sound_inverter0_i;
	protected PropSound sound_inverter1_e, sound_inverter1_i;
	protected PropSound sound_inverterStart_e, sound_inverterStart_i;
	protected PropSound sound_inverterStop_e, sound_inverterStop_i;

	protected PropSound sound_highTension_e, sound_highTension_i;
	protected PropSound sound_highTensionStart_e, sound_highTensionStart_i;
	protected PropSound sound_highTensionStop_e, sound_highTensionStop_i;

	public float volume_highTension;
	public float volume_inverter0;
	public float volume_inverter1;
	public float volume_traction0;
	public float volume_traction1;
	public float volume_traction2;
	public float volume_tractionCircuit;

	public float pitch_highTension;
	public float pitch_inverter0;
	public float pitch_inverter1;
	public float pitch_traction0;
	public float pitch_traction1;
	public float pitch_traction2;
	public float pitch_tractionCircuit;


	public ElectricEngineSoundsBR101(VehParEngineElectricBR101 engineVehiclePart, PartTypeEngineElectric enginePartType)
	{
		this.engineVehiclePart = engineVehiclePart;
		this.enginePartType = enginePartType;

		/*
		 * Exterior Sounds
		 */
		this.engineVehiclePart.addProperty(sound_highTension_e = new PropSoundEngineBR101(this, 8, false));
//		this.engineVehiclePart.addProperty(sound_highTensionStart_e = new PropSoundEngineBR101Effect(this, 6, false));
//		this.engineVehiclePart.addProperty(sound_highTensionStop_e = new PropSoundEngineBR101Effect(this, 7, false));
		this.engineVehiclePart.addProperty(sound_inverter0_e = new PropSoundEngineBR101(this, 0, false));
		this.engineVehiclePart.addProperty(sound_inverter1_e = new PropSoundEngineBR101(this, 1, false));
		this.engineVehiclePart.addProperty(sound_tractionCircuit_e = new PropSoundEngineBR101(this, 2, false));

		/*
		 * Interior Sounds
		 */
		this.engineVehiclePart.addProperty(sound_highTension_i = new PropSoundEngineBR101(this, 8, true));
//		this.engineVehiclePart.addProperty(sound_highTensionStart_i = new PropSoundEngineBR101Effect(this, 6, true));
//		this.engineVehiclePart.addProperty(sound_highTensionStop_i = new PropSoundEngineBR101Effect(this, 7, true));
		this.engineVehiclePart.addProperty(sound_inverter0_i = new PropSoundEngineBR101(this, 0, true));
		this.engineVehiclePart.addProperty(sound_inverter1_i = new PropSoundEngineBR101(this, 1, true));
		this.engineVehiclePart.addProperty(sound_traction0_i = new PropSoundEngineBR101(this, 3, true));
		this.engineVehiclePart.addProperty(sound_traction1_i = new PropSoundEngineBR101(this, 4, true));
		this.engineVehiclePart.addProperty(sound_traction2_i = new PropSoundEngineBR101(this, 5, true));
		this.engineVehiclePart.addProperty(sound_tractionCircuit_i = new PropSoundEngineBR101(this, 2, true));
	}


	/**
	 * This method should be called each game tick by the parent vehicle part to update the sound effects contained within this class.
	 */
	public void tick()
	{
		if (!enginePartType.hasSounds.get())
		{
			volume_inverter0 = 0.0F;
			volume_inverter1 = 0.0F;
			volume_traction0 = 0.0F;
			volume_traction1 = 0.0F;
			volume_traction2 = 0.0F;
			volume_tractionCircuit = 0.0F;
			return;
		}

		float speed = (float)Math.abs(enginePartType.getTrain().getLastSpeed() * 3.6F * 20.0F);
		float motorActivity = enginePartType.motorActivity.get();
		float motorPolarity = enginePartType.motorPolarity.get();

		volume_highTension = updateVolumeHighTension();
		volume_inverter0 = updateVolumeInverter(0, motorPolarity, speed);
		volume_inverter1 = updateVolumeInverter(1, motorPolarity, speed);
		volume_traction0 = updateVolumeTraction(0, motorActivity, speed);
		volume_traction1 = updateVolumeTraction(1, motorActivity, speed);
		volume_traction2 = updateVolumeTraction(2, motorActivity, speed);
		volume_tractionCircuit = updateVolumeTractionCircuit(motorPolarity);

		pitch_highTension = 1.0F;
		pitch_inverter0 = updatePitchInverter(0, speed);
		pitch_inverter1 = updatePitchInverter(1, speed);
		pitch_traction0 = updatePitchTraction(0, speed);
		pitch_traction1 = updatePitchTraction(1, speed);
		pitch_traction2 = updatePitchTraction(2, speed);
		pitch_tractionCircuit = 1.0F;
	}


	/*
	 * High from 	45.00  to 180.00
	 * Medium from 	11.25  to  45.00
	 * Low from		 2.81  to  11.25
	 */


	public float updateVolumeHighTension()
	{
		float target = enginePartType.enabled.get() ? 1.0F : 0.0F;

		if (target < volume_highTension - 0.02F)
		{
			target = volume_highTension - 0.02F;
		}
		else if (target > volume_highTension + 0.02F)
		{
			target = volume_highTension + 0.02F;
		}

		return target;
	}

	/**
	 * Determines what the volume of the supplied inverter sound effect should be at the given moment, based on the supplied engine/train information.
	 *
	 * @param type - The sound effect's index.
	 * @return - A {@code float} between {@code 0.0F} and {@code 1.0F}.
	 */
	public float updateVolumeInverter(int type, float motorPolarity, float speed)
	{
		switch (type)
		{
			case 0:
			{
				float volume;

				if (speed < 1.0F)
				{
					return 0.0F;
				}
				else if (speed < 5.0F)
				{
					volume = (speed - 1.0F) / 4.0F;
					volume *= Math.abs(motorPolarity);
					return volume;
				}
				else if (speed < 20.0F)
				{
					volume = Math.abs(motorPolarity);
					return volume;
				}
				else if (speed < 20.5F)
				{
					volume = 1.0F - ((speed - 20.0F) / 0.5F);
					volume *= Math.abs(motorPolarity);
					return volume;
				}
				else
				{
					return 0.0F;
				}
			}

			case 1:
			{
				float volume;

				if (speed < 20.0F)
				{
					return 0.0F;
				}
				else if (speed < 20.5F)
				{
					volume = (speed - 20.0F) / 0.5F;
					volume *= Math.abs(motorPolarity);
					return volume;
				}
				else if (speed < 70.0F)
				{
					volume = 1.0F - ((speed - 20.5F) / (70.0F - 20.5F));
					volume *= Math.abs(motorPolarity);
					return volume;
				}
				else
				{
					return 0.0F;
				}
			}

			default:
			{
				return 0.0F;
			}
		}
	}


	/**
	 * Determines what the pitch of the supplied inverter sound effect should be at the given moment, based on the supplied engine/train information.
	 *
	 * @param type - The sound effect's index.
	 * @return - A {@code float} between {@code 0.5F} and {@code 2.0F}.
	 */
	public float updatePitchInverter(int type, float speed)
	{
		switch (type)
		{
			case 1:
			{
				if (speed < 20.0F)
				{
					return 0.5F;
				}
				else if (speed < 20.5F)
				{
					return 0.5F + (speed - 20.0F);
				}
				else if (speed < 70.0F)
				{
					return 1.0F - (float)(Math.pow(speed - 20.5F, 2.0F) / 10000F);
				}
				else
				{
					return 0.5F;
				}
			}

			default:
			{
				return 1.0F;
			}
		}
	}


	public float updateVolumeTraction(int type, float motorActivity, float speed)
	{
		switch (type)
		{
			case 0:
			{
				if (speed < 2.8125F)
				{
					return Math.abs(motorActivity) * Math.abs(speed / 2.8125F);
				}
				else if (speed >= 11.25F)
				{
					return 0.0F;
				}
				break;
			}

			case 1:
			{
				if (speed < 11.25F || speed >= 45.0F)
				{
					return 0.0F;
				}
				break;
			}

			case 2:
			{
				if (speed < 45.0F)
				{
					return 0.0F;
				}
				break;
			}

			default:
			{
				return 0.0F;
			}
		}

		return Math.abs(motorActivity);
	}

	public float updatePitchTraction(int type, float speed)
	{
		float result = 1.0F;

		switch (type)
		{
			case 0:
			{
				result = 1.0F + ((speed - 5.625F) / 5.625F);
				break;
			}

			case 1:
			{
				result = 1.0F + ((speed - 22.5F) / 22.5F);
				break;
			}

			case 2:
			{
				result = 1.0F + ((speed - 90.0F) / 90.0F);
				break;
			}

			default:
			{
				break;
			}
		}

		if (result < 0.5F)
		{
			return 0.5F;
		}
		else if (result > 2.0F)
		{
			return 2.0F;
		}

		return result;
	}


	/**
	 * Determines what the volume of the traction circuit sound effect should be at the given moment, based on the supplied engine/train information.
	 *
	 * @return - A {@code float} between {@code 0.0F} and {@code 1.0F}.
	 */
	public float updateVolumeTractionCircuit(float motorPolarity)
	{
		float result = Math.abs(motorPolarity) * 5.0F;

		if (result > 1.0F)
		{
			result = 1.0F;
		}

		return result;
	}


	public static class PropSoundEngineBR101Effect extends AElectricEngineSound
	{
		public PropSoundEngineBR101Effect(ElectricEngineSoundsBR101 wrapper, int index, boolean interior)
		{
			super(wrapper, wrapper.engineVehiclePart, index, String.format("%s:vehPar_engineSoundBR101_%d", ModData.ID, index), "electricEngineSoundsBR101", interior);
		}

		@Override
		public void play()
		{
			super.play();
		}

		@Override
		public Integer get()
		{
			return property;
		}

		@Override
		protected float getPitch()
		{
			return 1.0F;
		}

		protected float getVolume()
		{
			if (!(wrapper instanceof ElectricEngineSoundsBR101))
			{
				return 0.0F;
			}

			ElectricEngineSoundsBR101 wrapper = (ElectricEngineSoundsBR101)this.wrapper;

			/*
			 * Make sure only interior or exterior sounds are played depending on the perspective of the player's camera.
			 */
			if (interior != null && wrapper.engineVehiclePart.getTrain().worldObj.isRemote)
			{
				if (isPlayerInside(wrapper.enginePartType.getTrain()))
				{
					if (!interior)
					{
						return 0.0F;
					}
				}
				else
				{
					if (interior)
					{
						return 0.0F;
					}
				}
			}

			return 1.0F;
		}
	}



	public static class PropSoundEngineBR101 extends AElectricEngineSoundRepeated
	{

		public PropSoundEngineBR101(ElectricEngineSoundsBR101 wrapper, int index, Boolean interior)
		{
			super(wrapper, wrapper.engineVehiclePart, index, String.format("%s:vehPar_engineSoundBR101_%d", ModData.ID, index), "electricEngineSoundsBR101", interior);
		}


		@Override
		protected float getPitch()
		{
			if (!(wrapper instanceof ElectricEngineSoundsBR101))
			{
				return 1.0F;
			}

			ElectricEngineSoundsBR101 wrapper = (ElectricEngineSoundsBR101)this.wrapper;

			/*
			 * Get the current pitch setting for this sound effect from the wrapper class.
			 */
			switch (index)
			{
				case 0:
					return wrapper.pitch_inverter0;
				case 1:
					return wrapper.pitch_inverter1;
				case 2:
					return wrapper.pitch_tractionCircuit;
				case 3:
					return wrapper.pitch_traction0;
				case 4:
					return wrapper.pitch_traction1;
				case 5:
					return wrapper.pitch_traction2;
				case 8:
					return wrapper.pitch_highTension;
				default:
					return 1.0F;
			}
		}


		@Override
		protected float getVolume()
		{
			if (!(wrapper instanceof ElectricEngineSoundsBR101))
			{
				return 0.0F;
			}

			ElectricEngineSoundsBR101 wrapper = (ElectricEngineSoundsBR101)this.wrapper;


			/*
			 * Make sure only interior or exterior sounds are played depending on the perspective of the player's camera.
			 */
			if (interior != null && wrapper.engineVehiclePart.getTrain().worldObj.isRemote)
			{
				if (isPlayerInside(wrapper.enginePartType.getTrain()))
				{
					if (!interior)
					{
						return 0.0F;
					}
				}
				else
				{
					if (interior)
					{
						return 0.0F;
					}
				}
			}

			/*
			 * Get the current volume level for this sound effect from the wrapper class.
			 */
			switch (index)
			{
				case 0:
					return wrapper.volume_inverter0;
				case 1:
					return wrapper.volume_inverter1;
				case 2:
					return wrapper.volume_tractionCircuit;
				case 3:
					return wrapper.volume_traction0;
				case 4:
					return wrapper.volume_traction1;
				case 5:
					return wrapper.volume_traction2;
				case 8:
					return wrapper.volume_highTension;
				default:
					return 0.0F;
			}
		}
	}
}
