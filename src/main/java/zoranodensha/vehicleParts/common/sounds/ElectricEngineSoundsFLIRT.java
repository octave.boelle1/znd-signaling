package zoranodensha.vehicleParts.common.sounds;

import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.type.PartTypeEngineElectric;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.common.parts.bogie.VehParAxisFLIRT;



public class ElectricEngineSoundsFLIRT extends APartSounds
{
	protected final VehParAxisFLIRT engineVehiclePart;

	protected final PartTypeEngineElectric enginePartType;


	protected PropSound sound_highTension_e;

	protected PropSound sound_inverter0_e, sound_inverter0_i;
	protected PropSound sound_inverter1_e, sound_inverter1_i;
	protected PropSound sound_inverter2_e, sound_inverter2_i;

	protected PropSound sound_traction0_e, sound_traction0_i;
	protected PropSound sound_traction1_e, sound_traction1_i;
	protected PropSound sound_traction2_e, sound_traction2_i;


	public float volume_highTension;
	public float volume_inverter0;
	public float volume_inverter1;
	public float volume_inverter2;
	public float volume_traction0;
	public float volume_traction1;
	public float volume_traction2;

	public float pitch_highTension;
	public float pitch_inverter0;
	public float pitch_inverter1;
	public float pitch_inverter2;
	public float pitch_traction0;
	public float pitch_traction1;
	public float pitch_traction2;


	public ElectricEngineSoundsFLIRT(VehParAxisFLIRT engineVehiclePart, PartTypeEngineElectric enginePartType)
	{
		this.engineVehiclePart = engineVehiclePart;
		this.enginePartType = enginePartType;

		/*
		 * Exterior Sounds
		 */
		this.engineVehiclePart.addProperty(sound_highTension_e = new PropSoundEngineFLIRT(this, 0, false));
		this.engineVehiclePart.addProperty(sound_inverter0_e = new PropSoundEngineFLIRT(this, 1, false));
		this.engineVehiclePart.addProperty(sound_inverter1_e = new PropSoundEngineFLIRT(this, 2, false));
		this.engineVehiclePart.addProperty(sound_inverter2_e = new PropSoundEngineFLIRT(this, 3, false));
		this.engineVehiclePart.addProperty(sound_traction2_e = new PropSoundEngineFLIRT(this, 6, false));

		/*
		 * Interior Sounds
		 */
		this.engineVehiclePart.addProperty(sound_inverter0_i = new PropSoundEngineFLIRT(this, 1, true));
		this.engineVehiclePart.addProperty(sound_inverter1_i = new PropSoundEngineFLIRT(this, 2, true));
		this.engineVehiclePart.addProperty(sound_inverter2_i = new PropSoundEngineFLIRT(this, 3, true));
		this.engineVehiclePart.addProperty(sound_traction0_i = new PropSoundEngineFLIRT(this, 4, true));
		this.engineVehiclePart.addProperty(sound_traction1_i = new PropSoundEngineFLIRT(this, 5, true));
		this.engineVehiclePart.addProperty(sound_traction2_i = new PropSoundEngineFLIRT(this, 6, true));
	}


	/**
	 * This method should be called each game tick by the parent vehicle part to update the sound effects contained within this class.
	 */
	public void tick()
	{
		if (!enginePartType.hasSounds.get())
		{
			volume_highTension = volume_inverter0 = volume_inverter1 = volume_inverter2 = volume_traction0 = volume_traction1 = 0.0F;
			return;
		}

		float speed = (float)Math.abs(enginePartType.getTrain().getLastSpeed() * 3.6F * 20.0F);
		float motorActivity = enginePartType.motorActivity.get();
		float motorPolarity = enginePartType.motorPolarity.get();

		volume_highTension = updateVolumeHighTension();
		volume_inverter0 = updateVolumeInverter(0, speed, motorPolarity) * 0.30F;
		volume_inverter1 = updateVolumeInverter(1, speed, motorPolarity) * 0.30F;
		volume_inverter2 = updateVolumeInverter(2, speed, motorPolarity) * 0.15F;
		volume_traction0 = updateVolumeTraction(0, motorActivity * 1.50F, speed);
		volume_traction1 = updateVolumeTraction(1, motorActivity * 1.50F, speed);
		volume_traction2 = updateVolumeTraction(2, motorActivity * 1.75F, speed);

		pitch_highTension = 1.0F;
		pitch_inverter0 = 1.0F;
		pitch_inverter1 = 1.0F;
		pitch_inverter2 = 1.0F;
		pitch_traction0 = updatePitchTraction(0, speed);
		pitch_traction1 = updatePitchTraction(1, speed);
		pitch_traction2 = updatePitchTraction(2, speed);
	}


	public float updateVolumeHighTension()
	{
		float target = enginePartType.enabled.get() ? 1.0F : 0.0F;

		if (target < volume_highTension - 0.02F)
		{
			target = volume_highTension - 0.02F;
		}
		else if (target > volume_highTension + 0.02F)
		{
			target = volume_highTension + 0.02F;
		}

		return target;
	}


	public float updateVolumeInverter(int type, float speed, float motorPolarity)
	{
		float volume;

		switch (type)
		{
			case 0:
			{
				if (speed < 5.0F)
				{
					volume = speed / 5.0F;
					volume *= Math.abs(motorPolarity);
					return volume;
				}
				else if (speed < 30.0F)
				{
					volume = 1.0F - ((speed - 5.0F) / 25.0F);
					volume *= Math.abs(motorPolarity);
					return volume;
				}
				else
				{
					return 0.0F;
				}
			}

			case 1:
			{
				if (speed < 5.0F)
				{
					return 0.0F;
				}
				else if (speed < 30.0F)
				{
					volume = (speed - 5.0F) / 25.0F;
					volume *= Math.abs(motorPolarity);
					return volume;
				}
				else if (speed < 30.5F)
				{
					volume = 1.0F - ((speed - 30.0F) / 0.5F);
					volume *= Math.abs(motorPolarity);
					return volume;
				}
				else
				{
					return 0.0F;
				}
			}

			case 2:
			{
				if (speed < 30.0F)
				{
					return 0.0F;
				}
				else if (speed < 30.5F)
				{
					volume = (speed - 30.0F) / 0.5F;
					volume *= Math.abs(motorPolarity);
					return volume;
				}
				else if (speed < 50.0F)
				{
					volume = 1.0F;
					volume *= Math.abs(motorPolarity);
					return volume;
				}
				else if (speed < 100.0F)
				{
					volume = 1.0F - ((speed - 50.0F) / 50.0F);
					volume *= Math.abs(motorPolarity);
					return volume;
				}
				else
				{
					return 0.0F;
				}
			}

			default:
			{
				return 0.0F;
			}
		}
	}


	public float updateVolumeTraction(int type, float motorActivity, float speed)
	{
		switch (type)
		{
			case 0:
			{
				if (speed < 5.0F)
				{
					return Math.abs(motorActivity) * Math.abs(speed / 5.0F);
				}
				else if (speed < 20.0F)
				{
					return Math.abs(motorActivity);
				}
				else
				{
					return 0.0F;
				}
			}

			case 1:
			{
				if (speed < 20.0F)
				{
					return 0.0F;
				}
				else if (speed < 40.0F)
				{
					return Math.abs(motorActivity);
				}
				else if (speed < 80.0F)
				{
					return Math.abs(motorActivity) * (1.0F - ((Math.abs(speed) - 40.0F) / 40.0F));
				}
				else
				{
					return 0.0F;
				}
			}

			case 2:
			{
				if (speed < 40.0F)
				{
					return 0.0F;
				}
				else if (speed < 80.0F)
				{
					return Math.abs(motorActivity) * ((Math.abs(speed) - 40.0F) / 40.0F);
				}
				else
				{
					return Math.abs(motorActivity);
				}
			}

			default:
			{
				return 0.0F;
			}
		}
	}


	/*
	 * 0:  5.0  -  20.0
	 * 1: 20.0  -  80.0
	 * 2: 40.0  - 160.0
	 */


	public float updatePitchTraction(int type, float speed)
	{
		float result = 1.0F;

		switch (type)
		{
			case 0:
			{
				result = 1.0F + ((speed - 10.0F) / 10.0F);
				break;
			}

			case 1:
			{
				result = 1.0F + ((speed - 40.0F) / 40.0F);
				break;
			}

			case 2:
			{
				result = 1.0F + ((speed - 80.0F) / 80.0F);
				break;
			}

			default:
			{
				break;
			}
		}

		if (result < 0.5F)
		{
			return 0.5F;
		}
		else if (result > 2.0F)
		{
			return 2.0F;
		}

		return result;
	}


	public static class PropSoundEngineFLIRT extends AElectricEngineSoundRepeated
	{
		public PropSoundEngineFLIRT(ElectricEngineSoundsFLIRT wrapper, int index, boolean interior)
		{
			super(wrapper, wrapper.engineVehiclePart, index, String.format("%s:vehPar_engineSoundFLIRT_%d", ModData.ID, index), "electricEngineSoundsFLIRT", interior);
		}

		@Override
		protected float getPitch()
		{
			if (!(wrapper instanceof ElectricEngineSoundsFLIRT))
			{
				return 1.0F;
			}

			ElectricEngineSoundsFLIRT wrapper = (ElectricEngineSoundsFLIRT)this.wrapper;

			switch (index)
			{
				case 0:
					return wrapper.pitch_highTension;
				case 1:
					return wrapper.pitch_inverter0;
				case 2:
					return wrapper.pitch_inverter1;
				case 3:
					return wrapper.pitch_inverter2;
				case 4:
					return wrapper.pitch_traction0;
				case 5:
					return wrapper.pitch_traction1;
				case 6:
					return wrapper.pitch_traction2;
				default:
					return 1.0F;
			}
		}

		@Override
		protected float getVolume()
		{
			if (!(wrapper instanceof ElectricEngineSoundsFLIRT))
			{
				return 0.0F;
			}

			ElectricEngineSoundsFLIRT wrapper = (ElectricEngineSoundsFLIRT)this.wrapper;

			/*
			 * Make sure only interior or exterior sounds are played depending on the perspective of the player's camera.
			 */
			if (interior != null && wrapper.engineVehiclePart.getTrain().worldObj.isRemote)
			{
				if (isPlayerInside(wrapper.enginePartType.getTrain()))
				{
					if (!interior)
					{
						return 0.0F;
					}
				}
				else
				{
					if (interior)
					{
						return 0.0F;
					}
				}
			}

			switch (index)
			{
				case 0:
					return wrapper.volume_highTension;
				case 1:
					return wrapper.volume_inverter0;
				case 2:
					return wrapper.volume_inverter1;
				case 3:
					return wrapper.volume_inverter2;
				case 4:
					return wrapper.volume_traction0;
				case 5:
					return wrapper.volume_traction1;
				case 6:
					return wrapper.volume_traction2;
				default:
					return 0.0F;
			}
		}
	}
}
