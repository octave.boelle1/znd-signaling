package zoranodensha.vehicleParts.common.sounds;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropSound;



public abstract class APartSounds
{
	/** Sound volume threshold. Any sounds below this volume won't be played. */
	public static final float SOUND_THRESHOLD = 0.01F;


	/**
	 * <p>
	 * Provides a method of determining if the current client player is currently sitting inside the train associated with this bogie. If this method returns {@code true}, relevant interior sounds should be played whilst muting
	 * exterior-only sounds.
	 * </p>
	 * <p>
	 * If the player is in third-person mode, this method will return {@code false}.
	 * </p>
	 *
	 * @return {@code true} if this method is called on the client-side and the client player is sitting inside this bogie's train. Otherwise, {@code false}.
	 */
	@SideOnly(Side.CLIENT)
	public static boolean isPlayerInside(Train train)
	{
		if (train.worldObj.isRemote)
		{
			if (Minecraft.getMinecraft() != null)
			{
				if (Minecraft.getMinecraft().thePlayer != null)
				{
					if (Minecraft.getMinecraft().gameSettings != null && Minecraft.getMinecraft().gameSettings.thirdPersonView != 0)
					{
						return false;
					}
					else if (train.isPassenger(Minecraft.getMinecraft().thePlayer))
					{
						return true;
					}
				}
			}
		}

		return false;
	}


	public static abstract class AElectricEngineSound extends PropSound
	{
		/**
		 * The parent electric engine sounds wrapper class. This class will hold all the necessary volume and pitch values (stored as {@code float}s) which this sound effect class will refer to when adjusting the volume and pitch.
		 */
		protected final APartSounds wrapper;
		/**
		 * The index of this electric engine sound effect, so this class knows which values to read from the supplied wrapper class.
		 */
		protected final int index;
		protected final Boolean interior;


		/**
		 * Initialises a new instance of the {@link AElectricEngineSound} class.
		 *
		 * @param wrapper - The electric engine sounds wrapper class.
		 * @param index - The index of electric engine sound this sound property represents.
		 * @param resourceLocation - A {@code String} which points to the sound effect file that will be played when this sound effect is audible.
		 * @param name - A {@code String} that is used as the name of this Property in the NBT system.
		 */
		public AElectricEngineSound(APartSounds wrapper, VehParBase parent, int index, String resourceLocation, String name, Boolean interior)
		{
			super(parent, interior != null ? (resourceLocation + (interior ? "_int" : "_ext")) : resourceLocation, name);

			this.wrapper = wrapper;
			this.index = index;
			this.interior = interior;
		}

		@Override
		public Integer get()
		{
			return getVolume() > 0.0F ? -1 : 0;
		}

		/**
		 * Called every tick, the value returned is the current pitch value in the wrapper class.
		 */
		protected abstract float getPitch();

		/**
		 * Called every tick, the value returned is the current volume value in the wrapper class.
		 */
		protected abstract float getVolume();

		@Override
		@SideOnly(Side.CLIENT)
		protected BasicSound getNewSound(ResourceLocation resLoc)
		{
			return new BasicSound(resLoc, this)
			{
				@Override
				public float getPitch()
				{
					return ((AElectricEngineSound)prop).getPitch();
				}

				;

				@Override
				public float getVolume()
				{
					return ((AElectricEngineSound)prop).getVolume();
				}
			};
		}
	}



	/**
	 * An abstract class which helps build the foundation of a Sound Effect Property which can be added to a vehicle part, and which will automatically update its pitch and volume based on the values provided in a supplied wrapper class
	 * (e.g. the {@link ElectricEngineSounds} class).
	 *
	 * @author jaffa
	 * @see PropSound.PropSoundRepeated
	 */
	public static abstract class AElectricEngineSoundRepeated extends AElectricEngineSound
	{
		/**
		 * Initialises a new instance of the {@link AElectricEngineSound} class.
		 *
		 * @param wrapper - The electric engine sounds wrapper class.
		 * @param index - The index of electric engine sound this sound property represents.
		 * @param resourceLocation - A {@code String} which points to the sound effect file that will be played when this sound effect is audible.
		 * @param name - A {@code String} that is used as the name of this Property in the NBT system.
		 */
		public AElectricEngineSoundRepeated(APartSounds wrapper, VehParBase parent, int index, String resourceLocation, String name, Boolean interior)
		{
			super(wrapper, parent, index, resourceLocation, name, interior);
		}

		@Override
		@SideOnly(Side.CLIENT)
		protected BasicSound getNewSound(ResourceLocation resLoc)
		{
			return super.getNewSound(resLoc).setCanRepeat();
		}
	}

}
