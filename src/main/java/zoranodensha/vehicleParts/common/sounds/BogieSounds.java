package zoranodensha.vehicleParts.common.sounds;

import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.property.PropSound.PropSoundRepeated;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.common.parts.bogie.AVehParBogieImpl;



/**
 * Wrapper class that holds sounds of a bogie/ motor.
 */
public class BogieSounds extends APartSounds
{
	/** The bogie this sound wrapper belongs to. */
	protected final AVehParBogieImpl bogie;

	/*
	 * Sound properties.
	 */
	/** Property holding bogie brake sounds. */
	protected PropSoundBrake prop_soundBrakeCylinderRelease_e;
	protected PropSoundBrake prop_soundBrakeCylinderRelease_i;
	protected PropSoundBrake prop_soundBrakeSqueal_e;
	protected PropSoundBrake prop_soundBrakeSqueal_i;

	protected PropSound prop_soundRolling60e;
	protected PropSound prop_soundRolling60i;
	protected PropSound prop_soundRolling100e;
	protected PropSound prop_soundRolling100i;
	protected PropSound prop_soundRolling160e;
	protected PropSound prop_soundRolling160i;
	protected PropSound prop_soundRollingWind;
	protected PropSound prop_soundRollingFreight60e;
	protected PropSound prop_soundRollingFreight60i;
	
	protected PropSound prop_soundRubbing;

	/*
	 * Sound-related values.
	 */
	public float volumeBrakeCylinderRelease;
	public float volumeSquealFreight;
	public float volumeSquealPassenger;
	public float volumeRolling60;
	public float volumeRolling100;
	public float volumeRolling160;
	public float volumeRollingWind;
	public float volumeRollingFreight;
	
	public float volumeRubbing;
	public float volumeSwitchSlow;
	public float volumeSwitchMedium;
	public float volumeSwitchFast;

	public float pitchSquealFreight;
	public float pitchSquealPassenger;
	public float pitchRolling60;
	public float pitchRolling100;
	public float pitchRolling160;
	public float pitchRollingWind;
	public float pitchRollingFreight;



	public BogieSounds(AVehParBogieImpl bogie, EBogieStyle style)
	{
		this.bogie = bogie;

		/*
		 * Add all sound properties to the bogie.
		 */
		bogie.addProperty(prop_soundBrakeCylinderRelease_e = new PropSoundBrake(this, 0, false));
		bogie.addProperty(prop_soundBrakeCylinderRelease_i = new PropSoundBrake(this, 0, true));

		switch (style)
		{
			case FREIGHT: {
				bogie.addProperty(prop_soundBrakeSqueal_e = new PropSoundBrake(this, 2, false));
				bogie.addProperty(prop_soundBrakeSqueal_i = new PropSoundBrake(this, 2, true));
				bogie.addProperty(prop_soundRollingFreight60e = new PropSoundRolling(this, 0, false));
				bogie.addProperty(prop_soundRollingFreight60i = new PropSoundRolling(this, 0, true));
				break;
			}

			case PASSENGER: {
				bogie.addProperty(prop_soundBrakeSqueal_e = new PropSoundBrake(this, 1, false));
				bogie.addProperty(prop_soundBrakeSqueal_i = new PropSoundBrake(this, 1, true));
				bogie.addProperty(prop_soundRolling60e = new PropSoundRolling(this, 1, false));
				bogie.addProperty(prop_soundRolling60i = new PropSoundRolling(this, 1, true));
				bogie.addProperty(prop_soundRolling100e = new PropSoundRolling(this, 2, false));
				bogie.addProperty(prop_soundRolling100i = new PropSoundRolling(this, 2, true));
				bogie.addProperty(prop_soundRolling160e = new PropSoundRolling(this, 3, false));
				bogie.addProperty(prop_soundRolling160i = new PropSoundRolling(this, 3, true));
				bogie.addProperty(prop_soundRollingWind = new PropSoundRolling(this, 4, true));
				bogie.addProperty(prop_soundRubbing = new PropSoundRolling(this, 5, true)); // XXX
				break;
			}
		}
	}

	protected float getPitchRolling(int type)
	{
		switch (type)
		{
			case 0:
				return pitchRollingFreight;

			case 1:
				return pitchRolling60;

			case 2:
				return pitchRolling100;

			case 3:
				return pitchRolling160;

			case 4:
				return pitchRollingWind;

			default:
				return 1.0F;
		}
	}

	protected float getVolumeRolling(int type)
	{
		switch (type)
		{
			case 0:
				return volumeRollingFreight;

			case 1:
				return volumeRolling60;

			case 2:
				return volumeRolling100;

			case 3:
				return volumeRolling160;

			case 4:
				return volumeRollingWind;

			case 5:
				return volumeRubbing;
				
			case 6:
				return volumeSwitchSlow;
				
			case 7:
				return volumeSwitchMedium;
				
			case 8:
				return volumeSwitchFast;

			default:
				return 0.0F;
		}
	}

	// XXX
	private boolean wasOnSwitch = true;
	
	public void onUpdate()
	{
		float speed = (float)Math.abs(bogie.getTrain().getLastSpeed() * 3.6F * 20.0F);
		float brakeCylinderValueChange = bogie.type_bogie.pneumaticsBrakeCylinder.valueAfter - bogie.type_bogie.pneumaticsBrakeCylinder.valueBefore;
		boolean onCurve = bogie.type_bogie.onCurve.get();
		boolean onSwitch = bogie.type_bogie.onSwitch.get();
		

		/*
		 * Brake Cylinder Release
		 */
		{
			float newVolumeBrakeCylinderRelease = brakeCylinderValueChange * -0.005F;
			newVolumeBrakeCylinderRelease = (newVolumeBrakeCylinderRelease < SOUND_THRESHOLD) ? 0.0F : MathHelper.clamp_float(Math.abs(newVolumeBrakeCylinderRelease), SOUND_THRESHOLD, 0.8F);

			if (newVolumeBrakeCylinderRelease != 0.0F && Math.abs(newVolumeBrakeCylinderRelease) > Math.abs(volumeBrakeCylinderRelease))
			{
				volumeBrakeCylinderRelease = newVolumeBrakeCylinderRelease;
			}
			else
			{
				if (Math.abs(volumeBrakeCylinderRelease) < SOUND_THRESHOLD * 0.1F)
				{
					volumeBrakeCylinderRelease = 0.0F;
				}
				else
				{
					volumeBrakeCylinderRelease *= 0.87F;
				}
			}
		}
		
		if(onCurve && speed >= 25.0F && speed <= 45.0F)
		{
			volumeRubbing += (1.0F - volumeRubbing) * 0.10F;
		}
		else
		{
			volumeRubbing += (0.0F - volumeRubbing) * 0.20F;
		}
//		float targetSwitchSlow = 0.0F;
//		float targetSwitchMedium = 0.0F;
//		float targetSwitchFast = 0.0F;
//		if(onSwitch)
//		{
//			targetSwitchSlow = (speed >= 10.0F && speed < 50.0F) ? 1.0F : volumeSwitchSlow * 0.9F;
//			targetSwitchMedium = (speed >= 50.0F && speed < 100.0F) ? 1.0F : volumeSwitchMedium * 0.9F;
//			targetSwitchFast = (speed >= 100.0F) ? 1.0F : volumeSwitchFast * 0.9F;
//		}
//		volumeSwitchSlow += (targetSwitchSlow - volumeSwitchSlow) * 0.5F;
//		volumeSwitchMedium += (targetSwitchMedium - volumeSwitchMedium) * 0.5F;
//		volumeSwitchFast += (targetSwitchFast - volumeSwitchFast) * 0.5F;
		
		if(onSwitch && !wasOnSwitch)
		{
			double x = bogie.type_bogie.getPosX();
			double y = bogie.type_bogie.getPosY();
			double z = bogie.type_bogie.getPosZ();
			
			int type = 0;
			float pitch = 1.0F;
			
			if(speed >= 10.0F && speed < 30.0F)
			{
				type = 6;
				pitch += (speed - 30.0F) / 60.0F;
			}
			else if(speed >= 30.0F && speed < 100.0F)
			{
				type = 7;
				pitch += (speed - 65.0F) / 80.0F;
			}
			else if(speed >= 100.0F)
			{
				type = 8;
			}
			
			if(type != 0)
			{
				bogie.type_bogie.getTrain().worldObj.playSound(x, y, z, ModData.ID + ":vehPar_axisEngine_roll" + type + "_int", 1.0F, pitch, false);
			}
		}
		wasOnSwitch = onSwitch;
		

		volumeRolling60 = updateVolumeRolling(speed, 1);
		volumeRolling100 = updateVolumeRolling(speed, 2);
		volumeRolling160 = updateVolumeRolling(speed, 3);
		volumeRollingWind = updateVolumeRolling(speed, 4);
		volumeRollingFreight = updateVolumeRolling(speed, 0);
		volumeSquealFreight = updateVolumeSqueal(speed, bogie.type_bogie.brakeTempSurface.get(), EBogieStyle.FREIGHT);
		volumeSquealPassenger = updateVolumeSqueal(speed, bogie.type_bogie.brakeTempSurface.get(), EBogieStyle.PASSENGER);

		pitchRolling60 = updatePitchRolling(speed, 0);
		pitchRolling100 = updatePitchRolling(speed, 1);
		pitchRolling160 = updatePitchRolling(speed, 2);
		pitchRollingFreight = updatePitchRolling(speed, 0);
		pitchSquealFreight = updatePitchSqueal(speed, EBogieStyle.FREIGHT);
		pitchSquealPassenger = updatePitchSqueal(speed, EBogieStyle.PASSENGER);
	}

	/**
	 * Calculates the rolling sound pitch for this bogie.
	 *
	 * @return Rolling sound pitch, clamped between {@code 0.0F} and {@code 2.0F}.
	 */
	protected float updatePitchRolling(float speed, int type)
	{
		switch(type)
		{
			case 4:
			{
				if(speed <= 80.0F)
				{
					return 0.5F;
				}
				else if(speed >= 160.0F)
				{
					return 1.0F;
				}
				else
				{
					return 1.0F + ((speed - 160.0F) / 160.0F);
				}
			}

			default:
				return 1.0F;
		}

//		return MathHelper.clamp_float(0.75F + ((speed - (type * 60)) / 200.0F), 0.0F, 2.0F);
	}

	/**
	 * Calculates the squeal sound's pitch for this bogie.
	 *
	 * @return Squeal pitch, somewhere between {@code 0.0F} and {@code 1.0F}.
	 */
	protected float updatePitchSqueal(float speed, EBogieStyle style)
	{
		switch (style)
		{
			case FREIGHT: {
				if (speed < 5.0F)
				{
					return 1.0F - ((speed - 5.0F) / 256.0F);
				}
				return 1.0F;
			}

			case PASSENGER: {
				if (speed < 4.0F)
				{
					return 1.0F - (((speed - 4.0F) / 32.0F) * Math.abs(bogie.type_bogie.pneumaticsBrakeCylinder.getPressure() / 500.0F));
				}
				return 1.0F;
			}

			default: {
				return 1.0F;
			}
		}
	}

	/**
	 * Calculates the rolling sound volume for this bogie.
	 *
	 * @return Rolling sound volume, somewhere between {@code 0.0F} and
	 *         {@code 1.0F}.
	 */
	private float updateVolumeRolling(float speed, int type)
	{
		float volume = 0.0F;

		switch (type)
		{
			case 0: // Freight
			{
				if (speed > 0.0F && speed < 80.0F)
				{
					volume = speed / 80.0F;
				}
				else if (speed >= 80.0F)
				{
					volume = 1.0F;
				}

				break;
			}

			case 1: // 60 km/h
			{
				if (speed > 0.0F && speed < 60.0F)
				{
					volume = speed / 60.0F;
				}
				else if (speed >= 60.0F && speed < 100.0F)
				{
					volume = 1.0F - ((speed - 60.0F) / 40.0F);
				}

				break;
			}

			case 2: // 100 km/h
			{
				if(speed >= 60.0F && speed < 100.0F)
				{
					volume = (speed - 60.0F) / 40.0F;
				}
				else if(speed >= 100.0F && speed < 160.0F)
				{
					volume = 1.0F - ((speed - 100.0F) / 60.0F);
				}

				break;
			}

			case 3: // 160 km/h
			{
				if (speed > 100.0F && speed < 160.0F)
				{
					volume = (speed - 100.0F) / 60.0F;
				}
				else if (speed >= 160.0F)
				{
					volume = 1.0F;
				}
				break;
			}

			case 4: // Wind
			{
				if(speed > 80.0F && speed < 160.0F)
				{
					volume = (speed - 80.0F) / 160.0F;
				}
				else if(speed >= 160.0F)
				{
					volume = 0.5F;
				}
			}
		}

		return volume;

	}

	/**
	 * Calculates the squeal sound volume for this bogie.
	 *
	 * @return Squeal sound volume, somewhere between {@code 0.0F} (off) and
	 *         {@code 5.0F} (screechy af).
	 */
	private float updateVolumeSqueal(float speed, float brakeTemp, EBogieStyle style)
	{
		float squeal = bogie.type_bogie.pneumaticsBrakeCylinder.getPressure() * 0.005F;

		switch (style)
		{
			case FREIGHT: {
				if (speed < 3.0F)
				{
					squeal *= (speed / 3.0F);
				}
				else if (speed > 30.0F)
				{
					squeal *= (60.0F - speed) / 30.0F;
				}

				break;
			}

			case PASSENGER: {
				if (speed < 1.0F)
				{
					squeal *= speed;
				}
				else if (speed > 4.0F)
				{
					squeal *= (8.0F - speed) / 4.0F;
				}

				break;
			}
		}

		if (brakeTemp >= 80.0F)
		{
			squeal = 0.0F;
		}
		else if (brakeTemp > 40.0F)
		{
			squeal *= (80.0F - brakeTemp) / 40.0F;
		}

		return (squeal < SOUND_THRESHOLD) ? 0.0F : MathHelper.clamp_float(Math.abs(squeal), SOUND_THRESHOLD, 0.25F);
	}



	/**
	 * Property holding bogie brake sounds.
	 */
	public static class PropSoundBrake extends PropSoundRepeated
	{
		protected final BogieSounds wrapper;
		protected final int index;
		protected final Boolean interior;



		public PropSoundBrake(BogieSounds wrapper, int index, Boolean interior)
		{
			super(wrapper.bogie, String.format("%s:vehPar_axis_%d" + (interior != null ? (interior ? "_int" : "_ext") : ""), ModData.ID, index), "brakeSounds");
			this.wrapper = wrapper;
			this.index = index;
			this.interior = interior;
		}

		@Override
		public Integer get()
		{
			return getVolume() > 0.0F ? -1 : 0;
		}

		/**
		 * Helper method to determine volume of this sound.
		 */
		protected float getVolume()
		{
			if (interior != null && wrapper.bogie.getTrain().worldObj.isRemote)
			{
				if (isPlayerInside(wrapper.bogie.getTrain()))
				{
					if (!interior)
					{
						return 0.0F;
					}
				}
				else
				{
					if (interior)
					{
						return 0.0F;
					}
				}
			}

			switch (index)
			{
				case 0:
					return wrapper.volumeBrakeCylinderRelease;

				case 1:
					return wrapper.volumeSquealPassenger;

				case 2:
					return wrapper.volumeSquealFreight;

				default:
					return 0.0F;
			}
		}

		protected float getPitch()
		{
			switch (index)
			{
				case 1:
					return wrapper.pitchSquealPassenger;

				case 2:
					return wrapper.pitchSquealFreight;

				default:
					return 1.0F;
			}
		}

		@Override
		protected BasicSound getNewSound(ResourceLocation resLoc)
		{
			return new BasicSound(resLoc, this)
			{
				@Override
				public float getVolume()
				{
					return ((PropSoundBrake)prop).getVolume();
				};

				@Override
				public float getPitch()
				{
					return ((PropSoundBrake)prop).getPitch();
				}
			}.setCanRepeat();
		}
	}

	/**
	 * Property holding bogie rolling sounds.
	 */
	public static class PropSoundRolling extends PropSoundRepeated
	{
		protected final BogieSounds wrapper;
		protected final int index;
		protected final Boolean interior;



		public PropSoundRolling(BogieSounds wrapper, int index, Boolean interior)
		{
			super(wrapper.bogie, String.format("%s:vehPar_axisEngine_roll%d" + (interior != null ? (interior ? "_int" : "_ext") : ""), ModData.ID, index), "bogieSounds");
			this.wrapper = wrapper;
			this.index = index;
			this.interior = interior;
		}

		@Override
		protected BasicSound getNewSound(ResourceLocation resLoc)
		{
			return new BasicSound(resLoc, this)
			{
				@Override
				public float getPitch()
				{
					if (interior)
					{
						return 1.0F;
					}

					return ((PropSoundRolling)prop).getPitch();
				};

				@Override
				public float getVolume()
				{
					return ((PropSoundRolling)prop).getVolume();
				}
			}.setCanRepeat();
		}

		@Override
		public Integer get()
		{
			return getVolume() > 0.0F ? -1 : 0;
		}

		protected float getPitch()
		{
			return wrapper.getPitchRolling(index);
		}

		protected float getVolume()
		{
			if (interior != null && wrapper.bogie.getTrain().worldObj.isRemote)
			{
				if (isPlayerInside(wrapper.bogie.getTrain()))
				{
					if (!interior)
					{
						return 0.0F;
					}
				}
				else
				{
					if (interior)
					{
						return 0.0F;
					}
				}
			}

			return wrapper.getVolumeRolling(index);
		}
	}

	/**
	 * An enum which can be passed to the constructor of the {@link BogieSounds}
	 * class to configure what type of sound effects it produces.
	 *
	 * @author Jaffa
	 */
	public enum EBogieStyle
	{
		/**
		 * Sounds like a bogie of a freight train, such as from a cargo wagon or hopper.
		 */
		FREIGHT,
		/**
		 * Sounds like a passenger train bogie, which are more refined sounding than
		 * freight and don't make as much noise.
		 */
		PASSENGER
	}
}