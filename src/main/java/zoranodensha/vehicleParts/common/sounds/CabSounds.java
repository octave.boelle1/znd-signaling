package zoranodensha.vehicleParts.common.sounds;

import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import zoranodensha.api.util.ESyncDir;
import zoranodensha.api.util.SyncDir;
import zoranodensha.api.vehicles.part.property.PropSound;
import zoranodensha.api.vehicles.part.type.cab.MTMS;
import zoranodensha.api.vehicles.part.type.cab.PropReverser;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.common.parts.seat.AVehParCabBase;



public class CabSounds extends APartSounds
{
	protected final AVehParCabBase cab;

	/*
	 * Sounds
	 */
	protected PropSoundAir prop_soundBrakeValveCharge;
	protected PropSoundAir prop_soundBrakeValveVent;
	protected PropSoundAir prop_soundEmergencyValveVent;
	protected PropSoundAir prop_soundEqualisingReservoirCharge;
	protected PropSoundAir prop_soundEqualisingReservoirVent;

	protected PropSound.PropSoundRepeated prop_soundMTCSOverspeed;
	protected boolean canPlayMTCSWarn = true;
	protected PropSound prop_soundMTCSWarn;

	protected boolean canPlayEmergencyBrakeApply = true;
	protected PropSound prop_soundEmergencyBrakeApply;
	protected boolean canPlayEmergencyBrakeRelease = true;
	protected PropSound prop_soundEmergencyBrakeRelease;

	/*
	 * Sound Values
	 */
	@SyncDir(ESyncDir.NOSYNC) public float volumeBrakeValveCharge;
	@SyncDir(ESyncDir.NOSYNC) public float volumeBrakeValveVent;
	@SyncDir(ESyncDir.NOSYNC) public float volumeEmergencyValveVent;
	@SyncDir(ESyncDir.NOSYNC) public float volumeEqualisingReservoirCharge;
	@SyncDir(ESyncDir.NOSYNC) public float volumeEqualisingReservoirVent;


	public CabSounds(AVehParCabBase cab)
	{
		this.cab = cab;

		/*
		 * Add all cab sounds to the supplied cab instance.
		 */
		cab.addProperty(prop_soundBrakeValveCharge = new PropSoundAir(this, 0));
		cab.addProperty(prop_soundBrakeValveVent = new PropSoundAir(this, 1));
		cab.addProperty(prop_soundEqualisingReservoirCharge = new PropSoundAir(this, 2));
		cab.addProperty(prop_soundEqualisingReservoirVent = new PropSoundAir(this, 3));
		cab.addProperty(prop_soundEmergencyValveVent = new PropSoundAir(this, 4));

		cab.addProperty(prop_soundMTCSOverspeed = new PropSound.PropSoundRepeated(cab, String.format("%s:vehPar_cab_mtcsOverspeed", ModData.ID), "mtcsSounds"));
		cab.addProperty(prop_soundMTCSWarn = new PropSound(cab, String.format("%s:vehPar_cab_mtcsWarn", ModData.ID), "mtcsSounds"));

		cab.addProperty(prop_soundEmergencyBrakeApply = new PropSound(cab, String.format("%s:vehPar_cab_5", ModData.ID), "airSoundsExtra"));
		cab.addProperty(prop_soundEmergencyBrakeRelease = new PropSound(cab, String.format("%s:vehPar_cab_6", ModData.ID), "airSoundsExtra"));
	}

	/**
	 * Method used to update all the volumes and pitches of the contained cab sounds in this wrapper.
	 */
	public void onUpdate()
	{
		/*
		 * MTCS
		 */
		MTMS mtms = cab.type_cab.mtms;
		if (mtms != null && cab.getTrain().worldObj.isRemote && isPlayerInside(cab.getTrain()))
		{
			if (mtms.getOverspeedAmount() >= 1.0F && canPlayMTCSWarn)
			{
				prop_soundMTCSWarn.play();
				canPlayMTCSWarn = false;
			}
			else if (mtms.getOverspeedAmount() <= 0.0F)
			{
				prop_soundMTCSWarn.set(0);
				canPlayMTCSWarn = true;
			}

			if (mtms.getOverspeedAmount() > 5.0F && !mtms.penaltyBrake.get())
			{
				prop_soundMTCSWarn.set(0);
				prop_soundMTCSOverspeed.play();
			}
			else
			{
				prop_soundMTCSOverspeed.set(0);
			}
		}
		else
		{
			prop_soundMTCSOverspeed.set(0);
			prop_soundMTCSWarn.set(0);
		}

		/*
		 * Extra Air Sounds
		 */
		if (cab.type_cab.controlBrakePneumatic.get() < 1.0F && cab.type_cab.getDoForceStop())
		{
			canPlayEmergencyBrakeRelease = true;
			prop_soundEmergencyBrakeRelease.set(0);

			if (canPlayEmergencyBrakeApply)
			{
				canPlayEmergencyBrakeApply = false;
				prop_soundEmergencyBrakeApply.play();
			}
		}
		else
		{
			canPlayEmergencyBrakeApply = true;
			prop_soundEmergencyBrakeApply.set(0);

			if (canPlayEmergencyBrakeRelease)
			{
				canPlayEmergencyBrakeRelease = false;
				prop_soundEmergencyBrakeRelease.play();
			}
		}

		/*
		 * Pneumatics
		 */

		final boolean emergency = cab.type_cab.controlBrakePneumatic.get() < 1.0F && (cab.type_cab.getDoForceStop() || cab.type_cab.getReverserState().equals(PropReverser.EReverser.LOCKED));
		float brakeValveChange = cab.type_cab.brakeValve.valueModification;
		float equalisingReservoirChange = cab.type_cab.equalisingReservoir.valueModification;

		/*
		 * BV Charge
		 */
		{
			float newVolumeBrakeValveCharge = brakeValveChange / 30.0F;
			newVolumeBrakeValveCharge = (newVolumeBrakeValveCharge < SOUND_THRESHOLD) ? 0.0F : MathHelper.clamp_float(Math.abs(newVolumeBrakeValveCharge), SOUND_THRESHOLD, 1.0F);

			if (cab.getTrain().worldObj.isRemote && isPlayerInside(cab.getTrain()))
			{
				newVolumeBrakeValveCharge *= 0.50F;
			}

			if (newVolumeBrakeValveCharge != 0.0F && Math.abs(newVolumeBrakeValveCharge) > Math.abs(volumeBrakeValveCharge))
			{
				volumeBrakeValveCharge = newVolumeBrakeValveCharge;
			}
			else
			{
				if (Math.abs(volumeBrakeValveCharge) < SOUND_THRESHOLD)
				{
					volumeBrakeValveCharge = 0.0F;
				}
				else
				{
					volumeBrakeValveCharge *= 0.90F;
				}
			}
		}

		/*
		 * BV Vent
		 */
		if (cab.getTrain().worldObj.isRemote)
		{
			if (emergency)
			{
				if (Math.abs(volumeBrakeValveVent) < SOUND_THRESHOLD)
				{
					volumeBrakeValveVent = 0.0F;
				}
				else
				{
					volumeBrakeValveVent *= 0.85F;
				}
			}
			else
			{
				float newVolumeBrakeValveVent = brakeValveChange / -40.0F;
				newVolumeBrakeValveVent = (newVolumeBrakeValveVent < SOUND_THRESHOLD) ? 0.0F : MathHelper.clamp_float(Math.abs(newVolumeBrakeValveVent), SOUND_THRESHOLD, 1.0F);

				if (newVolumeBrakeValveVent != 0.0F && Math.abs(newVolumeBrakeValveVent) > Math.abs(volumeBrakeValveVent))
				{
					volumeBrakeValveVent = newVolumeBrakeValveVent;
				}
				else
				{
					if (Math.abs(volumeBrakeValveVent) < SOUND_THRESHOLD)
					{
						volumeBrakeValveVent = 0.0F;
					}
					else
					{
						volumeBrakeValveVent *= 0.88F;
					}
				}
			}
		}
		else
		{
			volumeBrakeValveVent = 0.0F;
		}

		/*
		 * BV Emergency
		 */
		if (emergency && cab.getTrain().worldObj.isRemote)
		{
			float newVolumeEmergencyValveVent = brakeValveChange / -160.0F;

			newVolumeEmergencyValveVent = (newVolumeEmergencyValveVent < SOUND_THRESHOLD) ? 0.0F : MathHelper.clamp_float(Math.abs(newVolumeEmergencyValveVent), SOUND_THRESHOLD, 0.5F);

			if (newVolumeEmergencyValveVent != 0.0F && Math.abs(newVolumeEmergencyValveVent) > Math.abs(volumeEmergencyValveVent))
			{
				volumeEmergencyValveVent = newVolumeEmergencyValveVent;
			}
			else
			{
				if (Math.abs(volumeEmergencyValveVent) < SOUND_THRESHOLD)
				{
					volumeEmergencyValveVent = 0.0F;
				}
				else
				{
					volumeEmergencyValveVent *= 0.88F;
				}
			}
		}
		else
		{
			if (Math.abs(volumeEmergencyValveVent) < SOUND_THRESHOLD)
			{
				volumeEmergencyValveVent = 0.0F;
			}
			else
			{
				volumeEmergencyValveVent *= 0.85F;
			}
		}

		/*
		 * ER Charge
		 */
		{
			float newVolumeEqualisingReservoirCharge = equalisingReservoirChange * 0.05F;
			newVolumeEqualisingReservoirCharge = (newVolumeEqualisingReservoirCharge < SOUND_THRESHOLD) ? 0.0F : MathHelper.clamp_float(Math.abs(newVolumeEqualisingReservoirCharge), SOUND_THRESHOLD, 1.0F);

			if (newVolumeEqualisingReservoirCharge != 0.0F && Math.abs(newVolumeEqualisingReservoirCharge) > Math.abs(volumeEqualisingReservoirCharge))
			{
				volumeEqualisingReservoirCharge = newVolumeEqualisingReservoirCharge;
			}
			else
			{
				if (Math.abs(volumeEqualisingReservoirCharge) < SOUND_THRESHOLD)
				{
					volumeEqualisingReservoirCharge = 0.0F;
				}
				else
				{
					volumeEqualisingReservoirCharge *= 0.88F;
				}
			}

			volumeEqualisingReservoirCharge = 0.0F;
		}

		/*
		 * ER Vent
		 */
		{
			float newVolumeEqualisingReservoirVent = equalisingReservoirChange * -0.01F;
			newVolumeEqualisingReservoirVent = (newVolumeEqualisingReservoirVent < SOUND_THRESHOLD) ? 0.0F : MathHelper.clamp_float(Math.abs(newVolumeEqualisingReservoirVent), SOUND_THRESHOLD, 1.0F);

			if (cab.getTrain().worldObj.isRemote && !isPlayerInside(cab.getTrain()))
			{
				newVolumeEqualisingReservoirVent *= 0.25F;
			}

			if (newVolumeEqualisingReservoirVent != 0.0F && Math.abs(newVolumeEqualisingReservoirVent) > Math.abs(volumeEqualisingReservoirVent))
			{
				volumeEqualisingReservoirVent = newVolumeEqualisingReservoirVent;
			}
			else
			{
				if (Math.abs(volumeEqualisingReservoirVent) < SOUND_THRESHOLD)
				{
					volumeEqualisingReservoirVent = 0.0F;
				}
				else
				{
					volumeEqualisingReservoirVent *= 0.88F;
				}
			}

			// volumeEqualisingReservoirVent = 0.0F;
		}
	}


	public static class PropSoundAir extends PropSound.PropSoundRepeated
	{
		/** The cab sound wrapper that this particular sound belongs to. */
		protected final CabSounds wrapper;

		/**
		 * The index of this sound. Each type of air sound in the cab has its own index to determine its volume and pitch.
		 */
		protected final int index;


		/**
		 * Initialises a new instance of pneumatics sound for a cab.
		 *
		 * @param wrapper The Cab Sounds wrapper that this sound belongs to.
		 * @param index - The index of the sound.
		 */
		public PropSoundAir(CabSounds wrapper, int index)
		{
			super(wrapper.cab, String.format("%s:vehPar_cab_%d", ModData.ID, index), "airSounds");

			this.wrapper = wrapper;
			this.index = index;
		}

		@Override
		public Integer get()
		{
			return getVolume() > 0.0F ? -1 : 0;
		}

		protected float getVolume()
		{
			switch (index)
			{
				case 0:
					return wrapper.volumeBrakeValveCharge;

				case 1:
					return wrapper.volumeBrakeValveVent;

				case 2:
					return wrapper.volumeEqualisingReservoirCharge;

				case 3:
					return wrapper.volumeEqualisingReservoirVent;

				case 4:
					return wrapper.volumeEmergencyValveVent;

				default:
					return 0.0F;
			}
		}

		protected float getPitch()
		{
			return 1.0F;
		}

		@Override
		protected BasicSound getNewSound(ResourceLocation resLoc)
		{
			return new BasicSound(resLoc, this)
			{
				@Override
				public float getVolume()
				{
					return ((PropSoundAir)prop).getVolume();
				}

				@Override
				public float getPitch()
				{
					return ((PropSoundAir)prop).getPitch();
				}
			}.setCanRepeat();
		}
	}
}
