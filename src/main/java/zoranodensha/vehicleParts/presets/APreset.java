package zoranodensha.vehicleParts.presets;

import java.util.ArrayList;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.Train.EDataKey;
import zoranodensha.api.vehicles.UVID;
import zoranodensha.api.vehicles.UVID.EIdentifier;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;



/**
 * Abstract preset class.
 */
public abstract class APreset
{
	public final NBTTagCompound nbt;



	public APreset(boolean isFinished, String creator, String name, int maxSpeed, int mass)
	{
		nbt = new NBTTagCompound();
		NBTTagCompound nbt0 = new NBTTagCompound();
		NBTTagList tagList = new NBTTagList();

		/* Add labels */
		nbt.setInteger(Train.EDataKey.MAX_SPEED.key, maxSpeed);
		nbt.setInteger(Train.EDataKey.MASS.key, mass);

		/* Add parts */
		OrderedPartsList parts = getParts();
		if (isFinished)
		{
			for (VehParBase part : parts)
				part.setIsFinished(true);
		}

		/* Write parts, append tags. */
		VehicleData vehicleData = new VehicleData(new UVID(creator.toUpperCase().substring(0, 5), EIdentifier.NUMBER.defValue), parts, creator, name);
		vehicleData.writeToNBT(nbt0, ETagCall.ITEM);

		tagList.appendTag(nbt0);
		nbt.setTag(EDataKey.VEHICLE_DATA.key, tagList);
	}


	/**
	 * Return a newly instantiated ArrayList containing all IVehiclePart instances that make up this vehicle.
	 */
	protected abstract OrderedPartsList getParts();

	/**
	 * Static method to retrieve a list of all default Preset files.
	 */
	public static final ArrayList<NBTTagCompound> getPresetList(boolean isFinished)
	{
		ArrayList<NBTTagCompound> list = new ArrayList<NBTTagCompound>();
		list.add((new BR101(isFinished)).nbt);
		list.add((new DBpza_HiDoorA(isFinished)).nbt);
		list.add((new DBpza_HiDoorB(isFinished)).nbt);
		list.add((new DBpza_LowDoor(isFinished)).nbt);
		list.add((new DBpzaCabCar(isFinished)).nbt);
		list.add((new FLIRT3(isFinished)).nbt);
		list.add((new FreightCar(isFinished)).nbt);
		list.add((new FreightCarContainer_2(isFinished)).nbt);
		list.add((new FreightCarHopper(isFinished)).nbt);
		list.add((new FreightCarOpen(isFinished)).nbt);
		list.add((new FreightCarOpenL(isFinished)).nbt);
		list.add((new FreightCarContainer_3(isFinished)).nbt);
		list.add((new FreightCarWood(isFinished)).nbt);
		list.add((new SlidingWallFreight_S(isFinished)).nbt);

		return list;
	}
}
