package zoranodensha.vehicleParts.presets;

import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportContainer;



public class FreightCarContainer_2 extends FreightCar
{
	public FreightCarContainer_2(boolean isFinished)
	{
		super(isFinished, "2-Container", 27);
	}

	@Override
	protected OrderedPartsList getParts()
	{
		OrderedPartsList list = super.getParts();
		float offY = -0.0955F;

		for (int i = -1; i < 2; i += 2)
		{
			VehParTransportContainer container = new VehParTransportContainer();
			container.setOffset(i * 2.55F, offY + 1.6475F, 0.0F);
			list.add(container);
		}

		return list;
	}
}
