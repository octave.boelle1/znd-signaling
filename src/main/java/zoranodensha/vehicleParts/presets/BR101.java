package zoranodensha.vehicleParts.presets;

import static zoranodensha.api.vehicles.part.type.APartTypeCoupling.COUPLING_DEFAULT_OFFSET_Y;

import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.type.PartTypeLamp.EFlareBehaviour;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.common.util.Helpers;
import zoranodensha.vehicleParts.common.parts.bogie.VehParAxisBR101;
import zoranodensha.vehicleParts.common.parts.buffer.VehParBufferBR101;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateBR101;
import zoranodensha.vehicleParts.common.parts.chassis.VehParBaseplateCabBR101;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisBR101;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisCabBR101;
import zoranodensha.vehicleParts.common.parts.coupler.VehParCouplerScrew;
import zoranodensha.vehicleParts.common.parts.engine.VehParEngineElectricBR101;
import zoranodensha.vehicleParts.common.parts.other.VehParCompressor;
import zoranodensha.vehicleParts.common.parts.other.VehParFenderBR101;
import zoranodensha.vehicleParts.common.parts.other.VehParLamp;
import zoranodensha.vehicleParts.common.parts.other.VehParOilTanksBR101;
import zoranodensha.vehicleParts.common.parts.other.VehParPantograph;
import zoranodensha.vehicleParts.common.parts.other.VehParPloughBR101;
import zoranodensha.vehicleParts.common.parts.seat.VehParCabLocomotive;



public class BR101 extends APreset
{
	public BR101(boolean isFinished)
	{
		super(isFinished, "Xenoniuss", "BR 101", 220, 84);
	}

	@Override
	protected OrderedPartsList getParts()
	{
		OrderedPartsList list = new OrderedPartsList();
		float offY = -0.0705F;
		float f = 0.0F;

		for (int i = 1; i > -2; i -= 2)
		{
			VehParBufferBR101 buffer = new VehParBufferBR101();
			buffer.setOffset(5.420F * i, COUPLING_DEFAULT_OFFSET_Y, 0.0F);
			buffer.setRotation(0.0F, f, 0.0F);
			buffer.doRenderPlate.set(true);
			list.add(buffer);

			VehParCouplerScrew coupler = new VehParCouplerScrew();
			coupler.setOffset(5.375F * i, COUPLING_DEFAULT_OFFSET_Y, 0.0F);
			coupler.setRotation(0.0F, f, 0.0F);
			coupler.type_coupling.expansion.set(0.195F);
			coupler.type_coupling.extraDimension.set(Helpers.calculateCouplerExtraDimension(buffer, coupler));
			list.add(coupler);

			VehParPloughBR101 plough = new VehParPloughBR101();
			plough.setOffset(5.17F * i, offY + 0.34F, 0.0F);
			plough.setRotation(0.0F, f, 0.0F);
			plough.setScale(1.0F, 1.5F, 1.2F);
			list.add(plough);

			VehParCabLocomotive cab = new VehParCabLocomotive();
			cab.setOffset(4.338F * i, offY + 1.412F, 0.212F * i);
			cab.setRotation(0.0F, f, 0.0F);
			cab.prop_color.set(ELiveryColor.NAVY.color());
			list.add(cab);

			VehParPantograph panto = new VehParPantograph();
			panto.setOffset(-3.02F * i, offY + 2.27F, 0.0F);
			panto.setRotation(0.0F, (f + 180) % 360, 0.0F);
			list.add(panto);

			VehParEngineElectricBR101 battery = new VehParEngineElectricBR101();
			battery.setOffset(1.44F * i, offY + 1.7F, 0.0F);
			list.add(battery);

			VehParAxisBR101 engine = new VehParAxisBR101();
			engine.setOffset(-3.72F * i, 0.4195F, 0.0F);
			list.add(engine);

			VehParChassisCabBR101 chassisCab = new VehParChassisCabBR101();
			chassisCab.setOffset(4.51F * i, offY + 1.651F, 0.0F);
			chassisCab.setRotation(0.0F, f, 0.0F);
			list.add(chassisCab);

			for (int k = -1; k <= 1; k += 2)
			{
				VehParLamp flare = new VehParLamp();
				flare.type_lamp.behaviour.set(EFlareBehaviour.FRONT_HIGH);
				flare.type_lamp.flareTilt.set(-5.0F);
				flare.setOffset(5.41F * i, offY + 1.051F, 0.49F * k);
				flare.setRotation(0.0F, f, 0.0F);
				list.add(flare);

				flare = new VehParLamp();
				flare.type_lamp.behaviour.set(EFlareBehaviour.REAR);
				flare.type_lamp.flareTilt.set(-5.0F);
				flare.color.set(ColorRGBA.RED.toHEX());
				flare.setOffset(5.41F * i, offY + 1.051F, 0.41F * k);
				flare.setRotation(0.0F, f, 0.0F);
				list.add(flare);
			}

			VehParLamp shuntFlare = new VehParLamp();
			shuntFlare.type_lamp.behaviour.set(EFlareBehaviour.FRONT_HIGH);
			shuntFlare.type_lamp.flareTilt.set(-20.0F);
			shuntFlare.setOffset(5.22F * i, offY + 1.526F, 0.0F);
			shuntFlare.setRotation(0.0F, f, 0.0F);
			list.add(shuntFlare);

			VehParBaseplateCabBR101 baseplateCab = new VehParBaseplateCabBR101();
			baseplateCab.setOffset(4.51F * i, offY + 0.69F, 0.0F);
			baseplateCab.setRotation(0.0F, f, 0.0F);
			list.add(baseplateCab);

			VehParBaseplateBR101 baseplate = new VehParBaseplateBR101();
			baseplate.setOffset(2.885F * i, offY + 0.69F, 0.0F);
			baseplate.setRotation(0.0F, f, 0.0F);
			baseplate.hasUVID.set(false);
			list.add(baseplate);

			VehParBaseplateBR101 baseplate2 = new VehParBaseplateBR101();
			baseplate2.setOffset(1.44F * i, offY + 0.69F, 0.0F);
			baseplate2.setRotation(0.0F, f, 0.0F);
			baseplate2.hasUVID.set(false);
			list.add(baseplate2);

			for (int i2 = -3; i2 < 3; ++i2)
			{
				VehParFenderBR101 fender = new VehParFenderBR101();
				fender.setOffset(i2 + 0.5F, offY + 2.33F, 0.534F * i);
				fender.setRotation(0.0F, f, 0.0F);
				fender.setScale(2.0F, 1.0F, 1.0F);
				list.add(fender);
			}

			VehParChassisBR101 chassis = new VehParChassisBR101();
			chassis.setOffset(1.44F * i, offY + 1.59F, 0.0F);
			chassis.setScale(1.0F, 0.96F, 1.0F);
			list.add(chassis);

			VehParChassisBR101 chassis2 = new VehParChassisBR101();
			chassis2.setOffset(2.885F * i, offY + 1.59F, 0.0F);
			chassis2.setScale(1.0F, 0.96F, 1.0F);
			list.add(chassis2);

			if (i == 1)
			{
				VehParEngineElectricBR101 battery2 = new VehParEngineElectricBR101();
				battery2.type_engine.hasSounds.set(true);
				battery2.setOffset(0.0F, offY + 1.7F, 0.0F);
				list.add(battery2);

				VehParBaseplateBR101 baseplate3 = new VehParBaseplateBR101();
				baseplate3.setOffset(0.0F, offY + 0.69F, 0.0F);
				baseplate3.setRotation(0.0F, f, 0.0F);
				list.add(baseplate3);

				VehParChassisBR101 chassis3 = new VehParChassisBR101();
				chassis3.setOffset(0.0F, offY + 1.59F, 0.0F);
				chassis3.setScale(1.0F, 0.96F, 1.0F);
				list.add(chassis3);

				VehParOilTanksBR101 tanks = new VehParOilTanksBR101();
				tanks.setOffset(0.0F, offY + 0.475F, 0.0F);
				tanks.setScale(1.4F, 1.0F, 1.0F);
				list.add(tanks);
				
				VehParCompressor compressor = new VehParCompressor();
				compressor.setOffset(0.0F, offY + 0.600F, 0.0F);
				list.add(compressor);
			}

			f = 180.0F;
		}
		return list;
	}
}
