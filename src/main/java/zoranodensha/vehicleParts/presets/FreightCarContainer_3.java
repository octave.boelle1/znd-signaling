package zoranodensha.vehicleParts.presets;

import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.common.util.ZnDMathHelper;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportContainer;
import zoranodensha.vehicleParts.common.parts.transport.VehParTransportTankContainer;



public class FreightCarContainer_3 extends FreightCar
{
	public FreightCarContainer_3(boolean isFinished)
	{
		super(isFinished, "3-Container", 29);
	}

	@Override
	protected OrderedPartsList getParts()
	{
		OrderedPartsList list = super.getParts();
		float offY = -0.0955F;

		final int tankIndex = ZnDMathHelper.ranInt(-1, 1);
		for (int i = -1; i < 2; ++i)
		{
			if (i == tankIndex)
			{
				VehParTransportTankContainer tank = new VehParTransportTankContainer();
				tank.setOffset(i * 3.1F, offY + 1.6475F, 0.0F);
				list.add(tank);
			}
			else
			{
				VehParTransportContainer container = new VehParTransportContainer();
				container.setOffset(i * 3.1F, offY + 1.6475F, 0.0F);
				container.pieces.set(4);
				list.add(container);
			}
		}

		return list;
	}
}
