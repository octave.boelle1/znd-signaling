package zoranodensha.vehicleParts.presets;

import static zoranodensha.api.vehicles.part.type.APartTypeCoupling.COUPLING_DEFAULT_OFFSET_Y;

import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.vehicles.part.type.PartTypeLamp.EFlareBehaviour;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.vehicleParts.common.parts.basic.VehParBasicCube;
import zoranodensha.vehicleParts.common.parts.bogie.VehParAxisFLIRT;
import zoranodensha.vehicleParts.common.parts.bogie.VehParAxisFLIRTTrailer;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisCabFLIRT;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisDoorsFLIRT;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisEndFLIRT;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisFLIRT;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisFLIRT.EChassis;
import zoranodensha.vehicleParts.common.parts.chassis.VehParChassisFillerFLIRT;
import zoranodensha.vehicleParts.common.parts.coupler.VehParCouplerSfbg;
import zoranodensha.vehicleParts.common.parts.other.VehParCompressor;
import zoranodensha.vehicleParts.common.parts.other.VehParElectricsFLIRT;
import zoranodensha.vehicleParts.common.parts.other.VehParFenderFLIRT;
import zoranodensha.vehicleParts.common.parts.other.VehParLamp;
import zoranodensha.vehicleParts.common.parts.other.VehParPantograph;
import zoranodensha.vehicleParts.common.parts.other.VehParPloughFLIRT;
import zoranodensha.vehicleParts.common.parts.seat.VehParCabPassenger;
import zoranodensha.vehicleParts.common.parts.seat.VehParSeat;



public class FLIRT3 extends APreset
{
	public FLIRT3(boolean isFinished)
	{
		super(isFinished, "Xenoniuss", "FLIRT 3 (Double)", 160, 84);
	}

	@Override
	protected OrderedPartsList getParts()
	{
		OrderedPartsList list = new OrderedPartsList();
		final float offY = 1.315F;
		float f = 0.0F;

		for (int i = 1; i > -2; i -= 2)
		{
			/* VehParPloughFLIRT */
			VehParPloughFLIRT plough = new VehParPloughFLIRT();
			plough.setOffset(12.7F * i, offY - 1.115F, 0.0F);
			plough.setRotation(0.0F, f, 0.0F);
			list.add(plough);

			/* VehParAxisFLIRT */
			VehParAxisFLIRT axis = new VehParAxisFLIRT(true);
			axis.setOffset(10.5F * i, 0.48F, 0.0F);
			list.add(axis);

			/* VehParAxisFLIRTTrailer */
			VehParAxisFLIRTTrailer trailer = new VehParAxisFLIRTTrailer();
			if (i == 1)
			{
				trailer.setOffset(0.0F, 0.48F, 0.0F);
				trailer.hasIntercirculation.set(true);
				trailer.isJacobs.set(true);
				trailer.intercirculationY.set(0.85F);
				list.add(trailer);
			}

			/* VehParElectricsFLIRT */
			for (float f2 : new float[] { 3.1F, 4.1F, 5.7F, 7.35F, 8.2F, 10.2F })
			{
				VehParElectricsFLIRT electric = new VehParElectricsFLIRT();
				electric.setOffset((f2 - 0.25F) * i, offY + ((f2 < 4.8F) ? 1.0F : 1.1F), 0.0F);
				list.add(electric);

				electric = new VehParElectricsFLIRT();
				electric.setOffset((f2 + 0.25F) * i, offY + ((f2 < 4.8F) ? 1.0F : 1.1F), 0.0F);
				list.add(electric);
			}

			/* VehParChassisFLIRT - Low-floor and destination boards */
			for (float f2 : new float[] { 4.2F, 5.2F, 6.2F })
			{
				VehParChassisFLIRT chassis = new VehParChassisFLIRT();
				chassis.setOffset(f2 * i, offY, 0.0F);
				chassis.prop_type.set((f2 == 5.2F) ? EChassis.LO_BD : EChassis.LO);
				list.add(chassis);
			}

			/* VehParChassisCabFLIRT */
			VehParChassisCabFLIRT chassisCab = new VehParChassisCabFLIRT();
			chassisCab.setOffset(12.3125F * i, offY + 0.1F, 0.0F);
			chassisCab.setRotation(0.0F, f, 0.0F);
			list.add(chassisCab);

			for (int k = -1; k <= 1; k += 2)
			{
				VehParLamp flare = new VehParLamp();
				flare.type_lamp.behaviour.set(EFlareBehaviour.FRONT);
				flare.setOffset(13.5325F * i, offY - 0.325F, 0.4F * k);
				flare.setRotation(0.0F, f, 0.0F);
				list.add(flare);

				flare = new VehParLamp();
				flare.type_lamp.behaviour.set(EFlareBehaviour.HIGH);
				flare.setOffset(13.5325F * i, offY - 0.325F, 0.5F * k);
				flare.setRotation(0.0F, f, 0.0F);
				list.add(flare);

				flare = new VehParLamp();
				flare.type_lamp.behaviour.set(EFlareBehaviour.REAR);
				flare.color.set(ColorRGBA.RED.clone().setAlpha(0.5F).toHEX());
				flare.setOffset(13.5325F * i, offY - 0.325F, 0.4F * k);
				flare.setRotation(0.0F, f, 0.0F);
				list.add(flare);
			}

			VehParLamp shuntFlare = new VehParLamp();
			shuntFlare.type_lamp.behaviour.set(EFlareBehaviour.FRONT);
			shuntFlare.setOffset(12.4325F * i, offY + 1.1F, 0.0F);
			shuntFlare.setRotation(0.0F, f, 0.0F);
			list.add(shuntFlare);

			/* VehParChassisFLIRT - Medium-floor and low-floor */
			for (float f2 : new float[] { 0.9F, 1.9F })
			{
				VehParChassisFLIRT chassis = new VehParChassisFLIRT();
				chassis.setOffset(f2 * i, offY, 0.0F);
				chassis.prop_type.set((f2 == 0.9F) ? EChassis.MD : EChassis.LO);
				chassis.hasUVID.set(f2 == 1.9F);
				list.add(chassis);
			}

			/* VehParChassisFLIRT - High-floor and transition */
			for (float f2 : new float[] { 8.5F, 9.5F })
			{
				VehParChassisFLIRT chassis = new VehParChassisFLIRT();
				chassis.setOffset(f2 * i, offY, 0.0F);
				chassis.prop_type.set((f2 == 9.5F) ? EChassis.TR : EChassis.HI);
				chassis.setRotation(0.0F, (chassis.prop_type.get() == EChassis.TR) ? f : 0.0F, 0.0F);
				chassis.colorStripesSingle.set(ELiveryColor.GOLD.color());
				list.add(chassis);
			}

			/* VehParChassisDoorsFLIRT */
			for (float f2 : new float[] { 3.05F, 7.35F })
			{
				VehParChassisDoorsFLIRT doors = new VehParChassisDoorsFLIRT();
				doors.setOffset(f2 * i, offY, 0.0F);
				list.add(doors);
			}

			/* VehParChassisEndFLIRT */
			VehParChassisEndFLIRT end = new VehParChassisEndFLIRT();
			end.setOffset(0.25F * i, offY, 0.0F);
			end.setRotation(0.0F, 180.0F + f, 0.0F);
			list.add(end);

			/* VehParChassisFillerFLIRT */
			for (float f2 : new float[] { 10.25F, 10.75F })
			{
				VehParChassisFillerFLIRT filler = new VehParChassisFillerFLIRT();
				filler.setOffset(f2 * i, offY + 0.15F, 0.0F);
				filler.setRotation(0.0F, 180.0F + f, 0.0F);
				filler.isHighFloor.set(true);
				filler.colorStripesSingle.set(ELiveryColor.GOLD.color());
				list.add(filler);
			}

			/* VehParCouplerSfbg */
			VehParCouplerSfbg coupler = new VehParCouplerSfbg();
			coupler.setOffset(13.1F * i, COUPLING_DEFAULT_OFFSET_Y, 0.0F);
			coupler.setRotation(0.0F, f, 0.0F);
			coupler.prop_type.set(2);
			list.add(coupler);

			/* VehParFenderFLIRT */
			for (float f2 : new float[] { 6.5F, 7.5F, 8.5F, 9.5F, 10.5F })
			{
				VehParFenderFLIRT fender = new VehParFenderFLIRT();
				fender.setOffset(f2 * i, offY + 1.094F, -0.6345F);
				list.add(fender);

				fender = new VehParFenderFLIRT();
				fender.setOffset(f2 * i, offY + 1.094F, 0.6345F);
				fender.setRotation(0.0F, 180.0F, 0.0F);
				list.add(fender);
			}

			/* VehParPantographBR101 */
			if (i == 1)
			{
				VehParPantograph panto = new VehParPantograph();
				panto.setOffset(1.0F, offY + 0.98F, 0.0F);
				list.add(panto);
			}

			/*
			 * Compressor
			 */
			if (i == 1)
			{
				VehParCompressor compressor = new VehParCompressor();
				compressor.type_compressor.compressorSpeed.set(1.5F);
				compressor.setRotation(0.0F, 0.0F, -90.0F);
				compressor.setOffset(-9.0F, 0.43F, 0.0F);
				list.add(compressor);
			}

			/* VehParCabPassenger */
			VehParCabPassenger cab = new VehParCabPassenger();
			cab.setOffset(12.645F * i, offY + 0.06F, 0.0F);
			cab.prop_color.set(ELiveryColor.NAVY.color().multiply(0.7F));
			cab.setRotation(0.0F, f, 0.0F);
			list.add(cab);

			/* VehParBasicCube */
			VehParBasicCube table = new VehParBasicCube();
			table.setOffset(13.26F * i, offY + 0.095F, 0.0F);
			table.setScale(0.2F, 0.1F, 0.525F);
			table.color.set(ELiveryColor.WHITE.color());
			list.add(table);

			/* VehParSeat */
			for (float[] f2 : new float[][] { { 0.545F, -0.235F, 0.45F, 180F + f }, { 1.345F, -0.235F, 0.45F, f }, { 1.78F, -0.445F, 0.45F, 90F }, { 2.23F, -0.445F, 0.45F, 90F }, { 3.995F, -0.445F, 0.45F, 180F + f },
					{ 4.795F, -0.445F, 0.45F, 180F + f }, { 5.595F, -0.445F, 0.45F, f }, { 6.395F, -0.445F, 0.45F, f }, { 8.2F, -0.135F, 0.45F, 180F + f }, { 9.05F, -0.135F, 0.45F, f }, { 9.51F, -0.135F, 0.45F, 180F + f },
					{ 10.31F, -0.135F, 0.45F, f } })
			{
				for (int j = -1; j < 2; j += 2)
				{
					VehParSeat seat = new VehParSeat();
					seat.setOffset(f2[0] * i, offY + f2[1], f2[2] * j);
					seat.setRotation(0.0F, (f2[3] % 180F != 0.0F ? -j * f2[3] : f2[3]), 0.0F);

					if (f2[0] > 8.0F)
					{
						seat.prop_color.set(ELiveryColor.NAVY.color().multiply(0.5F));
						seat.prop_colorHandles.set(ELiveryColor.GOLD.color());
						seat.setScale(0.75F, 0.75F, 0.85F);
					}
					else
					{
						seat.prop_color.set(ELiveryColor.NAVY.color());
						seat.setScale(0.75F, 0.75F, 0.75F);
					}
					list.add(seat);
				}
			}

			f = 180.0F;
		}
		return list;
	}
}
