package zoranodensha.vehicleParts.presets;

import zoranodensha.api.util.ColorRGBA;



/**
 * Default Zora no Densha livery colours.
 */
public enum ELiveryColor
{
	GOLD(0xDBB300FF),
	GREY_DARK(0x1F1F1FFF),
	GREY_LIGHT(0x6F6F6FFF),
	GREY_MEDIUM(0x3F3F3FFF),
	NAVY(0x112266FF),
	WHITE(0xEFEFEFFF);

	/** Hexadecimal color of this value. */
	private final int hex;



	private ELiveryColor(int hex)
	{
		this.hex = hex;
	}

	/**
	 * Converts this value to a color.
	 * 
	 * @return {@link zoranodensha.api.util.ColorRGBA Color} of this value.
	 */
	public ColorRGBA color()
	{
		return new ColorRGBA(hex);
	}
}