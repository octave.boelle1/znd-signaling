package zoranodensha.signals;

import net.minecraft.nbt.NBTTagCompound;



/**
 * A class representing the physical arm of a semaphore signal. A single signal instance may contain multiple instances of this class if the signal has multiple physical semaphore arms.
 * 
 * @author Jaffa
 */
public class SignalArm
{

	private static final String nbtPrefix = "SignaArm_";

	/**
	 * The ID of this signal arm, used to differentiate between other signal arms in the {@link NBTTagCompound}.
	 */
	public int ID;

	/**
	 * The current physical angle of the signal arm.
	 */
	private float angle;
	/**
	 * The positive maximum for the value of {@link SignalArm#angle}.
	 */
	private float angleMaximum;
	/**
	 * The negative maximum for the value of {@link SignalArm#angle}.
	 */
	private float angleMinimum;
	/**
	 * The angle that the signal arm will attempt to rotate to.
	 */
	private float angleTarget;
	/**
	 * The angle that the signal arm was at during the previous tick, used to calculate smoother animations.
	 */
	private float lastAngle;
	/**
	 * The current rotational velocity of the signal arm, measured in {@code degrees/tick}.
	 */
	private float velocity;



	/**
	 * Initialises a new instance of the {@link SignalArm} instance.
	 * 
	 * @param ID - The numerical ID to associate with this instance to differentiate it from other signal arms in the {@link NBTTagCompound}.
	 * @param startingAngle - The starting angle of the signal arm, in degrees.
	 * @param minimumAngle - The negative maximum angle of the signal arm, in degrees.
	 * @param maximumAngle - The positive maximum angle of the signal arm, in degrees.
	 */
	public SignalArm(int ID, float startingAngle, float minimumAngle, float maximumAngle)
	{
		/*
		 * ID
		 */
		this.ID = ID;

		/*
		 * Assign all starting values.
		 */
		this.angle = startingAngle;
		this.angleMaximum = maximumAngle;
		this.angleMinimum = minimumAngle;
		this.lastAngle = angle;
		this.velocity = 0.0F;
	}


	/**
	 * Gets the current signal arm angle in degrees for this current tick.<br>
	 * If you plan on using this value in an animation, it is recommended to also pass the {@code partialTick} parameter to this method.
	 * 
	 * @return - A {@code float}.
	 */
	public float getAngle()
	{
		if (angle < angleMinimum)
			return angleMinimum;
		if (angle > angleMaximum)
			return angleMaximum;

		return angle;
	}


	/**
	 * Gets the current signal arm angle in degrees for this current tick.<br>
	 * This value will be interpolated in such a way as to make this value appropriate for smooth animations.
	 * 
	 * @param partialTick - The current {@code partialTick} value of the game client.
	 * @return - A {@code float}.
	 */
	public float getAngle(float partialTick)
	{
		float toReturn = lastAngle + ((angle - lastAngle) * partialTick);

		if (toReturn < angleMinimum)
			return angleMinimum;
		if (toReturn > angleMaximum)
			return angleMaximum;

		return toReturn;
	}


	/**
	 * Updates this instance with information stored in the supplied {@link NBTTagCompound}.
	 */
	public void readFromNBT(NBTTagCompound nbt)
	{
		angle = nbt.getFloat(nbtPrefix + "angle");
		lastAngle = nbt.getFloat(nbtPrefix + "last_angle");
		angleMaximum = nbt.getFloat(nbtPrefix + "angle_maximum");
		angleMinimum = nbt.getFloat(nbtPrefix + "angle_minimum");
		angleTarget = nbt.getFloat(nbtPrefix + "angle_target");
		velocity = nbt.getFloat(nbtPrefix + "velocity");
	}


	/**
	 * Sets the target angle that this signal arm will attempt to rotate to.
	 * 
	 * @param angleTarget - The target angle that this signal arm will attempt to rotate to. This value is a measurement of {@code degrees}.
	 */
	public void setAngleTarget(float angleTarget)
	{
		this.angleTarget = angleTarget;
	}


	/**
	 * Performs an update tick on this signal arm. Calculations to control the signal arm rotation and velocity is controlled in this method, and should be called every tick.
	 */
	public void update()
	{
		lastAngle = angle;

		velocity += (angleTarget - angle) * 0.02F;
		angle += velocity;
		velocity *= 0.90F;

		if (angle < angleMinimum)
		{
			angle = angleMinimum;
			velocity *= -0.75F;
		}

		if (angle > angleMaximum)
		{
			angle = angleMaximum;
			velocity *= -0.75F;
		}
	}


	/**
	 * Saves the information contained in this instance to the supplied {@link NBTTagCompound}.
	 */
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setFloat(nbtPrefix + "angle", angle);
		nbt.setFloat(nbtPrefix + "last_angle", lastAngle);
		nbt.setFloat(nbtPrefix + "angle_maximum", angleMaximum);
		nbt.setFloat(nbtPrefix + "angle_minimum", angleMinimum);
		nbt.setFloat(nbtPrefix + "angle_target", angleTarget);
		nbt.setFloat(nbtPrefix + "velocity", velocity);
	}

}
