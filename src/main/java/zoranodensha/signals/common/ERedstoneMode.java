package zoranodensha.signals.common;

/**
 * Enum describing different method of signal operation when redstone signals are applied to the signal's block.
 */
public enum ERedstoneMode
{
	/**
	 * The signal functions normally. Any redstone signals into the signal have no effect.
	 */
	NORMAL,

	/**
	 * The signal functions normally, only if there is no redstone signal present. As soon as a redstone signal
	 * is detected, the signal will be set to its most restrictive indication immediately.
	 */
	REDSTONE_DISABLED,

	/**
	 * The signal functions normally, only if there is a redstone signal present. If no redstone signal is detected,
	 * the signal will be set to its most restrictive indication immediately.
	 */
	REDSTONE_ENABLED
}
