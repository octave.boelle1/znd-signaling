package zoranodensha.signals.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.logging.log4j.Level;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import zoranodensha.api.structures.signals.*;
import zoranodensha.api.structures.signals.landmark.ALandmark;
import zoranodensha.api.structures.signals.landmark.UndefinedLandmark;
import zoranodensha.api.util.ColorRGBA;
import zoranodensha.api.util.Vec4i;
import zoranodensha.common.blocks.tileEntity.TileEntitySignal;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.util.ZnDMathHelper;
import zoranodensha.trackpack.common.section.ZnD_Straight_0000_0000;


public abstract class ASignal implements ISignal
{
	/**
	 * The number of ticks between 'signal ticks' ({@link #pathTick()}).
	 */
	private static final int TICK_TIMER_MAX = 40;

	/*
	 * Signal Lamp Colours
	 */
	public static final ColorRGBA SIGNAL_GREEN = new ColorRGBA(0.15F, 1.00F, 0.30F);
	public static final ColorRGBA SIGNAL_AMBER = new ColorRGBA(1.00F, 0.75F, 0.00F);
	public static final ColorRGBA SIGNAL_RED = new ColorRGBA(1.00F, 0.05F, 0.05F);
	public static final ColorRGBA SIGNAL_LUNAR = new ColorRGBA(0.68F, 0.80F, 1.00F);
	public static final ColorRGBA SIGNAL_WHITE = new ColorRGBA(1.00F, 1.00F, 1.00F);

	/**
	 * The unlocalised name/identifier for this signal.
	 */
	protected final String unlocalisedName;

	/**
	 * The parent tile entity.
	 */
	protected TileEntitySignal tileEntity;

	protected boolean hasMagnet = false;
	protected int targetMagnetX = 0;
	protected int targetMagnetY = 0;
	protected int targetMagnetZ = 0;


	private ETrafficResult lastResult = ETrafficResult.STOP;
	/**
	 * <p>
	 * A list of coordinates which point to switches and crossovers that this signal must take control of when setting a path. If this list is {@code null} or empty, this signal will behave like a
	 * normal automatic signal.
	 * </p>
	 * <p>
	 * Each {@link Vec4i} in the list represents the location of the source block of a set of points (the {@code x, y, z} values) and the {@code w} value represents the path that the set of points
	 * must be set to before the signal can clear. For non-movable crossovers (e.g. diamond crossovers) this value can simply stay at {@code 0}.
	 * </p>
	 */
	public ArrayList<Vec4i> interlockList;
	/**
	 * {@code 0}: Route is not set.
	 * {@code 1}: Route is blocked by an obstruction.
	 * {@code 2}: Route is being set.
	 * {@code 3}: Route is set.
	 */
	protected int interlockState = 0;


	/**
	 * Hash table of additional signal properties.
	 */
	protected HashMap<String, SignalProperty> properties;

	/**
	 * Properties
	 */
	protected String KEY_ONEWAY = "oneWay";
	protected String KEY_POSTOFFSET = "postOffset";
	protected String KEY_REDSTONE = "redstoneOperation";
	protected String KEY_CHUNK_DEBUG = "chunkDebug";

	/**
	 * This signal's ID, which is used in signalling applications such as signal box
	 * operation.
	 */
	protected String ID;


	/**
	 * The length of this signal's signal block, in metres.
	 */
	protected float blockLength = 0.0F;

	/**
	 * A small message that contains debug information about the current state of the signal.
	 */
	public String note;

	/**
	 * The number of signal blocks free ahead of this signal.
	 */
	protected int blocksFree = 0;

	/**
	 * The speed limit for this signal's signal block.
	 */
	protected float speedLimit = 40.0F;


	/**
	 * The last-known speed limit for the next signal. If there isn't a signal after
	 * this one, this should just be 0.
	 */
	protected float nextSpeedLimit = 0.0F;

	/**
	 * A counter used to determine when to call the {@link #pathTick()} method.
	 */
	protected int signalTickTimer = TICK_TIMER_MAX;

	/**
	 * A list of 3D vertices which demonstrate the path the signal looks at each time it ticks. This is used for debugging purposes, e.g. to find out why a signal is showing red when the track ahead
	 * is clear (the vertices can be rendered as a line in the game world by pressing F3).
	 */
	protected ArrayList<Double> pathDebug;

	protected ALandmark landmark = null;


	/**
	 * <p>
	 * Initialises a new instance of the {@link zoranodensha.signals.common.ASignal}
	 * class.
	 * </p>
	 *
	 * @param name - The unlocalised name of this signal.
	 */
	public ASignal(String name)
	{
		this.unlocalisedName = name;

		// Initialise the properties hashmap.
		this.properties = new HashMap<String, SignalProperty>();

		this.interlockList = new ArrayList<Vec4i>();

		/*
		 * Add Properties
		 */
		properties.put(KEY_ONEWAY, new SignalProperty(0, 0, 1));
		properties.put(KEY_POSTOFFSET, new SignalProperty(1, -1, 1));
		properties.put(KEY_REDSTONE, new SignalProperty(0, 0, 2));

		properties.put(KEY_CHUNK_DEBUG, new SignalProperty(0, 0, 1));

		signalTickTimer = ZnDMathHelper.ranInt(0, TICK_TIMER_MAX);
	}


	/**
	 * Goes through all currently-interlocked track sections owned by this signal and removes them from the
	 * interlocking list, thus allowing other signals to claim those track sections. This method will not check
	 * if trains are approaching this signal, so check beforehand if you intend to safely set this signal back to
	 * STOP.
	 */
	protected void clearInterlocking()
	{
		int count = 0;

		for (Vec4i interlockPoint : interlockList)
		{
			TileEntity interlockTrack = tileEntity.getWorldObj().getTileEntity(interlockPoint.x, interlockPoint.y, interlockPoint.z);
			TileEntityTrackBase interlockTrackBase;

			if (interlockTrack instanceof TileEntityTrackBase)
			{
				interlockTrackBase = (TileEntityTrackBase) interlockTrack;

				if (interlockTrackBase.isInterlockedBy(tileEntity.getWorldObj(), this))
				{
					interlockTrackBase.interlock(tileEntity.getWorldObj(), null);
					interlockTrackBase.setField(5, -1);
					count++;
				}
			}
		}

		if (count >= 1)
		{
			ModCenter.log.printf(Level.INFO, "[%s] Released %d point(s) from my control.", getID(), count);
		}
	}


	@Override
	public void danger()
	{
		blocksFree = 0;
		markForUpdate();
	}


	@Override
	public HashMap<String, SignalProperty> getAdditionalProperties()
	{
		return this.properties;
	}


	@Override
	public float getBlockLength()
	{
		return this.blockLength;
	}


	@Override
	public ArrayList<Double> getDebugPath()
	{
		return pathDebug;
	}


	@Override
	public String getID()
	{
		if (this.ID == null)
		{
			Date date = new Date();
			DateFormat condensedFormat = new SimpleDateFormat("HHmm");
			this.ID = condensedFormat.format(date);
			return this.ID;
		}

		return this.ID;
	}


	@Override
	public SignalIndication getIndication()
	{
		return new SignalIndication(blocksFree, this.blocksFree > 0 ? speedLimit : 0, nextSpeedLimit);
	}


	@Override
	public ArrayList<Vec4i> getInterlockedTracks()
	{
		return interlockList;
	}


	/**
	 * <p>
	 * A helper method to get a signal magnet instance ({@code TileEntityTrackBase}) from the game world. <b>If no magnet is found, this method will return {@code null}.</b>
	 * </p>
	 *
	 * @param world - A world instance.
	 * @param x     - X
	 * @param y     - Y
	 * @param z     - Z
	 * @return - A {@link zoranodensha.common.blocks.tileEntity.TileEntityTrackBase} instance holding a signal magnet if one is found, otherwise {@code null}.
	 */
	public static TileEntityTrackBase getMagnet(World world, int x, int y, int z)
	{
		TileEntity tileEntity = world.getTileEntity(x, y, z);

		if (tileEntity instanceof TileEntityTrackBase)
		{
			TileEntityTrackBase track = (TileEntityTrackBase) tileEntity;

			if (track.getInstanceOfShape() instanceof ISignalMagnet)
			{
				return track;
			}
		}

		return null;
	}


	@Override
	public String getName()
	{
		return this.unlocalisedName;
	}


	/**
	 * @return - The current 'redstone operation mode' setting for this signal.
	 * @see ERedstoneMode
	 */
	public ERedstoneMode getRedstoneMode()
	{
		SignalProperty redstoneProperty = properties.get(KEY_REDSTONE);

		if (redstoneProperty != null)
		{
			switch (redstoneProperty.get())
			{
				case 0:
					return ERedstoneMode.NORMAL;

				case 1:
					return ERedstoneMode.REDSTONE_ENABLED;

				case 2:
					return ERedstoneMode.REDSTONE_DISABLED;
			}
		}

		return ERedstoneMode.NORMAL;
	}


	@Override
	public TileEntity getTileEntity()
	{
		return this.tileEntity;
	}


	@Override
	public int getX()
	{
		return tileEntity.xCoord;
	}


	@Override
	public int getY()
	{
		return tileEntity.yCoord;
	}


	@Override
	public int getZ()
	{
		return tileEntity.zCoord;
	}


	@Override
	public boolean isOneWay()
	{
		SignalProperty oneWayProperty = properties.get(KEY_ONEWAY);
		return (oneWayProperty != null) && (oneWayProperty.get() != 0);
	}


	@Override
	public void markForUpdate()
	{
		if (tileEntity != null)
		{
			tileEntity.markDirty();

			if (tileEntity.hasWorldObj())
			{
				tileEntity.getWorldObj().markBlockForUpdate(getX(), getY(), getZ());
			}
		}
	}

	@Override
	public void onTileEntityChange(TileEntitySignal tileEntitySignal)
	{
		this.tileEntity = tileEntitySignal;
		markForUpdate();
	}

	@Override
	public void onUpdate()
	{
		if (tileEntity.getWorldObj() == null)
		{
			return;
		}

		/*
		 * Server-side
		 */
		if (!tileEntity.getWorldObj().isRemote)
		{
			/*
			 * Signal Tick
			 */
			if (--signalTickTimer <= 0)
			{
				//long startTime = System.nanoTime();

				pathTick();

				// Stop measuring execution time
				/*long endTime = System.nanoTime();

				// Calculate the execution time in milliseconds
				long executionTime
						= (endTime - startTime);*/

				//System.out.println(executionTime);

				signalTickTimer = TICK_TIMER_MAX;
			}


		}


		updateLamps();



		/*
		 * Both server and client-side
		 */

	}


	@Override
	public void pathTick()
	{
		if (tileEntity.getWorldObj() == null || tileEntity.getWorldObj().isRemote)
		{
			return;
		}

		/*
		 * Check if powered.
		 */
		if (getRedstoneMode() != ERedstoneMode.NORMAL)
		{
			boolean powered = tileEntity.getWorldObj().isBlockIndirectlyGettingPowered(getX(), getY(), getZ());

			if (getRedstoneMode() == ERedstoneMode.REDSTONE_ENABLED && !powered)
			{
				danger();
				clearInterlocking();
				return;
			}
			else if (getRedstoneMode() == ERedstoneMode.REDSTONE_DISABLED && powered)
			{
				danger();
				clearInterlocking();
				return;
			}
		}

		/*
		 * Make sure there is a linked signal magnet.
		 */
		TileEntityTrackBase magnet = getMagnet(tileEntity.getWorldObj(), targetMagnetX, targetMagnetY, targetMagnetZ);
		hasMagnet = magnet != null;

		if (!hasMagnet)
		{
			danger();
			return;
		}

		/*
		 * Make sure the signal magnet track base has a magnet attached (aka it isn't
		 * just a plain track with no magnet).
		 */
		if (!magnet.getField(ZnD_Straight_0000_0000.INDEX_HASMAGNET, false, Boolean.class))
		{
			danger();
			return;
		}


		/*
		 * Assign default speed limits...
		 */
		this.speedLimit = -1.0F;
		this.nextSpeedLimit = -1.0F;

		/*
		 * Create the list to use for path debugging purposes.
		 */
		ArrayList<Float> pathDebug = new ArrayList<Float>();


		/*
		 * Determine where to start the balise pulse.
		 */
		Vec3 pulsePosition = Vec3.createVectorHelper(0.0D, 0.0D, 0.0D);
		Vec3 pulseDirection = Vec3.createVectorHelper(1.0D, 0.0D, 0.0D);
		int magnetMetaData = magnet.getOrientation();

		/*
		 * Pulse starting position
		 */
		pulsePosition.xCoord += magnet.xCoord + 0.500D;
		pulsePosition.yCoord += magnet.yCoord + 0.125D;
		pulsePosition.zCoord += magnet.zCoord + 0.500D;

		/*
		 * Pulse starting direction
		 */
		pulseDirection = BalisePulse.getSignalMagnetDirection(magnet);

		/*
		 * Create the pulse.
		 */
		/*BalisePulse approachCheckPulse = new BalisePulse(pulsePosition, Vec3.createVectorHelper(pulseDirection.xCoord * -1.0F, pulseDirection.yCoord * -1.0F, pulseDirection.zCoord * -1.0F));
		BalisePulse pulse = new BalisePulse(pulsePosition, pulseDirection);

		boolean forceChunks = properties.get(KEY_CHUNK_DEBUG).get() != 0;
		SignalBlockState approachCheckState = approachCheckPulse.getSignalBlockState(getTileEntity().getWorldObj(), false);
		SignalBlockState blockState = pulse.getSignalBlockState(getTileEntity().getWorldObj(), forceChunks);

		this.note = blockState.note;

		if (blockState.pathDebug != null)
		{
			this.pathDebug = blockState.pathDebug;
		}
		else
		{
			this.pathDebug = null;
		}

		*//*
		 * Automatic Pathing
		 *//*
		interlockState = 0;
		if (!interlockList.isEmpty())
		{
			World world = getTileEntity().getWorldObj();

			if (!approachCheckState.hasTrainInInterlocking)
			{
				danger();


				*//*
				 * Attempt to clear interlocking for any points.
				 *//*
				if (!blockState.hasTrainInInterlocking)
				{
					clearInterlocking();
				}

				return;
			}

			boolean fullyInterlocked = true;
			for (Vec4i interlockPoint : interlockList)
			{
				TileEntity interlockTrack = world.getTileEntity(interlockPoint.x, interlockPoint.y, interlockPoint.z);
				TileEntityTrackBase interlockTrackBase;

				if (interlockTrack instanceof TileEntityTrackBase)
				{
					interlockTrackBase = (TileEntityTrackBase) interlockTrack;

					if (interlockTrackBase.isInterlocked(world))
					{
						if (interlockTrackBase.isInterlockedBy(world, this))
						{
							// Interlock the same track again to keep the interlocking up to date.
							interlockTrackBase.interlock(world, this);

							// These points are interlocked by us, so we can now set the correct path.
							if (interlockTrackBase.getPath() != interlockPoint.w)
							{
								ModCenter.log.printf(Level.DEBUG, "[%s] Changing path of track.", getID());
								interlockTrackBase.setField(5, interlockPoint.w);
								interlockTrackBase.markForUpdate();
							}
							else
							{
								ModCenter.log.printf(Level.DEBUG, "[%s] Track is on the correct path.", getID());
								continue;
							}
						}
						else
						{
							ModCenter.log.printf(Level.DEBUG, "[%s] Relevant points are currently interlocked by another signal.", getID());
							interlockState = 1;
						}
					}
					else
					{
						ModCenter.log.printf(Level.DEBUG, "[%s] Interlocking relevant points now.", getID());
						interlockTrackBase.interlock(world, this);
					}
				}

				fullyInterlocked = false;
			}

			if (!fullyInterlocked)
			{
				ModCenter.log.printf(Level.DEBUG, "[%s] Not all points are interlocked.", getID());
				danger();

				if (interlockState == 0)
				{
					interlockState = 2;
				}

				return;
			}
			else
			{
				interlockState = 3;
			}
		}


		*//*
		 * Update this signal's block length.
		 *//*
		this.blockLength = Math.max(1.0F, blockState.blockLength);

		*//*
		 * Found a train
		 *//*
		if (blockState.hasTrain)
		{
			danger();

			if (interlockState >= 1)
			{
				interlockState = 1;
			}

			return;
		}

		*//*
		 * Found another signal
		 *//*
		if (blockState.nextSignal != null)
		{
			this.blocksFree = blockState.nextSignal.getIndication().getBlocksFree() + 1;
			this.nextSpeedLimit = blockState.nextSignal.getIndication().getSpeedLimit();
			this.blockLength = blockState.blockLength;

			if (this.blocksFree > 11)
			{
				this.blocksFree = 11;
			}

			markForUpdate();
		}

		*//*
		 * No signal was found.
		 *//*
		else
		{
			if (blockState.hasBuffer)
			{
				this.blocksFree = 1;

				markForUpdate();
			}
			else
			{
				danger();
			}
		}

		*//*
		 * Lowering speed limit
		 *//*
		if (blockState.isDivergingRoute)
		{
			this.speedLimit = 40.0F;
		}*/

		/*
		 * If the speed limit of the signal has changed, mark the tile for a block
		 * update to make sure the changes are visible.
		 */



		//markForUpdate();

		//markForUpdate();
	}


	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		/*
		 * Otherwise, read the data normally.
		 */
		blockLength = nbt.getFloat("blockLength");
		blocksFree = nbt.getInteger("blocksFree");
		setID(nbt.getString("ID"));
		speedLimit = nbt.getFloat("speedLimit");
		nextSpeedLimit = nbt.getFloat("nextSpeedLimit");

		note = nbt.getString("note");

		/*
		 * Magnet
		 */
		hasMagnet = nbt.getBoolean("hasMagnet");
		targetMagnetX = nbt.getInteger("magnetX");
		targetMagnetY = nbt.getInteger("magnetY");
		targetMagnetZ = nbt.getInteger("magnetZ");

		/*
		 * Interlocking
		 */
		interlockList.clear();
		for (int i = 0; i < nbt.getInteger("interlockCount"); i++)
		{
			interlockList.add(new Vec4i(nbt.getInteger("interlockX" + i), nbt.getInteger("interlockY" + i), nbt.getInteger("interlockZ" + i), nbt.getInteger("interlockW" + i)));
		}
		interlockState = nbt.getInteger("interlockState");

		/*
		 * Properties
		 */
		this.updateProperties(nbt.getCompoundTag("properties"));

		/*
		 * Reading the path debug data from the NBT tag.
		 */
		int debugSize = nbt.getInteger("pathDebugSize");
		if (debugSize > 0)
		{
			this.pathDebug = new ArrayList<Double>();
			for (int index = 0; index < debugSize; index++)
			{
				this.pathDebug.add(nbt.getDouble("pathDebug" + index));
			}
		}
		else
		{
			this.pathDebug = null;
		}
	}


	/**
	 * Any extra properties (such as signal height, design, indicator type(s), etc)
	 * can be saved to the supplied NBT Tag Compound.
	 *
	 * @param nbt - An {@link NBTTagCompound} to which extra signal properties and
	 *            settings can be saved.
	 */
	public void saveProperties(NBTTagCompound nbt)
	{
		properties.get(KEY_ONEWAY).writeToNBT(nbt, KEY_ONEWAY);
		properties.get(KEY_REDSTONE).writeToNBT(nbt, KEY_REDSTONE);
		properties.get(KEY_CHUNK_DEBUG).writeToNBT(nbt, KEY_CHUNK_DEBUG);
	}


	/**
	 * Called to assign this signal's nameplate ID.
	 *
	 * @param newID - A {@code String} to assign as the new ID. This string may be
	 *              any length, however not all signals may be able to render it
	 *              correctly if it is too long.
	 */
	public void setID(String newID)
	{
		if (this.ID == null || !this.ID.equals(newID))
		{
			this.ID = newID;
			markForUpdate();
		}
	}

	@Override
	public void setTargetMagnet(int x, int y, int z)
	{
		targetMagnetX = x;
		targetMagnetY = y;
		targetMagnetZ = z;

		markForUpdate();
	}


	/**
	 * A method which can be called each tick to update the signal lamps.
	 */
	public abstract void updateLamps();


	/**
	 * Called to update any signal properties saved in
	 * {@link ASignal#saveProperties(NBTTagCompound)}.
	 *
	 * @param nbt - An {@link NBTTagCompound} containing saved properties, the same
	 *            instance which is saved to in
	 *            {@link ASignal#saveProperties(NBTTagCompound)}.
	 */
	public void updateProperties(NBTTagCompound nbt)
	{
		properties.get(KEY_ONEWAY).readFromNBT(nbt, KEY_ONEWAY);
		properties.get(KEY_REDSTONE).readFromNBT(nbt, KEY_REDSTONE);
		properties.get(KEY_CHUNK_DEBUG).readFromNBT(nbt, KEY_CHUNK_DEBUG);
	}


	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		nbt.setFloat("blockLength", blockLength);
		nbt.setInteger("blocksFree", blocksFree);
		nbt.setString("ID", getID());
		nbt.setFloat("speedLimit", speedLimit);
		nbt.setFloat("nextSpeedLimit", nextSpeedLimit);

		if(note != null && !note.isEmpty())
		{
			nbt.setString("note", note);
		}
		else
		{
			nbt.setString("note", "Debug information unavailable.");
		}

		/*
		 * Magnet
		 */
		nbt.setBoolean("hasMagnet", hasMagnet);
		nbt.setInteger("magnetX", targetMagnetX);
		nbt.setInteger("magnetY", targetMagnetY);
		nbt.setInteger("magnetZ", targetMagnetZ);

		/*
		 * Properties
		 */
		NBTTagCompound propertiesNBT = new NBTTagCompound();
		this.saveProperties(propertiesNBT);
		nbt.setTag("properties", propertiesNBT);

		/*
		 * Interlocking
		 */
		nbt.setInteger("interlockCount", interlockList.size());
		for (int i = 0; i < interlockList.size(); i++)
		{
			nbt.setInteger("interlockX" + i, interlockList.get(i).x);
			nbt.setInteger("interlockY" + i, interlockList.get(i).y);
			nbt.setInteger("interlockZ" + i, interlockList.get(i).z);
			nbt.setInteger("interlockW" + i, interlockList.get(i).w);
		}
		nbt.setInteger("interlockState", interlockState);

		/*
		 * The path debugging data.
		 * Essentially, an array is being written as individual NBT data tags.
		 */
		if (pathDebug != null)
		{
			nbt.setInteger("pathDebugSize", pathDebug.size());

			for (int index = 0; index < pathDebug.size(); index++)
			{
				nbt.setDouble("pathDebug" + index, pathDebug.get(index));
			}
		}
		else
		{
			nbt.setInteger("pathDebugSize", 0);
		}
	}

	@Override
	public ALandmark getLandmark() {
		return landmark;
	}

	@Override
	public void setLandmark(ALandmark landmark) {
		this.landmark = landmark;

		if (!tileEntity.getWorldObj().isRemote) {
			if (landmark != null) {
				ID = landmark.getID();
				speedLimit = landmark.getResult().getSpeed();
				nextSpeedLimit = landmark.getPulse() != null ? landmark.getResult().getSpeed() : ETrafficResult.STOP.getSpeed();;

				if (landmark.next() != null)
				{
					ALandmark nextLandmark = landmark.next();
					switch(nextLandmark.getResult()) {
						case HIGH_SPEED:
							blocksFree = 3;
							break;
						case CLEAR:
							blocksFree = 2;
							break;
						case CAUTION:
							blocksFree = 1;
							break;
						default:
							blocksFree = 0;
					}
					blockLength = landmark.next().getDistance();
				}
			}
			//if (landmark != null && lastResult != landmark.getResult()) {
			//	lastResult = landmark.getResult();
				markForUpdate();
			//}
		}




	}

	public TileEntityTrackBase getMagnetTrack() {
		return tileEntity.getWorldObj() != null ? getMagnet(tileEntity.getWorldObj(), targetMagnetX, targetMagnetY, targetMagnetZ) : null;
	}
}
