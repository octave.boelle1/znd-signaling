package zoranodensha.signals.common.zd;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.model.IModelCustom;
import zoranodensha.signals.SignalResources;
import zoranodensha.signals.common.ASpeedSign;
import zoranodensha.vehicleParts.client.render.RenderUtil;



/**
 * A normal Zora no Densha track speed limit sign. This design is a white square with a black border and a number in the centre.
 * 
 * @author Jaffa
 */
public class SpeedSignZD extends ASpeedSign
{

	/**
	 * Initialises a new instance of the normal Zora no Densha speed sign class.
	 */
	public SpeedSignZD()
	{
		super("zoranodensha.speedSign.zd");

		setSpeedLimit(50);
	}


	@Override
	public void registerRecipes()
	{
		// TODO Recipe
	}


	@Override
	public void render(float partialTick)
	{
		IModelCustom model = SignalResources.model_zd_speedSign;

		GL11.glPushMatrix();
		{
			/*
			 * Texture
			 */
			Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);

			/*
			 * Position
			 */
			GL11.glTranslatef(0.0F, 0.0F, -0.25F * this.offset);

			/*
			 * Render Components
			 */
			GL11.glColor3f(0.75F, 0.75F, 0.75F);
			model.renderPart("post");

			GL11.glColor3f(0.1F, 0.1F, 0.1F);
			model.renderPart("normal_board_black");

			GL11.glColor3f(0.9F, 0.9F, 0.9F);
			model.renderPart("normal_board");

			/*
			 * Render the number itself
			 */
			GL11.glPushMatrix();
			{
				GL11.glTranslatef(0.005F, 1.389F, 0.113F);
				if (getSpeedLimit() >= 100)
				{
					GL11.glTranslatef(0.0F, 0.0F, 0.026F);
					GL11.glScalef(0.028F, 0.028F, 0.022F);
				}
				else if (getSpeedLimit() >= 10)
				{
					GL11.glScalef(0.028F, 0.028F, 0.028F);
				}
				else
				{
					GL11.glTranslatef(0.0F, 0.02F, -0.050F);
					GL11.glScalef(0.032F, 0.032F, 0.033F);
				}

				GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
				GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);

				FontRenderer font = Minecraft.getMinecraft().fontRenderer;
				final boolean unicode = font.getUnicodeFlag();

				GL11.glScalef(0.75F, 1.0F, 1.0F);
				font.drawString(String.valueOf(getSpeedLimit()), 0, 0, 0x000000);
				font.setUnicodeFlag(unicode);
			}
			GL11.glPopMatrix();

		}
		GL11.glPopMatrix();
	}


	@Override
	public void renderItem(ItemRenderType type)
	{
		if (type.equals(ItemRenderType.INVENTORY))
		{
			IModelCustom model = SignalResources.model_zd_speedSign;

			GL11.glPushMatrix();
			{
				/*
				 * Texture
				 */
				Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);

				/*
				 * Render
				 */
				GL11.glTranslatef(0.0F, -2.19F, 0.0F);
				GL11.glScalef(1.70F, 1.70F, 1.70F);

				GL11.glColor3f(0.10F, 0.10F, 0.10F);
				model.renderPart("normal_board_black");

				GL11.glColor3f(0.95F, 0.95F, 0.95F);

				model.renderPart("normal_board");

				GL11.glPushMatrix();
				{
					GL11.glTranslatef(0.005F, 1.389F, 0.14F);
					GL11.glScalef(0.032F, 0.032F, 0.024F);

					GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
					GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);

					FontRenderer font = Minecraft.getMinecraft().fontRenderer;
					final boolean unicode = font.getUnicodeFlag();

					font.drawString(String.valueOf(50), 0, 0, 0x000000);
					font.setUnicodeFlag(unicode);
				}
				GL11.glPopMatrix();

			}
			GL11.glPopMatrix();
		}
		else
		{
			this.render(0.0F);
		}
	}

}
