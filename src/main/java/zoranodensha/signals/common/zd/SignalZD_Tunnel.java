package zoranodensha.signals.common.zd;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.oredict.ShapedOreRecipe;
import org.lwjgl.opengl.GL11;
import zoranodensha.api.structures.signals.SignalProperty;
import zoranodensha.api.util.Vec2;
import zoranodensha.client.render.tileEntity.TESRSignal;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.util.ZnDMathHelper;
import zoranodensha.signals.SignalLamp;
import zoranodensha.signals.SignalResources;
import zoranodensha.signals.common.ASignal;
import zoranodensha.vehicleParts.client.render.RenderUtil;



public class SignalZD_Tunnel extends ASignal
{
	protected SignalLamp lampA, lampB, lampC, lampD, lampE, lampF;

	public SignalZD_Tunnel()
	{
		super("zoranodensha.signal.zd.tunnel");

		/*
		 * Lamps
		 */
		lampA = new SignalLamp(0, false, SignalLamp.ELampMode.GREEN, false);
		lampB = new SignalLamp(1, false, SignalLamp.ELampMode.AMBER, false);
		lampC = new SignalLamp(2, false, SignalLamp.ELampMode.RED, false);
		lampD = new SignalLamp(3, false, SignalLamp.ELampMode.LUNAR, false);
		lampE = new SignalLamp(4, false, SignalLamp.ELampMode.AMBER, false);
		lampF = new SignalLamp(5, false, SignalLamp.ELampMode.GREEN, false);
	}

	@Override public Vec2 getDimensions()
	{
		return new Vec2(0.3F, 1.0F);
	}

	@Override public boolean isRepeater()
	{
		return false;
	}

	@Override public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		lampA.readFromNBT(nbt);
		lampB.readFromNBT(nbt);
		lampC.readFromNBT(nbt);
		lampD.readFromNBT(nbt);
		lampE.readFromNBT(nbt);
		lampF.readFromNBT(nbt);
	}

	@Override public void registerRecipes()
	{
		ItemStack thisStack = new ItemStack(ModCenter.ItemSignal, 1);

		NBTTagCompound thisCompound = new NBTTagCompound();
		thisCompound.setString("name", this.getName());
		thisStack.setTagCompound(thisCompound);

		GameRegistry.addRecipe(new ShapedOreRecipe(thisStack, " L ", " BS", " C ", 'L', Blocks.redstone_lamp, 'S', "ingotSteel", 'C', "circuitBasic", 'B', "steelCasing"));
	}

	@Override public void render(float partialTick, boolean isInventory)
	{
		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		IModelCustom model = SignalResources.model_zd_tunnel;

		/*
		 * Load Properties
		 */
		SignalProperty postOffsetProperty = getAdditionalProperties().get(KEY_POSTOFFSET);
		final int offset = postOffsetProperty != null ? postOffsetProperty.get() : 0;

		GL11.glPushMatrix();
		{
			switch (MinecraftForgeClient.getRenderPass())
			{
				case -1:
				{
					GL11.glScalef(2.0F, 2.0F, 2.0F);

					/*
					 * Render all signal components.
					 */
					GL11.glColor3f(0.3F, 0.3F, 0.3F);
					model.renderPart("head");
					GL11.glColor3f(0.9F, 0.9F, 0.9F);
					model.renderPart("head_rim");
					GL11.glColor3f(0.1F, 0.1F, 0.1F);
					model.renderPart("head_face");

					break;
				}

				case 0:
				{
					GL11.glTranslatef(0.0F, 0.4F, 0.0F);

					/*
					 * Render all signal components.
					 */
					GL11.glPushMatrix();
					{
						GL11.glColor3f(0.4F, 0.4F, 0.4F);

						switch (offset)
						{
							case -1:
							{
								GL11.glTranslatef(0.0F, 0.0F, 0.5F);
								model.renderPart("baseplate_bolts_flipped");
								model.renderPart("bracket_flipped");
								break;
							}

							case 0:
							{
								break;
							}

							case 1:
							{
								GL11.glTranslatef(0.0F, 0.0F, -0.5F);
								model.renderPart("baseplate_bolts");
								model.renderPart("bracket");
								break;
							}
						}
					}
					GL11.glPopMatrix();

					GL11.glPushMatrix();
					{
						switch (offset)
						{
							case -1:
							{
								GL11.glTranslatef(0.0F, 0.0F, 0.3F);
								break;
							}

							case 1:
							{
								GL11.glTranslatef(0.0F, 0.0F, -0.3F);
								break;
							}
						}

						GL11.glColor3f(0.3F, 0.3F, 0.3F);
						model.renderPart("head");
						GL11.glColor3f(0.9F, 0.9F, 0.9F);
						model.renderPart("head_rim");
						GL11.glColor3f(0.1F, 0.1F, 0.1F);
						model.renderPart("head_face");

						/*
						 * Lamps
						 */
						RenderUtil.lightmapPush();
						GL11.glPushAttrib(GL11.GL_LIGHTING);
						{
							if (lampA.getLampType())
							{
								Minecraft.getMinecraft().renderEngine.bindTexture(SignalResources.texture_bulbLED);
							}
							else
							{
								Minecraft.getMinecraft().renderEngine.bindTexture(SignalResources.texture_bulbIncandescent);
							}

							RenderUtil.lightmapBright();
							GL11.glDisable(GL11.GL_LIGHTING);

							SIGNAL_GREEN.multiply(lampA.getIntensity(partialTick)).apply();
							model.renderPart("lamp_a");

							SIGNAL_AMBER.multiply(lampB.getIntensity(partialTick)).apply();
							model.renderPart("lamp_b");

							SIGNAL_RED.multiply(lampC.getIntensity(partialTick)).apply();
							model.renderPart("lamp_c");

							SIGNAL_LUNAR.multiply(lampD.getIntensity(partialTick)).apply();
							model.renderPart("lamp_d");

							SIGNAL_AMBER.multiply(lampE.getIntensity(partialTick)).apply();
							model.renderPart("lamp_e");

							SIGNAL_GREEN.multiply(lampF.getIntensity(partialTick)).apply();
							model.renderPart("lamp_f");
						}
						GL11.glPopAttrib();
						RenderUtil.lightmapPop();
					}
					GL11.glPopMatrix();
				}

				case 1:
				{
					GL11.glTranslatef(0.0F, 0.4F, 0.0F);

					GL11.glTranslatef(0.0F, 0.0F, -0.3F * offset);

					TESRSignal.renderLampFlare(this, lampA, 0.141F, 0.65F, 0.0F, partialTick, 0.0F);
					TESRSignal.renderLampFlare(this, lampB, 0.142F, 0.50F, 0.0F, partialTick, 0.0F);
					TESRSignal.renderLampFlare(this, lampC, 0.143F, 0.35F, 0.0F, partialTick, 0.0F);
					TESRSignal.renderLampFlare(this, lampD, 0.144F, -0.03F, 0.0F, partialTick, 0.0F);
					TESRSignal.renderLampFlare(this, lampE, 0.145F, -0.18F, 0.0F, partialTick, 0.0F);
					TESRSignal.renderLampFlare(this, lampF, 0.146F, -0.33F, 0.0F, partialTick, 0.0F);

					break;
				}
			}
		}
		GL11.glPopMatrix();
	}

	@Override public void renderItem(IItemRenderer.ItemRenderType type)
	{
		this.render(0.0F, type == IItemRenderer.ItemRenderType.INVENTORY);
	}

	@Override public void saveProperties(NBTTagCompound nbt)
	{
		super.saveProperties(nbt);

		properties.get(KEY_POSTOFFSET).writeToNBT(nbt, KEY_POSTOFFSET);
	}

	@Override public void updateLamps()
	{
		final boolean flash = System.currentTimeMillis() % 700 > 300;
		final int lampCycle = (int)((System.currentTimeMillis() % 1000) / 250);

		lampA.setState(false);
		lampB.setState(false);
		lampC.setState(false);
		lampD.setState(false);
		lampE.setState(false);
		lampF.setState(false);

		if (hasMagnet)
		{
			final boolean turnout = speedLimit >= 0.0F && speedLimit <= 40.0F;
			final boolean nextTurnout = nextSpeedLimit >= 0.0F && nextSpeedLimit <= 40.0F;

			if (!turnout && !nextTurnout)
			{
				if (blocksFree <= 0)
				{
					// STOP
					lampC.setState(true);
				}
				else if (blocksFree == 1)
				{
					// CAUTION
					lampB.setState(true);
				}
				else if (blocksFree == 2)
				{
					// CLEAR
					lampA.setState(true);
				}
				else if (blocksFree >= 3)
				{
					// HIGH SPEED
					lampA.setState(true);
					lampF.setState(true);
				}
			}
			else if (turnout && !nextTurnout)
			{
				if (blocksFree <= 0)
				{
					// STOP
					lampC.setState(true);
				}
				else if (blocksFree == 1)
				{
					// CAUTION SLOW
					lampB.setState(true);
					lampE.setState(true);
				}
				else if (blocksFree >= 2)
				{
					// CLEAR SLOW
					lampB.setState(true);
					lampF.setState(true);
				}
			}
			else if (!turnout && nextTurnout)
			{
				if (blocksFree <= 0)
				{
					// STOP
					lampC.setState(true);
				}
				else if (blocksFree == 1)
				{
					// CAUTION
					lampB.setState(true);
				}
				else if (blocksFree >= 2)
				{
					// EXPECT SLOW
					lampB.setState(flash);
				}
			}
			else if (turnout && nextTurnout)
			{
				if (blocksFree <= 0)
				{
					// STOP
					lampC.setState(true);
				}
				else if (blocksFree == 1)
				{
					// CAUTION SLOW
					lampB.setState(true);
					lampE.setState(true);
				}
				else if (blocksFree >= 2)
				{
					// SUSTAINED SLOW
					lampB.setState(true);
					lampE.setState(flash);
				}
			}

			// Route Indicator
			switch (interlockState)
			{
				case 1:
				{
					lampD.setState(true);
					break;
				}

				case 2:
				{
					lampD.setState(flash);
					break;
				}

				case 3:
				default:
				{
					break;
				}
			}
		}
		else
		{
			/*
			 * When not connected to a magnet, show a blank signal.
			 */
		}

		lampA.update();
		lampB.update();
		lampC.update();
		lampD.update();
		lampE.update();
		lampF.update();
	}

	@Override public void updateProperties(NBTTagCompound nbt)
	{
		super.updateProperties(nbt);

		properties.get(KEY_POSTOFFSET).readFromNBT(nbt, KEY_POSTOFFSET);
	}

	@Override public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);

		lampA.writeToNBT(nbt);
		lampB.writeToNBT(nbt);
		lampC.writeToNBT(nbt);
		lampD.writeToNBT(nbt);
		lampE.writeToNBT(nbt);
		lampF.writeToNBT(nbt);
	}
}
