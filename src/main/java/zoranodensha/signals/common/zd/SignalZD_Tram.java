package zoranodensha.signals.common.zd;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.structures.signals.SignalProperty;
import zoranodensha.api.util.Vec2;
import zoranodensha.client.render.tileEntity.TESRSignal;
import zoranodensha.common.core.ModCenter;
import zoranodensha.signals.SignalLamp;
import zoranodensha.signals.SignalLamp.ELampMode;
import zoranodensha.signals.SignalResources;
import zoranodensha.signals.common.ASignal;
import zoranodensha.vehicleParts.client.render.RenderUtil;

import java.util.HashMap;
import java.util.Map;


public class SignalZD_Tram extends ASignal
{
	/*
	 * Lamps
	 */
	protected SignalLamp lampA, lampB, lampC, lampD, lampE, lampF;



	/**
	 * Initialises a new instance of the
	 * {@link zoranodensha.signals.common.zd.SignalZD_Tram} class.
	 */
	public SignalZD_Tram()
	{
		super("zoranodensha.signal.zd.tram");

		/*
		 * Lamps
		 */
		lampA = new SignalLamp(0, false, ELampMode.AMBER, true);
		lampB = new SignalLamp(1, false, ELampMode.AMBER, true);
		lampC = new SignalLamp(2, false, ELampMode.WHITE, true);
		lampD = new SignalLamp(3, false, ELampMode.WHITE, true);
		lampE = new SignalLamp(4, false, ELampMode.WHITE, true);
		lampF = new SignalLamp(5, false, ELampMode.WHITE, true);
	}

	@Override
	public Vec2 getDimensions()
	{
		return new Vec2(0.3F, 3.75F);
	}

	@Override
	public boolean isRepeater()
	{
		return false;
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		lampA.readFromNBT(nbt);
		lampB.readFromNBT(nbt);
		lampC.readFromNBT(nbt);
		lampD.readFromNBT(nbt);
		lampE.readFromNBT(nbt);
		lampF.readFromNBT(nbt);
	}

	@Override
	public void registerRecipes()
	{
		ItemStack thisStack = new ItemStack(ModCenter.ItemSignal, 1);

		NBTTagCompound thisCompound = new NBTTagCompound();
		thisCompound.setString("name", this.getName());
		thisStack.setTagCompound(thisCompound);

		GameRegistry.addRecipe(new ShapedOreRecipe(thisStack, " L ", " B ", "SCS", 'L', Blocks.redstone_lamp, 'S', "ingotSteel", 'C', "circuitBasic", 'B', "steelCasing"));
	}

	@Override
	public void render(float partialTick, boolean isInventory)
	{
		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		IModelCustom model = SignalResources.model_zd_block;

		/*
		 * Load Properties
		 */
		SignalProperty postOffsetProperty = getAdditionalProperties().get(KEY_POSTOFFSET);
		final int offset = postOffsetProperty != null ? postOffsetProperty.get() : 0;

		GL11.glPushMatrix();
		{
			switch (MinecraftForgeClient.getRenderPass())
			{
				case -1: {
					GL11.glTranslatef(0.0F, -2.0F, 0.0F);

					/*
					 * Render all signal components.
					 */
					GL11.glColor3f(0.7F, 0.7F, 0.7F);
					if (!isInventory) {
						model.renderPart("post");

						model.renderPart("cage");
					}
					GL11.glColor3f(0.3F, 0.3F, 0.3F);
					model.renderPart("head_top");
					model.renderPart("head_bottom");
					GL11.glColor3f(0.9F, 0.9F, 0.9F);
					model.renderPart("head_top_rim");
					model.renderPart("head_bottom_rim");
					GL11.glColor3f(0.1F, 0.1F, 0.1F);
					model.renderPart("head_top_covers");
					model.renderPart("head_top_face");
					model.renderPart("head_bottom_face");

					break;
				}

				case 0: {
					/*
					 * Move the signal to the correct spot.
					 */
					GL11.glTranslatef(0.0F, 0.0F, -0.25F * offset);

					/*
					 * Render all signal components.
					 */
					GL11.glColor3f(0.4F, 0.4F, 0.4F);
					model.renderPart("baseplate");
					model.renderPart("baseplate_panels");
					GL11.glColor3f(0.3F, 0.3F, 0.3F);
					model.renderPart("baseplate_bolts");
					GL11.glColor3f(0.7F, 0.7F, 0.7F);
					model.renderPart("post");
					model.renderPart("cage");
					GL11.glColor3f(0.2F, 0.2F, 0.2F);
					model.renderPart("post_plate_black");
					GL11.glColor3f(1.0F, 1.0F, 0.0F);
					model.renderPart("post_plate_yellow");
					GL11.glColor3f(0.9F, 0.9F, 0.9F);
					model.renderPart("nameplate");

					GL11.glColor3f(0.3F, 0.3F, 0.3F);
					model.renderPart("head_top");
					GL11.glColor3f(0.9F, 0.9F, 0.9F);
					model.renderPart("head_top_rim");

					GL11.glColor3f(0.1F, 0.1F, 0.1F);
					model.renderPart("head_top_covers");
					model.renderPart("head_top_face");

					if (interlockList.size() > 0)
					{
						GL11.glColor3f(0.3F, 0.3F, 0.3F);
						model.renderPart("head_bottom");
						GL11.glColor3f(0.9F, 0.9F, 0.9F);
						model.renderPart("head_bottom_rim");
						GL11.glColor3f(0.1F, 0.1F, 0.1F);
						model.renderPart("head_bottom_face");
					}

					/*
					 * Nameplate
					 */
					if (this.getID().length() >= 4)
					{
						TESRSignal.renderSignalID(this.getID().substring(0, 2), 0.14F, 1.90F, 0.059F, 0.0F, 0x000000, 1.25F);
						TESRSignal.renderSignalID(this.getID().substring(2, 4), 0.14F, 1.81F, 0.059F, 0.0F, 0x000000, 1.25F);
						Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
						GL11.glColor3f(1.0F, 1.0F, 1.0F);
					}

					/*
					 * Lamps
					 */
					RenderUtil.lightmapPush();
					GL11.glPushAttrib(GL11.GL_LIGHTING);
					{
						if (lampA.getLampType())
						{
							Minecraft.getMinecraft().renderEngine.bindTexture(SignalResources.texture_bulbLED);
						}
						else
						{
							Minecraft.getMinecraft().renderEngine.bindTexture(SignalResources.texture_bulbIncandescent);
						}

						RenderUtil.lightmapBright();
						GL11.glDisable(GL11.GL_LIGHTING);


						SIGNAL_AMBER.multiply(isInventory ? 1.0F : lampA.getIntensity(partialTick)).apply();
						model.renderPart("lamp_a");

						SIGNAL_AMBER.multiply(lampB.getIntensity(partialTick)).apply();
						model.renderPart("lamp_b");

						SIGNAL_WHITE.multiply(lampC.getIntensity(partialTick)).apply();
						model.renderPart("lamp_c");

						SIGNAL_WHITE.multiply(lampD.getIntensity(partialTick)).apply();
						model.renderPart("lamp_d");

						SIGNAL_WHITE.multiply(lampE.getIntensity(partialTick)).apply();
						model.renderPart("lamp_e");

						SIGNAL_WHITE.multiply(lampF.getIntensity(partialTick)).apply();
						model.renderPart("lamp_f");

						/*
						 * Route Indicator
						 */
						if (interlockList != null && interlockList.size() != 0 && interlockState >= 1)
						{
							Minecraft.getMinecraft().renderEngine.bindTexture(SignalResources.texture_bulbLED_quad);

							GL11.glColor3f(1.0F, 1.0F, 1.0F);
							switch (interlockState)
							{
								case 1:
									model.renderPart("route_blocked");
									break;

								case 2:
									model.renderPart("route_interlocking");
									break;

								case 3:
									model.renderPart("route_set");
									break;
							}
						}
					}
					GL11.glPopAttrib();
					RenderUtil.lightmapPop();

					break;
				}

				case 1: {
					// Move the signal to the side so it doesn't come too close to the tracks.
					GL11.glTranslatef(0.0F, 0.0F, -0.25F * offset);

					TESRSignal.renderLampFlare(this, lampA, 0.141F, 3.480F, 0.0875F, partialTick, 0.0F);
					TESRSignal.renderLampFlare(this, lampB, 0.142F, 3.480F, -0.0875F, partialTick, 0.0F);
					TESRSignal.renderLampFlare(this, lampC, 0.143F, 3.340F, 0.0875F, partialTick, 0.0F);
					TESRSignal.renderLampFlare(this, lampD, 0.144F, 3.340F, -0.0875F, partialTick, 0.0F);
					TESRSignal.renderLampFlare(this, lampE, 0.145F, 3.180F, 0.0875F, partialTick, 0.0F);
					TESRSignal.renderLampFlare(this, lampF, 0.146F, 3.180F, -0.0875F, partialTick, 0.0F);
					break;
				}
			}
		}
		GL11.glPopMatrix();
	}

	@Override
	public void renderItem(ItemRenderType type)
	{
		this.render(0.0F, type == ItemRenderType.INVENTORY);
	}

	@Override
	public void saveProperties(NBTTagCompound nbt)
	{
		super.saveProperties(nbt);

		properties.get(KEY_POSTOFFSET).writeToNBT(nbt, KEY_POSTOFFSET);
	}

	@Override
	public void updateLamps()
	{
		final boolean flash = System.currentTimeMillis() % 700 > 300;

		Map<SignalLamp, Boolean> lampStates = createLampStateMap();

		if (hasMagnet)
		{
			final boolean turnout = speedLimit >= 0.0F && speedLimit <= 40.0F;
			final boolean nextTurnout = nextSpeedLimit >= 0.0F && nextSpeedLimit <= 40.0F;

			if (!turnout && !nextTurnout)
			{
				if (blocksFree <= 0)
				{
					// STOP
					lampStates.put(lampC, true);
					lampStates.put(lampD, true);
				}
				else if (blocksFree >= 1)
				{
					// CLEAR
					lampStates.put(lampC, true);
					lampStates.put(lampD, true);
					lampStates.put(lampE, true);
					lampStates.put(lampF, true);
				}
			}
			else if (turnout && !nextTurnout)
			{
				if (blocksFree <= 0)
				{
					// STOP
					lampStates.put(lampC, true);
					lampStates.put(lampD, true);
				}
				else if (blocksFree >= 1)
				{
					// CLEAR (TURNOUT)
					lampStates.put(lampA, flash);
					lampStates.put(lampB, flash);
					lampStates.put(lampC, flash);
					lampStates.put(lampD, flash);
					lampStates.put(lampE, true);
					lampStates.put(lampF, true);
				}
			}
			else if (!turnout && nextTurnout)
			{
				if (blocksFree <= 0)
				{
					// STOP
					lampStates.put(lampC, true);
					lampStates.put(lampD, true);
				}
				else if (blocksFree >= 1)
				{
					// CLEAR
					lampStates.put(lampC, true);
					lampStates.put(lampD, true);
					lampStates.put(lampE, true);
					lampStates.put(lampF, true);
				}
			}
			else if (turnout && nextTurnout)
			{
				if (blocksFree <= 0)
				{
					// STOP
					lampC.setState(true);
					lampD.setState(true);
				}
				else if (blocksFree >= 1)
				{
					// CLEAR (TURNOUT)
					lampStates.put(lampA, flash);
					lampStates.put(lampB, flash);
					lampStates.put(lampC, flash);
					lampStates.put(lampD, flash);
					lampStates.put(lampE, true);
					lampStates.put(lampF, true);
				}
			}
		}
		else
		{
			/*
			 * When not connected to a magnet, show a blank signal.
			 */
		}

		lampA.update();
		lampB.update();
		lampC.update();
		lampD.update();
		lampE.update();
		lampF.update();
	}

	@Override
	public void updateProperties(NBTTagCompound nbt)
	{
		super.updateProperties(nbt);

		properties.get(KEY_POSTOFFSET).readFromNBT(nbt, KEY_POSTOFFSET);
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);

		lampA.writeToNBT(nbt);
		lampB.writeToNBT(nbt);
		lampC.writeToNBT(nbt);
		lampD.writeToNBT(nbt);
		lampE.writeToNBT(nbt);
		lampF.writeToNBT(nbt);
	}

	private Map<SignalLamp,Boolean> createLampStateMap() {
		Map<SignalLamp,Boolean> lampStates = new HashMap<SignalLamp,Boolean>();
		lampStates.put(lampA, false);
		lampStates.put(lampB, false);
		lampStates.put(lampC, false);
		lampStates.put(lampD, false);
		lampStates.put(lampE, false);
		lampStates.put(lampF, false);
		return lampStates;
	}
}
