package zoranodensha.signals.common.zd;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.structures.signals.SignalProperty;
import zoranodensha.api.util.Vec2;
import zoranodensha.client.render.tileEntity.TESRSignal;
import zoranodensha.common.core.ModCenter;
import zoranodensha.signals.SignalLamp;
import zoranodensha.signals.SignalResources;
import zoranodensha.signals.SignalLamp.ELampMode;
import zoranodensha.signals.common.ASignal;
import zoranodensha.vehicleParts.client.render.RenderUtil;



public class SignalZD_Dwarf extends ASignal
{

	/*
	 * Lamps
	 */
	protected SignalLamp lampA, lampB;



	/**
	 * Initialises a new instance of the {@link zoranodensha.signals.common.zd.SignalZD_Dwarf} class.
	 */
	public SignalZD_Dwarf()
	{
		super("zoranodensha.signal.zd.dwarf");

		/*
		 * Lamps
		 */
		lampA = new SignalLamp(0, false, ELampMode.RED, true);
		lampB = new SignalLamp(1, false, ELampMode.WHITE, true);
	}

	@Override
	public Vec2 getDimensions()
	{
		return new Vec2(0.35F, 0.80F);
	}

	@Override
	public boolean isRepeater()
	{
		return false;
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		lampA.readFromNBT(nbt);
		lampB.readFromNBT(nbt);
	}

	@Override
	public void registerRecipes()
	{
		ItemStack thisStack = new ItemStack(ModCenter.ItemSignal, 1);

		NBTTagCompound thisCompound = new NBTTagCompound();
		thisCompound.setString("name", this.getName());
		thisStack.setTagCompound(thisCompound);

		GameRegistry.addRecipe(new ShapedOreRecipe(thisStack, " L ", "SCS", 'L', Blocks.redstone_lamp, 'S', "ingotSteel", 'C', "circuitBasic", 'B', "steelCasing"));
	}

	@Override
	public void render(float partialTick, boolean isInventory)
	{
		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		IModelCustom model = SignalResources.model_zd_dwarf;

		/*
		 * Load Properties
		 */
		SignalProperty postOffsetProperty = getAdditionalProperties().get(KEY_POSTOFFSET);
		final int offset = postOffsetProperty != null ? postOffsetProperty.get() : 0;

		GL11.glPushMatrix();
		{
			switch (MinecraftForgeClient.getRenderPass())
			{
				case -1: {

					GL11.glScalef(2.0F, 2.0F, 2.0F);

					/*
					 * Render all signal components.
					 */
					GL11.glColor3f(0.7F, 0.7F, 0.7F);
					model.renderPart("baseplate");
					GL11.glColor3f(0.4F, 0.4F, 0.4F);
					model.renderPart("baseplate_bolts");
					model.renderPart("bracket");

					GL11.glColor3f(0.3F, 0.3F, 0.3F);
					model.renderPart("head");
					GL11.glColor3f(0.9F, 0.9F, 0.9F);
					model.renderPart("head_rim");
					GL11.glColor3f(0.1F, 0.1F, 0.1F);
					model.renderPart("head_covers");
					model.renderPart("head_face");

					break;
				}

				case 0: {
					/*
					 * Move the signal to the correct spot.
					 */
					GL11.glTranslatef(0.0F, 0.0F, -0.25F * offset);

					/*
					 * Render all signal components.
					 */
					GL11.glColor3f(0.7F, 0.7F, 0.7F);
					model.renderPart("baseplate");
					GL11.glColor3f(0.4F, 0.4F, 0.4F);
					model.renderPart("baseplate_bolts");
					model.renderPart("bracket");

					GL11.glColor3f(0.3F, 0.3F, 0.3F);
					model.renderPart("head");
					GL11.glColor3f(0.9F, 0.9F, 0.9F);
					model.renderPart("head_rim");
					GL11.glColor3f(0.1F, 0.1F, 0.1F);
					model.renderPart("head_covers");
					model.renderPart("head_face");

					/*
					 * Lamps
					 */
					RenderUtil.lightmapPush();
					GL11.glPushAttrib(GL11.GL_LIGHTING);
					{
						if (lampA.getLampType())
						{
							Minecraft.getMinecraft().renderEngine.bindTexture(SignalResources.texture_bulbLED);
						}
						else
						{
							Minecraft.getMinecraft().renderEngine.bindTexture(SignalResources.texture_bulbIncandescent);
						}

						RenderUtil.lightmapBright();
						GL11.glDisable(GL11.GL_LIGHTING);

						SIGNAL_RED.multiply(lampA.getIntensity(partialTick)).apply();
						model.renderPart("lamp_a");

						SIGNAL_WHITE.multiply(lampB.getIntensity(partialTick)).apply();
						model.renderPart("lamp_b");
					}
					GL11.glPopAttrib();
					RenderUtil.lightmapPop();

					break;
				}

				case 1: {
					// Move the signal to the side so it doesn't come too close to the tracks.
					GL11.glTranslatef(0.0F, 0.0F, -0.25F * offset);

					TESRSignal.renderLampFlare(this, lampA, 0.0F, 0.620F, 0.0F, partialTick, 15.0F);
					TESRSignal.renderLampFlare(this, lampB, 0.03F, 0.480F, 0.0F, partialTick, 15.0F);
					break;
				}
			}
		}
		GL11.glPopMatrix();
	}

	@Override
	public void renderItem(ItemRenderType type)
	{
		this.render(0.0F, type == ItemRenderType.INVENTORY);
	}

	@Override
	public void saveProperties(NBTTagCompound nbt)
	{
		super.saveProperties(nbt);

		properties.get(KEY_POSTOFFSET).writeToNBT(nbt, KEY_POSTOFFSET);
	}

	@Override
	public void updateLamps()
	{
		lampA.setState(false);
		lampB.setState(false);

		if (hasMagnet)
		{
			if (blocksFree <= 0)
			{
				lampA.setState(true);
			}
			else if (blocksFree >= 1)
			{
				lampB.setState(true);
			}
		}
		else
		{
			/*
			 * Signal will remain blank
			 */
		}

		lampA.update();
		lampB.update();
	}

	@Override
	public void updateProperties(NBTTagCompound nbt)
	{
		super.updateProperties(nbt);

		properties.get(KEY_POSTOFFSET).readFromNBT(nbt, KEY_POSTOFFSET);
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);

		lampA.writeToNBT(nbt);
		lampB.writeToNBT(nbt);
	}

}
