package zoranodensha.signals.common.zd;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.client.Minecraft;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.IItemRenderer.ItemRenderType;
import net.minecraftforge.client.model.IModelCustom;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.structures.signals.SignalProperty;
import zoranodensha.api.util.Vec2;
import zoranodensha.client.render.tileEntity.TESRSignal;
import zoranodensha.common.core.ModCenter;
import zoranodensha.signals.SignalLamp;
import zoranodensha.signals.SignalLamp.ELampMode;
import zoranodensha.signals.SignalResources;
import zoranodensha.signals.common.ASignal;
import zoranodensha.vehicleParts.client.render.RenderUtil;



public class SignalZD_Flag extends ASignal
{
	/*
	 * Lamps
	 */
	protected SignalLamp lamp;



	public SignalZD_Flag()
	{
		super("zoranodensha.signal.zd.flag");

		/*
		 * Lamp
		 */
		lamp = new SignalLamp(0, false, ELampMode.WHITE, true);
	}

	@Override
	public Vec2 getDimensions()
	{
		return new Vec2(0.3F, 2.7F);
	}

	@Override
	public boolean isRepeater()
	{
		return false;
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		lamp.readFromNBT(nbt);
	}

	@Override
	public void registerRecipes()
	{
		ItemStack thisStack = new ItemStack(ModCenter.ItemSignal, 1);

		NBTTagCompound thisCompound = new NBTTagCompound();
		thisCompound.setString("name", this.getName());
		thisStack.setTagCompound(thisCompound);

		GameRegistry.addRecipe(new ShapedOreRecipe(thisStack, " F ", " B ", "SCS", 'F', "plateSteel", 'S', "ingotSteel", 'C', "circuitBasic", 'B', "steelCasing"));
	}

	@Override
	public void render(float partialTick, boolean isInventory)
	{
		Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
		IModelCustom model = SignalResources.model_zd_flag;

		/*
		 * Load Properties
		 */
		SignalProperty postOffsetProperty = getAdditionalProperties().get(KEY_POSTOFFSET);
		final int offset = postOffsetProperty != null ? postOffsetProperty.get() : 0;

		GL11.glPushMatrix();
		{
			switch (MinecraftForgeClient.getRenderPass())
			{

				case -1: {
					GL11.glTranslatef(0.0F, -1.0F, 0.0F);
					/*
					 * Render all components
					 */
					GL11.glColor3f(0.7F, 0.7F, 0.7F);
					model.renderPart("post");

					GL11.glColor3f(0.9F, 0.9F, 0.9F);
					model.renderPart("nameplate");

					GL11.glPushMatrix();
					{
						GL11.glTranslatef(-0.04F, 2.25F, 0.0F);
						GL11.glScalef(0.8F, 0.8F, 0.8F);

						GL11.glColor3f(0.6F, 0.6F, 0.6F);
						model.renderPart("flag_frame");

						GL11.glColor3f(0.5F, 0.5F, 0.5F);
						model.renderPart("flag");

						GL11.glColor3f(0.9F, 0.9F, 0.9F);
						model.renderPart("flag_white");

						GL11.glColor3f(0.0F, 0.0F, 0.9F);
						model.renderPart("flag_blue");

						GL11.glColor3f(0.9F, 0.7F, 0.0F);
						model.renderPart("flag_yellow");
					}
					GL11.glPopMatrix();

					GL11.glPushMatrix();
					{
						GL11.glTranslatef(-0.04F, 1.80F, 0.0F);

						GL11.glColor3f(0.6F, 0.6F, 0.6F);
						model.renderPart("head_frame");

						GL11.glColor3f(0.5F, 0.5F, 0.5F);
						model.renderPart("head");

						GL11.glColor3f(0.1F, 0.1F, 0.1F);
						model.renderPart("head_face");
						model.renderPart("head_cover");

						GL11.glColor3f(0.9F, 0.9F, 0.9F);
						model.renderPart("head_rim");

						GL11.glColor3f(1.0F, 1.0F, 1.0F);
						model.renderPart("lamp");
					}
					GL11.glPopMatrix();
					break;
				}

				case 0: {
					/*
					 * Move signal to the correct spot
					 */
					GL11.glTranslatef(0.0F, 0.0F, -0.25F * offset);

					/*
					 * Render all components
					 */
					GL11.glColor3f(0.4F, 0.4F, 0.4F);
					model.renderPart("baseplate");
					model.renderPart("baseplate_panels");
					GL11.glColor3f(0.3F, 0.3F, 0.3F);
					model.renderPart("baseplate_bolts");
					GL11.glColor3f(0.7F, 0.7F, 0.7F);
					model.renderPart("post");
					GL11.glColor3f(0.9F, 0.9F, 0.9F);
					model.renderPart("nameplate");

					GL11.glPushMatrix();
					{
						if (offset < 0)
						{
							GL11.glTranslatef(0.00F, 1.80F, -0.04F);
						}
						else
						{
							GL11.glTranslatef(0.00F, 1.80F, 0.04F);
							GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
						}

						GL11.glColor3f(0.6F, 0.6F, 0.6F);
						model.renderPart("head_frame");

						GL11.glColor3f(0.5F, 0.5F, 0.5F);
						model.renderPart("head");

						GL11.glColor3f(0.1F, 0.1F, 0.1F);
						model.renderPart("head_face");

						GL11.glColor3f(0.9F, 0.9F, 0.9F);
						model.renderPart("head_rim");

						RenderUtil.lightmapPush();
						GL11.glPushAttrib(GL11.GL_LIGHTING);
						{
							RenderUtil.lightmapBright();
							GL11.glDisable(GL11.GL_LIGHTING);

							if (lamp.getLampType())
							{
								Minecraft.getMinecraft().renderEngine.bindTexture(SignalResources.texture_bulbLED);
							}
							else
							{
								Minecraft.getMinecraft().renderEngine.bindTexture(SignalResources.texture_bulbIncandescent);
							}

							SIGNAL_WHITE.multiply(lamp.getIntensity(partialTick)).apply();
							model.renderPart("lamp");

							Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
						}
						GL11.glPopAttrib();
						RenderUtil.lightmapPop();

						GL11.glColor3f(0.1F, 0.1F, 0.1F);
						if (offset >= 0)
						{
							GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
							GL11.glTranslatef(0.0F, 0.0F, 0.30F);
							model.renderPart("head_cover");
						}
						else
						{
							model.renderPart("head_cover");
						}
					}
					GL11.glPopMatrix();

					GL11.glPushMatrix();
					{
						if (offset < 0)
						{
							GL11.glTranslatef(0.00F, 2.25F, -0.04F);
						}
						else
						{
							GL11.glTranslatef(0.00F, 2.25F, 0.04F);
							GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
						}

						GL11.glScalef(0.8F, 0.8F, 0.8F);

						GL11.glColor3f(0.6F, 0.6F, 0.6F);
						model.renderPart("flag_frame");

						GL11.glColor3f(0.5F, 0.5F, 0.5F);
						model.renderPart("flag");

						GL11.glColor3f(0.9F, 0.9F, 0.9F);
						model.renderPart("flag_white");

						GL11.glColor3f(0.0F, 0.0F, 0.9F);
						model.renderPart("flag_blue");

						GL11.glColor3f(0.9F, 0.7F, 0.0F);
						model.renderPart("flag_yellow");
					}
					GL11.glPopMatrix();

					/*
					 * Nameplate
					 */
					if (this.getID().length() >= 4)
					{
						TESRSignal.renderSignalID(this.getID().substring(0, 2), 0.10F, 1.53F, 0.059F, 0.0F, 0x000000, 1.25F);
						TESRSignal.renderSignalID(this.getID().substring(2, 4), 0.10F, 1.44F, 0.059F, 0.0F, 0x000000, 1.25F);
						Minecraft.getMinecraft().renderEngine.bindTexture(RenderUtil.texture_white);
						GL11.glColor3f(1.0F, 1.0F, 1.0F);
					}

					break;
				}

				case 1: {
					// Move the signal to the side so it doesn't come too close to the tracks.
					GL11.glTranslatef(0.0F, 0.0F, -0.07F * offset);

					// TESRSignal.renderLampFlare(this, lamp, 0.141F, 1.8F, 0.0F, partialTick, 0.0F);
					break;
				}
			}
		}
		GL11.glPopMatrix();
	}

	@Override
	public void renderItem(ItemRenderType type)
	{
		this.render(0.0F, type == ItemRenderType.INVENTORY);
	}

	@Override
	public void saveProperties(NBTTagCompound nbt)
	{
		super.saveProperties(nbt);

		properties.get(KEY_POSTOFFSET).writeToNBT(nbt, KEY_POSTOFFSET);
	}

	@Override
	public void updateLamps()
	{
		lamp.setState(false);

		if (hasMagnet)
		{
			if (blocksFree >= 1)
			{
				lamp.setState(true);
			}
		}

		lamp.update();
	}

	@Override
	public void updateProperties(NBTTagCompound nbt)
	{
		super.updateProperties(nbt);

		properties.get(KEY_POSTOFFSET).readFromNBT(nbt, KEY_POSTOFFSET);
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);

		lamp.writeToNBT(nbt);
	}
}
