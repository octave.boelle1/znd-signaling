package zoranodensha.signals;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import zoranodensha.common.core.ModData;



public class SignalResources
{
	/*
	 * Models
	 */
	/** ZnD Block Signal */
	public static final IModelCustom model_zd_block = AdvancedModelLoader.loadModel(new ResourceLocation(ModData.ID, "models/signals/ModelSignal_ZD_Block.obj"));
	/** ZnD Dwarf Signal */
	public static final IModelCustom model_zd_dwarf = AdvancedModelLoader.loadModel(new ResourceLocation(ModData.ID, "models/signals/ModelSignal_ZD_Dwarf.obj"));
	/** ZnD MTCS Flag Signal */
	public static final IModelCustom model_zd_flag = AdvancedModelLoader.loadModel(new ResourceLocation(ModData.ID, "models/signals/ModelSignal_ZD_Flag.obj"));
	/** ZnD Tunnel Signal */
	public static final IModelCustom model_zd_tunnel = AdvancedModelLoader.loadModel(new ResourceLocation(ModData.ID, "models/signals/ModelSignal_ZD_Tunnel.obj"));

	/** ZnD Speed Signs */
	public static final IModelCustom model_zd_speedSign = AdvancedModelLoader.loadModel(new ResourceLocation(ModData.ID, "models/signals/ModelSpeedSign_ZD.obj"));

	/*
	 * Textures
	 */
	/** Incandescent Bulb */
	public static final ResourceLocation texture_bulbIncandescent = new ResourceLocation(ModData.ID, "textures/models/signals/bulb_incandescent.png");
	/** LED Bulb */
	public static final ResourceLocation texture_bulbLED = new ResourceLocation(ModData.ID, "textures/models/signals/bulb_led.png");
	/** LED Bulb (Quadratic) */
	public static final ResourceLocation texture_bulbLED_quad = new ResourceLocation(ModData.ID, "textures/models/signals/bulb_led_quadratic.png");
}
