package zoranodensha.common.oregen;

import java.util.Random;

import cpw.mods.fml.common.IWorldGenerator;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import zoranodensha.common.core.ModCenter;



public class OreGeneration implements IWorldGenerator
{
	private WorldGenMinable shaleGen;
	private WorldGenMinable shaleGen2;
	
	public OreGeneration()
	{
		shaleGen = new WorldGenMinable(ModCenter.BlockOreShale, ModCenter.cfg.worldGen.veinSize);
		shaleGen2 = new WorldGenMinable(ModCenter.BlockOreShale, 2, Blocks.coal_ore);
	}
	
	@Override
	public void generate(Random ran, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider)
	{
		if (ModCenter.cfg.worldGen.enableOreSpawn)
		{
			switch (world.provider.dimensionId)
			{
				case 0:
					for (int chance = 4; chance > 0; --chance)
					{
						shaleGen.generate(world, ran, chunkX * 16 + ran.nextInt(16), ran.nextInt(64), chunkZ * 16 + ran.nextInt(16));
						shaleGen2.generate(world, ran, chunkX * 16 + ran.nextInt(16), ran.nextInt(128), chunkZ * 16 + ran.nextInt(16));
					}
					break;
			}
		}
	}
}
