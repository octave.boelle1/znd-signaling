package zoranodensha.common.util;

import org.lwjgl.util.vector.Vector3f;



/**
 * A vector class containing a point with normal vector.
 */
public final class VecDir
{
	public Vector3f point;
	public Vector3f normal;



	public VecDir(float pointX, float pointY, float pointZ, float normX, float normY, float normZ)
	{
		point = new Vector3f(pointX, pointY, pointZ);
		normal = new Vector3f(normX, normY, normZ);
	}

	/**
	 * Returns the passed in Vector3f, offset by this VecDir's point vector.
	 */
	public Vector3f getOffset(Vector3f vec)
	{
		if (vec == null)
		{
			return point;
		}

		return vec.translate(point.x, point.y, point.z);
	}

	/**
	 * Returns the point on the normal with a value of {@code y = 0}.
	 */
	public Vector3f getOnNormal()
	{
		if (point.y == 0.0F || normal.y == 0.0F)
		{
			return null;
		}

		float f = -point.y / normal.y;

		if (f >= 0.0F)
		{
			return this.getOnNormal(f);
		}

		return null;
	}

	/**
	 * Return a point on the normal vector as Vector3f.
	 *
	 * @param f - The position on the normal. f = 1.0F is the full length of the normal.
	 */
	public Vector3f getOnNormal(float f)
	{
		Vector3f vec = new Vector3f(normal.x, normal.y, normal.z);
		vec.scale(f);

		return vec;
	}
}
