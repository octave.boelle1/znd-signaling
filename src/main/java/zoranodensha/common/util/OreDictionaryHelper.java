package zoranodensha.common.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;



public abstract class OreDictionaryHelper
{
	/**
	 * Parses the OreDictionary for all registered items registered with the String s.
	 */
	public static Item[] getIndicesInOreDictionary(String s)
	{
		List<ItemStack> list = OreDictionary.getOres(s);
		Object[] itemStacks = list.toArray();
		Item[] items = new Item[itemStacks.length];

		for (int i = 0; i < items.length; ++i)
		{
			items[i] = (itemStacks[i] instanceof ItemStack ? ((ItemStack)itemStacks[i]).getItem() : null);
		}

		return items;
	}

	/**
	 * Parses the OreDictionary for all registered items whose registered String starts with the given String s.
	 */
	public static Item[] getIndicesInOreDictionaryExtended(String s)
	{
		String[] oreNames = OreDictionary.getOreNames();
		List<ItemStack> list = new ArrayList<ItemStack>();

		for (String oreName : oreNames)
		{
			if (!Helpers.isUndefined(oreName) && oreName.startsWith(s))
			{
				Iterator<ItemStack> itera = OreDictionary.getOres(oreName).iterator();

				while (itera.hasNext())
				{
					list.add(itera.next());
				}
			}
		}

		Object[] itemStacks = list.toArray();
		Item[] items = new Item[itemStacks.length];

		for (int i = 0; i < items.length; ++i)
		{
			items[i] = (itemStacks[i] instanceof ItemStack ? ((ItemStack)itemStacks[i]).getItem() : null);
		}

		return items;
	}
}
