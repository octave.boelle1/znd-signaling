package zoranodensha.common.util;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.commons.io.FileUtils;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.handlers.CollisionHandler;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.client.entityFX.EParticles;
import zoranodensha.client.entityFX.EntityBloodParticle;
import zoranodensha.client.entityFX.EntityPowerSpark;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.registry.ModPartRegistry;



/**
 * Several helper methods used by Zora no Densha.
 */
public abstract class Helpers
{

	/**
	 * Determines the distance between the maximum X of the supplied coupler and the maximum X of the supplied buffer. The resulting value is used to set the {@link zoranodensha.api.vehicles.part.type.PartTypeCouplingChain#extraDimension}
	 * value of a chain coupler, which is what ensures that vehicles coupled with the chain coupler end up at the correct distance apart from each other (or that the buffers don't intersect or have a gap between them).
	 *
	 * @param buffer - The buffer associated with the screw coupler to calculate.
	 * @param coupler - The coupler associated with the buffer supplied as the first argument.
	 * @return - A {@code float} value which can be assigned to the coupler's extra dimension field.
	 */
	public static float calculateCouplerExtraDimension(VehParBase buffer, VehParBase coupler)
	{
		float bufferMaximumX = Math.abs(buffer.getOffset()[0]) + (buffer.getScaledWidth() * 0.5F);
		float couplerMaximumX = Math.abs(coupler.getOffset()[0]) + (coupler.getScaledWidth() * 0.5F);

		return bufferMaximumX - couplerMaximumX;
	}


	/**
	 * Sorts the given Array of ItemStacks by their ID, metadata and stack size in ascending order.
	 *
	 * @return The sorted Array as ArrayList.
	 */
	public static ArrayList<ItemStack> getSortedArrayList(Object[] obj)
	{
		/*
		 * Instantiate returned ArrayList and check whether given Collection isn't null nor empty.
		 */
		ArrayList<ItemStack> list = new ArrayList<ItemStack>();

		if (obj == null || obj.length == 0)
		{
			return list;
		}

		/*
		 * Iiterate through Collection and add each element to the ArrayList, depending on whether they are of lesser or higher value (i.e. ID, metadata, stack size).
		 */
		ItemStack itemStack;
		ItemStack elem;
		int size;
		int stackVal;
		int elemVal;

		for (int i = obj.length - 1; i >= 0; --i)
		{
			itemStack = (ItemStack)obj[i];

			/*
			 * If the ArrayList is empty, just add whatever we get here.
			 */
			if (list.isEmpty())
			{
				list.add(itemStack);
				continue;
			}

			/*
			 * Otherwise, iterate through the ArrayList and compare each element with the current element.
			 */
			size = list.size();

			inner:
			for (int j = 0; j <= size; ++j)
			{
				if (j == size)
				{
					/*
					 * We've been through the entire ArrayList and haven't been able to find an index that is
					 * of greater value than the current one, so we'll just append it to the list's end.
					 */

					list.add(itemStack);
					break inner;
				}

				elem = list.get(j);

				/*
				 * Compare ID, metadata, and stack size.
				 */
				stackVal = Item.getIdFromItem(itemStack.getItem());
				elemVal = Item.getIdFromItem(elem.getItem());

				if (stackVal < elemVal)
				{
					list.add(j, itemStack);
					break inner;
				}
				else if (stackVal == elemVal)
				{
					stackVal = itemStack.getItemDamage();
					elemVal = elem.getItemDamage();

					if (stackVal < elemVal)
					{
						list.add(j, itemStack);
						break inner;
					}
					else if (stackVal == elemVal)
					{
						if (itemStack.stackSize <= elem.stackSize)
						{
							list.add(itemStack);
							break inner;
						}
					}
				}
			}
		}

		return list;
	}

	/**
	 * Compares whether the given String argument is undefined, e.g. null, empty or consists of no characters.
	 *
	 * @return True if the String is undefined.
	 */
	public static boolean isUndefined(String compare)
	{
		return (compare == null || compare.isEmpty() || "".equals(compare));
	}

	/**
	 * Parse the given directory for files of given file type and return all matches in an ArrayList.
	 *
	 * @param fileType - The file suffix to search for.
	 * @param path - The path to search on.
	 * @param maxDepth - Number of levels this search may descend in the directory.
	 * @return An {@link java.util.ArrayList ArrayList} containing all matching {@link java.io.File files}.
	 */
	public static ArrayList<File> parseFor(String fileType, File path, int maxDepth)
	{
		ArrayList<File> list = new ArrayList<File>();

		try
		{
			File[] addons = path.listFiles();

			if (addons != null)
			{
				for (File file : addons)
				{
					try
					{
						if (file.isDirectory())
						{
							list.addAll(parseFor(fileType, file, maxDepth - 1));
						}
						else if (file.getName().toLowerCase().endsWith(".jar"))
						{
							JarFile jarFile = new JarFile(file);
							Enumeration<JarEntry> enumera = jarFile.entries();

							while (enumera.hasMoreElements())
							{
								JarEntry jarEntry = null;
								InputStream input = null;

								try
								{
									jarEntry = enumera.nextElement();

									if (!jarEntry.getName().endsWith(fileType))
									{
										continue;
									}

									input = jarFile.getInputStream(jarEntry);
									File file0 = File.createTempFile("preset", ModPartRegistry.PRESET_SUFFIX);
									file0.deleteOnExit();

									FileUtils.copyInputStreamToFile(input, file0);
									list.add(file0);
								}
								catch (Exception e)
								{
									/* Silent catch. */
								}
								finally
								{
									if (input != null)
									{
										input.close();
									}
								}
							}

							jarFile.close();
						}
						else if (file.getName().toLowerCase().endsWith(fileType))
						{
							list.add(file);
						}
					}
					catch (Exception e)
					{
						ModCenter.log.warn("Failed to load a file (" + file.getName() + ") while parsing for files of type '" + fileType.toUpperCase() + "'.", e);
					}
				}
			}
		}
		catch (Exception e)
		{
			ModCenter.log.error("Encountered an exception while parsing for files of type '" + fileType.toUpperCase() + "'.", e);
		}

		return list;
	}

	/**
	 * Called by client-sided packet handling to spawn blood particles if conditions are met.
	 */
	@SideOnly(Side.CLIENT)
	public static void spawnParticles(EParticles type, Object... params)
	{
		switch (type)
		{
			case BLOOD:
				if (ModCenter.cfg.rendering.enableRealisticBlood)
				{
					World world = Minecraft.getMinecraft().theWorld;
					Entity vehicle = world.getEntityByID((Integer)params[1]);

					if (vehicle instanceof Train && params[0] instanceof EntityLivingBase)
					{
						/* Determine damage points and particle amount. */
						Entity victim = (Entity)params[0];
						int damage = (Integer)params[2];
						int amount = (ModCenter.cfg.rendering.detailOther * 2) + 2;
						amount = ((damage * amount * amount) + (ZnDMathHelper.ran.nextInt(damage * amount)));

						/* Scale particle amount by mass relative to the player entity. */
						final float defaultMass = CollisionHandler.INSTANCE.getEntityMass(Minecraft.getMinecraft().thePlayer);
						if (defaultMass != 0.0F)
						{
							float entityMass = CollisionHandler.INSTANCE.getEntityMass(victim);
							amount = Math.round(amount * (entityMass / defaultMass));
						}

						/* Spawn particles. */
						for (int i = amount - 1; i >= 0; --i)
						{
							Minecraft.getMinecraft().effectRenderer.addEffect(new EntityBloodParticle(victim, (Train)vehicle));
						}
					}
				}
				break;

			case SPARKS:
				if (ModCenter.cfg.rendering.detailOther > 0)
				{
					Minecraft.getMinecraft().effectRenderer.addEffect(new EntityPowerSpark(Minecraft.getMinecraft().theWorld, (Double)params[0], (Double)params[1], (Double)params[2]));
				}
				break;
		}
	}
}
