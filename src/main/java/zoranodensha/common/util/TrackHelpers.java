package zoranodensha.common.util;

import java.util.Iterator;
import java.util.List;

import cpw.mods.fml.common.network.NetworkRegistry.TargetPoint;
import net.minecraft.block.BlockFalling;
import net.minecraft.block.BlockRailBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.common.IMinecartCollisionHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.minecart.MinecartUpdateEvent;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.asm.Accessors;
import zoranodensha.common.network.packet.PacketMinecartUpdate;



public abstract class TrackHelpers
{
	private static final int[][][] railMeta = new int[][][] { { { 8, 7 }, { 9, 8 }, { 6, 9 }, { 7, 6 } }, { { 3, 2 }, { 5, 5 }, { 3, 2 }, { 5, 5 }, } };

	private static final int[][][][] trackMeta = new int[][][][] { { { { 0, 0, -1 }, { -1, 0, 0 } }, { { 0, 0, 1 }, { -1, 0, 0 } } }, { { { 0, 0, -1 }, { 1, 0, 0 } }, { { 0, 0, -1 }, { -1, 0, 0 } } },
			{ { { 0, 0, 1 }, { 1, 0, 0 } }, { { 0, 0, -1 }, { 1, 0, 0 } } }, { { { 0, 0, 1 }, { -1, 0, 0 } }, { { 0, 0, 1 }, { 1, 0, 0 } } } };



	/**
	 * Checks if the block at the given x, y and z coordinate can be sustained by the block below.
	 * Returns true if so.
	 */
	public static boolean canBeSustained(World world, int x, int y, int z)
	{
		return !(world.getBlock(x, y - 1, z) instanceof BlockFalling);
	}

	/**
	 * Called to apply Minecraft's vanilla physics on the given EntityMinecart while moving the vehicle on a ZnD track.
	 */
	public static final void computeMinecartMovement(int x, int y, int z, double maxSpeed, EntityMinecart minecart, ITrackBase trackBase)
	{
		minecart.prevPosX = minecart.posX;
		minecart.prevPosY = minecart.posY;
		minecart.prevPosZ = minecart.posZ;
		minecart.motionY -= 0.04D;

		int rota = trackBase.getOrientation() % 4;
		int dir = trackBase.getDirectionOfSection().toInt() > 0 ? 1 : 0;
		EShape eShape = trackBase.getInstanceOfShape().getEShapeType();

		double[] d0 = new double[] { minecart.posX, minecart.posY, minecart.posZ };
		Accessors.func_145821_a(minecart, x, y, z, maxSpeed, 0.0D, BlockRailBaseGag.instance(), railMeta[EShape.CURVE.equals(eShape) ? 0 : 1][rota][dir]);
		minecart.setPosition(d0[0], d0[1], d0[2]);

		double d1 = 0.0D;
		double d2 = 0.0D;
		boolean isNotStraight = !EShape.STRAIGHT.equals(eShape);

		if (isNotStraight)
		{
			d1 = trackMeta[rota][dir][1][0] - trackMeta[rota][dir][0][0];
			d2 = trackMeta[rota][dir][1][2] - trackMeta[rota][dir][0][2];
		}
		else
		{
			d1 = rota % 2 == 0 ? 2.0D : 0.0D;
			d2 = rota % 2 != 0 ? 2.0D : 0.0D;
		}

		double d3 = Math.sqrt((d1 * d1) + (d2 * d2));
		double d4 = (minecart.motionX * d1) + (minecart.motionZ * d2);

		if (d4 < 0.0D)
		{
			d1 = -d1;
			d2 = -d2;
		}

		d4 = Math.sqrt((minecart.motionX * minecart.motionX) + (minecart.motionZ * minecart.motionZ));

		if (d4 > 2.0D)
		{
			d4 = 2.0D;
		}

		minecart.motionX = d4 * d1 / d3;
		minecart.motionZ = d4 * d2 / d3;

		if (minecart.riddenByEntity instanceof EntityLivingBase)
		{
			d4 = ((EntityLivingBase)minecart.riddenByEntity).moveForward;

			if (d4 > 0.0D)
			{
				d1 = -Math.sin(minecart.riddenByEntity.rotationYaw * (Math.PI / 180.0D));
				d2 = Math.cos(minecart.riddenByEntity.rotationYaw * (Math.PI / 180.0D));

				if ((minecart.motionX * minecart.motionX) + (minecart.motionZ * minecart.motionZ) < 0.01D)
				{
					minecart.motionX += d1 * 0.1D;
					minecart.motionZ += d2 * 0.1D;
				}
			}
		}

		minecart.moveMinecartOnRail(x, y, z, maxSpeed);
		double[][] rotations = trackBase.getPositionOnTrack(minecart, minecart.posX, minecart.posY, minecart.posZ, minecart.rotationYaw, minecart.rotationPitch);
		minecart.setPosition(rotations[0][0], rotations[0][1] + minecart.getYOffset() + 0.125D, rotations[0][2]);
		Accessors.applyDrag(minecart);

		if (isNotStraight)
		{
			int xNew = MathHelper.floor_double(minecart.posX);
			int zNew = MathHelper.floor_double(minecart.posZ);

			if (xNew != x || zNew != z)
			{
				d4 = Math.sqrt((minecart.motionX * minecart.motionX) + (minecart.motionZ * minecart.motionZ));
				minecart.motionX = d4 * (xNew - x);
				minecart.motionZ = d4 * (zNew - z);
			}
		}

		computeMinecartRotation(minecart);
		Accessors.func_145775_I(minecart);

		AxisAlignedBB axisAlignedBB;
		IMinecartCollisionHandler collisionHandler = EntityMinecart.getCollisionHandler();

		if (collisionHandler != null)
		{
			axisAlignedBB = collisionHandler.getMinecartCollisionBox(minecart);
		}
		else
		{
			axisAlignedBB = minecart.boundingBox.expand(0.2D, 0.0D, 0.2D);
		}

		List<?> list = minecart.worldObj.getEntitiesWithinAABBExcludingEntity(minecart, axisAlignedBB);

		if (list != null && !list.isEmpty())
		{
			Iterator<?> itera = list.iterator();

			while (itera.hasNext())
			{
				Entity entity = (Entity)itera.next();

				if (entity != minecart.riddenByEntity && entity.canBePushed() && entity instanceof EntityMinecart)
				{
					entity.applyEntityCollision(minecart);
				}
			}
		}

		if (minecart.riddenByEntity != null && minecart.riddenByEntity.isDead)
		{
			if (minecart.riddenByEntity.ridingEntity == minecart)
			{
				minecart.riddenByEntity.ridingEntity = null;
			}

			minecart.riddenByEntity = null;
		}

		MinecraftForge.EVENT_BUS.post(new MinecartUpdateEvent(minecart, (float)minecart.posX, (float)minecart.posY, (float)minecart.posZ));
		ModCenter.snw.sendToAllAround(new PacketMinecartUpdate(minecart), new TargetPoint(minecart.dimension, minecart.posX, minecart.posY, minecart.posZ, 60.0D));
	}

	/**
	 * Called to calculate the EntityMinecart's rotations the way vanilla Minecraft does.
	 */
	public static final void computeMinecartRotation(EntityMinecart cart)
	{
		cart.rotationPitch = 0.0F;
		double d1 = cart.prevPosX - cart.posX;
		double d2 = cart.prevPosZ - cart.posZ;
		boolean flag = Accessors.isInReverse(cart);

		if (d1 * d1 + d2 * d2 > 0.001D)
		{
			cart.rotationYaw = (float)(Math.atan2(d2, d1) * 180.0D / Math.PI);

			if (flag)
			{
				cart.rotationYaw += 180.0F;
			}
		}

		d1 = MathHelper.wrapAngleTo180_float(cart.rotationYaw - cart.prevRotationYaw);

		if (d1 < -170.0D || d1 >= 170.0D)
		{
			cart.rotationYaw += 180.0F;
			Accessors.isInReverse(cart, !flag);
		}

		Accessors.setRotation(cart, MathHelper.wrapAngleTo180_float(cart.rotationYaw), cart.rotationPitch);
	}



	static class BlockRailBaseGag extends BlockRailBase
	{
		static final BlockRailBaseGag instance = new BlockRailBaseGag();



		protected BlockRailBaseGag()
		{
			super(false);
		}

		public static BlockRailBaseGag instance()
		{
			return instance;
		}
	}
}
