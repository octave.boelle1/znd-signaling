package zoranodensha.common.network.packet;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityMinecart;
import zoranodensha.common.core.ModCenter;



public class PacketMinecartUpdate implements IMessage
{
	private int entityID;
	private float minecartRotationYaw;
	private float minecartRotationPitch;



	public PacketMinecartUpdate()
	{
	}

	public PacketMinecartUpdate(EntityMinecart minecart)
	{
		entityID = minecart.getEntityId();
		minecartRotationYaw = minecart.rotationYaw;
		minecartRotationPitch = minecart.rotationPitch;
	}

	@Override
	public void fromBytes(ByteBuf bbuf)
	{
		entityID = bbuf.readInt();
		minecartRotationYaw = bbuf.readFloat();
		minecartRotationPitch = bbuf.readFloat();
	}

	@Override
	public void toBytes(ByteBuf bbuf)
	{
		bbuf.writeInt(entityID);
		bbuf.writeFloat(minecartRotationYaw);
		bbuf.writeFloat(minecartRotationPitch);
	}



	public static class Handler implements IMessageHandler<PacketMinecartUpdate, IMessage>
	{
		@Override
		public IMessage onMessage(PacketMinecartUpdate message, MessageContext context)
		{
			Entity entity = ModCenter.proxy.getPlayer(context).worldObj.getEntityByID(message.entityID);

			if (entity instanceof EntityMinecart)
			{
				entity.rotationYaw = message.minecartRotationYaw % 360.0F;
				entity.rotationPitch = message.minecartRotationPitch % 360.0F;
			}

			return null;
		}
	}
}
