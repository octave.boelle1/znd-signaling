package zoranodensha.common.network.packet.gui;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.inventory.Container;
import zoranodensha.common.containers.ContainerEngineerTable;



public class PacketGUIEngineerTable implements IMessage
{
	private byte button;



	public PacketGUIEngineerTable()
	{
	}

	public PacketGUIEngineerTable(byte button)
	{
		this.button = button;

		if (button == 3 || button == 4)
		{
			Container container = Minecraft.getMinecraft().thePlayer.openContainer;

			if (container instanceof ContainerEngineerTable)
			{
				((ContainerEngineerTable)container).addSlotsToContainer();
			}
		}
	}

	@Override
	public void fromBytes(ByteBuf bbuf)
	{
		button = bbuf.readByte();
	}

	@Override
	public void toBytes(ByteBuf bbuf)
	{
		bbuf.writeByte(button);
	}



	public static class Handler implements IMessageHandler<PacketGUIEngineerTable, IMessage>
	{
		@Override
		public IMessage onMessage(PacketGUIEngineerTable message, MessageContext context)
		{
			Container container = context.getServerHandler().playerEntity.openContainer;

			if (container instanceof ContainerEngineerTable)
			{
				ContainerEngineerTable containerTable = (ContainerEngineerTable)container;
				containerTable.tileEntity.notifyOfButtonPressed(message.button);

				if (message.button == 3 || message.button == 4)
				{
					containerTable.addSlotsToContainer();
				}
			}

			return null;
		}
	}
}
