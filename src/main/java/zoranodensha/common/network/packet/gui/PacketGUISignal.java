package zoranodensha.common.network.packet.gui;

import org.apache.logging.log4j.Level;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import io.netty.buffer.ByteBuf;
import net.minecraft.tileentity.TileEntity;
import zoranodensha.api.structures.signals.ISignal;
import zoranodensha.common.blocks.tileEntity.TileEntitySignal;
import zoranodensha.common.core.ModCenter;



public class PacketGUISignal implements IMessage
{

	private int teX, teY, teZ;

	private String newKey = null;
	private Integer newValue = null;



	/**
	 * Initialises a new instance of the
	 * {@link zoranodensha.common.network.packet.gui.PacketGUISignal} class.
	 */
	public PacketGUISignal()
	{

	}


	/**
	 * Initialises a new instance of the
	 * {@link zoranodensha.common.network.packet.gui.PacketGUISignal} class.
	 * 
	 * @param x - The X position of the signal's tile entity.
	 * @param y - The Y position of the signal's tile entity.
	 * @param z - The Z position of the signal's tile entity.
	 */
	public PacketGUISignal(int x, int y, int z)
	{
		this.teX = x;
		this.teY = y;
		this.teZ = z;
	}


	/**
	 * Adds an updated property to assign to the signal.
	 * 
	 * @param newKey - The key of the signal's property to change.
	 * @param newValue - The new value of the signal's property to change (this is
	 *            an ordinal of an enum).
	 * @return - This packet, for easier initialisation.
	 */
	public PacketGUISignal addNewProperty(String key, Integer value)
	{
		if (key != null && value != null)
		{
			this.newKey = key;
			this.newValue = value;
		}

		return this;
	}


	@Override
	public void fromBytes(ByteBuf buffer)
	{
		/*
		 * Position
		 */
		teX = buffer.readInt();
		teY = buffer.readInt();
		teZ = buffer.readInt();

		/*
		 * New Property
		 */
		if (buffer.readBoolean())
		{
			newKey = ByteBufUtils.readUTF8String(buffer);
			newValue = buffer.readInt();
		}
		else
		{
			newKey = null;
			newValue = null;
		}
	}


	@Override
	public void toBytes(ByteBuf buffer)
	{
		/*
		 * Position
		 */
		buffer.writeInt(teX);
		buffer.writeInt(teY);
		buffer.writeInt(teZ);

		/*
		 * New Property
		 */
		if (newKey != null && newValue != null)
		{
			buffer.writeBoolean(true);

			ByteBufUtils.writeUTF8String(buffer, newKey);
			buffer.writeInt(newValue);
		}
		else
		{
			buffer.writeBoolean(false);
		}
	}



	public static class Handler implements IMessageHandler<PacketGUISignal, IMessage>
	{

		@Override
		public IMessage onMessage(PacketGUISignal message, MessageContext context)
		{
			TileEntity tileEntity = context.getServerHandler().playerEntity.worldObj.getTileEntity(message.teX, message.teY, message.teZ);

			if (tileEntity instanceof TileEntitySignal)
			{
				ISignal signal = ((TileEntitySignal)tileEntity).getSignal();

				/*
				 * Reset Interlock List
				 */
				if (message.newKey.equals("reset"))
				{
					signal.getInterlockedTracks().clear();
					signal.markForUpdate();

					return null;
				}

				/*
				 * New Property
				 */
				if (message.newKey != null && message.newValue != null)
				{
					signal.getAdditionalProperties().get(message.newKey).set(message.newValue);
					signal.markForUpdate();
				}
			}

			return null;
		}

	}
}
