package zoranodensha.common.network.packet.gui;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.inventory.Container;
import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.common.containers.ContainerEngineerTable;



public class PacketGUIEngineerTablePreset implements IMessage
{
	private byte flag;
	private NBTTagCompound nbt;



	public PacketGUIEngineerTablePreset()
	{
	}

	@SideOnly(Side.CLIENT)
	public PacketGUIEngineerTablePreset(NBTTagCompound nbt, byte flag)
	{
		this.flag = flag;
		this.nbt = nbt;

		if (flag < 3)
		{
			Container container = Minecraft.getMinecraft().thePlayer.openContainer;

			if (container instanceof ContainerEngineerTable)
			{
				ContainerEngineerTable containerEngineerTable = (ContainerEngineerTable)container;

				containerEngineerTable.tileEntity.onPresetChange(this.nbt, this.flag);
				containerEngineerTable.addSlotsToContainer();
			}
		}
	}

	@Override
	public void fromBytes(ByteBuf bbuf)
	{
		if ((flag = bbuf.readByte()) != -1)
		{
			nbt = ByteBufUtils.readTag(bbuf);
		}
	}

	@Override
	public void toBytes(ByteBuf bbuf)
	{
		if (nbt != null)
		{
			bbuf.writeByte(flag);
			ByteBufUtils.writeTag(bbuf, nbt);
		}
		else
		{
			bbuf.writeByte((byte)-1);
		}
	}



	public static class Handler implements IMessageHandler<PacketGUIEngineerTablePreset, IMessage>
	{
		@Override
		public IMessage onMessage(PacketGUIEngineerTablePreset message, MessageContext context)
		{
			if (message.flag > -1)
			{
				Container container = context.getServerHandler().playerEntity.openContainer;

				if (container instanceof ContainerEngineerTable)
				{
					ContainerEngineerTable containerEngineerTable = (ContainerEngineerTable)container;
					containerEngineerTable.tileEntity.onPresetChange(message.nbt, message.flag);

					if (message.flag != 3)
					{
						containerEngineerTable.addSlotsToContainer();
					}
				}
			}

			return null;
		}
	}
}
