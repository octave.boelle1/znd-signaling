package zoranodensha.common.items;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;



public class ItemBlockPlatform extends ItemBlock
{
	public ItemBlockPlatform(Block block)
	{
		super(block);
		setHasSubtypes(true);
	}

	@Override
	public int getMetadata(int meta)
	{
		return meta;
	}
}