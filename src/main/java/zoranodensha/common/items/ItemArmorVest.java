package zoranodensha.common.items;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class ItemArmorVest extends ItemArmor
{
	private boolean registered = false;
	private final EVest type;



	public ItemArmorVest(EVest type)
	{
		super(EnumHelper.addArmorMaterial("Vest_Material", 5, new int[] { 0, 3, 0, 0 }, 15), 0, 1);
		this.type = type;
		
		setCreativeTab(ModCreativeTabs.generic);
		setMaxStackSize(1);
		setUnlocalizedName(ModData.ID + ".items.vest" + type.toName());
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String subtype)
	{
		return ModData.ID + ":textures/armor/vest" + type.toName() + ".png";
	}

	@Override
	public void registerIcons(IIconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon(ModData.ID + ":itemVest" + type.toName());
	}

	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}
		registered = true;

		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this), new Object[] { "WDW", "LWL", "LWL", 'L', Items.leather, 'W', new ItemStack(Blocks.wool, 1, 1), 'D', type.oreName }));
	}



	public static enum EVest
	{
		ORANGE,
		YELLOW;

		public final String oreName;



		private EVest()
		{
			oreName = "dye" + toName();
		}
		
		public String toName()
		{
			String s = toString();
			return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
		}

		@Override
		public String toString()
		{
			return super.toString().toLowerCase();
		}
	}
}