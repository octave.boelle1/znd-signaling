package zoranodensha.common.items;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class ItemPlatePlastic extends Item
{
	private static boolean registered = false;



	public ItemPlatePlastic()
	{
		setCreativeTab(ModCreativeTabs.generic);
		setUnlocalizedName(ModData.ID + ".items.plasticSheet");
	}


	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconReg)
	{
		itemIcon = iconReg.registerIcon(ModData.ID + ":itemPlatePlastic");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		OreDictionary.registerOre("sheetPlastic", this);
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 4), "PP ", "PP ", "   ", 'P', "dustPlastic"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 4), " PP", " PP", "   ", 'P', "dustPlastic"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 4), "   ", "PP ", "PP ", 'P', "dustPlastic"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 4), "   ", " PP", " PP", 'P', "dustPlastic"));
	}
}
