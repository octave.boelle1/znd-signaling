/**
 *
 */
package zoranodensha.common.items;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import org.lwjgl.Sys;
import zoranodensha.api.structures.signals.ISignal;
import zoranodensha.api.structures.signals.ISignalEquipment;
import zoranodensha.api.structures.signals.ISignalMagnet;
import zoranodensha.api.structures.signals.ISpeedSign;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.util.Vec4i;
import zoranodensha.common.blocks.tileEntity.TileEntitySignal;
import zoranodensha.common.blocks.tileEntity.TileEntitySpeedSign;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBaseGag;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.common.network.packet.PacketChatMessage;
import zoranodensha.signals.common.ASignal;
import zoranodensha.trackpack.common.section.ARailwaySectionTrackPackSignalMagnet;

/**
 * <p>
 * A tool used for linking signals to magnets.
 * </p>
 *
 * @author Jaffa
 */
public class ItemSignalTool extends Item
{
	/** Equipment exchange. */
	private ISignal equipmentSignal;
	private ISpeedSign equipmentSpeedSign;
	private ISignalMagnet equipmentMagnet;
	private Vec4i interlockState;


	public ItemSignalTool()
	{
		setCreativeTab(ModCreativeTabs.generic);
		setMaxStackSize(1);
		setUnlocalizedName(ModData.ID + ".items.signalTool");
	}


	/**
	 * Helper method used to get the {@link zoranodensha.api.structures.signals.ITracksideEquipment} instance from the provided tile entity. If the tile entity does not contain the instance in question, a null will be returned.
	 *
	 * @param tile - The {@link TileEntity} to retreive the equipment instance from.
	 * @return - If one is found, a {@link zoranodensha.api.structures.signals.ITracksideEquipment} instance, otherwise {@code null}.
	 */
	private ISignalEquipment getEquipment(TileEntity tile)
	{

		System.out.println("Equipment : " + tile);

		/*
		 * Signal
		 */
		if (tile instanceof TileEntitySignal)
		{
			return ((TileEntitySignal)tile).getSignal();
		}

		/*
		 * Speed Sign
		 */
		if (tile instanceof TileEntitySpeedSign)
		{
			return ((TileEntitySpeedSign)tile).getSpeedSign();
		}

		/*
		 * Track Magnet
		 */
		if (tile instanceof TileEntityTrackBase)
		{
			if (((TileEntityTrackBase)tile).getInstanceOfShape() instanceof ISignalMagnet)
			{
				if(((TileEntityTrackBase)tile).getField(ARailwaySectionTrackPackSignalMagnet.INDEX_HASMAGNET, false, Boolean.class))
				{
					return (ISignalMagnet)((TileEntityTrackBase)tile).getInstanceOfShape();
				}
			}
		}

		return null;
	}


	@Override
	public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player)
	{
		return itemStack;
	}


	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int side, float px, float py, float pz)
	{

		// Only run on server worlds.
		if (world.isRemote)
			return false;

		/*
		 * Firstly, determine the tile which was selected. It must contain an
		 * IEquipmentTile instance.
		 */
		TileEntity tileEntity = world.getTileEntity(x, y, z);
		ISignalEquipment equipmentTile = getEquipment(tileEntity);

		if (equipmentTile != null)
		{
			/*
			 * Then, determine the scenario...
			 */

			/*
			 * 1. Selecting a signal.
			 */
			if (equipmentSignal == null && equipmentSpeedSign == null && equipmentMagnet == null && equipmentTile instanceof ISignal)
			{
				equipmentSignal = (ISignal)equipmentTile;

				if (interlockState != null)
				{
					if (equipmentSignal instanceof ASignal)
					{
						((ASignal)equipmentSignal).interlockList.add(interlockState);
					}

					interlockState = null;
					equipmentSignal = null;

					PacketChatMessage.sendMessageToPlayer(player, ".text.signalTool.saved");
				}
				else
				{
					PacketChatMessage.sendMessageToPlayer(player, ".text.signalTool.started");
				}

				return true;
			}

			/*
			 * 2. Selecting a speed sign.
			 */
			else if (equipmentSignal == null && equipmentSpeedSign == null && equipmentMagnet == null && equipmentTile instanceof ISpeedSign)
			{
				equipmentSpeedSign = (ISpeedSign)equipmentTile;

				PacketChatMessage.sendMessageToPlayer(player, ".text.signalTool.started");

				return true;
			}

			/*
			 * 3. Selecting signal magnet for a signal.
			 */
			else if (equipmentSignal != null && equipmentSpeedSign == null && equipmentMagnet == null && equipmentTile instanceof ISignalMagnet)
			{
				if (equipmentSignal.getX() == x && equipmentSignal.getY() == y && equipmentSignal.getZ() == z)
				{
					equipmentSignal = null;
					equipmentMagnet = null;

					PacketChatMessage.sendMessageToPlayer(player, ".text.signalTool.cancelled");

					return true;
				}

				equipmentMagnet = (ISignalMagnet)equipmentTile;

				/*
				 * Signal -> Signal Magnet
				 */
				{
					/*
					 * Get the track base from the magnet tile, as we'll need this later
					 */
					ITrackBase magnetTrackBase;
					if (tileEntity instanceof TileEntityTrackBase)
					{
						magnetTrackBase = (ITrackBase)tileEntity;
					}
					else if (tileEntity instanceof TileEntityTrackBaseGag)
					{
						magnetTrackBase = ((TileEntityTrackBaseGag)tileEntity).getSourceTile((World)null);
					}
					else
					{
						magnetTrackBase = null;
					}

					/*
					 * Create the link between them.
					 */
					if (magnetTrackBase != null)
					{
						TileEntityTrackBase magnet = equipmentSignal.getMagnetTrack();
						if (magnet != null && magnet.getInstanceOfShape() instanceof ISignalMagnet)
						{
							((ISignalMagnet) magnet.getInstanceOfShape()).clearSignal(magnet);
						}

						equipmentSignal.setTargetMagnet(x, y, z);
						equipmentMagnet.setLinkedSignal(equipmentSignal, magnetTrackBase);

						equipmentSignal = null;
						equipmentMagnet = null;

						PacketChatMessage.sendMessageToPlayer(player, ".text.signalTool.linked");
						return true;
					}
					else
					{
						return false;
					}
				}

			}

			/*
			 * 4. Selecting a signal magnet for a speed sign.
			 */
			else if (equipmentSignal == null && equipmentSpeedSign != null && equipmentMagnet == null && equipmentTile instanceof ISignalMagnet)
			{
				equipmentMagnet = (ISignalMagnet)equipmentTile;

				/*
				 * Speed sign -> Signal magnet
				 */
				{
					/*
					 * Get the track base from the magnet tile, as we'll need this later
					 */
					ITrackBase magnetTrackBase;
					if (tileEntity instanceof TileEntityTrackBase)
					{
						magnetTrackBase = (ITrackBase)tileEntity;
					}
					else if (tileEntity instanceof TileEntityTrackBaseGag)
					{
						magnetTrackBase = ((TileEntityTrackBaseGag)tileEntity).getSourceTile((World)null);
					}
					else
					{
						magnetTrackBase = null;
					}

					if (magnetTrackBase != null)
					{
						equipmentMagnet.setTrackSpeed(equipmentSpeedSign.getSpeedLimit(), magnetTrackBase);

						equipmentSpeedSign = null;
						equipmentMagnet = null;

						PacketChatMessage.sendMessageToPlayer(player, ".text.signalTool.speedApplied");
						return true;
					}
					else
					{
						return false;
					}
				}
			}

			/*
			 * 5. Selecting a signal magnet to configure as a station stopping point or terminating location.
			 */
			else if (equipmentSignal == null && equipmentSpeedSign == null && equipmentMagnet == null && equipmentTile instanceof ISignalMagnet)
			{
				equipmentMagnet = (ISignalMagnet)equipmentTile;

				/*
				 * Get the track base instance of the magnet to apply the changes.
				 */
				ITrackBase magnetTrackBase = (ITrackBase)tileEntity;
				boolean isCurrentlyStation = equipmentMagnet.getIsStation(magnetTrackBase);
				boolean isCurrentlyTermination = equipmentMagnet.getIsTermination(magnetTrackBase);

				if (!isCurrentlyStation && !isCurrentlyTermination)
				{
					equipmentMagnet.setIsStation(magnetTrackBase, true);
					PacketChatMessage.sendMessageToPlayer(player, ".text.signalTool.isStation");
				}
				else if (isCurrentlyStation && !isCurrentlyTermination)
				{
					equipmentMagnet.setIsStation(magnetTrackBase, false);
					equipmentMagnet.setIsTermination(magnetTrackBase, true);
					PacketChatMessage.sendMessageToPlayer(player, ".text.signalTool.isTermination");
				}
				else
				{
					equipmentMagnet.setIsStation(magnetTrackBase, false);
					equipmentMagnet.setIsTermination(magnetTrackBase, false);
					PacketChatMessage.sendMessageToPlayer(player, ".text.signalTool.isNone");
				}

				equipmentMagnet = null;
			}

			return true;
		}
		else if (equipmentSignal == null && equipmentSpeedSign == null && tileEntity instanceof ITrackBase)
		{
			System.out.println("TrackBase : " + tileEntity);

			TileEntityTrackBase trackBase = null;

			if (tileEntity instanceof TileEntityTrackBase)
			{
				trackBase = (TileEntityTrackBase)tileEntity;
			}
			else if (tileEntity instanceof TileEntityTrackBaseGag)
			{
				ITrackBase sourceTile = ((TileEntityTrackBaseGag)tileEntity).getSourceTile((World)null);

				if (sourceTile instanceof TileEntityTrackBase)
				{
					trackBase = (TileEntityTrackBase)sourceTile;
				}
			}

			if (trackBase != null)
			{
				if (interlockState != null)
				{
					interlockState = null;
					PacketChatMessage.sendMessageToPlayer(player, ".text.signalTool.cancelled");
				}
				else
				{
					System.out.println("Path : " + trackBase.getPath());
					interlockState = new Vec4i(trackBase.xCoord, trackBase.yCoord, trackBase.zCoord, trackBase.getPath());
					PacketChatMessage.sendMessageToPlayer(player, ".text.signalTool.copied");
				}

				return true;
			}
		}

		return false;

	}


	@Override
	public void registerIcons(IIconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon(ModData.ID + ":itemSignalTool");
	}

	public void registerRecipes()
	{
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this), new ItemStack(ModCenter.ItemRailwayTool)));
	}

}
