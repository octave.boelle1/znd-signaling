package zoranodensha.common.items;

import java.util.Arrays;
import java.util.HashSet;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import mods.railcraft.api.core.items.IToolCrowbar;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.common.blocks.BlockTrackBase;
import zoranodensha.common.blocks.BlockTrackBaseGag;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class ItemCrowbar extends ItemTool implements IToolCrowbar
{
	private static boolean registered = false;



	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ItemCrowbar()
	{
		super(3.0F, ToolMaterial.IRON, new HashSet(Arrays.asList(new Block[] { ModCenter.BlockRailwaySign, ModCenter.BlockTrackBase, ModCenter.BlockTrackBaseGag, Blocks.rail })));

		setCreativeTab(ModCreativeTabs.generic);
		setHarvestLevel("crowbar", 2);
		setUnlocalizedName(ModData.ID + ".items.crowbar");
	}

	@Override
	public boolean canBoost(EntityPlayer player, ItemStack crowbar, EntityMinecart cart)
	{
		return !player.isSneaking();
	}

	@Override
	public boolean canLink(EntityPlayer player, ItemStack crowbar, EntityMinecart cart)
	{
		return player.isSneaking();
	}

	@Override
	public boolean canWhack(EntityPlayer player, ItemStack crowbar, int x, int y, int z)
	{
		return true;
	}

	@Override
	public float func_150893_a(ItemStack itemStack, Block block)
	{
		if (block instanceof BlockTrackBase || block instanceof BlockTrackBaseGag)
		{
			return 14F;
		}

		return super.func_150893_a(itemStack, block);
	}

	@Override
	public void onBoost(EntityPlayer player, ItemStack crowbar, EntityMinecart cart)
	{
		if (crowbar != null)
		{
			crowbar.damageItem(3, player);
		}
	}

	@Override
	public void onLink(EntityPlayer player, ItemStack crowbar, EntityMinecart cart)
	{
		if (crowbar != null)
		{
			crowbar.damageItem(2, player);
		}
	}

	@Override
	public void onWhack(EntityPlayer player, ItemStack crowbar, int x, int y, int z)
	{
		if (crowbar != null)
		{
			crowbar.damageItem(1, player);
		}
	}


	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconReg)
	{
		itemIcon = iconReg.registerIcon(ModData.ID + ":itemCrowbar");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this), " OI", "OIO", "IO ", 'O', "dyeOrange", 'I', "ingotIron"));
	}
}
