package zoranodensha.common.items;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import zoranodensha.api.structures.signals.ASpeedSignRegistry;
import zoranodensha.api.structures.signals.ISpeedSign;
import zoranodensha.common.blocks.tileEntity.TileEntitySpeedSign;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.common.util.ZnDMathHelper;



public class ItemSpeedSign extends Item
{
	public static boolean useBingBong = false;



	public ItemSpeedSign()
	{
		setCreativeTab(ModCreativeTabs.generic);
		setUnlocalizedName(ModData.ID + ".items.itemSpeedSign");
		setMaxStackSize(8);
		setMaxDamage(0);
		setHasSubtypes(false);
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void getSubItems(Item item, CreativeTabs creativeTab, List list)
	{
		ArrayList<String> registeredSpeedSigns = ASpeedSignRegistry.getRegisteredSpeedSignNames();

		for (int i = 0; i < registeredSpeedSigns.size(); i++)
		{
			ItemStack speedSignItemStack = new ItemStack(ModCenter.ItemSpeedSign, 1, i);

			NBTTagCompound stackNBT = new NBTTagCompound();
			stackNBT.setString("name", registeredSpeedSigns.get(i));
			speedSignItemStack.setTagCompound(stackNBT);

			list.add(speedSignItemStack);
		}
	}

	@Override
	public String getUnlocalizedName(ItemStack itemStack)
	{
		if (itemStack.hasTagCompound())
		{
			return itemStack.getTagCompound().getString("name");
		}
		return ModData.ID + ".items.itemSpeedSign";
	}

	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int side, float px, float py, float pz)
	{
		Block block = world.getBlock(x, y, z);

		if (block == Blocks.snow_layer && (world.getBlockMetadata(x, y, z) & 7) < 1)
		{
			side = 1;
		}
		else if (block != Blocks.vine && block != Blocks.tallgrass && block != Blocks.deadbush && !block.isReplaceable(world, x, y, z))
		{
			switch (side)
			{
				case 0:
					y--;
					break;
				case 1:
					y++;
					break;
				case 2:
					z--;
					break;
				case 3:
					z++;
					break;
				case 4:
					x--;
					break;
				case 5:
					x++;
					break;
			}
		}

		if (itemStack == null || itemStack.stackSize <= 0)
		{
			return false;
		}
		else if (world.canPlaceEntityOnSide(ModCenter.BlockSpeedSign, x, y, z, false, side, player, itemStack))
		{
			ISpeedSign iSign = ASpeedSignRegistry.getSpeedSignFromName(itemStack.getTagCompound().getString("name"));

			/*
			 * Return if no proper sign instance was returned from the registry.
			 */
			if (iSign == null)
				return false;

			int angle = MathHelper.floor_double((double)((player.rotationYaw + 180.0F) * 16.0F / 360.0F) + 0.5D) & 15;

			if (world.setBlock(x, y, z, ModCenter.BlockSpeedSign, angle, 2))
			{
				TileEntity tileEntity = world.getTileEntity(x, y, z);

				if (tileEntity instanceof TileEntitySpeedSign)
				{
					TileEntitySpeedSign sign = (TileEntitySpeedSign)tileEntity;

					sign.setSpeedSign(iSign);

					Random random = new Random();

					world.markBlockForUpdate(x, y, z);
					if (useBingBong)
					{
						world.playSoundEffect(x + 0.5D, y + 0.5D, z + 0.5D, "zoranodensha:block_placement" + (random.nextBoolean() ? "A" : "B"), 0.80F, 1.0F + (0.25F * (random.nextFloat() - random.nextFloat())));
					}
					else
					{
						world.playSoundEffect(x + 0.5D, y + 0.5D, z + 0.5D, "minecraft:random.anvil_land", 0.25F, 1.1F);
					}

					itemStack.stackSize--;

					return true;
				}
			}
		}

		/*
		 * If this point is reached, the block placement failed, so return false.
		 */
		return false;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IIconRegister iconReg)
	{
		itemIcon = iconReg.registerIcon(ModData.ID + ":itemSpeedSign");
	}
}
