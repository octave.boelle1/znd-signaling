package zoranodensha.common.items;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import zoranodensha.api.structures.signals.ASignalRegistry;
import zoranodensha.api.structures.signals.ISignal;
import zoranodensha.common.blocks.tileEntity.TileEntitySignal;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.common.util.ZnDMathHelper;
import zoranodensha.signals.common.ASignal;



public class ItemSignal extends Item
{
	public static boolean useBingBong = false;



	/**
	 * Initialises a new instance of the
	 * {@link zoranodensha.common.items.ItemSignal} class.
	 */
	public ItemSignal()
	{
		setCreativeTab(ModCreativeTabs.generic);
		setUnlocalizedName(ModData.ID + ".items.itemSignal");
		setMaxStackSize(8);
		setMaxDamage(0);
		setHasSubtypes(true);
	}


	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void getSubItems(Item item, CreativeTabs creativeTab, List list)
	{
		/*
		 * Get all default signals.
		 */
		ArrayList<String> registeredSignals = ASignalRegistry.getRegisteredSignalNames();

		/*
		 * Add each signal to the list.
		 */
		for (int i = 0; i < registeredSignals.size(); i++)
		{
			ItemStack signalItemStack = new ItemStack(ModCenter.ItemSignal, 1, i);

			NBTTagCompound stackNBT = new NBTTagCompound();
			stackNBT.setString("name", registeredSignals.get(i));
			signalItemStack.setTagCompound(stackNBT);

			list.add(signalItemStack);
		}
	}


	@Override
	public String getUnlocalizedName(ItemStack itemStack)
	{
		if (itemStack.hasTagCompound())
		{
			return itemStack.getTagCompound().getString("name");
		}
		return ModData.ID + ".items.itemSignal";
	}


	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int side, float px, float py, float pz)
	{
		Block block = world.getBlock(x, y, z);

		if (block == Blocks.snow_layer && (world.getBlockMetadata(x, y, z) & 7) < 1)
		{
			side = 1;
		}
		else if (block != Blocks.vine && block != Blocks.tallgrass && block != Blocks.deadbush && !block.isReplaceable(world, x, y, z))
		{
			switch (side)
			{
				case 0:
					y--;
					break;
				case 1:
					y++;
					break;
				case 2:
					z--;
					break;
				case 3:
					z++;
					break;
				case 4:
					x--;
					break;
				case 5:
					x++;
					break;
			}
		}

		if (itemStack == null || itemStack.stackSize <= 0 || !itemStack.hasTagCompound())
		{
			return false;
		}
		else if (world.canPlaceEntityOnSide(ModCenter.BlockSignal, x, y, z, false, side, player, itemStack))
		{
			/* First determine ISignal type. */
			ISignal iSignal = ASignalRegistry.getSignalFromName(itemStack.getTagCompound().getString("name"));
			if (iSignal != null)
			{
				/*
				 * Figure out the angle of the signal.
				 */
				int angle = MathHelper.floor_double((double)((player.rotationYaw + 180.0F) * 16.0F / 360.0F) + 0.5D) & 15;

				/*
				 * Then set block at respective coordinates, automatically creating a
				 * TileEntitySignal.
				 */
				// if (world.setBlock(x, y, z, ModCenter.BlockSignal, ZnDMathHelper.getRotation(player.rotationYawHead), 2))
				if (world.setBlock(x, y, z, ModCenter.BlockSignal, angle, 2))
				{
					TileEntity tileEntity = world.getTileEntity(x, y, z);

					if (tileEntity instanceof TileEntitySignal)
					{
						TileEntitySignal signal = (TileEntitySignal)tileEntity;

						/*
						 * Set the signal's default/starting nameplate based on its position.
						 */
						if (iSignal instanceof ASignal)
						{
							String startingID = "";

							int chunkX = Math.abs(x / 16);
							int chunkZ = Math.abs(z / 16);
							int blockChunkX = Math.abs(x % 16);
							int blockChunkZ = Math.abs(z % 16);
							while (chunkX >= 26)
							{
								chunkX -= 26;
							}
							while (chunkZ >= 26)
							{
								chunkZ -= 26;
							}

							startingID = startingID.concat(String.valueOf((char)(65 + chunkX)));
							startingID = startingID.concat(String.valueOf((char)(65 + chunkZ)));
							startingID = startingID.concat(Integer.toHexString(blockChunkX));
							startingID = startingID.concat(Integer.toHexString(blockChunkZ));

							startingID = startingID.toUpperCase();

							((ASignal)iSignal).setID(startingID);
						}

						signal.setSignal(iSignal);

						Random random = new Random();

						world.markBlockForUpdate(x, y, z);
						if (useBingBong)
						{
							world.playSoundEffect(x + 0.5D, y + 0.5D, z + 0.5D, "zoranodensha:block_placement" + (random.nextBoolean() ? "A" : "B"), 0.80F, 0.7F + (0.25F * (random.nextFloat() - random.nextFloat())));
						}
						else
						{
							world.playSoundEffect(x + 0.5D, y + 0.5D, z + 0.5D, "minecraft:random.anvil_land", 0.35F, 0.7F);
						}

						itemStack.stackSize--;
						return true;
					}
				}
			}
		}

		return false;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IIconRegister iconReg)
	{
		itemIcon = iconReg.registerIcon(ModData.ID + ":itemSignal");
	}

	public class BingBongCommand implements ICommand
	{

		@Override
		@SuppressWarnings("rawtypes")
		public List addTabCompletionOptions(ICommandSender sender, String[] text)
		{
			return null;
		}

		@Override
		public boolean canCommandSenderUseCommand(ICommandSender sender)
		{
			return true;
		}

		@Override
		public int compareTo(Object object)
		{
			if (object instanceof ICommand)
			{
				return getCommandName().compareTo(((ICommand)object).getCommandName());
			}

			return 0;
		}

		@Override
		@SuppressWarnings("rawtypes")
		public List getCommandAliases()
		{
			return null;
		}

		@Override
		public String getCommandName()
		{
			return "china";
		}

		@Override
		public String getCommandUsage(ICommandSender sender)
		{
			return String.format("%s.usage", getClass().getCanonicalName().toLowerCase());
		}

		@Override
		public boolean isUsernameIndex(String[] text, int index)
		{
			return false;
		}

		@Override
		public void processCommand(ICommandSender sender, String[] args)
		{
			if (sender instanceof EntityPlayer)
			{
				ItemSignal.useBingBong = !ItemSignal.useBingBong;
				ItemSpeedSign.useBingBong = !ItemSpeedSign.useBingBong;
			}
		}

	}
}
