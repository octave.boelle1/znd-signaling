package zoranodensha.common.items;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import zoranodensha.common.blocks.tileEntity.TileEntityContainerInventory;



public class ItemBlockCoalCoke extends ItemBlock
{
	public ItemBlockCoalCoke(Block block)
	{
		super(block);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void addInformation(ItemStack itemStack, EntityPlayer player, List list, boolean par4)
	{
		if (TileEntityContainerInventory.getBurnTime(itemStack) > 0)
		{
			list.add(TileEntityContainerInventory.getBurnTime(itemStack) + " Fuel Units");
		}
	}
}
