package zoranodensha.common.items;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class ItemIngotSteel extends Item
{
	private static boolean registered = false;



	public ItemIngotSteel()
	{
		setCreativeTab(ModCreativeTabs.generic);
		setUnlocalizedName(ModData.ID + ".items.ingotSteel");
	}


	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister iconReg)
	{
		itemIcon = iconReg.registerIcon(ModData.ID + ":itemIngotSteel");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		OreDictionary.registerOre("ingotSteel", this);
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(this, 9), "blockSteel"));
		GameRegistry.addRecipe(new ShapelessOreRecipe(	new ItemStack(this), "nuggetSteel", "nuggetSteel", "nuggetSteel", "nuggetSteel", "nuggetSteel", "nuggetSteel", "nuggetSteel", "nuggetSteel",
														"nuggetSteel"));
	}
}
