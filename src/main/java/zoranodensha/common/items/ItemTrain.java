package zoranodensha.common.items;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.event.train.TrainSpawnEvent;
import zoranodensha.api.vehicles.handlers.MovementHandler;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.client.gui.table.model.VehicleModel;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.vehicleParts.presets.APreset;



public class ItemTrain extends Item
{
	@SideOnly(Side.CLIENT) private static HashMap<Integer, VehicleModel> vehicleMap;



	public ItemTrain()
	{
		setCreativeTab(ModCreativeTabs.presets);
		setMaxStackSize(1);
		setUnlocalizedName(ModData.ID + ".items.itemTrain");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void addInformation(ItemStack itemStack, EntityPlayer player, List list, boolean advanced)
	{
		NBTTagCompound nbt = itemStack.getTagCompound();
		if (nbt != null)
		{
			if (nbt.hasKey(Train.EDataKey.VEHICLE_DATA.key))
			{
				NBTTagList tagList = nbt.getTagList(Train.EDataKey.VEHICLE_DATA.key, 10);
				NBTTagCompound nbt0 = tagList.getCompoundTagAt(0);

				if (nbt0.hasKey(VehicleData.EDataKey.CREATOR.key))
				{
					String s = nbt0.getString(VehicleData.EDataKey.CREATOR.key);
					if (!s.isEmpty())
					{
						list.add(s);
					}
				}
			}

			if (nbt.hasKey(Train.EDataKey.MAX_SPEED.key))
			{
				int maxSpeed = nbt.getInteger(Train.EDataKey.MAX_SPEED.key);
				if (maxSpeed > 0)
				{
					list.add("Max. " + maxSpeed + "km/h");
				}
			}

			if (nbt.hasKey(Train.EDataKey.MASS.key))
			{
				int mass = nbt.getInteger(Train.EDataKey.MASS.key);
				if (mass > 0)
				{
					list.add(mass + "t");
				}
			}
		}
	}

	@Override
	public String getItemStackDisplayName(ItemStack itemStack)
	{
		if (itemStack.hasTagCompound())
		{
			NBTTagCompound nbt = itemStack.getTagCompound();
			String s = VehicleHelper.getVehicleName(nbt);

			if (s != null)
			{
				if (nbt.getBoolean(Train.EDataKey.IS_EPIC.key))
				{
					s = "§d" + s;
				}

				return s;
			}
		}

		return super.getItemStackDisplayName(itemStack);
	}

	@Override
	public boolean getShareTag()
	{
		return true;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void getSubItems(Item item, CreativeTabs creativeTab, List list)
	{
		ArrayList<NBTTagCompound> defaults = APreset.getPresetList(true);
		ItemStack itemStack;

		for (NBTTagCompound nbt : defaults)
		{
			itemStack = new ItemStack(ModCenter.ItemTrain);
			itemStack.setTagCompound(nbt);
			list.add(itemStack);
		}
	}

	@SideOnly(Side.CLIENT)
	public VehicleModel getVehicle(NBTTagCompound nbt)
	{
		if (nbt == null)
		{
			/* This should never happen. To avoid crashing, simply return null and hope for the best, nonetheless. */
			return null;
		}

		if (vehicleMap == null)
		{
			vehicleMap = new HashMap<Integer, VehicleModel>();
		}

		VehicleModel model = vehicleMap.get(nbt.hashCode());

		if (model == null)
		{
			model = new VehicleModel(Minecraft.getMinecraft().theWorld);
			model.readFromNBT(nbt);

			vehicleMap.put(nbt.hashCode(), model);
		}

		return model;
	}

	@Override
	public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int side, float px, float py, float pz)
	{
		if (itemStack == null || itemStack.stackSize == 0 || !itemStack.hasTagCompound())
		{
			return false;
		}

		boolean isBlueprint = VehicleHelper.getIsBlueprint(itemStack);
		if (!world.isRemote && checkAndDoSpawnVehicle(itemStack, isBlueprint, player, world, x, y, z))
		{
			if (!world.isRemote && !isBlueprint)
			{
				--itemStack.stackSize;
			}
		}

		return true;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerIcons(IIconRegister iconReg)
	{
		itemIcon = iconReg.registerIcon(ModData.ID + ":itemEngineersBlueprint");
	}

	/**
	 * Check whether a vehicle can be spawned at the given coordinate and read from the ItemStack's NBTTagCompound if so.<br>
	 * Spawns the vehicle if successful.
	 *
	 * @return {@code true} if the vehicle was successfully spawned.
	 */
	public static boolean checkAndDoSpawnVehicle(ItemStack itemStack, boolean isBlueprint, EntityPlayer player, World world, int x, int y, int z)
	{
		/*
		 * Ensure the ItemStack has an NBTTagCompound with a RailVehicle
		 */
		NBTTagCompound nbt = itemStack.getTagCompound();
		if (nbt == null || !nbt.hasKey(Train.EDataKey.VEHICLE_DATA.key))
		{
			return false;
		}

		/*
		 * Instantiate train, write new UVID.
		 */
		Train train = new Train(world);
		train.readEntityFromNBT(nbt, ETagCall.ITEM);

		if (train.getVehicleCount() != 1)
		{
			return false;
		}

		/* If enabled, apply instant placement. Otherwise set all isFinished flags to false. */
		if (isBlueprint)
		{
			final boolean flag = ModCenter.cfg.blocks_items.enableInstantVehicles && player.capabilities.isCreativeMode;
			for (VehParBase part : train.getVehicleParts(OrderedPartsList.SELECTOR_ANY))
			{
				part.setIsFinished(flag);
			}
		}

		/*
		 * Call MovementHandler#canSpawnVehicle() to handle further checks
		 */
		if (MovementHandler.INSTANCE.canSpawnVehicle(world, x, y, z, player, train))
		{
			/* Spawn vehicle and return */
			if (!MinecraftForge.EVENT_BUS.post(new TrainSpawnEvent(world, x, y, z, player, train)) && world.spawnEntityInWorld(train))
			{
				return true;
			}
		}

		return false;
	}
}