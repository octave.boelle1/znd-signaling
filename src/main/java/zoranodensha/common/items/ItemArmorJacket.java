package zoranodensha.common.items;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.common.items.ItemArmorVest.EVest;



public class ItemArmorJacket extends ItemArmor
{
	private boolean registered = false;
	private final EVest type;



	public ItemArmorJacket(EVest type)
	{
		super(EnumHelper.addArmorMaterial("Jacket_Material", 10, new int[] { 0, 6, 0, 0 }, 30), 0, 1);
		this.type = type;
		
		setCreativeTab(ModCreativeTabs.generic);
		setMaxStackSize(1);
		setUnlocalizedName(ModData.ID + ".items.jacket" + type.toName());
	}

	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String subtype)
	{
		return ModData.ID + ":textures/armor/jacket" + type.toName() + ".png";
	}

	@Override
	public void registerIcons(IIconRegister iconRegister)
	{
		itemIcon = iconRegister.registerIcon(ModData.ID + ":itemJacket" + type.toName());
	}

	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this), new Object[] { "YDY", "YCY", "BBB", 'Y', new ItemStack(Blocks.wool, 1, 4), 'C', Items.leather_chestplate, 'B', new ItemStack(Blocks.wool, 1, 11), 'D', type.oreName }));
	}
}