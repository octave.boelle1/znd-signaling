package zoranodensha.common.blocks;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import zoranodensha.common.blocks.tileEntity.TileEntityItemFluidHandler;
import zoranodensha.common.blocks.tileEntity.TileEntityRetorter;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.handlers.EGui;



public class BlockRetorter extends BlockContainer
{
	public BlockRetorter()
	{
		super(Material.iron);

		setBlockName(ModData.ID + ".blocks.retorter");
		setHardness(7.5F);
		setResistance(12.5F);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return (meta < 2 ? new TileEntityRetorter() : new TileEntityItemFluidHandler());
	}

	@Override
	public Item getItemDropped(int i, Random ran, int i1)
	{
		return null;
	}

	@Override
	public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z, EntityPlayer player)
	{
		return null;
	}

	@Override
	public int getRenderType()
	{
		return -1;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float xf, float yf, float zf)
	{
		switch (world.getBlockMetadata(x, y, z))
		{
			case 0:
			case 1:
				player.openGui(ModCenter.instance, EGui.RETORTER.ordinal(), world, x, y, z);
				return true;

			case 2:
				return world.getBlock(x, y, z - 1).onBlockActivated(world, x, y, z - 1, player, side, xf, yf, zf);

			case 3:
				return world.getBlock(x - 1, y, z).onBlockActivated(world, x - 1, y, z, player, side, xf, yf, zf);

			case 4:
				return world.getBlock(x, y, z + 1).onBlockActivated(world, x, y, z + 1, player, side, xf, yf, zf);

			case 5:
				return world.getBlock(x + 1, y, z).onBlockActivated(world, x + 1, y, z, player, side, xf, yf, zf);
		}

		return false;
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		if (!world.isRemote)
		{
			switch (world.getBlockMetadata(x, y, z))
			{
				case 0:
					if (world.isAirBlock(x, y, z - 1) || world.isAirBlock(x, y, z + 1))
					{
						world.setBlock(x, y, z - 1, Blocks.iron_block, 0, 2);
						world.setBlock(x, y, z, Blocks.iron_block, 0, 2);
						world.setBlock(x, y, z + 1, Blocks.iron_block, 0, 2);
					}
					break;

				case 1:
					if (world.isAirBlock(x - 1, y, z) || world.isAirBlock(x + 1, y, z))
					{
						world.setBlock(x - 1, y, z, Blocks.iron_block, 0, 2);
						world.setBlock(x, y, z, Blocks.iron_block, 0, 2);
						world.setBlock(x + 1, y, z, Blocks.iron_block, 0, 2);
					}
					break;

				case 2:
					if (world.isAirBlock(x, y, z - 1) || world.isAirBlock(x, y, z - 2))
					{
						world.setBlock(x, y, z, Blocks.iron_block, 0, 2);
						world.setBlock(x, y, z - 1, Blocks.iron_block, 0, 2);
						world.setBlock(x, y, z - 2, Blocks.iron_block, 0, 2);
					}
					break;

				case 3:
					if (world.isAirBlock(x - 1, y, z) || world.isAirBlock(x - 2, y, z))
					{
						world.setBlock(x, y, z, Blocks.iron_block, 0, 2);
						world.setBlock(x - 1, y, z, Blocks.iron_block, 0, 2);
						world.setBlock(x - 2, y, z, Blocks.iron_block, 0, 2);
					}
					break;

				case 4:
					if (world.isAirBlock(x, y, z + 1) || world.isAirBlock(x, y, z + 2))
					{
						world.setBlock(x, y, z, Blocks.iron_block, 0, 2);
						world.setBlock(x, y, z + 1, Blocks.iron_block, 0, 2);
						world.setBlock(x, y, z + 2, Blocks.iron_block, 0, 2);
					}
					break;

				case 5:
					if (world.isAirBlock(x + 1, y, z) || world.isAirBlock(x + 2, y, z))
					{
						world.setBlock(x, y, z, Blocks.iron_block, 0, 2);
						world.setBlock(x + 1, y, z, Blocks.iron_block, 0, 2);
						world.setBlock(x + 2, y, z, Blocks.iron_block, 0, 2);
					}
					break;
			}
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void randomDisplayTick(World world, int x, int y, int z, Random ran)
	{
		TileEntity tileEntity = null;

		switch (world.getBlockMetadata(x, y, z))
		{
			case 0:
			case 1:
				tileEntity = world.getTileEntity(x, y, z);
				break;

			case 2:
				tileEntity = world.getTileEntity(x, y, z - 1);
				break;

			case 3:
				tileEntity = world.getTileEntity(x - 1, y, z);
				break;

			case 4:
				tileEntity = world.getTileEntity(x, y, z + 1);
				break;

			case 5:
				tileEntity = world.getTileEntity(x + 1, y, z);
				break;
		}

		if (tileEntity instanceof TileEntityRetorter)
		{
			int i = ((TileEntityRetorter)tileEntity).data[0];

			if (i > 0)
			{
				double d0 = x + 0.5D;
				double d1 = y + 0.625D + ran.nextDouble() * 0.375D;
				double d2 = z + 0.5D;
				double d3 = ran.nextDouble() * 0.6D - 0.3D;

				world.spawnParticle("smoke", d0 + d3 + 0.3D, d1, d2 + d3, 0.0D, ran.nextDouble() * 0.1D, 0.0D);
			}
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister icon)
	{
		blockIcon = icon.registerIcon(ModData.ID + ":blockCollisionbox");
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}
}
