package zoranodensha.common.blocks;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import zoranodensha.api.structures.signals.ISignal;
import zoranodensha.api.structures.signals.ISpeedSign;
import zoranodensha.api.structures.signals.SignalProperty;
import zoranodensha.api.util.Vec2;
import zoranodensha.common.blocks.tileEntity.TileEntitySignal;
import zoranodensha.common.blocks.tileEntity.TileEntitySpeedSign;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.handlers.EGui;
import zoranodensha.common.util.ZnDMathHelper;
import zoranodensha.signals.common.ASpeedSign;



public class BlockSpeedSign extends BlockContainer
{
	public BlockSpeedSign()
	{
		super(Material.iron);

		setBlockName(ModData.ID + ".blocks.speedSign");
		setBlockTextureName(ModData.ID + ":blockSpeedSign");

		setHardness(7.0F);
		setHarvestLevel("pickaxe", 2);
		setResistance(6.0F);

		this.isBlockContainer = true;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void addCollisionBoxesToList(World world, int x, int y, int z, AxisAlignedBB mask, List list, Entity entity)
	{
		return;
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntitySpeedSign();
	}

	@Override
	public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune)
	{
		return new ArrayList<ItemStack>();
	}

	@Override
	public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z, EntityPlayer player)
	{
		TileEntity tileEntity = world.getTileEntity(x, y, z);
		if (tileEntity instanceof TileEntitySpeedSign)
		{
			ISpeedSign sign = ((TileEntitySpeedSign)tileEntity).getSpeedSign();
			if (sign != null)
			{
				NBTTagCompound stackCompound = new NBTTagCompound();
				stackCompound.setString("name", sign.getName());

				ItemStack stack = new ItemStack(ModCenter.ItemSpeedSign);
				stack.setTagCompound(stackCompound);
				return stack;
			}
		}

		return null;
	}

	@Override
	public int getRenderType()
	{
		return -1;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float px, float py, float pz)
	{
		player.openGui(ModCenter.instance, EGui.SPEED_SIGN.ordinal(), world, x, y, z);
		return true;
	}

	@Override
	public boolean removedByPlayer(World world, @Nullable EntityPlayer player, int x, int y, int z, boolean willHarvest)
	{
		if (!world.isRemote)
		{
			/* Ensure to drop this sign only if the player was not in creative mode. */
			if (player == null || !player.capabilities.isCreativeMode)
			{
				TileEntity tileEntity = world.getTileEntity(x, y, z);
				if (tileEntity instanceof TileEntitySpeedSign)
				{
					ISpeedSign speedSign = ((TileEntitySpeedSign)tileEntity).getSpeedSign();
					if (speedSign != null)
					{
						/* Use the sign's name to spawn an item stack. */
						NBTTagCompound stackCompound = new NBTTagCompound();
						stackCompound.setString("name", speedSign.getName());

						ItemStack stack = new ItemStack(ModCenter.ItemSpeedSign);
						stack.setTagCompound(stackCompound);

						this.dropBlockAsItem(world, x, y, z, stack);
					}
				}
			}

			return world.setBlockToAir(x, y, z);
		}

		return false;
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z)
	{
		/*
		 * Declare the width of the speed sign, which will always be 0.2 m in this case.
		 */
		Vec2 bounds = new Vec2(0.2F, 1.5F);
		float nudgeX = 0.0F;
		float nudgeZ = 0.0F;
		int offset = 0;

		// Get the tile entity from the world.
		TileEntity attemptTileEntity = world.getTileEntity(x, y, z);
		ISpeedSign attemptSign = null;

		// Try to extract the speed sign instance out of the tile entity.
		if(attemptTileEntity instanceof TileEntitySpeedSign)
		{
			attemptSign = ((TileEntitySpeedSign)attemptTileEntity).getSpeedSign();
		}

		// If a speed sign was extracted, get the current offset of it.
		if(attemptSign instanceof ASpeedSign)
		{
			offset = ((ASpeedSign)attemptSign).getOffset();
		}

		// Determine the rotational position based on the metadata and offset it appropriately.
		int metaData = world.getBlockMetadata(x, y, z);
		double radians = ZnDMathHelper.RAD_MULTIPLIER * (metaData * 22.5D);
		nudgeX = (float) Math.cos(radians) * (0.25F * (float) offset);
		nudgeZ = (float) Math.sin(radians) * (0.25F * (float) offset);

		setBlockBounds(0.5F - (bounds.x / 2.0F) + nudgeX, 0.0F, 0.5F - (bounds.x / 2.0F) + nudgeZ, 0.5F + (bounds.x / 2.0F) + nudgeX, bounds.y, 0.5F + (bounds.x / 2.0F) + nudgeZ);
	}
}