package zoranodensha.common.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import mods.railcraft.api.core.items.IToolCrowbar;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.common.blocks.tileEntity.TileEntityRailwaySign;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.common.items.ItemRailwaySign;
import zoranodensha.common.util.ZnDMathHelper;



public class BlockRailwaySign extends BlockContainer
{
	private static boolean registered = false;



	public BlockRailwaySign()
	{
		super(Material.iron);

		setBlockName(ModData.ID + ".blocks.railwaySign");
		setCreativeTab(ModCreativeTabs.generic);
		setHardness(2.5F);
		setResistance(5.0F);
	}

	@Override
	public boolean canPlaceBlockAt(World world, int x, int y, int z)
	{
		return world.getBlock(x, y, z).isReplaceable(world, x, y, z) && world.getBlock(x, ++y, z).isReplaceable(world, x, y, z);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return (meta == 0 ? new TileEntityRailwaySign() : null);
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z)
	{
		setBlockBoundsBasedOnState(world, x, y, z);

		return AxisAlignedBB.getBoundingBox(x + minX, y + minY, z + minZ, x + maxX, y + maxY, z + maxZ);
	}

	@Override
	public int getRenderType()
	{
		return -1;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float xf, float yf, float zf)
	{
		if (world.getBlockMetadata(x, y, z) == 0)
		{
			TileEntity tileEntity = world.getTileEntity(x, y, z);
			ItemStack playerItem = player.getHeldItem();

			if (tileEntity instanceof TileEntityRailwaySign && playerItem != null)
			{
				TileEntityRailwaySign tileEntitySign = (TileEntityRailwaySign)tileEntity;

				if (playerItem.getItem() instanceof ItemRailwaySign)
				{
					int rotation = ZnDMathHelper.getRotation(player.rotationYawHead);

					if (playerItem.getItemDamage() < 6)
					{
						if (tileEntitySign.getSignID(0) == -1 && tileEntitySign.getSignID(1) == -1)
						{
							tileEntitySign.setSignIn(0, playerItem.getItemDamage() * 2, rotation);
							tileEntitySign.setSignIn(1, playerItem.getItemDamage() * 2 + 1, rotation);

							--playerItem.stackSize;

							return true;
						}
					}
					else
					{
						for (int i = 0; i < 2; ++i)
						{
							if (tileEntitySign.getSignID(i) == -1)
							{
								tileEntitySign.setSignIn(i, playerItem.getItemDamage() + 6, rotation);
								--playerItem.stackSize;

								return true;
							}
						}
					}
				}
				else if (playerItem.getItem() instanceof IToolCrowbar)
				{
					for (int i = 1; i > -1; --i)
					{
						if (tileEntitySign.getSignID(i) > 11)
						{
							if (player.inventory.addItemStackToInventory(new ItemStack(ModCenter.ItemRailwaySign, 1, ERailwaySign.toSign(tileEntitySign.getSignID(i)).toItemInt())))
							{
								tileEntitySign.clearSignIn(i);

								return true;
							}
						}
						else if (i == 0 && tileEntitySign.getSignID(i) >= 0)
						{
							if (player.inventory.addItemStackToInventory(new ItemStack(ModCenter.ItemRailwaySign, 1, ERailwaySign.toSign(tileEntitySign.getSignID(i)).toItemInt())))
							{
								tileEntitySign.clearSignIn(i);
								tileEntitySign.clearSignIn(i + 1);

								return true;
							}
						}
					}
				}
			}
		}
		else
		{
			Block block = world.getBlock(x, --y, z);

			if (block instanceof BlockRailwaySign)
			{
				return block.onBlockActivated(world, x, y, z, player, side, xf, yf, zf);
			}
		}

		return false;
	}

	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack itemStack)
	{
		if (!world.isRemote && world.getBlock(x, ++y, z) == Blocks.air)
		{
			world.setBlock(x, y, z, this, 1, 2);
		}
	}

	@Override
	public void onBlockPreDestroy(World world, int x, int y, int z, int oldMeta)
	{
		if (oldMeta == 0)
		{
			TileEntity tileEntity = world.getTileEntity(x, y, z);

			if (tileEntity instanceof TileEntityRailwaySign)
			{
				TileEntityRailwaySign tileEntitySign = (TileEntityRailwaySign)tileEntity;

				if (tileEntitySign.getSignID(0) > -1)
				{
					this.dropBlockAsItem(world, x, y, z, new ItemStack(ModCenter.ItemRailwaySign, 1, ERailwaySign.toSign(tileEntitySign.getSignID(0)).toItemInt()));
				}

				if (tileEntitySign.getSignID(1) > 11)
				{
					this.dropBlockAsItem(world, x, y, z, new ItemStack(ModCenter.ItemRailwaySign, 1, ERailwaySign.toSign(tileEntitySign.getSignID(1)).toItemInt()));
				}
			}
		}
		else
		{
			Block block0 = world.getBlock(x, --y, z);

			if (block0 instanceof BlockRailwaySign)
			{
				block0.onBlockPreDestroy(world, x, y, z, oldMeta);
			}
		}
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		if (!world.isRemote && world.isAirBlock(x, (world.getBlockMetadata(x, y, z) == 0 ? y + 1 : y - 1), z))
		{
			world.setBlockToAir(x, y, z);
		}
	}


	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister icon)
	{
		blockIcon = icon.registerIcon(ModData.ID + ":blockIconRailwaySign");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 2), " SW", "ISW", " SW", 'S', "stone", 'I', "ingotIron", 'W', "dyeWhite"));
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z)
	{
		switch (world.getBlockMetadata(x, y, z))
		{
			default:
				setBlockBounds(0.46875F, 0.0F, 0.46875F, 0.53125F, 1.0F, 0.53125F);

			case 1:
				setBlockBounds(0.46875F, 0.0F, 0.46875F, 0.53125F, 0.75175F, 0.53125F);
		}
	}



	/**
	 * All railway signs added by Zora no Densha.
	 */
	public static enum ERailwaySign
	{
		/** Crossing sign (single crossing within normal distance). Top part */
		CROSSING_SiNo_Top(0, 0, 30, 1),
		/** Crossing sign (single crossing within normal distance). Bottom part */
		CROSSING_SiNo_Bot(1, 0, 30, 14),
		/** Crossing sign (single crossing within short distance). Top part */
		CROSSING_SiSh_Top(2, 1, 37, 1),
		/** Crossing sign (single crossing within short distance). Bottom part */
		CROSSING_SiSh_Bot(3, 1, 37, 14),
		/** Crossing sign (several crossings within normal distance). Top part */
		CROSSING_DuNo_Top(4, 2, 44, 1),
		/** Crossing sign (several crossings within normal distance). Bottom part */
		CROSSING_DuNo_Bot(5, 2, 44, 14),
		/** Crossing sign (several crossings within short distance). Top part */
		CROSSING_DuSh_Top(6, 3, 51, 1),
		/** Crossing sign (several crossings within short distance). Bottom part */
		CROSSING_DuSh_Bot(7, 3, 51, 14),

		/** Information sign (Checkerboard indicating misplaced signal/ sign). Top part */
		INFORMATION_Check_Top(8, 4, 58, 1),
		/** Information sign (Checkerboard indicating misplaced signal/ sign). Bottom part */
		INFORMATION_Check_Bot(9, 4, 58, 14),
		/** Information sign (Arrow indicating end of speed restriction). Top part */
		INFORMATION_SpeedEnd_Top(10, 5, 65, 1),
		/** Information sign (Arrow indicating end of speed restriction). Bottom part */
		INFORMATION_SpeedEnd_Bot(11, 5, 65, 14),
		/** Information sign (Switch ahead). */
		INFORMATION_Switch(12, 6, 44, 27),
		/** Information sign (Crossing box nearby). */
		INFORMATION_Box(13, 7, 58, 27),
		/** Information sign (End of track within braking distance). */
		INFORMATION_End(14, 8, 65, 27),

		/** Command sign (Whistle here). */
		COMMAND_Whistle(15, 9, 30, 27),
		/** Command sign (Whistle here, only if not stopping). */
		COMMAND_Whistle1(16, 10, 37, 27),
		/** Command sign (Stop here, only if required). */
		COMMAND_Stop(17, 11, 51, 27),
		/** Command sign (Speed limit of 10 km/h). */
		COMMAND_Speed_10(18, 12, 30, 40),
		/** Command sign (Speed limit of 30 km/h). */
		COMMAND_Speed_30(19, 13, 37, 40),
		/** Command sign (Speed limit of 50 km/h). */
		COMMAND_Speed_50(20, 14, 44, 40),
		/** Command sign (Speed limit of 70 km/h). */
		COMMAND_Speed_70(21, 15, 51, 40),
		/** Command sign (Speed limit of 90 km/h). */
		COMMAND_Speed_90(22, 16, 58, 40),
		/** Command sign (Speed limit of 110 km/h). */
		COMMAND_Speed_110(23, 17, 65, 40),

		/** Construction sign (Construction area starts here). */
		CONSTRUCTION_Start(24, 18, 30, 53),
		/** Construction sign (Construction area ends here). */
		CONSTRUCTION_End(25, 19, 37, 53),
		/** Construction sign (Ignore signal). */
		CONSTRUCTION_Ignore(26, 20, 44, 53),
		/** Construction sign (Stop in front of crossing and close manually/ Crossing deactivated). */
		CONSTRUCTION_Crossing(27, 21, 51, 53),
		/** Construction sign (Speed limit of 10km/h, only inside the given construction area). */
		CONSTRUCTION_Speed_10(28, 22, 30, 66),
		/** Construction sign (Speed limit of 30km/h, only inside the given construction area). */
		CONSTRUCTION_Speed_30(29, 23, 37, 66),
		/** Construction sign (Speed limit of 50km/h, only inside the given construction area). */
		CONSTRUCTION_Speed_50(30, 24, 44, 66),
		/** Construction sign (Speed limit of 70km/h, only inside the given construction area). */
		CONSTRUCTION_Speed_70(31, 25, 51, 66),
		/** Construction sign (Speed limit of 90km/h, only inside the given construction area). */
		CONSTRUCTION_Speed_90(32, 26, 58, 66),
		/** Construction sign (Speed limit of 110km/h, only inside the given construction area). */
		CONSTRUCTION_Speed_110(33, 27, 65, 66),

		/** Railway Sign (Entering Class H Section). */
		RAILWAY_Class_H(34, 28, 30, 79),
		/** Railway Sign (Entering Class R Section). */
		RAILWAY_Class_R(35, 29, 37, 79),
		/** Railway Sign (Entering Class S Section). */
		RAILWAY_Class_S(36, 30, 44, 79),
		/** Railway Sign (Entering Class P Section). */
		RAILWAY_Class_P(37, 31, 51, 79);

		private final int sign_index;
		private final int item_index;
		private final int[] texCoords;



		private ERailwaySign(int signID, int itemID, int... coordinates)
		{
			sign_index = signID;
			item_index = itemID;
			texCoords = coordinates;
		}

		/**
		 * Returns the coordinate indices of this ERailwaySign in an integer array.
		 */
		public int[] toCoordinate()
		{
			return texCoords;
		}

		/**
		 * Returns an integer representing the given sign.
		 */
		public int toInt()
		{
			return sign_index;
		}

		/**
		 * Returns an integer representing the given sign's item.
		 */
		public int toItemInt()
		{
			return item_index;
		}

		public static ERailwaySign toSign(int i)
		{
			switch (i)
			{
				default:
					return null;

				case 0:
					return CROSSING_SiNo_Top;
				case 1:
					return CROSSING_SiNo_Bot;
				case 2:
					return CROSSING_SiSh_Top;
				case 3:
					return CROSSING_SiSh_Bot;
				case 4:
					return CROSSING_DuNo_Top;
				case 5:
					return CROSSING_DuNo_Bot;
				case 6:
					return CROSSING_DuSh_Top;
				case 7:
					return CROSSING_DuSh_Bot;

				case 8:
					return INFORMATION_Check_Top;
				case 9:
					return INFORMATION_Check_Bot;
				case 10:
					return INFORMATION_SpeedEnd_Top;
				case 11:
					return INFORMATION_SpeedEnd_Bot;
				case 12:
					return INFORMATION_Switch;
				case 13:
					return INFORMATION_Box;
				case 14:
					return INFORMATION_End;

				case 15:
					return COMMAND_Whistle;
				case 16:
					return COMMAND_Whistle1;
				case 17:
					return COMMAND_Stop;
				case 18:
					return COMMAND_Speed_10;
				case 19:
					return COMMAND_Speed_30;
				case 20:
					return COMMAND_Speed_50;
				case 21:
					return COMMAND_Speed_70;
				case 22:
					return COMMAND_Speed_90;
				case 23:
					return COMMAND_Speed_110;

				case 24:
					return CONSTRUCTION_Start;
				case 25:
					return CONSTRUCTION_End;
				case 26:
					return CONSTRUCTION_Ignore;
				case 27:
					return CONSTRUCTION_Crossing;
				case 28:
					return CONSTRUCTION_Speed_10;
				case 29:
					return CONSTRUCTION_Speed_30;
				case 30:
					return CONSTRUCTION_Speed_50;
				case 31:
					return CONSTRUCTION_Speed_70;
				case 32:
					return CONSTRUCTION_Speed_90;
				case 33:
					return CONSTRUCTION_Speed_110;

				case 34:
					return RAILWAY_Class_H;
				case 35:
					return RAILWAY_Class_R;
				case 36:
					return RAILWAY_Class_S;
				case 37:
					return RAILWAY_Class_P;
			}
		}
	}
}
