package zoranodensha.common.blocks;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.fluids.EFluid;



public class BlockFluid extends BlockFluidClassic
{
	private IIcon[] icons = new IIcon[2];
	private final EFluid type;



	public BlockFluid(Fluid fluid, EFluid type)
	{
		super(fluid, ModCenter.Material_Fluid);

		setBlockName(ModData.ID + ".blocks.fluid_" + type.ordinal());
		this.type = type;
	}

	@Override
	public boolean canDisplace(IBlockAccess world, int x, int y, int z)
	{
		if (world.getBlock(x, y, z).getMaterial().isLiquid())
		{
			return false;
		}

		return super.canDisplace(world, x, y, z);
	}

	@Override
	public boolean displaceIfPossible(World world, int x, int y, int z)
	{
		if (world.getBlock(x, y, z).getMaterial().isLiquid())
		{
			return false;
		}

		return super.displaceIfPossible(world, x, y, z);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		return (side == 0 || side == 1) ? icons[0] : icons[1];
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconReg)
	{
		for (int i = 0; i < icons.length; ++i)
		{
			icons[i] = iconReg.registerIcon(ModData.ID + ":blockFluid_" + type.ordinal() + "_" + i);
		}
	}
}
