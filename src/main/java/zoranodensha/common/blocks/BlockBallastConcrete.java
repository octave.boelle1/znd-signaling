package zoranodensha.common.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class BlockBallastConcrete extends Block
{
	private static boolean registered = false;



	public BlockBallastConcrete()
	{
		super(ModCenter.Material_Ballast);

		setBlockName(ModData.ID + ".blocks.ballastConcrete");
		setCreativeTab(ModCreativeTabs.generic);
		setHardness(4.0F);
		this.setHarvestLevel("pickaxe", 1);
		setResistance(8.5F);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public boolean getCanBlockGrass()
	{
		return false;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister icon)
	{
		blockIcon = icon.registerIcon(ModData.ID + ":blockBallastConcrete");
	}

	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}
		registered = true;
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this, 4), "GWG", "SGS", "TTT", 'G', new ItemStack(Blocks.gravel, 1, 0), 'W', Items.water_bucket, 'S', "sand", 'T', "stone"));
	}
}