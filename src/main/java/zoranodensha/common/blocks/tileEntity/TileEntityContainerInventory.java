package zoranodensha.common.blocks.tileEntity;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.MathHelper;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.util.Helpers;
import zoranodensha.common.util.ZnDMathHelper;



public abstract class TileEntityContainerInventory extends TileEntity implements ISidedInventory
{
	/** An array containing CraftingHandler data. Usually: 0 = burnTime; 1 = burnTimeMax; 2 = process */
	public int[] data = new int[3];
	/** The contents of the slots */
	public ItemStack[] slots;
	/** The default name of this TileEntity. */
	public final String name;
	/** A custom name for this block, if set. */
	public String customName;



	public TileEntityContainerInventory(ItemStack[] slots, String name)
	{
		this.slots = slots;
		this.name = name;
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack itemStack, int side)
	{
		return true;
	}

	/**
	 * Called to check whether the process is finished.
	 */
	protected abstract boolean canFinish();

	@Override
	public boolean canInsertItem(int slot, ItemStack itemStack, int side)
	{
		return isItemValidForSlot(slot, itemStack);
	}

	/**
	 * Called to check whether the process may continue.
	 */
	protected abstract boolean canProcess();

	@Override
	public void closeInventory()
	{
	}

	@Override
	public ItemStack decrStackSize(int slot, int dec)
	{
		if (isIndexPermitted(slot, 1))
		{
			if (slots[slot] != null)
			{
				ItemStack itemstack;

				if (slots[slot].stackSize <= dec)
				{
					itemstack = slots[slot];

					slots[slot] = null;
				}
				else
				{
					itemstack = slots[slot].splitStack(dec);

					if (slots[slot].stackSize == 0)
					{
						slots[slot] = null;
					}
				}

				markForUpdate();

				return itemstack;
			}
		}

		return null;
	}

	public int getBurnTimeScaled()
	{
		if (isIndexPermitted(0, 0) && isIndexPermitted(1, 0))
		{
			if (data[1] == 0)
			{
				data[1] = 200;
			}

			return (data[0] * 13 / data[1]);
		}

		return 0;
	}

	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);

		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, nbt);
	}

	@Override
	public String getInventoryName()
	{
		return (hasCustomInventoryName() ? customName : "tile." + ModData.ID + ".tileEntity." + name);
	}

	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}

	@Override
	public int getSizeInventory()
	{
		return slots.length;
	}

	@Override
	public ItemStack getStackInSlot(int slot)
	{
		return (isIndexPermitted(slot, 1) ? slots[slot] : null);
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int slot)
	{
		if (isIndexPermitted(slot, 1) && slots[slot] != null)
		{
			ItemStack itemStack = slots[slot];
			slots[slot] = null;

			return itemStack;
		}

		return null;
	}

	@Override
	public boolean hasCustomInventoryName()
	{
		return !Helpers.isUndefined(customName);
	}

	@Override
	public void invalidate()
	{
		super.invalidate();

		if (!worldObj.isRemote && ModCenter.cfg.blocks_items.multiBlockDrop)
		{
			for (int i = 0; i < getSizeInventory(); ++i)
			{
				ItemStack itemStack = getStackInSlot(i);

				if (itemStack != null)
				{
					float fX = ZnDMathHelper.ran.nextFloat() * 0.8F + 0.1F;
					float fY = ZnDMathHelper.ran.nextFloat() * 0.8F + 0.1F;
					float fZ = ZnDMathHelper.ran.nextFloat() * 0.8F + 0.1F;

					while (itemStack.stackSize > 0)
					{
						int j = MathHelper.clamp_int(ZnDMathHelper.ran.nextInt(21) + 10, 0, itemStack.stackSize);

						itemStack.stackSize -= j;
						EntityItem entityItem = new EntityItem(worldObj, xCoord + fX, yCoord + fY, zCoord + fZ, new ItemStack(itemStack.getItem(), j, itemStack.getItemDamage()));

						if (itemStack.hasTagCompound())
						{
							entityItem.getEntityItem().setTagCompound((NBTTagCompound)itemStack.getTagCompound().copy());
						}

						entityItem.motionX = ZnDMathHelper.ran.nextGaussian() * 0.05F;
						entityItem.motionY = ZnDMathHelper.ran.nextGaussian() * 0.05F + 0.2D;
						entityItem.motionZ = ZnDMathHelper.ran.nextGaussian() * 0.05F;
						worldObj.spawnEntityInWorld(entityItem);
					}
				}
			}
		}
	}

	/**
	 * Returns true if the given index is permitted to be used (i.e. wouldn't throw an Exception).
	 */
	public boolean isIndexPermitted(int index, int type)
	{
		switch (type)
		{
			case 0:
				return (index >= 0 && index < data.length);

			case 1:
				return (index >= 0 && index < slots.length);
		}

		return false;
	}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack itemStack)
	{
		return (isIndexPermitted(slot, 1) && itemStack != null);
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player)
	{
		return worldObj.getTileEntity(xCoord, yCoord, zCoord) == this && player.getDistanceSq(xCoord + 0.5D, yCoord + 0.5D, zCoord + 0.5D) <= 64.0D;
	}

	public void markForUpdate()
	{
		markDirty();
		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
	}

	@Override
	public void onDataPacket(NetworkManager networkManager, S35PacketUpdateTileEntity packet)
	{
		readFromNBT(packet.func_148857_g());
	}

	/**
	 * Called to replenish burn times.
	 */
	protected void onRefuel()
	{
		data[1] = data[0] = getBurnTime(slots[1]);

		if (data[0] > 0)
		{
			if (slots[1] != null)
			{
				--slots[1].stackSize;

				if (slots[1].stackSize == 0)
				{
					slots[1] = slots[1].getItem().getContainerItem(slots[1]);
				}
			}
		}
	}

	/**
	 * Called when a process is finished.
	 */
	protected abstract void onSmelt();

	@Override
	public void openInventory()
	{
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		for (int i = 0; i < data.length; ++i)
		{
			data[i] = nbt.getInteger("Data_" + i);
		}

		slots = new ItemStack[getSizeInventory()];

		NBTTagList tagList = nbt.getTagList("Items", 10);

		for (int i = 0; i < tagList.tagCount(); ++i)
		{
			NBTTagCompound nbt0 = tagList.getCompoundTagAt(i);
			byte b = nbt0.getByte("Slot");

			if (b >= 0 && b < slots.length)
			{
				try
				{
					slots[b] = ItemStack.loadItemStackFromNBT(nbt0);
				}
				catch (Throwable t)
				{
				}
			}
		}

		if (nbt.hasKey("CustomName", 8))
		{
			customName = nbt.getString("CustomName");
		}
	}

	@Override
	public void setInventorySlotContents(int slot, ItemStack itemStack)
	{
		if (isIndexPermitted(slot, 1))
		{
			slots[slot] = (itemStack != null && itemStack.stackSize < 1 ? null : itemStack);

			if (itemStack != null && itemStack.stackSize > getInventoryStackLimit())
			{
				itemStack.stackSize = getInventoryStackLimit();
			}

			markForUpdate();
		}
	}

	@Override
	public void updateEntity()
	{
		boolean flag0 = data[0] > 0;
		boolean flag1 = false;

		if (flag0)
		{
			--data[0];
		}

		if (!worldObj.isRemote)
		{
			if (data[0] != 0 || (slots[1] != null && slots[0] != null))
			{
				if (data[0] == 0 && canProcess())
				{
					onRefuel();
					flag1 = true;
				}

				if (data[0] > 0 && canProcess())
				{
					++data[2];

					if (canFinish())
					{
						onSmelt();
						data[2] = 0;
						flag1 = true;
					}
				}
				else
				{
					data[0] = data[1] = data[2] = 0;
				}
			}

			if (flag0 != data[0] > 0)
			{
				flag1 = true;
			}

			if (data[0] == 0)
			{
				data[2] = 0;
				flag1 = true;
			}
		}

		if (flag1)
		{
			markForUpdate();
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);

		for (int i = 0; i < data.length; ++i)
		{
			nbt.setInteger("Data_" + i, data[i]);
		}

		NBTTagList tagList = new NBTTagList();

		for (int i = 0; i < slots.length; ++i)
		{
			if (slots[i] != null)
			{
				NBTTagCompound nbt0 = new NBTTagCompound();
				slots[i].writeToNBT(nbt0).setByte("Slot", (byte)i);
				tagList.appendTag(nbt0);
			}
		}

		nbt.setTag("Items", tagList);

		if (hasCustomInventoryName())
		{
			nbt.setString("CustomName", customName);
		}
	}

	/**
	 * Returns the amount of ticks the given Item burns. 0 if the Item is no fuel.
	 */
	public static int getBurnTime(ItemStack itemStack)
	{
		int i = TileEntityFurnace.getItemBurnTime(itemStack);
		if (i == 0 && itemStack != null)
		{
			i = GameRegistry.getFuelValue(itemStack);
		}

		return i;
	}
}
