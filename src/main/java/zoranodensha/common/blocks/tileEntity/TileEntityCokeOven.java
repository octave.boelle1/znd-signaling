package zoranodensha.common.blocks.tileEntity;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.IFluidHandler;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.util.OreDictionaryHelper;
import zoranodensha.common.util.ZnDMathHelper;



public class TileEntityCokeOven extends TileEntityContainerInventory implements IFluidHandler
{
	private static final int[] slotsAccept = new int[] { 0, 1, 2, 4 };
	private static final Map<Item, int[]> itemsFluid = new HashMap<Item, int[]>();
	private static final Map<Item, ItemStack> itemsResult = new HashMap<Item, ItemStack>();
	// 0 = Item Input; 1 = Item Output; 2 = Bucket Input; 3 = Bucket Output
	static
	{
		for (Item item : OreDictionaryHelper.getIndicesInOreDictionaryExtended("log"))
		{
			itemsFluid.put(item, new int[] { 200, 200 });
			itemsResult.put(item, new ItemStack(Items.coal, 1, 1));
		}

		itemsFluid.put(Items.coal, new int[] { 500, 500 });
		itemsResult.put(Items.coal, new ItemStack(ModCenter.ItemCoalCoke));
		
		itemsFluid.put(Item.getItemFromBlock(Blocks.coal_block), new int[] { 4500, 4500 });
		itemsResult.put(Item.getItemFromBlock(Blocks.coal_block), new ItemStack(ModCenter.BlockCoalCoke));
	}
	
	private int update;
	private int slotMoveCooldown;
	private final FluidTank tank = new FluidTank(ModCenter.FluidOilCreosote, 0, 16000);



	public TileEntityCokeOven()
	{
		super(new ItemStack[4], "cokeOven");
	}


	@Override
	public boolean canDrain(ForgeDirection from, Fluid fluid)
	{
		return (tank.getFluid() != null) && tank.getFluid().isFluidEqual(new FluidStack(fluid, 0));
	}

	@Override
	public boolean canExtractItem(int slot, ItemStack itemStack, int side)
	{
		return (slot == 1 || slot == 3);
	}

	@Override
	public boolean canFill(ForgeDirection from, Fluid fluid)
	{
		return false;
	}

	@Override
	protected boolean canFinish()
	{
		return data[2] >= getBurnTime(slots[0]); //Wood 14 / Coal 60 / Coal block 540
	}

	@Override
	protected boolean canProcess()
	{
		int[] i = (slots[0] != null ? itemsFluid.get(slots[0].getItem()) : null);
		if (i != null)
		{
			if (tank.getCapacity() - tank.getFluidAmount() > i[0])
			{
				if (slots[1] == null)
				{
					return true;
				}
				
				ItemStack resultStack = itemsResult.get(slots[0].getItem());
				if (resultStack == null)
				{
					return false;
				}
				
				return (slots[1].getItem() == resultStack.getItem() && slots[1].stackSize < getInventoryStackLimit() && slots[1].stackSize < slots[1].getMaxStackSize());
			}
		}

		return false;
	}

	@Override
	public FluidStack drain(ForgeDirection from, FluidStack resource, boolean doDrain)
	{
		if (tank.getFluid() != null && tank.getFluid().isFluidEqual(resource))
		{
			FluidStack fluidStack = tank.drain(resource.amount, doDrain);
			if (fluidStack != null && fluidStack.amount > 0)
			{
				markForUpdate();
				return fluidStack;
			}
		}
		return null;
	}

	@Override
	public FluidStack drain(ForgeDirection from, int maxDrain, boolean doDrain)
	{
		FluidStack fluidStack = tank.drain(maxDrain, doDrain);
		if (fluidStack != null && fluidStack.amount > 0)
		{
			markForUpdate();
			return fluidStack;
		}
		return null;
	}

	@Override
	public int fill(ForgeDirection from, FluidStack resource, boolean doFill)
	{
		return 0;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int side)
	{
		return slotsAccept;
	}

	public FluidTank getTank()
	{
		return tank;
	}

	@Override
	public FluidTankInfo[] getTankInfo(ForgeDirection from)
	{
		return new FluidTankInfo[] { tank.getInfo() };
	}

	@Override
	public void invalidate()
	{
		super.invalidate();
		worldObj.func_147453_f(xCoord, yCoord, zCoord, ModCenter.BlockCokeOven);
	}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack itemStack)
	{
		if (!super.isItemValidForSlot(slot, itemStack))
		{
			return false;
		}

		switch (slot)
		{
			case 0:
				return itemsFluid.containsKey(itemStack.getItem()) || itemsResult.containsKey(itemStack.getItem());

			case 2:
				return FluidContainerRegistry.isEmptyContainer(itemStack);

			default:
				return false;
		}
	}

	@Override
	protected void onRefuel()
	{
		data[1] = data[0] = getBurnTime(slots[0]);  //Wood 14 / Coal 60 / Coal block 540
	}

	@Override
	protected void onSmelt()
	{
		ItemStack resultStack = itemsResult.get(slots[0].getItem());
		if (resultStack != null)
		{
			int[] i = itemsFluid.get(slots[0].getItem());

			if (slots[1] == null)
			{
				setInventorySlotContents(1, resultStack.copy());
			}
			else
			{
				++slots[1].stackSize;
			}

			tank.fill(new FluidStack(tank.getFluid().getFluid(), ZnDMathHelper.ranInt(i[0], i[1])), true);
			decrStackSize(0, 1);
		}
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		tank.readFromNBT(nbt.getCompoundTag("tank"));
	}

	@Override
	public void updateEntity()
	{
		boolean flag0 = data[0] > 0;
		boolean flag1 = false;

		if (flag0)
		{
			--data[0];
		}

		if (!worldObj.isRemote)
		{
			if (slotMoveCooldown > 0)
			{
				--slotMoveCooldown;
			}
			else
			{
				ItemStack itemStack = FluidContainerRegistry.fillFluidContainer(tank.drain(FluidContainerRegistry.BUCKET_VOLUME, false), slots[2]);

				if (tank.getFluidAmount() >= FluidContainerRegistry.BUCKET_VOLUME && itemStack != null)
				{
					if (slots[3] == null
							|| (itemStack.getItem().equals(slots[3].getItem()) && itemStack.getItemDamage() == slots[3].getItemDamage() && slots[3].stackSize < slots[3].getMaxStackSize()))
					{
						itemStack = FluidContainerRegistry.fillFluidContainer(tank.drain(FluidContainerRegistry.BUCKET_VOLUME, true), slots[2]);

						if (itemStack != null)
						{
							decrStackSize(2, 1);

							if (slots[3] == null)
							{
								setInventorySlotContents(3, itemStack);
							}
							else
							{
								++slots[3].stackSize;
							}
						}
					}
				}

				slotMoveCooldown = 10;
			}

			if (update > 0)
			{
				--update;
			}
			else if (update <= 0)
			{
				Block block = getBlockType();

				if (block == ModCenter.BlockCokeOven)
				{
					ModCenter.BlockCokeOven.checkStructureAndRefresh(worldObj, xCoord, yCoord, zCoord, true);
				}

				update = ModCenter.cfg.blocks_items.multiBlockUpdate;
			}

			if (slots[0] != null)
			{
				if (data[0] == 0 && canProcess())
				{
					onRefuel();
					flag1 = true;
				}

				if (data[0] > 0 && canProcess())
				{
					++data[2];

					if (canFinish())
					{
						onSmelt();
						data[2] = 0;
						flag1 = true;
					}
				}
				else
				{
					data[0] = data[1] = data[2] = 0;
				}
			}
			else
			{
				data[0] = data[1] = data[2] = 0;
			}

			if (flag0 != data[0] > 0)
			{
				flag1 = true;
			}

			if (data[0] == 0)
			{
				data[2] = 0;
				flag1 = true;
			}
		}

		if (flag1)
		{
			markForUpdate();
		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setTag("tank", tank.writeToNBT(new NBTTagCompound()));
	}
}
