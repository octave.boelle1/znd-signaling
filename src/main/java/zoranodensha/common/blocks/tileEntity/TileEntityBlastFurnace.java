package zoranodensha.common.blocks.tileEntity;

import java.util.HashMap;
import java.util.Map;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraftforge.fluids.FluidContainerRegistry;
import zoranodensha.common.blocks.BlockBlastFurnace;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.util.OreDictionaryHelper;



public class TileEntityBlastFurnace extends TileEntityContainerInventory
{
	private static final int[] slotsAccept = new int[] { 0, 1, 2 };
	private static final Map<Item, Item> items = new HashMap<Item, Item>();
	// 0 = Item Input; 1 = Fuel Input; 2 = Item Output
	static
	{
		for (Item item : OreDictionaryHelper.getIndicesInOreDictionary("nuggetIron"))
		{
			items.put(item, ModCenter.ItemNuggetSteel);
		}

		for (Item item : OreDictionaryHelper.getIndicesInOreDictionary("ingotIron"))
		{
			items.put(item, ModCenter.ItemIngotSteel);
		}

		for (Item item : OreDictionaryHelper.getIndicesInOreDictionary("dustIron"))
		{
			items.put(item, ModCenter.ItemIngotSteel);
		}

		for (Item item : OreDictionaryHelper.getIndicesInOreDictionary("blockIron"))
		{
			items.put(item, Item.getItemFromBlock(ModCenter.BlockSteel));
		}
	}


	private int update;



	public TileEntityBlastFurnace()
	{
		super(new ItemStack[3], "blastFurnace");
	}


	@Override
	public boolean canExtractItem(int slot, ItemStack itemStack, int side)
	{
		return (slot == 2);
	}

	@Override
	protected boolean canFinish()
	{
		return (data[2] >= getProcessTime(slots[0]));
	}

	@Override
	protected boolean canProcess()
	{
		boolean flag = slots[0] != null;

		if (slots[2] != null)
		{
			if (flag)
			{
				return slots[2].getItem() == items.get(slots[0].getItem()) && slots[2].stackSize < slots[2].getMaxStackSize();
			}
		}
		else
		{
			return flag;
		}

		return false;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int side)
	{
		return slotsAccept;
	}

	@SideOnly(Side.CLIENT)
	public int getProcessScaled()
	{
		int i = slots[0] != null ? getProcessTime(slots[0]) : 0;

		return (i > 0 ? (MathHelper.clamp_int(data[2], 0, i) * 24 / i) : 0);
	}

	public int getProcessTime(ItemStack itemStack)
	{
		if (itemStack != null)
		{
			Item result = items.get(itemStack.getItem());
			if (result == ModCenter.ItemNuggetSteel)
			{
				return 40;
			}
			else if (result == ModCenter.ItemIngotSteel)
			{
				return 360;
			}
			else if (result == Item.getItemFromBlock(ModCenter.BlockSteel))
			{
				return 3240;
			}
			
		}
		return 0;
	}

	@Override
	public void invalidate()
	{
		super.invalidate();

		worldObj.func_147453_f(xCoord, yCoord, zCoord, ModCenter.BlockBlastFurnace);
	}

	@Override
	public boolean isItemValidForSlot(int slot, ItemStack itemStack)
	{
		if (!super.isItemValidForSlot(slot, itemStack))
		{
			return false;
		}

		switch (slot)
		{
			case 0:
				return items.containsKey(itemStack.getItem());

			case 1:
				return (getBurnTime(itemStack) >= 1000) && !FluidContainerRegistry.isContainer(itemStack);

			default:
				return false;
		}
	}

	@Override
	protected void onSmelt()
	{
		if (slots[2] != null)
		{
			++slots[2].stackSize;
		}
		else
		{
			setInventorySlotContents(2, new ItemStack(items.get(slots[0].getItem())));
		}

		decrStackSize(0, 1);
	}

	@Override
	public void updateEntity()
	{
		if (!worldObj.isRemote)
		{
			if (update > 0)
			{
				--update;
			}
			else if (update <= 0)
			{
				Block block = getBlockType();

				if (block == ModCenter.BlockBlastFurnace)
				{
					((BlockBlastFurnace)block).checkStructureAndRefresh(worldObj, xCoord, yCoord, zCoord, true);
				}

				update = ModCenter.cfg.blocks_items.multiBlockUpdate;
			}
		}

		super.updateEntity();
	}
}
