package zoranodensha.common.blocks.tileEntity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.annotation.Nullable;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityMinecart;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import org.lwjgl.Sys;
import zoranodensha.api.structures.signals.*;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.ERailwayState;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.structures.tracks.Path;
import zoranodensha.api.util.Vec3i;
import zoranodensha.api.vehicles.util.TrackObj;
import zoranodensha.client.render.ICachedVertexState;
import zoranodensha.common.blocks.BlockTrackBase;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBaseNull.RailwaySectionNull;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.registry.ModTrackRegistry;
import zoranodensha.common.util.TrackHelpers;



public class TileEntityTrackBase extends TileEntity implements ITrackBase, ICachedVertexState, IPulseEmitter
{
	/** The currently set path. Must be at least 0 or above. */
	protected int currentPath;
	/** The length of this section. */
	protected int lengthOfSection;
	/** The orientation this track faces. Used as replacement of Minecraft's Metadata flag. */
	protected int orientation;
	/**
	 * XXX
	 */
	protected Vec3i interlockSignalLocation = null;
	protected long interlockSignalTime = 0L;

	/** An array containing all custom fields. Definition and usage are up to the extending classes. */
	protected Map<Integer, Object> fields = new HashMap<Integer, Object>();
	/** The default Path of this ITrackBase. */
	protected Path defaultPath;
	/** The EDirection instance this section faces. */
	protected EDirection directionOfSection = EDirection.NONE;
	/** The name of this section. Stored for NBT only. Initialised to {@code <Null>} by default in order to avoid a potential crash. */
	protected String nameOfSection = "<NULL>";
	/** The state of the track section. See {@link zoranodensha.api.structures.tracks.ITrackBase#getStateOfShape() getStateOfShape()} for detailed information. */
	protected ERailwayState stateOfShape = ERailwayState.BUILDINGGUIDE;
	/** The IRailwaySection instance of this ITrackBase. */
	protected IRailwaySection instance = new RailwaySectionNull();

	public TileEntityTrackBase()
	{
		ICachedVertexState.objectList.add(new WeakReference<ICachedVertexState>(this));
	}

	/**
	 * Initialize a new (source) track.
	 *
	 * @param orientation - The orientation of this track [0 - 3]
	 * @param state - The state of this track
	 * @param direction - The EDirection instance of this track
	 * @param section - The RailwaySection instance of this track
	 */
	public TileEntityTrackBase(int orientation, ERailwayState state, EDirection direction, IRailwaySection section)
	{
		this.orientation = orientation % 4;
		stateOfShape = state;
		directionOfSection = direction;
		instance = section;
		nameOfSection = section.getName();
		defaultPath = section.getPath(0);
		setPath(0);
		lengthOfSection = section.getLength();
		fields = section.initializeFields();
	}

	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);

		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, nbt);
	}

	@Override
	public EDirection getDirectionOfSection()
	{
		return directionOfSection;
	}

	@Override
	public <T extends Object> T getField(int index, T defaultVal, Class<T> type)
	{
		Object obj = fields.get(index);

		if (type.isInstance(obj))
		{
			return type.cast(obj);
		}

		return defaultVal;
	}

	@Override
	public Object getFieldUnchecked(int index)
	{
		return fields.get(index);
	}

	@Override
	public IRailwaySection getInstanceOfShape()
	{
		return instance;
	}

	@Override
	public int getLengthOfSection()
	{
		return lengthOfSection;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public double getMaxRenderDistanceSquared()
	{
		return (ModCenter.cfg.rendering.detailTracks > 0 ? 16384.0D : super.getMaxRenderDistanceSquared());
	}

	@Override
	public int getOrientation()
	{
		return orientation;
	}

	@Override
	public int getPath()
	{
		return currentPath;
	}

	public static double getLocalX(int rotation, double vehX, double vehZ, double tileX, double tileZ, double trackLength)
	{
		double localX = 0.0D;
		switch (rotation % 4)
		{
			case 0:
				localX = vehX - tileX;
				break;

			case 1:
				localX = vehZ - tileZ;
				break;

			case 2:
				localX = tileX - vehX + 1.0D;
				break;

			case 3:
				localX = tileZ - vehZ + 1.0D;
				break;
		}
		return (localX < -1) ? -1 : localX;
	}

	@Override
	public double[][] getPositionOnTrack(@Nullable Object vehicle, double vehX, double vehY, double vehZ, float defYaw, float defPitch)
	{
		if (!stateOfShape.isFinished())
		{
			return new double[][] { { vehX, vehY, vehZ }, { defPitch, defYaw, 0.0D } };
		}

		int length = getLengthOfSection();
		int rotation = getOrientation();
		double localX = 0.0D;
		double localZ = 0.0D;
		boolean flag = getDirectionOfSection().toInt() == 1;

		switch (rotation % 4)
		{
			case 0:
				localZ = vehZ - zCoord - 1.0D;
				break;

			case 1:
				localZ = vehX - xCoord;
				break;

			case 2:
				localZ = vehZ - zCoord;
				break;

			case 3:
				localZ = vehX - xCoord - 1.0D;
				break;
		}

		if (localZ < 0.0D)
		{
			localZ = -localZ;
		}

		if (flag)
		{
			rotation += 2;
		}

		localX = getLocalX(rotation, vehX, vehZ, xCoord, zCoord, length);
		double[][] ret = getInstanceOfShape().getPositionOnTrack(this, vehX, vehY, vehZ, vehicle, this, localX, localZ);

		if (ret != null)
		{
			if (ret.length == 2 && ret[0].length == 3 && ret[1].length == 3)
			{
				return ret;
			}
		}

		ret = new double[2][3];

		if (instance != null)
		{
			Path path = instance.getPath(currentPath);
			ret[0] = path.calculatePosition(localX);
			ret[1] = path.calculateRotation(localX);
		}
		else
		{
			ret[0] = defaultPath.calculatePosition(localX);
			ret[1] = defaultPath.calculateRotation(localX);
		}

		if (flag)
		{
			rotation += 2;
		}

		for (int i = 0; i < 3; ++i)
		{
			switch (i)
			{
				case 0:
					ret[1][i] *= (flag ? -45.0D : 45.0D);
					ret[1][i] += (rotation % 4) * 90.0D;
					break;

				case 1:
					ret[1][i] *= (rotation % 2 != 0 ? -1 : 1);
					break;

				default:
					break;
			}

			ret[1][i] %= 360.0D;
		}

		double[] pos = new double[] { 0.0D, ret[0][1] + yCoord, 0.0D };

		switch (rotation % 4)
		{
			case 0:
				pos[0] = vehX + ret[0][2];
				pos[2] = zCoord - ret[0][0] + 1.0D;
				break;

			case 1:
				pos[0] = xCoord + ret[0][0];
				pos[2] = vehZ + ret[0][2];
				break;

			case 2:
				pos[0] = vehX + ret[0][2];
				pos[2] = zCoord + ret[0][0];
				break;

			case 3:
				pos[0] = xCoord - ret[0][0] + 1.0D;
				pos[2] = vehZ + ret[0][2];
				break;
		}

		ret[0] = pos;

		return ret;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		double d0 = getLengthOfSection();

		if (instance != null)
		{
			return instance.getRenderBoundingBox(xCoord, yCoord, zCoord);
		}

		return AxisAlignedBB.getBoundingBox(xCoord - d0, yCoord, zCoord - d0, xCoord + d0 + 1.0D, yCoord, zCoord + d0 + 1.0D);
	}

	@Override
	public ERailwayState getStateOfShape()
	{
		return stateOfShape;
	}

	@Override
	public int getX()
	{
		return xCoord;
	}

	@Override
	public int getY()
	{
		return yCoord;
	}

	@Override
	public int getZ()
	{
		return zCoord;
	}

	@Override
	public Vec3 getEmitterPosition() {
		return Vec3.createVectorHelper(xCoord, yCoord, zCoord);
	}

	@Override
	public World getWorld() {
		return worldObj;
	}

	@Override
	public Object getEmitterEntity() {
		return this;
	}

	public void interlock(World world, ISignal signal)
	{
		if (signal != null)
		{
			interlockSignalLocation = new Vec3i(signal.getX(), signal.getY(), signal.getZ());
			interlockSignalTime = world.getTotalWorldTime();
		}
		else
		{
			interlockSignalLocation = null;
		}

		markForUpdate();
	}

	public boolean isInterlocked(World world)
	{
		if (interlockSignalLocation != null)
		{
			/*
			 * Check if the time this track was last interlocked was no longer than 10 seconds ago.
			 */
			if (world.getTotalWorldTime() - interlockSignalTime < 200)
			{
				return true;
			}
		}

		return false;
	}

	public boolean isInterlockedBy(World world, ISignal signal)
	{
		if (signal != null && interlockSignalLocation != null)
		{
			if (world.getTotalWorldTime() - interlockSignalTime < 200)
			{
				if (signal.getX() == interlockSignalLocation.x && signal.getY() == interlockSignalLocation.y && signal.getZ() == interlockSignalLocation.z)
				{
					return true;
				}
			}
		}

		return false;
	}

	public void markForUpdate()
	{
		markDirty();
		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);

		/* Mark track for render update. */
		setField(10, null);
	}

	@Override
	public void onDataPacket(NetworkManager networkManager, S35PacketUpdateTileEntity packet)
	{
		readFromNBT(packet.func_148857_g());
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void onVertexStateReset()
	{
		/* Mark track for render update. */
		setField(10, null);
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		orientation = nbt.getInteger("SectionRotation");
		lengthOfSection = nbt.getInteger("Length");
		currentPath = nbt.getInteger("CurrentPath");
		stateOfShape = ERailwayState.toState(nbt.getInteger("StateOfShape"));
		nameOfSection = nbt.getString("Instance");
		instance = ModTrackRegistry.getSection(nameOfSection);
		directionOfSection = EDirection.fromString(nbt.getString("Direction"));
		defaultPath = Path.readFromNBT(nbt);

		/*
		 * XXX
		 */
		if (nbt.hasKey("InterlockX"))
		{
			interlockSignalLocation = new Vec3i(nbt.getInteger("InterlockX"), nbt.getInteger("InterlockY"), nbt.getInteger("InterlockZ"));
		}
		else
		{
			interlockSignalLocation = null;
		}
		interlockSignalTime = nbt.getLong("InterlockTime");

		if (instance != null)
		{
			instance.onReadFromNBT(nbt, this);

			if (!instance.getValidPaths().contains(currentPath))
			{
				currentPath = 0;
			}

			defaultPath = instance.getPath(0);
		}

		/* Mark track for render update. */
		setField(10, null);
	}

	@Override
	public boolean setField(int index, Object param)
	{
		boolean ret = (fields.put(index, param) == null);
		if (index != 10)
		{
			/* Mark track for render update. */
			setField(10, null);
		}
		return ret;
	}

	@Override
	public void setPath(int path)
	{
		if (instance != null && instance.getValidPaths().contains(path))
		{
			currentPath = path;
		}
	}

	@Override
	public void setStateOfShape(ERailwayState state)
	{
		stateOfShape = state;
		markForUpdate();
	}

	@Override
	public void updateEntity()
	{
		if (instance != null)
		{
			instance.onUpdate(worldObj, xCoord, yCoord, zCoord, this, fields);

			for (PulseNotification notification : notifications) {

				if (pulses.containsKey(notification.getPulseID())) {
					getPulse(notification.getPulseID()).tickPulse(notification.getMovement(), notification.getBlocks());
				}

				notifications.remove(notification);
			}

		}
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		if (stateOfShape == null || directionOfSection == null)
		{
			return;
		}

		super.writeToNBT(nbt);

		nbt.setInteger("SectionRotation", orientation);
		nbt.setInteger("StateOfShape", stateOfShape.toInt());
		nbt.setInteger("Length", lengthOfSection);
		nbt.setInteger("CurrentPath", currentPath);
		nbt.setString("Instance", nameOfSection);
		nbt.setString("Direction", directionOfSection.toString());

		/*
		 * XXX
		 */
		if (interlockSignalLocation != null)
		{
			nbt.setInteger("InterlockX", interlockSignalLocation.x);
			nbt.setInteger("InterlockY", interlockSignalLocation.y);
			nbt.setInteger("InterlockZ", interlockSignalLocation.z);
		}
		nbt.setLong("InterlockTime", interlockSignalTime);

		/*
		 * Null-check to prevent a crash on server side.
		 */
		if (defaultPath != null)
		{
			defaultPath.writeToNBT(nbt);
		}

		if (instance != null)
		{
			instance.onWriteToNBT(nbt, this);
		}
	}

	/**
	 * Returns true if the given EntityMinecart is on an ITrackBase track.
	 */
	public static boolean getIsOnTrackBase(EntityMinecart minecart)
	{
		return getTrackBase(minecart) != null;
	}

	/**
	 * Helper method to get a relevant track below the given Minecart.
	 */
	public static TileEntity getTrackBase(EntityMinecart cart)
	{
		int x = MathHelper.floor_double(cart.posX);
		int y = MathHelper.floor_double(cart.posY);
		int z = MathHelper.floor_double(cart.posZ);
		TrackObj track;

		for (int i = 0; i <= 1; ++i)
		{
			track = TrackObj.isTrack(cart.worldObj, x, y - i, z, null);
			if (track != null && track.block instanceof BlockTrackBase)
			{
				TileEntity tile = track.world.getTileEntity(track.x, track.y, track.z);
				if (tile instanceof ITrackBase)
				{
					return tile;
				}
			}
		}

		return null;
	}

	/**
	 * Called by EntityMinecart on server worlds to apply a new position and rotation using ZnD's tracks.
	 * This relies on the EntityMinecart to be neither null nor to have a null World object (which in native code is the case).
	 *
	 * @return True if this vehicle is on an ITrackBase track and therefore should not compute other path finding mechanisms. Only returns true on server worlds.
	 */
	public static boolean setPositionOnTrackWithCheck(EntityMinecart minecart)
	{
		if (!minecart.worldObj.isRemote)
		{
			TileEntity track = getTrackBase(minecart);
			if (track != null)
			{
				TrackHelpers.computeMinecartMovement(track.xCoord, track.yCoord, track.zCoord, Math.min(0.4F, minecart.getCurrentCartSpeedCapOnRail()), minecart, (ITrackBase)track);
				return true;
			}
		}
		return false;
	}


	HashMap<UUID, APulse> pulses = new HashMap<>();
	CopyOnWriteArrayList<PulseNotification> notifications = new CopyOnWriteArrayList<>();

	public void notifyPulse(PulseNotification notification) {
		notifications.add(notification);
	}

	public APulse getPulse(UUID pulseID) {
		return pulses.get(pulseID);
	}

	public void addPulse(APulse pulse) {
		pulses.put(pulse.getID(), pulse);
	}

	public HashMap<UUID, APulse> getPulses() {
		return pulses;
	}

	public void clear() {
		for (APulse pulse : pulses.values()) {
			pulse.clear();
		}
	}

	public void removePulse(UUID id) {
		pulses.remove(id);
	}
}
