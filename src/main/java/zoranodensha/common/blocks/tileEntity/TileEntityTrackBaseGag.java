package zoranodensha.common.blocks.tileEntity;

import javax.annotation.Nullable;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.ERailwayState;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.api.structures.tracks.ITrackBase;



public class TileEntityTrackBaseGag extends TileEntity implements ITrackBase
{
	/** The coordinates of the TileEntity of the source track. If this block is the source track, this is {@code null}. */
	private int[] sourceCoords;

	/** Source tile of this track; cached for better performance. May be {@code null}. */
	private TileEntityTrackBase sourceTile;



	public TileEntityTrackBaseGag()
	{
	}

	public TileEntityTrackBaseGag(int x, int y, int z)
	{
		sourceCoords = new int[] { x, y, z };
	}

	@Override
	public boolean canUpdate()
	{
		return false;
	}

	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);

		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, nbt);
	}

	@Override
	public EDirection getDirectionOfSection()
	{
		return getSourceTile().getDirectionOfSection();
	}

	@Override
	public <T extends Object> T getField(int index, T defaultVal, Class<T> type)
	{
		return getSourceTile().getField(index, defaultVal, type);
	}

	@Override
	public Object getFieldUnchecked(int index)
	{
		return getSourceTile().getFieldUnchecked(index);
	}

	@Override
	public IRailwaySection getInstanceOfShape()
	{
		ITrackBase source = getSourceTile();

		return (source != null ? source.getInstanceOfShape() : null);
	}

	@Override
	public int getLengthOfSection()
	{
		return getSourceTile().getLengthOfSection();
	}

	@Override
	public int getOrientation()
	{
		return getSourceTile().getOrientation();
	}

	@Override
	public int getPath()
	{
		return getSourceTile().getPath();
	}

	@Override
	public double[][] getPositionOnTrack(@Nullable Object vehicle, double x, double y, double z, float defYaw, float defPitch)
	{
		return getSourceTile().getPositionOnTrack(vehicle, x, y, z, defYaw, defPitch);
	}

	/**
	 * A getter to access the source coordinates of this gag block's track.
	 */
	public int[] getSourceCoords()
	{
		return sourceCoords;
	}

	public ITrackBase getSourceTile(World... worlds)
	{
		World world = worldObj;

		if (world == null)
		{
			if (worlds != null && worlds.length > 0)
			{
				world = worlds[0];
			}
		}

		if (sourceTile == null || sourceTile.isInvalid())
		{
			if (sourceCoords != null && world != null)
			{
				TileEntity tileEntity = world.getTileEntity(sourceCoords[0], sourceCoords[1], sourceCoords[2]);
				if (tileEntity instanceof TileEntityTrackBase)
				{
					sourceTile = (TileEntityTrackBase)tileEntity;
					return sourceTile;
				}
			}
			return TileEntityTrackBaseNull.trackNull;
		}
		return sourceTile;
	}

	@Override
	public ERailwayState getStateOfShape()
	{
		return getSourceTile().getStateOfShape();
	}

	@Override
	public int getX()
	{
		return xCoord;
	}

	@Override
	public int getY()
	{
		return yCoord;
	}

	@Override
	public int getZ()
	{
		return zCoord;
	}

	@Override
	public World getWorld() {
		return worldObj;
	}

	@Override
	public void onDataPacket(NetworkManager networkManager, S35PacketUpdateTileEntity packet)
	{
		readFromNBT(packet.func_148857_g());
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		/* Reset source tile reference to make sure we don't keep old data. */
		sourceTile = null;

		/* Legacy compatibility, keep to ensure proper conversion of old maps. */
		if (nbt.hasKey("SourceX"))
		{
			sourceCoords = new int[] { nbt.getInteger("SourceX"), nbt.getInteger("SourceY"), nbt.getInteger("SourceZ") };
		}

		/* New implementation, uses relative coordinates and allows WorldEdit-copying of tracks. */
		else if (nbt.hasKey("relativeX"))
		{
			sourceCoords = new int[] { nbt.getInteger("relativeX"), nbt.getInteger("relativeY"), nbt.getInteger("relativeZ") };
			sourceCoords[0] += xCoord;
			sourceCoords[1] += yCoord;
			sourceCoords[2] += zCoord;
		}

		/* If neither key exists, kill this TileEntity. */
		else
		{
			invalidate();
		}
	}

	@Override
	public boolean setField(int index, Object param)
	{
		return getSourceTile().setField(index, param);
	}

	@Override
	public void setPath(int path)
	{
		getSourceTile().setPath(path);
	}

	@Override
	public void setStateOfShape(ERailwayState state)
	{
		getSourceTile().setStateOfShape(state);
	}

	@Override
	public void updateEntity()
	{
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);

		if (sourceCoords != null)
		{
			nbt.setInteger("relativeX", sourceCoords[0] - xCoord);
			nbt.setInteger("relativeY", sourceCoords[1] - yCoord);
			nbt.setInteger("relativeZ", sourceCoords[2] - zCoord);
		}
	}
}
