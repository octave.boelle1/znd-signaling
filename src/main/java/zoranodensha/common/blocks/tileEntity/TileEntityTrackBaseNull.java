package zoranodensha.common.blocks.tileEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.World;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.ERailwayState;
import zoranodensha.api.structures.tracks.EShape;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.api.structures.tracks.ARailwaySectionRenderer;
import zoranodensha.api.structures.tracks.IRailwaySectionRenderer;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.structures.tracks.Path;



public class TileEntityTrackBaseNull extends TileEntity implements ITrackBase
{
	public static final IRailwaySection sectionNull = new RailwaySectionNull();
	public static final ITrackBase trackNull = new TileEntityTrackBaseNull();



	@Override
	public EDirection getDirectionOfSection()
	{
		return EDirection.NONE;
	}

	@Override
	public <T extends Object> T getField(int index, T defaultVal, Class<T> type)
	{
		return defaultVal;
	}

	@Override
	public Object getFieldUnchecked(int index)
	{
		return null;
	}

	@Override
	public IRailwaySection getInstanceOfShape()
	{
		return sectionNull;
	}

	@Override
	public int getLengthOfSection()
	{
		return 0;
	}

	@Override
	public int getOrientation()
	{
		return 0;
	}

	@Override
	public int getPath()
	{
		return 0;
	}

	@Override
	public double[][] getPositionOnTrack(@Nullable Object vehicle, double x, double y, double z, float defYaw, float defPitch)
	{
		return new double[2][3];
	}

	@Override
	public ERailwayState getStateOfShape()
	{
		return ERailwayState.BUILDINGGUIDE;
	}

	@Override
	public int getX()
	{
		return xCoord;
	}

	@Override
	public int getY()
	{
		return yCoord;
	}

	@Override
	public int getZ()
	{
		return zCoord;
	}

	@Override
	public World getWorld() {
		return worldObj;
	}

	@Override
	public boolean setField(int index, Object param)
	{
		return false;
	}

	@Override
	public void setPath(int path)
	{
	}

	@Override
	public void setStateOfShape(ERailwayState state)
	{
	}



	public static class RailwaySectionNull implements IRailwaySection
	{
		@Override
		public boolean getCanStay(World world, int x, int y, int z, int rotation, EDirection dir)
		{
			return false;
		}

		@Override
		public EShape getEShapeType()
		{
			return EShape.STRAIGHT;
		}

		@Override
		public List<int[]> getGagBlocks(EDirection dir)
		{
			return new ArrayList<int[]>();
		}

		@Override
		public int getLength()
		{
			return 0;
		}

		@Override
		public String getName()
		{
			return "<Null>";
		}

		@Override
		public Path getPath(int path)
		{
			return new Path(new String[] { "0", "0" }, null);
		}

		@Override
		public double[][] getPositionOnTrack(ITrackBase trackBase, double vehX, double vehY, double vehZ, @Nullable Object vehicle, TileEntity tileEntity, double localX, double localZ)
		{
			return null;
		}

		@Override
		@SideOnly(Side.CLIENT)
		public IRailwaySectionRenderer getRender()
		{
			return null;
		}

		@Override
		public AxisAlignedBB getRenderBoundingBox(double x, double y, double z)
		{
			return AxisAlignedBB.getBoundingBox(x - 2.0D, y, z - 2.0D, x + 1.0D, y + 1.0D, z + 1.0D);
		}

		@Override
		public List<Integer> getValidPaths()
		{
			return new ArrayList<Integer>();
		}

		@Override
		public Map<Integer, Object> initializeFields()
		{
			return new HashMap<Integer, Object>();
		}

		@Override
		public TileEntity initializeTrack(ITrackBase trackBase)
		{
			return (TileEntity)trackBase;
		}

		@Override
		public boolean isMaintenanceTrack()
		{
			return false;
		}

		@Override
		public void onReadFromNBT(NBTTagCompound nbt, ITrackBase trackBase)
		{
		}

		@Override
		public List<ItemStack> onTrackDestroyed(TileEntity tileEntity, EntityPlayer player)
		{
			return null;
		}

		@Override
		public boolean onTrackWorkedOn(ITrackBase trackBase, EntityPlayer player, boolean isRemote)
		{
			return false;
		}

		@Override
		public boolean onTrackWorkedOn(ITrackBase trackBase, IInventory inventory, boolean isRemote)
		{
			return false;
		}

		@Override
		public Map<Integer, Object> onUpdate(World world, int x, int y, int z, ITrackBase trackBase, Map<Integer, Object> params)
		{
			return params;
		}

		@Override
		public void onWriteToNBT(NBTTagCompound nbt, ITrackBase trackBase)
		{
		}
	}
}
