package zoranodensha.common.blocks.tileEntity;

import net.minecraft.util.AxisAlignedBB;
import org.apache.logging.log4j.Level;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import zoranodensha.api.structures.signals.ASignalRegistry;
import zoranodensha.api.structures.signals.ISignal;
import zoranodensha.common.core.ModCenter;



/**
 * <h1>TileEntitySignal</h1>
 * <p>
 * A tile entity class which contains an
 * {@link zoranodensha.api.structures.signals.ISignal} instance.
 * </p>
 * 
 * @author Jaffa
 */
public class TileEntitySignal extends TileEntity
{
	/**
	 * The signal instance within this tile entity.
	 */
	private ISignal signal;

	public int targetMagnetX;
	public int targetMagnetY;
	public int targetMagnetZ;



	public TileEntitySignal()
	{
		this.signal = null;
	}


	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound descriptionPacket = new NBTTagCompound();
		this.writeToNBT(descriptionPacket);
		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, descriptionPacket);
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox()
	{
		return AxisAlignedBB.getBoundingBox(xCoord, yCoord, zCoord, xCoord + 1.0D, yCoord + 4.0D, zCoord + 1.0D);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public double getMaxRenderDistanceSquared()
	{
		return 16384.0D;
	}


	/**
	 * @return - This tile entity's signal instance. May be {@code null}.
	 */
	public ISignal getSignal()
	{
		return signal;
	}


	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity packet)
	{
		this.readFromNBT(packet.func_148857_g());
	}


	@Override
	public void updateEntity()
	{
		super.updateEntity();

		/*
		 * If this instance's signal isn't null, go ahead and update it.
		 */
		if (signal != null)
		{
			signal.onUpdate();
		}
	}


	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		if (nbt.hasKey("name"))
		{
			ISignal freshSignal = ASignalRegistry.getSignalFromName(nbt.getString("name"));

			if (freshSignal != null)
			{
				this.setSignal(freshSignal);
				this.signal.readFromNBT(nbt);
			}
		}
	}


	/**
	 * <p>
	 * Sets this tile entity's signal instance.
	 * </p>
	 * 
	 * @param newSignal - The signal to set as this tile entity's signal instance.
	 * @return - This tile entity.
	 */
	public TileEntity setSignal(ISignal newSignal)
	{
		this.signal = newSignal;
		this.signal.onTileEntityChange(this);

		return this;
	}


	@Override
	public boolean shouldRenderInPass(int pass)
	{
		return pass == 0 || pass == 1;
	}


	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);

		/*
		 * If this tile entity has a signal instance, write it to the NBT, along with a
		 * positive flag under the key 'signal'.
		 */
		if (signal != null)
		{
			nbt.setString("name", signal.getName());

			signal.writeToNBT(nbt);
		}
	}
}
