package zoranodensha.common.blocks.tileEntity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;



public class TileEntityTrackDecoration extends TileEntity
{
	/** The size of the piles */
	protected int pileSize = 0;



	public TileEntityTrackDecoration()
	{
	}

	@Override
	public boolean canUpdate()
	{
		return false;
	}

	/**
	 * Changes the size of this pile. If there is a single object left and "add" is false,
	 * removes the entire block.
	 *
	 * @param flag - 0 = Add an object; 1 = Remove an object; 2 = Clear the pile.
	 * @return True if successful.
	 */
	public boolean changePileSize(int flag)
	{
		if (flag == 0 && (blockMetadata / 4 == 2 ? pileSize < 3 : pileSize < 7))
		{
			++pileSize;
		}
		else if (flag == 1 && pileSize >= 0)
		{
			--pileSize;
		}
		else if (flag == 2)
		{
			pileSize = -1;
		}
		else
		{
			return false;
		}

		if (pileSize < 0)
		{
			worldObj.setBlockToAir(xCoord, yCoord, zCoord);
		}

		markForUpdate();

		return true;
	}

	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound nbt = new NBTTagCompound();
		writeToNBT(nbt);

		return new S35PacketUpdateTileEntity(xCoord, yCoord, zCoord, 1, nbt);
	}

	public int getPileSize()
	{
		return pileSize;
	}

	public void markForUpdate()
	{
		markDirty();
		worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
	}

	@Override
	public void onDataPacket(NetworkManager networkManager, S35PacketUpdateTileEntity packet)
	{
		readFromNBT(packet.func_148857_g());
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);

		pileSize = nbt.getInteger("PileSize");
	}

	@Override
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);

		nbt.setInteger("PileSize", pileSize);
	}
}
