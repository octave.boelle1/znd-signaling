package zoranodensha.common.blocks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import zoranodensha.common.blocks.tileEntity.TileEntityItemFluidHandler;
import zoranodensha.common.blocks.tileEntity.TileEntityRefinery;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.handlers.EGui;
import zoranodensha.common.util.OreDictionaryHelper;



public class BlockRefinery extends BlockContainer
{
	public static final int HEIGHT = 7;
	public static final List<Block> blocksListSteel = new ArrayList<Block>();
	public static final List<Block> blocksListStone = new ArrayList<Block>();



	public BlockRefinery()
	{
		super(Material.iron);

		refreshLists();
		setBlockName(ModData.ID + ".blocks.refinery");
		setHardness(9.0F);
		setResistance(15.0F);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return (meta == HEIGHT ? new TileEntityRefinery() : new TileEntityItemFluidHandler());
	}

	@Override
	public Item getItemDropped(int meta, Random ran, int fortune)
	{
		return null;
	}

	@Override
	public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z, EntityPlayer player)
	{
		return new ItemStack(world.getBlockMetadata(x, y, z) > 0 ? ModCenter.BlockSteel : Blocks.stone);
	}

	@Override
	public int getRenderType()
	{
		return -1;
	}

	public TileEntityRefinery getTileCoordinates(World world, int x, int y, int z, int meta)
	{
		if (meta > HEIGHT)
		{
			meta %= HEIGHT;
		}

		int flag = 80;

		while (meta != HEIGHT && flag > 0)
		{
			if (world.getBlock(x, y, z) != ModCenter.BlockRefinery && world.getBlock(x, y + 1, z) != ModCenter.BlockRefinery)
			{
				break;
			}

			meta = world.getBlockMetadata(x, y, z);

			if (meta < HEIGHT - 1)
			{
				++y;
			}
			else if (meta < HEIGHT)
			{
				for (int i = -1; i < 2; ++i)
				{
					for (int j = -1; j < 2; ++j)
					{
						if (world.getBlockMetadata(x + i, y, z + j) == HEIGHT)
						{
							x += i;
							z += j;
						}
					}
				}
			}

			--flag;
		}

		if (meta == HEIGHT)
		{
			TileEntity tileEntity = world.getTileEntity(x, y, z);

			if (tileEntity instanceof TileEntityRefinery)
			{
				return ((TileEntityRefinery)tileEntity);
			}
		}

		return null;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float xf, float yf, float zf)
	{
		TileEntityRefinery tileEntity = getTileCoordinates(world, x, y, z, world.getBlockMetadata(x, y, z));

		if (tileEntity != null)
		{
			player.openGui(ModCenter.instance, EGui.REFINERY.ordinal(), world, tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord);
			return true;
		}
		else if (!world.isRemote)
		{
			int meta = world.getBlockMetadata(x, y, z);

			if (meta == 0)
			{
				world.setBlock(x, y, z, Blocks.stone);
			}
			else if (meta < HEIGHT - 1)
			{
				world.setBlock(x, y, z, ModCenter.BlockSteel);
			}
			else
			{
				world.setBlockToAir(x, y, z);
			}
		}

		return false;
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		if (!world.isRemote && block == ModCenter.BlockRefinery)
		{
			TileEntityRefinery tileEntity = getTileCoordinates(world, x, y, z, world.getBlockMetadata(x, y, z));

			if (tileEntity != null)
			{
				removeStructure(world, tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord, true);
			}
			else
			{
				int meta = world.getBlockMetadata(x, y, z);

				if (meta == 0)
				{
					world.setBlock(x, y, z, Blocks.stone);
				}
				else if (meta < HEIGHT - 1)
				{
					world.setBlock(x, y, z, ModCenter.BlockSteel);
				}
				else
				{
					world.setBlockToAir(x, y, z);
				}
			}
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister icon)
	{
		blockIcon = icon.registerIcon(ModData.ID + ":blockCollisionbox");
	}

	@Override
	public boolean removedByPlayer(World world, EntityPlayer player, int x, int y, int z, boolean willHarvest)
	{
		int meta = world.getBlockMetadata(x, y, z);

		return world.setBlock(x, y, z, (meta > 0 ? meta < HEIGHT - 1 ? ModCenter.BlockSteel : Blocks.air : Blocks.stone));
	}

	public boolean removeStructure(World world, int x, int y, int z, boolean checkFirst)
	{
		boolean flag;
		y -= (HEIGHT - 1);

		if (checkFirst)
		{
			int flag0 = 0;

			for (int j = 0; j < HEIGHT; ++j)
			{
				for (int i = -1; i < 2; ++i)
				{
					for (int k = -1; k < 2; ++k)
					{
						if (world.getBlock(x + i, y + j, z + k) == ModCenter.BlockRefinery)
						{
							++flag0;
						}
					}
				}
			}

			flag = (flag0 == HEIGHT * 9);
		}
		else
		{
			flag = false;
		}

		if (!flag && !world.isRemote)
		{
			for (int j = 0; j < HEIGHT; ++j)
			{
				for (int i = -1; i < 2; ++i)
				{
					for (int k = -1; k < 2; ++k)
					{
						if (world.getBlock(x + i, y + j, z + k) == ModCenter.BlockRefinery)
						{
							int meta = world.getBlockMetadata(x + i, y + j, z + k);

							if (meta == 0)
							{
								world.setBlock(x + i, y + j, z + k, Blocks.stone);
							}
							else if (meta < HEIGHT - 1)
							{
								world.setBlock(x + i, y + j, z + k, ModCenter.BlockSteel);
							}
							else
							{
								world.setBlockToAir(x + i, y + j, z + k);
							}
						}
					}
				}
			}
		}

		return flag;
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	public static boolean canPlaceAt(World world, int x, int y, int z, boolean doPlace)
	{
		int flag0 = 0;
		int flag1 = 0;
		int flag2 = 0;

		y -= HEIGHT;

		for (int j = 0; j < HEIGHT; ++j)
		{
			if (j == 0)
			{
				for (int i = -1; i < 2; ++i)
				{
					for (int k = -1; k < 2; ++k)
					{
						if (blocksListStone.contains(world.getBlock(x + i, y + j, z + k)))
						{
							++flag0;
						}
					}
				}
			}
			else if (j < HEIGHT - 1)
			{
				for (int i = -1; i < 2; ++i)
				{
					for (int k = -1; k < 2; ++k)
					{
						if (blocksListSteel.contains(world.getBlock(x + i, y + j, z + k)))
						{
							++flag1;
						}
					}
				}
			}
			else
			{
				for (int i = -1; i < 2; ++i)
				{
					for (int k = -1; k < 2; ++k)
					{
						if (world.isAirBlock(x + i, y + j, k + z))
						{
							++flag2;
						}
					}
				}
			}
		}

		if (flag0 == 9 && flag2 == 9 && (flag0 + flag1 + flag2) == HEIGHT * 9)
		{
			if (doPlace && !world.isRemote)
			{
				for (int j = 0; j < HEIGHT; ++j)
				{
					for (int i = -1; i < 2; ++i)
					{
						for (int k = -1; k < 2; ++k)
						{
							world.setBlock(x + i, y + j, z + k, ModCenter.BlockRefinery);

							if (j == (HEIGHT - 1) && i == 0 && k == 0)
							{
								world.setBlockMetadataWithNotify(x + i, y + j, z + k, HEIGHT, 2);
								world.setTileEntity(x + i, y + j, z + k, ModCenter.BlockRefinery.createNewTileEntity(world, HEIGHT));
								world.markBlockForUpdate(x + i, y + j, z + k);
							}
							else
							{
								world.setBlockMetadataWithNotify(x + i, y + j, z + k, j, 2);
								world.markBlockForUpdate(x + i, y + j, z + k);
							}
						}
					}
				}
			}

			return true;
		}

		return false;
	}

	public static void refreshLists()
	{
		blocksListSteel.clear();
		blocksListStone.clear();

		for (Item item : OreDictionaryHelper.getIndicesInOreDictionary("blockSteel"))
		{
			Block block = Block.getBlockFromItem(item);

			if (block != Blocks.air)
			{
				blocksListSteel.add(block);
			}
		}

		for (Item item : OreDictionaryHelper.getIndicesInOreDictionary("stone"))
		{
			Block block = Block.getBlockFromItem(item);

			if (block != Blocks.air)
			{
				blocksListStone.add(block);
			}
		}
	}
}
