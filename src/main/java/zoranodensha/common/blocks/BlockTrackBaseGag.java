package zoranodensha.common.blocks;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import zoranodensha.api.structures.tracks.ERailwayState;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBaseGag;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_1131_1131;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_1843_1843;
import zoranodensha.trackpack.common.section.ZnD_StraightBuffer_4500_4500;
import zoranodensha.trackpack.common.section.ZnD_StraightSlope_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_StraightSlope16_0000_0000;



public class BlockTrackBaseGag extends BlockTrackBase
{
	/**
	 * Initialize a track's gag block.
	 */
	public BlockTrackBaseGag()
	{
		setBlockName(ModData.ID + ".blocks.trackBaseGag");
	}


	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityTrackBaseGag();
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z)
	{
		TileEntity tile = world.getTileEntity(x, y, z);
		if (tile instanceof ITrackBase)
		{
			ITrackBase track = (ITrackBase)tile;

			/* Special case I: buffer stop track's gag block. This one has a collision box. */
			if (track.getInstanceOfShape() instanceof ZnD_StraightBuffer_0000_0000)
			{
				if (track.getField(2, false, Boolean.class))
				{
					switch (track.getOrientation())
					{
						case 0:
						case 2:
							return AxisAlignedBB.getBoundingBox(x + 0.3125D, y, z, x + 0.6875D, y + 1, z + 1);

						case 1:
						case 3:
							return AxisAlignedBB.getBoundingBox(x, y, z + 0.3125D, x + 1, y + 1, z + 0.6875D);

						default:
							return AxisAlignedBB.getBoundingBox(x, y, z, x + 1, y + 1, z + 1);
					}
				}
			}
			else if (track.getInstanceOfShape() instanceof ZnD_StraightBuffer_1131_1131
					|| track.getInstanceOfShape() instanceof ZnD_StraightBuffer_1843_1843)
			{
				if (track.getField(2, false, Boolean.class) && world.getBlockMetadata(x, y, z) > 0)
				{
					switch (track.getOrientation())
					{
						case 0:
						case 2:
							return AxisAlignedBB.getBoundingBox(x + 0.3125D, y, z, x + 0.6875D, y + 1, z + 1);

						case 1:
						case 3:
							return AxisAlignedBB.getBoundingBox(x, y, z + 0.3125D, x + 1, y + 1, z + 0.6875D);

						default:
							return AxisAlignedBB.getBoundingBox(x, y, z, x + 1, y + 1, z + 1);
					}
				}
			}
			else if (track.getInstanceOfShape() instanceof ZnD_StraightBuffer_4500_4500)
			{
				if (track.getField(2, false, Boolean.class) && world.getBlockMetadata(x, y, z) > 0)
				{
					switch (track.getOrientation())
					{
						case 0:
						case 1:
						case 2:
						case 3:
							return AxisAlignedBB.getBoundingBox(x + 0.3125D, y, z + 0.3125D, x + 0.6875D, y + 1, z + 0.6875D);

						default:
							return AxisAlignedBB.getBoundingBox(x, y, z, x + 1, y + 1, z + 1);
					}
				}
			}

			/* Special case IIa: 10m straight slope tracks have collision boxes. */
			else if (track.getInstanceOfShape() instanceof ZnD_StraightSlope_0000_0000)
			{
				int meta = world.getBlockMetadata(x, y, z);
				if (meta > 0)
				{
					return AxisAlignedBB.getBoundingBox(x, y, z, x + 1.0D, y + (meta * 0.1D), z + 1.0D);
				}
			}
			
			/* Special case IIb: 16m straight slope tracks have collision boxes. */
			else if (track.getInstanceOfShape() instanceof ZnD_StraightSlope16_0000_0000)
			{
				int meta = world.getBlockMetadata(x, y, z);
				if (meta > 0)
				{
					return AxisAlignedBB.getBoundingBox(x, y, z, x + 1.0D, y + (meta * 0.0625D), z + 1.0D);
				}
			}
		}
		return null;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float fx, float fy, float fz)
	{
		TileEntity tileEntity = world.getTileEntity(x, y, z);
		if (tileEntity instanceof TileEntityTrackBaseGag)
		{
			int[] coords = ((TileEntityTrackBaseGag)tileEntity).getSourceCoords();
			if (coords != null && coords.length == 3)
			{
				Block block = world.getBlock(coords[0], coords[1], coords[2]);

				if (block == ModCenter.BlockTrackBase)
				{
					return block.onBlockActivated(world, coords[0], coords[1], coords[2], player, side, fx, fy, fz);
				}
			}
		}

		if (!world.isRemote)
		{
			world.setBlockToAir(x, y, z);
		}

		return false;
	}

	@Override
	public void onBlockPreDestroy(World world, int x, int y, int z, int oldMeta)
	{
		if (!world.isRemote)
		{
			TileEntity tileEntity = world.getTileEntity(x, y, z);
			if (tileEntity instanceof TileEntityTrackBaseGag)
			{
				int[] coords = ((TileEntityTrackBaseGag)tileEntity).getSourceCoords();
				if (coords != null && coords.length == 3)
				{
					Block block = world.getBlock(coords[0], coords[1], coords[2]);

					if (block == ModCenter.BlockTrackBase)
					{
						world.scheduleBlockUpdate(coords[0], coords[1], coords[2], block, 1);
					}
				}
			}
		}
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		if (!world.isRemote)
		{
			TileEntity tileEntity = world.getTileEntity(x, y, z);
			if (tileEntity instanceof TileEntityTrackBaseGag)
			{
				int[] coords = ((TileEntityTrackBaseGag)tileEntity).getSourceCoords();
				if (coords != null && coords.length == 3)
				{
					if ((tileEntity = world.getTileEntity(coords[0], coords[1], coords[2])) instanceof ITrackBase)
					{
						ITrackBase trackBase = (ITrackBase)tileEntity;
						if (ERailwayState.BUILDINGGUIDE.equals(trackBase.getStateOfShape()))
						{
							return;
						}
						
						IRailwaySection section = trackBase.getInstanceOfShape();
						if (section == null || !section.getCanStay(world, coords[0], coords[1], coords[2], trackBase.getOrientation(), trackBase.getDirectionOfSection()))
						{
							world.setBlockToAir(x, y, z);
						}
					}
				}
				else
				{
					world.setBlockToAir(x, y, z);
				}
			}
		}
	}

	@Override
	public boolean removedByPlayer(World world, EntityPlayer player, int x, int y, int z, boolean willHarvest)
	{
		if (!world.isRemote)
		{
			TileEntity tileEntity = world.getTileEntity(x, y, z);
			if (tileEntity instanceof TileEntityTrackBaseGag)
			{
				int[] coords = ((TileEntityTrackBaseGag)tileEntity).getSourceCoords();
				if (coords != null && coords.length == 3)
				{
					Block block = world.getBlock(coords[0], coords[1], coords[2]);
					if (block == ModCenter.BlockTrackBase)
					{
						return block.removedByPlayer(world, player, coords[0], coords[1], coords[2], willHarvest);
					}
				}
			}

			return super.removedByPlayer(world, player, x, y, z, willHarvest);
		}

		return false;
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z)
	{
		TileEntity tile = world.getTileEntity(x, y, z);
		if (tile instanceof ITrackBase)
		{
			ITrackBase track = (ITrackBase)tile;
			if (track.getInstanceOfShape() instanceof ZnD_StraightSlope_0000_0000)
			{
				int meta = world.getBlockMetadata(x, y, z);
				if (meta > 0)
				{
					setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, Math.min((meta * 0.1F) + 0.125F, 1.0F), 1.0F);
					return;
				}
			}
		}
		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
	}
}
