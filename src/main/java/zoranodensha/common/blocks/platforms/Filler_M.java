package zoranodensha.common.blocks.platforms;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.common.blocks.tileEntity.TileEntityPlatform;
import zoranodensha.common.blocks.tileEntity.TileEntityPlatform.APlatformType;
import zoranodensha.common.core.ModCenter;



public class Filler_M extends APlatformType
{
	public Filler_M()
	{
		super();
		
		for (APlatformType type : APlatformType.types)
		{
			if (type != this)
			{
				GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModCenter.BlockPlatform, 1, this.getID()), "   ", " S ", "   ", 'S', new ItemStack(ModCenter.BlockPlatform, 1, type.getID())));
			}
		}
	}
	
	@Override
	public float getHeight()
	{
		return 0.5F;
	}
	
	@Override
	@SideOnly(Side.CLIENT)
	public void render(IIcon icon, float offsetNegZ, float offsetPosZ, TileEntityPlatform tilePlatform)
	{
		if (tilePlatform == null)
		{
			Tessellator.instance.addTranslation(-0.125F, 0.25F, 0.0F);
		}
		
		super.render(icon, offsetNegZ, offsetPosZ, tilePlatform);
	}

	@Override
	@SideOnly(Side.CLIENT)
	protected void renderNegX(Tessellator tessellator, IIcon icon, float negZ, float posZ)
	{
		double[] v1, v2, v3, v4;

		v1 = new double[] { 0.0, 0.0, 0.0 };
		v2 = new double[] { 0.0, 0.0, 1.0 };
		v3 = new double[] { 0.0, getHeight(), 1.0 };
		v4 = new double[] { 0.0, getHeight(), 0.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, -1, 0, 0);
	}

	@Override
	@SideOnly(Side.CLIENT)
	protected void renderNegY(Tessellator tessellator, IIcon icon, float negZ, float posZ)
	{
		double[] v1, v2, v3, v4;

		v1 = new double[] { 0.0, 0.0, 1.0 };
		v2 = new double[] { 0.0, 0.0, 0.0 };
		v3 = new double[] { 1.0, 0.0, 0.0 };
		v4 = new double[] { 1.0, 0.0, 1.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 0, -1, 0);
	}

	@Override
	@SideOnly(Side.CLIENT)
	protected void renderNegZ(Tessellator tessellator, IIcon icon, float negZ, float posZ)
	{
		double[] v1, v2, v3, v4;

		v1 = new double[] { 1.0, 0.0, 0.0 };
		v2 = new double[] { 0.0, 0.0, 0.0 };
		v3 = new double[] { 0.0, getHeight(), 0.0 };
		v4 = new double[] { 1.0, getHeight(), 0.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 0, 0, -1);
	}

	@Override
	@SideOnly(Side.CLIENT)
	protected void renderPosX(Tessellator tessellator, IIcon icon, float negZ, float posZ)
	{
		double[] v1, v2, v3, v4;

		v1 = new double[] { 1.0, 0.0, 1.0 };
		v2 = new double[] { 1.0, 0.0, 0.0 };
		v3 = new double[] { 1.0, getHeight(), 0.0 };
		v4 = new double[] { 1.0, getHeight(), 1.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 1, 0, 0);
	}

	@Override
	@SideOnly(Side.CLIENT)
	protected void renderPosY(Tessellator tessellator, IIcon icon, float negZ, float posZ)
	{
		double[] v1, v2, v3, v4;

		v1 = new double[] { 1.0, getHeight(), 1.0 };
		v2 = new double[] { 1.0, getHeight(), 0.0 };
		v3 = new double[] { 0.0, getHeight(), 0.0 };
		v4 = new double[] { 0.0, getHeight(), 1.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 0, 1, 0);
	}

	@Override
	@SideOnly(Side.CLIENT)
	protected void renderPosZ(Tessellator tessellator, IIcon icon, float negZ, float posZ)
	{
		double[] v1, v2, v3, v4;

		v1 = new double[] { 0.0, 0.0, 1.0 };
		v2 = new double[] { 1.0, 0.0, 1.0 };
		v3 = new double[] { 1.0, getHeight(), 1.0 };
		v4 = new double[] { 0.0, getHeight(), 1.0 };
		renderSplitFace(tessellator, icon, v1, v2, v3, v4, 0, 0, 1);
	}
	
	@Override
	public void updateBoundingBox(Block block, int meta)
	{
		block.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, getHeight(), 1.0F);
	}
}