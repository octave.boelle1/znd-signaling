package zoranodensha.common.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;



public class BlockSteel extends Block
{
	private static boolean registered = false;



	public BlockSteel()
	{
		super(Material.iron);

		setBlockName(ModData.ID + ".blocks.steel");
		setCreativeTab(ModCreativeTabs.generic);
		setHardness(7.0F);
		this.setHarvestLevel("pickaxe", 2);
		setResistance(12.0F);
	}


	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister iconReg)
	{
		blockIcon = iconReg.registerIcon(ModData.ID + ":blockSteel");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		OreDictionary.registerOre("blockSteel", this);
		GameRegistry.addRecipe(new ShapelessOreRecipe(	new ItemStack(this), "ingotSteel", "ingotSteel", "ingotSteel", "ingotSteel", "ingotSteel", "ingotSteel", "ingotSteel", "ingotSteel",
														"ingotSteel"));
	}
}
