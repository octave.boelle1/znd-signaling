package zoranodensha.common.blocks;

import java.util.Random;

import net.minecraft.world.World;
import net.minecraftforge.fluids.Fluid;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.fluids.EFluid;



public class BlockFluidTar extends BlockFluid
{
	public BlockFluidTar(Fluid fluid, EFluid type)
	{
		super(fluid, type);
	}

	@Override
	public void updateTick(World world, int x, int y, int z, Random ran)
	{
		super.updateTick(world, x, y, z, ran);

		if (ran.nextInt(6) == 0 && isSourceBlock(world, x, y, z))
		{
			world.setBlock(x, y, z, ModCenter.BlockTar, 7, 2);
		}
	}
}
