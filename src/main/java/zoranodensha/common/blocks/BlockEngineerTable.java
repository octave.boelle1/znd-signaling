package zoranodensha.common.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.oredict.ShapedOreRecipe;
import zoranodensha.api.vehicles.Train;
import zoranodensha.common.blocks.tileEntity.TileEntityEngineerTable;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.handlers.EGui;
import zoranodensha.common.util.ZnDMathHelper;



public class BlockEngineerTable extends BlockContainer
{
	private static boolean registered = false;



	public BlockEngineerTable()
	{
		super(Material.wood);

		setBlockName(ModData.ID + ".blocks.engineerTable");
		setCreativeTab(ModCreativeTabs.generic);
		setHardness(2.0F);
		setResistance(5.0F);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityEngineerTable();
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z)
	{
		setBlockBoundsBasedOnState(world, x, y, z);

		return AxisAlignedBB.getBoundingBox(x + minX, y + minY, z + minZ, x + maxX, y + maxY, z + maxZ);
	}

	@Override
	public int getRenderType()
	{
		return -1;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float xf, float yf, float zf)
	{
		ItemStack itemStack = player.getHeldItem();

		if (itemStack != null && ModCenter.ItemTrain.equals(itemStack.getItem()) && itemStack.hasTagCompound())
		{
			NBTTagCompound nbt = itemStack.getTagCompound();

			if (nbt.hasKey(Train.EDataKey.VEHICLE_DATA.key))
			{
				player.openGui(ModCenter.instance, EGui.ENGINEER_TABLE_TRAINS.ordinal(), world, x, y, z);
				return true;
			}
		}

		player.openGui(ModCenter.instance, EGui.ENGINEER_TABLE.ordinal(), world, x, y, z);
		return true;
	}

	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack itemStack)
	{
		world.setBlockMetadataWithNotify(x, y, z, ZnDMathHelper.getRotation(entity.rotationYawHead), 2);
	}


	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister icon)
	{
		blockIcon = icon.registerIcon(ModData.ID + ":blockCollisionbox");
	}


	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;

		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(this), "WWW", "III", "I I", 'W', "plankWood", 'I', "ingotIron"));
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z)
	{
		switch (world.getBlockMetadata(x, y, z))
		{
			default:
				setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
				break;

			case 0:
				setBlockBounds(0.03125F, 0.0F, 0.0F, 0.96875F, 0.725F, 0.625F);
				break;

			case 1:
				setBlockBounds(0.375F, 0.0F, 0.03125F, 1.0F, 0.725F, 0.96875F);
				break;

			case 2:
				setBlockBounds(0.03125F, 0.0F, 0.375F, 0.96875F, 0.725F, 1.0F);
				break;

			case 3:
				setBlockBounds(0.0F, 0.0F, 0.03125F, 0.625F, 0.725F, 0.96875F);
				break;
		}
	}
}
