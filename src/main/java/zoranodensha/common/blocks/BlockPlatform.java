package zoranodensha.common.blocks;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import zoranodensha.api.vehicles.Train;
import zoranodensha.common.blocks.tileEntity.TileEntityPlatform;
import zoranodensha.common.blocks.tileEntity.TileEntityPlatform.APlatformType;
import zoranodensha.common.core.ModCreativeTabs;
import zoranodensha.common.core.ModData;
import zoranodensha.common.util.ZnDMathHelper;



public class BlockPlatform extends BlockContainer
{
	private static boolean registered = false;



	public BlockPlatform()
	{
		super(Material.rock);

		setBlockName(ModData.ID + ".blocks.platform");
		setCreativeTab(ModCreativeTabs.generic);
		setHardness(1.5F);
		setResistance(10.0F);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void addCollisionBoxesToList(World world, int x, int y, int z, AxisAlignedBB mask, List list, Entity entity)
	{
		/* Only append bounding boxes if the entity isn't a train. */
		if (!(entity instanceof Train))
		{
			super.addCollisionBoxesToList(world, x, y, z, mask, list, entity);
		}
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityPlatform(meta);
	}

	@Override
	public int damageDropped(int meta)
	{
		return meta;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public IIcon getIcon(int side, int meta)
	{
		return Blocks.stone.getIcon(side, meta);
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z)
	{
		setBlockBoundsBasedOnState(world, x, y, z);
		return super.getCollisionBoundingBoxFromPool(world, x, y, z);
	}

	@Override
	public int getDamageValue(World world, int x, int y, int z)
	{
		TileEntity tile = world.getTileEntity(x, y, z);
		if (tile instanceof TileEntityPlatform)
		{
			return ((TileEntityPlatform)tile).getType().getID();
		}
		return super.getDamageValue(world, x, y, z);
	}

	@Override
	public int getRenderType()
	{
		return -1;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@SideOnly(Side.CLIENT)
	@Override
	public void getSubBlocks(Item item, CreativeTabs creativeTabs, List list)
	{
		for (int i = 0; i < APlatformType.types.size(); ++i)
		{
			list.add(new ItemStack(item, 1, i));
		}
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float xf, float yf, float zf)
	{
		if (player == null || player.getHeldItem() == null)
		{
			return false;
		}

		ItemStack heldItem = player.getHeldItem();
		TileEntity tile = world.getTileEntity(x, y, z);

		if (tile instanceof TileEntityPlatform)
		{
			TileEntityPlatform tilePlatform = (TileEntityPlatform)tile;

			if (tilePlatform.getBlockTexture() == null)
			{
				if (tilePlatform.setBlockTexture(heldItem.copy().splitStack(1)))
				{
					if (!player.capabilities.isCreativeMode)
					{
						--heldItem.stackSize;
					}
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase entity, ItemStack itemStack)
	{
		TileEntity tile = world.getTileEntity(x, y, z);
		if (tile != null)
		{
			tile.invalidate();
		}

		world.setTileEntity(x, y, z, createNewTileEntity(world, itemStack.getItemDamage()));
		world.setBlockMetadataWithNotify(x, y, z, ZnDMathHelper.getRotation(entity.rotationYawHead), 2);
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		super.onNeighborBlockChange(world, x, y, z, block);
		world.markBlockForUpdate(x, y, z);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister icon)
	{
		blockIcon = icon.registerIcon(ModData.ID + ":blockCollisionbox");
	}

	public void registerRecipes()
	{
		if (registered)
		{
			return;
		}

		registered = true;
		APlatformType.register();
	}

	@Override
	public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune)
	{
		ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
		int count = quantityDropped(metadata, fortune, world.rand);
		int type = getDamageValue(world, x, y, z);

		for (int i = 0; i < count; i++)
		{
			Item item = getItemDropped(metadata, world.rand, fortune);
			if (item != null)
			{
				drops.add(new ItemStack(item, 1, type));
			}
		}

		return drops;
	}

	@Override
	public void harvestBlock(World world, EntityPlayer player, int x, int y, int z, int meta)
	{
		super.harvestBlock(world, player, x, y, z, meta);
		world.setBlockToAir(x, y, z);
	}

	@Override
	public boolean removedByPlayer(World world, EntityPlayer player, int x, int y, int z, boolean willHarvest)
	{
		TileEntity tile = world.getTileEntity(x, y, z);
		if (tile instanceof TileEntityPlatform)
		{
			TileEntityPlatform tilePlatform = (TileEntityPlatform)tile;
			ItemStack blockItem = tilePlatform.getBlockTexture();

			if (blockItem != null)
			{
				if (!player.capabilities.isCreativeMode)
				{
					dropBlockAsItem(world, x, y + 1, z, blockItem);
				}

				tilePlatform.setBlockTexture(null);
				return false;
			}
		}

		if (willHarvest)
			return true;

		return super.removedByPlayer(world, player, x, y, z, willHarvest);
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z)
	{
		/** Try to ask for platform type's block bounds. Otherwise, assume full block. */
		TileEntity tile = world.getTileEntity(x, y, z);
		if (tile instanceof TileEntityPlatform)
		{
			APlatformType type = ((TileEntityPlatform)tile).getType();
			type.updateBoundingBox(world.getBlock(x, y, z), world.getBlockMetadata(x, y, z));
		}
		else
		{
			super.setBlockBoundsBasedOnState(world, x, y, z);
		}
	}
}