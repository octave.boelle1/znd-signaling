package zoranodensha.common.blocks.material;

import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.MaterialLiquid;

public class MaterialFluid extends MaterialLiquid
{
	public MaterialFluid()
	{
		super(MapColor.blackColor);
	}
}