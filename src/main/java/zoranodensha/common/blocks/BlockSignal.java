package zoranodensha.common.blocks;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import zoranodensha.api.structures.signals.ISignal;
import zoranodensha.api.structures.signals.SignalProperty;
import zoranodensha.api.util.Vec2;
import zoranodensha.common.blocks.tileEntity.TileEntitySignal;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.handlers.EGui;
import zoranodensha.common.util.ZnDMathHelper;


/**
 * Block containing a {@link zoranodensha.common.blocks.tileEntity.TileEntitySignal}.
 *
 * @author Jaffa
 */
public class BlockSignal extends BlockContainer
{
	public BlockSignal()
	{
		super(Material.iron);

		setBlockName(ModData.ID + ".blocks.signal");
		setBlockTextureName(ModData.ID + ":blockSignal");

		setHardness(8.0F);
		setHarvestLevel("pickaxe", 2);
		setResistance(12.0F);

		this.isBlockContainer = true;
	}


	@Override
	@SuppressWarnings("rawtypes")
	public void addCollisionBoxesToList(World world, int x, int y, int z, AxisAlignedBB aabb, List list, Entity entity)
	{
		return;
	}

	@Override
	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
	{
		return false;
	}

	@Override
	public boolean canCreatureSpawn(EnumCreatureType type, IBlockAccess world, int x, int y, int z)
	{
		return false;
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntitySignal();
	}

	@Override
	public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune)
	{
		return new ArrayList<ItemStack>();
	}

	@Override
	public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z, EntityPlayer player)
	{
		TileEntity tileEntity = world.getTileEntity(x, y, z);
		if (tileEntity instanceof TileEntitySignal)
		{
			ISignal signal = ((TileEntitySignal) tileEntity).getSignal();
			if (signal != null)
			{
				NBTTagCompound nbt = new NBTTagCompound();
				nbt.setString("name", signal.getName());

				ItemStack stack = new ItemStack(ModCenter.ItemSignal);
				stack.setTagCompound(nbt);
				return stack;
			}
		}

		return null;
	}

	@Override
	public int getRenderType()
	{
		return -1;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float px, float py, float pz)
	{
		player.openGui(ModCenter.instance, EGui.SIGNAL.ordinal(), world, x, y, z);
		return true;
	}

	@Override
	public boolean removedByPlayer(World world, @Nullable EntityPlayer player, int x, int y, int z, boolean willHarvest)
	{
		if (!world.isRemote)
		{
			/* Ensure to drop this signal only if the player was not in creative mode. */
			if (player == null || !player.capabilities.isCreativeMode)
			{
				TileEntity tileEntity = world.getTileEntity(x, y, z);
				if (tileEntity instanceof TileEntitySignal)
				{
					ISignal signal = ((TileEntitySignal) tileEntity).getSignal();
					if (signal != null)
					{
						/* Use the signal's name to spawn an item stack. */
						NBTTagCompound stackCompound = new NBTTagCompound();
						stackCompound.setString("name", signal.getName());

						ItemStack stack = new ItemStack(ModCenter.ItemSignal);
						stack.setTagCompound(stackCompound);

						this.dropBlockAsItem(world, x, y, z, stack);
					}
				}
			}

			return world.setBlockToAir(x, y, z);
		}

		return false;
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public void setBlockBoundsBasedOnState(IBlockAccess world, int x, int y, int z)
	{
		Vec2 bounds = new Vec2(1.0F, 1.0F);
		float nudgeX = 0.0F;
		float nudgeZ = 0.0F;
		int offset = 0;

		TileEntity attemptTileEntity = world.getTileEntity(x, y, z);
		ISignal attemptSignal = null;
		if (attemptTileEntity instanceof TileEntitySignal)
		{
			attemptSignal = ((TileEntitySignal) attemptTileEntity).getSignal();

			if (attemptSignal != null)
			{
				bounds = attemptSignal.getDimensions();

				SignalProperty attemptProperty = attemptSignal.getAdditionalProperties().get("postOffset");
				if (attemptProperty != null)
				{
					offset = attemptProperty.get();
				}
			}
		}

		int metaData = world.getBlockMetadata(x, y, z);
		double radians = ZnDMathHelper.RAD_MULTIPLIER * (metaData * 22.5D);
		nudgeX = (float) Math.cos(radians) * (0.25F * (float) offset);
		nudgeZ = (float) Math.sin(radians) * (0.25F * (float) offset);

		setBlockBounds(0.5F - (bounds.x / 2.0F) + nudgeX, 0.0F, 0.5F - (bounds.x / 2.0F) + nudgeZ, 0.5F + (bounds.x / 2.0F) + nudgeX, bounds.y, 0.5F + (bounds.x / 2.0F) + nudgeZ);
	}
}