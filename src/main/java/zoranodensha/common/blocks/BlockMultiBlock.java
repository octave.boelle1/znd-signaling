package zoranodensha.common.blocks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import zoranodensha.common.blocks.tileEntity.TileEntityContainerInventory;
import zoranodensha.common.blocks.tileEntity.TileEntityItemFluidHandler;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.handlers.EGui;



public abstract class BlockMultiBlock extends BlockContainer
{
	public static final ArrayList<int[]> cache = new ArrayList<int[]>();
	private static final int[][] offset = new int[][] { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };

	protected final int height;
	protected final EGui gui;
	protected final Block block;
	
	protected IIcon[] icons = new IIcon[2];



	public BlockMultiBlock(int height, EGui gui, Block block)
	{
		super(Material.rock);

		this.height = height;
		this.gui = gui;
		this.block = block;
	}

	/**
	 * Checks for completeness of the structure outgoing from the top-most layer.
	 */
	protected abstract boolean checkStructure(World world, int x, int y, int z, boolean checkMeta, boolean hasTileEntity);

	/**
	 * Called to refresh the multi block structure.
	 */
	public boolean checkStructureAndRefresh(World world, int x, int y, int z, boolean hasTileEntity)
	{
		if (checkStructure(world, x, y, z, true, hasTileEntity))
		{
			if (!world.isRemote)
			{
				for (int i = -1; i < 2; ++i)
				{
					for (int j = 1 - height; j < 1; ++j)
					{
						for (int k = -1; k < 2; ++k)
						{
							int meta = 1;

							switch (j)
							{
								case -2:
									if ((i != 0 && k == 0) || (i == 0 && k != 0))
									{
										meta = 2;
									}
									break;

								case -1:
									if (i == 0 && k == 0)
									{
										continue;
									}
									break;

								case 0:
									if (i == 0 && k == 0)
									{
										meta = 3;
									}
									break;
							}

							world.setBlockMetadataWithNotify(x + i, y + j, z + k, meta, 2);
							world.markBlockForUpdate(x + i, y + j, z + k);
						}
					}
				}
			}

			return true;
		}

		if (!world.isRemote)
		{
			for (int i = -1; i < 2; ++i)
			{
				for (int j = -2; j < 1; ++j)
				{
					for (int k = -1; k < 2; ++k)
					{
						TileEntity tileEntity = world.getTileEntity(x + i, y + j, z + k);

						if (tileEntity instanceof TileEntityContainerInventory)
						{
							tileEntity.invalidate();
							world.markBlockForUpdate(x, y, z);
						}

						if (i != 0 || j != 0 || k != 0)
						{
							if (world.getBlock(x + i, y + j, z + k) == block && world.getBlockMetadata(x + i, y + j, z + k) > 0)
							{
								world.setBlockMetadataWithNotify(x, y, z, 0, 2);
								world.markBlockForUpdate(x, y, z);
							}
						}
					}
				}
			}
		}

		return false;
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return (meta > 0 ? new TileEntityItemFluidHandler() : null);
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int meta)
	{
		return (meta == 2 && side > 1) ? icons[1] : icons[0];
	}

	/**
	 * Called to search for the TileEntityContainerInventory that is the core of this multi block.
	 * If it can not be found, destroy the multi block.
	 */
	public abstract TileEntityContainerInventory getTileCoordinates(World world, int x, int y, int z);

	protected boolean isInCache(int x, int y, int z)
	{
		Iterator<int[]> itera = cache.iterator();

		while (itera.hasNext())
		{
			int[] i = itera.next();

			if (i[0] == x && i[1] == y && i[2] == z)
			{
				return true;
			}
		}

		return false;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float xf, float yf, float zf)
	{
		TileEntityContainerInventory tileEntity = getTileCoordinates(world, x, y, z);

		if (tileEntity != null)
		{
			player.openGui(ModCenter.instance, gui.ordinal(), world, tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord);
			return true;
		}
		else if (!world.isRemote && world.getBlockMetadata(x, y, z) > 0)
		{
			setBlockMetadataAdvanced(world, x, y, z, 0, block);
		}

		return (player != null && player.inventory != null && player.inventory.getCurrentItem() == null);
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		if (!world.isRemote)
		{
			if (getTileCoordinates(world, x, y, z) == null && world.getBlockMetadata(x, y, z) > 0)
			{
				setBlockMetadataAdvanced(world, x, y, z, 0, this.block);
			}
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void randomDisplayTick(World world, int x, int y, int z, Random ran)
	{
		if (world.getBlockMetadata(x, y, z) == 2)
		{
			TileEntityContainerInventory tileEntity = getTileCoordinates(world, x, y, z);

			if (tileEntity != null)
			{
				int i = tileEntity.data[0];

				if (i > 0)
				{
					double d0 = x + 0.5D;
					double d1 = y + 0.5D + ran.nextDouble() * 0.125D;
					double d2 = z + 0.5D;
					double d3 = ran.nextDouble() * 0.15D;

					if (tileEntity.xCoord > x)
					{
						d0 -= 0.55D;
					}
					else if (tileEntity.xCoord < x)
					{
						d0 += 0.55D;
					}
					else if (tileEntity.zCoord > z)
					{
						d2 -= 0.55D;
					}
					else if (tileEntity.zCoord < z)
					{
						d2 += 0.55D;
					}

					world.spawnParticle("smoke", d0 + d3, d1, d2 + d3, 0.0D, ran.nextDouble() * 0.1D, 0.0D);
					world.spawnParticle("flame", d0 + d3, d1, d2 + d3, 0.0D, 0.0D, 0.0D);
				}
			}
		}
	}

	/**
	 * Helper method to be called for easier replacement of multi block components.
	 */
	protected void setBlockMetadataAdvanced(World world, int x, int y, int z, int meta, Block block)
	{
		world.setBlockMetadataWithNotify(x, y, z, meta, 2);
		world.markBlockForUpdate(x, y, z);

		int x0;
		int y0;
		int z0;

		for (int i = 0; i < 6; ++i)
		{
			x0 = x + (offset[i / 2][0] * (i % 2 != 0 ? -1 : 1));
			y0 = y + (offset[i / 2][1] * (i % 2 != 0 ? -1 : 1));
			z0 = z + (offset[i / 2][2] * (i % 2 != 0 ? -1 : 1));

			if (!isInCache(x0, y0, z0))
			{
				world.notifyBlockOfNeighborChange(x0, y0, z0, this);
				cache.add(new int[] { x0, y0, z0 });
			}
		}

		cache.clear();
	}
}
