package zoranodensha.common.blocks;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.client.particle.EffectRenderer;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.ERailwayState;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.structures.tracks.RailwaySectionBase;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.handlers.MovementHandler;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.util.PositionStackEntry;
import zoranodensha.api.vehicles.util.TrackObj;
import zoranodensha.api.vehicles.util.TrackObj.ITrack;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBaseGag;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.registry.ModRecipeRegistry;
import zoranodensha.trackpack.common.section.ARailwaySectionTrackPackSignalMagnet;
import zoranodensha.trackpack.common.section.ZnD_StraightSlope_0000_0000;
import zoranodensha.trackpack.common.section.ZnD_Straight_0000_0000;



public class BlockTrackBase extends BlockContainer implements ITrack
{
	/**
	 * Initialise a new source track.
	 */
	public BlockTrackBase()
	{
		super(ModCenter.Material_Track);

		setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
		setBlockName(ModData.ID + ".blocks.trackBase");
		setHardness(2.5F);
		setHarvestLevel("crowbar", 2);
		setResistance(5.0F);
		setTickRandomly(true);
	}


	@Override
	@SuppressWarnings("rawtypes")
	public void addCollisionBoxesToList(World world, int x, int y, int z, AxisAlignedBB mask, List list, Entity entity)
	{
		if (entity instanceof Train)
		{
			TileEntity tile = world.getTileEntity(x, y, z);
			if (tile instanceof ITrackBase && ((ITrackBase)tile).getInstanceOfShape() instanceof ZnD_StraightSlope_0000_0000)
				return;
		}
		super.addCollisionBoxesToList(world, x, y, z, mask, list, entity);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public boolean addDestroyEffects(World world, int x, int y, int z, int meta, EffectRenderer effectRenderer)
	{
		return false;
	}

	@Override
	public float bogieFriction(TrackObj track, PartTypeBogie bogie)
	{
		TileEntity tile = track.world.getTileEntity(track.x, track.y, track.z);
		if (tile instanceof ITrackBase)
		{
			ITrackBase iTrackBase = (ITrackBase)tile;
			if (iTrackBase.getStateOfShape().isFinished() && iTrackBase.getInstanceOfShape() instanceof ZnD_StraightSlope_0000_0000)
			{
				/* Retrieve current and next partial bogie position on the track. */
				Vec3 vec = bogie.getDirection();
				double x = bogie.getPosX();
				double y = bogie.getPosY();
				double z = bogie.getPosZ();
				double[] pos0 = iTrackBase.getPositionOnTrack(bogie, x, y, z, 0.0F, 0.0F)[0];
				double[] pos1 = iTrackBase.getPositionOnTrack(bogie, x + (vec.xCoord * 0.1D), y + (vec.yCoord * 0.1D), z + (vec.zCoord * 0.1D), 0.0F, 0.0F)[0];

				/* If there's any height difference, determine angle between both points. */
				if (pos0[1] != pos1[1])
				{
					x = pos0[0] - pos1[0];
					z = pos0[2] - pos1[2];

					x = (x == 0.0D) ? Math.abs(z) : (z == 0.0D) ? Math.abs(x) : Math.sqrt(x * x + z * z);
					y = pos0[1] - pos1[1];

					if (x > 0.0D)
					{
						float angle = (float)Math.atan2(x, y);
						float accel = MathHelper.sin(MathHelper.abs(angle)) * MovementHandler.ROLLING_RESISTANCE;

						/* If the next position is above the current, brake. Otherwise, accelerate. */
						return (pos0[1] < pos1[1]) ? -accel : accel;
					}
				}
			}
		}
		return 0.0F;
	}

	@Override
	public boolean canConnectRedstone(IBlockAccess world, int x, int y, int z, int side)
	{
		TileEntity tileEntity = world.getTileEntity(x, y, z);
		if (tileEntity instanceof TileEntityTrackBase)
		{
			return (((TileEntityTrackBase)tileEntity).getInstanceOfShape() instanceof ARailwaySectionTrackPackSignalMagnet);
		}

		return false;
	}

	@Override
	public boolean canCreatureSpawn(EnumCreatureType type, IBlockAccess world, int x, int y, int z)
	{
		return false;
	}

	@Override
	public boolean canProvidePower()
	{
		return true;
	}

	@Override
	public boolean clampToTrack(TrackObj track, @Nullable PartTypeBogie bogie, PositionStackEntry pse)
	{
		TileEntity tile = track.world.getTileEntity(track.x, track.y, track.z);
		if (tile instanceof ITrackBase)
		{
			double[][] newPos;
			if (bogie != null)
			{
				newPos = ((ITrackBase)tile).getPositionOnTrack(bogie, pse.x, pse.y, pse.z, pse.yaw, pse.pitch);
			}
			else
			{
				newPos = ((ITrackBase)tile).getPositionOnTrack(pse, pse.x, pse.y, pse.z, pse.yaw, pse.pitch);
			}

			pse.setPosition(newPos[0][0], newPos[0][1], newPos[0][2]);
			pse.setRotation((float)newPos[1][2], (float)newPos[1][0], (float)newPos[1][1]);

			if (bogie != null)
			{
				pse.y += bogie.getParent().getOffset()[1] + 0.125F;
			}
			return true;
		}

		return false;
	}

	@Override
	public TileEntity createNewTileEntity(World world, int meta)
	{
		return new TileEntityTrackBase();
	}

	@Override
	public void dropBlockAsItem(World world, int x, int y, int z, ItemStack itemStack)
	{
		super.dropBlockAsItem(world, x, y, z, itemStack);
	}

	@Override
	public float getBlockHardness(World world, int x, int y, int z)
	{
		TileEntity tile = world.getTileEntity(x, y, z);
		if (tile instanceof ITrackBase)
		{
			if (((ITrackBase)tile).getStateOfShape() == ERailwayState.BUILDINGGUIDE)
			{
				return 0.0F;
			}
		}
		return super.getBlockHardness(world, x, y, z);
	}

	@Override
	public AxisAlignedBB getCollisionBoundingBoxFromPool(World world, int x, int y, int z)
	{
		return null;
	}

	@Override
	public Item getItemDropped(int i, Random ran, int i1)
	{
		return null;
	}

	@Override
	public ItemStack getPickBlock(MovingObjectPosition target, World world, int x, int y, int z, EntityPlayer player)
	{
		TileEntity tileEntity = world.getTileEntity(x, y, z);
		if (tileEntity instanceof ITrackBase)
		{
			ITrackBase trackBase = (ITrackBase)tileEntity;
			ItemStack itemStack = new ItemStack(ModCenter.ItemEngineersBlueprint);
			itemStack.setTagCompound(ModRecipeRegistry.getBlueprintNBT(0, trackBase.getInstanceOfShape(), trackBase.getDirectionOfSection()));

			if (player != null)
			{
				for (ItemStack itemStack0 : player.inventory.mainInventory)
				{
					if (itemStack0 != null && itemStack.getDisplayName().equals(itemStack0.getDisplayName()))
					{
						return null;
					}
				}

				return itemStack;
			}
		}

		return null;
	}

	@Override
	public int getRenderType()
	{
		return -1;
	}

	/**
	 * Helper method to determine if this track should output a redstone signal.
	 * 
	 * @param blockAccess - An object to obtain the game world instance from.
	 * @param x - The X position of this track.
	 * @param y - The Y position of this track.
	 * @param z - The Z position of this track.
	 * @return - A boolean indicating whether this track tile should output any redstone due to detecting a train.
	 */
	private boolean isDetectingTrain(IBlockAccess blockAccess, int x, int y, int z)
	{
		/*
		 * Get the tile entity from the provided coordinates.
		 */
		TileEntity tileEntitySupplied = blockAccess.getTileEntity(x, y, z);

		if (tileEntitySupplied instanceof ITrackBase)
		{
			ITrackBase trackBase = (ITrackBase)tileEntitySupplied;
			IRailwaySection section = trackBase.getInstanceOfShape();

			/*
			 * Check if the track is a magnet.
			 */
			if (section instanceof ARailwaySectionTrackPackSignalMagnet)
			{
				/*
				 * We can now use the trackBase instance to check:
				 * a) if a magnet is installed, and
				 * b) if the 'detected train' flag is true.
				 */
				boolean magnetInstalled = trackBase.getField(ARailwaySectionTrackPackSignalMagnet.INDEX_HASMAGNET, false, Boolean.class);
				if (magnetInstalled)
				{
					boolean detectTrain = trackBase.getField(ARailwaySectionTrackPackSignalMagnet.INDEX_DETECTTRAIN, false, Boolean.class);

					return detectTrain;
				}
			}
		}

		return false;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public int isProvidingStrongPower(IBlockAccess blockAccess, int x, int y, int z, int side)
	{
		if (isDetectingTrain(blockAccess, x, y, z))
		{
			if (ForgeDirection.getOrientation(side).equals(ForgeDirection.UP))
			{
				return 15;
			}
		}

		/*
		 * Call the superclass if no train is detected.
		 */
		return super.isProvidingStrongPower(blockAccess, x, y, z, side);
	}

	@Override
	public int isProvidingWeakPower(IBlockAccess blockAccess, int x, int y, int z, int side)
	{
		if (isDetectingTrain(blockAccess, x, y, z))
		{
			TileEntity te = blockAccess.getTileEntity(x, y, z);
			if (te instanceof ITrackBase)
			{
				ITrackBase trackBase = (ITrackBase)te;

				/* Check the direction of the track - only output redstone to the sides. */
				int orientation = trackBase.getOrientation();
				ForgeDirection redstoneDirection = ForgeDirection.getOrientation(side);

				switch (orientation)
				{
					// East & West
					case 0:
					case 2: {
						if (redstoneDirection.equals(ForgeDirection.NORTH) || redstoneDirection.equals(ForgeDirection.SOUTH))
						{
							return 15;
						}
						break;
					}

					// North & South
					case 1:
					case 3: {
						if (redstoneDirection.equals(ForgeDirection.WEST) || redstoneDirection.equals(ForgeDirection.EAST))
						{
							return 15;
						}
						break;
					}
				}
			}
		}

		/*
		 * Call the superclass if no train is detected.
		 */
		return super.isProvidingWeakPower(blockAccess, x, y, z, side);
	}

	@Override
	public boolean isTrack(TrackObj track, PartTypeBogie bogie)
	{
		TileEntity tile = track.world.getTileEntity(track.x, track.y, track.z);
		if (tile instanceof ITrackBase)
		{
			/* If the track isn't finished, we can't use it. */
			ITrackBase iTrackBase = (ITrackBase)tile;
			if (!iTrackBase.getStateOfShape().isFinished())
			{
				return false;
			}

			/* If a bogie is specified, ensure bogie and track rotation match. */
			if (bogie != null)
			{
				final float bogieYaw = bogie.getRotationYaw();
				final float trackYaw = (float)iTrackBase.getPositionOnTrack(bogie, track.x + 0.5D, track.y, track.z + 0.5D, bogieYaw, 0.0F)[1][0];
				return MovementHandler.INSTANCE.getIsRotationMatching(bogieYaw, trackYaw, 20.0F);
			}

			return true;
		}
		return false;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int side, float fx, float fy, float fz)
	{
		TileEntity tileEntity = world.getTileEntity(x, y, z);
		if (tileEntity instanceof ITrackBase)
		{
			ITrackBase trackBase = (ITrackBase)tileEntity;
			IRailwaySection section = trackBase.getInstanceOfShape();

			if (section != null)
			{
				if (section.getCanStay(world, x, y, z, trackBase.getOrientation(), trackBase.getDirectionOfSection()))
				{
					if (section.onTrackWorkedOn(trackBase, player, world.isRemote))
					{
						world.markBlockForUpdate(x, y, z);
						world.notifyBlockChange(x, y, z, world.getBlock(x, y, z));

						return interactWithGagBlocks(world, x, y - 1, z, trackBase.getOrientation(), section, trackBase.getDirectionOfSection(), 4);
					}
				}
				else if (!world.isRemote && !ERailwayState.BUILDINGGUIDE.equals(trackBase.getStateOfShape()))
				{
					return world.setBlockToAir(x, y, z);
				}
			}
		}
		else if (!world.isRemote)
		{
			return world.setBlockToAir(x, y, z);
		}

		return false;
	}

	@Override
	public void onBlockPreDestroy(World world, int x, int y, int z, int oldMeta)
	{
		if (!world.isRemote)
		{
			TileEntity tileEntity = world.getTileEntity(x, y, z);

			if (tileEntity instanceof ITrackBase)
			{
				ITrackBase trackBase = (ITrackBase)tileEntity;
				IRailwaySection section = trackBase.getInstanceOfShape();

				if (section == null)
				{
					return;
				}

				interactWithGagBlocks(world, x, y, z, trackBase.getOrientation(), section, trackBase.getDirectionOfSection(), 2);
			}
		}
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block block)
	{
		if (!world.isRemote)
		{
			TileEntity tileEntity = world.getTileEntity(x, y, z);

			if (tileEntity instanceof ITrackBase)
			{
				ITrackBase trackBase = (ITrackBase)tileEntity;

				if (ERailwayState.BUILDINGGUIDE.equals(trackBase.getStateOfShape()))
				{
					return;
				}

				IRailwaySection section = trackBase.getInstanceOfShape();

				if (section == null || !section.getCanStay(world, x, y, z, trackBase.getOrientation(), trackBase.getDirectionOfSection()))
				{
					world.setBlockToAir(x, y, z);
				}
			}
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerBlockIcons(IIconRegister icon)
	{
		blockIcon = icon.registerIcon(ModData.ID + ":blockCollisionbox");
	}

	@Override
	public boolean removedByPlayer(World world, @Nullable EntityPlayer player, int x, int y, int z, boolean willHarvest)
	{
		if (!world.isRemote)
		{
			TileEntity tileEntity = world.getTileEntity(x, y, z);

			if (tileEntity instanceof ITrackBase)
			{
				ITrackBase trackBase = (ITrackBase)tileEntity;
				IRailwaySection section = trackBase.getInstanceOfShape();

				if (section == null)
				{
					return false;
				}

				if (player == null || !player.capabilities.isCreativeMode)
				{
					List<ItemStack> drops = section.onTrackDestroyed(tileEntity, player);

					if (drops == null)
					{
						world.markBlockForUpdate(x, y, z);

						return false;
					}

					boolean flag = false;

					for (ItemStack itemStack : drops)
					{
						if (itemStack == null)
						{
							flag = true;
						}
						else
						{
							this.dropBlockAsItem(world, x, y, z, itemStack);
						}
					}

					if (flag)
					{
						return (interactWithGagBlocks(world, x, y, z, trackBase.getOrientation(), section, trackBase.getDirectionOfSection(), 2)
								&& interactWithGagBlocks(world, x, y, z, trackBase.getOrientation(), section, trackBase.getDirectionOfSection(), 4) && world.setBlockToAir(x, y, z));
					}

					return true;
				}

				return (interactWithGagBlocks(world, x, y, z, trackBase.getOrientation(), section, trackBase.getDirectionOfSection(), 2)
						&& interactWithGagBlocks(world, x, y, z, trackBase.getOrientation(), section, trackBase.getDirectionOfSection(), 4) && world.setBlockToAir(x, y, z));
			}

			world.setBlockToAir(x, y, z);
		}

		return false;
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public int tickRate(World world)
	{
		return 1; // TODO @All - Can this be increased?
	}

	@Override
	public void updateTick(World world, int x, int y, int z, Random ran)
	{
		if (!world.isRemote)
		{
			// TileEntity tileEntity = world.getTileEntity(x, y, z);
			//
			// if (tileEntity instanceof ITrackBase)
			// {
			// ITrackBase trackBase = (ITrackBase)tileEntity;
			// IRailwaySection section = trackBase.getInstanceOfShape();
			//
			// if (section == null)
			// {
			// return;
			// }
			//
			// if (interactWithGagBlocks(world, x, y, z, trackBase.getOrientation(), section, trackBase.getDirectionOfSection(), 2))
			// {
			// world.setBlockToAir(x, y, z);
			// interactWithGagBlocks(world, x, y - 1, z, trackBase.getOrientation(), section, trackBase.getDirectionOfSection(), 4);
			// }
			// }
		}
	}

	/**
	 * Called to place all gag blocks.
	 *
	 * @param flag - 0 = Checks for clear space and solid ground below, 1 = Checks
	 *            for clear space and creates gag blocks if possible, 2 = Removes
	 *            all gag blocks, 3 = Checks for all gag blocks, 4 = Notifies all
	 *            blocks of an neighbor update.
	 * @return True if successful.
	 */
	public static boolean interactWithGagBlocks(World world, int x, int y, int z, int rotation, IRailwaySection section, EDirection dir, int flag)
	{
		if (section != null)
		{
			List<int[]> list = RailwaySectionBase.getGagBlocksWithRotation(0, rotation, section.getGagBlocks(dir));
			Iterator<int[]> itera = list.iterator();

			if (flag < 2)
			{
				while (itera.hasNext())
				{
					int[] i = itera.next();

					if (!world.getBlock(x + i[0], y + i[1], z + i[2]).isReplaceable(world, x + i[0], y + i[1], z + i[2]) || !world.getBlock(x + i[0], y + i[1] - 1, z + i[2]).isSideSolid(world, x, y - 1, z, ForgeDirection.UP))
					{
						return false;
					}
				}

				if (flag == 0)
				{
					return true;
				}

				itera = list.iterator();

				while (itera.hasNext())
				{
					int[] i = itera.next();

					if (i[0] == 0 && i[1] == 0 && i[2] == 0)
					{
						continue;
					}

					world.setBlock(x + i[0], y + i[1], z + i[2], ModCenter.BlockTrackBaseGag, (i.length == 4 ? i[3] : 0), 2);
					world.setTileEntity(x + i[0], y + i[1], z + i[2], new TileEntityTrackBaseGag(x, y, z));
					world.notifyBlockChange(x + i[0], y + i[1], z + i[2], ModCenter.BlockTrackBaseGag);
				}

				return true;
			}
			else if (flag == 2)
			{
				while (itera.hasNext())
				{
					int[] i = itera.next();

					if (i[0] == 0 && i[1] == 0 && i[2] == 0)
					{
						continue;
					}

					if (world.getBlock(x + i[0], y + i[1], z + i[2]) == ModCenter.BlockTrackBaseGag)
					{
						world.setBlockToAir(x + i[0], y + i[1], z + i[2]);
						world.notifyBlockChange(x + i[0], y + i[1], z + i[2], ModCenter.BlockTrackBaseGag);
					}
				}

				return true;
			}
			else if (flag == 3)
			{
				while (itera.hasNext())
				{
					int[] i = itera.next();

					if (i[0] == 0 && i[1] == 0 && i[2] == 0)
					{
						continue;
					}

					if (world.getBlock(x + i[0], y + i[1], z + i[2]) != ModCenter.BlockTrackBaseGag)
					{
						return false;
					}
				}

				return true;
			}
			else if (flag == 4)
			{
				while (itera.hasNext())
				{
					int[] i = itera.next();

					world.notifyBlockChange(x + i[0], y + i[1], z + i[2], ModCenter.BlockTrackBase);
				}

				return true;
			}
		}

		return false;
	}
}
