package zoranodensha.common.containers.slot;

import java.util.ArrayList;

import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import zoranodensha.api.structures.EEngineerTableTab;
import zoranodensha.common.blocks.tileEntity.TileEntityEngineerTable;
import zoranodensha.common.core.registry.ModRecipeRegistry;



public class SlotEngineerTablePieces extends Slot
{
	private final ItemStack itemStack;



	public SlotEngineerTablePieces(TileEntityEngineerTable tileEntity, int id, int x, int y, EEngineerTableTab tab, int itemId)
	{
		super(tileEntity, id, x, y);

		itemStack = getItemStack(tab, itemId, tileEntity);
	}

	@Override
	public ItemStack decrStackSize(int dec)
	{
		return itemStack;
	}

	@Override
	public ItemStack getStack()
	{
		return itemStack;
	}

	@Override
	public boolean isItemValid(ItemStack itemStack)
	{
		return (itemStack != null && itemStack.stackSize == 0);
	}

	public static ItemStack getItemStack(EEngineerTableTab tab, int itemId, TileEntityEngineerTable tileEntity)
	{
		if (itemId < 0)
		{
			return null;
		}

		if (tab.isTrainTab() && tileEntity != null)
		{
			int presetSize = tileEntity.presets.size();
			int importSize = TileEntityEngineerTable.presetsImport.size();

			if (itemId < presetSize)
			{
				return tileEntity.presets.get(itemId);
			}
			else if ((itemId -= presetSize) < importSize)
			{
				return TileEntityEngineerTable.presetsImport.get(itemId);
			}
			else if ((itemId -= importSize) < ModRecipeRegistry.getIngredientsByType(tab.toInt()).size())
			{
				return ModRecipeRegistry.getIngredientsByType(tab.toInt()).get(itemId);
			}

			return null;
		}

		ArrayList<ItemStack> list = ModRecipeRegistry.getIngredientsByType(tab.toInt());

		return (itemId < list.size() ? list.get(itemId) : null);
	}
}
