package zoranodensha.common.containers.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;



public class SlotEngineerTableInventory extends Slot
{
	public SlotEngineerTableInventory(IInventory inventory, int id, int x, int y)
	{
		super(inventory, id, x, y);
	}

	@Override
	public boolean isItemValid(ItemStack itemStack)
	{
		return (itemStack != null && itemStack.stackSize > 0);
	}
}
