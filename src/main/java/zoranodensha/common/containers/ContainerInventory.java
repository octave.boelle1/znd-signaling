package zoranodensha.common.containers;

import java.util.Iterator;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import zoranodensha.common.blocks.tileEntity.TileEntityContainerInventory;



public class ContainerInventory extends Container
{
	public TileEntityContainerInventory tileEntity;
	public int[] lastData;



	public ContainerInventory(TileEntityContainerInventory tileEntity)
	{
		this.tileEntity = tileEntity;
		lastData = new int[tileEntity.data.length];
	}

	@Override
	public void addCraftingToCrafters(ICrafting iCrafting)
	{
		super.addCraftingToCrafters(iCrafting);

		for (int i = 0; i < tileEntity.data.length; ++i)
		{
			iCrafting.sendProgressBarUpdate(this, i, tileEntity.data[i]);
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer player)
	{
		return tileEntity.isUseableByPlayer(player);
	}

	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();

		Iterator<?> itera;

		for (int i = 0; i < lastData.length; ++i)
		{
			if (lastData[i] != tileEntity.data[i])
			{
				lastData[i] = tileEntity.data[i];
				itera = crafters.iterator();

				while (itera.hasNext())
				{
					((ICrafting)itera.next()).sendProgressBarUpdate(this, i, tileEntity.data[i]);
				}
			}
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void updateProgressBar(int slot, int update)
	{
		if (tileEntity.isIndexPermitted(slot, 0))
		{
			tileEntity.data[slot] = update;
		}
	}
}
