package zoranodensha.common.containers;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import zoranodensha.common.blocks.tileEntity.TileEntityRefinery;
import zoranodensha.common.containers.slot.SlotRestrictive;



public class ContainerRefinery extends ContainerInventory
{
	public int lastTemperature;



	public ContainerRefinery(InventoryPlayer inventory, TileEntityRefinery tileEntity)
	{
		super(tileEntity);

		lastTemperature = tileEntity.getTemperature();
		addSlotToContainer(new SlotRestrictive(tileEntity, 0, 8, 19));
		addSlotToContainer(new SlotRestrictive(tileEntity, 1, 36, 102));
		addSlotToContainer(new SlotRestrictive(tileEntity, 2, 8, 102));
		addSlotToContainer(new SlotRestrictive(tileEntity, 3, 64, 19));
		addSlotToContainer(new SlotRestrictive(tileEntity, 4, 86, 19));
		addSlotToContainer(new SlotRestrictive(tileEntity, 5, 108, 19));
		addSlotToContainer(new SlotRestrictive(tileEntity, 6, 130, 19));
		addSlotToContainer(new SlotRestrictive(tileEntity, 7, 152, 19));
		addSlotToContainer(new SlotRestrictive(tileEntity, 8, 64, 102));
		addSlotToContainer(new SlotRestrictive(tileEntity, 9, 86, 102));
		addSlotToContainer(new SlotRestrictive(tileEntity, 10, 108, 102));
		addSlotToContainer(new SlotRestrictive(tileEntity, 11, 130, 102));
		addSlotToContainer(new SlotRestrictive(tileEntity, 12, 152, 102));

		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 9; ++j)
			{
				addSlotToContainer(new Slot(inventory, j + i * 9 + 9, 8 + j * 18, 136 + i * 18));
			}
		}

		for (int i = 0; i < 9; ++i)
		{
			addSlotToContainer(new Slot(inventory, i, 8 + i * 18, 194));
		}
	}

	@Override
	public void addCraftingToCrafters(ICrafting iCrafting)
	{
		super.addCraftingToCrafters(iCrafting);

		iCrafting.sendProgressBarUpdate(this, 3, ((TileEntityRefinery)tileEntity).getTemperature());
	}

	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();

		for (int i = 0; i < crafters.size(); ++i)
		{
			ICrafting iCrafting = (ICrafting)crafters.get(i);

			int j = ((TileEntityRefinery)tileEntity).getTemperature();

			if (lastTemperature != j)
			{
				iCrafting.sendProgressBarUpdate(this, 3, j);
			}

			lastTemperature = j;
		}
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slotId)
	{
		ItemStack itemStack = null;
		Slot slot = (Slot)inventorySlots.get(slotId);

		if (slot != null && slot.getHasStack())
		{
			ItemStack itemStack0 = slot.getStack();
			itemStack = itemStack0.copy();

			if (slotId == 2 || (slotId > 7 && slotId < 13))
			{
				if (!mergeItemStack(itemStack0, 13, 49, true))
				{
					return null;
				}

				slot.onSlotChange(itemStack0, itemStack);
			}
			else if (slotId > 12)
			{
				if (tileEntity.isItemValidForSlot(0, itemStack0))
				{
					if (!mergeItemStack(itemStack0, 0, 1, false))
					{
						return null;
					}
				}
				else if (tileEntity.isItemValidForSlot(1, itemStack0))
				{
					if (!mergeItemStack(itemStack0, 1, 2, false))
					{
						return null;
					}
				}
				else if (tileEntity.isItemValidForSlot(3, itemStack0))
				{
					if (!mergeItemStack(itemStack0, 3, 4, false))
					{
						return null;
					}
				}
				else if (tileEntity.isItemValidForSlot(4, itemStack0))
				{
					if (!mergeItemStack(itemStack0, 4, 5, false))
					{
						return null;
					}
				}
				else if (tileEntity.isItemValidForSlot(5, itemStack0))
				{
					if (!mergeItemStack(itemStack0, 5, 6, false))
					{
						return null;
					}
				}
				else if (tileEntity.isItemValidForSlot(6, itemStack0))
				{
					if (!mergeItemStack(itemStack0, 6, 7, false))
					{
						return null;
					}
				}
				else if (tileEntity.isItemValidForSlot(7, itemStack0))
				{
					if (!mergeItemStack(itemStack0, 7, 8, false))
					{
						return null;
					}
				}
				else if (slotId > 12 && slotId < 40)
				{
					if (!mergeItemStack(itemStack0, 40, 49, false))
					{
						return null;
					}
				}
				else if (slotId > 39 && slotId < 49 && !mergeItemStack(itemStack0, 13, 40, false))
				{
					return null;
				}
			}
			else if (!mergeItemStack(itemStack0, 13, 49, false))
			{
				return null;
			}

			if (itemStack0.stackSize == 0)
			{
				slot.putStack((ItemStack)null);
			}
			else
			{
				slot.onSlotChanged();
			}

			if (itemStack0.stackSize == itemStack.stackSize)
			{
				return null;
			}

			slot.onPickupFromSlot(player, itemStack0);
		}

		return itemStack;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void updateProgressBar(int slot, int update)
	{
		if (slot == 3)
		{
			((TileEntityRefinery)tileEntity).temperature = update;
		}
		else
		{
			super.updateProgressBar(slot, update);
		}
	}
}
