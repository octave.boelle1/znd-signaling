package zoranodensha.common.containers;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import zoranodensha.api.structures.EEngineerTableTab;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.client.gui.table.buttons.ButtonPopUpDragDownMenu;
import zoranodensha.client.gui.table.buttons.EButtonDragDownType;
import zoranodensha.common.blocks.tileEntity.TileEntityEngineerTable;
import zoranodensha.common.containers.slot.SlotEngineerTableInventory;
import zoranodensha.common.containers.slot.SlotEngineerTableOutput;
import zoranodensha.common.containers.slot.SlotEngineerTablePieces;
import zoranodensha.common.containers.slot.SlotEngineerTableRecipe;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.handlers.EGui;
import zoranodensha.common.core.registry.ModPartRegistry;
import zoranodensha.common.items.ItemTrain;



public class ContainerEngineerTable extends Container
{
	public TileEntityEngineerTable tileEntity;
	private InventoryPlayer inventory;
	private EEngineerTableTab lastTab;
	private int lastProcess;



	public ContainerEngineerTable(InventoryPlayer inventory, TileEntityEngineerTable tileEntity)
	{
		this.tileEntity = tileEntity;
		this.inventory = inventory;
		lastTab = tileEntity.tab;
		addSlotsToContainer();
	}

	@Override
	public void addCraftingToCrafters(ICrafting iCrafting)
	{
		super.addCraftingToCrafters(iCrafting);

		iCrafting.sendProgressBarUpdate(this, 0, tileEntity.process);
	}

	public void addSlotsToContainer()
	{
		inventorySlots.clear();
		inventoryItemStacks.clear();

		addSlotToContainer(new SlotEngineerTableOutput(tileEntity, 0, 7, 109, new ItemStack(Items.paper)));
		addSlotToContainer(new SlotEngineerTableOutput(tileEntity, 1, 25, 109, new ItemStack(Items.dye, 0)));
		addSlotToContainer(new SlotEngineerTableOutput(tileEntity, 2, 77, 109, new ItemStack(ModCenter.ItemEngineersBlueprint), new ItemStack(ModCenter.ItemTrain)));

		for (int i = 0; i < 3; ++i)
		{
			for (int j = 0; j < 9; ++j)
			{
				addSlotToContainer(new SlotEngineerTableInventory(inventory, j + i * 9 + 9, 8 + j * 18, 136 + i * 18));
			}
		}

		for (int i = 0; i < 9; ++i)
		{
			addSlotToContainer(new SlotEngineerTableInventory(inventory, i, 8 + i * 18, 194));
		}

		for (int i = 0; i < 5; ++i)
		{
			for (int j = 0; j < 3; ++j)
			{
				addSlotToContainer(new SlotEngineerTablePieces(tileEntity, j + i * 3 + 3, 109 + j * 18, 41 + i * 18, lastTab, j + i * 3 + (tileEntity.page * 15)));
			}
		}

		if (!lastTab.isTrainTab())
		{
			for (int i = 0; i < 5; ++i)
			{
				for (int j = 0; j < 5; ++j)
				{
					addSlotToContainer(new SlotEngineerTableRecipe(tileEntity, j + i * 5 + 18, 7 + j * 18, 7 + i * 18));
				}
			}
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer player)
	{
		return tileEntity.isUseableByPlayer(player);
	}

	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();

		boolean processChanged = lastProcess != tileEntity.process;
		boolean tabChanged = lastTab != tileEntity.tab;

		for (int i = 0; i < crafters.size(); ++i)
		{
			ICrafting iCrafting = (ICrafting)crafters.get(i);

			if (processChanged)
			{
				iCrafting.sendProgressBarUpdate(this, 0, tileEntity.process);
			}

			if (tabChanged && iCrafting instanceof EntityPlayer)
			{
				((EntityPlayer)iCrafting).openGui(ModCenter.instance, EGui.ENGINEER_TABLE.ordinal(), tileEntity.getWorldObj(), tileEntity.xCoord, tileEntity.yCoord, tileEntity.zCoord);
			}
		}

		lastProcess = tileEntity.process;
	}

	@Override
	public void putStackInSlot(int index, ItemStack itemStack)
	{
		if (index >= 0 && index < inventorySlots.size())
		{
			getSlot(index).putStack(itemStack);
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void putStacksInSlots(ItemStack[] itemStacks)
	{
		int slotsSize = inventorySlots.size();

		for (int i = 0; i < itemStacks.length; ++i)
		{
			if (i >= slotsSize)
			{
				break;
			}

			getSlot(i).putStack(itemStacks[i]);
		}
	}

	@SideOnly(Side.CLIENT)
	private void slotClick(int index)
	{
		if (Minecraft.getMinecraft().currentScreen instanceof GUIEngineerTable)
		{
			GUIEngineerTable guiEngineerTable = ((GUIEngineerTable)Minecraft.getMinecraft().currentScreen);
			Slot slot0 = (Slot)inventorySlots.get(index);

			if (slot0 == null || !slot0.getHasStack() || guiEngineerTable.buttonPopUp != null)
			{
				return;
			}

			index -= 39;

			if (guiEngineerTable.buttonPopUp == null)
			{
				EButtonDragDownType[] buttons = null;
				ItemStack itemStack = slot0.getStack();

				if (itemStack.getItem() instanceof ItemTrain)
				{
					switch (itemStack.getTagCompound().getInteger(ModPartRegistry.PRESET_KEY))
					{
						default:
							if (index == 0)
							{
								buttons = new EButtonDragDownType[] { EButtonDragDownType.PRESET_LOAD, EButtonDragDownType.PRESET_CREATE_BLUEPRINT, EButtonDragDownType.PRESET_EXPORT,
										EButtonDragDownType.PRESET_REMOVE };
							}
							else
							{
								buttons = new EButtonDragDownType[] { EButtonDragDownType.PRESET_LOAD, EButtonDragDownType.PRESET_CREATE_BLUEPRINT, EButtonDragDownType.PRESET_EXPORT,
										EButtonDragDownType.PRESET_REMOVE, EButtonDragDownType.PRESET_MOVE_TO_BEGIN };
							}
							break;

						case 1:
							buttons = new EButtonDragDownType[] { EButtonDragDownType.PRESET_LOAD, EButtonDragDownType.PRESET_CREATE_BLUEPRINT, EButtonDragDownType.PRESET_REMOVE_FILE };
							break;

						case 2:
							buttons = new EButtonDragDownType[] { EButtonDragDownType.PRESET_LOAD, EButtonDragDownType.PRESET_CREATE_BLUEPRINT };
							break;
					}
				}
				else
				{
					buttons = new EButtonDragDownType[] { EButtonDragDownType.ITEM_ADD_TO_MODEL, EButtonDragDownType.ITEM_DESELECT_ALL, EButtonDragDownType.ITEM_SELECT_ALL };
				}

				guiEngineerTable.buttonPopUp =
						new ButtonPopUpDragDownMenu(slot0.getStack(), 4, 109 + ((index % 3) * 18) + guiEngineerTable.getXPos(), 41 + ((index / 3) * 18) + guiEngineerTable.getYPos(), buttons);
				guiEngineerTable.initGui();
			}
		}
	}

	@Override
	public ItemStack slotClick(int index, int i0, int i1, EntityPlayer player)
	{
		if (index < 0 || index >= inventorySlots.size())
		{
			return null;
		}

		if (!tileEntity.tab.isTrainTab() || index < 39 || index > 53)
		{
			return super.slotClick(index, i0, i1, player);
		}

		if (player.worldObj.isRemote)
		{
			this.slotClick(index);
		}

		return null;
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer player, int slotId)
	{
		ItemStack itemStack = null;
		Slot slot = (Slot)inventorySlots.get(slotId);

		if (slot != null && slot.getHasStack())
		{
			ItemStack itemStack0 = slot.getStack();
			itemStack = itemStack0.copy();

			if (slotId == 2)
			{
				if (!mergeItemStack(itemStack0, 3, 39, true))
				{
					return null;
				}

				slot.onSlotChange(itemStack0, itemStack);
			}
			else if (slotId > 2 && slotId < 39)
			{
				if (itemStack0.getItem() == Items.paper)
				{
					Slot slot0 = (Slot)inventorySlots.get(0);

					if (slot0 != null && !slot0.getHasStack())
					{
						if (itemStack0.stackSize > slot0.getSlotStackLimit())
						{
							slot0.putStack(itemStack0.splitStack(slot0.getSlotStackLimit()));
						}
						else
						{
							if (!mergeItemStack(itemStack0, 0, 1, false))
							{
								return null;
							}
						}
					}
				}
				else if (itemStack0.getItem() == Items.dye && itemStack0.getItemDamage() == 0)
				{
					Slot slot0 = (Slot)inventorySlots.get(1);

					if (slot0 != null && !slot0.getHasStack())
					{
						if (itemStack0.stackSize > slot0.getSlotStackLimit())
						{
							slot0.putStack(itemStack0.splitStack(slot0.getSlotStackLimit()));
						}
						else
						{
							if (!mergeItemStack(itemStack0, 1, 2, false))
							{
								return null;
							}
						}
					}
				}
				else if (slotId > 2 && slotId < 30)
				{
					if (!mergeItemStack(itemStack0, 30, 39, false))
					{
						return null;
					}
				}
				else if (slotId > 29 && !mergeItemStack(itemStack0, 3, 30, false))
				{
					return null;
				}
			}
			else if (!mergeItemStack(itemStack0, 3, 39, false))
			{
				return null;
			}

			if (itemStack0.stackSize == 0)
			{
				slot.putStack((ItemStack)null);
			}
			else
			{
				slot.onSlotChanged();
			}

			if (itemStack0.stackSize == itemStack.stackSize)
			{
				return null;
			}

			slot.onPickupFromSlot(player, itemStack0);
		}

		return itemStack;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void updateProgressBar(int slot, int update)
	{
		if (slot == 0)
		{
			tileEntity.process = update;
		}
	}
}
