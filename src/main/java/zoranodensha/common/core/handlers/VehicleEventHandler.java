package zoranodensha.common.core.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import cpw.mods.fml.common.eventhandler.Event.Result;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentStyle;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;
import net.minecraftforge.common.util.ForgeDirection;
import net.minecraftforge.fluids.FluidTankInfo;
import zoranodensha.api.structures.tracks.ITrackBase;
import zoranodensha.api.util.APILogger;
import zoranodensha.api.util.ETagCall;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.VehicleData;
import zoranodensha.api.vehicles.event.PlayerInteractTrainEvent;
import zoranodensha.api.vehicles.event.living.LivingHitByTrainEvent;
import zoranodensha.api.vehicles.event.living.LivingKilledByTrainEvent;
import zoranodensha.api.vehicles.event.partType.PartTypeGUIOpenEvent;
import zoranodensha.api.vehicles.event.property.PropertyMissingNBTEvent;
import zoranodensha.api.vehicles.event.train.TrainMiddleClickEvent;
import zoranodensha.api.vehicles.event.train.TrainSpawnEvent;
import zoranodensha.api.vehicles.event.train.TrainUpdateEvent.TrainUpdateSpeedEvent;
import zoranodensha.api.vehicles.event.vehicle.VehicleDropEvent;
import zoranodensha.api.vehicles.event.vehicle.VehicleListChangedEvent;
import zoranodensha.api.vehicles.handlers.CouplingHandler;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.property.PropBoolean;
import zoranodensha.api.vehicles.part.property.PropColor;
import zoranodensha.api.vehicles.part.property.PropFloat;
import zoranodensha.api.vehicles.part.property.PropInteger;
import zoranodensha.api.vehicles.part.property.PropString;
import zoranodensha.api.vehicles.part.type.PartTypeBogie;
import zoranodensha.api.vehicles.part.type.PartTypeTransportItem;
import zoranodensha.api.vehicles.part.type.PartTypeTransportTank;
import zoranodensha.api.vehicles.part.util.AVehiclePartRegistry;
import zoranodensha.api.vehicles.part.util.OrderedPartsList;
import zoranodensha.api.vehicles.part.util.OrderedPartsList.IPartsSelector;
import zoranodensha.api.vehicles.part.util.OrderedTypesList;
import zoranodensha.api.vehicles.part.util.PartHelper;
import zoranodensha.api.vehicles.util.TrackObj;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.common.blocks.BlockTrackBase;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;
import zoranodensha.common.core.registry.ModPartRegistry;
import zoranodensha.common.damageSource.DamageSourceVehicleCollision;
import zoranodensha.trackpack.common.section.ZnD_StraightMaint_0000_0000;
import zoranodensha.vehicleParts.common.parts.basic.AVehParBasic;
import zoranodensha.vehicleParts.common.parts.seat.VehParSeat;



/**
 * Event handler class for any events involving {@link Train}s. Extended and registered by the mod's standard event handler.
 */
public class VehicleEventHandler
{
	/** Array containing the suffixes of all properties ignored by the {@link #propertyMissingNBTEvent(PropertyMissingNBTEvent) legacy handler event}. */
	private static final String[] ignoredProps = new String[] { "color_arm_bot", "color_arm_top", "color_base_frame", "color_base_ins", "color_base_springs", "color_head_frame", "color_head_guides", "destination", "doorShutAt", "doorSpeed",
			"hasMotor", "length", "localY", "motorActivity", "mountedOffsetY", "pieces", "size", "textureURL", "tractiveEffort" };


	@SubscribeEvent
	public void livingHitByTrainEvent(LivingHitByTrainEvent event)
	{
		if (event.damage < 1.0F || event.entity.isEntityInvulnerable() || (event.entity instanceof EntityPlayer && ((EntityPlayer)event.entity).capabilities.isCreativeMode))
		{
			event.setCanceled(true);
		}
		else
		{
			event.damageSource = new DamageSourceVehicleCollision(event.vehicle);
			event.setResult(Result.ALLOW);
		}
	}

	@SubscribeEvent
	public void livingKilledByTrainEvent(LivingKilledByTrainEvent event)
	{
		if (!event.train.worldObj.isRemote)
		{
			APILogger.info(this, String.format("[TRAIN (%s @ %.2fx %.2fy %.2fz)] Reported collision with entity: %s", event.train.getCommandSenderName(), event.train.posX, event.train.posY, event.train.posZ, event.entityLiving.toString()));
		}
	}

	@SubscribeEvent
	public void partTypeGUIOpenEvent(PartTypeGUIOpenEvent event)
	{
		if (event.getResult() != Result.ALLOW || event.mod == null)
		{
			if (event.partType instanceof PartTypeTransportTank)
			{
				event.modGuiID = EGui.VEHPAR_TRANSPORT_TANK.ordinal();
			}
			else if (event.partType instanceof PartTypeTransportItem)
			{
				event.modGuiID = EGui.VEHPAR_TRANSPORT_ITEM.ordinal();
			}
			else
			{
				// Not our part type; don't handle.
				return;
			}
			
			event.mod = ModCenter.instance;
			event.setResult(Result.ALLOW);
		}
	}

	@SubscribeEvent
	public void playerInteractTrainEvent(PlayerInteractTrainEvent event)
	{
		if (event.train.worldObj.isRemote)
		{
			/*
			 * Ensure the player is shift-clicking.
			 */
			if (!event.entityPlayer.isSneaking())
			{
				return;
			}

			/*
			 * Populate set of unfinished parts.
			 */
			final HashSet<String> unfinishedParts = new HashSet<String>();
			{
				event.train.getVehicleParts(new IPartsSelector()
				{
					@Override
					public boolean addToList(VehParBase part)
					{
						/* If part is unfinished, place into the set. */
						if (!part.getIsFinished() && !unfinishedParts.contains(part.getUnlocalizedName()))
						{
							unfinishedParts.add(part.getUnlocalizedName());
						}
						return false;
					}
				});
			}

			/*
			 * Read populated set and post chat messages.
			 */
			if (!unfinishedParts.isEmpty())
			{
				ChatComponentStyle text = new ChatComponentTranslation(ModData.ID + ".text.train.unfinishedParts");
				event.entityPlayer.addChatMessage(text);

				for (String s : unfinishedParts)
				{
					text = new ChatComponentTranslation(s);
					text.setChatStyle(new ChatStyle().setColor(EnumChatFormatting.ITALIC));
					event.entityPlayer.addChatMessage(text);
				}
			}
		}
	}

	@SubscribeEvent
	public void propertyMissingNBTEvent(PropertyMissingNBTEvent event)
	{
		/* If this is a Zora no Densha part, try legacy NBT read calls. */
		if (event.property.getParent().getName().startsWith(ModData.ID))
		{
			/* Skip properties which haven't existed before. */
			for (String s : ignoredProps)
			{
				if (event.nbtKey.endsWith(s))
				{
					return;
				}
			}

			/*
			 * Wrap it all in a huge try-catch block because it's the easiest way to handle exceptions here.
			 */
			try
			{
				/* Some properties used to be incorrectly saved without the "zoranodensha_" prefix. */
				String key = event.nbtKey.substring(event.nbtKey.indexOf("_") + 1);

				/* PropBoolean */
				if (event.property instanceof PropBoolean)
				{
					/* UVID flags used to be called differently. */
					if (event.nbtKey.contains("hasUVID"))
					{
						key = event.nbtKey.replaceAll("hasUVID", "renderUVID");
					}

					if (event.nbtTag.hasKey(key))
					{
						Boolean b = event.nbtTag.getBoolean(key);
						if (event.property.get() == null || !event.property.get().equals(b))
						{
							event.property.set(b);
						}
					}
				}

				/* PropColor */
				else if (event.property instanceof PropColor)
				{
					/* Special case: VehParSeat and VehParCab used to have different names for seat padding color. */
					if (event.property.getParent() instanceof VehParSeat)
					{
						if (key.endsWith("padding"))
						{
							key = event.property.getParent().getClass().getSimpleName() + "color_padding";
						}
					}

					/* Now try to read color data. */
					if (event.nbtTag.hasKey(key))
					{
						//@formatter:off
						int hex = event.nbtTag.getInteger(key);
						((PropColor)event.property).get().setColor((hex >> 16 & 0xFF) / 255.0F,
						                                           (hex >> 8 & 0xFF) / 255.0F,
						                                           (hex >> 0 & 0xFF) / 255.0F);
						//@formatter:on
					}
					else if (event.nbtTag.hasKey(key + "_a"))
					{
						//@formatter:off
						int hex = event.nbtTag.getInteger(key + "_a");
						((PropColor)event.property).get().setColor((hex >> 16 & 0xFF) / 255.0F,
						                                           (hex >> 8 & 0xFF) / 255.0F,
						                                           (hex >> 0 & 0xFF) / 255.0F,
						                                           (hex >> 24 & 0xFF) / 255.0F);
						//@formatter:on
					}

					/* Special case: AVehParBasic used to have a separate alpha channel. */
					if (event.property.getParent() instanceof AVehParBasic)
					{
						key = event.property.getParent().getClass().getSimpleName() + "_alpha";
						if (event.nbtTag.hasKey(key))
						{
							((PropColor)event.property).get().setAlpha(MathHelper.clamp_float(event.nbtTag.getFloat(key), 0.0F, 1.0F));
						}
					}
				}

				/* PropFloat */
				else if (event.property instanceof PropFloat)
				{
					Float f = event.nbtTag.getFloat(key);
					if (event.property.get() == null || !event.property.get().equals(f))
					{
						event.property.set(f);
					}
				}

				/* PropInteger */
				else if (event.property instanceof PropInteger)
				{
					/* Special case: VehParEngienDieselF7 had a typo in the maximum RPM property name. */
					if (key.endsWith("rpmMaximum"))
					{
						key = key.replace("_", "");
					}

					if (event.nbtTag.hasKey(key))
					{
						Integer i = event.nbtTag.getInteger(key);
						if (event.property.get() == null || !event.property.get().equals(i))
						{
							event.property.set(i);
						}
					}
				}

				/* PropString */
				else if (event.property instanceof PropString)
				{
					if (event.nbtTag.hasKey(key))
					{
						String s = event.nbtTag.getString(key);
						if (event.property.get() == null || !event.property.get().equals(s))
						{
							event.property.set(s);
						}
					}
				}
			}
			catch (Exception e)
			{
				/* Silent catch. */
			}
		}
	}

	@SubscribeEvent
	public void trainMiddleClickEvent(TrainMiddleClickEvent event)
	{
		/* Determine which vehicle was hit and copy vehicle data. */
		Train train;
		{
			double hitX = event.movObjPos.hitVec.xCoord;
			double hitY = event.movObjPos.hitVec.yCoord;
			double hitZ = event.movObjPos.hitVec.zCoord;

			VehParBase part = event.train.getVehicleParts(OrderedPartsList.SELECTOR_ANY).getClosestPart(hitX, hitY, hitZ);
			train = new Train(event.train.worldObj, part.getVehicle());
		}

		/* Create vehicle NBT tag. */
		NBTTagCompound nbt;
		{
			nbt = new NBTTagCompound();
			nbt.setBoolean(Train.EDataKey.IS_BLUEPRINT.key, true);
			train.writeEntityToNBT(nbt, ETagCall.ITEM);
		}

		/* Add vehicle item. */
		event.result = new ItemStack(ModCenter.ItemTrain, 1, 0);
		event.result.setTagCompound(nbt);
		event.setResult(Result.ALLOW);
	}

	@SubscribeEvent
	public void trainUpdateSpeedEvent(TrainUpdateSpeedEvent event)
	{
		if (!event.train.worldObj.isRemote && ModCenter.cfg.trains.generalSpeedLimit > 0)
		{
			float limit = ModCenter.cfg.trains.generalSpeedLimit / (3.6F * 20.0F); // [km/h] -> [m/tick]
			if (Math.abs(event.result) > limit)
			{
				event.result = Math.copySign(limit, event.result);
			}
		}
	}

	@SubscribeEvent
	public void trainSpawnEvent(TrainSpawnEvent event)
	{
		/*
		 * Don't interfere with non-player or non-blueprint spawns.
		 */
		if (event.train.worldObj.isRemote)
		{
			return;
		}
		if (event.player == null || event.player.capabilities.isCreativeMode || !VehicleHelper.getIsBlueprint(event.player.getHeldItem()))
		{
			return;
		}

		/*
		 * Check whether the train may spawn.
		 */
		if (!ModCenter.cfg.trains.enableBlueprintSpawnAnyTrack)
		{
			for (PartTypeBogie bogie : event.train.getVehiclePartTypes(OrderedTypesList.SELECTOR_BOGIE_NODUMMY))
			{
				/* Ensure track is a Zora no Densha type track. */
				TrackObj track = PartHelper.getTrackBelow(bogie);
				if (track == null || !(track.block instanceof BlockTrackBase))
				{
					event.setCanceled(true);
					return;
				}

				/* Ensure it has an ITrackBase tile. */
				TileEntity tile = track.world.getTileEntity(track.x, track.y, track.z);
				if (!(tile instanceof ITrackBase))
				{
					event.setCanceled(true);
					return;
				}

				/* Ensure the ITrackBase is a maintenance track. */
				if (!(((ITrackBase)tile).getInstanceOfShape() instanceof ZnD_StraightMaint_0000_0000))
				{
					event.setCanceled(true);
					return;
				}
			}
		}
	}

	@SubscribeEvent
	public void vehicleDropEvent(VehicleDropEvent event)
	{
		/* Precondition check. */
		Train dropTrain = event.train;
		if (dropTrain.worldObj.isRemote || dropTrain.isDead || !ModCenter.cfg.blocks_items.trainItemDrop)
		{
			return;
		}

		/*
		 * 
		 * Isolate the vehicle that was hit from its potentially existing neighbours.
		 * 
		 */
		{
			/* Create backup of the involved train. */
			NBTTagCompound eventTrainNBT = new NBTTagCompound();
			dropTrain.writeToNBT(eventTrainNBT);

			/* Split the train involved if required. */
			int eventTrainVehicles = dropTrain.getVehicleCount();
			if (eventTrainVehicles > 1)
			{
				Train newTrainF = null;
				Train newTrainB = null;

				/* Split off all vehicles in front of the vehicle. */
				if (event.vehicle.getCouplingFront() != null)
				{
					newTrainF = CouplingHandler.INSTANCE.getSplitTrain(event.vehicle.getCouplingFront());
					if (newTrainF != null)
					{
						boolean isSuccessful;
						{
							newTrainF.forceSpawn = true;
							newTrainF.updatePosition();
							isSuccessful = dropTrain.worldObj.spawnEntityInWorld(newTrainF);
							newTrainF.forceSpawn = false;
						}

						if (!isSuccessful)
						{
							/* In case of failure, undo changes to the train and silently abort. */
							dropTrain.readFromNBT(eventTrainNBT);
							return;
						}
					}
				}

				/* Split off all vehicles behind the vehicle. */
				if (event.vehicle.getCouplingBack() != null)
				{
					newTrainB = CouplingHandler.INSTANCE.getSplitTrain(event.vehicle.getCouplingBack());
					if (newTrainB != null)
					{
						boolean isSuccessful;
						{
							newTrainB.forceSpawn = true;
							newTrainB.updatePosition();
							isSuccessful = dropTrain.worldObj.spawnEntityInWorld(newTrainB);
							newTrainB.forceSpawn = false;
						}

						if (!isSuccessful)
						{
							/* In case of failure, undo changes to the train and silently abort. */
							if (newTrainF != null)
							{
								newTrainF.setDead();
							}

							dropTrain.readFromNBT(eventTrainNBT);
							return;
						}
					}
				}
			}

			/* Update the dropped train as it may have changed. Ensure that we are about to drop exactly one vehicle. */
			dropTrain = event.vehicle.getTrain();
			if (dropTrain == null || dropTrain.getVehicleCount() != 1)
			{
				return;
			}

			/* Undo any offsets done to the vehicle. */
			VehicleData vehicle = dropTrain.getVehicleAt(0);
			dropTrain.removeVehicle(vehicle);
			vehicle.popChanges();
			dropTrain.addVehicle(vehicle);
		}

		/*
		 * 
		 * Actually drop the vehicle and its contents, if applicable.
		 * 
		 */
		{
			/* Set entity dead before dropping. */
			dropTrain.setDead();

			/*
			 * Drop freight inventory, if applicable.
			 */
			if (!ModCenter.cfg.trains.enableFreightKeepInventory)
			{
				for (PartTypeTransportItem part : dropTrain.getVehiclePartTypes(OrderedTypesList.SELECTOR_TRANSPORT_ITEM))
				{
					for (int i = part.getSizeInventory() - 1; i >= 0; --i)
					{
						ItemStack droppedStack = part.getStackInSlot(i);
						if (droppedStack != null && droppedStack.stackSize > 0)
						{
							dropTrain.entityDropItem(droppedStack, 0.3F);
						}
						part.setInventorySlotContents(i, null);
					}
				}

				for (PartTypeTransportTank part : dropTrain.getVehiclePartTypes(OrderedTypesList.SELECTOR_TRANSPORT_TANK))
				{
					FluidTankInfo[] tankInfos = part.getTankInfo(ForgeDirection.UNKNOWN);
					if (tankInfos != null)
					{
						for (FluidTankInfo tankInfo : tankInfos)
						{
							part.drain(ForgeDirection.UNKNOWN, tankInfo.fluid, true);
						}
					}
				}
			}

			/* If the vehicle consists only of finished parts, drop the vehicle as a whole. */
			if (dropTrain.isFinished())
			{
				NBTTagCompound nbt = new NBTTagCompound();
				dropTrain.writeEntityToNBT(nbt, ETagCall.ITEM);

				ItemStack itemStack = new ItemStack(ModCenter.ItemTrain, 1);
				itemStack.setTagCompound(nbt);

				event.results.add(itemStack);
				event.setResult(Result.ALLOW);

				return;
			}

			/* Otherwise drop as separate parts. */
			HashMap<String, ItemStack> itemMap = new HashMap<String, ItemStack>();
			ArrayList<ItemStack> items = ModPartRegistry.getPartItemStacks();
			Iterator<ItemStack> iteraItems;
			ItemStack itemStack;
			VehParBase itemPart;
			String name;

			for (VehParBase part : dropTrain.getVehicleParts(OrderedPartsList.SELECTOR_FINISHED))
			{
				name = part.getName();

				if ((itemStack = itemMap.get(name)) != null)
				{
					/* If there is an entry in the itemMap, increase its stack size. */
					++itemStack.stackSize;
					itemMap.put(name, itemStack);
				}
				else
				{
					/* Otherwise, create a new entry: */
					iteraItems = items.iterator();

					while (iteraItems.hasNext())
					{
						/* For each registered ItemStack, check if its IVehiclePart is existent. */
						itemStack = iteraItems.next();
						itemPart = AVehiclePartRegistry.getPartFromItemStack(itemStack);

						if (itemPart != null && name.equals(itemPart.getName()))
						{
							itemMap.put(name, itemStack);
							break;
						}
					}
				}
			}

			event.results.addAll(itemMap.values());
			event.setResult(Result.ALLOW);
		}
	}

	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void vehicleListChangedEvent(VehicleListChangedEvent event)
	{
		if (event.train.worldObj.isRemote)
		{
			/* Initialise vehicle map if necessary. */
			if (VehicleData.globalVehicleMap == null)
			{
				VehicleData.globalVehicleMap = new HashMap<Integer, VehicleData>();
			}

			/* Update the global vehicle map with the updated train's vehicle list data. */
			for (VehicleData vehicle : event.train)
			{
				VehicleData.globalVehicleMap.put(vehicle.getID(), vehicle);
			}
		}
	}
}