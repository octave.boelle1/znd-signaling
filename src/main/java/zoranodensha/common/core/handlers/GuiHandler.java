package zoranodensha.common.core.handlers;

import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import zoranodensha.api.structures.EEngineerTableTab;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.type.PartTypeTransportItem;
import zoranodensha.api.vehicles.part.type.PartTypeTransportTank;
import zoranodensha.client.gui.GUIBlastFurnace;
import zoranodensha.client.gui.GUICokeOven;
import zoranodensha.client.gui.GUIRefinery;
import zoranodensha.client.gui.GUIRetorter;
import zoranodensha.client.gui.GUISignal;
import zoranodensha.client.gui.GUISpeedSign;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.common.blocks.tileEntity.TileEntityBlastFurnace;
import zoranodensha.common.blocks.tileEntity.TileEntityCokeOven;
import zoranodensha.common.blocks.tileEntity.TileEntityEngineerTable;
import zoranodensha.common.blocks.tileEntity.TileEntityRefinery;
import zoranodensha.common.blocks.tileEntity.TileEntityRetorter;
import zoranodensha.common.blocks.tileEntity.TileEntitySignal;
import zoranodensha.common.blocks.tileEntity.TileEntitySpeedSign;
import zoranodensha.common.containers.ContainerBlastFurnace;
import zoranodensha.common.containers.ContainerCokeOven;
import zoranodensha.common.containers.ContainerEngineerTable;
import zoranodensha.common.containers.ContainerRefinery;
import zoranodensha.common.containers.ContainerRetorter;
import zoranodensha.vehicleParts.client.gui.GUIPartTypeTransportItem;
import zoranodensha.vehicleParts.client.gui.GUIPartTypeTransportTank;
import zoranodensha.vehicleParts.common.container.ContainerPartTypeTransportItem;
import zoranodensha.vehicleParts.common.container.ContainerPartTypeTransportTank;



public class GuiHandler implements IGuiHandler
{

	@Override
	public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
	{
		EGui guiType = EGui.fromInt(id);
		if (guiType == null)
		{
			return null;
		}

		TileEntity tileEntity = world.getTileEntity(x, y, z);

		switch (guiType)
		{
			case BLAST_FURNACE: {
				if (tileEntity instanceof TileEntityBlastFurnace)
				{
					return new GUIBlastFurnace(player.inventory, (TileEntityBlastFurnace)tileEntity);
				}
				break;
			}

			case COKE_OVEN: {
				if (tileEntity instanceof TileEntityCokeOven)
				{
					return new GUICokeOven(player.inventory, (TileEntityCokeOven)tileEntity);
				}
				break;
			}

			case ENGINEER_TABLE: {
				if (tileEntity instanceof TileEntityEngineerTable)
				{
					TileEntityEngineerTable tileEntityTable = (TileEntityEngineerTable)tileEntity;
					int usedBy = tileEntityTable.getUsedBy();

					if (usedBy > -1 && usedBy != player.getEntityId())
					{
						return null;
					}

					return new GUIEngineerTable(player.inventory, tileEntityTable);
				}
				break;
			}

			case ENGINEER_TABLE_TRAINS: {
				if (tileEntity instanceof TileEntityEngineerTable)
				{
					TileEntityEngineerTable tileEntityTable = (TileEntityEngineerTable)tileEntity;
					int usedBy = tileEntityTable.getUsedBy();

					if (usedBy > -1 && usedBy != player.getEntityId())
					{
						return null;
					}

					return new GUIEngineerTable(player, tileEntityTable);
				}
				break;
			}

			case REFINERY: {
				if (tileEntity instanceof TileEntityRefinery)
				{
					return new GUIRefinery(player.inventory, (TileEntityRefinery)tileEntity);
				}
				break;
			}

			case RETORTER: {
				if (tileEntity instanceof TileEntityRetorter)
				{
					return new GUIRetorter(player.inventory, (TileEntityRetorter)tileEntity);
				}
				break;
			}

			case SIGNAL: {
				if (tileEntity instanceof TileEntitySignal)
				{
					return new GUISignal(((TileEntitySignal)tileEntity).getSignal());
				}

				break;
			}

			case SPEED_SIGN: {
				if (tileEntity instanceof TileEntitySpeedSign)
				{
					return new GUISpeedSign(((TileEntitySpeedSign)tileEntity).getSpeedSign());
				}
			}

			case VEHPAR_TRANSPORT_ITEM: {
				Entity entity = world.getEntityByID(x);
				if (entity instanceof Train)
				{
					VehParBase part = ((Train)entity).getPartFromID(y, z);
					if (part != null)
					{
						PartTypeTransportItem partTypeTransportItem = part.getPartType(PartTypeTransportItem.class);
						if (partTypeTransportItem != null)
						{
							return new GUIPartTypeTransportItem(player.inventory, partTypeTransportItem);
						}
					}
				}

				break;
			}

			case VEHPAR_TRANSPORT_TANK: {
				Entity entity = world.getEntityByID(x);
				if (entity instanceof Train)
				{
					VehParBase part = ((Train)entity).getPartFromID(y, z);
					if (part != null)
					{
						PartTypeTransportTank partTypeTransportTank = part.getPartType(PartTypeTransportTank.class);
						if (partTypeTransportTank != null)
						{
							return new GUIPartTypeTransportTank(player.inventory, partTypeTransportTank);
						}
					}
				}

				break;
			}
		}

		return null;
	}

	@Override
	public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
	{
		EGui guiType = EGui.fromInt(id);
		if (guiType == null)
		{
			return null;
		}

		TileEntity tileEntity = world.getTileEntity(x, y, z);

		switch (guiType)
		{
			case BLAST_FURNACE: {
				if (tileEntity instanceof TileEntityBlastFurnace)
				{
					return new ContainerBlastFurnace(player.inventory, (TileEntityBlastFurnace)tileEntity);
				}
				break;
			}

			case COKE_OVEN: {
				if (tileEntity instanceof TileEntityCokeOven)
				{
					return new ContainerCokeOven(player.inventory, (TileEntityCokeOven)tileEntity);
				}
				break;
			}

			case ENGINEER_TABLE: {
				if (tileEntity instanceof TileEntityEngineerTable)
				{
					TileEntityEngineerTable tileEntityTable = (TileEntityEngineerTable)tileEntity;
					int entityID = player.getEntityId();
					int usedBy = tileEntityTable.getUsedBy();

					if (usedBy > -1 && usedBy != entityID)
					{
						return null;
					}

					tileEntityTable.setIsUsed(player);

					return new ContainerEngineerTable(player.inventory, tileEntityTable);
				}
				break;
			}

			case ENGINEER_TABLE_TRAINS: {
				if (tileEntity instanceof TileEntityEngineerTable)
				{
					TileEntityEngineerTable tileEntityTable = (TileEntityEngineerTable)tileEntity;
					int entityID = player.getEntityId();
					int usedBy = tileEntityTable.getUsedBy();

					if (usedBy > -1 && usedBy != entityID)
					{
						return null;
					}

					tileEntityTable.setIsUsed(player);
					tileEntityTable.tab = EEngineerTableTab.TRAINS;

					return new ContainerEngineerTable(player.inventory, tileEntityTable);
				}
				break;
			}

			case REFINERY: {
				if (tileEntity instanceof TileEntityRefinery)
				{
					return new ContainerRefinery(player.inventory, (TileEntityRefinery)tileEntity);
				}
				break;
			}

			case RETORTER: {
				if (tileEntity instanceof TileEntityRetorter)
				{
					return new ContainerRetorter(player.inventory, (TileEntityRetorter)tileEntity);
				}
				break;
			}

			case VEHPAR_TRANSPORT_ITEM: {
				Entity entity = world.getEntityByID(x);
				if (entity instanceof Train)
				{
					VehParBase part = ((Train)entity).getPartFromID(y, z);
					if (part != null)
					{
						PartTypeTransportItem partTypeTransportItem = part.getPartType(PartTypeTransportItem.class);
						if (partTypeTransportItem != null)
						{
							return new ContainerPartTypeTransportItem(player.inventory, partTypeTransportItem);
						}
					}
				}
			}
				break;

			case VEHPAR_TRANSPORT_TANK: {
				Entity entity = world.getEntityByID(x);
				if (entity instanceof Train)
				{
					VehParBase part = ((Train)entity).getPartFromID(y, z);
					if (part != null)
					{
						PartTypeTransportTank partTypeTransportTank = part.getPartType(PartTypeTransportTank.class);
						if (partTypeTransportTank != null)
						{
							return new ContainerPartTypeTransportTank(player.inventory, partTypeTransportTank);
						}
					}
				}
			}
				break;
		}

		return null;
	}
}
