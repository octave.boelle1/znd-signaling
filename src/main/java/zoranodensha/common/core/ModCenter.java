package zoranodensha.common.core;

import java.io.File;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.audio.SoundRegistry;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.item.ItemStack;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.oredict.OreDictionary;
import paulscode.sound.SoundSystemConfig;
import zoranodensha.api.util.APILogger;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.packet.APropertyMessage;
import zoranodensha.api.vehicles.packet.TrainMessage_RayTrace;
import zoranodensha.api.vehicles.packet.TrainMessage_UpdateVehicles;
import zoranodensha.api.vehicles.part.type.cab.CommandAnnouncement;
import zoranodensha.api.vehicles.part.type.cab.CommandDestination;
import zoranodensha.api.vehicles.part.type.seat.EntitySeat;
import zoranodensha.common.blocks.BlockBallastConcrete;
import zoranodensha.common.blocks.BlockBallastGravel;
import zoranodensha.common.blocks.BlockBallastGravelCorner;
import zoranodensha.common.blocks.BlockBlastFurnace;
import zoranodensha.common.blocks.BlockCoalCoke;
import zoranodensha.common.blocks.BlockCokeOven;
import zoranodensha.common.blocks.BlockEngineerTable;
import zoranodensha.common.blocks.BlockFluid;
import zoranodensha.common.blocks.BlockFluidFlammable;
import zoranodensha.common.blocks.BlockFluidTar;
import zoranodensha.common.blocks.BlockOreShale;
import zoranodensha.common.blocks.BlockPlatform;
import zoranodensha.common.blocks.BlockRailwaySign;
import zoranodensha.common.blocks.BlockRefinery;
import zoranodensha.common.blocks.BlockRetorter;
import zoranodensha.common.blocks.BlockSignal;
import zoranodensha.common.blocks.BlockSpeedSign;
import zoranodensha.common.blocks.BlockSteel;
import zoranodensha.common.blocks.BlockTar;
import zoranodensha.common.blocks.BlockTrackBase;
import zoranodensha.common.blocks.BlockTrackBaseGag;
import zoranodensha.common.blocks.BlockTrackDecoration;
import zoranodensha.common.blocks.material.MaterialBallast;
import zoranodensha.common.blocks.material.MaterialFluid;
import zoranodensha.common.blocks.material.MaterialTrack;
import zoranodensha.common.blocks.tileEntity.TileEntityBallastGravel;
import zoranodensha.common.blocks.tileEntity.TileEntityBlastFurnace;
import zoranodensha.common.blocks.tileEntity.TileEntityCokeOven;
import zoranodensha.common.blocks.tileEntity.TileEntityEngineerTable;
import zoranodensha.common.blocks.tileEntity.TileEntityItemFluidHandler;
import zoranodensha.common.blocks.tileEntity.TileEntityPlatform;
import zoranodensha.common.blocks.tileEntity.TileEntityRailwaySign;
import zoranodensha.common.blocks.tileEntity.TileEntityRefinery;
import zoranodensha.common.blocks.tileEntity.TileEntityRetorter;
import zoranodensha.common.blocks.tileEntity.TileEntitySignal;
import zoranodensha.common.blocks.tileEntity.TileEntitySpeedSign;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBaseGag;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackDecoration;
import zoranodensha.common.core.cfg.ModConfig;
import zoranodensha.common.core.compat.RailcraftCompat;
import zoranodensha.common.core.handlers.FuelHandler;
import zoranodensha.common.core.handlers.GuiHandler;
import zoranodensha.common.entity.SCP4633;
import zoranodensha.common.fluids.FluidFuelDiesel;
import zoranodensha.common.fluids.FluidFuelPetrol;
import zoranodensha.common.fluids.FluidOilBioCrude;
import zoranodensha.common.fluids.FluidOilCreosote;
import zoranodensha.common.fluids.FluidOilLubricant;
import zoranodensha.common.fluids.FluidOilMachinery;
import zoranodensha.common.fluids.FluidOilShaleCrude;
import zoranodensha.common.fluids.FluidTar;
import zoranodensha.common.items.ItemArmorJacket;
import zoranodensha.common.items.ItemArmorVest;
import zoranodensha.common.items.ItemArmorVest.EVest;
import zoranodensha.common.items.ItemBlockCoalCoke;
import zoranodensha.common.items.ItemBlockPlatform;
import zoranodensha.common.items.ItemBucketFluid;
import zoranodensha.common.items.ItemCoalCoke;
import zoranodensha.common.items.ItemCrowbar;
import zoranodensha.common.items.ItemDriverKey;
import zoranodensha.common.items.ItemEngineerTableTracks;
import zoranodensha.common.items.ItemEngineersBlueprint;
import zoranodensha.common.items.ItemIngotSteel;
import zoranodensha.common.items.ItemMachineKit;
import zoranodensha.common.items.ItemNuggetSteel;
import zoranodensha.common.items.ItemOilShale;
import zoranodensha.common.items.ItemPart;
import zoranodensha.common.items.ItemPlastic;
import zoranodensha.common.items.ItemPlatePlastic;
import zoranodensha.common.items.ItemPlateSteel;
import zoranodensha.common.items.ItemRailwaySign;
import zoranodensha.common.items.ItemRailwayTool;
import zoranodensha.common.items.ItemSignal;
import zoranodensha.common.items.ItemSignalTool;
import zoranodensha.common.items.ItemSpeedSign;
import zoranodensha.common.items.ItemTrackPart;
import zoranodensha.common.items.ItemTrain;
import zoranodensha.common.items.ItemVehiclePart;
import zoranodensha.common.network.packet.PacketChatMessage;
import zoranodensha.common.network.packet.PacketMinecartUpdate;
import zoranodensha.common.network.packet.gui.PacketGUIEngineerTable;
import zoranodensha.common.network.packet.gui.PacketGUIEngineerTablePreset;
import zoranodensha.common.network.packet.gui.PacketGUISignal;
import zoranodensha.common.network.packet.gui.PacketGUISpeedSign;
import zoranodensha.common.oregen.OreGeneration;

import static zoranodensha.common.fluids.EFluid.*;



@Mod(modid = ModData.ID, name = ModData.NAME, version = ModData.VERSION, guiFactory = ModData.ID + ".client.gui.config.GuiFactory")
public class ModCenter
{

	@Instance(ModData.ID) public static ModCenter instance;

	/**
	 * A {@link java.io.File File} reference to the add-ons directory. May be {@code null}; initialised during {@link #preInit(FMLPreInitializationEvent) preInit()}.
	 */
	public static File DIR_ADDONS;
	public static final String DIR_RESOURCES_TRACKS = "models/tracks/";
	public static final String DIR_RESOURCES_VEHPAR = "models/vehicleParts/";

	@SidedProxy(clientSide = ModData.ID + ".client.ModProxyClient", serverSide = ModData.ID + ".common.core.ModProxyCommon") public static ModProxyCommon proxy;

	public static Logger log;
	public static ModConfig cfg;
	public static SimpleNetworkWrapper snw;

	public static FluidFuelDiesel FluidFuelDiesel;
	public static FluidFuelPetrol FluidFuelPetrol;
	public static FluidOilBioCrude FluidOilBioCrude;
	public static FluidOilCreosote FluidOilCreosote;
	public static FluidOilLubricant FluidOilLubricant;
	public static FluidOilMachinery FluidOilMachinery;
	public static FluidOilShaleCrude FluidOilShaleCrude;
	public static FluidTar FluidTar;

	public static MaterialBallast Material_Ballast;
	public static MaterialFluid Material_Fluid;
	public static MaterialTrack Material_Track;

	public static BlockBallastConcrete BlockBallastConcrete;
	public static BlockBallastGravel BlockBallastGravel;
	public static BlockBallastGravelCorner BlockBallastGravelCorner;
	public static BlockBlastFurnace BlockBlastFurnace;
	public static BlockCoalCoke BlockCoalCoke;
	public static BlockCokeOven BlockCokeOven;
	public static BlockEngineerTable BlockEngineerTable;
	public static BlockFluid BlockFluidFuelDiesel;
	public static BlockFluid BlockFluidFuelPetrol;
	public static BlockFluid BlockFluidOilBioCrude;
	public static BlockFluid BlockFluidOilCreosote;
	public static BlockFluid BlockFluidOilLubricant;
	public static BlockFluid BlockFluidOilMachinery;
	public static BlockFluid BlockFluidOilShaleCrude;
	public static BlockFluid BlockFluidTar;
	public static BlockOreShale BlockOreShale;
	public static BlockPlatform BlockPlatform;
	public static BlockRailwaySign BlockRailwaySign;
	public static BlockRefinery BlockRefinery;
	public static BlockRetorter BlockRetorter;
	public static BlockSignal BlockSignal;
	public static BlockSpeedSign BlockSpeedSign;
	public static BlockSteel BlockSteel;
	public static BlockTar BlockTar;
	public static BlockTrackBase BlockTrackBase;
	public static BlockTrackBaseGag BlockTrackBaseGag;
	public static BlockTrackDecoration BlockTrackDecoration;

	public static ItemArmorJacket ItemArmorJacketOrange;
	public static ItemArmorJacket ItemArmorJacketYellow;
	public static ItemArmorVest ItemArmorVestOrange;
	public static ItemArmorVest ItemArmorVestYellow;

	public static ItemBucketFluid ItemBucketFuelDiesel;
	public static ItemBucketFluid ItemBucketFuelPetrol;
	public static ItemBucketFluid ItemBucketOilBioCrude;
	public static ItemBucketFluid ItemBucketOilCreosote;
	public static ItemBucketFluid ItemBucketOilLubricant;
	public static ItemBucketFluid ItemBucketOilMachinery;
	public static ItemBucketFluid ItemBucketOilShaleCrude;
	public static ItemBucketFluid ItemBucketTar;
	public static ItemCoalCoke ItemCoalCoke;
	public static ItemCrowbar ItemCrowbar;
	public static ItemDriverKey ItemDriverKey;
	public static ItemEngineersBlueprint ItemEngineersBlueprint;
	public static ItemEngineerTableTracks ItemEngineerTableTracks;
	public static ItemIngotSteel ItemIngotSteel;
	public static ItemMachineKit ItemMachineKit;
	public static ItemNuggetSteel ItemNuggetSteel;
	public static ItemOilShale ItemOilShale;
	public static ItemPlastic ItemPlastic;
	public static ItemPart ItemPart;
	public static ItemPlatePlastic ItemPlatePlastic;
	public static ItemPlateSteel ItemPlateSteel;
	public static ItemRailwaySign ItemRailwaySign;
	public static ItemRailwayTool ItemRailwayTool;
	public static ItemSignal ItemSignal;
	public static ItemSignalTool ItemSignalTool;
	public static ItemSpeedSign ItemSpeedSign;
	public static ItemTrackPart ItemTrackPart;
	public static ItemTrain ItemTrain;
	public static ItemVehiclePart ItemVehiclePart;


	@EventHandler
	public void mainInit(FMLInitializationEvent event)
	{


		/* Register recipes. */
		ItemArmorVestOrange.registerRecipes();
		ItemArmorVestYellow.registerRecipes();
		ItemArmorJacketOrange.registerRecipes();
		ItemCoalCoke.registerRecipes();
		ItemCrowbar.registerRecipes();
		ItemDriverKey.registerRecipes();
		ItemIngotSteel.registerRecipes();
		ItemMachineKit.registerRecipes();
		ItemNuggetSteel.registerRecipes();
		ItemOilShale.registerRecipes();
		ItemPlastic.registerRecipes();
		ItemPart.registerRecipes();
		ItemPlatePlastic.registerRecipes();
		ItemPlateSteel.registerRecipes();
		ItemRailwaySign.registerRecipes();
		ItemRailwayTool.registerRecipes();
		ItemSignalTool.registerRecipes();
		ItemTrackPart.registerRecipes();

		BlockBallastConcrete.registerRecipes();
		BlockBallastGravel.registerRecipes();
		BlockBlastFurnace.registerRecipes();
		BlockCoalCoke.registerRecipes();
		BlockCokeOven.registerRecipes();
		BlockEngineerTable.registerRecipes();
		BlockOreShale.registerRecipes();
		BlockPlatform.registerRecipes();
		BlockRailwaySign.registerRecipes();
		BlockSteel.registerRecipes();
		BlockTar.registerRecipes();

		/* OreDictionary. */
		OreDictionary.registerOre("bucketCreosote", ItemBucketOilCreosote);

		/* Activate compatibility integration. */
		if (cfg.compat.isLoaded_Railcraft)
		{
			RailcraftCompat.registerBallast();
			RailcraftCompat.registerMachineRecipes();
			RailcraftCompat.registerOther();
		}

		/* Register handlers and renderers. */
		GameRegistry.registerFuelHandler(new FuelHandler());
		NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());
		proxy.mainInit(event);

		/* Register packets */
		snw = NetworkRegistry.INSTANCE.newSimpleChannel(ModData.ID);
		int packetID = APropertyMessage.init(snw);

		snw.registerMessage(PacketChatMessage.Handler.class, PacketChatMessage.class, ++packetID, Side.CLIENT);
		snw.registerMessage(PacketGUIEngineerTable.Handler.class, PacketGUIEngineerTable.class, ++packetID, Side.SERVER);
		snw.registerMessage(PacketGUIEngineerTablePreset.Handler.class, PacketGUIEngineerTablePreset.class, ++packetID, Side.SERVER);
		snw.registerMessage(PacketMinecartUpdate.Handler.class, PacketMinecartUpdate.class, ++packetID, Side.CLIENT);
		snw.registerMessage(PacketGUISignal.Handler.class, PacketGUISignal.class, ++packetID, Side.SERVER);
		snw.registerMessage(PacketGUISpeedSign.Handler.class, PacketGUISpeedSign.class, ++packetID, Side.SERVER);

		snw.registerMessage(TrainMessage_RayTrace.Handler.class, TrainMessage_RayTrace.class, ++packetID, Side.SERVER);
		snw.registerMessage(TrainMessage_UpdateVehicles.Handler.class, TrainMessage_UpdateVehicles.class, ++packetID, Side.CLIENT);
	}


	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		ForgeChunkManager.setForcedChunkLoadingCallback(instance, new ModChunkloadCallback());

		proxy.postInit(event);
	}


	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		log = FMLLog.getLogger();
		cfg = ModConfig.init(event.getSuggestedConfigurationFile());
		APILogger.overrideLogger(log);

		File dir = event.getSourceFile();
		while (!dir.isDirectory())
		{
			dir = dir.getParentFile();
		}

		DIR_ADDONS = new File(dir, ModData.NAME + "/");
		if (!DIR_ADDONS.exists())
		{
			DIR_ADDONS.mkdirs();
		}

		FluidFuelDiesel = new FluidFuelDiesel();
		FluidFuelPetrol = new FluidFuelPetrol();
		FluidOilBioCrude = new FluidOilBioCrude();
		FluidOilCreosote = new FluidOilCreosote();
		FluidOilLubricant = new FluidOilLubricant();
		FluidOilMachinery = new FluidOilMachinery();
		FluidOilShaleCrude = new FluidOilShaleCrude();
		FluidTar = new FluidTar();

		FluidRegistry.registerFluid(FluidFuelDiesel);
		FluidRegistry.registerFluid(FluidFuelPetrol);
		FluidRegistry.registerFluid(FluidOilBioCrude);
		FluidRegistry.registerFluid(FluidOilCreosote);
		FluidRegistry.registerFluid(FluidOilLubricant);
		FluidRegistry.registerFluid(FluidOilMachinery);
		FluidRegistry.registerFluid(FluidOilShaleCrude);
		FluidRegistry.registerFluid(FluidTar);

		Material_Ballast = new MaterialBallast();
		Material_Fluid = new MaterialFluid();
		Material_Track = new MaterialTrack();

		BlockBallastConcrete = new BlockBallastConcrete();
		BlockBallastGravel = new BlockBallastGravel();
		BlockBallastGravelCorner = new BlockBallastGravelCorner();
		BlockBlastFurnace = new BlockBlastFurnace();
		BlockCoalCoke = new BlockCoalCoke();
		BlockCokeOven = new BlockCokeOven();
		BlockEngineerTable = new BlockEngineerTable();
		BlockFluidFuelDiesel = new BlockFluidFlammable(FluidFuelDiesel, DIESEL);
		BlockFluidFuelPetrol = new BlockFluidFlammable(FluidFuelPetrol, PETROL);
		BlockFluidOilBioCrude = new BlockFluid(FluidOilBioCrude, BIO_CRUDE);
		BlockFluidOilCreosote = new BlockFluidFlammable(FluidOilCreosote, CREOSOTE);
		BlockFluidOilLubricant = new BlockFluid(FluidOilLubricant, LUBRICANT);
		BlockFluidOilMachinery = new BlockFluid(FluidOilMachinery, MACHINERY);
		BlockFluidOilShaleCrude = new BlockFluid(FluidOilShaleCrude, SHALE_CRUDE);
		BlockFluidTar = new BlockFluidTar(FluidTar, TAR);
		BlockOreShale = new BlockOreShale();
		BlockPlatform = new BlockPlatform();
		BlockRailwaySign = new BlockRailwaySign();
		BlockRefinery = new BlockRefinery();
		BlockRetorter = new BlockRetorter();
		BlockSignal = new BlockSignal();
		BlockSpeedSign = new BlockSpeedSign();
		BlockSteel = new BlockSteel();
		BlockTar = new BlockTar();
		BlockTrackBase = new BlockTrackBase();
		BlockTrackBaseGag = new BlockTrackBaseGag();
		BlockTrackDecoration = new BlockTrackDecoration();

		GameRegistry.registerBlock(BlockBallastConcrete, BlockBallastConcrete.getUnlocalizedName());
		GameRegistry.registerBlock(BlockBallastGravel, BlockBallastGravel.getUnlocalizedName());
		GameRegistry.registerBlock(BlockBallastGravelCorner, BlockBallastGravelCorner.getUnlocalizedName());
		GameRegistry.registerBlock(BlockBlastFurnace, BlockBlastFurnace.getUnlocalizedName());
		GameRegistry.registerBlock(BlockCoalCoke, ItemBlockCoalCoke.class, BlockCoalCoke.getUnlocalizedName());
		GameRegistry.registerBlock(BlockCokeOven, BlockCokeOven.getUnlocalizedName());
		GameRegistry.registerBlock(BlockEngineerTable, BlockEngineerTable.getUnlocalizedName());
		GameRegistry.registerBlock(BlockFluidFuelDiesel, BlockFluidFuelDiesel.getUnlocalizedName());
		GameRegistry.registerBlock(BlockFluidFuelPetrol, BlockFluidFuelPetrol.getUnlocalizedName());
		GameRegistry.registerBlock(BlockFluidOilBioCrude, BlockFluidOilBioCrude.getUnlocalizedName());
		GameRegistry.registerBlock(BlockFluidOilCreosote, BlockFluidOilCreosote.getUnlocalizedName());
		GameRegistry.registerBlock(BlockFluidOilLubricant, BlockFluidOilLubricant.getUnlocalizedName());
		GameRegistry.registerBlock(BlockFluidOilMachinery, BlockFluidOilMachinery.getUnlocalizedName());
		GameRegistry.registerBlock(BlockFluidOilShaleCrude, BlockFluidOilShaleCrude.getUnlocalizedName());
		GameRegistry.registerBlock(BlockFluidTar, BlockFluidTar.getUnlocalizedName());
		GameRegistry.registerBlock(BlockOreShale, BlockOreShale.getUnlocalizedName());
		GameRegistry.registerBlock(BlockPlatform, ItemBlockPlatform.class, BlockPlatform.getUnlocalizedName());
		GameRegistry.registerBlock(BlockRailwaySign, BlockRailwaySign.getUnlocalizedName());
		GameRegistry.registerBlock(BlockRefinery, BlockRefinery.getUnlocalizedName());
		GameRegistry.registerBlock(BlockRetorter, BlockRetorter.getUnlocalizedName());
		GameRegistry.registerBlock(BlockSignal, BlockSignal.getUnlocalizedName());
		GameRegistry.registerBlock(BlockSpeedSign, BlockSpeedSign.getUnlocalizedName());
		GameRegistry.registerBlock(BlockSteel, BlockSteel.getUnlocalizedName());
		GameRegistry.registerBlock(BlockTar, BlockTar.getUnlocalizedName());
		GameRegistry.registerBlock(BlockTrackBase, BlockTrackBase.getUnlocalizedName());
		GameRegistry.registerBlock(BlockTrackBaseGag, BlockTrackBaseGag.getUnlocalizedName());
		GameRegistry.registerBlock(BlockTrackDecoration, BlockTrackDecoration.getUnlocalizedName());

		GameRegistry.registerTileEntity(TileEntityBallastGravel.class, ModData.ID + ".tileEntity.ballastGravel");
		GameRegistry.registerTileEntity(TileEntityBlastFurnace.class, ModData.ID + ".tileEntity.blastFurnace");
		GameRegistry.registerTileEntity(TileEntityCokeOven.class, ModData.ID + ".tileEntity.cokeOven");
		GameRegistry.registerTileEntity(TileEntityEngineerTable.class, ModData.ID + ".tileEntity.engineerTable");
		GameRegistry.registerTileEntity(TileEntityItemFluidHandler.class, ModData.ID + ".tileEntity.itemFluidHandler");
		GameRegistry.registerTileEntity(TileEntityPlatform.class, ModData.ID + ".tileEntity.platform");
		GameRegistry.registerTileEntity(TileEntityRailwaySign.class, ModData.ID + ".tileEntity.railwaySign");
		GameRegistry.registerTileEntity(TileEntityRefinery.class, ModData.ID + ".tileEntity.refinery");
		GameRegistry.registerTileEntity(TileEntityRetorter.class, ModData.ID + ".tileEntity.retorter");
		GameRegistry.registerTileEntity(TileEntitySignal.class, ModData.ID + ".tileEntity.signal");
		GameRegistry.registerTileEntity(TileEntitySpeedSign.class, ModData.ID + ".tileEntity.speedSign");
		GameRegistry.registerTileEntity(TileEntityTrackBase.class, ModData.ID + ".tileEntity.trackBase");
		GameRegistry.registerTileEntity(TileEntityTrackBaseGag.class, ModData.ID + ".tileEntity.trackBaseGag");
		GameRegistry.registerTileEntity(TileEntityTrackDecoration.class, ModData.ID + ".tileEntity.trackDecoration");

		ItemArmorJacketOrange = new ItemArmorJacket(EVest.ORANGE);
		ItemArmorJacketYellow = new ItemArmorJacket(EVest.YELLOW);
		ItemArmorVestOrange = new ItemArmorVest(ItemArmorVest.EVest.ORANGE);
		ItemArmorVestYellow = new ItemArmorVest(ItemArmorVest.EVest.YELLOW);
		ItemBucketFuelDiesel = new ItemBucketFluid(BlockFluidFuelDiesel, DIESEL);
		ItemBucketFuelPetrol = new ItemBucketFluid(BlockFluidFuelPetrol, PETROL);
		ItemBucketOilBioCrude = new ItemBucketFluid(BlockFluidOilBioCrude, BIO_CRUDE);
		ItemBucketOilCreosote = new ItemBucketFluid(BlockFluidOilCreosote, CREOSOTE);
		ItemBucketOilLubricant = new ItemBucketFluid(BlockFluidOilLubricant, LUBRICANT);
		ItemBucketOilMachinery = new ItemBucketFluid(BlockFluidOilMachinery, MACHINERY);
		ItemBucketOilShaleCrude = new ItemBucketFluid(BlockFluidOilShaleCrude, SHALE_CRUDE);
		ItemBucketTar = new ItemBucketFluid(BlockFluidTar, TAR);
		ItemCoalCoke = new ItemCoalCoke();
		ItemCrowbar = new ItemCrowbar();
		ItemDriverKey = new ItemDriverKey();
		ItemEngineersBlueprint = new ItemEngineersBlueprint();
		ItemEngineerTableTracks = new ItemEngineerTableTracks();
		ItemIngotSteel = new ItemIngotSteel();
		ItemMachineKit = new ItemMachineKit();
		ItemNuggetSteel = new ItemNuggetSteel();
		ItemOilShale = new ItemOilShale();
		ItemPlastic = new ItemPlastic();
		ItemPart = new ItemPart();
		ItemPlatePlastic = new ItemPlatePlastic();
		ItemPlateSteel = new ItemPlateSteel();
		ItemSignal = new ItemSignal();
		ItemSignalTool = new ItemSignalTool();
		ItemSpeedSign = new ItemSpeedSign();
		ItemTrain = new ItemTrain();
		ItemRailwaySign = new ItemRailwaySign();
		ItemRailwayTool = new ItemRailwayTool();
		ItemTrackPart = new ItemTrackPart();
		ItemVehiclePart = new ItemVehiclePart();

		GameRegistry.registerItem(ItemArmorJacketOrange, ItemArmorJacketOrange.getUnlocalizedName());
		GameRegistry.registerItem(ItemArmorJacketYellow, ItemArmorJacketYellow.getUnlocalizedName());
		GameRegistry.registerItem(ItemArmorVestOrange, ItemArmorVestOrange.getUnlocalizedName());
		GameRegistry.registerItem(ItemArmorVestYellow, ItemArmorVestYellow.getUnlocalizedName());
		GameRegistry.registerItem(ItemBucketFuelDiesel, ItemBucketFuelDiesel.getUnlocalizedName());
		GameRegistry.registerItem(ItemBucketFuelPetrol, ItemBucketFuelPetrol.getUnlocalizedName());
		GameRegistry.registerItem(ItemBucketOilBioCrude, ItemBucketOilBioCrude.getUnlocalizedName());
		GameRegistry.registerItem(ItemBucketOilCreosote, ItemBucketOilCreosote.getUnlocalizedName());
		GameRegistry.registerItem(ItemBucketOilLubricant, ItemBucketOilLubricant.getUnlocalizedName());
		GameRegistry.registerItem(ItemBucketOilMachinery, ItemBucketOilMachinery.getUnlocalizedName());
		GameRegistry.registerItem(ItemBucketOilShaleCrude, ItemBucketOilShaleCrude.getUnlocalizedName());
		GameRegistry.registerItem(ItemBucketTar, ItemBucketTar.getUnlocalizedName());
		GameRegistry.registerItem(ItemCoalCoke, ItemCoalCoke.getUnlocalizedName());
		GameRegistry.registerItem(ItemCrowbar, ItemCrowbar.getUnlocalizedName());
		GameRegistry.registerItem(ItemDriverKey, ItemDriverKey.getUnlocalizedName());
		GameRegistry.registerItem(ItemEngineersBlueprint, ItemEngineersBlueprint.getUnlocalizedName());
		GameRegistry.registerItem(ItemEngineerTableTracks, ItemEngineerTableTracks.getUnlocalizedName());
		GameRegistry.registerItem(ItemIngotSteel, ItemIngotSteel.getUnlocalizedName());
		GameRegistry.registerItem(ItemMachineKit, ItemMachineKit.getUnlocalizedName());
		GameRegistry.registerItem(ItemNuggetSteel, ItemNuggetSteel.getUnlocalizedName());
		GameRegistry.registerItem(ItemOilShale, ItemOilShale.getUnlocalizedName());
		GameRegistry.registerItem(ItemPlastic, ItemPlastic.getUnlocalizedName());
		GameRegistry.registerItem(ItemPart, ItemPart.getUnlocalizedName());
		GameRegistry.registerItem(ItemPlatePlastic, ItemPlatePlastic.getUnlocalizedName());
		GameRegistry.registerItem(ItemPlateSteel, ItemPlateSteel.getUnlocalizedName());
		GameRegistry.registerItem(ItemSignal, ItemSignal.getUnlocalizedName());
		GameRegistry.registerItem(ItemSignalTool, ItemSignalTool.getUnlocalizedName());
		GameRegistry.registerItem(ItemSpeedSign, ItemSpeedSign.getUnlocalizedName());
		GameRegistry.registerItem(ItemTrain, ItemTrain.getUnlocalizedName());
		GameRegistry.registerItem(ItemRailwaySign, ItemRailwaySign.getUnlocalizedName());
		GameRegistry.registerItem(ItemRailwayTool, ItemRailwayTool.getUnlocalizedName());
		GameRegistry.registerItem(ItemTrackPart, ItemTrackPart.getUnlocalizedName());
		GameRegistry.registerItem(ItemVehiclePart, ItemVehiclePart.getUnlocalizedName());

		FluidContainerRegistry.registerFluidContainer(FluidFuelDiesel, new ItemStack(ItemBucketFuelDiesel), FluidContainerRegistry.EMPTY_BUCKET);
		FluidContainerRegistry.registerFluidContainer(FluidFuelPetrol, new ItemStack(ItemBucketFuelPetrol), FluidContainerRegistry.EMPTY_BUCKET);
		FluidContainerRegistry.registerFluidContainer(FluidOilBioCrude, new ItemStack(ItemBucketOilBioCrude), FluidContainerRegistry.EMPTY_BUCKET);
		FluidContainerRegistry.registerFluidContainer(FluidOilCreosote, new ItemStack(ItemBucketOilCreosote), FluidContainerRegistry.EMPTY_BUCKET);
		FluidContainerRegistry.registerFluidContainer(FluidOilLubricant, new ItemStack(ItemBucketOilLubricant), FluidContainerRegistry.EMPTY_BUCKET);
		FluidContainerRegistry.registerFluidContainer(FluidOilMachinery, new ItemStack(ItemBucketOilMachinery), FluidContainerRegistry.EMPTY_BUCKET);
		FluidContainerRegistry.registerFluidContainer(FluidOilShaleCrude, new ItemStack(ItemBucketOilShaleCrude), FluidContainerRegistry.EMPTY_BUCKET);
		FluidContainerRegistry.registerFluidContainer(FluidTar, new ItemStack(ItemBucketTar), FluidContainerRegistry.EMPTY_BUCKET);

		/*
		 * == NOTE ==
		 * ModData.ID + ".entity.train" is actually a wrong entity name, as Forge for some reason turns it into
		 * "zoranodensha.zoranodensha.entity.train" which is obviously wrong. However, we need to keep this registry key
		 * in order to avoid purging trains from users' worlds..
		 */
		EntityRegistry.registerModEntity(Train.class, ModData.ID + ".entity.train", 0, this, 512, 8, false);
		EntityRegistry.registerModEntity(EntitySeat.class, "entity.seat", 1, this, 512, 8, false);

		if (cfg.easterEggs.enableHalloween)
		{
			EntityRegistry.registerModEntity(SCP4633.class, "entity.scp", 2, this, 128, 1, true);
			EntityRegistry.addSpawn(ModData.ID + ".entity.scp", 3, 1, 1, EnumCreatureType.monster, BiomeGenBase.forest, BiomeGenBase.plains, BiomeGenBase.jungle);
		}

		GameRegistry.registerWorldGenerator(new OreGeneration(), 0);
	}

	@EventHandler
	public void serverInit(FMLServerStartingEvent event)
	{
		event.registerServerCommand(new CommandAnnouncement());
		event.registerServerCommand(new CommandDestination());
		event.registerServerCommand(ItemSignal.new BingBongCommand());
	}
}