package zoranodensha.common.core;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.ChunkCoordIntPair;
import net.minecraft.world.World;
import net.minecraft.world.WorldSavedData;
import net.minecraftforge.common.ForgeChunkManager;
import net.minecraftforge.common.ForgeChunkManager.OrderedLoadingCallback;
import net.minecraftforge.common.ForgeChunkManager.Ticket;



public class ModChunkloadCallback implements OrderedLoadingCallback
{
	/*
	 * Ticket Mod Data Information
	 * 
	 * chunkX -> int
	 * chunkZ -> int
	 * train -> int, entity ID of the train that registered it.
	 */

	@Override
	public void ticketsLoaded(List<Ticket> tickets, World world)
	{
		for (Ticket ticket : tickets)
		{
			NBTTagCompound ticketData = ticket.getModData();

			int chunkPositionX = ticketData.getInteger("chunkX");
			int chunkPositionZ = ticketData.getInteger("chunkZ");

			ForgeChunkManager.forceChunk(ticket, new ChunkCoordIntPair(chunkPositionX, chunkPositionZ));
		}
	}


	/**
	 * <p>
	 * In this method, Forge gives us a list of chunk tickets that were in-use during the previous session of this world.
	 * We need to go through all the tickets and determine if they should be kept and have their chunks re-loaded, or discarded.
	 * </p>
	 * <p>
	 * Every ticket that is loaded by Zora no Densha contains an integer which is the Entity ID of the train that loaded it. We use that information in this method, along with information stored in
	 * the {@link TrainDatabase} to check if each ticket should remain or be discarded. The risk of discarding a ticket when it should actually be kept is that any driverless trains in unloaded chunks
	 * may become stuck in those unloaded chunks because their previously-used chunk loading tickets don't actually survive the world reload. So it's critical that in this method they are preserved so
	 * any trains are always kept in loaded chunks.
	 * </p>
	 */
	@Override
	public List<Ticket> ticketsLoaded(List<Ticket> tickets, World world, int maxTicketCount)
	{
		TrainDatabase trainDatabase = TrainDatabase.getTrainDatabase(world);

		/*
		 * Create a new list to store the tickets that we are going to keep, and then re-load.
		 */
		ArrayList<Ticket> finalTickets = new ArrayList<Ticket>();

		/*
		 * Keep track of how many tickets we don't keep.
		 */
		int droppedTickets = 0;
		boolean goodTicket = false;

		/*
		 * Go through all the tickets given to us by Forge and decide if we should keep them or discard them.
		 */
		for (Ticket ticket : tickets)
		{
			/*
			 * We can get the train ID that registered the ticket by accessing the extra information stored in the ticket.
			 * We can use this train ID to determine if the ticket should be kept.
			 */
			int trainEntityID = ticket.getModData().getInteger("train");
			long worldTime = world.getTotalWorldTime();
			long trainTime;

			if (trainDatabase.getAutodriveTrainExists(trainEntityID))
			{
				trainTime = trainDatabase.getAutodriveTrainTime(trainEntityID);

				if (worldTime - trainTime < 20)
				{
					goodTicket = true;
				}
			}

			if (goodTicket)
			{
				if (finalTickets.size() < maxTicketCount)
				{
					finalTickets.add(ticket);
				}
				else
				{
					droppedTickets++;
				}
			}
			else
			{
				droppedTickets++;
			}
		}

		ModCenter.log.printf(Level.INFO, "%s is keeping %d chunks loaded from the previous session, after discarding %d.", ModData.NAME, finalTickets.size(), droppedTickets);

		return finalTickets;
	}

}
