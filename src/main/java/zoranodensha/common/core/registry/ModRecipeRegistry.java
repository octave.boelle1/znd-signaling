package zoranodensha.common.core.registry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.structures.IEngineerTableExtension.EngineerTableIngredientHelper;
import zoranodensha.api.structures.tracks.EDirection;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.util.Helpers;



/**
 * The registry for EngineerTable recipesTracks.
 */
public abstract class ModRecipeRegistry
{
	/** The list of all registered track-recipe shapes. */
	private static List<EngineerTableRecipe> recipesTracks = new ArrayList<EngineerTableRecipe>();

	/** The list of all registered track-recipe ingredients. */
	private static HashMap<String, ItemStack> ingredientsTracks = new HashMap<String, ItemStack>();



	/**
	 * Returns a new NBTTagCompound that contains the given label of an given instance, e.g. an instance of RailwaySection for a track.
	 */
	public static NBTTagCompound getBlueprintNBT(int type, Object... params)
	{
		if (params == null || params.length == 0)
		{
			return null;
		}

		NBTTagCompound nbt = new NBTTagCompound();

		switch (type)
		{
			case 0:
				nbt.setString("Type", "RailwaySection");

				for (Object obj : params)
				{
					if (obj instanceof IRailwaySection)
					{
						IRailwaySection section = (IRailwaySection)obj;
						nbt.setString("Instance", section.getName());
					}
					else if (obj instanceof EDirection)
					{
						nbt.setString("Flag", ((EDirection)obj).toString());
					}
				}
				break;

			default:
				return null;
		}

		return nbt;
	}

	/**
	 * Returns the ingredients of the corresponding tab in an ArrayList.
	 */
	public static ArrayList<ItemStack> getIngredientsByType(int tab)
	{
		switch (tab)
		{
			default:
				return new ArrayList<ItemStack>();

			case 0:
				return Helpers.getSortedArrayList(ingredientsTracks.values().toArray());

			case 1:
				return ModPartRegistry.getPartItemStacks();
		}
	}

	/**
	 * Returns all registered track recipes.
	 */
	public static List<EngineerTableRecipe> getRecipesTracks()
	{
		return recipesTracks;
	}

	/**
	 * Returns the length of the given tab.
	 */
	public static int getTabLength(int tab)
	{
		switch (tab)
		{
			default:
				return 0;

			case 0:
				return ingredientsTracks.size();

			case 1:
				return ModPartRegistry.getPartItemStacks().size();
		}
	}

	/**
	 * Register an ingredient for the EngineerTable.
	 *
	 * @return True if successful.
	 */
	public static boolean registerEngineerTableIngredient(EngineerTableIngredientHelper ingredient)
	{
		boolean[] flags = new boolean[2];
		boolean flag = true;

		if (Helpers.isUndefined(ingredient.name))
		{
			return false;
		}

		for (int i = 0; i < ingredient.types.length; ++i)
		{
			ingredient.itemStack.stackSize = 0;

			switch (ingredient.types[i])
			{
				case TRACKS:
					if (!flags[0] && !ingredientsTracks.containsKey(ingredient.name))
					{
						ingredientsTracks.put(ingredient.name, ingredient.itemStack);
						flags[0] = true;
					}
					break;

				default:
					flag = false;
					break;
			}
		}

		return flag;
	}

	/**
	 * Register an EngineerTableRecipe instance.
	 *
	 * @param type - The tab this recipe belongs to.
	 * @param result - The resulting IRailwaySection instance as String representation.
	 * @param matrices - The corresponding matrix indices.
	 * @param indices - The single-letter String and Item instance representation of the matrices. Example: [...], "W", new ItemStack(Blocks.Wood), [...] Or: [...], new ItemStack(Items.Apple), "A",
	 *            [...]
	 * @return True if successful.
	 */
	public static boolean registerTrackRecipe(String result, EDirection dir, String[] matrices, Object... indices)
	{
		NBTTagCompound nbtResult = getBlueprintNBT(0, ModTrackRegistry.getSection(result), dir);
		HashMap<Character, ItemStack> indexMap = new HashMap<Character, ItemStack>();
		String s = "";

		ModTrackRegistry.registerBlueprint(nbtResult);

		if (matrices.length != 5)
		{
			throw new IllegalArgumentException("Engineer Table recipe matrix has to contain five indices! Passed matrix contained " + matrices.length + " indices.");
		}

		for (int i = 0; i < matrices.length; ++i)
		{
			if (matrices[i].length() != 5)
			{
				throw new IllegalArgumentException("Engineer Table recipe matrix has to contain five indices per row! Current row (index " + i + ") had " + matrices[i].length() + " indices.");
			}

			s += matrices[i];
		}

		for (int i = 0; i < indices.length; ++i)
		{
			if (indices[i] == null)
			{
				continue;
			}

			if (indices[i] instanceof String)
			{
				ItemStack itemStack = null;
				String s0 = (String)indices[i];

				if (i + 1 < indices.length && indices[i + 1] != null)
				{
					Object obj = indices[i + 1];

					if (obj instanceof Item)
					{
						itemStack = new ItemStack(((Item)obj), 0, 0);
					}
					else if (obj instanceof ItemStack)
					{
						ItemStack itemStack0 = (ItemStack)obj;
						itemStack0.stackSize = 0;
						itemStack = itemStack0;
					}
					else if (obj instanceof String)
					{
						itemStack = ingredientsTracks.get(obj);
					}
				}

				if (itemStack != null && s0.length() == 1)
				{
					indexMap.put(s0.charAt(0), itemStack);
					++i;
				}
			}
			else if (indices[i] instanceof Item)
			{
				ItemStack itemStack = new ItemStack(((Item)indices[i]), 0, 0);
				String s0 = "";

				if (i + 1 < indices.length)
				{
					Object obj = indices[i + 1];

					if (obj instanceof String)
					{
						s0 = (String)obj;
					}
				}

				if (s0.length() == 1)
				{
					indexMap.put(s0.charAt(0), itemStack);
					++i;
				}
			}
			else if (indices[i] instanceof ItemStack || indices[i] instanceof String)
			{
				ItemStack itemStack;
				String s0 = "";

				if (indices[i] instanceof String)
				{
					itemStack = ingredientsTracks.get(indices[i]);
				}
				else
				{
					itemStack = (ItemStack)indices[i];
					itemStack.stackSize = 0;
				}

				if (i + 1 < indices.length)
				{
					Object obj = indices[i + 1];

					if (obj instanceof String)
					{
						s0 = (String)obj;
					}
				}

				if (s0.length() == 1)
				{
					indexMap.put(s0.charAt(0), itemStack);
					++i;
				}
			}
			else
			{
				throw new IllegalArgumentException("Incorrect object parameters passed! String, Item and ItemStack are allowed. Passed parameter was " + indices[i]);
			}
		}

		char[] order = s.toCharArray();
		ItemStack[] recipe = new ItemStack[25];

		for (int i = 0; i < recipe.length; ++i)
		{
			recipe[i] = indexMap.get(order[i]);
		}

		return recipesTracks.add(new EngineerTableRecipe(recipe, nbtResult));
	}



	public static class EngineerTableRecipe
	{
		ItemStack[] recipe;
		NBTTagCompound nbt;



		public EngineerTableRecipe(ItemStack[] recipe, NBTTagCompound nbtResult)
		{
			this.recipe = recipe;
			nbt = nbtResult;
		}

		public boolean getIsRecipeMatching(ItemStack[] matrix)
		{
			if (matrix.length == recipe.length)
			{
				for (int i = 0; i < matrix.length; ++i)
				{
					int flag = (matrix[i] != null ? 3 : 0) + (recipe[i] != null ? 3 : 0);

					if (flag % 2 != 0)
					{
						return false;
					}

					if (flag == 6)
					{
						if (matrix[i].getItemDamage() != recipe[i].getItemDamage() || matrix[i].stackSize != recipe[i].stackSize)
						{
							return false;
						}
					}
				}

				return true;
			}

			return false;
		}

		public ItemStack getResult()
		{
			ItemStack result = new ItemStack(ModCenter.ItemEngineersBlueprint);
			result.setTagCompound(result.writeToNBT(nbt));

			return result;
		}
	}
}
