package zoranodensha.common.core.registry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.structures.tracks.IRailwaySection;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.util.Helpers;



/**
 * The registry for all track shapes and their corresponding data.
 */
public abstract class ModTrackRegistry
{
	/** The Map containing all registered RailwaySection instances. */
	private static Map<String, IRailwaySection> instances = new HashMap<String, IRailwaySection>();

	/** The ArrayList of all registered recipes' Blueprints. */
	private static ArrayList<NBTTagCompound> blueprints = new ArrayList<NBTTagCompound>();



	/**
	 * Adds all registered sections (and their sub-types, if existent) to the given List.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void addBlueprintsToList(List list)
	{
		Iterator<NBTTagCompound> itera = blueprints.iterator();
		final Item item = ModCenter.ItemEngineersBlueprint;

		while (itera.hasNext())
		{
			NBTTagCompound nbt = itera.next();
			ItemStack itemStack = new ItemStack(item, 1);
			itemStack.setTagCompound(nbt);

			list.add(itemStack);
		}
	}

	/**
	 * Return a RailwaySection instance for the given key name, or null if such is not existent.
	 */
	public static IRailwaySection getSection(String name)
	{
		return instances.get(name);
	}

	/**
	 * Called to add the given NBTTagCompound to the ArrayList of Blueprints.
	 */
	public static boolean registerBlueprint(NBTTagCompound nbt)
	{
		Iterator<NBTTagCompound> itera = blueprints.iterator();

		while (itera.hasNext())
		{
			NBTTagCompound nbt0 = itera.next();

			if (nbt0.equals(nbt))
			{
				return false;
			}
		}

		return blueprints.add(nbt);
	}

	/**
	 * Register a RailwaySection instance.
	 *
	 * @throws IllegalArgumentException If any data array returned is invalid.
	 */
	public static boolean registerSection(IRailwaySection section)
	{
		if (section != null)
		{
			String name = section.getName();
			String prefix = "[ModTrackRegistry] A mod tried to register a RailwaySection ";

			if (Helpers.isUndefined(name))
			{
				throw new IllegalArgumentException(prefix + "with invalid name '" + name + "'.");
			}

			int length = section.getLength();

			if (length <= 0)
			{
				throw new IllegalArgumentException(prefix + "with an invalid length of " + length + ".");
			}

			List<Integer> paths = section.getValidPaths();

			if (paths == null || paths.size() == 0)
			{
				throw new IllegalArgumentException(prefix + "whose returned paths are either null or empty.");
			}

			if (section.getEShapeType() == null)
			{
				throw new IllegalArgumentException(prefix + "a null EShape name.");
			}

			instances.put(name, section);

			return true;
		}

		return false;
	}
}
