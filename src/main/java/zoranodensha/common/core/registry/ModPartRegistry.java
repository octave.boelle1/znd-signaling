package zoranodensha.common.core.registry;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import zoranodensha.api.structures.EEngineerTableTab;
import zoranodensha.api.structures.IEngineerTableExtension.EngineerTableIngredientHelper;
import zoranodensha.api.vehicles.Train;
import zoranodensha.api.vehicles.part.VehParBase;
import zoranodensha.api.vehicles.part.util.AVehiclePartRegistry;
import zoranodensha.api.vehicles.util.VehicleHelper;
import zoranodensha.client.ModProxyClient;
import zoranodensha.client.gui.table.GUIEngineerTable;
import zoranodensha.client.gui.table.buttons.ButtonPopUpOverrideConfirm;
import zoranodensha.common.containers.ContainerEngineerTable;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.util.Helpers;



/**
 * Extension of {@link AVehiclePartRegistry} to handle Preset files as well as their import and export.
 */
public abstract class ModPartRegistry extends AVehiclePartRegistry
{
	/** NBT label String to identify all Preset files. */
	public static final String PRESET_KEY = "PresetFile";
	/** Suffix for all Preset files. */
	public static final String PRESET_SUFFIX = ".zndpreset";



	/**
	 * Returns a list containing {@link net.minecraft.item.ItemStack ItemStacks} of all registered vehicle parts.
	 */
	public static final ArrayList<ItemStack> getPartItemStacks()
	{
		ArrayList<ItemStack> list = new ArrayList<ItemStack>();
		Iterator<VehParBase> itera = instances.values().iterator();

		while (itera.hasNext())
		{
			VehParBase part = itera.next();
			if (part == null)
			{
				continue;
			}

			String name = part.getName();
			if (name == null || name.isEmpty())
			{
				continue;
			}

			ItemStack itemStack = new ItemStack(ModCenter.ItemVehiclePart, 1);
			NBTTagCompound nbt = itemStack.getTagCompound();
			if (nbt == null)
			{
				nbt = itemStack.writeToNBT(new NBTTagCompound());
			}

			nbt.setString(VehParBase.NBT_KEY, name);
			itemStack.setTagCompound(nbt);
			list.add(itemStack);
		}

		return list;
	}

	/**
	 * Export the given {@link net.minecraft.nbt.NBTTagCompound NBTTagCompound} as Preset.
	 *
	 * @return {@code true} if the exported Preset would override an existing file.
	 */
	@SideOnly(Side.CLIENT)
	public static boolean presetExport(NBTTagCompound nbt, GUIEngineerTable gui, boolean isConfirmed)
	{
		/* Verify data */
		String label = "null";
		String filename;

		if (nbt == null || Helpers.isUndefined(label = VehicleHelper.getVehicleName(nbt)) || nbt.getTagList(Train.EDataKey.VEHICLE_DATA.key, 10).tagCount() == 0)
		{
			ModCenter.log.error("[ModPartRegistry] Couldn't export Preset (" + label + "): Null or empty NBTTagCompound.");
			return false;
		}

		/* Write data to file */
		File file = new File(filename = (ModCenter.DIR_ADDONS + "/" + label + PRESET_SUFFIX));

		if (file.exists())
		{
			if (isConfirmed)
			{
				file.delete();
			}
			else
			{
				gui.buttonPopUp = new ButtonPopUpOverrideConfirm(2, (gui.width / 2) - 45, (gui.height / 2) - 16, gui, nbt);
				gui.initGui();
				return true;
			}
		}

		try
		{
			file.createNewFile();
		}
		catch (Exception e)
		{
			ModCenter.log.error("[ModPartRegistry] Couldn't export Preset (" + label + "): Failed to create file (" + filename + ").", e);
			return false;
		}

		try
		{
			CompressedStreamTools.write(nbt, file);
		}
		catch (Exception e)
		{
			ModCenter.log.error("[ModPartRegistry] Couldn't export Preset (" + label + "): Encountered exception while writing.", e);
			return false;
		}

		ModCenter.log.debug("[ModPartRegistry] Successfully exported Preset (" + label + ") to '" + filename + "'");
		ModProxyClient.parsePresets();

		Container container = Minecraft.getMinecraft().thePlayer.openContainer;

		if (container instanceof ContainerEngineerTable)
		{
			((ContainerEngineerTable)container).addSlotsToContainer();
		}

		return false;
	}

	/**
	 * Import a Preset as {@link net.minecraft.nbt.NBTTagCompound NBTTagCompound} from the given {@link java.io.File File}.
	 */
	@SideOnly(Side.CLIENT)
	public static NBTTagCompound presetImport(File file)
	{
		if (file != null && file.exists())
		{
			try
			{
				NBTTagCompound nbt = CompressedStreamTools.read(file);

				return nbt;
			}
			catch (Throwable t)
			{
				ModCenter.log.error("[ModPartRegistry] Encountered exception while reading Preset data from file (" + file.getName() + ").", t);
			}
		}

		return null;
	}

	/**
	 * Calls recipe registration methods of all registered vehicle parts.
	 */
	public static void registerRecipes()
	{
		for (ItemStack itemStack : getPartItemStacks())
		{
			VehParBase part = getPartFromItemStack(itemStack);
			if (part == null)
			{
				ModCenter.log.warn("[ModPartRegistry] There's an ItemStack registered without a corresponding vehicle part instance: " + itemStack);
				continue;
			}

			part.registerItemRecipe(itemStack);
			ModRecipeRegistry.registerEngineerTableIngredient(new EngineerTableIngredientHelper(itemStack, part.getName(), EEngineerTableTab.TRAINS));
		}
	}
}
