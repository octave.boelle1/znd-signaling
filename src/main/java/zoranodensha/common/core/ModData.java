package zoranodensha.common.core;

public final class ModData
{
	public static final String ID = "zoranodensha";
	public static final String NAME = "Zora no Densha";
	public static final String MCVERSION = "1.7.10";
	public static final String MODVERSION = "0.10.0.0";
	public static final String VERSION = MCVERSION + "-" + MODVERSION;
}