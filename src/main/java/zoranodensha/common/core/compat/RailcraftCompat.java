package zoranodensha.common.core.compat;

import cpw.mods.fml.common.Optional.Method;
import cpw.mods.fml.common.event.FMLInterModComms;
import mods.railcraft.api.crafting.RailcraftCraftingManager;
import mods.railcraft.api.fuel.FuelManager;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidStack;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;



/**
 * This class provides additional integration into Railcraft's API for Zora no Densha features.
 */
public final class RailcraftCompat
{
	/**
	 * Called to register Zora no Densha ballast via Forge IMC to Railcraft as valid for the Tunnel Bore.
	 */
	@Method(modid = "Railcraft")
	public static final void registerBallast()
	{
		FMLInterModComms.sendMessage("Railcraft", "ballast", ModData.ID + ":" + ModCenter.BlockBallastGravel.getUnlocalizedName() + "@0");
		FMLInterModComms.sendMessage("Railcraft", "ballast", ModData.ID + ":" + ModCenter.BlockBallastConcrete.getUnlocalizedName() + "@0");
	}

	/**
	 * Called to register recipes to all Railcraft machines, such as the Blast Furnace or Rock Crusher.
	 */
	@Method(modid = "Railcraft")
	public static final void registerMachineRecipes()
	{
		RailcraftCraftingManager.blastFurnace.addRecipe(new ItemStack(Items.iron_ingot), false, false, 640, new ItemStack(ModCenter.ItemIngotSteel));
		RailcraftCraftingManager.blastFurnace.addRecipe(new ItemStack(Item.getItemFromBlock(Blocks.iron_block)), false, false, 5120, new ItemStack(Item.getItemFromBlock(ModCenter.BlockSteel)));
		RailcraftCraftingManager.cokeOven.addRecipe(new ItemStack(Items.coal), false, false, new ItemStack(ModCenter.ItemCoalCoke), new FluidStack(ModCenter.FluidOilCreosote, 90), 200);
		RailcraftCraftingManager.cokeOven.addRecipe(new ItemStack(Item.getItemFromBlock(Blocks.coal_block)), false, false, new ItemStack(Item.getItemFromBlock(ModCenter.BlockCoalCoke)),
													new FluidStack(ModCenter.FluidOilCreosote, 900), 1800);
	}

	/**
	 * Called to register further content, such as boiler fuel heat.
	 */
	@Method(modid = "Railcraft")
	public static final void registerOther()
	{
		FuelManager.addBoilerFuel(ModCenter.FluidFuelDiesel, 43000);
		FuelManager.addBoilerFuel(ModCenter.FluidFuelPetrol, 48000);
		FuelManager.addBoilerFuel(ModCenter.FluidOilCreosote, 4800);
	}
}
