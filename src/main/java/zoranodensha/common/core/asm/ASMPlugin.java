package zoranodensha.common.core.asm;

import java.util.Map;

import cpw.mods.fml.relauncher.IFMLLoadingPlugin;
import zoranodensha.common.core.ModData;



@IFMLLoadingPlugin.TransformerExclusions({ "zoranodensha.common.core.asm", "zoranodensha.api.asm" })
@IFMLLoadingPlugin.MCVersion(ModData.MCVERSION)
@IFMLLoadingPlugin.SortingIndex(1001)
public class ASMPlugin implements IFMLLoadingPlugin
{
	@Override
	public String getAccessTransformerClass()
	{
		return null;
	}

	@Override
	public String[] getASMTransformerClass()
	{
		return new String[] { ASMTransformer.class.getName() };
	}

	@Override
	public String getModContainerClass()
	{
		return null;
	}

	@Override
	public String getSetupClass()
	{
		return null;
	}

	@Override
	public void injectData(Map<String, Object> data)
	{
	}
}
