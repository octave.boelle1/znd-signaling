package zoranodensha.common.core.asm;

import java.util.ListIterator;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.InsnNode;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.VarInsnNode;

import net.minecraft.launchwrapper.IClassTransformer;
import net.minecraft.launchwrapper.Launch;
import zoranodensha.api.vehicles.part.type.seat.ASMHook;
import zoranodensha.common.blocks.tileEntity.TileEntityTrackBase;



/**
 * Zora no Densha ASMTransformer class. Adds byte code manipulation to Minecraft.
 */
public class ASMTransformer implements IClassTransformer
{
	/** Array holding anonymous method transformer subclasses. */
	private static final AMethod[] methods = new AMethod[] {

			/**
			 * Called to apply ASM transformations on {@link net.minecraft.entity.EntityLivingBase EntityLivingBase},
			 * effectively changing method <i>{@link net.minecraft.entity.EntityLivingBase#dismountEntity(net.minecraft.entity.Entity) dismountEntity()}</i>.
			 */
			new AMethod()
			{
				@Override
				public String getClassName(boolean deobf)
				{
					return ESrgNames.C_EntityLivingBase.val(deobf);
				}

				@Override
				public boolean getDoVisit(MethodNode method, boolean deobf)
				{
					return (method.name.equals(ESrgNames.M_dismountEntity.val(deobf)) && method.desc.equals("(L" + ESrgNames.C_Entity.val(true) + ";)V"));
				}

				@Override
				public String getMethodName()
				{
					return ESrgNames.M_dismountEntity.val(true);
				}

				@Override
				public boolean transform(AbstractInsnNode insnNode, MethodNode methodNode, boolean deobf)
				{
					if (insnNode instanceof LabelNode)
					{
						InsnList insnList = new InsnList();
						insnList.add(new VarInsnNode(Opcodes.ALOAD, 0));
						insnList.add(new MethodInsnNode(Opcodes.INVOKESTATIC, ASMHook.class.getName().replace(".", "/"), "movePassengerToSafePlace",
						                                "(L" + ESrgNames.C_EntityLivingBase.val(true) + ";)Z", false));
						LabelNode label0 = new LabelNode();
						insnList.add(new JumpInsnNode(Opcodes.IFEQ, label0));
						insnList.add(new InsnNode(Opcodes.RETURN));
						insnList.add(label0);
						methodNode.instructions.insert(insnNode.getNext(), insnList);
						return true;
					}
					return false;
				}
			},

			/**
			 * Called to apply ASM transformation on {@link net.minecraft.entity.item.EntityMinecart EntityMinecart},
			 * effectively changing method <i>{@link net.minecraft.entity.item.EntityMinecart#onUpdate() onUpdate()}</i>.
			 */
			new AMethod()
			{
				@Override
				public String getClassName(boolean deobf)
				{
					return ESrgNames.C_EntityMinecart.val(deobf);
				}

				@Override
				public boolean getDoVisit(MethodNode method, boolean deobf)
				{
					return (method.name.equals(ESrgNames.M_onUpdate.val(deobf)) && "()V".equals(method.desc));
				}

				@Override
				public String getMethodName()
				{
					return ESrgNames.M_onUpdate.val(true);
				}

				@Override
				public boolean transform(AbstractInsnNode insnNode, MethodNode methodNode, boolean deobf)
				{
					if (insnNode.getOpcode() == Opcodes.INVOKEVIRTUAL && insnNode instanceof MethodInsnNode)
					{
						MethodInsnNode methodInsnNode = (MethodInsnNode)insnNode;
						if (methodInsnNode.owner.equals(ESrgNames.C_Profiler.val(true)) && methodInsnNode.name.equals(ESrgNames.M_endSection.val(deobf)) && "()V".equals(methodInsnNode.desc))
						{
							InsnList insnList = new InsnList();
							insnList.add(new VarInsnNode(Opcodes.ALOAD, 0));
							insnList.add(new MethodInsnNode(Opcodes.INVOKESTATIC, TileEntityTrackBase.class.getName().replace(".", "/"), "setPositionOnTrackWithCheck",
							                                "(L" + ESrgNames.C_EntityMinecart.val(true) + ";)Z", false));
							LabelNode label0 = new LabelNode();
							insnList.add(new JumpInsnNode(Opcodes.IFEQ, label0));
							insnList.add(new InsnNode(Opcodes.RETURN));
							insnList.add(label0);
							methodNode.instructions.insert(insnNode.getNext(), insnList);
							return true;
						}
					}
					return false;
				}
			},

			/**
			 * Called to apply ASM transformation on {@link net.minecraft.entity.item.EntityMinecart EntityMinecart},
			 * effectively changing method <i>{@link net.minecraft.entity.item.EntityMinecart#func_145821_a(int, int, int, double, double, double, net.minecraft.block.Block, int) func_145821_a()}</i>.
			 */
			new AMethod()
			{
				@Override
				public String getClassName(boolean deobf)
				{
					return ESrgNames.C_EntityMinecart.val(deobf);
				}

				@Override
				public boolean getDoVisit(MethodNode method, boolean deobf)
				{
					return (method.name.equals(ESrgNames.M_func_145821_a.val(deobf)) && method.desc.equals("(IIIDDL" + ESrgNames.C_Block.val(true) + ";I)V"));
				}

				@Override
				public String getMethodName()
				{
					return ESrgNames.M_func_145821_a.val(true);
				}

				@Override
				public boolean transform(AbstractInsnNode insnNode, MethodNode methodNode, boolean deobf)
				{
					if (insnNode.getOpcode() == Opcodes.PUTFIELD && insnNode instanceof FieldInsnNode)
					{
						FieldInsnNode fieldInsnNode = (FieldInsnNode)insnNode;
						if (fieldInsnNode.owner.equals(ESrgNames.C_EntityMinecart.val(true)) && fieldInsnNode.name.equals(ESrgNames.F_fallDistance.val(deobf)) && "F".equals(fieldInsnNode.desc))
						{
							InsnList insnList = new InsnList();
							insnList.add(new VarInsnNode(Opcodes.ALOAD, 0));
							insnList.add(new MethodInsnNode(Opcodes.INVOKESTATIC, TileEntityTrackBase.class.getName().replace(".", "/"), "getIsOnTrackBase",
							                                "(L" + ESrgNames.C_EntityMinecart.val(true) + ";)Z", false));
							LabelNode label0 = new LabelNode();
							insnList.add(new JumpInsnNode(Opcodes.IFEQ, label0));
							insnList.add(new InsnNode(Opcodes.RETURN));
							insnList.add(label0);
							methodNode.instructions.insert(insnNode.getNext(), insnList);
							return true;
						}
					}
					return false;
				}
			},

			/**
			 * Called to apply ASM transformations on {@link net.minecraft.client.renderer.entity.RenderManager RenderManager},
			 * effectively changing method <i>{@link net.minecraft.client.renderer.entity.RenderManager#renderDebugBoundingBox(net.minecraft.entity.Entity, double, double, double, float, float)
			 * renderDebugBoundingBox()}</i>.
			 */
			new AMethod()
			{
				@Override
				public String getClassName(boolean deobf)
				{
					return ESrgNames.C_RenderManager.val(deobf);
				}

				@Override
				public boolean getDoVisit(MethodNode method, boolean deobf)
				{
					return (method.name.equals(ESrgNames.M_renderDebugBoundingBox.val(deobf)) && ("(L" + ESrgNames.C_Entity.val(true) + ";DDDFF)V").equals(method.desc));
				}

				@Override
				public String getMethodName()
				{
					return ESrgNames.M_renderDebugBoundingBox.val(true);
				}

				@Override
				public boolean transform(AbstractInsnNode insnNode, MethodNode methodNode, boolean deobf)
				{
					if (insnNode.getOpcode() == Opcodes.INVOKESTATIC && insnNode instanceof MethodInsnNode)
					{
						MethodInsnNode methodInsnNode = (MethodInsnNode)insnNode;
						if (methodInsnNode.owner.equals(ESrgNames.C_GL11.val(true)) && methodInsnNode.name.equals(ESrgNames.M_glDepthMask.val(deobf)) && "(Z)V".equals(methodInsnNode.desc))
						{
							InsnList insnList = new InsnList();
							insnList.add(new VarInsnNode(Opcodes.DLOAD, 4));
							insnList.add(new VarInsnNode(Opcodes.ALOAD, 1));
							insnList.add(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, ESrgNames.C_Entity.val(true), ESrgNames.M_getYOffset.val(deobf), "()D", false));
							insnList.add(new InsnNode(Opcodes.DSUB));
							insnList.add(new VarInsnNode(Opcodes.DSTORE, 4));
							methodNode.instructions.insert(insnNode.getNext(), insnList);
							return true;
						}
					}
					return false;
				}
			} };



	@Override
	public byte[] transform(String obfName, String deobfName, byte[] basicClass)
	{
		final boolean deobf = Boolean.TRUE.equals(Launch.blackboard.get("fml.deobfuscatedEnvironment"));
		String name = (deobf) ? deobfName : obfName;

		outer: for (AMethod aMethod : methods)
		{
			if (aMethod.getClassName(deobf).replaceAll("/", ".").equals(name))
			{
				/* Initialise. */
				print("ASM transformation begins. [DeobfEnv=%b, Class=%s, Method=%s]", deobf, aMethod.getClassName(true), aMethod.getMethodName());
				ClassNode classNode = new ClassNode();
				ClassReader classReader = new ClassReader(basicClass);
				classReader.accept(classNode, 0);

				/* Search method node. */
				for (MethodNode methodNode : classNode.methods)
				{
					if (aMethod.getDoVisit(methodNode, deobf))
					{
						/* Search INSN node and transform. */
						ListIterator<AbstractInsnNode> itera = methodNode.instructions.iterator();
						while (itera.hasNext())
						{
							if (aMethod.transform(itera.next(), methodNode, deobf))
							{
								/* Write to byte buffer. */
								ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
								classNode.accept(writer);
								basicClass = writer.toByteArray();

								print("ASM transformation finished successfully.");
								continue outer;
							}
						}
						print("ASM transformation FAILED: Node not found.");
						continue outer;
					}
				}
				print("ASM transformation FAILED: Method not found.");
			}
		}

		return basicClass;
	}

	/**
	 * Helper method to print the given message with a footprint.
	 */
	private static final void print(String format, Object... args)
	{
		System.out.println(String.format("[Zora no Densha][%s] %s", ASMTransformer.class.getSimpleName(), String.format(format, args)));
	}



	/**
	 *
	 */
	private static abstract class AMethod
	{
		/**
		 * Return the full class name of the class this method resides in.
		 *
		 * @param deobf - If {@code true}, return the deobfuscated class name.
		 * @return This method's full class name.
		 */
		public abstract String getClassName(boolean deobf);

		/**
		 * Check whether the given method node is the target method node.
		 *
		 * @param methodNode - The method node in question.
		 * @param deobf - {@code true} if the environment is deobfuscated.
		 * @return {@code true} if the given method node is the target method.
		 */
		public abstract boolean getDoVisit(MethodNode methodNode, boolean deobf);

		/**
		 * Return the full (if possible, deobfuscated) name of the transformed method.
		 * 
		 * @return The transformed method's name.
		 */
		public abstract String getMethodName();

		/**
		 * Transform the given node.
		 *
		 * @param insnNode - The node to modify.
		 * @param methodNode - The respective method node.
		 * @param deobf - {@code true} if the environment is deobfuscated.
		 * @return {@code true} if successful.
		 */
		public abstract boolean transform(AbstractInsnNode insnNode, MethodNode methodNode, boolean deobf);
	}
}
