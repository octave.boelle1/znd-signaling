package zoranodensha.common.core;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.Level;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraft.world.WorldSavedData;
import net.minecraft.world.storage.MapStorage;



/**
 * <p>
 * A database which holds information about trains in the game world.
 * </p>
 * 
 * @author Jaffa
 */
public class TrainDatabase extends WorldSavedData
{

	/**
	 * The name of the database. This string will be used when a new database is created and saved to the world.
	 */
	private static final String DATABASE_NAME = ModData.ID + "_TrainDatabase";

	/**
	 * The key to use when dealing with any NBT information regarding the autodrive trains array list.
	 */
	private static final String AUTODRIVE_TRAINS_KEY = "autodriveTrainsK";
	private static final String AUTODRIVE_TRAINS_VALUE = "autodriveTrainsV";

	/**
	 * A list of Entity IDs of trains in the world which are currently in 'autodrive' mode, and their respective 'time' values.
	 * 
	 * @see {@link net.minecraft.entity.Entity#getEntityId()}
	 * @see {@link net.minecraft.world.World#getTotalWorldTime()}
	 */
	private HashMap<Integer, Long> autodriveTrains;



	/**
	 * <p>
	 * Initialises a new instance of the Train Database class with a default name.
	 * </p>
	 */
	public TrainDatabase()
	{
		this(DATABASE_NAME);
	}

	/**
	 * <p>
	 * Initialises a new instance of the Train Database class with a custom name.
	 * </p>
	 * 
	 * @param databaseName - The name to assign to the new database.
	 */
	public TrainDatabase(String databaseName)
	{
		super(databaseName);

		initialiseDatabase();
	}


	/**
	 * <p>
	 * Gets whether there is a database entry created by a train of entity ID {@code entityID}.
	 * </p>
	 * 
	 * @return - A {@code boolean} which will be {@code true} if there is an existing 'time' value created by a train of entity ID {@code entityID}.
	 */
	public boolean getAutodriveTrainExists(int entityID)
	{
		return autodriveTrains.containsKey(entityID);
	}


	/**
	 * <p>
	 * Gets the 'time' value that was last updated by the train of entity ID {@code entityID}.
	 * </p>
	 * 
	 * @return - A {@code long} value which is the last 'time' value that the train of the supplied entity ID updated.
	 */
	public Long getAutodriveTrainTime(int entityID)
	{
		return autodriveTrains.get(entityID);
	}


	/**
	 * <p>
	 * Attempts to retreive an existing train database from the specified world object. If there is no database, a new one will be created and attached to the specified world object.
	 * </p>
	 * 
	 * @param world - The world object to load from.
	 * @return - A {@link TrainDatabase} instance.
	 */
	public static TrainDatabase getTrainDatabase(World world)
	{
		/*
		 * Get the per-world (dimension-based) world storage.
		 */
		MapStorage mapStorage = world.perWorldStorage;

		/*
		 * Extract any existing database from the storage.
		 */
		TrainDatabase instance = (TrainDatabase)mapStorage.loadData(TrainDatabase.class, DATABASE_NAME);

		/*
		 * If the database is null, create a new blank one.
		 */
		if (instance == null)
		{
			instance = new TrainDatabase();
			mapStorage.setData(DATABASE_NAME, instance);
		}

		return instance;
	}


	/**
	 * <p>
	 * When called, all database values will be reset to their defaults.
	 * </p>
	 */
	private void initialiseDatabase()
	{
		autodriveTrains = new HashMap<Integer, Long>();
	}


	@Override
	public final void readFromNBT(NBTTagCompound nbt)
	{
		/*
		 * Reset all ArrayLists, etc
		 */
		initialiseDatabase();

		/*
		 * Load autodrive trains
		 */
		int index = 0;
		while (true)
		{
			if (nbt.hasKey(AUTODRIVE_TRAINS_KEY + index))
			{
				autodriveTrains.put(nbt.getInteger(AUTODRIVE_TRAINS_KEY + index), nbt.getLong(AUTODRIVE_TRAINS_VALUE + index));
				index++;
			}
			else
			{
				break;
			}
		}
	}


	/**
	 * <p>
	 * Called from a train to update its database entry with a new 'time' value. A train calling this method every game tick with an up-to-date 'time' value is sufficient to allow the chunkloading
	 * system to determine if loaded chunks should be kept when the world reloads. An up-to-date 'time' value proves that the train is not dead and all the chunks it loaded in the current session
	 * should be kept for the next session.
	 * </p>
	 * <p>
	 * For example, if a train is destroyed, its 'time' value in this database will not be up to date compared to {@link World#getTotalWorldTime()} and the chunkloading system will determine that the
	 * train was dead when the world was last unloaded and therefore not keep any chunks loaded by that train.
	 * </p>
	 * 
	 * @param entityID - The entity ID of the train that calls this method.
	 * @param time - A {@code long} which is obtained with {@link World#getTotalWorldTime()}.
	 */
	public void updateAutodriveTrainTime(int entityID, long time)
	{
		autodriveTrains.put(entityID, time);
		markDirty();
	}


	@Override
	public final void writeToNBT(NBTTagCompound nbt)
	{
		/*
		 * Save autodrive trains
		 */
		int index = 0;
		for (Integer train : autodriveTrains.keySet())
		{
			nbt.setInteger(AUTODRIVE_TRAINS_KEY + index, train);
			nbt.setLong(AUTODRIVE_TRAINS_VALUE + index, autodriveTrains.get(train));
			index++;
		}
	}

}
