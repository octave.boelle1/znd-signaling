package zoranodensha.common.core.cfg;

public class Ctgy_BlocksItems extends AConfigCategory
{
	public boolean enableFuelExplosion;
	public boolean enableInstantTracks;
	public boolean enableInstantVehicles;
	public boolean enableKeyCrafting;
	public boolean enableTrackExplosion;
	public boolean multiBlockDrop;
	public int multiBlockUpdate;
	public boolean trainItemDrop;



	public Ctgy_BlocksItems()
	{
		super("blocks_items");
	}

	@Override
	protected void loadCategory()
	{
		enableFuelExplosion = getBoolean("fuel_explosion", true);
		enableInstantTracks = getBoolean("instant_tracks", true);
		enableInstantVehicles = getBoolean("instant_vehicles", true);
		enableKeyCrafting = getBoolean("key_crafting", true);
		enableTrackExplosion = getBoolean("track_explosion", true);
		multiBlockDrop = getBoolean("multi_block_drop", true);
		multiBlockUpdate = getInt("multi_block_update", 5, 0, 100);
		trainItemDrop = getBoolean("vehicle_item_drop", true);
	}
}