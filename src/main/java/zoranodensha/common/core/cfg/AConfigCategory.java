package zoranodensha.common.core.cfg;

/**
 * Configuration category, holding the respective category's name.<br>
 * Entries are held by their respective subclass category.
 */
public abstract class AConfigCategory
{
	/** Category name. */
	private final String categoryName;
	/** Category language key. */
	private final String categoryLocale;



	public AConfigCategory(String name)
	{
		categoryName = name;
		categoryLocale = ModConfig.langKey + "." + name + ".";
	}


	/*
	 * =====================================
	 * Helper methods to read config values.
	 * =====================================
	 */
	protected boolean getBoolean(String name, boolean defaultValue)
	{
		return ModConfig.config.getBoolean(name, categoryName, defaultValue, "", categoryLocale + name);
	}
	
	protected float getFloat(String name, float defaultValue, float minValue, float maxValue)
	{
		return ModConfig.config.getFloat(name, categoryName, defaultValue, minValue, maxValue, "", categoryLocale + name);
	}
	
	protected int getInt(String name, int defaultValue, int minValue, int maxValue)
	{
		return ModConfig.config.getInt(name, categoryName, defaultValue, minValue, maxValue, "", categoryLocale + name);
	}
	
	protected String getString(String name, String defaultValue)
	{
		return ModConfig.config.getString(name, categoryName, defaultValue, "", categoryLocale + name);
	}


	/*
	 * =======================================
	 * Load methods, overridden by subclasses.
	 * =======================================
	 */
	/**
	 * Load this category from the mod's configuration file.
	 */
	public void load()
	{
		ModConfig.config.getCategory(categoryName).setLanguageKey(categoryLocale + "ctgy");
		loadCategory();
	}

	/**
	 * Load category-specific configuration values.
	 */
	protected abstract void loadCategory();
}