package zoranodensha.common.core.cfg;

import java.io.File;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraftforge.common.config.Configuration;
import zoranodensha.common.core.ModCenter;
import zoranodensha.common.core.ModData;



/**
 * General mod configuration file.
 */
public class ModConfig
{
	/** Initialised in {@link #init(File) init()}, during {@link cpw.mods.fml.common.event.FMLPreInitializationEvent preInit}. */
	public static Configuration config;
	/** Configuration language key, used for localisation. */
	public static final String langKey = ModData.ID + ".cfg";

	/*
	 * General categories
	 */
	public Ctgy_BlocksItems blocks_items;
	public Ctgy_Compat compat;
	public Ctgy_EasterEggs easterEggs;
	public Ctgy_General general;
	public Ctgy_Trains trains;
	public Ctgy_WorldGen worldGen;

	/*
	 * Client-sided categories
	 */
	@SideOnly(Side.CLIENT) public zoranodensha.client.cfg.Ctgy_CabControls cabControls;
	@SideOnly(Side.CLIENT) public zoranodensha.client.cfg.Ctgy_ModelEditor modelEditor;
	@SideOnly(Side.CLIENT) public zoranodensha.client.cfg.Ctgy_Rendering rendering;



	/**
	 * Initialises the configuration's values.
	 */
	public void init()
	{
		blocks_items = new Ctgy_BlocksItems();
		compat = new Ctgy_Compat();
		easterEggs = new Ctgy_EasterEggs();
		general = new Ctgy_General();
		trains = new Ctgy_Trains();
		worldGen = new Ctgy_WorldGen();

		blocks_items.load();
		compat.load();
		easterEggs.load();
		general.load();
		trains.load();
		worldGen.load();

		if (config.hasChanged())
		{
			config.save();
		}
	}

	/**
	 * Creates and initialises a suggested configuration from the given file for the active side.
	 */
	public static final ModConfig init(File suggestedFile)
	{
		ModConfig cfg = ModCenter.proxy.getModConfig();
		config = new Configuration(suggestedFile, true);
		config.load();
		cfg.init();

		return cfg;
	}
}