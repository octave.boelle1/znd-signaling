package zoranodensha.common.fluids;

import net.minecraft.util.IIcon;
import net.minecraftforge.fluids.Fluid;
import zoranodensha.common.core.ModCenter;



public class FluidTar extends Fluid
{
	public FluidTar()
	{
		super("tar");

		setBlock(ModCenter.BlockFluidTar);
		setDensity(1153);
		setTemperature(333);
		setViscosity(5000);
	}

	// @Override
	// public int getColor() {
	//
	// return 0xBBA021;
	// }

	@Override
	public IIcon getFlowingIcon()
	{
		return block.getIcon(2, 0);
	}

	@Override
	public IIcon getStillIcon()
	{
		return block.getIcon(0, 0);
	}
}
