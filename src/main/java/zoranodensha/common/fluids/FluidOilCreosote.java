package zoranodensha.common.fluids;

import net.minecraft.util.IIcon;
import net.minecraftforge.fluids.Fluid;
import zoranodensha.common.core.ModCenter;



public class FluidOilCreosote extends Fluid
{
	public FluidOilCreosote()
	{
		super("creosote");

		setBlock(ModCenter.BlockFluidOilCreosote);
		setDensity(1070);
		setTemperature(298);
		setViscosity(400);
	}

	// @Override
	// public int getColor() {
	//
	// return 0x313131;
	// }

	@Override
	public IIcon getFlowingIcon()
	{
		return block.getIcon(2, 0);
	}

	@Override
	public IIcon getStillIcon()
	{
		return block.getIcon(0, 0);
	}
}
