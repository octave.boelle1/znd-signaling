package zoranodensha.common.fluids;

import net.minecraft.util.IIcon;
import net.minecraftforge.fluids.Fluid;
import zoranodensha.common.core.ModCenter;



public class FluidFuelPetrol extends Fluid
{
	public FluidFuelPetrol()
	{
		super("fuel");

		setBlock(ModCenter.BlockFluidFuelPetrol);
		setDensity(737);
		setTemperature(289);
		setViscosity(700);
	}

	// @Override
	// public int getColor() {
	//
	// return 0xBB7B21;
	// }

	@Override
	public IIcon getFlowingIcon()
	{
		return block.getIcon(2, 0);
	}

	@Override
	public IIcon getStillIcon()
	{
		return block.getIcon(0, 0);
	}
}
