package zoranodensha.common.fluids;

/**
 * Identifiers for Zora no Densha fluid types.
 * 
 * @author Leshuwa Kaiheiwa
 */
public enum EFluid
{
	DIESEL,
	PETROL,
	BIO_CRUDE,
	CREOSOTE,
	LUBRICANT,
	MACHINERY,
	SHALE_CRUDE,
	TAR
}
