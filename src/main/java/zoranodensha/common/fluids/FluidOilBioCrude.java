package zoranodensha.common.fluids;

import net.minecraft.util.IIcon;
import net.minecraftforge.fluids.Fluid;
import zoranodensha.common.core.ModCenter;



public class FluidOilBioCrude extends Fluid
{
	public FluidOilBioCrude()
	{
		super("oilBioCrude");

		setBlock(ModCenter.BlockFluidOilBioCrude);
		setDensity(1250);
		setTemperature(293);
		setViscosity(2300);
	}

	// @Override
	// public int getColor() {
	//
	// return 0xB1741F;
	// }

	@Override
	public IIcon getFlowingIcon()
	{
		return block.getIcon(2, 0);
	}

	@Override
	public IIcon getStillIcon()
	{
		return block.getIcon(0, 0);
	}
}
