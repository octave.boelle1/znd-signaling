package zoranodensha.common.fluids;

import net.minecraft.util.IIcon;
import net.minecraftforge.fluids.Fluid;
import zoranodensha.common.core.ModCenter;



public class FluidOilMachinery extends Fluid
{
	public FluidOilMachinery()
	{
		super("oilMachinery");

		setBlock(ModCenter.BlockFluidOilMachinery);
		setDensity(910);
		setTemperature(288);
		setViscosity(900);
	}

	// @Override
	// public int getColor() {
	//
	// return 0xBB9821;
	// }

	@Override
	public IIcon getFlowingIcon()
	{
		return block.getIcon(2, 0);
	}

	@Override
	public IIcon getStillIcon()
	{
		return block.getIcon(0, 0);
	}
}
