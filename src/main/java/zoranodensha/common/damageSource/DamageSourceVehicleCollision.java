package zoranodensha.common.damageSource;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.StatCollector;
import zoranodensha.common.core.ModData;
import zoranodensha.common.util.ZnDMathHelper;



public class DamageSourceVehicleCollision extends EntityDamageSource
{
	public DamageSourceVehicleCollision(Entity entity)
	{
		super(ModData.ID + ".trainCollision", entity);
		setDamageIsAbsolute();
	}

	@Override
	public IChatComponent func_151519_b(EntityLivingBase entity)
	{
		ItemStack itemStack = damageSourceEntity instanceof EntityLivingBase ? ((EntityLivingBase)damageSourceEntity).getHeldItem() : null;
		String s = "death.attack." + damageType;
		String s1 = s + ".player";

		if (itemStack != null && itemStack.hasDisplayName() && StatCollector.canTranslate(s1))
		{
			return new ChatComponentTranslation(s1, new Object[] { entity.func_145748_c_(), damageSourceEntity.func_145748_c_(), itemStack.func_151000_E() });
		}

		return (StatCollector.canTranslate(s) ? new ChatComponentTranslation(s, new Object[] { entity.func_145748_c_(), damageSourceEntity.func_145748_c_() })
				: new ChatComponentTranslation((s + "_" + ZnDMathHelper.ran.nextInt(4)), new Object[] { entity.func_145748_c_(), damageSourceEntity.func_145748_c_() }));
	}
}
